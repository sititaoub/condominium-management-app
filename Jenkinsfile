/**
 * Load shared libs
 */
@Library('jenkins-pipeline-cedac-library')_
import com.cedac.jenkins.pipeline.utils.JobUtilities
import com.cedac.jenkins.pipeline.rest.client.RestClient

import groovy.json.JsonBuilder;
import groovy.json.JsonOutput;
import groovy.json.JsonSlurperClassic;

/**
 * Define global vars
 */ 
def pom
def version
// Nome branch corrente
def branchName = env.BRANCH_NAME
// User IDs allowed to complete production stage
def prodApproverUsers = JobUtilities.getUsersAllowedToDeployToProduction();

/*** Nome micro service per il deploy via Supervisor ***/
def microServiceName = 'condominium-management-app'

/*** Nome service per il deploy via Docker ***/
def dockerServiceName = 'condominium-management-app'

/**
 *	UNIQUE build number
 */
JobUtilities.printEngravedMessage this, "BuildNumber currently: $BUILD_NUMBER"

JobUtilities.getUniqueBuildNumber this

JobUtilities.printEngravedMessage this, "Buildnumber changed to: $env.BUILD_ID"

/**
 *	Pipeline NOT ALLOWED from 'master' branch
 */
if( branchName ==~ /.*master.*/ ) {
	currentBuild.result = 'ABORTED'
	error( "Branch $branchName not allowed to run")
}

/**
 * Only docker nodes are good for run
 */
node('test1-docker-local') {
    // Set JDK and Maven envs
    withEnv(["JAVA_HOME=${tool 'jdk_8'}", "PATH+MAVEN=${tool 'Maven 3.2.1'}:${tool 'jdk_8'}/bin"]) {
    
    	stageGitCheckoutAndMavenBuild( productName: microServiceName, microServiceName: microServiceName, dockerServiceName: dockerServiceName );
/*
    stage('Checkout & Build') {
        // Get some code from a GitHub repository
        checkout scm
        // If we're not in a multi-branch pipeline, get branch name from git
        if( branchName == null ) {
        	branchName = sh(returnStdout: true, script: 'git show -s --pretty=%d HEAD').trim()
        }
        echo "Current branch: $branchName"

        // we want to pick up the version from the pom
        pom = readMavenPom file: 'pom.xml'
        version = pom.version.replace("-SNAPSHOT", ".rc${env.BUILD_ID}")
        // export release candidate version
        env.CEDAC_RELEASE_CANDIDATE=version
        
        // Run the maven build
        if (isUnix()) {
            sh "mvn -Dmaven.test.skip clean package"
        } else {
            bat "mvn -Dmaven.test.skip clean package"
        }
    }
*/
    	stageMavenTestVerify( productName: microServiceName, test: false, reportTests: false, archiveJar: true );
/*
    stage('Unit Tests') {
        if (isUnix()) {
            echo 'DISABLED: sh "mvn test verify"'
        } else {
            echo 'DISABLED: bat "mvn test verify"'
        }
        // No tests currently available!!
        //junit '** /target/surefire-reports/TEST-*.xml'
        archive 'target/ *.jar'
        manager.addShortText("v$version")
    }
*/
    	stageDockerDeployToDev( productName: microServiceName, docker: true, deploy: true, dockerServicePath: '/home/manager/dockerize/management-dev', dockerServiceName: dockerServiceName );
/*
    stage('Deploy to DEV') {
        input "Ready to promote version ${version} to DEV env?"
        
        // Build docker image
        if (isUnix()) {
            sh "mvn validate docker:build -P docker"
        } else {
            bat "mvn validate docker:build -P docker"
        }
        
        // Tag and push docker image
        curTag = pom.properties['docker.image.prefix'] + '/' + pom.artifactId + ':' + pom.version
        bldTag = pom.properties['docker.image.prefix'] + '/' + pom.artifactId + ':' + version
        sh "docker tag $curTag $bldTag"
        sh "docker push $bldTag"

        build job: 'update-docker-on-manager-svil',
            parameters: [
                [$class: 'StringParameterValue', name: 'LOCAL_PATH', value: '/home/manager/dockerize/management-dev'], 
                [$class: 'StringParameterValue', name: 'FULL_DOCKER_IMAGE', value: bldTag], 
                [$class: 'StringParameterValue', name: 'SERVICE_NAME', value: dockerServiceName]
            ]

        manager.removeBadge(0)
        manager.addBadge("/plugin/promoted-builds/icons/16x16/star-blue.png", "v$version deployed to DEV")
    }
*/
    	stageDockerDeployToTest( productName: microServiceName, deploy: true, dockerServicePath: '/home/manager/dockerize/management-test', dockerServiceName: dockerServiceName, dockerStackName: 'c102man_stack' );
/*
    stage('Deploy to TEST') {
        input "Ready to promote version ${version} to TEST env?"

        build job: 'exec-shell-on-cond102-test-2',
            parameters: [
                [$class: 'StringParameterValue', name: 'LOCAL_PATH', value: '/home/manager/dockerize/management-dev'], 
                [$class: 'StringParameterValue', name: 'FULL_DOCKER_IMAGE', value: bldTag], 
                [$class: 'StringParameterValue', name: 'SERVICE_NAME', value: dockerServiceName]
            ]

        manager.removeBadge(0)
        manager.addBadge("/plugin/promoted-builds/icons/16x16/star-green.png", "v$version deployed to TEST")
    }
*/
    /**
     * Allow UAT & PRODUCTION promotions only from 'develop' branch
     */
	if( branchName ==~ /develop/ ) {
	
    	stageSupervisorDeployToUat( productName: microServiceName, deploy: true, notifySlack: true, microServiceName: microServiceName );
/*
	    stage('Deploy to UAT') {
	        // SM added: notify to Slack
	        slackSend channel: '#test-collaudo', color: 'good', message: 'Installazione di '+ pom.artifactId +" v$version in TEST/COLLAUDO imminente..."
	    	
	        input "Ready to promote version ${version} to UAT env?"
	
			filNam = pom.artifactId + '-' + pom.version + '.jar'
	
	        build job: 'deploy-to-collaudo-srv3',
	            parameters: [
	                [$class: 'StringParameterValue', name: 'Server', value: '172.16.1.49'], 
	                [$class: 'StringParameterValue', name: 'Job', value: JOB_NAME],
	                [$class: 'StringParameterValue', name: 'FileName', value: filNam],
	                [$class: 'StringParameterValue', name: 'MicroServiceName', value: microServiceName],
	                [$class: 'StringParameterValue', name: 'BuildSelection', value: "<SpecificBuildSelector><buildNumber>$BUILD_NUMBER</buildNumber></SpecificBuildSelector>"]
	            ]
	
	        manager.removeBadge(0)
	        manager.addBadge("/plugin/promoted-builds/icons/16x16/star-gold.png", "v$version deployed to UAT")
	        // SM added: notify to Slack
	        logs = JobUtilities.getRedmineReferencesFromLastBuild this
	        slackSend channel: '#test-collaudo', color: 'good', message: 'Installazione di '+ pom.artifactId +" v$version in TEST/COLLAUDO conclusa correttamente.\nSegnalazioni collegate:\n$logs"
	    }
*/
    	stageSupervisorDeployToProduction( productName: microServiceName, archiveJar: true, docker: true, deploy: false, microServiceName: microServiceName );
/*
	    stage('Deploy to PRODUCTION') {
	    	milestone label: "Release to PRODUCTION"
	        timeout(time: 1, unit: 'HOURS') {
	            input message: "Ready to RELEASE version ${version} to PRODUCTION env?", submitter: prodApproverUsers
	            
	            // Release maven project            
	 			sshagent(['jenkinsSSH']) {
		            if (isUnix()) {
	                    sh 'mvn -Dresume=false -B jgitflow:release-start jgitflow:release-finish -P jenkins-release'
		                sh "git checkout master"
		                // Reload POM and archive artifact
				        pom = readMavenPom file: 'pom.xml'
				        echo "pom: $pom"
	            		// Archive released artifact
			            archive 'target/ *.jar'
			            // Restore develop branch
			            sh "git checkout develop"
		            } else {
		                bat "mvn -Dresume=false -B jgitflow:release-start jgitflow:release-finish -P jenkins-release"
		                // TODO
		            }
	            }
	            // manager.removeBadge(0)	// Do not remove previous badge
	            manager.addBadge("/plugin/m2release/img/releasebadge.png", "v$pom.version correctly RELEASED")
	            // Mark this as keep forever
	            JobUtilities.markCurrentJobAsKeepForever this
	
		        // Tag and push docker image as RELEASED
		        rlsTag = pom.properties['docker.image.prefix'] + '/' + pom.artifactId + ':' + pom.version
		        lstTag = pom.properties['docker.image.prefix'] + '/' + pom.artifactId + ':latest'
		        sh "docker tag $curTag $rlsTag"
		        sh "docker push $rlsTag"
		        sh "docker tag $curTag $lstTag"
		        sh "docker push $lstTag"
		
				// Deploy to production            
		 		filNam = pom.artifactId + '-' + pom.version + '.jar'
/ * TODO
		        build job: 'deploy-to-collaudo-srv3',
		            parameters: [
		                [$class: 'StringParameterValue', name: 'Server', value: '172.16.1.49'], 
		                [$class: 'StringParameterValue', name: 'Job', value: JOB_NAME],
		                [$class: 'StringParameterValue', name: 'FileName', value: filNam],
		                [$class: 'StringParameterValue', name: 'MicroServiceName', value: microServiceName],
		                [$class: 'StringParameterValue', name: 'BuildSelection', value: "<SpecificBuildSelector><buildNumber>$BUILD_NUMBER</buildNumber></SpecificBuildSelector>"]
		            ]
* /
	            manager.addBadge("green.gif", "v$pom.version deployed to PRODUCTION")
	        }
	    }
*/
    } else {
		JobUtilities.printEngravedMessage this, "Branch $branchName not allowed to deploy to UAT & PRODUCTION"
	}
}
}

