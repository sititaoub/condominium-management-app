/*
 * accounting-record.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .controller('AccountingTransactionController', AccountingTransactionController);

    AccountingTransactionController.$inject = ['$state', 'AccountingTransactionService', 'AccountingTransactionFactoryService', 'NgTableParams', 'AlertService', 'NotifierService', 'transactionFormConfiguration'];

    /* @ngInject */
    function AccountingTransactionController($state, AccountingTransactionService, AccountingTransactionFactoryService, NgTableParams, AlertService, NotifierService, transactionFormConfiguration) {
        var vm = this;
        vm.removeTransaction = removeTransaction;
        vm.openEditForm = openEditForm;
        vm.toggleFilters = toggleFilters;
        vm.openPrintRipartitionsOption = openPrintRipartitionsOption;
        vm.openPrintFinalBalanceOption = openPrintFinalBalanceOption;
        vm.doPrintConfirm = doPrintConfirm;

        activate();

        ////////////////

        function activate() {
            vm.loading = false;
            vm.firstLoad = true;
            vm.transactionFormConfiguration = transactionFormConfiguration;
            vm.periodId= $state.params.periodId;
            vm.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10
            }, {
                    getData: loadAccountingTransactions,
                });

            vm.showFilters = vm.tableParams.hasFilter();

            vm.accoutingTransactionTypes = _.keys(transactionFormConfiguration).map(function (key) {
                return { name: key };
            });

        }

        function toggleFilters() {
            vm.showFilters = !vm.showFilters;
        }

        function openEditForm(row) {
            var editUrl = '.edit-' + vm.transactionFormConfiguration[row.type].suffix;
            $state.go(editUrl, { transactionId: row.id });
        }


        function removeTransaction(record) {

            var service = AccountingTransactionFactoryService.getService(record.type);

            var checkRemove = service.checkRemoveTransaction(record);

            if (checkRemove) {
                AlertService.askConfirm('AccountingTransaction.Remove.Message', record).then(function () {
                    service.removeTransaction($state.params.condominiumId, $state.params.periodId,
                        record.id).then(function () {
                            return NotifierService.notifySuccess('AccountingTransaction.Remove.Success');
                        }).catch(function (err) {
                            return NotifierService.notifyError('AccountingTransaction.Remove.Failure', err);
                        }).finally(function () {
                            vm.tableParams.reload();
                        })
                }
                );
            } else
                return NotifierService.notifyError('AccountingTransaction.Remove.ExistChild');
        }

        function loadAccountingTransactions(params) {
            $state.go('.', params.uiRouterUrl(), { inherit: false });
            return AccountingTransactionService.getPage($state.params.condominiumId, $state.params.periodId,
                params.page() - 1, params.count(), params.sorting(), params.filter()).then(function (page) {
                    params.total(page.totalElements);
                    vm.firstLoad = false;
                    return page.content;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    vm.loading = false;
                });
        }

        function openPrintRipartitionsOption() {
            AccountingTransactionService.open({
                condominiumId: vm.condominiumId,
                periodId: vm.periodId
            }, 'app/accounting-transaction/print-ripartition-options.html');
        }

        function openPrintFinalBalanceOption() {
            AccountingTransactionService.open({
                condominiumId: vm.condominiumId,
                periodId: vm.periodId
            }, 'app/accounting-transaction/print-final-balance-options.html');
        }

        function doPrintConfirm(printType, outputFormat) {
            var param = {};
            param.condominiumId = $state.params.condominiumId;
            param.financialPeriodId = $state.params.periodId;
            param.showSubRecord = vm.printOption.onlyAccount;
            param.showPreviousYearBalance = vm.printOption.showLastYear;
            param.showInstalment = vm.printOption.showInstalmentPlan
            param.printDetail=vm.printOption.printDetail
            param.outputFormat=outputFormat;

            AccountingTransactionService.getReport(printType, param);
        }



    }

})();

