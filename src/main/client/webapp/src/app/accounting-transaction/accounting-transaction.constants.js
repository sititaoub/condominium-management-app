/*
 * accounting-record.constants.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        /**
        * Start Path for selection.
        */
        .constant('referenceDocumentList', [
            'INVOICE', 'RECEIPT', 'PLOT'
        ]).
        constant('manualTransactionTypes', [
            'MAN_TX_GENERAL', 'MAN_TX_PERSONAL', 'MAN_TX_REVENUE'
        ]).
        constant('moneyTransferTransactionTypes', [
            'MONEY_TRANSFER_TX', 'INVESTMENT_FUND_TX', 'DISINVESTMENT_FUND_TX'
        ]).
        constant('transactionFormConfiguration',  {
        "MAN_TX_GENERAL" :        {suffix: "manual-tx",             toGivepath: 'COST_PATH',			toHavepath: 'SUPPLIER_PATH',  detail: "true",  modify: "true",  delete: "true", useOnlySimplified: false, showProforma: true,  withHolding: true, },
        "MAN_TX_PERSONAL":        {suffix: "manual-tx",             toGivepath: 'PERSONAL_COST_PATH',   toHavepath: 'SUPPLIER_PATH',  detail: "true",  modify: "true",  delete: "true", useOnlySimplified: false, showProforma: true,  withHolding: true},
        "MAN_TX_REVENUE" :        {suffix: "manual-tx",             toGivepath: 'REVENUE_PATH',         toHavepath: 'CUSTOMER_PATH',  detail: "true",  modify: "true",  delete: "true", useOnlySimplified: true,  showProforma: false,  withHolding: false},
        "INSTALMENT_TX"  :        {suffix: "instalment-tx" ,        toGivepath: 'REVENUE_PATH',         toHavepath: 'REVENUE_PATH',   detail: "true",  modify: "false", delete: "false"},
        "MONEY_TRANSFER_TX" :     {suffix: "money-transfer",        toGivepath: 'RESOURCE_PATH',        toHavepath: 'RESOURCE_PATH',  detail: "true",  modify: "true",  delete: "true"},
        "INVESTMENT_FUND_TX" :    {suffix: "money-transfer",        toGivepath: 'FUND_PATH',            toHavepath: 'PROVISION_PATH', detail: "true",  modify: "true",  delete: "true"},
        "DISINVESTMENT_FUND_TX" : {suffix: "money-transfer",        toGivepath: 'PROVISION_PATH',       toHavepath: 'FUND_PATH',      detail: "true",  modify: "true",  delete: "true"},
        "USER_DEFINED_TX"                   : {suffix: "user-defined",        toGivepath: 'ROOT_PATH',            toHavepath: 'ROOT_PATH',      detail: "false", modify: "true",  delete: "true"},
        //"ADVANCE_PAYMENT_TX"                : {suffix: "user-defined",        useTemplate:  true,                                               detail: "false", modify: "true",  delete: "true"},
        //"CLOSING_ADVANCE_PAYMENT_TX"        : {suffix: "user-defined",        useTemplate:  true,                                               detail: "false", modify: "true",  delete: "true"}
        } 
        


         ). 
        constant('recordFormConfiguration',
        {
            "PAYMENT":                {update: "true", delete: "true"},
            "WITHHOLDING":            {update: "true", delete: "false"},
            "INSTALMENT_PAYMENT":     {update: "true", delete: "true"},
        }).
        constant('withHoldingRates',
        [
            {'id': 0,  'label': 'INDIVIDUAL_FIRM', 'tax': 4},
            {'id': 1, 'label':  'COMPANY', 'tax': 4},
            {'id': 2, 'label':  'PROFESSIONALS', 'tax': 20},
            {'id': 3,  'label': 'COMMISSIONS', 'tax': 23},
            {'id': 4,  'label': 'SELF_EMPLOYED', 'tax': 30},
        ]).
        constant('ivaRates',
        [
            {'id': 'IVA_4',  'tax': 4, notSubject: false, exempt: false},
            {'id': 'IVA_10', 'tax': 10, notSubject: false, exempt: false},
            {'id': 'IVA_22', 'tax': 22, notSubject: false, exempt: false},
            {'id': 'EXEMPT', 'tax': 0, notSubject: false, exempt: true},
            {'id': 'NOT_SUBJECT', 'tax': 0, notSubject: true, exempt: false},
        ])
        .constant('deductibilityTypes',
        [   'A','B','C','D','E','F','G','H','I','L','M','N','O','P'
        ])
        .constant('paymentModeList', [
            'BANK_TRANSFER','RIBA','CASH'])
        .constant('instalmentpaymentModeList', [
                'BANK_TRANSFER','MAV','CASH']
        );    
        

})();

