/*
 * technician-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.condominiums')
        .component('instalmentPaymentCreate', {
            controller: InstalmentPaymentCreateController,
            bindings: {
                'close': '&',
                'dismiss': '&' ,
                'resolve' : '<',
            },
            templateUrl: 'app/accounting-transaction/instalment-transaction/instalment-payment-create.html'
        });

        InstalmentPaymentCreateController.$inject = [];

    /* @ngInject */
    function InstalmentPaymentCreateController() {
        var $ctrl = this;
        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.value = {};
             
            $ctrl.transaction = $ctrl.resolve.selectedTX;
            $ctrl.rootCreditPath = $ctrl.transaction.rootCreditPath;
            $ctrl.companyId   = $ctrl.resolve.companyId;
            $ctrl.financialPeriodId = $ctrl.resolve.financialPeriodId;
        }

        function doSave() {
            $ctrl.close({ 
                $value: $ctrl.value });
        }
    }

})();

