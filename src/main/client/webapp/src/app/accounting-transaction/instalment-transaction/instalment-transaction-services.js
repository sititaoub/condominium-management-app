/*
 * instalment-transaction.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .factory('InstalmentTransactionService',InstalmentTransactionService);

        InstalmentTransactionService.$inject = ['$rootScope', '$sessionStorage', 'Restangular', 'contextAddressManagement', '_','$uibModal','$q'];

    /* @ngInject */
    function InstalmentTransactionService($rootScope, $sessionStorage, Restangular, contextAddressManagement, _, $uibModal, $q) {
        var InstalmentTransactionService = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressManagement + 'v1');
        });
        
        
        var service = {
            getTransaction:         getTransaction,
            createRecord:           createInstalmentPayment,
            updateRecord:           updateInstalmentPayment,
            removeRecord:           removeInstalmentPayment,
            addInstalmentPayment:   addInstalmentPayment,
            checkRemoveTransaction: checkRemoveTransaction,
            checkRemoveRecord:      checkRemoveInstalmentPayment,
            getRecord:              getInstalmentPayment,
            checkNewRecordAction: checkNewRecordAction,
            openPopupNewRecord:  openPopupNewRecord,
            openPopupEditRecord: openPopupEditRecord
        };
        return service;

         ////////////////

        /**
         * MANAGE Transaction
         * 
         */


         /**
         * Instalment Plan Transaction
         *
         * @typedef {Object} InstalmentIssueTxDTO~InstalmentIssueTxDTO
         * @property {number} id - The unique id of transaction.
         * @property {Number[]} instalmentsIdList - The list of instlment is paid for the plane.
         * @property {string} description - The transaction description.
         * @property {Date} startDate - The compentence start date.
         * @property {Date} endDate - The compentence end date.
         * @property {number} totalAmount - The amount of transaction.
         * @property {string} type - The transaction type.
         * @property {string} notes - The transaction notes.
         * @property {string} status - The transaction status.
         * @property {number} financialPeriodIdList - List of financialPeriod Id,
         * @property {number} condominiumExternalId - The unique external condominiumId.
         *  
         * 
         */

         /**
         * Select the instalment transaction  supplied condominium id and financialPeriod id.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} financialPeriodId - The unique id of financialPeriodId.
         * @param {number} transactionId - The unique id of transaction.
         * @returns {InstalmentIssueTxDTO~InstalmentIssueTxDTO} - The current financial period.
         */
         function getTransaction(condominiumId, financialPeriodId, transactionId) {
            return InstalmentTransactionService.one("condominiums", condominiumId)
                .one("financial-period",financialPeriodId).one("instalment-transactions",transactionId)
                .get().then(function (transaction) {
                    transaction.startDate =  transaction.startDate ? new Date(transaction.startDate) : undefined;
                    transaction.endDate   =  transaction.endDate   ? new Date(transaction.endDate)   : undefined;
                    return transaction;
                });
        }


        
         /**
         * Return true if transaction can deleted
         *
         * @param  {InstalmentIssueTxDTO~InstalmentIssueTxDTO} InstalmentIssueTxDTO - Transaction to delete
         * @returns {boolean} - The result.
         */
        function checkRemoveTransaction(transaction) {
            return false; 
        }



        /**
         * MANAGE InstalmentPaymentDTO single payment for the instalment plan transaction 
         * 
         * @typedef {Object} InstalmentPaymentDTO~InstalmentPaymentDTO
         * @property {number} id - The unique id of transaction.
         * @property {string} protocol - The code protocol of payment.
         * @property {string} referenceDocument - The the associated reference document description.
         * @property {Date} date - The date of payment
         * @property {number} amount - The amount of payment.
         * @property {string} resourceMatPath - The matherialized path of resource.
         * @property {string} personMaterializedPath - The matherialized path of person.
         * @property {number} accountingWritingExternalId - The writing id associated with the payment,
         * @property {number[]} instalmentsIdList - The list containing the ids paid with the payment
         * 	 
         */

        
         /**
         * Add a payment for the instalment plan transaction whose identifier is passed with the parameter 
         * 
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} financialPeriodId - The unique id of financial period.  
         * @param {number} transactionId - The unique id of transaction. 
         * @param {InstalmentPaymentDTO} payment - The payment to add.   
         * @returns {number} id - The unique id of payment.
         */
        function createInstalmentPayment(condominiumId, financialPeriodId, transactionId, payment  ) {
            return InstalmentTransactionService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).one("instalment-transactions",transactionId)
            .post("payment",payment);
        }


        /**
         * Add a payment for the instalment plan transaction associated with the financial period
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} financialPeriodId - The unique id of financial period.  
         * @param {InstalmentPaymentDTO} payment - The payment to add.   
         * @returns {number} id - The unique id of payment.
         */
        function addInstalmentPayment(condominiumId, financialPeriodId, payment  ) {
            return InstalmentTransactionService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).one("instalment-transactions","")
            .post("add-instalment-payment",payment);
        }
         

        
        /**
         * Update a payment for the instalment plan transaction  
         * 
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} financialPeriodId - The unique id of financial period.  
         * @param {number} transactionId - The unique id of transaction. 
         * @param {InstalmentPaymentDTO} payment - The payment to update.   
         * @returns {number} id - The unique id of payment.
         */
        function updateInstalmentPayment(condominiumId, financialPeriodId, transactionId, payment ) {
             return InstalmentTransactionService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).one("instalment-transactions",transactionId)
            .one("payment",payment.id)
            .doPUT(payment);
        } 
        
       /**
         * Check if is possibile to delete payment
         * @returns {boolean} result of check
         */
        function checkRemoveInstalmentPayment(payment) {
            var deferred = $q.defer();
            return $q.resolve(true); 
        }

                
        /**
         * remove a payment for the instalment plan transaction  
         * 
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} financialPeriodId - The unique id of financial period.  
         * @param {number} transactionId - The unique id of transaction. 
         * @param {InstalmentPaymentDTO} paymentId - THe id of payment to delete   
         * @returns {number} id - The unique id of payment.
         */
        function removeInstalmentPayment(condominiumId, financialPeriodId, transactionId, paymentId) {
            return InstalmentTransactionService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).one("instalment-transactions",transactionId)
            .one("payment",paymentId)
            .remove();
        }


         /**
         * Retrieve a single instalment payment
         * 
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} financialPeriodId - The unique id of financial period.  
         * @param {number} transactionId - The unique id of transaction. 
         * @param {InstalmentPaymentDTO} paymentId - THe id of payment to retrieve   
         * @returns {number} id - The unique id of payment.
         */
        function getInstalmentPayment(condominiumId, financialPeriodId, transactionId, paymentId) {
            return InstalmentTransactionService.one("condominiums", condominiumId)
                .one("financial-period",financialPeriodId).one("instalment-transactions",transactionId)
                .one("payment", paymentId).get().then(function (payment) {
                    payment.date =  payment.date ? new Date(payment.date) : undefined;
                    return payment;
                });
        }

        function checkNewRecordAction(transaction){
            return true;
        }
            
        /**
         * Open popup edit record
         * 
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} companyId - The unique id of financial period.  
         * @param {number} financialPeriodId - The unique id of transaction. 
         * @param {InstalmentIssueTxDTO} transaction - transaction of payment   
         * @param {InstalmentPaymentDTO} record - select payment to edit  
         * @returns {number} id - The unique id of payment.
         */
        function openPopupEditRecord(condominiumId, companyId, financialPeriodId, transaction, record) {

            var deferred = $q.defer();
            return $uibModal.open({
                 component: 'instalmentPaymentEdit',
                 size: 'lg',
                 resolve: {
                     selectedTX: function () { return  transaction; },
                     selectedPym:   function () { return record; },
                     condominiumId: function () { return  condominiumId; },
                     companyId: function () { return  companyId; },
                     financialPeriodId: function () { return  financialPeriodId;}
                 },
                 windowClass: 'account-chooser-modal'
             }).result.then(function (newPayment) {
           return $q.resolve(newPayment);
             });
        }
        
        /**
         * Open popup new record
         * 
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} companyId - The unique id of financial period.  
         * @param {number} financialPeriodId - The unique id of transaction. 
         * @param {InstalmentIssueTxDTO} transaction - transaction of payment   
         */
        function openPopupNewRecord(condominiumId, companyId, financialPeriodId, transaction) {
          var deferred = $q.defer();
           return $uibModal.open({
                component: 'instalmentPaymentCreate',
                size: 'lg',
                resolve: {
                    selectedTX: function () { return  transaction; },
                    condominiumId: function () { return  condominiumId; },
                    companyId: function () { return  companyId; },
                    financialPeriodId: function () { return  financialPeriodId;}
                },
                windowClass: 'account-chooser-modal'
            }).result.then(function (newPayment) {
                return $q.resolve(newPayment);
            });
        }


    }

})();

