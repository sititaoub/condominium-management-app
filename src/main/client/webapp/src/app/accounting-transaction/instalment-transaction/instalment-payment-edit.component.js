/*
 * technician-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.condominiums')
        .component('instalmentPaymentEdit', {
            controller: InstalmentPaymentEditController,
            bindings: {
                'close': '&',
                'dismiss': '&' ,
                'resolve' : '<',
            },
            templateUrl: 'app/accounting-transaction/instalment-transaction/instalment-payment-edit.html'
        });

        InstalmentPaymentEditController.$inject = ['InstalmentTransactionService','NotifierService'];

    /* @ngInject */
    function InstalmentPaymentEditController(InstalmentTransactionService,NotifierService) {
        var $ctrl = this;
        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.transaction = $ctrl.resolve.selectedTX;
            $ctrl.rootCreditPath = $ctrl.transaction.rootCreditPath;
            $ctrl.companyId   = $ctrl.resolve.companyId;
            $ctrl.financialPeriodId = $ctrl.resolve.financialPeriodId;
            $ctrl.paymentId =  $ctrl.resolve.selectedPym.id; 
            $ctrl.condominiumId   = $ctrl.resolve.condominiumId;
            loadItem($ctrl.condominiumId, $ctrl.financialPeriodId,  $ctrl.transaction.id, $ctrl.paymentId);
        }


        function loadItem(condominiumId, financialPeriodId, transactionId, paymentId){
            
            InstalmentTransactionService.getRecord(condominiumId, financialPeriodId, transactionId, 
                paymentId).then(function (payment) {
                $ctrl.value = angular.copy(payment);
                $ctrl.loading = false;
            }).catch(function () {
                return NotifierService.notifyError('InstalmentPayment.LoadDetail.Failure');
                $ctrl.loading = true;
            })
    
            }


        function doSave() {
            $ctrl.close({ 
                $value: $ctrl.value });
        }
    }

})();

