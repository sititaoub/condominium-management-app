/*
 * technician-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.condominiums')
        .component('instalmentPaymentInput', {
            controller: InstalmentPaymentInputController,
            bindings: {
                ngModel: '<',
                companyId: '<',
                rootCreditPath: '<',
                financialperiodId: '<',
                personMatPath: '@?',         
                amount: '<?'        
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/accounting-transaction/instalment-transaction/instalment-payment-input.html'
        });

        InstalmentPaymentInputController.$inject = ['InstalmentTransactionService', 'FinancialPeriodService','AccountingTransactionService','MaterializedPathLookupService','NotifierService','instalmentpaymentModeList'];

    /* @ngInject */
    function InstalmentPaymentInputController(InstalmentTransactionService, FinancialPeriodService, AccountingTransactionService, MaterializedPathLookupService,NotifierService, instalmentpaymentModeList) {
        var $ctrl = this;
        $ctrl.loading = true;
        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.value = new Object();
        $ctrl.instalmentpaymentModeList = instalmentpaymentModeList;
        ////////////


        function onInit() {

            $ctrl.ngModelCtrl.$render = function () {
                
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
                
                if (angular.isDefined($ctrl.personMatPath))
                {
                    $ctrl.value.personMaterializedPath = $ctrl.personMatPath;
                    $ctrl.disableUpdateRootCredit = true;
                }
               
                if (angular.isDefined($ctrl.amount))
                {
                    $ctrl.disableUpdateAmount = true;
                    $ctrl.value.amount = $ctrl.amount;
                }                
            };
            
            
            $ctrl.paymentDateOpened = false;
 

            $ctrl.prefixedPersonPath =   $ctrl.rootCreditPath; 


            loadFinancialPeriod();
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function loadFinancialPeriod() {
            FinancialPeriodService.getOne($ctrl.companyId, $ctrl.financialperiodId).then(function (period) {
                $ctrl.period = period;

                MaterializedPathLookupService.getOne($ctrl.companyId, $ctrl.period.chartOfAccountsId, "RESOURCE_PATH").then(function (mathPath) {
                    $ctrl.prefixedAccountPath = mathPath.materializedPath;
                    $ctrl.loading = false;
                });

            }).catch(function () {
                NotifierService.notifyError();
            })
        }
    }

 







})();

