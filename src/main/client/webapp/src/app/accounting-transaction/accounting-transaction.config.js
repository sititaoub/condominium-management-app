/*
 * accounting-record.config.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .config(setupI18n);

    setupI18n.$inject = ['$translatePartialLoaderProvider'];

    /* @ngInject */
    function setupI18n($translatePartialLoaderProvider) {
        $translatePartialLoaderProvider.addPart('accounting-transaction');
    }
})();
