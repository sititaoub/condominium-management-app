/*
 * accounting-transaction.route.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider' ];

    /* @ngInject */
    function setupRoutes ($stateProvider ) {
        $stateProvider.state('index.accounting-transaction', {
            url: '/condominiums/{condominiumId}/financial-period/{periodId}/accounting-transaction',
            abstract: true,
            template: '<ui-view></ui-view>',
            redirectTo: 'index.accounting-transaction.list',
            data: {
                skipFinancialPeriodCheck: false
            },
            onEnter: ['$state', '$transition$', 'FinancialPeriodService', function ($state, $transition$, FinancialPeriodService) {
                // Ensure that if 'current' is provided as financial period, the current financial period is provided.
                if ($transition$.params().periodId === 'current') {
                    var params = angular.copy($transition$.params());
                    params.periodId = FinancialPeriodService.getCurrent($transition$.params().companyId, $transition$.params().condominiumId).id;
                    return $state.target($transition$.targetState().name(), params);
                }
                return true;
            }]    
         }).state('index.accounting-transaction.list', {
            url: "?page&count&sort&writingDescription&type&startDate&endDate&startAmount&endAmount",
            templateUrl: "app/accounting-transaction/accounting-transaction.html",
            controller: "AccountingTransactionController",
            params: {
                page: { dynamic: true,  squash: true, value: '1', inherits: true },
                count: { dynamic: true, squash: true, value: '10', inherits: true },
                writingDescription: { dynamic: true, inherits: true },
                type:        { dynamic: true, inherits: true },
                startDate:   { dynamic: true, inherits: true },
                endDate:     { dynamic: true, inherits: true },
                startAmount: { dynamic: true, inherits: true },
                endAmount:   { dynamic: true, inherits: true }

            },
            controllerAs: 'vm'
        }).state('index.accounting-transaction.list.add-manual-tx', {
            url: '/new-manual-tx?transactionType&invoiceId',
            views: {
                '@index.accounting-transaction': {
                    templateUrl: "app/accounting-transaction/manual-transaction/manual-transaction-create.html",
                    controller: "ManualTxCreateController",
                    controllerAs: 'vm'
                }
            },
            resolve: {
                period: ['$transition$', 'FinancialPeriodService', function ($transition$, FinancialPeriodService) {
                    return  FinancialPeriodService.getOne($transition$.params().companyId, $transition$.params().periodId);
                }]
            }   
        }).state('index.accounting-transaction.list.edit-manual-tx', {
            url: '/edit-manual-tx/{transactionId}',
            views: {
                '@index.accounting-transaction': {
                    templateUrl: "app/accounting-transaction/manual-transaction/manual-transaction-edit.html",
                    controller: "ManualTxEditController",
                    controllerAs: 'vm'
                }
            },
            resolve: {
                period: ['$transition$', 'FinancialPeriodService', function ($transition$, FinancialPeriodService) {
                    return  FinancialPeriodService.getOne($transition$.params().companyId, $transition$.params().periodId);
                }]
            }          
        }).state('index.accounting-transaction.list.add-money-transfer', {
            url: '/new-money-tx?transactionType&invoiceId',
            views: {
                '@index.accounting-transaction': {
                    templateUrl: "app/accounting-transaction/money-transfer/money-transfer-tx-create.html",
                    controller: "MoneyTransferTxCreateController",
                    controllerAs: 'vm'  
                }
            },
            resolve: {
                period: ['$transition$', 'FinancialPeriodService', function ($transition$, FinancialPeriodService) {
                    return  FinancialPeriodService.getOne($transition$.params().companyId, $transition$.params().periodId);
                }]
            }    
        })
        .state('index.accounting-transaction.list.edit-money-transfer', {
            url: '/edit-money-tx/{transactionId}',
            views: {
                '@index.accounting-transaction': {
                    templateUrl: "app/accounting-transaction/money-transfer/money-transfer-tx-edit.html",
                    controller: "MoneyTransferTxEditController",
                    controllerAs: 'vm'
                }
            },
            resolve: {
                period: ['$transition$', 'FinancialPeriodService', function ($transition$, FinancialPeriodService) {
                    return  FinancialPeriodService.getOne($transition$.params().companyId, $transition$.params().periodId);
                }]
            }         
        })
        .state('index.accounting-transaction.list.add-user-defined', {
            url: '/new-user-defined-tx?template&transactionType',  
            views: {
                '@index.accounting-transaction': {
                    templateUrl: "app/accounting-transaction/user-defined/user-defined-tx-create.html",
                    controller: "UserDefinedTxCreateController",
                    controllerAs: 'vm'
                }
            },
            resolve: {
                period: ['$transition$', 'FinancialPeriodService', function ($transition$, FinancialPeriodService) {
                    return  FinancialPeriodService.getOne($transition$.params().companyId, $transition$.params().periodId);
                }]
            }   
        })
        .state('index.accounting-transaction.list.edit-user-defined', {
            url: '/edit-user-defined-tx/{transactionId}',
            views: {
                '@index.accounting-transaction': {
                    templateUrl: "app/accounting-transaction/user-defined/user-defined-tx-edit.html",
                    controller: "UserDefinedTxEditController",
                    controllerAs: 'vm'
                }
            },
            resolve: {
                period: ['$transition$', 'FinancialPeriodService', function ($transition$, FinancialPeriodService) {
                    return  FinancialPeriodService.getOne($transition$.params().companyId, $transition$.params().periodId);
                }]
            }   
        })
        .state('index.accounting-transaction.list.detailWriting', {
            url: '/{transactionId}/accounting-transaction-detail-writing?transactionType',
            views: {
                '@index.accounting-transaction': {
                    templateUrl: "app/accounting-transaction/accounting-transaction-writing-detail.html",
                    controller: "AccountingTransactionDetailWritingController",
                    controllerAs: 'vm'
                }
            },
            resolve: {
                writingList: ['$transition$', 'AccountingTransactionService', function ($transition$, AccountingTransactionService) {
                    return  AccountingTransactionService.getWritings($transition$.params().condominiumId, $transition$.params().periodId, $transition$.params().transactionId);
                }]
            }                   
        })
            .state('index.accounting-transaction.list.detail', {
            url: '/{transactionId}/accounting-transaction-detail?transactionType',
            views: {
                '@index.accounting-transaction': {
                    templateUrl: "app/accounting-transaction/accounting-transaction-detail.html",
                    controller: "AccountingTransactionDetailController",
                    controllerAs: 'vm'
                }
            },
        });
 
    }

})();
