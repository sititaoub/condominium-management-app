/*
 * accounting-transaction.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .controller('AccountingTransactionDetailWritingController', AccountingTransactionDetailWritingController); 
        
        AccountingTransactionDetailWritingController.$inject = ['$state', '$uibModal','AccountingTransactionFactoryService','NgTableParams', 'AlertService', 'NotifierService',
        'FinancialPeriodService','recordFormConfiguration', 'writingList'];

   /* @ngInject */
   function AccountingTransactionDetailWritingController($state, $uibModal, AccountingTransactionFactoryService, NgTableParams, AlertService, NotifierService, 
    FinancialPeriodService,recordFormConfiguration, writingList) {
    var vm = this;

    activate($state.params.transactionType);

    function activate(type) {
        vm.loading = true;
        vm.firstLoad = true;
        vm.recordFormConfiguration = recordFormConfiguration;
        vm.service = AccountingTransactionFactoryService.getService(type);
        vm.accordian = {isOpen : false};
        vm.writingList = writingList; 
        vm.firstLoad = false;
        vm.periodId =  $state.params.periodId;
        loadTransaction(type);
    }

    function loadTransaction(type) {
        vm.service = AccountingTransactionFactoryService.getService(type);
         return vm.service.getTransaction( $state.params.condominiumId, 
                    $state.params.periodId,$state.params.transactionId).then(function (transaction) {
                    vm.tx = transaction;
                    vm.loading = false;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    vm.loading = false;
         });
    }




}

})();

