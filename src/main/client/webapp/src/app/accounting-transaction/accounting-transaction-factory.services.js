/*
 * accounting-transaction.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .factory('AccountingTransactionFactoryService', AccountingTransactionFactoryService);

        AccountingTransactionFactoryService.$inject = ['ManualTransactionService', 'InstalmentTransactionService', 'MoneyTransferTxService','UserDefinedTxService'];

    /* @ngInject */
    function AccountingTransactionFactoryService(ManualTransactionService, InstalmentTransactionService, MoneyTransferTxService,UserDefinedTxService) {
        
        
        var service = {
            getService: getService,
        };
        return service;

 
        function getService(type) {
            var implementation = ManualTransactionService;
            if (type == 'MAN_TX_GENERAL' || type == 'MAN_TX_PERSONAL' || type == 'MAN_TX_REVENUE')
                implementation = ManualTransactionService;
            else if (type == 'INSTALMENT_TX')
                implementation = InstalmentTransactionService;
            else if (type == 'MONEY_TRANSFER_TX' || type == 'INVESTMENT_FUND_TX' || type == 'DISINVESTMENT_FUND_TX')
                implementation = MoneyTransferTxService;
            else if (type == 'USER_DEFINED_TX')
                implementation = UserDefinedTxService;
        
            return implementation; 
        }
  
    
    }

})();

