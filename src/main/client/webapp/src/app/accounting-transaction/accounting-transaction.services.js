/*
 * accounting-transaction.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .factory('AccountingTransactionService', AccountingTransactionService);

    AccountingTransactionService.$inject = ['$rootScope', '$sessionStorage', 'Restangular', 'contextAddressManagement', '_', 'transactionFormConfiguration',
        'recordFormConfiguration', '$uibModal','$window'];

    /* @ngInject */
    function AccountingTransactionService($rootScope, $sessionStorage, Restangular, contextAddressManagement, _,
        transactionFormConfiguration, recordFormConfiguration, $uibModal,$window) {
        var AccountingTransactions = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressManagement + 'v1');

        });


        var service = {
            getPage: getPage,
            getRecords: getRecords,
            getWritings: getWritings,
            getTransactionFormConfiguration: getTransactionFormConfiguration,
            open:open,
            getReport:getReport
        };
        return service;



        /**
         * Manage generic transaction
         */
        function getPage(condominiumId, financialPeriodId, page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return AccountingTransactions.one("condominiums", condominiumId)
                .one("financial-period", financialPeriodId).all("accounting-transactions").get("", params);
        }


        function getRecords(condominiumId, financialPeriodId, transactionId, page, size, sorting, filters) {
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            })
            return AccountingTransactions.one("condominiums", condominiumId)
                .one("financial-period", financialPeriodId).one("accounting-transactions", transactionId)
                .all("records").get("", params);
        }

        function getWritings(condominiumId, financialPeriodId, transactionId) {
            return AccountingTransactions.one("condominiums", condominiumId)
                .one("financial-period", financialPeriodId).one("accounting-transactions", transactionId)
                .all("writings").get("");
        }



        function getTransactionFormConfiguration(key) {
            return  transactionFormConfiguration[key];
        }


        /**
         * Open ui modal
         * @param {Object} options 
         * @param {String} templateUrl 
         */
        function open(options, templateUrl) {
            return $uibModal.open({
                templateUrl: templateUrl,
                windowClass: 'inmodal centre-cost-modal-container',
                bindToController: true,
                controller: 'AccountingTransactionController',
                controllerAs: 'vm',
                resolve: {
                    condominiumId: function () { return options.condominiumId; },
                    periodId: function () { return options.periodId; }
                }
            });
        }

        /**
         *  call end-point for download report
         * @param {Object} options - parameter for print Report
         */
        function getReport(printType, options) {
            var url = AccountingTransactions.all("final-balance").all("reports").all(printType).getRequestedUrl();
            url = url + "?condominiumId=" + options.condominiumId + "&financialPeriodId=" + options.financialPeriodId + "&format=" + options.outputFormat;
            if (options.showSubRecord)
                url = url + "&showSubRecord=" + options.showSubRecord;
            if (options.showPreviousYearBalance)
                url = url + "&showPreviousYearBalance=" + options.showPreviousYearBalance;
            if (options.showInstalment)
                url = url + "&showInstalment=" + options.showInstalment;
            if(options.printDetail)
            url = url + "&printDetail=" + options.printDetail;
            $window.open(url, "_blank");
        }

    }

})();

