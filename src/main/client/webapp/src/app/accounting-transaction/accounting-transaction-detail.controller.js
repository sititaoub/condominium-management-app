/*
 * accounting-transaction.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .controller('AccountingTransactionDetailController', AccountingTransactionDetailController); 
        
        AccountingTransactionDetailController.$inject = ['$state', '$uibModal','AccountingTransactionFactoryService','NgTableParams', 'AlertService', 'NotifierService',
        'FinancialPeriodService','recordFormConfiguration', 'AccountingTransactionService'];

   /* @ngInject */
   function AccountingTransactionDetailController($state, $uibModal, AccountingTransactionFactoryService, NgTableParams, AlertService, NotifierService, 
    FinancialPeriodService,recordFormConfiguration, AccountingTransactionService) {
    var vm = this;

    activate($state.params.transactionType);

    function activate(type) {
        vm.loading = true;
        vm.firstLoad = true;
        vm.removeRecord = removeRecord;
        vm.checkNewRecordAction = checkNewRecordAction;
        vm.newRecord = newRecord;
        vm.editRecord = editRecord;
        vm.type = type;
        vm.recordFormConfiguration = recordFormConfiguration;
        vm.service = AccountingTransactionFactoryService.getService(type);
        
        vm.tableParams = NgTableParams.fromUiStateParams($state.params, {
            page: 1,
            count: 10
        }, {
            getData: loadDetail
        });
        loadTransaction();
    }

    function loadDetail(params) {
   
        if (!vm.firstLoad) {
            $state.go('.', params.uiRouterUrl(), {notify: false, inherit: false});
        }
     
         return AccountingTransactionService.getRecords($state.params.condominiumId, $state.params.periodId,
            $state.params.transactionId,
            params.page() - 1, params.count(), params.sorting(), params.filter()).then(function (page) {
            params.total(page.totalElements);
            vm.firstLoad = false;
            return page.content;
            }).catch(function () {
            return NotifierService.notifyError();
            }).finally(function () {
            vm.loading = false;
            });
    }

    function loadTransaction() {
         
         return vm.service.getTransaction( $state.params.condominiumId, 
                    $state.params.periodId,$state.params.transactionId).then(function (transaction) {
                    vm.tx = transaction;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    vm.loading = false;
         });
    }




    function checkNewRecordAction(transaction){
        return vm.service.checkNewRecordAction(transaction);
    }
 
    function newRecord() {
        
        vm.service.openPopupNewRecord($state.params.condominiumId, $state.params.companyId, $state.params.periodId, vm.tx).
        then(function (result) {
            vm.service.createRecord($state.params.condominiumId, $state.params.periodId,
                $state.params.transactionId, result).then(function () {
                   return NotifierService.notifySuccess('ManualTransaction.Create.Success').then(function () {
                    activate(vm.type);
                   });
               }).catch(function (err) {
                   return NotifierService.notifyError('ManualTransaction.Create.Failure',err);
               });

               
           
        });
    }

    function editRecord(record) {

        

        vm.service.openPopupEditRecord($state.params.condominiumId, $state.params.companyId, $state.params.periodId, vm.tx, record).
        then(function (result) {
  
            vm.service.updateRecord($state.params.condominiumId,$state.params.periodId, $state.params.transactionId, 
                result).then(function () {
                return NotifierService.notifySuccess('ManualTransaction.Update.Success').then(function () {
                    activate(vm.type);
                });
            }).catch(function (err) {
                return NotifierService.notifyError('ManualTransaction.Update.Failure',err);
            });
           
        });
    }

    function removeRecord(record) {
       
        var checkRemove = vm.service.checkRemoveRecord($state.params.condominiumId,$state.params.periodId, $state.params.transactionId, record.id).then(function () {
           
                AlertService.askConfirm('ManualTransaction.Remove.Message', record).then(function () {
                    vm.service.removeRecord($state.params.condominiumId, $state.params.periodId,
                        $state.params.transactionId, record.id).then(function () {
                        return NotifierService.notifySuccess('ManualTransaction.Remove.Success');
                    }).catch(function (err) {
                        return NotifierService.notifyError('ManualTransaction.Remove.Failure', err);
                    }).finally(function () {
                        vm.tableParams.reload();
                    })
        
        
                    
                });
            }).catch(function (err) {
                return NotifierService.notifyError('ManualTransaction.Remove.ChildPaid');
            });    
 
    }
    

}

})();

