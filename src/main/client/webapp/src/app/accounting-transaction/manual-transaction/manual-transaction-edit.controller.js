/*
 * financial-period-create.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .controller('ManualTxEditController', ManualTxEditController);

    ManualTxEditController.$inject = ['$state', 'managementTypes', 'ManualTransactionService', 'NotifierService', 'AlertService', 'period', 'MaterializedPathLookupService',
        'AccountingTransactionService', 'manualTransactionTypes', 'referenceDocumentList', 'withHoldingRates', 'deductibilityTypes', 'paymentModeList', '$q'];

    /* @ngInject */
    function ManualTxEditController($state, managementTypes, ManualTransactionService, NotifierService, AlertService, period, MaterializedPathLookupService, AccountingTransactionService,
        manualTransactionTypes, referenceDocumentList, withHoldingRates, deductibilityTypes, paymentModeList, $q) {
        var vm = this;

        vm.doSave = doSave;
        vm.withHoldingRates = withHoldingRates;
        vm.deductibilityTypes = deductibilityTypes;
        vm.paymentModeList = paymentModeList;
        activate();

        ////////////////

        function activate() {
            vm.companyId = $state.params.companyId;
            vm.periodId = $state.params.periodId;
            vm.periodLoaded = false;
            vm.loading = true;
            //CAlandar flag
            vm.setProforma = setProforma;
            vm.updateInvoice = updateInvoice;
            vm.onChangeType = onChangeType;
            vm.onChangeWithholdingEnabled = onChangeWithholdingEnabled;
            vm.paymentDateOpened = false;
            vm.proformaDateOpened = false;
            vm.openPopupProforma = openPopupProforma;

            vm.isUpdate = true;

            //costant
            vm.manualTransactionTypes = manualTransactionTypes;
            vm.referenceDocumentList = referenceDocumentList;
            vm.period = period;
            vm.periodLoaded = true;
            loadCost();

        }

        function loadCost() {
            ManualTransactionService.getTransaction($state.params.condominiumId, $state.params.periodId,
                $state.params.transactionId).then(function (transaction) {
                    vm.firstLoad = false;
                    vm.loadedAmount = transaction.totalAmount;
                    vm.tx = transaction;

                    var counterAmount = _.find(transaction.counter.amountCounter, function (c) { return c.type == 'PAYMENT' });
                    vm.amountPaid = counterAmount.amountPaid;
                    vm.withHoldingAmountPaid = 0;

                    if (transaction.withHolding) {
                        var counterWh = _.find(transaction.counter.amountCounter, function (c) { return c.type == 'WITHHOLDING' });
                        vm.withHoldingAmountPaid = counterWh.amountPaid;
                    }


                    var toGivepath = AccountingTransactionService.getTransactionFormConfiguration(vm.tx.type).toGivepath;
                    var firstChain = MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, toGivepath).then(function (mathPath) {
                        vm.economicRootPath = mathPath.materializedPath;
                    });

                    var toHavepath  = AccountingTransactionService.getTransactionFormConfiguration(vm.tx.type).toHavepath;
                    var secondChain = MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, toHavepath).then(function (mathPath) {
                        vm.patrimonialRootPath = mathPath.materializedPath;
                    });

                    var thirdChain =  MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, "RESOURCE_PATH").then(function (mathPath) {
                        vm.prefixedResourceRootPath = mathPath.materializedPath;
                    });

                    var fourthChain = MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, "PROFORMA_PATH").then(function (mathPath) {
                        vm.proformaRootPath = mathPath.materializedPath;
                    });

                    $q.all([firstChain, secondChain, thirdChain, fourthChain]).then(function() {

                        vm.showWithHolding = AccountingTransactionService.getTransactionFormConfiguration(vm.tx.type).withHolding;
                        vm.showProforma = AccountingTransactionService.getTransactionFormConfiguration(vm.tx.type).showProforma;
                        vm.useOnlySimplified = AccountingTransactionService.getTransactionFormConfiguration(vm.tx.type).useOnlySimplified;

                        vm.updateDisabled = (transaction.financialPeriodWritingId != $state.params.periodId);
                        vm.tx.accreditationNote ? vm.invoiceType = 'NOTE' : vm.invoiceType = 'INVOICE';
                        if (!transaction.proformaInvoice) {
                            vm.supplierMatPath = vm.tx.patrimonialWritingDetail.materializedPath;
                        } else {
                            vm.supplierMatPath = vm.tx.proformaSupplierMatPath;
                        }
                        vm.loading = false;

                    });
                });
        }


        function doSave() {
            vm.tx.condominiumExternalId = $state.params.condominiumId;
            vm.tx.financialPeriodExternalId = $state.params.periodId;

            if (checkDate()){

            var checkChangedAmount =  vm.loadedAmount  != vm.tx.totalAmount;    
            if (!vm.tx.proformaInvoice && (checkChangedAmount || vm.form.expiryDate.$dirty)) {
                AlertService.askConfirm('AccountingTransaction.Update.Warning').then(function () {
                    return internalSave();
                }
                );
            }
            else
                return internalSave();
            }
            else{
                NotifierService.notifyError('ManualTransaction.Update.DateRange');
                return $q.reject();
            }
        }

        function checkDate() {
            return vm.tx.endDate >= vm.tx.startDate
        }


        function internalSave() {
            if (!vm.tx.proformaInvoice) {
                vm.tx.patrimonialWritingDetail.materializedPath = vm.supplierMatPath;
            } else {
                vm.tx.patrimonialWritingDetail.materializedPath = vm.proformaRootPath;
                vm.tx.proformaSupplierMatPath = vm.supplierMatPath;
            }

            return ManualTransactionService.updateTransaction($state.params.condominiumId,
                $state.params.periodId, vm.tx).then(function () {
                    return NotifierService.notifySuccess('AccountingTransaction.Update.Success').then(function () {
                        return $state.go('^', { inherits: true });
                    });
                }).catch(function (err) {
                    NotifierService.notifyError('AccountingTransaction.Update.Failure', err);
                    return $q.reject(err);
                });
        }


        function setProforma(transaction) {
            if (transaction.proformaInvoice) {
                vm.tx.invoiceDate = "";
                vm.tx.invoiceExpireDate = "";
                vm.tx.referenceDocument = "";
            }
            else {
                vm.tx.proformaDate = "";
                vm.tx.proformaReferenceDocument = "";
                vm.proformaRootPath = "";
            }

        }

        function updateInvoice(transaction, value) {
            transaction.proformaAccounted = value;
            if (!value) {
                transaction.invoiceDate = "";
                transaction.invoiceExpireDate = "";
                transaction.referenceDocument = "";
            }

        }

        function onChangeType() {
            vm.tx.accreditationNote = (vm.invoiceType == 'NOTE');

            if (vm.tx.accreditationNote) {
                vm.tx.proformaInvoice = false;
                vm.tx.withHolding = false;
            }
        }

        function onChangeWithholdingEnabled(){
            if (vm.tx.withHolding)
                vm.tx.simplified = false;
        }


        //updateProforma(condominiumId, financialPeriodId, cost) 
        function openPopupProforma(){
            ManualTransactionService.openPopupProforma($state.params.condominiumId, $state.params.companyId,
                vm.period, vm.tx).then(function (result) {
                    ManualTransactionService.updateProforma($state.params.condominiumId, $state.params.periodId, result).then(function () {
                           return NotifierService.notifySuccess('ManualTransaction.Create.Success').then(function () {
                            activate(vm.type);
                           });
                       }).catch(function (err) {
                           return NotifierService.notifyError('ManualTransaction.Create.Failure',err);
                       });
        
                       
                   
                });


                
        }


        

    }

})();

