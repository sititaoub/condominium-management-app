/*
 * financial-period-create.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .controller('ProformaUpdateController', ProformaUpdateController);

    ProformaUpdateController.$inject = ['$uibModalInstance', '$state', '$translate', 'ManualTransactionService', 'MaterializedPathLookupService', 'NotifierService', 'AccountingTransactionService',
        'transaction', 'period'];

    /* @ngInject */
    function ProformaUpdateController($uibModalInstance, $state, $translate, ManualTransactionService, MaterializedPathLookupService, NotifierService, AccountingTransactionService,
        transaction, period) {
        var vm = this;
        vm.doSave = doSave;
        vm.updateProforma = updateProforma;

        vm.tx = angular.copy(transaction);
        vm.period = period;


        function init() {
            vm.loading = false;
        }

        function updateProforma(tx) {
            vm.loading = false;
            $uibModalInstance.close(vm.tx);
        }

        function doSave() {
            vm.loading = false;
            $uibModalInstance.close(vm.tx);
        }

        function updateProforma(transaction) {
            if (!transaction.proformaAccounted) {
                transaction.invoiceDate = "";
                transaction.invoiceExpireDate = "";
                transaction.referenceDocument ="";
            }

        }


    }
})();

