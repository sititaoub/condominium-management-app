/*
 * financial-period-create.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .controller('PaymentCreateController', PaymentCreateController);

        PaymentCreateController.$inject = ['$uibModalInstance', '$state', '$translate','ManualTransactionService', 'MaterializedPathLookupService', 'NotifierService','FinancialPeriodService','AccountingTransactionService','selectedTX','paymentModeList'];

    /* @ngInject */
    function PaymentCreateController($uibModalInstance, $state, $translate, ManualTransactionService, MaterializedPathLookupService, NotifierService, FinancialPeriodService,AccountingTransactionService, selectedTX,paymentModeList) {
        var vm = this;
        vm.doSave = doSave;
        
        activate();
      
        function activate() {
            loadFinancialPeriod();
        }    


        function init() {
            vm.loading = false;
            vm.paymentDateOpened = false;
    
            MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, "RESOURCE_PATH").then(function (mathPath) {
                vm.prefixedAccountPath = mathPath.materializedPath;
            });

            vm.showWithHolding  = selectedTX.withHolding;
            
            if (selectedTX.proformaInvoice)
                vm.patrimonialPath = selectedTX.proformaSupplierMatPath;
            else 
                vm.patrimonialPath = selectedTX.patrimonialWritingDetail.materializedPath;
            
            vm.isUpdate = false;
            vm.paymentModeList = paymentModeList;
            vm.dto = new Object();
            vm.dto.paymentData = new Object();
            vm.dto.paymentData.record =  new Object();
            vm.dto.paymentData.record.date = new Date();
            
            var documentDescription = selectedTX.proformaInvoice ? selectedTX.proformaReferenceDocument 
                    : selectedTX.referenceDocument != null ? selectedTX.referenceDocument : "";

            $translate('DESCRIZIONE_PAGAMENTO',{ 
                documentNumber: documentDescription
            } ).then(function (text) {
                vm.dto.paymentData.record.description = text;
            })
            
            vm.dto.paymentData.record.referenceDocument = documentDescription;

            if (vm.showWithHolding)
            {
                vm.dto.withholdingData =  new Object();
                vm.dto.withholdingData.record = new Object();
                $translate('DESCRIZIONE_RITENUTA', { 
                    documentNumber: documentDescription
                } ).then(function (text) {
                    vm.dto.withholdingData.record.description = text;
                })
            }

            calcualateAmount();
           
        }

        function calcualateAmount() {
      
            var counterAmount =  _.find(selectedTX.counter.amountCounter, function(c) { return c.type == 'PAYMENT' });
            vm.residualAmount = counterAmount.amount - counterAmount.amountPaid;

            if (vm.residualAmount>0) 
               vm.dto.paymentData.record.amount = vm.residualAmount;
            
            if (selectedTX.withHolding){      
                var counterWithHoldingAmount =  _.find(selectedTX.counter.amountCounter, function(c) { return c.type == 'WITHHOLDING' });
                vm.residualWithHoldingAmount = counterWithHoldingAmount.amount - counterWithHoldingAmount.amountPaid;
            
                if (vm.residualWithHoldingAmount<0) 
                       vm.residualWithHoldingAmount = 0;
            }
        }


        function loadFinancialPeriod() {
            FinancialPeriodService.getOne($state.params.companyId, $state.params.periodId).then(function (period) {
                vm.period = period;
                init();
            }).catch(function () {
                NotifierService.notifyError();
            })
        }

        function doSave() {
          vm.dto.paymentData.patrimonialWritingDetail= new Object();
          vm.dto.paymentData.patrimonialWritingDetail.materializedPath = vm.patrimonialPath;
          $uibModalInstance.close(vm.dto);
        }

        
 
    }

})();

