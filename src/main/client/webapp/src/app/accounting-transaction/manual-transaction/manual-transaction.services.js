/*
 * accounting-transaction.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .factory('ManualTransactionService',ManualTransactionService);

        ManualTransactionService.$inject = ['$rootScope', '$sessionStorage', 'Restangular', 'contextAddressManagement', '_','$uibModal','$q','withHoldingRates', 'ivaRates'];

    /* @ngInject */
    function ManualTransactionService($rootScope, $sessionStorage, Restangular, contextAddressManagement, _, $uibModal, $q,withHoldingRates, ivaRates) {
        var ManualTransactionService = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressManagement + 'v1');
        });
        
        
        var service = {
            getTransaction:      getTransaction,
            createTransaction:   createTransaction,
            updateTransaction:   updateTransaction,
            removeTransaction:   removeTransaction,
            updateProforma:      updateProforma,
            checkRemoveTransaction: checkRemoveTransaction,
            createRecord:        createPayment,
            updateRecord:        updatePayment,
            removeRecord:        removePayment,
            checkRemoveRecord:   checkRemovePayment,
            getRecord:           getPayment,
            checkNewRecordAction: checkNewRecordAction,
            openPopupNewRecord:  openPopupNewRecord,
            openPopupEditRecord: openPopupEditRecord,
            openPopupProforma: openPopupProforma,
            fillTransaction:      fillTransaction,
        };
        return service;


        /**
         * MANAGE Cost
         * 
         */

         function getTransaction(condominiumId, financialPeriodId, transactionId) {
            return ManualTransactionService.one("condominiums", condominiumId)
                .one("financial-period",financialPeriodId).one("manual-transactions",transactionId)
                .get().then(function (cost) {
                    cost.startDate =  cost.startDate ? new Date(cost.startDate) : undefined;
                    cost.endDate   =  cost.endDate   ? new Date(cost.endDate)   : undefined;
                    cost.expiryDate   =  cost.expiryDate   ? new Date(cost.expiryDate)   : undefined;
                    cost.invoiceDate  =  cost.invoiceDate    ? new Date(cost.invoiceDate)   : undefined;
                    cost.proformaDate =  cost.proformaDate  ? new Date(cost.proformaDate)   : undefined;
                    cost.recordingDate = cost.recordingDate ? new Date(cost.recordingDate)   : undefined;
                    return cost;
                });
        }


        
        function createTransaction( condominiumId, financialPeriodId, cost ) {
            return ManualTransactionService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).one("manual-transactions").post("",cost);
        }

        function checkRemoveTransaction(transaction) {
            return (transaction.counter.numOfChildren == 0) 
        }
        


        function removeTransaction(condominiumId,financialPeriodId, transactionId) {
            return ManualTransactionService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).all("manual-transactions").one("", transactionId).remove();
    
        }

        function updateTransaction(condominiumId, financialPeriodId, cost) {
            return ManualTransactionService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).one("manual-transactions",cost.id)
            .doPUT(cost);
        } 

        function updateProforma(condominiumId, financialPeriodId, cost) {
            return ManualTransactionService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).one("manual-transactions",cost.id)
            .post("update-proforma", cost);
        } 

        /**
         * MANAGE Payment
         *
         */
        function createPayment(condominiumId, financialPeriodId, transactionId, payment  ) {
            return ManualTransactionService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).one("manual-transactions",transactionId)
            .post("payment",payment);
        }

        function updatePayment(condominiumId, financialPeriodId, transactionId, payment ) {
             return ManualTransactionService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).one("manual-transactions",transactionId)
            .one("payment",payment.paymentData.record.id)
            .doPUT(payment);
        } 
        
        function removePayment(condominiumId,financialPeriodId, transactionId, paymentId) {
            return ManualTransactionService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).one("manual-transactions",transactionId)
            .one("payment",paymentId).remove();
        }

        function checkRemovePayment(condominiumId, financialPeriodId, transactionId, paymentId) {
            return ManualTransactionService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).one("manual-transactions",transactionId)
            .one("check-remove-payment",paymentId).head();
        }
       

        function getPayment(condominiumId, financialPeriodId, transactionId, recordId) {
            return ManualTransactionService.one("condominiums", condominiumId)
                .one("financial-period",financialPeriodId).one("manual-transactions",transactionId)
                .one("payment",recordId).get().then(function (payment) {
                    payment.paymentData.record.date =  payment.paymentData.record.date ? new Date(payment.paymentData.record.date) : undefined;
                    if ( payment.withholdingData.record != null)
                    {
                        payment.withholdingData.record.date =  payment.withholdingData.record.date ? new Date(payment.withholdingData.record.date) : undefined;
                        payment.withholdingData.record.expiryDate =  payment.withholdingData.record.expiryDate ? new Date(payment.withholdingData.record.expiryDate) : undefined;
                    }
                    return payment;
                });
        }
        

        function checkNewRecordAction(transaction){
            return true;
        }

        function openPopupNewRecord(condominiumId, companyId, financialPeriodId, transaction) {
            return $uibModal.open({
                bindToController: true,
                templateUrl: 'app/accounting-transaction/manual-transaction/payment-create.html',
                controllerAs: 'vm',
                size: 'lg',
                controller: 'PaymentCreateController',
                resolve: {
                    selectedTX: function () { return  transaction; }
                },
                windowClass: 'account-chooser-modal'
            }).result.then(function (newPayment) {
                return $q.resolve(newPayment);
            });
        }

        function openPopupEditRecord(condominiumId, companyId, financialPeriodId, transaction, record ) {
            return $uibModal.open({
                bindToController: true,
                templateUrl: 'app/accounting-transaction/manual-transaction/payment-edit.html',
                controllerAs: 'vm',
                size: 'lg',
                controller: 'PaymentEditController',
                resolve: {
                    selectedTX: function () { return  transaction; },
                    selectedPym:   function () { return record; }
                },
                windowClass: 'account-chooser-modal'
            }).result.then(function (payment) {
                return $q.resolve(payment);
            });
        }
   
        function openPopupProforma(condominiumId, companyId, period, transaction) {
            return $uibModal.open({
                bindToController: true,
                templateUrl: 'app/accounting-transaction/manual-transaction/proforma-update.html',
                controllerAs: 'vm',
                size: 'lg',
                controller: 'ProformaUpdateController',
                resolve: {
                    transaction: function () { return  transaction; },
                    period: function () { return  period; },
                    
                },
                windowClass: 'account-chooser-modal'
            }).result.then(function (transaction) {
                return $q.resolve(transaction);
            });
        }


        function fillTransaction(invoice){
             
            var transaction = new Object();
            transaction.invoiceDate = invoice.data;
            var haveWithholding = invoice.ritenute.length > 0;
            var haveIva         = invoice.imposte.length > 0;
            var isSimplified    = !haveWithholding && !haveIva;
            var withholdingToPay = 0;
            transaction.invoiceDetail = new Object();
            transaction.invoiceId     = invoice.id;
            transaction.totalAmount = invoice.totale.valore;

            if (invoice.tipoDocumento === 'PFIN'){
                transaction.proforma = true;}
            else if (invoice.tipoDocumento === 'CREN'){
                transaction.accreditationNote = true;   
            }    

            if (haveWithholding){
                var selectedWithholdingJson =  _.find( withHoldingRates, function (obj) { return obj.tax === invoice.ritenute[0].aliquota;});
                transaction.withholdingRate = selectedWithholdingJson.tax;
                transaction.withHolding = true;
                transaction.invoiceDetail.selectedWithholding = selectedWithholdingJson.label;
            }
            else{
                transaction.withholdingRate = undefined;
                transaction.withHolding = false;
            }

            if (!isSimplified){
                transaction.invoiceDetail.amountRow = new Array();

                _.forEach(invoice.imposte,function (row){
                    var iva = new  Object();
                    iva.amount                     = row.importo.valore;
                    iva.subjectedWithholding       = row.soggettaRitenuta;
                    var selectedIvaJson = _.find(ivaRates, function (c) { 
                        return c.tax == row.aliquota;
                    });
                    iva.selectedIva = selectedIvaJson.id;
                    iva.withholdingToPay = angular.isDefined(transaction.withholdingRate) ? parseFloat((iva.amount * transaction.withholdingRate / 100)) : 0;
                    transaction.invoiceDetail.amountRow.push(iva);
                    withholdingToPay += iva.withholdingToPay;
                });

                transaction.invoiceDetail.withholdingToPay = withholdingToPay;
                transaction.invoiceDetail.netAmount        = transaction.totalAmount = invoice.totale.valore;
                - withholdingToPay;
            }
            
            transaction.patrimonialWritingDetail = new Object();
            transaction.patrimonialWritingDetail.materializedPath = "02.003.0001."+ invoice.supplier.codice;     // SM supplier account binded, TODO use config for base path //08523
            
            if (angular.isDefined(invoice.competenza) && invoice.competenza != null ){
                transaction.startDate = angular.isDefined(invoice.competenza.from) ? invoice.competenza.from : null;
                transaction.endDate   = angular.isDefined(invoice.competenza.to)   ? invoice.competenza.to : null; 
            }
            transaction.simplified =  isSimplified;
            transaction.referenceDocument = invoice.numero;
            transaction.description       = invoice.numero;
            transaction.recordingDate = new Date();
            // SM expiry date, if present
            if( angular.isDefined(invoice.pagamenti[0].dettagli[0].scadenza) ) {
                transaction.expiryDate = new Date( invoice.pagamenti[0].dettagli[0].scadenza );
                //transaction.createExpiry = true;
            }


            return transaction;
        }
    }

})();

