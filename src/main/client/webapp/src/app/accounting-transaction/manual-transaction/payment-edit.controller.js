/*
 * financial-period-create.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */

(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .controller('PaymentEditController', PaymentEditController);

    PaymentEditController.$inject = ['$uibModalInstance', '$state', 'AccountingTransactionService', 'ManualTransactionService', 'MaterializedPathLookupService', 'NotifierService',
        'FinancialPeriodService', 'selectedTX', 'selectedPym', 'paymentModeList'];

    /* @ngInject */
    function PaymentEditController($uibModalInstance, $state, AccountingTransactionService, ManualTransactionService, MaterializedPathLookupService, NotifierService,
        FinancialPeriodService,  selectedTX, selectedPym, paymentModeList) {
        var vm = this;
        vm.doSave = doSave;
        vm.checkGenerateCredit = checkGenerateCredit;
        vm.paymentModeList = paymentModeList;
        activate();

        function activate() {
            loadFinancialPeriod();
        }

        function init() {
            vm.loading = true;
            vm.paymentDateOpened = false;
            vm.withholdingDate = false;

            MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, "RESOURCE_PATH").then(function (mathPath) {
                vm.prefixedAccountPath = mathPath.materializedPath;
            });

            vm.changeWithholdingPaid = changeWithholdingPaid;

            loadItem();
        }

        function loadFinancialPeriod() {
            FinancialPeriodService.getOne($state.params.companyId, $state.params.periodId).then(function (period) {
                vm.period = period;
                init();
            }).catch(function () {
                NotifierService.notifyError();
            })
        }

        function loadItem() {

            ManualTransactionService.getRecord($state.params.condominiumId, $state.params.periodId, $state.params.transactionId,
                selectedPym.id).then(function (payment) {
                    vm.firstLoad = false;
                    vm.dto = payment;
                    vm.showWithHolding = selectedTX.withHolding;
                    vm.isUpdate = true;

                    if (selectedTX.proformaInvoice)
                        vm.patrimonialPath = selectedTX.proformaSupplierMatPath;
                    else
                        vm.patrimonialPath = selectedTX.patrimonialWritingDetail.materializedPath;

                    if (vm.dto.paymentData.resourceWritingDetail.materializedPath == null)
                        vm.dto.paymentData.resourceWritingDetail.materializedPath = undefined;

                    if (selectedTX.withHolding) {
                        
                        if (vm.dto.withholdingData.resourceWritingPath == null)
                            vm.dto.withholdingData.resourceWritingPath = vm.dto.paymentData.resourceWritingPath;

                        vm.witholdinhGenerateCredit = (!angular.isUndefined(vm.dto.withholdingData.generateCreditWritingId) &&
                            vm.dto.withholdingData.generateCreditWritingId != null);

                        vm.dto.withholdingData.costWritingDetail = selectedTX.economicWritingDetail;
                    }

                    vm.loading = false;


                })
                .catch(function () {
                    return NotifierService.notifyError('ManualTransaction.LoadDetail.Failure');
                    vm.loading = true;
                })

        }


        function changeWithholdingPaid() {
            if (!vm.dto.withholdingPaid) {
                vm.dto.withholdingData.generateCreditAmount = undefined;
                vm.dto.withholdingData.sanctionAmount = undefined;
                vm.dto.withholdingData.interestAmount = undefined;
                vm.dto.withholdingData.usingCreditAmount = undefined;
                vm.dto.withholdingData.difference = undefined;

            }

        }

        function checkGenerateCredit(value) {
            return value >= 0;
        }



        function doSave() {

            vm.dto.paymentData.patrimonialWritingDetail = new Object();
            vm.dto.paymentData.patrimonialWritingDetail.materializedPath = vm.patrimonialPath;
            $uibModalInstance.close(vm.dto);
        }




    }

})();

