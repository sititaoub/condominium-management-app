/*
 * cost-management-create.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .controller('ManualTxCreateController', ManualTxCreateController);

    ManualTxCreateController.$inject = ['$state', 'ManualTransactionService', 'period', 'NotifierService', 'MaterializedPathLookupService',
        'AccountingTransactionService', 'manualTransactionTypes', 'referenceDocumentList', 'withHoldingRates', 'deductibilityTypes', 'paymentModeList','$q','PurchaseInvoicesService'];

    /* @ngInject */
    function ManualTxCreateController($state, ManualTransactionService, period, NotifierService, MaterializedPathLookupService,
        AccountingTransactionService, manualTransactionTypes, referenceDocumentList, withHoldingRates, deductibilityTypes, paymentModeList, $q, PurchaseInvoicesService) {
        var vm = this;
        vm.loading = true;
        vm.companyId = $state.params.companyId;
        vm.periodId = $state.params.periodId;
        vm.doInsertAndContinue = doInsertAndContinue;
        vm.doInsertAndExit = doInsertAndExit;
        vm.setProforma = setProforma;
        vm.onChangeType = onChangeType;
        vm.onChangeWithholdingEnabled = onChangeWithholdingEnabled;
        vm.withHoldingRates = withHoldingRates;
        vm.deductibilityTypes = deductibilityTypes;
        vm.paymentModeList = paymentModeList;

        activate();

        ////////////////

        function activate() {
            vm.period = period;
            vm.periodLoaded = true;
            initializeVar();
        }

        function initializeVar() {
           
            //date flag
            vm.dateFromOpened = false;
            vm.dateToOpened = false;
            vm.proformaDateOpened = false;
            vm.invoiceExpireDateOpened = false;
            vm.invoiceDateOpened = false;
            vm.paymentDateOpened = false;
            vm.periodLoaded = true;
            vm.invoice = 
            //costant
            vm.manualTransactionTypes = manualTransactionTypes;
            vm.referenceDocumentList = referenceDocumentList;
           
            var invoiceId = $state.params.invoiceId;

            var kind = _.find(manualTransactionTypes,
                function (val) { return val == $state.params.transactionType; });

            if (angular.isUndefined(kind))
                kind = manualTransactionTypes[0];
            
            var toGivepath = AccountingTransactionService.getTransactionFormConfiguration(kind).toGivepath;
            var firstChain =  MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, toGivepath).then(function (mathPath) {
                vm.economicRootPath = mathPath.materializedPath;
            });

            var toHavepath = AccountingTransactionService.getTransactionFormConfiguration(kind).toHavepath;
            var secondChain =  MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, toHavepath).then(function (mathPath) {
                vm.patrimonialRootPath = mathPath.materializedPath;
            });

            var thirdChain =   MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, "RESOURCE_PATH").then(function (mathPath) {
                vm.prefixedResourceRootPath = mathPath.materializedPath;
            });

            var fourthChain =   MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, "PROFORMA_PATH").then(function (mathPath) {
                vm.proformaRootPath = mathPath.materializedPath;
            });

            vm.showWithHolding   = AccountingTransactionService.getTransactionFormConfiguration(kind).withHolding;
            vm.showProforma      = AccountingTransactionService.getTransactionFormConfiguration(kind).showProforma;
            vm.useOnlySimplified = AccountingTransactionService.getTransactionFormConfiguration(kind).useOnlySimplified;
             
            $q.all([firstChain, secondChain, thirdChain, fourthChain]).then(function() {
                vm.supplierMatPath = undefined;
                vm.amountPaid = 0;
                vm.withHoldingAmountPaid = 0;
                vm.isUpdate = false;

                if (angular.isDefined(invoiceId)){
                    PurchaseInvoicesService.getOne( vm.companyId, invoiceId).then(function (invoice) {
                        vm.invoiceDetail = invoice;
                        vm.tx = ManualTransactionService.fillTransaction(vm.invoiceDetail);
                        
                        if (angular.isDefined(vm.tx.patrimonialWritingDetail.materializedPath))
                            vm.supplierMatPath = vm.tx.patrimonialWritingDetail.materializedPath;
                        
                        vm.invoiceType = vm.tx.accreditationNote ? 'NOTE' : 'INVOICE';
                        vm.tx.type = kind;
                        vm.loading = false;
                    });
                

                }else{
                    vm.tx = new Object();
                    vm.tx.simplified = true;
                    vm.tx.invoiceDetail = new Object();
                    vm.tx.economicWritingDetail = undefined;
                    vm.tx.patrimonialWritingDetail = new Object();
                    vm.loading = false;
                    vm.invoiceType = vm.tx.accreditationNote ? 'NOTE' : 'INVOICE';
                    vm.tx.type = kind;
                }

                
               
            });


        }




        function onChangeType(){
             vm.tx.accreditationNote = (vm.invoiceType == 'NOTE');
             
             if (vm.tx.accreditationNote){     
                vm.tx.proformaInvoice = false;
                vm.tx.withHolding = false;
             }   
        }

        function onChangeWithholdingEnabled(){
            if (vm.tx.withHolding)
                vm.tx.simplified = false;
        }


        function doInsertAndContinue() {
            if (checkDate()){
                preInsert();
                return ManualTransactionService.createTransaction($state.params.condominiumId, $state.params.periodId, vm.tx).then(function () {
                    return NotifierService.notifySuccess('ManualTransaction.Create.Success').then(function () {
                        $state.reload();
                    });
                })
                .catch(function (err) {
                    NotifierService.notifyError('ManualTransaction.Create.Failure',err);
                    return $q.reject(err);
                });
             }else{
                NotifierService.notifyError('ManualTransaction.Create.DateRange');
                return $q.reject();
            }
        }

        function doInsertAndExit() {
            if (checkDate()){
                preInsert(); 
                return ManualTransactionService.createTransaction($state.params.condominiumId, $state.params.periodId, vm.tx).then(function () {
                    return NotifierService.notifySuccess('ManualTransaction.Create.Success').then(function () {
                        return $state.go('^', { inherits: true });
                    });
                })
                .catch(function (err) {
                    NotifierService.notifyError('ManualTransaction.Create.Failure',err);
                    return $q.reject(err);
                });
            }else{
                NotifierService.notifyError('ManualTransaction.Create.DateRange');
                return $q.reject();
            }
        }


        function checkDate(){
           return vm.tx.endDate >=  vm.tx.startDate
        }

        function preInsert() {

            vm.tx.condominiumExternalId = $state.params.condominiumId;
            vm.tx.financialPeriodExternalId = $state.params.periodId;

            vm.totalPercentage = _.sumBy(vm.tx.economicWritingDetail, 'coefficient');
            vm.tx.createExpiry = true;
            if (!vm.tx.proformaInvoice) {
                vm.tx.patrimonialWritingDetail.materializedPath = vm.supplierMatPath;
            } else {
                vm.tx.patrimonialWritingDetail.materializedPath = vm.proformaRootPath;
                vm.tx.proformaSupplierMatPath = vm.supplierMatPath;
            }

            if (vm.tx.alreadyPaid) {
                vm.tx.payment.amount = vm.tx.amountNet;
                vm.tx.payment.patrimonialWritingDetail = new Object();
                vm.tx.payment.patrimonialWritingDetail.materializedPath = vm.tx.patrimonialWritingDetail.materializedPath;

                if (vm.tx.proformaInvoice)
                    vm.tx.payment.patrimonialWritingDetail.materializedPath = vm.tx.proformaSupplierMatPath;
                else
                    vm.tx.payment.patrimonialWritingDetail.materializedPath = vm.tx.patrimonialWritingDetail.materializedPath;
            }
        }


        function setProforma(transaction) {
            if (transaction.proformaInvoice) {
                vm.tx.invoiceDetail.invoiceDate = "";
                vm.tx.invoiceExpireDate = "";
                vm.tx.referenceDocument = "";
            }
            else {
                vm.tx.proformaDate = "";
                vm.tx.proformaReferenceDocument = "";
            }

        }

    }

})();

