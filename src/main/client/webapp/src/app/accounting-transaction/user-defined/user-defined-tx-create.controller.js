/*
 * cost-management-create.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .controller('UserDefinedTxCreateController', UserDefinedTxCreateController);

    UserDefinedTxCreateController.$inject = ['$state', 'UserDefinedTxService', 'period', 'MaterializedPathLookupService', 'NotifierService',
        'AccountingTransactionService', 'AccountingWritingService', '$q', 'AutomatedWritingsServices'];

    /* @ngInject */
    function UserDefinedTxCreateController($state, UserDefinedTxService, period, MaterializedPathLookupService, NotifierService, AccountingTransactionService,
        AccountingWritingService, $q, AutomatedWritingsServices) {
        var vm = this;
        vm.companyId = $state.params.companyId;
        vm.periodId = $state.params.periodId;
        
        vm.doSave = doSave;
        vm.loading = true;
        activate();

        ////////////////

        function activate() {
            vm.externaltemplate = angular.isDefined($state.params.template);
            vm.periodLoaded = false;
            vm.period = period;
            vm.periodLoaded = true;
            initializeVar();
        }


        function initializeVar() {
            var kind = $state.params.transactionType;
            vm.template = $state.params.template;
            vm.dateFromOpened = false;
            vm.dateToOpened = false;
            vm.recordingDateOpened = false;
            vm.isUpdate = false;

            vm.tx = new Object();
            vm.tx.writing = new Object();
            vm.tx.type = kind;
            vm.disableUpdatePath = false;

            var useTemplate = AccountingTransactionService.getTransactionFormConfiguration(kind).useTemplate;

            if (useTemplate){
                vm.template = vm.tx.type;
                vm.disableUpdatePath = true;
                vm.externaltemplate = true;
                vm.tx.writing.type = vm.tx.type;
            }else
                vm.tx.writing.type = 'USER_DEFINED_TRANSACTION';

            vm.tx.competence = new Object();
            
            if (vm.externaltemplate){
                
                AutomatedWritingsServices.getByCode($state.params.companyId, vm.period.chartOfAccountId, vm.template).then(function (template) {
                    vm.tx.writing.automatedWriting = template;
                    vm.tx.writing.description = template.description;
                    vm.loading = false;
                });

            }else {

                AutomatedWritingsServices.getPage($state.params.companyId, null, 0, 1000).then(function (page) {
                    vm.templates = page.content;
                    vm.loading = false;
                });
    
            }

        }


        function doSave() {

            if (checkDate()) {
                vm.tx.condominiumExternalId = $state.params.condominiumId;
                vm.tx.writing.financialPeriodId = $state.params.periodId;
                var toGiveAmount = _.sumBy(vm.tx.toGiveDetail, "amount");
                var toHaveAmount = _.sumBy(vm.tx.toHaveDetail, "amount");

                if (toGiveAmount != toHaveAmount) {
                    NotifierService.notifyError('UserDefinedTransaction.AmountError');
                    return $q.reject();
                }
                else {
                    return UserDefinedTxService.createTransaction($state.params.condominiumId, $state.params.periodId, vm.tx).then(function () {
                        return NotifierService.notifySuccess('UserDefinedTransaction.Create.Success').then(function () {
                            return $state.go('^', { inherits: true });
                        });
                    })
                        .catch(function (err) {
                            NotifierService.notifyError('UserDefinedTransaction.Create.Failure', err);
                            return $q.reject(err);
                        });
                }
            } else {
                NotifierService.notifyError('UserDefinedTransaction.Create.DateRange');
                return $q.reject();
            }
        }

        function checkDate(){
            return vm.tx.writing.competence.endDate >=  vm.tx.writing.competence.startDate
         }


    }

})();

