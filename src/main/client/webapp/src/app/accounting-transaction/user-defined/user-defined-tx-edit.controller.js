/*
 * financial-period-create.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .controller('UserDefinedTxEditController', UserDefinedTxEditController);

    UserDefinedTxEditController.$inject = ['$state', 'UserDefinedTxService', 'NotifierService', 'period', 'MaterializedPathLookupService',
        'AccountingTransactionService', 'AccountingWritingService', '$q'];

    /* @ngInject */
    function UserDefinedTxEditController($state, UserDefinedTxService, NotifierService, period,
        MaterializedPathLookupService, AccountingTransactionService, AccountingWritingService, $q) {
        var vm = this;

        vm.doSave = doSave;
        activate();

        ////////////////

        function activate() {
            vm.companyId = $state.params.companyId;
            vm.periodId = $state.params.periodId;
            vm.periodLoaded = false;
            vm.loading = true;
            //Calandar flag
            vm.dateFromOpened = false;
            vm.dateToOpened = false;
            vm.isUpdate = true;
            vm.period = period;
            vm.periodLoaded = true;
            loadCost();

        }

        function loadCost() {
            UserDefinedTxService.getTransaction($state.params.condominiumId, $state.params.periodId,
                $state.params.transactionId).then(function (transaction) {
                    vm.firstLoad = false;
                    vm.tx = transaction;
                    var kind = vm.tx.type;

                    
                    var useTemplate = AccountingTransactionService.getTransactionFormConfiguration(kind).useTemplate;

                    if (useTemplate){
                        vm.disableUpdatePath = true;
                     }

                    vm.updateDisabled = (transaction.writing.financialPeriodId != $state.params.periodId);

                    vm.loading = false;
                });
        }




        function doSave() {

            if (checkDate()) {

                    vm.tx.condominiumExternalId = $state.params.condominiumId;
                    vm.tx.financialPeriodExternalId = $state.params.periodId;
                    var toGiveAmount = _.sumBy(vm.tx.toGiveDetail, "amount");
                    var toHaveAmount = _.sumBy(vm.tx.toHaveDetail, "amount");

                    if (toGiveAmount != toHaveAmount) {
                        NotifierService.notifyError('UserDefinedTransaction.AmountError');
                        return $q.reject();
                    }
                    else {

                        return UserDefinedTxService.updateTransaction($state.params.condominiumId,
                            $state.params.periodId, vm.tx).then(function () {
                                return NotifierService.notifySuccess('UserDefinedTransaction.Update.Success').then(function () {
                                    return $state.go('^', { inherits: true });
                                });
                            }).catch(function (err) {
                                NotifierService.notifyError('UserDefinedTransaction.Update.Failure', err);
                                return $q.reject(err);
                            });
                    }
            }else{
                NotifierService.notifyError('ManualTransaction.Update.DateRange');
                return $q.reject();
            }

        }

        
        function checkDate(){
            return vm.tx.writing.competence.endDate >=  vm.tx.writing.competence.startDate;
         }


    }

})();

