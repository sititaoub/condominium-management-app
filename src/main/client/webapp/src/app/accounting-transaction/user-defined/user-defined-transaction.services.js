/*
 * accounting-transaction.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .factory('UserDefinedTxService', UserDefinedTxService);

        UserDefinedTxService.$inject = ['$rootScope', '$sessionStorage', 'Restangular', 'contextAddressManagement', '_','$uibModal','$q'];

    /* @ngInject */
    function UserDefinedTxService($rootScope, $sessionStorage, Restangular, contextAddressManagement, _, $uibModal, $q) {
        var UserDefinedTxService = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressManagement + 'v1');
        });
        
        
        var service = {
            getTransaction:      getTransaction,
            createTransaction:   createTransaction,
            removeTransaction:   removeTransaction,
            checkRemoveTransaction: checkRemoveTransaction,
            updateTransaction:   updateTransaction,
            checkNewRecordAction: checkNewRecordAction
        };
        return service;


        /**
         * MANAGE Cost
         * 
         */

         function getTransaction(condominiumId, financialPeriodId, transactionId) {
            return UserDefinedTxService.one("condominiums", condominiumId)
                .one("financial-period",financialPeriodId).one("user-transactions",transactionId)
                .get().then(function (tx) {
                    if (angular.isDefined(tx.writing)){
                        tx.writing.competence.startDate =  tx.writing.competence.startDate ? new Date(tx.writing.competence.startDate) : undefined;
                        tx.writing.competence.endDate   =  tx.writing.competence.endDate   ? new Date(tx.writing.competence.endDate)   : undefined;
                        tx.writing.recordingDate   = tx.writing.recordingDate   ? new Date( tx.writing.recordingDate)   : undefined;
                    }
                    return tx;
                });
        }

        function checkRemoveTransaction(transaction) {
            return true;
        }

        function removeTransaction(condominiumId,financialPeriodId, transactionId) {
            return UserDefinedTxService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).all("user-transactions").one("", transactionId).remove();
    
        }
        
        function createTransaction( condominiumId, financialPeriodId, tx ) {

            return UserDefinedTxService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).one("user-transactions").post("",tx);
        }

        function updateTransaction(condominiumId, financialPeriodId, tx) {
            return UserDefinedTxService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).one("user-transactions",tx.id)
            .doPUT(tx);
        } 
   

        function checkNewRecordAction(transaction){
            return false;
        }


    }

})();

