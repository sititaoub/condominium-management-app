/*
 * accounting-transaction.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .factory('MoneyTransferTxService',MoneyTransferTxService);

        MoneyTransferTxService.$inject = ['$rootScope', '$sessionStorage', 'Restangular', 'contextAddressManagement', '_','$uibModal','$q'];

    /* @ngInject */
    function MoneyTransferTxService($rootScope, $sessionStorage, Restangular, contextAddressManagement, _, $uibModal, $q) {
        var MoneyTransferTxService = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressManagement + 'v1');
        });
        
        
        var service = {
            getTransaction:      getTransaction,
            createTransaction:   createTransaction,
            removeTransaction:   removeTransaction,
            checkRemoveTransaction: checkRemoveTransaction,
            updateTransaction:   updateTransaction, 
            checkNewRecordAction: checkNewRecordAction
        };
        return service;


        /**
         * MANAGE Cost
         * 
         */

         function getTransaction(condominiumId, financialPeriodId, transactionId) {
            return MoneyTransferTxService.one("condominiums", condominiumId)
                .one("financial-period",financialPeriodId).one("money-transfers",transactionId)
                .get().then(function (tx) {
                    tx.startDate =  tx.startDate ? new Date(tx.startDate) : undefined;
                    tx.endDate   =tx.endDate   ? new Date(tx.endDate)   : undefined;
                    return tx;
                });
        }

        function checkRemoveTransaction(transaction) {
            return true;
        }

        function removeTransaction(condominiumId,financialPeriodId, transactionId) {
            return MoneyTransferTxService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).all("money-transfers").one("", transactionId).remove();
    
        }
        
        function createTransaction( condominiumId, financialPeriodId, tx ) {

            return MoneyTransferTxService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).one("money-transfers").post("",tx);
        }

        function updateTransaction(condominiumId, financialPeriodId, tx) {
            return MoneyTransferTxService.one("condominiums", condominiumId)
            .one("financial-period",financialPeriodId).one("money-transfers",tx.id)
            .doPUT(tx);
        } 
   

        function checkNewRecordAction(transaction){
            return false;
        }


    }

})();

