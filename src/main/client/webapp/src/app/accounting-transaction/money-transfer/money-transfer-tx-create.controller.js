/*
 * cost-management-create.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .controller('MoneyTransferTxCreateController', MoneyTransferTxCreateController);

        MoneyTransferTxCreateController.$inject = ['$state', 'MoneyTransferTxService', 'period', 'MaterializedPathLookupService', 'NotifierService',
    'AccountingTransactionService','moneyTransferTransactionTypes','$q'];

    /* @ngInject */
    function MoneyTransferTxCreateController($state, MoneyTransferTxService, period, MaterializedPathLookupService, NotifierService, AccountingTransactionService, moneyTransferTransactionTypes,$q) {
        var vm = this;
      
        vm.doSave = doSave;
        vm.moneyTransferTransactionTypes = moneyTransferTransactionTypes;
        vm.checkFinancialPeriodDate = checkFinancialPeriodDate;
        vm.loading = true;    
        activate();

        ////////////////

        function activate() {
            vm.period = period;
            vm.periodLoaded = true;
            initializeVar();    
        }


        function initializeVar() {
            
            vm.dateFromOpened = false;
            vm.dateOpened = false;

            var kind = _.find(moneyTransferTransactionTypes, 
                function(val){ return val ==  $state.params.transactionType; });
           
            if (angular.isUndefined(kind)) 
                kind = moneyTransferTransactionTypes[0];

            var toGivepath   = AccountingTransactionService.getTransactionFormConfiguration(kind).toGivepath;
            var firstChain   =  MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, toGivepath).then(function (mathPath) {
                vm.prefixedResourceFromPath = mathPath.materializedPath;
            });

            var toHavepath =  AccountingTransactionService.getTransactionFormConfiguration(kind).toHavepath;
            var secondChain   =  MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, toHavepath).then(function (mathPath) {
                vm.prefixedResourceToPath = mathPath.materializedPath;
            });
   

            $q.all([firstChain, secondChain]).then(function() {
                    
                vm.isUpdate = false;
                vm.tx = new Object();
            
                vm.tx.resourceTo = new Object();
                vm.tx.resourceFrom    = new Object(); 
                vm.tx.type = kind;      
                vm.loading = false;  
            });  

        }


        function doSave() {
            preInsert();

            if (vm.tx.resourceTo.materializedPath == vm.tx.resourceFrom.materializedPath){
                 NotifierService.notifyError('MoneyTransferTransaction.ResourceError');
                 return $q.reject();}
            else {
                return MoneyTransferTxService.createTransaction($state.params.condominiumId,$state.params.periodId,vm.tx).then(function () {
                    return NotifierService.notifySuccess('MoneyTransferTransaction.Create.Success.' + vm.tx.type).then(function () {
                        return $state.go('^', { inherits: true });
                    });
                })
                .catch(function (err) {
                    NotifierService.notifyError('MoneyTransferTransaction.Create.Failure',err);
                    return $q.reject(err);
                });
             }
        }


        function preInsert()
        {
            vm.tx.condominiumExternalId     = $state.params.condominiumId;
            vm.tx.financialPeriodExternalId = $state.params.periodId;
            
            if (vm.tx.alreadyPaid){
                vm.tx.payment.amount=vm.tx.amountNet;
                vm.tx.payment.patrimonialWritingDetail = new Object();
                vm.tx.payment.patrimonialWritingDetail.materializedPath=vm.tx.patrimonialWritingDetail.materializedPath;
            }
         }


         function checkFinancialPeriodDate(valTocheck) {
            if (!angular.isUndefined(vm.period)) {
                var dateToCheck = new Date(valTocheck);
                dateToCheck.setHours(0, 0, 0, 0)

                var startDate = new Date(vm.period.period.startDate);
                startDate.setHours(0, 0, 0, 0)

                var endDate = new Date(vm.period.period.endDate);
                endDate.setHours(0, 0, 0, 0)

                return dateToCheck >= startDate && dateToCheck <= endDate;
            }
            else return true;
        }
    }

})();

