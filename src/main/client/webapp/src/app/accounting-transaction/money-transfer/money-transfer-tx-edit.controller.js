/*
 * financial-period-create.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .controller('MoneyTransferTxEditController', MoneyTransferTxEditController);

        MoneyTransferTxEditController.$inject = ['$state', 'managementTypes', 'MoneyTransferTxService', 'NotifierService', 'period','AccountingTransactionService','manualTransactionTypes','moneyTransferTransactionTypes','MaterializedPathLookupService','$q'];

    /* @ngInject */
    function MoneyTransferTxEditController($state, managementTypes, MoneyTransferTxService, NotifierService, period, AccountingTransactionService, manualTransactionTypes,
         moneyTransferTransactionTypes, MaterializedPathLookupService, $q) {
        var vm = this;

        vm.doSave = doSave;
        vm.moneyTransferTransactionTypes = moneyTransferTransactionTypes;
        vm.checkFinancialPeriodDate = checkFinancialPeriodDate;
        
        activate();
    
        ////////////////

        function activate() {
            vm.period = period;
            vm.periodLoaded = true;
            vm.loading = true;
            vm.dateOpened = false;
            vm.isUpdate = true;
            
            loadTx();
        }

        function loadTx() {
            MoneyTransferTxService.getTransaction($state.params.condominiumId, $state.params.periodId,
                $state.params.transactionId).then(function (transaction) {
               
                vm.tx = transaction;
              
                vm.manualTransactionTypes = manualTransactionTypes;
         
                var toGivepath   = AccountingTransactionService.getTransactionFormConfiguration(vm.tx.type).toGivepath;
                var firstChain   = MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, toGivepath).then(function (mathPath) {
                    vm.prefixedResourceFromPath = mathPath.materializedPath;
                });
    
                var toHavepath  = AccountingTransactionService.getTransactionFormConfiguration(vm.tx.type).toHavepath;
                var secondChain = MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, toHavepath).then(function (mathPath) {
                    vm.prefixedResourceToPath = mathPath.materializedPath;
                });

                $q.all([firstChain, secondChain]).then(function() {
                   vm.firstLoad = false;
                   vm.loading = false;
                });    
            });
        }

        function doSave() {
            vm.tx.condominiumExternalId     = $state.params.condominiumId;
            vm.tx.financialPeriodExternalId = $state.params.periodId;
            
            if (vm.tx.resourceTo.materializedPath == vm.tx.resourceFrom.materializedPath){
                NotifierService.notifyError('MoneyTransferTransaction.ResourceError');
                return $q.reject();}
            else {

                   return MoneyTransferTxService.updateTransaction($state.params.condominiumId,
                            $state.params.periodId, vm.tx).then(function () {
                        return NotifierService.notifySuccess('AccountingTransaction.Update.Success').then(function () {
                            return $state.go('^', { inherits: true });
                        });
                    }).catch(function (err) {
                         NotifierService.notifyError('AccountingTransaction.Update.Failure',err);
                         return $q.reject(err);
                    });
                }
            }


            function checkFinancialPeriodDate(valTocheck) {
                if (!angular.isUndefined(vm.period)) {
                    var dateToCheck = new Date(valTocheck);
                    dateToCheck.setHours(0, 0, 0, 0)
    
                    var startDate = new Date(vm.period.period.startDate);
                    startDate.setHours(0, 0, 0, 0)
    
                    var endDate = new Date(vm.period.period.endDate);
                    endDate.setHours(0, 0, 0, 0)
    
                    return dateToCheck >= startDate && dateToCheck <= endDate;
                }
                else return true;
            }

    }

})();

