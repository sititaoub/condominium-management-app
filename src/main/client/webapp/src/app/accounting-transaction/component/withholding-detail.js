/*
 * amount-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.condominiums')
        .component('withholdingDetail', {
            controller: WithholdingDetailController,
            
            bindings: {
                ngModel: '<',
                companyId: '<',
                rootPath: '<',
                transaction: '<',
                haveWithholding: '<',
                facilitatedSupplier: '<',
                financialperiodId: '<',
                totalTaxableInput: "<",
                totalIvaInput: "<"
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/accounting-transaction/component/withholding-detail.html'
        });

        WithholdingDetailController.$inject = ['withHoldingRates'];

    // MANAGE THIS JSON OBJECT 
    // witholdigData {
    // 
    //                  totalTaxable          = totalTaxable holds the taxable amount amount received by external input    
    //                  totalAmount           = contains the sum of taxableInput and totalIvaINput received as input  $ctrl.totalTaxableInput + $ctrl.totalIvaInput           
    //                  withholdingRate       = contains the withholding rate selected by user
    //                  withholdingToPay     = contains the computed amount of withholding to pay
    //                  exemptIva             = contains the exempt iva amount provided by user input
    //                  freeWithholdingTax    = contains the amount free of withholding provided by user input
    //                  freeWithholdingTaxIva = contains the computed amount of IVA (with 22% rate or 0 if is facilitated supplier) of freeWithholdingTax amount
    //                  freeWithholdingAmount = sum freeWithholdingTax + freeWithholdingTaxIva
    //                  totalDocument         = totalAmount + exemptIvaAmount + freeWithholdingAmount 
    //                  toPay                 = totalDocument - withholdingToPay
    //  }
    //    

    /* @ngInject */
    function WithholdingDetailController(withHoldingRates) {
        var $ctrl = this;
        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.$onChanges = bindingOnChange;
        $ctrl.value = undefined;
        $ctrl.doSum = doSum;
        $ctrl.withHoldingRates = withHoldingRates;

        ////////////

        function onInit() {

            $ctrl.ngModelCtrl.$render = function () {
                
                doSum();
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
                $ctrl.updateEnabled = true;
            };

            $ctrl.loading = false;
        }

        function bindingOnChange(changes) {

            if (changes.facilitatedSupplier || changes.totalTaxableInput
            || changes.totalIvaInput){  
                onChange();
            }


        }
        

        function onChange() {
            doSum();
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function doSum() {

            var total = 0;
            var iva = 0;
            var taxable = 0;
 
            if (angular.isUndefined($ctrl.value))
                $ctrl.value = {};
            
            $ctrl.value.totalTaxable = parseFloat($ctrl.totalTaxableInput);              
            $ctrl.totalIva = $ctrl.facilitatedSupplier ?  0 : $ctrl.totalIvaInput;
            $ctrl.value.totalAmount = parseFloat($ctrl.totalTaxableInput + $ctrl.totalIva);              
            $ctrl.value.withholdingRate = angular.isDefined($ctrl.withHoldingRate) ? $ctrl.withHoldingRates[$ctrl.withHoldingRate].tax : 0
            $ctrl.value.withholdingToPay = parseFloat(($ctrl.totalTaxableInput * $ctrl.value.withholdingRate / 100));

            var exemptIvaAmount = angular.isNumber($ctrl.value.exemptIva) ? parseFloat($ctrl.value.exemptIva) : 0;
            var freeWithholdingAmount = 0;
            
            if (angular.isDefined($ctrl.value.freeWithholdingTax)) {
                var rate= $ctrl.facilitatedSupplier ? 0 : 22;
                $ctrl.value.freeWithholdingTaxIva = parseFloat(($ctrl.value.freeWithholdingTax * rate / 100));
                freeWithholdingAmount = parseFloat($ctrl.value.freeWithholdingTax + $ctrl.value.freeWithholdingTaxIva);
            }

            $ctrl.value.totalDocument = parseFloat($ctrl.value.totalAmount + exemptIvaAmount + freeWithholdingAmount);
            $ctrl.value.toPay = parseFloat($ctrl.value.totalDocument - $ctrl.value.withholdingToPay);

        }



    }









})();

