/*
 * technician-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.condominiums')
        .component('materializedPathList', {
            controller: MaterializedPathListController,
            bindings: {
                ngModel: '<',
                transaction: '<',
                period: '<',
                ngDisabled: '<?',
                componentTitle: '@',
                externalAmountToDivide: '<?',
                externalRootPath: '<?',
                template: '<?',
                disableUpdatePath: '<?',
                enableDuplicatePath: '<?',
                isToHave: '<?'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/accounting-transaction/component/materialized-path-list.html'
        });

    MaterializedPathListController.$inject = ['$attrs'];

    /* @ngInject */
    function MaterializedPathListController( $attrs) {
        var $ctrl = this;
        $ctrl.$onInit = onInit;
        $ctrl.initialized = false;
        $ctrl.onChange = onChange;
        $ctrl.changeRowType = changeRowType;
        $ctrl.$onChanges = bindingOnChange;
        $ctrl.value = undefined;
        $ctrl.doSum = doSum;
        $ctrl.fnCheckPercentage   =  fnCheckPercentage;
        $ctrl.fnCheckSum          =  fnCheckSum;
        $ctrl.fnCheckAmountPositive =  fnCheckAmountPositive;
        $ctrl.addRow       = addRow;
        $ctrl.removeRow    = removeRow;
        $ctrl.duplicateRow = duplicateRow;
        $ctrl.totalPercentage = 0;
        $ctrl.usingExternalAmount = false;
        $ctrl.setUsingPercentage = setUsingPercentage;
        $ctrl.sumBy=_.sumBy;
        ////////////

 
        function onInit() {
 
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.sum = 0;
                $ctrl.externalAmountIsDefined    =  angular.isDefined($attrs.externalAmountToDivide);
                $ctrl.externaltemplateIsDefined  =  angular.isDefined($attrs.template);
                $ctrl.rootPathAccountIsDefined   =  angular.isDefined($attrs.externalRootPath);
                $ctrl.showPercentageToolbar      =  $ctrl.externalAmountIsDefined;
                $ctrl.value                      =  $ctrl.ngModelCtrl.$viewValue;
                $ctrl.writingType                =  !$ctrl.isToHave ? 'TO_GIVE' : 'TO_HAVE';
                $ctrl.placeHolder                =  undefined;
                

				//if model contains value
                if (angular.isArray($ctrl.value) && $ctrl.value.length > 0){
					$ctrl.usingPercentage  = false;
                    var rootPath    =  $ctrl.rootPathAccountIsDefined ? $ctrl.externalRootPath : undefined;
                    $ctrl.value     = _.map($ctrl.value, function(detail){
                        return _.defaults({
                            coefficient: 0,
                            "rootPath": rootPath , 
                            "rootPathIsDefined" : $ctrl.rootPathAccountIsDefined, 
                            "percentage" : false
                        }, detail)
                    });
                }else{

                    if ($ctrl.template){
                        reloadTemplate($ctrl.template);    
                    }else   
                        initEmptyTemplate();
                }

                $ctrl.initialized = true;
                doSum();
                $ctrl.loading = false;
            };

  
        }

        function initEmptyTemplate(){
            $ctrl.value= new Array();
            $ctrl.usingPercentage = true;
            var rootPath          =  $ctrl.rootPathAccountIsDefined ? $ctrl.externalRootPath : undefined; 
            var amountType        =  $ctrl.externalAmountIsDefined ? "PERCENTAGE" : "AMOUNT";
            addInternalRow(rootPath, undefined, 100, amountType, 0 );
        }

        function reloadAmount(amount){
            if ($ctrl.value.length == 1 && $ctrl.value[0].percentage)
                 $ctrl.value[0].amount = amount;
        }


        function reloadTemplate(template){

            if (template != null){
                            
                $ctrl.value= new Array();
                var filteredTemplate = _.filter(template.details, { type: $ctrl.writingType }) ;
                $ctrl.usingPercentage = true;
                _.forEach(filteredTemplate,function (row){
                    var amount      =  row.amountType !=  'PERCENTAGE' ? row.amountValue : 0;
                    var coefficient =  row.amountType === 'PERCENTAGE' ? row.amountValue : 0;
                    var type        =  row.type;
                    var rootPath    =  $ctrl.rootPathAccountIsDefined ? $ctrl.externalRootPath : undefined; 
                    var matPath     =  row.materializedPath;
                    if (_.endsWith(row.materializedPath,'*')){
                        rootPath  = row.materializedPath.substring(0, row.materializedPath.length-2);
                        matPath   = undefined;
                    }
                    addInternalRow(rootPath, matPath, coefficient, row.amountType, amount, undefined );
                });
            }else{
                initEmptyTemplate();
            }

        }


        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function bindingOnChange(changes) { 
            if ($ctrl.initialized)
            { 
                if (changes.template)
                    reloadTemplate($ctrl.template);                    
                
                if (changes.externalAmountToDivide)
                     reloadAmount($ctrl.externalAmountToDivide);
    
                if (changes.externalAmountToDivide || changes.template)
                   doSum();
            }

        }

        function doSum(){

            if (angular.isUndefined($ctrl.externalAmountToDivide))
                 $ctrl.externalAmountToDivide = 0;

            var total = 0;
            
            if ($ctrl.usingPercentage){
                $ctrl.sum = 0;
          
                var totalFixedAmount =   _.sumBy(_.flatMap(_.filter($ctrl.value, function (row) {return !row.percentage;})),'amount');
                var amountToDivide = $ctrl.externalAmountToDivide - totalFixedAmount;
                var foundPercentage = false;   
                var coefficient;
                angular.forEach($ctrl.value, function (value) {
                 
                var computed = value.amount;
                    if (value.percentage){
                        coefficient =  value.coefficient ?  value.coefficient : 0;
                        computed =  parseFloat((parseFloat(amountToDivide.toFixed(2) / 100) * parseFloat( coefficient.toFixed(4))).toFixed(2));
                        value.amount =  parseFloat(computed).toFixed(2);
                        foundPercentage = true;
                    }

                        total += value.coefficient;
                        
                        $ctrl.sum += parseFloat( computed.toFixed(2));
                });

                $ctrl.totalPercentage = parseFloat(total.toFixed(4));

                if ($ctrl.externalAmountIsDefined){
                    $ctrl.checkPercentage = !foundPercentage || ( $ctrl.totalPercentage == 100);
                    $ctrl.checkAmount     = ( parseFloat( $ctrl.sum.toFixed(2))  ==  $ctrl.externalAmountToDivide);
                }
            }
            else{ //using amount splitting of external amount
                angular.forEach($ctrl.value, function (value) {
                    var computed = (parseFloat(value.amount).toFixed(2) * 100) / parseFloat($ctrl.externalAmountToDivide);
                    value.coefficient =  parseFloat(parseFloat(computed).toFixed(4));
                    value.percentage  =  true;
                    total += parseFloat(value.amount);
      
                });
                $ctrl.sum = total;              
                $ctrl.checkPercentage = true;
                $ctrl.checkAmount  = !$ctrl.externalAmountIsDefined ||( parseFloat( $ctrl.sum.toFixed(2)) ==  $ctrl.externalAmountToDivide);
            }
            
            $ctrl.existsAmountNegative =   _.size(_.flatMap(_.filter($ctrl.value, function (row) {return row.amount<0;})))>0;
            onChange();
        }



        function addRow() {
            
            var newValue = 100 - parseFloat($ctrl.totalPercentage.toFixed(4));
            newValue =parseFloat(newValue.toFixed(4));
            newValue = newValue > 0 ? newValue : 0 ;
            var rootPath    =  $ctrl.rootPathAccountIsDefined ? $ctrl.externalRootPath : undefined; 
            addInternalRow(rootPath, undefined, newValue, "PERCENTAGE", 0, undefined);
            doSum();
        }

        function duplicateRow(row, index) {

            var newValue = 100 - parseFloat($ctrl.totalPercentage.toFixed(4));
            newValue =parseFloat(newValue.toFixed(4));
            newValue = newValue > 0 ? newValue : 0 ;
            var rootPath    =  row.rootPath ? row.rootPath : undefined; 
            var matPath     =  row.materializedPath ? row.materializedPath : undefined; 
            
            addInternalRow(rootPath, matPath, newValue, "PERCENTAGE", 0, index+1);
            doSum();
        }

        function addInternalRow(rootPath, materializedPath, coefficient, amountType, amount, position) {
            
            var newRow = {"rootPath": rootPath, "rootPathIsDefined" : angular.isDefined(rootPath),  "materializedPath": materializedPath, 
            "coefficient": coefficient, "type" : $ctrl.writingType ,  "amount" :  angular.isDefined(amount) ?  amount : 0 , "percentage" : amountType ===  'PERCENTAGE'};
            var newPosition = position? position : $ctrl.value.length;
            $ctrl.value.splice(newPosition, 0, newRow);
        }

        function changeRowType(row){
            
            if (row.percentage && $ctrl.value.length==1){
                row.coefficient = 100;
            }
            else 
                row.coefficient = 0;
            
            row.amount = 0;
            doSum();
        }
        

        function removeRow(index) {
            $ctrl.value.splice(index,1);

            if ($ctrl.value.length==1){
                
                $ctrl.value[0].coefficient=$ctrl.value[0].percentage ? 100 : 0;
                if ($ctrl.externalAmountIsDefined) 
                    $ctrl.value[0].amount = $ctrl.externalAmountToDivide;
                $ctrl.usingPercentage = true;    
            }    
            doSum();
            onChange();
        }
        
        function setUsingPercentage(value){
            $ctrl.usingPercentage = value;
            doSum();
            onChange()
        }

        function fnCheckPercentage()
        {
            return  !$ctrl.externalAmountIsDefined  || $ctrl.checkPercentage;
        }

        function fnCheckSum()
        {
            return  !$ctrl.externalAmountIsDefined  || $ctrl.checkAmount;
        }
       
        function fnCheckAmountPositive(value){
            return !$ctrl.existsAmountNegative;
        }


    }

 







})();

