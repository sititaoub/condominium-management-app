/*
 * amount-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.condominiums')
        .component('invoiceAmount', {
            controller: AmountListController,

            bindings: {
                ngModel: '<',
                companyId: '<',
                rootPath: '<',
                transaction: '<',
                simplified: '<',
                withholdingEnabled: '<',
                facilitatedSupplier: '<',
                invoiceDate: '<',
                financialperiodId: '<',
                totalAmount: '='
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/accounting-transaction/component/invoice-amount.html'
        });

    AmountListController.$inject = ['ivaRates', 'withHoldingRates'];

    /* @ngInject */
    function AmountListController(ivaRates, withHoldingRates) {
        var $ctrl = this;
        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.onChangeWithholding = onChangeWithholding;
        $ctrl.$onChanges = bindingOnChange;
        $ctrl.value = undefined;
        $ctrl.doSum = doSum;
        $ctrl.addRow = addRow;
        $ctrl.removeRow = removeRow;
        $ctrl.ivaRates = ivaRates;
        $ctrl.withHoldingRates = withHoldingRates;
        ////////////

        function onInit() {

            $ctrl.ngModelCtrl.$render = function () {

                $ctrl.value              = $ctrl.ngModelCtrl.$viewValue;
                
                if (angular.isUndefined($ctrl.value) || !angular.isArray($ctrl.value.amountRow) || $ctrl.value.amountRow.length == 0) {
                    $ctrl.value = { totalAmount: 0, totalTax: 0, totalTaxable: 0, totalWithholdingTaxable: 0, withholdingToPay: 0, totalExemptIva: 0, totalNotSubjectIva: 0 };
                    $ctrl.value.amountRow = new Array();
                    $ctrl.value.invoiceDate  = $ctrl.invoiceDate;
                }
                else
                    doSum(false);

                $ctrl.updateEnabled = true;
            };

            $ctrl.loading = false;
            $ctrl.initialized = true;
        }

        function bindingOnChange(changes) {

            if ($ctrl.initialized) {
                if (changes.withholdingEnabled) {

                    if (!changes.withholdingEnabled.currentValue) {
                        $ctrl.value.withholdingToPay = 0;
                    }    
                }

                if (changes.invoiceDate){
                     $ctrl.value.invoiceDate  = $ctrl.invoiceDate;
                }

                if (changes.facilitatedSupplier) {
                    if (changes.facilitatedSupplier.currentValue) {
                        angular.forEach($ctrl.value.amountRow, function (value) {
                            value.selectedIva = 'NOT_SUBJECT';
                            value.subjectedWithholding = false;
                        });

                        $ctrl.value.withholdingRate = undefined;
                        doSum(true);
                    }
                }
                if (changes.simplified){
                    onChangeSimplified();
                }
                
            }
        }


        function onChange() {
            doSum(true);
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }


        function onChangeWithholding() {
            $ctrl.totalAmount = $ctrl.value.netAmount + $ctrl.value.withholdingToPay;
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function onChangeSimplified() {
            if ($ctrl.simplified)
                $ctrl.value.amountRow.splice(0, $ctrl.value.amountRow.length);
            else
                addRow();
            
            onChange();
        }


        function doSum(reloadWithholdingAmount) {
            var total = 0;
            var taxable = 0;
            var tax = 0;
            var amount = 0;
            var exemptIva = 0;
            var withholdingToPay = 0;
            var totalWithholdingTaxable = 0;
            var notSubjectIva = 0;
            $ctrl.value.withholdingRate = undefined;
             
            if ($ctrl.withholdingEnabled){
                var selectedWithholdingJson =  _.find( $ctrl.withHoldingRates, function (obj) { return obj.label === $ctrl.value.selectedWithholding; });
                $ctrl.value.withholdingRate =  angular.isDefined(selectedWithholdingJson) ? selectedWithholdingJson.tax : 0;
            }
             
            angular.forEach($ctrl.value.amountRow, function (value) {
                if (angular.isDefined(value.amount))  {
                    var selectedIvaJson = _.find( $ctrl.ivaRates, function (obj) { return obj.id === value.selectedIva; });

                    if (angular.isDefined(selectedIvaJson)){
                        value.rate = selectedIvaJson.tax;
                        value.exemptIva = selectedIvaJson.exempt ? value.amount : 0;
                        value.notSubjectIva = selectedIvaJson.notSubject ? value.amount : 0;
                        value.type = selectedIvaJson.label;
                    }else{
                        value.rate = 0;
                        value.exemptIva = false;
                        value.notSubjectIva = false;
                    }

                    value.tax = parseFloat((value.amount * value.rate / 100));
                    value.taxable = value.rate === 0 ? 0 : value.amount;
                    value.withholdingRate = value.subjectedWithholding ? $ctrl.value.withholdingRate : 0;
                    value.withholdingToPay = angular.isDefined($ctrl.value.withholdingRate) ? parseFloat((value.amount * value.withholdingRate / 100)) : 0;
                    value.total = parseFloat(value.tax + value.amount);
                }

                totalWithholdingTaxable += value.subjectedWithholding ? value.amount : 0;
                withholdingToPay += angular.isDefined(value.withholdingToPay) ? value.withholdingToPay : 0;
                amount  += angular.isDefined(value.amount) ? value.amount : 0;
                taxable += angular.isDefined(value.taxable) ? value.taxable : 0;
                notSubjectIva += angular.isDefined(value.notSubjectIva) ? value.notSubjectIva : 0;
                exemptIva += angular.isDefined(value.exemptIva) ? value.exemptIva : 0;
                tax += angular.isDefined(value.tax) ? value.tax : 0;
                total += angular.isDefined(value.total) ? value.total : 0;
            });


            $ctrl.value.totalWithholdingTaxable = parseFloat(totalWithholdingTaxable.toFixed(2));
            
            $ctrl.value.totalExemptIva = parseFloat(exemptIva.toFixed(2));
            $ctrl.value.totalNotSubjectIva = parseFloat(notSubjectIva.toFixed(2));
            $ctrl.value.totalAmount = parseFloat(amount.toFixed(2));
            $ctrl.value.totalTaxable = parseFloat(taxable.toFixed(2));
            $ctrl.value.totalTax = parseFloat(tax.toFixed(2));
            $ctrl.value.total    = parseFloat(total.toFixed(2));
            
            if (reloadWithholdingAmount){
                $ctrl.value.withholdingToPay = parseFloat(withholdingToPay.toFixed(2));
                $ctrl.value.netAmount = $ctrl.value.total - $ctrl.value.withholdingToPay;
            }
            $ctrl.totalAmount = $ctrl.value.total;
        }

        function addRow() {

            var newRow =
                {
                    "amount": undefined,
                    "selectedIva": $ctrl.facilitatedSupplier ?  'NOT_SUBJECT': undefined,
                    "iva": 0,
                    "subjectedWithholding": !$ctrl.facilitatedSupplier,
                    "total": undefined
                };
            $ctrl.value.amountRow.push(newRow);
            onChange();
        }

        function removeRow(index) {
            $ctrl.value.amountRow.splice(index, 1);
            onChange();
        }


    }









})();

