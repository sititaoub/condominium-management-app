/*
 * amount-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.condominiums')
        .component('fiscalDetail', {
            controller: Detail770Controller,
            
            bindings: {
                ngModel: '<',
                companyId: '<',
                rootPath: '<',
                transaction: '<',
                exemptAmountInput: "<",
                otherExemptAmountInput: "<",
                taxableAmountInput: "<",
                withholdingToPayInput: "<"
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/accounting-transaction/component/fiscal-detail.html'
        });

        Detail770Controller.$inject = ['InstalmentTransactionService', 'AccountingTransactionService', 'NotifierService' ];

    /* @ngInject */
    function Detail770Controller(InstalmentTransactionService, AccountingTransactionService, NotifierService) {
        var $ctrl = this;
        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.$onChanges = bindingOnChange;
        $ctrl.value = undefined;
    
        ////////////

        function onInit() {
            copyAmount();
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
                $ctrl.updateEnabled = true;
            };

            $ctrl.loading = false;
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function bindingOnChange(changes) {

            if (changes.taxableAmountInput || changes.exemptAmountInput
            || changes.otherExemptAmountInput || changes.withholdingToPayInput){
                copyAmount();
            }

        }
        

        function copyAmount() {

            $ctrl.lordAmount           = readFloat($ctrl.taxableAmountInput) + readFloat($ctrl.otherExemptAmountInput);   
            $ctrl.otherExemptAmount    = readFloat($ctrl.otherExemptAmountInput);    

            if ($ctrl.withholdingToPay == 0)
              $ctrl.otherExemptAmount += readFloat($ctrl.taxableAmountInput);

            $ctrl.taxableAmount        = readFloat($ctrl.taxableAmountInput);    
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }


        function readFloat(float){
            return angular.isDefined(float) ? parseFloat(float): 0;
        }



    }









})();

