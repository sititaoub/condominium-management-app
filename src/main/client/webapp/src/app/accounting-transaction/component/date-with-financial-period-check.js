/*
 * technician-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.condominiums')
        .component('dateWithFinancialPeriodCheck', {
            controller: DateWithFinancialPeriodCheck,
            bindings: {
                ngModel: '<',
                companyId: '<',
                ngDisabled: '<?',
                ngRequired: '<?',
                title: '@',
                checkGreaterStartDate: '<?',
                checkLowestEndDate: '<?',
                checkDateRange: '<?',
                externalInitialBindings: '<?',
                financialPeriod: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/accounting-transaction/component/date-with-financial-period-check.html'
        });

    DateWithFinancialPeriodCheck.$inject = ['$attrs'];

    /* @ngInject */
    function DateWithFinancialPeriodCheck($attrs) {
        var $ctrl = this;
        $ctrl.$onInit = onInit;
        $ctrl.initialized = false;
        $ctrl.onChange = onChange;
        $ctrl.$onChanges = bindingOnChange;
        $ctrl.fnCheckEndDate = fnCheckEndDate;
        $ctrl.fnCheckStartDate = fnCheckStartDate;
        $ctrl.fnCheckDateRange = fnCheckDateRange;
        $ctrl.value = undefined;
        ////////////


        function onInit() {

            $ctrl.ngModelCtrl.$render = function () {

                $ctrl.value = angular.isUndefined($ctrl.ngModelCtrl.$viewValue) ? null : $ctrl.ngModelCtrl.$viewValue;
                $ctrl.initialized = true;
                $ctrl.startDatePeriod = $ctrl.financialPeriod.period.startDate;
                $ctrl.endDatePeriod = $ctrl.financialPeriod.period.endDate;

            };

            $ctrl.dateIsOpen = false;


        }

        function onChange() {

            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));

        }

        function bindingOnChange(changes) {
            if ($ctrl.initialized) {
                if (changes.externalInitialBindings) {

                    if (angular.isDefined($ctrl.externalInitialBindings)) {
                        if (angular.isUndefined($ctrl.value) || $ctrl.value === null) {
                            if (performValidation($ctrl.externalInitialBindings)) {
                                $ctrl.value = angular.copy($ctrl.externalInitialBindings);
                                onChange();
                            }
                        }
                    }

                }

            }
        }

        function performValidation(value) {
            return fnCheckEndDate(value) && fnCheckStartDate(value) && fnCheckDateRange(value);
        }

        function fnCheckEndDate(valTocheck) {
            if (!$ctrl.checkDateRange && $ctrl.checkLowestEndDate && angular.isDefined(valTocheck) && valTocheck != null) {
                var dateToCheck = getDateWithoutTime(valTocheck);
                var returnV = dateToCheck <= getDateWithoutTime($ctrl.endDatePeriod)
                return returnV;
            }
            else return true;
        }

        function fnCheckStartDate(valTocheck) {
            if (!$ctrl.checkDateRange && $ctrl.checkGreaterStartDate && angular.isDefined(valTocheck) && valTocheck != null) {
                var dateToCheck = getDateWithoutTime(valTocheck);
                var returnV = dateToCheck >= getDateWithoutTime($ctrl.startDatePeriod)
                return returnV;
            }
            else return true;
        }

        function fnCheckDateRange(valTocheck) {
            if ($ctrl.checkDateRange && angular.isDefined(valTocheck) && valTocheck != null) {
                var dateToCheck = getDateWithoutTime(valTocheck);
                return dateToCheck >= getDateWithoutTime($ctrl.startDatePeriod) && dateToCheck <= getDateWithoutTime($ctrl.endDatePeriod)
            }
            else return true;

        }


        function getDateWithoutTime(date) {
            var dateToReturn = new Date(date);
            dateToReturn.setHours(0, 0, 0, 0)
            return dateToReturn;
        }

    }









})();

