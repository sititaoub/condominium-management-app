/*
 * accounting-transaction.module.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction', [
            'condominiumManagementApp.core',
            'condominiumManagementApp.commons'
        ]);

})();
