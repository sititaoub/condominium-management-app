/*
 * group-members-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('groupMembersInput', {
            controller: GroupMembersInputController,
            bindings: {
                ngModel: '<',
                condominium: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/persons/group-members-input.html'
        });

    GroupMembersInputController.$inject = ['_'];

    /* @ngInject */
    function GroupMembersInputController(_) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.onValueChange = onValueChange;
        $ctrl.onValueTypeChange = onValueTypeChange;
        $ctrl.addRow = addRow;
        $ctrl.removeRow = removeRow;
        $ctrl.getCurrentFraction = getCurrentFraction;
        $ctrl.getSumMinValue = getSumMinValue;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
                calculateSum();
            };
        }

        function onValueTypeChange() {
            onChange();
        }

        function onValueChange() {
            calculateSum();
            onChange();
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function calculateSum() {
            $ctrl.sum = _.sumBy($ctrl.value.members, 'value');
        }

        function addRow() {
            $ctrl.value.members.push({ value: 0 });
            calculateSum();
            onChange();
        }

        function removeRow(index) {
            $ctrl.value.members.splice(index, 1);
            calculateSum();
            onChange();
        }

        function getCurrentFraction() {
            return $ctrl.value && $ctrl.value.valueType === 'PERCENT' ? 4 : 0;
        }

        function getSumMinValue() {
            return $ctrl.value && $ctrl.value.valueType === 'PERCENT' ? 100 : 0;
        }
    }

})();

