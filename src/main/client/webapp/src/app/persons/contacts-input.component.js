/*
 * contacts-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('contactsInput', {
            controller: ContactsInputController,
            bindings: {
                ngModel: '<',
                ngRequired: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/persons/contacts-input.html'
        });

    ContactsInputController.$inject = ['PersonsService'];

    /* @ngInject */
    function ContactsInputController(PersonsService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            PersonsService.allCommunicationTypes().then(function (allTypes) {
                $ctrl.allCommunicationTypes = allTypes;
            });
            PersonsService.traceableCommunicationTypes().then(function (traceableTypes) {
                $ctrl.traceableCommunicationTypes = traceableTypes;
            });

            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

