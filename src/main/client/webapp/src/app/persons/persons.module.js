(function () {
    'use strict';

    angular
        .module("condominiumManagementApp.person", [
            'condominiumManagementApp.core',
            'condominiumManagementApp.commons'
        ]);

})();