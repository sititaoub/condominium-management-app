/*
 * person-group-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('personGroupEdit', {
            controller: PersonGroupEditController,
            bindings: {
                'condominium': '<',
                'group': '<'
            },
            templateUrl: 'app/persons/person-group-edit.html'
        });

    PersonGroupEditController.$inject = ['$state', 'PersonGroupService', 'NotifierService'];

    /* @ngInject */
    function PersonGroupEditController($state, PersonGroupService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.groupId = $ctrl.group.master.id;
        }

        function doSave() {
            PersonGroupService.update($ctrl.groupId, $ctrl.group).then(function () {
                return NotifierService.notifySuccess('Persons.Group.Edit.Success');
            }).then(function () {
                $state.go("^");
            }).catch(function () {
                return NotifierService.notifyError('Persons.Group.Edit.Failure');
            });
        }
    }

})();

