/*
 * persons.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .factory('PersonsService', PersonsService);

    PersonsService.$inject = ['Restangular', 'contextAddressPeople', '_'];

    /* @ngInject */
    function PersonsService(Restangular, contextAddressPeople, _) {
        var Persons = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressPeople + 'v1')
            configurer.addResponseInterceptor(function (data, operation, what, url, response) {
                if (operation === "post") {
                    var location = response.headers('Location');
                    return location.substr(location.lastIndexOf('/') + 1);
                }
                return data;
            });
        }).all('persons');
        var service = {
            getPage: getPage,
            getOne: getOne,
            allCommunicationTypes: allCommunicationTypes,
            traceableCommunicationTypes: traceableCommunicationTypes,
            create: create,
            update: update,
            remove: remove
        };
        return service;

        ////////////////

        /**
         * Persons page.
         *
         * @typedef {Object} Persons~PersonsPage
         * @property {number} number - The requested page number.
         * @property {number} size - The requested page size.
         * @property {number} totalElements - The total number of elements.
         * @property {number} totalPages - The total number of pages.
         * @property {Persons~Person[]} content - The page content.
         */

        /**
         * Person object.
         *
         * @typedef {Object} Persons~Person
         * @property {number} id - The unique id of person.
         * @property {string} taxCode - The tax code.
         * @property {string} vatNumber - The vat number.
         * @property {string} companyName - The company name.
         * @property {string} firstName - The first name.
         * @property {string} lastName - The last name.
         * @property {string} streetName - The address street name.
         * @property {string} buildingNumber - The address building number.
         * @property {string} town - The address town.
         * @property {string} district - The address district.
         * @property {string} province - The address province.
         */

        /**
         * Retrieve a single page of person for given condominium id.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=10] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<Persons~PersonPage|Error>} - The promise of page.
         */
        function getPage(condominiumId, page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                condominiumId: condominiumId,
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return Persons.get("", params);
        }

        /**
         * Retrieve a single person.
         *
         * @param {number} id - The unique person id.
         * @returns {Promise<Persons~Person|Error>} - The promise of page.
         */
        function getOne(id) {
            return Persons.one("", id).get();
        }

        /**
         * Retrieve a list of all communication types.
         *
         * @return {Promise<String[]|Error>} - The promise.
         */
        function allCommunicationTypes() {
            return Persons.all("communication-types").all("all").getList();
        }

        /**
         * Retrieve a list of all traceable communication types.
         *
         * @return {Promise<String[]|Error>} - The promise.
         */
        function traceableCommunicationTypes() {
            return Persons.all("communication-types").all("traceable").getList();
        }

        /**
         * Create a new person.
         *
         * @param {Persons~Person} person - The person to be created.
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function create(person) {
            return Persons.post(person);
        }

        /**
         * Updates an existing person.
         *
         * @param {number} id - The unique id of person to update.
         * @param {Persons~Person} person - The person to be updated
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function update(id, person) {
            return Persons.one("", id).customPUT(person);
        }

        /**
         * Remove the person with supplied id.
         *
         * @param {number} id - The unique id of person
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function remove(id) {
            return Persons.one("", id).remove();
        }
    }

})();

