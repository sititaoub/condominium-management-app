/*
 * persons-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('personsList', {
            controller: PersonsListController,
            bindings: {
                'condominium': '<'
            },
            templateUrl: 'app/persons/persons-list.html'
        });

    PersonsListController.$inject = ['$state', '$filter', 'NgTableParams', 'PersonsService', 'PersonGroupService', 'NotifierService', 'AlertService'];

    /* @ngInject */
    function PersonsListController($state, $filter, NgTableParams, PersonsService, PersonGroupService, NotifierService, AlertService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.toggleFilters = toggleFilters;
        $ctrl.deleteGroup = deleteGroup;
        $ctrl.deletePerson = deletePerson;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10,
                sort: 'taxCode,asc'
            }, {
                getData: loadPersons
            }, 'p_');
            $ctrl.showFilters = $ctrl.tableParams.hasFilter();
            $ctrl.groupFilter = [{
                id: false,
                title: $filter('translate')('Persons.List.Groups.People')
            }, {
                id: true,
                title: $filter('translate')('Persons.List.Groups.Groups')
            }];
        }

        function toggleFilters() {
            $ctrl.showFilters = !$ctrl.showFilters;
        }

        function deleteGroup(group) {
            AlertService.askConfirm('Persons.Group.Remove.Message', group).then(function () {
                PersonGroupService.remove(group.id).then(function () {
                    return NotifierService.notifySuccess('Persons.Group.Remove.Success');
                }).catch(function () {
                    return NotifierService.notifyError('Persons.Group.Remove.Failure');
                }).finally(function () {
                    $ctrl.tableParams.reload();
                })
            });
        }

        function deletePerson(person) {
            AlertService.askConfirm('Persons.Remove.Message', person).then(function () {
                PersonsService.remove(person.id).then(function () {
                    return NotifierService.notifySuccess('Persons.Remove.Success');
                }).catch(function () {
                    return NotifierService.notifyError('Persons.Remove.Failure');
                }).finally(function () {
                    $ctrl.tableParams.reload();
                })
            });
        }

        function loadPersons(params) {
            $state.go('.', params.uiRouterUrl('p_'), { inherit: false });
            return PersonsService.getPage($ctrl.condominium.id, params.page() - 1, params.count(), params.sorting(), params.filter())
                .then(function (data) {
                    params.total(data.totalElements);
                    return data.content;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    $ctrl.loading = false;
                });
        }
    }

})();

