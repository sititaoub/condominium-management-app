/*
 * person-picker-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('personPicker', {
            controller: PersonPickerController,
            bindings: {
                close: '&',
                dismiss: '&',
                resolve: '<'
            },
            templateUrl: 'app/persons/person-picker.html'
        });

    PersonPickerController.$inject = ['NgTableParams', 'PersonsService', 'NotifierService'];

    /* @ngInject */
    function PersonPickerController(NgTableParams, PersonsService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doPick = doPick;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.tableParams = new NgTableParams({
                page: 1,
                size: 10,
                filter: {
                    groups: $ctrl.resolve.includeGroups,
                },
                sort: 'taxCode,asc'
            }, {
                getData: loadPersons
            });
        }

        function doPick(value) {
            $ctrl.close({ $value: value });
        }

        function loadPersons(params) {
            return PersonsService.getPage(
                $ctrl.resolve.condominiumId,
                params.page() - 1,
                params.count(),
                params.sorting(),
                params.filter()
            ).then(function (data) {
                params.total(data.totalElements);
                return data.content;
            }).catch(function () {
                NotifierService.notifyError();
                $ctrl.dismiss();
            }).finally(function () {
                $ctrl.loading = false;
            });
        }
    }

})();

