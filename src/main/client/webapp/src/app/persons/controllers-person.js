
"use strict";


angular.module("condominiumManagementApp.person")
	.controller("person.FormCtrl", [
    "$scope",
    "$rootScope",
    "$state",
    "$stateParams",
    "$filter",
    "toaster",
    "LookupService",
    "PersonCrudResource",
    "UtilsService",
    "SweetAlert",
    function ($scope, $rootScope, $state, $stateParams, $filter, toaster,
              LookupService,PersonCrudResource,UtilsService,SweetAlert) {
        $scope.condominiumId = $stateParams.condominiumId || $stateParams.id;
        $scope.personId = $stateParams.personId;
        $scope.description = $stateParams.description;

        $scope.back = function(){
            $state.go('^.list',{condominiumId: $scope.condominiumId});
            //UtilsService.backNav();
        };

        //Se è un Edit
        if ($scope.personId != 'new') {

            $scope.formData = PersonCrudResource.get({id: $scope.personId}, function (data) {

                $scope.description= data.companyName ? data.companyName : data.firstName +" "+data.lastName;

                $scope.district.selected={
                    'name':data.address.district,
                    'province':data.address.province
                };
            });

        } else { //Se è un nuovo inserimento

            $scope.formData = {
                contacts: {},
                address:{
                    "country": "IT"
                }
            };


        }


        //Datepicker
        $scope.format="dd-MM-yyyy";
        $scope.popupFrom = { opened: false  };
        $scope.openFrom = function() { $scope.popupFrom.opened = true;  };


        //District
        $scope.district={selected:""};
        $scope.oldQuery="";

        $scope.onSelectDistrictCallback= function(item, model){
            $scope.formData.address.province=item.province;
            $scope.formData.address.district=item.name;

        }

        $scope.funcAsync = function (query) {
            /*La ricerca su 8000 comuni è molto lenta di conseguenza queste casistiche servono a ridurre
             al minimo il numero di chiamate
             */
            var strQuery= query.substr(0,3);
            var strOldQuery= $scope.oldQuery.substr(0,3);

            if (((strQuery!=strOldQuery)&&(query.length>2)))
            {
                LookupService.getDistricts(query).then(function (data) {

                    $scope.districtsLookup = data.data;
                });
            }

            if (query.length<=3)
            {
                $scope.districtsLookup=[];
            }

            $scope.oldQuery=query;
        };

        $scope.submitForm = function(form) {
            if (form.$valid) {

                if ($scope.formData.address.postalCode=="")  $scope.formData.address.postalCode=null;
                if ($scope.formData.address.province=="")  $scope.formData.address.province=null;
                $scope.formData.condominiumId = $scope.condominiumId;



                if ($scope.personId=='new') {

                    $scope.newPerson = new PersonCrudResource($scope.formData);

                    PersonCrudResource.save($scope.newPerson,
                        function(hUnit, headers){
                            toaster.pop({
                                type: 'success',
                                title: 'Salvataggio effettuato',
                                showCloseButton: true,
                                timeout: 2000
                            });
                            $scope.personId = headers('location').split("/").pop();
                            $state.go('^.list',{condominiumId: $scope.condominiumId});
                        },
                        function(){
                            toaster.pop({
                                type: 'error',
                                title: 'Si è vericato un problema durante il salvataggio',
                                showCloseButton: true,
                                timeout: 10000
                            });
                        });
                } else {
                    $scope.formData.$update({id: $scope.personId},
                        function(hUnit){
                            toaster.pop({
                                type: 'success',
                                title: 'Salvataggio effettuato',
                                showCloseButton: true,
                                timeout: 2000
                            });
                            $state.go('^.list',{condominiumId: $scope.condominiumId});

                        },
                        function(){
                            toaster.pop({
                                type: 'error',
                                title: 'Si è vericato un problema durante il salvataggio',
                                showCloseButton: true,
                                timeout: 10000
                            });
                        });
                }

            } else {
                form.submitted = true;
            }
        }
    }


]);


