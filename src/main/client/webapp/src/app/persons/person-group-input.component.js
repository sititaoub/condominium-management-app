/*
 * person-group-input.directive.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('personGroupInput', {
            controller: PersonGroupInputController,
            bindings: {
                condominium: '<',
                ngModel: '<'

            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/persons/person-group-input.html'
        });

    PersonGroupInputController.$inject = [];

    /* @ngInject */
    function PersonGroupInputController() {
        var $ctrl = this;

        this.$onInit = onInit;
        this.onChange = onChange;

        ////////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue || {
                    master: {
                        condominiumId: $ctrl.condominium.id,
                        contacts: {},
                        additionalContacts: {},
                        postalAddress: angular.copy($ctrl.condominium.address),
                        residenceAddress: angular.copy($ctrl.condominium.address)
                    },
                    valueType: 'PERCENT',
                    members: [{ value: 0 }, { value: 0 }]
                };
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

