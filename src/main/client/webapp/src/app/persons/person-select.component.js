/*
 * person-select.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('personSelect', {
            controller: PersonSelectController,
            bindings: {
                id: '@',
                name: '@',
                class: '@',
                ngModel: '<',
                ngDisabled: '<',
                condominiumId: '<',
                includeGroups: '<?'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/persons/person-select.html'
        });

    PersonSelectController.$inject = ['$uibModal', 'PersonsService', '$filter', '_'];

    /* @ngInject */
    function PersonSelectController($uibModal, PersonsService, $filter, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.openPicker = openPicker;
        $ctrl.getPersons = getPersons;
        $ctrl.onSelect = onSelect;
        $ctrl.onChange = onChange;
        $ctrl.clearValue = clearValue;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.fullName = $filter('fullName')($ctrl.ngModelCtrl.$viewValue);
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }
        
        function openPicker() {
            $uibModal.open({
                component: 'personPicker',
                size: 'lg',
                resolve: {
                    condominiumId: _.constant($ctrl.condominiumId),
                    includeGroups: _.constant($ctrl.includeGroups)
                }
            }).result.then(onSelect);
        }

        function getPersons(name) {
            return PersonsService.getPage($ctrl.condominiumId, 0, 100, {}, { name: name }).then(function(page) {
                return page.content;
            });
        }

        function onSelect(person) {
            $ctrl.fullName = $filter('fullName')(person);
            $ctrl.value = person;
            onChange();
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function clearValue() {
            $ctrl.value = undefined;
            $ctrl.fullName = undefined;
            onChange();
        }
    }

})();

