
"use strict";

angular.module("condominiumManagementApp.person").controller("person.ListCtrl", [
    "$scope",
    "$state",
    "$stateParams",
    "$timeout",
    "toaster",
    "DTOptionsBuilder",
    "DTColumnBuilder",
    "SweetAlert",
    "PersonResource",
    "PersonCrudResource",
    "TableService",
    function ($scope, $state, $stateParams, $timeout, toaster,
              DTOptionsBuilder, DTColumnBuilder, SweetAlert,
              PersonResource, PersonCrudResource, TableService) {


        TableService.getTableOptions($scope);

        $scope.condominiumId = $stateParams.condominiumId
        /**
         * persons - Data used in Tables view for Data Tables plugin
         */
        var loadPersons = function(){
            $scope.listPersonsLoading = true;
            $scope.persons =  PersonResource.query({condominiumId: $scope.condominiumId, page:"0",size:"500"}
                ,function(data) {
                    $scope.listPersonsLoading = false;

                },function(data) {
                    $scope.listPersonsLoading = false;

                });
        }
        loadPersons();

        $scope.deletePerson= function(personId) {

            SweetAlert.swal({
                    title: "Vuoi davvero eliminare la persona?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    animation: "slide-from-top"
                },
                function (isConfirm) {
                    if (isConfirm) {
                        PersonCrudResource.remove({id: personId},
                            function(response){
                                toaster.pop({
                                    type: 'success',
                                    title: 'Eliminazione effettuata',
                                    showCloseButton: true,
                                    timeout: 2000
                                });
                                // redirect putted into event handler
                                /*
                                $state.go('.', $stateParams, {reload:true})
                                */
                                loadPersons();
                            },
                            function(response){
                                toaster.pop({
                                    type: 'error',
                                    title: $filter('translate')(response.data.userMessage),
                                    showCloseButton: true,
                                    timeout: 10000
                                });
                            });
                    }
                });
        };

    }


]);
