/*
 * identification-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('identificationInput', {
            controller: IdentificationInputController,
            bindings: {
                'ngModel': '<'
            },
            templateUrl: 'app/persons/identification-input.html',
            require: {
                'ngModelCtrl': 'ngModel'
            }
        });

    IdentificationInputController.$inject = [];

    /* @ngInject */
    function IdentificationInputController() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

