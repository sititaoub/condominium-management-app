/*
 * group-identification-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('groupIdentificationInput', {
            controller: GroupIdentificationInputController,
            bindings: {
                ngModel: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/persons/group-identification-input.html'
        });

    GroupIdentificationInputController.$inject = ['PersonGroupService'];

    /* @ngInject */
    function GroupIdentificationInputController(PersonGroupService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            PersonGroupService.allManagementTypes().then(function (managementTypes) {
                $ctrl.managementTypes = managementTypes;
            });
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

