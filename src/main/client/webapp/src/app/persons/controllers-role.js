
"use strict";


angular.module("condominiumManagementApp.person")
	.controller("person.RoleCtrl", [
    "$scope",
    "$rootScope",
    "$state",
    "$stateParams",
    "$filter",
    "$uibModal",
    "toaster",
    "LookupService",
    "RoleCrudResource",
    "RoleService",
    "PersonService",
    "UtilsService",
    "SweetAlert",
    function ($scope, $rootScope, $state, $stateParams, $filter, $uibModal, toaster,
              LookupService, RoleCrudResource, RoleService,
              PersonService, UtilsService, SweetAlert) {

        $scope.condominiumId = $stateParams.condominiumId || $stateParams.id;
        $scope.roleId = $stateParams.roleId;
        $scope.huId = $stateParams.huId;
        $scope.description = $stateParams.description;                        
        
        $scope.back = function(){
            $scope.$emit("backRole", {id:$scope.roleId, huId:$scope.huId});
        };

        RoleService.getRoleType().then(function(data){
            $scope.roleTypes = data.data
        },function(){
            toaster.pop({
                type: 'error',
                title: $filter('translate')('error.get.role.type'),
                showCloseButton: true,
                timeout: 10000
            });
        });

        //Se è un Edit
        if ($scope.roleId != 'new') {
            $scope.$emit("selectRole", {id:$scope.roleId, huId:$scope.huId});

            $scope.formData = RoleCrudResource.get({id: $scope.roleId}, function (data) {
                $scope.formData.from = moment(data.from).valueOf()
                $scope.formData.to = moment(data.to).valueOf()

                $scope.description = $filter('translate')(data.roleType) +" "+
                    data.rolePercentage+"%";
                if(!$scope.person) {
                    $scope.person = {selected: ""};
                }
                $scope.person.selected = data.person;
            });

        } else { //Se è un nuovo inserimento
            $scope.$emit("preaddRole", {huId:$scope.huId});
            $scope.formData = {
                housingUnitId: $scope.huId
            }
        }


        $scope.fromDatePicker = UtilsService.datePicker();
        $scope.toDatePicker = UtilsService.datePicker();

        $scope.onSelectPerson= function(item, model){
            $scope.formData.personId = item.id;
            $scope.formData.lastName = item.lastName;
            $scope.formData.firstName = item.firstName;

        }

        $scope.searchPersonAsync = function (query) {            
            // init var
            if(!$scope.oldQuery || !$scope.person){                
                $scope.oldQuery = '';
                $scope.person = { selected:"" };                                
            }

            /* ricerca lenta queste casistiche servono a ridurre
             al minimo il numero di chiamate
             */
            var strQuery= query.substr(0,2);
            var strOldQuery= $scope.oldQuery.substr(0,2);

            if (((strQuery!=strOldQuery)&&(query.length>1))) {
                PersonService.queryPersons($scope.condominiumId,query).then(function (data) {
                    $scope.persons = data.data.content;
                },function(){
                    toaster.pop({
                        type: 'error',
                        title: $filter('translate')('error.get.role.type'),
                        showCloseButton: true,
                        timeout: 10000
                    });

                });
            }

            if (query.length<=2) {
                $scope.persons=[];
            }
            $scope.oldQuery=query;
        };

        $scope.submitForm = function(form) {
            if (form.$valid) {
                $scope.formData.condominiumId = $scope.condominiumId;

                if ($scope.roleId=='new') {
                    $scope.newRole = new RoleCrudResource($scope.formData);

                    RoleCrudResource.save($scope.newRole,
                        function(role, headers){
                            toaster.pop({
                                type: 'success',
                                title: 'Salvataggio effettuato',
                                showCloseButton: true,
                                timeout: 2000
                            });
                            /*
                            $scope.description = $filter('translate')(role.roleType)+" "+
                                role.rolePercentage+"%";*/
                            role.id = headers('location').split("/").pop();
                            $scope.$emit("addRole", role);

                        },
                        function(response){
                            /*
                            if (response.status=="404" && response.statusText=="Not Found"){
                                var textError='Il Seriale inserito non è corretto';
                            } else */
                            var textError='Si è vericato un problema durante il salvataggio';

                            toaster.pop({
                                type: 'error',
                                title: textError,
                                showCloseButton: true,
                                timeout: 10000
                            });
                        });
                } else {
                    $scope.formData.$update({id: $scope.roleId},
                        function(role){
                            toaster.pop({
                                type: 'success',
                                title: 'Salvataggio effettuato',
                                showCloseButton: true,
                                timeout: 2000
                            });
                            /*$scope.description = $filter('translate')(role.roleType)+" "+
                                role.rolePercentage+"%";*/
                            $scope.$emit("editRole", role);

                        },
                        function(response){
                            var textError='Si è vericato un problema durante il salvataggio';

                            toaster.pop({
                                type: 'error',
                                title: textError,
                                showCloseButton: true,
                                timeout: 10000
                            });
                        });
                }

            } else {
                form.submitted = true;
            }
        }

        $scope.delete= function() {

            SweetAlert.swal({
                    title: $filter('translate')("role.delete.confirm"),
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    animation: "slide-from-top"
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $scope.formData.$remove({id: $scope.roleId},
                            function(response){
                                toaster.pop({
                                    type: 'success',
                                    title: 'Eliminazione effettuata',
                                    showCloseButton: true,
                                    timeout: 2000
                                });
                                $scope.$emit("removeRole", {id: $scope.roleId});
                                // redirect putted into event handler
                            },
                            function(response){
                                toaster.pop({
                                    type: 'error',
                                    title: $filter('translate')(response.data.userMessage),
                                    showCloseButton: true,
                                    timeout: 10000
                                });
                            });
                    }
                });
        };

        $scope.openManagePerson=function(){
            var modalInstance = $uibModal.open({
                templateUrl: 'app/persons/partials/modal-person.html',
                controller:'person.ModalCtrl',
                size: 'lg',
                scope: $scope
            });

        }
    }


]);


