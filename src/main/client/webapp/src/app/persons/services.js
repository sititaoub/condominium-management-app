(function () {
    "use strict";
    angular.module("condominiumManagementApp.person")
        .factory("PersonResource", ["$resource", "contextAddress", function ($resource, contextAddress) {
            return $resource(contextAddress + "v1/persons/?condominiumId=:condominiumId",
                {condominiumId: '@condominiumId'},
                {
                    update: {method: 'PUT'},
                    query: {method: 'GET', isArray: false}
                }
            );
        }])
        .factory("PersonCrudResource", ["$resource", "contextAddress", function ($resource, contextAddress) {
            return $resource(contextAddress + "v1/persons/:id",
                {id: '@id'},
                {
                    update: {method: 'PUT'},
                    query: {method: 'GET', isArray: false}
                }
            );

        }])
        .factory("RoleCrudResource", ["$resource", "contextAddress", function ($resource, contextAddress) {
            return $resource(contextAddress + "v1/housing-unit-person-role/:id",
                {id: '@id'},
                {
                    update: {method: 'PUT'},
                    query: {method: 'GET', isArray: false}
                }
            );

        }])
        .service('PersonService', ['$http', 'contextAddress',
            function ($http, contextAddress) {
                return {
                    queryPersons: function (condominiumId, name) {
                        return $http({
                            method: 'GET',
                            url: contextAddress + 'v1/persons',
                            params: {
                                condominiumId: condominiumId,
                                name: name,
                                size: 2000
                            }
                        });
                    }
                }
            }]);
})();