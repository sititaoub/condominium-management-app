(function () {
    'use strict';

    angular.module("condominiumManagementApp.person")
        .config(function ($stateProvider) {
            $stateProvider.state('index.person', {
                url: "/condominiums/{condominiumId}/person",
                abstract: true,
                redirectTo: 'index.person.list',
                data: {
                    skipFinancialPeriodCheck: true
                },
                template: '<div class="person-container" ui-view></div>',
                resolve: {
                    condominium: ['$transition$', 'CondominiumService', function ($transition$, CondominiumService) {
                        return CondominiumService.getOne($transition$.params().condominiumId);
                    }]
                }
            }).state('index.person.list', {
                url: "/list?p_page&p_count&p_sort&p_groups&p_taxCode&p_vatNumber&p_name",
                component: 'personsList',
                params: {
                    p_page: { dynamic: true, squash: true, value: '1', inherits: true },
                    p_count: { dynamic: true, squash: true, value: '10', inherits: true },
                    p_sort: { dynamic: true, squash: true, value: 'taxCode,asc', inherits: true },
                    p_groups: { dynamic: true, squash: true },
                    p_taxCode: { dynamic: true, squash: true },
                    p_vatNumber: { dynamic: true, squash: true },
                    p_name: { dynamic: true, squash: true }
                }
            }).state('index.person.list.create', {
                url: "/create",
                views: {
                    '@index.person': {
                        component: 'personCreate'
                    }
                }
            }).state('index.person.list.edit', {
                url: "/{personId}/edit",
                views: {
                    '@index.person': {
                        component: 'personEdit'
                    }
                },
                resolve: {
                    person: ['$transition$', 'PersonsService', function ($transition$, PersonsService) {
                        return PersonsService.getOne($transition$.params().personId);
                    }]
                }
            }).state('index.person.list.createGroup', {
                url: '/group/create',
                views: {
                    '@index.person': {
                        component: 'personGroupCreate'
                    }
                }
            }).state('index.person.list.editGroup', {
                url: '/group/{groupId}/edit',
                views: {
                    '@index.person': {
                        component: 'personGroupEdit'
                    }
                },
                resolve: {
                    group: ['$transition$', 'PersonGroupService', function ($transition$, PersonGroupService) {
                        return PersonGroupService.getOne($transition$.params().groupId);
                    }]
                }
            });
        });
})();