(function () {
    "use strict";

    angular.module("condominiumManagementApp.person")
        .config(function ($translatePartialLoaderProvider) {
            $translatePartialLoaderProvider.addPart("persons");
        });
})();