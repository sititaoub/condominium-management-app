/*
 * person-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('personCreate', {
            controller: PersonCreateController,
            bindings: {
                'condominium': '<'
            },
            templateUrl: 'app/persons/person-create.html'
        });

    PersonCreateController.$inject = ['$state', 'PersonsService', 'NotifierService'];

    /* @ngInject */
    function PersonCreateController($state, PersonsService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.person = undefined;
        }

        function doSave() {
            PersonsService.create($ctrl.person).then(function () {
                return NotifierService.notifySuccess('Persons.Create.Success');
            }).then(function () {
                $state.go("^");
            }).catch(function () {
                return NotifierService.notifyError('Persons.Create.Failure');
            });
        }
    }

})();

