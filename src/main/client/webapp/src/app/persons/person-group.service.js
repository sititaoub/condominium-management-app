/*
 * person-group.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .factory('PersonGroupService', PersonGroupService);

    PersonGroupService.$inject = ['Restangular', 'contextAddressPeople', '_'];

    /* @ngInject */
    function PersonGroupService(Restangular, contextAddressPeople, _) {
        var Groups = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressPeople + 'v1')
            configurer.addResponseInterceptor(function (data, operation, what, url, response) {
                if (operation === "post") {
                    var location = response.headers('Location');
                    return location.substr(location.lastIndexOf('/') + 1);
                }
                return data;
            });
        }).all('persons').all('groups');
        var service = {
            getOne: getOne,
            allManagementTypes: allManagementTypes,
            create: create,
            update: update,
            remove: remove
        };
        return service;

        ////////////////

        /**
         * @namespace Persons
         */

        /**
         * Group object.
         *
         * @typedef {Object} Persons~Group
         * @property {number} id - The unique id of person.
         * @property {string} taxCode - The tax code.
         * @property {string} vatNumber - The vat number.
         * @property {string} companyName - The company name.
         * @property {string} firstName - The first name.
         * @property {string} lastName - The last name.
         * @property {string} streetName - The address street name.
         * @property {string} buildingNumber - The address building number.
         * @property {string} town - The address town.
         * @property {string} district - The address district.
         * @property {string} province - The address province.
         */

        /**
         * Retrieve a single group.
         *
         * @param {number} id - The unique person id.
         * @returns {Promise<Persons~Group|Error>} - The promise of page.
         */
        function getOne(id) {
            return Groups.one("", id).get();
        }

        /**
         * Retrieve a list of all communication types.
         *
         * @return {Promise<String[]|Error>} - The promise.
         */
        function allManagementTypes() {
            return Groups.all("management-types").all("all").getList();
        }

        /**
         * Create a new group.
         *
         * @param {Persons~Group} group - The group to be created.
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function create(group) {
            return Groups.post(group);
        }

        /**
         * Updates an existing group.
         *
         * @param {number} id - The unique id of group to update.
         * @param {Persons~Group} group - The group to be updated
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function update(id, group) {
            return Groups.one("", id).customPUT(group);
        }

        /**
         * Remove the group with supplied id.
         *
         * @param {number} id - The unique id of person
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function remove(id) {
            return Groups.one("", id).remove();
        }
    }

})();

