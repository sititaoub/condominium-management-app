/*
 * person-group-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('personGroupCreate', {
            controller: PersonGroupCreateController,
            bindings: {
                condominium: '<'
            },
            templateUrl: 'app/persons/person-group-create.html'
        });

    PersonGroupCreateController.$inject = ['$state', 'PersonGroupService', 'NotifierService'];

    /* @ngInject */
    function PersonGroupCreateController($state, PersonGroupService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.group = undefined;
        }

        function doSave() {
            PersonGroupService.create($ctrl.group).then(function () {
                return NotifierService.notifySuccess('Persons.Group.Create.Success');
            }).then(function () {
                $state.go("^");
            }).catch(function () {
                return NotifierService.notifyError('Persons.Group.Create.Failure');
            });
        }
    }

})();

