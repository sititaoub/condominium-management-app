/*
 * person-input.directive.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('personInput', {
            controller: PersonInputController,
            bindings: {
                condominium: '<',
                ngModel: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/persons/person-input.html'
        });

    PersonInputController.$inject = [];

    /* @ngInject */
    function PersonInputController() {
        var $ctrl = this;

        this.$onInit = onInit;
        this.onChange = onChange;

        ////////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue || {
                        condominiumId: $ctrl.condominium.id,
                        contacts: {},
                        additionalContacts: {},
                        postalAddress: angular.copy($ctrl.condominium.address),
                        residenceAddress: angular.copy($ctrl.condominium.address)
                    };
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

