/*
 * person-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('personEdit', {
            controller: PersonEditController,
            bindings: {
                'condominium': '<',
                'person': '<'
            },
            templateUrl: 'app/persons/person-edit.html'
        });

    PersonEditController.$inject = ['$state', 'PersonsService', 'NotifierService'];

    /* @ngInject */
    function PersonEditController($state, PersonsService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.personId = $ctrl.person.id;
        }

        function doSave() {
            PersonsService.update($ctrl.personId, $ctrl.person).then(function () {
                return NotifierService.notifySuccess('Persons.Edit.Success');
            }).then(function () {
                $state.go("^");
            }).catch(function () {
                return NotifierService.notifyError('Persons.Edit.Failure');
            });
        }
    }

})();

