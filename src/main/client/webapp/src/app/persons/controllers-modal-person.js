
"use strict";


angular.module("condominiumManagementApp.person")
	.controller("person.ModalCtrl", [
    "$scope",
    "$rootScope",
    "$state",
    "$stateParams",
    "$filter",
    function ($scope, $rootScope, $state, $stateParams, $filter) {
        $state.go('index.structure.persons.list',$scope.condominiumId);
    }


]);


