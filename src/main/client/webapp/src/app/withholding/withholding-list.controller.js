/*
 * budget-edit.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.budget')
        .controller('WithholdingListController', WithholdingListController);

    WithholdingListController.$inject = ['$state', 'NgTableParams', 'WithholdingService', 'WithholdingInterest', 'NotifierService',
    'AccountingTransactionService', 'MaterializedPathLookupService', 'FinancialPeriodService', 'AlertService'];

    /* @ngInject */
    function WithholdingListController($state, NgTableParams, WithholdingService, WithholdingInterest, NotifierService, AccountingTransactionService, MaterializedPathLookupService,
        FinancialPeriodService, AlertService) {
        var vm = this;
        vm.condominiumId = $state.params.condominiumId;
        vm.financialPeriodId = $state.params.periodId;
        vm.companyId = $state.params.companyId;
        vm.toggleFilters = toggleFilters;
        vm.openSanctionInterestPopup = openSanctionInterestPopup;
        vm.doExtractFile = doExtractFile;
        vm.doPay = doPay;
        vm.doReset = doReset;
        vm.doCalculateAll = doCalculateAll;
        vm.changeCumulativeFlag = changeCumulativeFlag;
        vm.changePaymentDate = changePaymentDate;
        vm.idRequest = undefined;
        activate();

        ////////////////


        function activate() {
            initializeVar()
            
        }


        function initializeVar() {
            vm.loading = true;
            vm.accountPath = undefined;
            vm.showFilters = false;
            vm.paymentDateToOpened = false;

            vm.cumulativePayment = false;
            vm.tableParams = new NgTableParams();
            vm.tableParams.settings({ counts: [] });
            loadFinancialPeriod();
           

        }

        function loadFinancialPeriod() {
             
            FinancialPeriodService.getOne($state.params.companyId, $state.params.periodId).then(function (period) {
                vm.period = period;
                vm.periodLoaded = true;
                MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, "RESOURCE_PATH").then(function (mathPath) {
                    vm.prefixedResourceRootPath = mathPath.materializedPath;
                });
                MaterializedPathLookupService.getOne($state.params.companyId, vm.period.chartOfAccountsId, "SUPPLIER_PATH").then(function (mathPath) {
                    vm.prefixedSupplierRootPath = mathPath.materializedPath;
                });
                
                loadList();
               
            }).catch(function () {
                NotifierService.notifyError();
            })
        }

        function loadList() {

            WithholdingService.getList(vm.condominiumId, vm.financialPeriodId).then(function (list) {
                vm.firstLoad = false;
                vm.paymentDate = new Date();
                vm.computedInterest = false;


                _.map(list, function (element) {
                    element.paymentDate = vm.paymentDate;
                    element.checked = vm.cumulativePayment ? vm.paymentDate <= new Date(element.withholdingCumulativeExpiryDate) : vm.paymentDate <= new Date(element.withholdingExpiryDate);
                    element.cumulative = vm.cumulativePayment;
                    element.selected = false;
                }
                )
                loadData(list);

            }).catch(function () {
                return NotifierService.notifyError();
            }).finally(function () {
                vm.loading = false;
            });
        }

        function loadData(list) {
            vm.list = list;
            vm.tableParams.settings({ dataset: vm.list });
            vm.loading = false;
        }

        /**
         * Toggle the show/hide status of filters.
         */
        function toggleFilters() {
            vm.showFilters = !vm.showFilters;
        }

        function changeCumulativeFlag() {
            _.map(vm.list, function (el) {
                el.cumulative = vm.cumulativePayment;
                el.checked = el.cumulative ? vm.paymentDate <= new Date(el.withholdingCumulativeExpiryDate) : vm.paymentDate <= new Date(el.withholdingExpiryDate);
            });
        }

        function changePaymentDate() {
            _.map(vm.list, function (el) {
                el.paymentDate = vm.paymentDate;
                el.checked = el.cumulative ? vm.paymentDate <= new Date(el.withholdingCumulativeExpiryDate) : vm.paymentDate <= new Date(el.withholdingExpiryDate);
            });
        }

        function doReset() {

            AlertService.askConfirm('Withholding.List.ChangeFlag').then(function () {
                loadList();
            })

        }

        function doCalculateAll() {

            WithholdingService.calculateAllInterestAndSanction($state.params.companyId, $state.params.periodId, vm.list, vm.paymentDate).then(function (response) {
                vm.loading = true;
                vm.computedInterest = true;
                loadData(response);
            });

        }



        /**
  * Ask the user for rounding value.
  */
        function openSanctionInterestPopup(row) {
            WithholdingInterest.open(row, vm.paymentDate, vm.cumulativePayment).result.then(function (result) {
                row.interestAmount = result.interestAmount;
                row.sanctionAmount = result.sanctionAmount;
                row.checked = true;
                vm.computedInterest = true;
            })
        }

        function doExtractFile() {
            if (elementIsSelected() && !angular.isUndefined(vm.extractType)) {
                var exportObj = new Object();
                exportObj.withholdingList = vm.tableParams.data;
                exportObj.codSia = vm.codSia;
                exportObj.unifyPayment = vm.unifyPayment;
                exportObj.accountPath = vm.accountPath
                exportObj.paymentDate = vm.paymentDate;
                exportObj.extractType = vm.extractType;
                exportObj.intermediaryPath = vm.intermediaryPath;

                WithholdingService.saveRequest(vm.condominiumId, vm.financialPeriodId, exportObj).then(function (response) {
                    vm.idRequest = response;
                    WithholdingService.exportFile(vm.condominiumId, vm.financialPeriodId, vm.idRequest);

                });
            }
            else {
                return NotifierService.notifyError('Withholding.List.SelectOneAndType');
            }
        }

        function doPay(row) {

            if (elementIsSelected()) {
                var exportObj = new Object();
                exportObj.withholdingList = vm.tableParams.data;
                exportObj.codSia = vm.codSia;
                exportObj.unifyPayment = vm.unifyPayment;
                exportObj.accountPath = vm.accountPath
                exportObj.paymentDate = vm.paymentDate;
                exportObj.extractType = vm.extractType;

                WithholdingService.pay(vm.condominiumId, vm.financialPeriodId, exportObj).then(function () {
                    return NotifierService.notifySuccess('Withholding.List.CalculateAll.Success').then(function () {
                        loadList();
                    });
                }).catch(function () {
                    return NotifierService.notifyError('Withholding.List.CalculateAll.Failure');
                });

            }
            else {
                return NotifierService.notifyError('Withholding.List.SelectOne');
            }
        }

        function elementIsSelected() {
            return (_.some(vm.tableParams.data, function (row) {
                return row.selected;
            }));
        }


    }

})();

