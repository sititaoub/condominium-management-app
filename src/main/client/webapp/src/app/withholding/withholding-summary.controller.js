/*
 * budget-edit.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.budget')
        .controller('WithholdingSummaryController', WithholdingSummaryController);

        WithholdingSummaryController.$inject = ['$state', 'NgTableParams', 'WithholdingService', 'WithholdingInterest',  'AlertService','NotifierService'];

    /* @ngInject */
    function WithholdingSummaryController($state, NgTableParams, WithholdingService, WithholdingInterest, AlertService, NotifierService) {
        var vm = this;
        vm.condominiumId = $state.params.condominiumId;
        vm.financialPeriodId = $state.params.periodId;
        vm.companyId = $state.params.companyId;
        vm.companyId = $state.params.companyId;

        activate();

        ////////////////


        function activate() {
            initializeVar()
        }


        function initializeVar() {
            vm.loading = true;
            vm.accountPath = undefined;
            vm.showFilters = false;
            vm.sumBy = _.sumBy;

            vm.cumulativePayment = true;

            vm.tableParams = NgTableParams.fromUiStateParams($state.params, {
            }, {
                    counts: [],
                    getData: loadList,
                });

        }

        function loadList(params) {
            if (!vm.firstLoad) {
                $state.go('.', params.uiRouterUrl(), { notify: false, inherit: false });
            }
            return WithholdingService.summary(vm.condominiumId, vm.financialPeriodId).then(function (list) {
                vm.firstLoad = false;
                vm.paymentDate = new Date();
                vm.computedInterest = false;
                vm.rows = list;
                return  vm.rows;

            }).catch(function () {
                return NotifierService.notifyError();
            }).finally(function () {
                vm.loading = false;
            });
        }



    }

})();

