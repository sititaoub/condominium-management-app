/*
 * budget.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.withholding')
        .config(setupRoutes);

        setupRoutes.$inject = ['$stateProvider'];

        /* @ngInject */
        function setupRoutes($stateProvider) {
            $stateProvider.state('index.withholding', {
                url: '/condominiums/{condominiumId}/financial-period/{periodId}/withholding',
                abstract: true,
                template: '<ui-view></ui-view>',
                data: {
                    skipFinancialPeriodCheck: false
                },
            onEnter: ['$state', '$transition$', 'FinancialPeriodService', function ($state, $transition$, FinancialPeriodService) {
                // Ensure that if 'current' is provided as financial period, the current financial period is provided.
                if ($transition$.params().periodId === 'current') {
                    var params = angular.copy($transition$.params());
                    params.periodId = FinancialPeriodService.getCurrent($transition$.params().companyId, $transition$.params().condominiumId).id;
                    return $state.target($transition$.targetState().name(), params);
                }
                return true;
            }] 
            }).state('index.withholding.summary', {
               url: '/summary',
               controller: 'WithholdingSummaryController',
               controllerAs: 'vm',
               templateUrl: 'app/withholding/withholding-summary.html'
            }).state('index.withholding.list', {
                url: '',
                controller: 'WithholdingListController',
                controllerAs: 'vm',
                templateUrl: 'app/withholding/withholding-list.html'
            });
        }

})();