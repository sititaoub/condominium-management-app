/*
 * accounting-transaction.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingTransaction')
        .factory('WithholdingService', WithholdingService);

    WithholdingService.$inject = ['$rootScope', '$sessionStorage', 'Restangular', 'contextAddressManagement', '_', '$uibModal', '$q'];

    /* @ngInject */
    function WithholdingService($rootScope, $sessionStorage, Restangular, contextAddressManagement, _, $uibModal, $q) {
        var WithholdingService = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressManagement + 'v1');
        });


        var service = {
            getList: getList,
            calculateInterestAndSanction: calculateInterestAndSanction,
            calculateAllInterestAndSanction: calculateAllInterestAndSanction,
            exportFile: exportFile,
            pay: pay,
            saveRequest: saveRequest,
            summary: summary
        };
        return service;


        /**
         * MANAGE withholding
         * 
         */

        function getList(condominiumId, financialPeriodId) {

            return WithholdingService.one("condominiums", condominiumId)
                .one("financial-period", financialPeriodId)
                .getList("withholdings").then(function (withholding) {
                    withholding.withholdingDate = withholding.withholdingDate ? new Date(withholding.withholdingDate) : undefined;
                    withholding.withholdingExpiryDate = withholding.withholdingExpiryDate ? new Date(withholding.withholdingExpiryDate) : undefined;
                    withholding.withholdingCumulativeExpiryDate = withholding.withholdingCumulativeExpiryDate ? new Date(withholding.withholdingCumulativeExpiryDate) : undefined;
                    return withholding;
                });
        }

        function calculateInterestAndSanction(condominiumId, financialPeriodId, withholding) {

            return WithholdingService.one("condominiums", condominiumId)
                .one("financial-period", financialPeriodId)
                .one("withholdings", withholding.withholdingId).post("calculate-interest-and-sanction", withholding).then(function (withholding) {
                    withholding.withholdingDate = withholding.withholdingDate ? new Date(withholding.withholdingDate) : undefined;
                    withholding.withholdingExpiryDate = withholding.withholdingExpiryDate ? new Date(withholding.withholdingExpiryDate) : undefined;
                    withholding.withholdingCumulativeExpiryDate = withholding.withholdingCumulativeExpiryDate ? new Date(withholding.withholdingCumulativeExpiryDate) : undefined;
                    return withholding;
                });
        }

        function calculateAllInterestAndSanction(condominiumId, financialPeriodId, withholdingList, paymentDate) {

            return WithholdingService.one("condominiums", condominiumId)
                .one("financial-period", financialPeriodId)
                .one("withholdings", "").post("calculate-all-interest-and-sanction", withholdingList).then(function (withholding) {

                    _.map(withholding, function (element) {
                        element.withholdingDate = element.withholdingDate ? new Date(element.withholdingDate) : undefined;
                        element.withholdingExpiryDate = element.withholdingExpiryDate ? new Date(element.withholdingExpiryDate) : undefined;
                        element.withholdingCumulativeExpiryDate = element.withholdingCumulativeExpiryDate ? new Date(element.withholdingCumulativeExpiryDate) : undefined;
                        element.paymentDate = paymentDate;
                        element.checked = true;
                        element.computedInterest = true;
                        element.cumulative = true;
                        element.selected = false;
                    });

                    return withholding;
                });
        }

        /**
         */
        function saveRequest(condominiumId, financialPeriodId, dto) {
            return WithholdingService.one("condominiums", condominiumId)
                .one("financial-period", financialPeriodId)
                .one("withholdings", "")
                .post("save-request", dto)
        }


        /**
         *  call end-point for download report
         */
        function exportFile(condominiumId, financialPeriodId, idRequest) {
            var url = WithholdingService.one("condominiums", condominiumId)
                .one("financial-period", financialPeriodId)
                .one("withholdings", "")
                .one("export", idRequest).getRequestedUrl();

            window.open(url, "_blank");
        }


        function pay(condominiumId, financialPeriodId, dto) {
            return WithholdingService.one("condominiums", condominiumId)
                .one("financial-period", financialPeriodId)
                .one("withholdings", "")
                .post("pay", dto)
        }


        function summary(condominiumId, financialPeriodId) {

            return WithholdingService.one("condominiums", condominiumId)
                .one("financial-period", financialPeriodId).one("withholdings", "")
                .getList("summary").then(function (withholdingSumary) {
                    return withholdingSumary;
                });
        }


    }

})();

