/*
 * budget-rounding-edit.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.budget')
        .factory('WithholdingInterest', WithholdingInterest);

    WithholdingInterest.$inject = ['$uibModal'];

    /* @ngInject */
    function WithholdingInterest($uibModal) {
        var service = {
            open: open
        };
        return service;

        ////////////////

    
        function open(selectedItem, paymentDate, cumulativePayment) {
         
            return $uibModal.open({
                templateUrl: 'app/withholding/withholding-interest.html',
                windowClass: 'inmodal budget-container',
                bindToController: true,
                controller: WithholdingInterestController,
                controllerAs: 'vm',
                resolve: {
                    selectedItem: function() { return selectedItem; },
                    paymentDate: function()  { return paymentDate; },
                    cumulativePayment: function()  { return cumulativePayment; }
                }
            });
        }
    }


    WithholdingInterestController.$inject = ['$state','$uibModalInstance','selectedItem','paymentDate','cumulativePayment','WithholdingService' ];

    /* @ngInject */
    function WithholdingInterestController($state, $uibModalInstance, selectedItem, paymentDate, cumulativePayment, WithholdingService) {
        var vm = this;
       
        vm.doConfirm = doConfirm;
        vm.condominiumId = $state.params.condominiumId;
        vm.financialPeriodId = $state.params.periodId;
        vm.paymentDate = new Date(paymentDate) ;
        vm.selectedItem = selectedItem;
        vm.cumulativePayment = cumulativePayment;
        vm.doCalculate = doCalculate;
        activate();

        ////////////////

        function activate() {
        }

        function doCalculate() {
            vm.selectedItem.cumulative = vm.cumulativePayment;
            WithholdingService.calculateInterestAndSanction(vm.condominiumId,  vm.financialPeriodId, vm.selectedItem).then(function (result) {
                  vm.selectedItem = result;
                 });
        }

        function doConfirm() {
            $uibModalInstance.close(vm.selectedItem);
        }
    }

})();

