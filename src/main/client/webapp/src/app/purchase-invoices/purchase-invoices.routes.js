/*
 * purchase-invoices.routes.js
 *
 * (C) 2017-2018 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.purchaseInvoices')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider'];

    /* @ngInject */
    function setupRoutes($stateProvider) {
        $stateProvider.state('index.purchase-invoices', {
            url: '/condominiums/{condominiumId}/financial-period/{periodId}/purchase-invoices',
            abstract: true,
            template: '<ui-view></ui-view>',
            redirectTo: 'index.purchase-invoices.list',
            data: {
                skipFinancialPeriodCheck: false
            },
            onEnter: ['$state', '$transition$', 'FinancialPeriodService', function ($state, $transition$, FinancialPeriodService) {
                // Ensure that if 'current' is provided as financial period, the current financial period is provided.
                if ($transition$.params().periodId === 'current') {
                    var params = angular.copy($transition$.params());
                    params.periodId = FinancialPeriodService.getCurrent($transition$.params().companyId, $transition$.params().condominiumId).id;
                    return $state.target($transition$.targetState().name(), params);
                }
                return true;
            }]
         }).state('index.purchase-invoices.list', {
            url: "?page&count",
            templateUrl: "app/purchase-invoices/purchase-invoices.html",
            controller: "purchaseInvoicesController",
            controllerAs: 'vm',
            params: {
                page: { dynamic: true, squash: true, value: '1' },
                count: { dynamic: true, squash: true, value: '10' },
                sort: { dynamic: true, squash: true, value: 'data,desc' },
                status: "IMPORTATO"  // Show this status only invoices
            }
        }).state('index.purchase-invoices.list.detail', {
            url: '/detail/{id}',
            views: {
                '@index.purchase-invoices': {
                    templateUrl: "app/purchase-invoices/purchase-invoices-detail.html",
                    controller: "purchaseInvoiceDetailController",
                    controllerAs: 'vm',
                }
            },
            resolve: {
                invoice: ['$transition$', 'PurchaseInvoicesService', function ($transition$, PurchaseInvoicesService) {
                    return PurchaseInvoicesService.getOne($transition$.params().companyId,$transition$.params().id);
                }]
            }
        });
    }

})();
