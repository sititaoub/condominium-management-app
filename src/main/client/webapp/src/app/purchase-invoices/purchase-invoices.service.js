/*
 * purchase-invoices.service.js
 *
 * (C) 2017-2018 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.purchaseInvoices')
        .factory('PurchaseInvoicesService', PurchaseInvoicesService);

        PurchaseInvoicesService.$inject = ['$q',  'Restangular', 'contextAddressInvoices', 'CacheFactory', '_'];

    /* @ngInject */
    function PurchaseInvoicesService($q, Restangular, contextAddressInvoices,  CacheFactory,  _) {
        var PurchaseInvoices = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressInvoices + 'v2');

        });

        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressInvoices + 'v2');
        });

        var service = {
            getPage: getPage,
            getOne: getOne,
            downloadAttachment: downloadAttachment
        };
        return service;


        ////////////////

        /**
         * Retrieve a single page of PurchaseInvoice for given company id.
         *
         * @param {number} companyId - The unique company id.
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=10] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<PurchaseInvoice~PurchaseInvoicePage|Error>} - The promise of page.
         */
        function getPage(companyId, page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return PurchaseInvoices
                .one("companies", companyId)
                .all("purchase-invoices").get("", params);
        }

        /**
         * Retrieve a single PurchaseInvoice.
         *
         * @param {number} companyId - The unique company id.
         * @param {number} id - The unique id of invoice.
         * @returns {Promise<PurchaseInvoice~PurchaseInvoice|Error>} - The promise over the invoice.
         */
        function getOne(companyId, id) {
            return PurchaseInvoices.one("companies", companyId).one("purchase-invoices", id).get().then(function (invoice) {
                invoice.data = invoice.data ? new Date(invoice.data) : undefined;
                if( invoice.competenza != undefined ) {
                    invoice.competenza.from = invoice.competenza.from ? new Date(invoice.competenza.from) : undefined;
                    invoice.competenza.to   = invoice.competenza.to ? new Date(invoice.competenza.to) : undefined;
                }
                angular.forEach( invoice.pagamenti, function(value,key) {
                    angular.forEach( value.dettagli, function(v2,k2) {
                        v2.scadenza = v2.scadenza ? new Date(v2.scadenza) : undefined;
                    })
                })
/*
                // SM TEST ONLY add some payments
                invoice.pagamenti[1] = new Object();
                invoice.pagamenti[1].id = 9991;
                invoice.pagamenti[1].tipo = "PGRT";
                invoice.pagamenti[1].dettagli = new Array();
                invoice.pagamenti[1].dettagli[0] = new Object();
                invoice.pagamenti[1].dettagli[0].importo = new Object();
                invoice.pagamenti[1].dettagli[0].importo.valore = 123.45;
                invoice.pagamenti[1].dettagli[0].metodo = "BONFC";
                invoice.pagamenti[1].dettagli[0].scadenza = new Date("2018-08-22");
                invoice.pagamenti[1].dettagli[0].stato = "PPGT";

                invoice.pagamenti[1].dettagli[1] = new Object();
                invoice.pagamenti[1].dettagli[1].importo = new Object();
                invoice.pagamenti[1].dettagli[1].importo.valore = 234.56;
                invoice.pagamenti[1].dettagli[1].metodo = "BLBNC";
                invoice.pagamenti[1].dettagli[1].scadenza = new Date("2018-08-22");
                invoice.pagamenti[1].dettagli[1].stato = "PGT_";
                invoice.pagamenti[1].dettagli[1].conto = new Object();
                invoice.pagamenti[1].dettagli[1].conto.iban = "IT50L0103012194000000304892"

                invoice.pagamenti[1].dettagli[2] = new Object();
                invoice.pagamenti[1].dettagli[2].importo = new Object();
                invoice.pagamenti[1].dettagli[2].importo.valore = 234.56;
                invoice.pagamenti[1].dettagli[2].metodo = "BLBNC";
                invoice.pagamenti[1].dettagli[2].scadenza = new Date("2017-08-22");
                invoice.pagamenti[1].dettagli[2].stato = "PGT_";

                invoice.pagamenti[1].dettagli[3] = new Object();
                invoice.pagamenti[1].dettagli[3].importo = new Object();
                invoice.pagamenti[1].dettagli[3].importo.valore = 234.56;
                invoice.pagamenti[1].dettagli[3].metodo = "BLBNC";
                invoice.pagamenti[1].dettagli[3].scadenza = new Date("2017-08-22");
                invoice.pagamenti[1].dettagli[3].stato = "PPGT";
                // SM TEST ONLY
*/
                return invoice;
            });
        }

        /**
         * Download of invoice attachment
         * @param {number} companyId - The unique company id.
         * @param {number} id - The unique id of invoice.
         * @param {String} type - The type of attachment to download (pdf,cbi...)
         */
        function downloadAttachment(companyId, id, type) {
            PurchaseInvoices
                .one("companies", companyId)
                .one("purchase-invoices", id)
                .one("attachment")
                .withHttpConfig({ responseType: 'blob' })
                .get( { type: type } )
                .then(function (response) {
                    //var file = new Blob([response.data], {type: 'application/pdf'});
                    //var url = (window.URL || window.webkitURL).createObjectURL(file);
                    var url = (window.URL || window.webkitURL).createObjectURL(response);
                    window.open(url);
                });
        }
    }

})();

