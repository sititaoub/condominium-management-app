/*
 * purchase-invoices.constants.js
 *
 * (C) 2017-2018 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.purchaseInvoices')
        .constant('empty', {
        });      

})();
