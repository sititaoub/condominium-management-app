/*
 * purchase-invoices.controller.js
 *
 * (C) 2017-2018 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.purchaseInvoices')
        .controller('purchaseInvoiceDetailController', purchaseInvoiceDetailController);

        purchaseInvoiceDetailController.$inject = ['$state', '$uibModal', 'PurchaseInvoicesService', 'NgTableParams','NotifierService','invoice'];

    /* @ngInject */
    function purchaseInvoiceDetailController($state, $uibModal, PurchaseInvoicesService, NgTableParams, NotifierService, invoice) {
        var vm = this;
        vm.params = $state.params;
        vm.invoice = invoice;
        vm.now = new Date();
        vm.toggleFilters = toggleFilters;
        vm.downloadAttachment = downloadAttachment;
        activate();
        
        ////////////////

        function activate() {
            vm.loading = true;
            vm.firstLoad = true;
            vm.filterDateOpened = false;
            /*
            vm.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10
            }, {
                    getData: loadPurchaseInvoices,
                });

            vm.showFilters = vm.tableParams.hasFilter();
            */
        }

        function toggleFilters() {
            vm.showFilters = !vm.showFilters;
        }

        /**
         * Perform a download of invoice attachment
         * @param {Number} id - The Id of invocie to download attachment of
         * @param {String} type - The type of attachment to download (pdf,cbi...)
         */
        function downloadAttachment(id,type) {
            PurchaseInvoicesService.downloadAttachment($state.params.companyId, id, type);
        }

    }

})();

