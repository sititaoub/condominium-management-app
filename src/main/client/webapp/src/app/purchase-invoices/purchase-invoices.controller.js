/*
 * purchase-invoices.controller.js
 *
 * (C) 2017-2018 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.purchaseInvoices')
        .controller('purchaseInvoicesController', purchaseInvoicesController);

        purchaseInvoicesController.$inject = ['$state', '$uibModal', 'PurchaseInvoicesService', 'AccountingTransactionService', 'NgTableParams','NotifierService','FinancialPeriodService'];

    /* @ngInject */
    function purchaseInvoicesController($state, $uibModal,PurchaseInvoicesService, AccountingTransactionService, NgTableParams,NotifierService,FinancialPeriodService) {
        var vm = this;
        vm.params = $state.params;
        vm.toggleFilters = toggleFilters;
        vm.downloadAttachment = downloadAttachment;
        activate();
        
        ////////////////

        function activate() {
            vm.loading = true;
            vm.firstLoad = true;
            vm.filterDateOpened = false;
            vm.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10
            }, {
                    getData: loadPurchaseInvoices,
                });

            vm.showFilters = vm.tableParams.hasFilter();
        }

        function toggleFilters() {
            vm.showFilters = !vm.showFilters;
        }

        function loadPurchaseInvoices(params) {
            $state.go('.', params.uiRouterUrl(), { inherit: false });
            return PurchaseInvoicesService.getPage($state.params.companyId,
                params.page() - 1, params.count(), params.sorting(), params.filter()).then(function (page) {
                    params.total(page.totalElements);
                    vm.firstLoad = false;
               
                    var result = _.map(page.content, function(invoice){
                        return _.defaults( invoice )
                    });

                    return result;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    vm.loading = false;
                });
        }

        /**
         * Perform a download of invoice attachment
         * @param {Number} id - The Id of invocie to download attachment of
         * @param {String} type - The type of attachment to download (pdf,cbi...)
         */
        function downloadAttachment(id,type) {
            PurchaseInvoicesService.downloadAttachment($state.params.companyId, id, type);
        }


    }

})();

