/*
 * meetings.module.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings', [
            'condominiumManagementApp.core',
            'condominiumManagementApp.commons'
        ]);

})();