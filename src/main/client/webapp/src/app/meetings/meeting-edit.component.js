/*
 * meeting-detail.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('meetingEdit', {
            controller: MeetingEditController,
            bindings: {
                'condominium': '<',
                'meeting': '<'
            },
            templateUrl: 'app/meetings/meeting-edit.html'
        });

    MeetingEditController.$inject = ['$state', '$timeout', 'MeetingService', 'NotifierService', 'moment'];

    /* @ngInject */
    function MeetingEditController($state, $timeout, MeetingService, NotifierService, moment) {
        var $ctrl = this;

        var EXACTLY_ONE_DAY = moment.duration(1, 'day').as('milliseconds');

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;
        $ctrl.firstCallDateChanged = firstCallDateChanged;

        ////////////

        function onInit() {
        }

        function firstCallDateChanged(previousDateTime, dateTime) {
            var changeSecondCall = true;
            var firstMoment = moment(previousDateTime);
            var newFirstMoment = moment(dateTime);
            var newLastMoment = moment(dateTime).add(10, 'days');
            if (angular.isDefined($ctrl.meeting.secondCall.dateTime)) {
                var secondMoment = moment($ctrl.meeting.secondCall.dateTime);
                var diff = secondMoment.diff(firstMoment);
                changeSecondCall = diff === EXACTLY_ONE_DAY || secondMoment.isBefore(newFirstMoment)
                    || secondMoment.isAfter(newLastMoment);
            }
            if (changeSecondCall) {
                $timeout(function () {
                    $ctrl.meeting.secondCall.dateTime = moment(dateTime).add(1, 'day').utc().toDate();
                    $ctrl.meeting.secondCall = angular.copy($ctrl.value.meeting.secondCall);
                });
            }
        }

        function doSave() {
            return MeetingService.update($ctrl.condominium.id, $ctrl.meeting.id, $ctrl.meeting).then(function () {
                return NotifierService.notifySuccess('Meetings.Edit.Success');
            }).then(function () {
                return $state.go('^');
            }).catch(function () {
                return NotifierService.notifyError('Meetings.Edit.Failure');
            });
        }
    }

})();

