/*
 * call-chooser.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('callChooser', {
            controller: CallChooserController,
            bindings: {
                close: '&',
                dismiss: '&',
                meeting: '<'
            },
            templateUrl: 'app/meetings/call-chooser.html'
        });

    CallChooserController.$inject = [];

    /* @ngInject */
    function CallChooserController() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.call = "1";
        }

        function doSave() {
            $ctrl.close({ $value: $ctrl.call })
        }
    }

})();

