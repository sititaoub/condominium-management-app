/*
 * appeal-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('appealEdit', {
            controller: AppealEditController,
            bindings: {
                resolve: '<',
                close: '&',
                dismiss: '&'
            },
            templateUrl: 'app/meetings/appeal/appeal-edit.html'
        });

    AppealEditController.$inject = ['$q','moment', 'MeetingUnrollingService', 'NotifierService'];

    /* @ngInject */
    function AppealEditController($q, moment, MeetingUnrollingService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.call = $ctrl.resolve.call;
            $ctrl.expectedParticipants = $ctrl.resolve.expectedParticipants;
            $ctrl.appeal = $ctrl.resolve.appeal;
        }

        function doSave() {
            return MeetingUnrollingService.updateAppeal(
                $ctrl.resolve.condominiumId,
                $ctrl.resolve.meetingId,
                $ctrl.resolve.call,
                $ctrl.resolve.entryId,
                $ctrl.appeal
            ).then(function () {
                return NotifierService.notifySuccess('Meetings.Unroll.Appeal.Edit.Success');
            }).then(function () {
                return $ctrl.close({ $value: $ctrl.appeal });
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Unroll.Appeal.Edit.Failure');
                return $q.reject(err);
            });
        }
    }

})();

