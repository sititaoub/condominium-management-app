/*
 * variation-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('variationCreate', {
            controller: VariationCreateController,
            bindings: {
                resolve: '<',
                close: '&',
                dismiss: '&'
            },
            templateUrl: 'app/meetings/appeal/variation-create.html'
        });

    VariationCreateController.$inject = ['$q','moment', 'MeetingUnrollingService', 'NotifierService'];

    /* @ngInject */
    function VariationCreateController($q, moment, MeetingUnrollingService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.call = $ctrl.resolve.call;
            $ctrl.expectedParticipants = $ctrl.resolve.expectedParticipants;
            $ctrl.variation = {
                type: 'VARIATION',
                dateTime: moment().toDate(),
                presences: $ctrl.resolve.actualAppeal.presences
            };
        }

        function doSave() {
            return MeetingUnrollingService.addVariation(
                $ctrl.resolve.condominiumId,
                $ctrl.resolve.meetingId,
                $ctrl.resolve.call,
                $ctrl.variation
            ).then(function () {
                return NotifierService.notifySuccess('Meetings.Unroll.Variation.Create.Success');
            }).then(function () {
                return $ctrl.close({ $value: $ctrl.variation });
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Unroll.Appeal.Variation.Failure');
                return $q.reject(err);
            })
        }
    }

})();

