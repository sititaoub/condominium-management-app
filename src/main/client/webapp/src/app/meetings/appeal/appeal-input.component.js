/*
 * appeal-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('appealInput', {
            controller: AppealInputController,
            bindings: {
                ngModel: '<',
                call: '<',
                expectedParticipants: '<',
                variation: '<?'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/meetings/appeal/appeal-input.html'
        });

    AppealInputController.$inject = ['$filter', 'NgTableParams', 'VotingRuleService', 'moment', '_'];

    /* @ngInject */
    function AppealInputController($filter, NgTableParams, VotingRuleService, moment, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.onChecklistChanged = onChecklistChanged;
        $ctrl.onDelegateChange = onDelegateChange;
        $ctrl.isPresent = isPresent;

        ////////////

        function onInit() {
            $ctrl.expectedParticipants = _.map($ctrl.expectedParticipants, function (p) {
                p.fullName = $filter('fullName')(p) + ' ' + p.personId;
                return p;
            });
            $ctrl.tableParams = new NgTableParams({
                page: 1,
                count: 5,
                group: 'fullName',
                sorting: {
                    fullName: 'asc'
                }
            }, {
                dataset: $ctrl.expectedParticipants,
                counts: [5, 10, 25, 50, 100],
                groupOptions: {
                    isExpanded: false
                }
            });
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
                $ctrl.value.totalPeople = _.uniq(_.map($ctrl.expectedParticipants, 'personId')).length;
                $ctrl.value.totalSum = _.sumBy($ctrl.expectedParticipants, 'thousandths');
                if (_.isString($ctrl.value.dateTime)) {
                    $ctrl.value.dateTime = new Date($ctrl.value.dateTime);
                }
                $ctrl.presences = _.map($ctrl.value.presences, 'personId');
                $ctrl.selectedPeople = $ctrl.value.presences.length;
                $ctrl.selectedSum = _.sumBy($ctrl.value.presences, 'thousandths');
                $ctrl.delegates = {};
                _.forEach(_.filter($ctrl.value.presences, 'delegate'), function (presence) {
                    $ctrl.delegates[presence.personId] = presence.delegate;
                });
                $ctrl.delegatesCount = _.filter($ctrl.value.presences, 'delegate').length;
            };
            loadConstitutionRule();
        }

        function onChange() {
            $ctrl.value.presences = _.map(_.groupBy(_.filter($ctrl.expectedParticipants, function (p) {
                return isPresent(p.personId);
            }), 'personId'), function (p) {
                return {
                    personId: p[0].personId,
                    personName: $filter('fullName')(p[0]),
                    delegate: $ctrl.delegates[p[0].personId],
                    thousandths: _.sumBy(p, 'thousandths')
                }
            });
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function onDelegateChange() {
            $ctrl.delegatesCount = _.filter(_.values($ctrl.delegates), function (el) { return el !== ''; }).length;
            if ($ctrl.value.totalPeople > 20) {
                // TODO Check that no delegates go over 1/5 of thousandths and 1/5 of people count.
            }
            onChange();
        }

        function onChecklistChanged() {
            $ctrl.selectedSum = _.sumBy(_.filter($ctrl.expectedParticipants, function (p) {
                return _.includes($ctrl.presences, p.personId);
            }), 'thousandths');
            $ctrl.selectedPeople = $ctrl.presences.length;
            evaulateRule();
            onChange();
        }

        function loadConstitutionRule() {
            VotingRuleService.getConstitutionRule().then(function (rule) {
                $ctrl.constitutionRule = rule;
                evaulateRule();
                onChange();
            });
        }

        function evaulateRule() {
            var peopleResult = VotingRuleService.evaluateRule($ctrl.constitutionRule, $ctrl.call, 'participating', $ctrl.selectedPeople, $ctrl.value.totalPeople);
            var thousandthsResult = VotingRuleService.evaluateRule($ctrl.constitutionRule, $ctrl.call, 'thousandths', $ctrl.selectedSum, $ctrl.value.totalSum);
            $ctrl.value.quorum = peopleResult && thousandthsResult;
        }

        function isPresent(personId) {
            return _.includes($ctrl.presences, personId);
        }
    }

})();

