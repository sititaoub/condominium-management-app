/*
 * variation-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('variationEdit', {
            controller: VariationEditController,
            bindings: {
                resolve: '<',
                close: '&',
                dismiss: '&'
            },
            templateUrl: 'app/meetings/appeal/variation-edit.html'
        });

    VariationEditController.$inject = ['$q','moment', 'MeetingUnrollingService', 'NotifierService'];

    /* @ngInject */
    function VariationEditController($q, moment, MeetingUnrollingService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.call = $ctrl.resolve.call;
            $ctrl.expectedParticipants = $ctrl.resolve.expectedParticipants;
            $ctrl.variation = $ctrl.resolve.variation;
        }

        function doSave() {
            return MeetingUnrollingService.updateVariation(
                $ctrl.resolve.condominiumId,
                $ctrl.resolve.meetingId,
                $ctrl.resolve.call,
                $ctrl.resolve.entryId,
                $ctrl.variation
            ).then(function () {
                return NotifierService.notifySuccess('Meetings.Unroll.Variation.Edit.Success');
            }).then(function () {
                return $ctrl.close({ $value: $ctrl.variation });
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Unroll.Variation.Edit.Failure');
                return $q.reject(err);
            });
        }
    }

})();

