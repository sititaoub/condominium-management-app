/*
 * appeal-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('appealCreate', {
            controller: AppealCreateController,
            bindings: {
                resolve: '<',
                close: '&',
                dismiss: '&'
            },
            templateUrl: 'app/meetings/appeal/appeal-create.html'
        });

    AppealCreateController.$inject = ['$q','moment', 'MeetingUnrollingService', 'NotifierService'];

    /* @ngInject */
    function AppealCreateController($q, moment, MeetingUnrollingService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.call = $ctrl.resolve.call;
            $ctrl.expectedParticipants = $ctrl.resolve.expectedParticipants;
            $ctrl.appeal = {
                type: 'APPEAL',
                dateTime: moment().add(moment().utcOffset(), 'minutes').toDate(),
                presences: []
            };
        }

        function doSave() {
            return MeetingUnrollingService.addAppeal(
                $ctrl.resolve.condominiumId,
                $ctrl.resolve.meetingId,
                $ctrl.resolve.call,
                $ctrl.appeal
            ).then(function () {
                return NotifierService.notifySuccess('Meetings.Unroll.Appeal.Create.Success');
            }).then(function () {
                return $ctrl.close({ $value: $ctrl.appeal });
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Unroll.Appeal.Create.Failure');
                return $q.reject(err);
            })
        }
    }

})();

