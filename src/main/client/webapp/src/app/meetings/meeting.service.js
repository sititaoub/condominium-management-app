/*
 * meeting.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .factory('MeetingService', MeetingService);

    MeetingService.$inject = ['$q', '$log', 'Restangular', 'contextAddressMeetings', '_', 'moment'];

    /* @ngInject */
    function MeetingService($q, $log, Restangular, contextAddressMeetings, _, moment) {
        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressMeetings + 'v1');
            configurer.addResponseInterceptor(function (data, operation, what, url, response) {
                if (operation === "post" && response.status === 201) {
                    var location = response.headers('Location');
                    return location.substr(location.lastIndexOf('/') + 1);
                }
                return data;
            });
        });

        var service = {
            getPage: getPage,
            getOne: getOne,
            getAttachments: getAttachments,
            getParticipants: getParticipants,
            getCandidateParticipants: getCandidateParticipants,
            getExpectedParticipants: getExpectedParticipants,
            getSummoning: getSummoning,
            getAgenda: getAgenda,
            getEntry: getEntry,
            create: create,
            update: update,
            updateParticipants: updateParticipants,
            remove: remove,
            addEntry: addEntry,
            updateEntry: updateEntry,
            removeEntry: removeEntry,
            moveEntryUp: moveEntryUp,
            moveEntryDown: moveEntryDown,
            summonPeople: summonPeople,
            uploadAttachment: uploadAttachment,
            removeAttachment: removeAttachment,
            generatePrints: generatePrints,
            mergeAndDownload: mergeAndDownload,
            archiveAndDownload: archiveAndDownload,
            sendSummoning: sendSummoning
        };
        return service;

        ////////////////

        /**
         * @namespace Meetings
         */

        /**
         * Meetings page.
         *
         * @typedef {Object} Meetings~MeetingsPage
         * @memberOf Meetings
         * @property {number} number - The requested page number.
         * @property {number} size - The requested page size.
         * @property {number} totalElements - The total number of elements.
         * @property {number} totalPages - The total number of pages.
         * @property {Meetings~Meeting[]} content - The page content.
         */

        /**
         * Meeting object.
         *
         * @typedef {Object} Meetings~Meeting
         * @memberOf Meetings
         * @property {number} id - The unique id of meeting.
         * @property {string} description - The description of meeting
         * @property {ThousandthsTables~ThousandthsTable} propertyTable - The property table to be used.
         * @property {FinancialPeriod~FinancialPeriod} financialPeriod - The financial period.
         * @property {Meetings~MeetingCall} firstCall - The first call.
         * @property {Meetings~MeetingCall} secondCall - The second call.
         * @property {string} notes - Some notes about the meeting.
         * @property {string} privateNots - Private notes about the meeting.
         */

        /**
         * Meeting call.
         *
         * @typedef {Object} Meetings~MeetingCall
         * @memberOf Meetings
         * @property {Date} dateTime - Date and time of call
         * @property {Meeting~PostalAddress} - Meeting postal address
         */

        /**
         * Meeting postal address.
         *
         * @typedef {Object} Meetings~PostalAddress
         * @memberOf Meetings
         * @property {string} streetName - The address street name.
         * @property {string} buildingNumber - The address building number.
         * @property {string} town - The address town.
         * @property {string} district - The address district.
         * @property {string} province - The address province.
         * @property {string} country - The address country
         * @property {number} [lat] - The latitude
         * @property {number} [lng] - The longitude.
         */

        /**
         * Meeting participants.
         *
         * @typedef {Object} Meetings~Participants
         * @memberOf Meetings
         * @property {string} participantsType - The type of participants.
         * @property {number[]} participants - The list of unique identifier for participants.
         */

        /**
         * Meeting candidate participant.
         *
         * @typedef {Object} Meetings~CandidateParticipant
         * @memberOf Meetings
         * @property {number} referenceId - The unique id of participant.
         * @property {number} buildingId - The unique id of building.
         * @property {string} building - The description of building.
         * @property {number} groupId - The unique id of group.
         * @property {string} group - The description of group.
         * @property {number} unitId - The unique id of housing unit.
         * @property {string} unit - The description of housing unit.
         * @property {Meetings~Person} person - The object holding the person data.
         * @property {string} role - The person role.
         * @property {number} value - The role percentage.
         */

        /**
         * Meeting expected participant.
         *
         * @typedef {Object} Meetings~ExpectedParticipant
         * @memberOf Meetings
         * @property {number} buildingId - The unique id of building.
         * @property {string} building - The description of building.
         * @property {number} groupId - The unique id of group.
         * @property {string} group - The description of group.
         * @property {number} unitId - The unique id of housing unit.
         * @property {string} unit - The description of housing unit.
         * @property {Object[]} roles - The participant roles.
         * @property {number} coefficient - The coefficient
         */

        /**
         * Meeting agenda
         *
         * @typedef {Object} Meetings~Person
         * @memberOf Meetings
         * @property {number} id - Th person id
         * @property {string} fistName - The person first name.
         * @property {string} lastName - The person last name.
         * @property {string} companyName - The person company name.
         */

        /**
         * Meeting agenda
         *
         * @typedef {Object} Meetings~Agenda
         * @memberOf Meetings
         * @property {string} title - The agenda title.
         * @property {string} description - The agenda description (if any).
         * @property {string} privateNotes - Private notes about the entry.
         * @property {string[]} votersRoles - The list of voters roles.
         * @property {VotingRules~VotingRule} votingRule - The voting rule
         * @property {boolean} readonly - True if this entry is readonly.
         */

        /**
         * Meeting expected participant.
         *
         * @typedef {Object} Meetings~AgendaVoter
         * @memberOf Meetings
         * @property {number} buildingId - The unique id of building.
         * @property {string} building - The description of building.
         * @property {number} groupId - The unique id of group.
         * @property {string} group - The description of group.
         * @property {number} unitId - The unique id of housing unit.
         * @property {string} unit - The description of housing unit.
         * @property {Object[]} roles - The participant roles.
         * @property {number} coefficient - The coefficient
         */

        /**
         * Meeting summoning.
         *
         * @typedef {Object} Meetings~Summoning
         * @memberOf Meetings
         * @property {number[]} summonedPeople - The unique identifier of summoned people
         */

        /**
         * Send configuration.
         *
         * @typedef {Object} Meetings~SendConfiguration
         * @memberOf Meetings
         * @property {number} personId - The unique id of person to which send the summoning
         * @property {number} channel - The channel selected for the summoning
         */

        /**
         * Attachment object.
         *
         * @typedef {Object} Meetings~Attachment
         * @memberOf Meetings
         * @property {string} id - The unique id of attachment
         * @property {string} fileName - The file name
         * @property {number} contentLength - The length (in bytes)
         * @property {string} contentType - The type of content
         * @property {string} [downloadURL] - The url to download content from
         */

        /**
         * Retrieve a single page of meetings.
         *
         * @param {number} condominiumId - The unique id of condominium
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=10] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<Meetings~MeetingsPage|Error>} - The promise of page.
         */
        function getPage(condominiumId, page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return Api.one('condominiums', condominiumId).all('meetings').get("", params);
        }

        /**
         * Retrieve a single meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique manager id.
         * @returns {Promise<Meetings~Meeting|Error>} - The promise of page.
         */
        function getOne(condominiumId, id) {
            return Api.one('condominiums', condominiumId).one('meetings', id).get().then(function (meeting) {
                var offset = moment().utcOffset();
                if (angular.isDefined(meeting.firstCall) && angular.isDefined(meeting.firstCall.dateTime)) {
                    meeting.firstCall.dateTime = moment(meeting.firstCall.dateTime).add(offset, 'minutes').toDate();
                }
                if (angular.isDefined(meeting.secondCall) && angular.isDefined(meeting.secondCall.dateTime)) {
                    meeting.secondCall.dateTime = moment(meeting.secondCall.dateTime).add(offset, 'minutes').toDate();
                }
                return meeting;
            });
        }

        /**
         * Retrieve the attachments of meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique manager id.
         * @returns {Promise<Meetings~Attachment[]|Error>} - The promise of list of attachments
         */
        function getAttachments(condominiumId, id) {
            return Api.one('condominiums', condominiumId).one('meetings', id).all('attachments').getList();
        }

        /**
         * Retrieve the participants of a meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique manager id.
         * @returns {Promise<Meetings~Participants|Error>} - The promise of page.
         */
        function getParticipants(condominiumId, id) {
            return Api.one('condominiums', condominiumId).one('meetings', id).all('participants').get('');
        }

        /**
         * Retrieve the candidate participants of this meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique manager id.
         * @returns {Promise<Meetings~CandidateParticipant[]|Error>} - The promise of page.
         */
        function getCandidateParticipants(condominiumId, id) {
            return Api.one('condominiums', condominiumId).one('meetings', id).all('participants').all('candidates').getList()
        }

        /**
         * Retrieve the list of expected participants.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique manager id.
         * @returns {Promise<Meetings~ExpectedParticipant[]|Error>} - The promise of page.
         */
        function getExpectedParticipants(condominiumId, id) {
            return Api.one('condominiums', condominiumId).one('meetings', id).all('participants').all('expected').getList()
        }

        /**
         * Retrieve the summoned participants of this meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique manager id.
         * @returns {Promise<Meetings~Summoning|Error>} - The promise of page.
         */
        function getSummoning(condominiumId, id) {
            return Api.one('condominiums', condominiumId).one('meetings', id).all("summoning").get("");
        }

        /**
         * Retrieve the agenda of a meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique manager id.
         * @returns {Promise<Meetings~Agenda[]|Error>} - The promise of list
         */
        function getAgenda(condominiumId, id) {
            return Api.one('condominiums', condominiumId).one('meetings', id).all('agenda').getList();
        }

        /**
         * Retrieve an entry of an agenda of a meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique manager id.
         * @param {number} index - The index
         * @returns {Promise<Meetings~Agenda|Error>} - The promise of entry.
         */
        function getEntry(condominiumId, id, index) {
            return Api.one('condominiums', condominiumId).one('meetings', id).one('agenda', index).get();
        }

        /**
         * Create a new meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {Meetings~Meeting} meeting - The meeting to be created.
         * @param {Meetings~Participants} participants - The set of participants.
         * @param {number} defaultAgenda - The unique id of default agenda to use to initialize this meeting.
         * @returns {Promise<Number|Error>} - The promise of result.
         */
        function create(condominiumId, meeting, participants, defaultAgenda) {
            return Api.one('condominiums', condominiumId).all('meetings').post({
                financialPeriodId: meeting.financialPeriod.id,
                description: meeting.description,
                propertyTableId: angular.isUndefined(meeting.propertyTable) ? undefined : meeting.propertyTable.id,
                participantsType: participants.participantsType,
                participants: participants.participants,
                defaultAgendaId: angular.isUndefined(defaultAgenda) ? undefined : defaultAgenda.id,
                firstCall: meeting.firstCall,
                secondCall: meeting.secondCall
            });
        }

        /**
         * Updates an existing meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique id of meeting to update.
         * @param {Meetings~Meeting} meeting - The meeting to be updated
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function update(condominiumId, id, meeting) {
            return Api.one('condominiums', condominiumId).one('meetings', id).customPUT({
                financialPeriodId: meeting.financialPeriod.id,
                description: meeting.description,
                propertyTableId: meeting.propertyTable.id,
                firstCall: meeting.firstCall,
                secondCall: meeting.secondCall,
                notes: meeting.notes,
                privateNotes: meeting.privateNots
            });
        }

        /**
         * Update the participants of existing meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique id of meeting to update.
         * @param {Meetings~Participants} participants - The unique id of participants.
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function updateParticipants(condominiumId, id, participants) {
            return Api.one('condominiums', condominiumId).one('meetings', id).all('participants').customPUT(participants);
        }

        /**
         * Remove the meeting with supplied id.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique id of manager
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function remove(condominiumId, id) {
            return Api.one('condominiums', condominiumId).one('meetings', id).remove();
        }

        /**
         * Adds a new entry to meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique id of manager
         * @param {Meeting~Agenda} entry - The entry
         * @returns {Promise<number|Error>} - The promise with the index of newly created entry
         */
        function addEntry(condominiumId, id, entry) {
            return Api.one('condominiums', condominiumId).one('meetings', id).all('agenda').post(entry);
        }

        /**
         * Updates an existing entry in meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique id of manager
         * @param {number} index - The unique index of entry
         * @param {Meeting~Agenda} entry - The entry
         * @returns {Promise<Object|Error>} - The promise with the index of newly created entry
         */
        function updateEntry(condominiumId, id, index, entry) {
            return Api.one('condominiums', condominiumId).one('meetings', id).one('agenda', index).customPUT(entry);
        }

        /**
         * Removes an existing entry in meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique id of manager
         * @param {number} index - The unique index of entry
         * @returns {Promise<Object|Error>} - The promise with the index of newly created entry
         */
        function removeEntry(condominiumId, id, index) {
            return Api.one('condominiums', condominiumId).one('meetings', id).one('agenda', index).remove();
        }

        /**
         * Moves an existing entry in meeting up one position.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique id of manager
         * @param {number} index - The unique index of entry
         * @returns {Promise<Object|Error>} - The promise with the index of newly created entry
         */
        function moveEntryUp(condominiumId, id, index) {
            return Api.one('condominiums', condominiumId).one('meetings', id).one('agenda', index).all('position')
                .customPUT(undefined, undefined, {direction: 'up'});
        }

        /**
         * Moves an existing entry in meeting down one position.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique id of manager
         * @param {number} index - The unique index of entry
         * @returns {Promise<Object|Error>} - The promise with the index of newly created entry
         */
        function moveEntryDown(condominiumId, id, index) {
            return Api.one('condominiums', condominiumId).one('meetings', id).one('agenda', index).all('position')
                .customPUT(undefined, undefined, {direction: 'down'});
        }

        /**
         * Summon the people with supplied identifier.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique id of manager
         * @param {number[]} peopleId - The unique id of people to summon
         * @returns {Promise<Object|Error>} - The promise with operation outcome.
         */
        function summonPeople(condominiumId, id, peopleId) {
            return Api.one('condominiums', condominiumId).one('meetings', id).all("summoning").customPUT({
                personIds: peopleId
            });
        }

        /**
         * Retrieve the URL used to upload the attachments.
         *
         * @param {number} condominiumId - The unique id of condominium
         * @param {number} id - The unique id of manager
         * @returns {string} - The upload URL
         */
        function uploadAttachment(condominiumId, id) {
            return Api.one('condominiums', condominiumId).one('meetings', id).all("attachments").getRestangularUrl();
        }

        /**
         * Remove the attachment.
         *
         * @param {number} condominiumId - The unique id of condominium
         * @param {number} id - The unique id of manager
         * @param {string} attachmentId - The unique id of attachment
         * @return {Promise<Void|Error>} - The promise of operation outcome
         */

        function removeAttachment(condominiumId, id, attachmentId) {
            return Api.one('condominiums', condominiumId).one('meetings', id).one("attachments", attachmentId).remove();
        }

        /**
         * Generate prints for supplied meetings
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique id of meeting
         * @param {number} templateId - The unique id of template to use to generate prints
         * @returns {Promise<Object|Error>} - The promise with operation outcome.
         */
        function generatePrints(condominiumId, id, templateId) {
            return Api.one('condominiums', condominiumId).one('meetings', id).all('summoning').all('documents').post({
                templateId: templateId
            });
        }

        /**
         * Generate prints for supplied meetings
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique id of meeting
         * @param {number} templateId - The unique id of template to use to generate prints
         * @param {number[]} participantsId - The array of unique participants identifiers
         * @returns {Promise<Object|Error>} - The promise with operation outcome.
         */
        function mergeAndDownload(condominiumId, id, templateId, participantsId) {
            return Api.one('condominiums', condominiumId).one('meetings', id).all('summoning').all('documents')
                .withHttpConfig({responseType: 'arraybuffer', cache: false})
                .customPOST({
                    templateId: templateId,
                    personIds: participantsId
                }, "download", undefined, {
                    accept: 'application/pdf'
                });
        }

        /**
         * Generate prints for supplied meetings
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique id of meeting
         * @param {number} templateId - The unique id of template to use to generate prints
         * @param {number[]} participantsId - The array of unique participants identifiers
         * @returns {Promise<Object|Error>} - The promise with operation outcome.
         */
        function archiveAndDownload(condominiumId, id, templateId, participantsId) {
            return Api.one('condominiums', condominiumId).one('meetings', id).all('summoning').all('documents')
                .withHttpConfig({responseType: 'arraybuffer', cache: false})
                .customPOST({
                    templateId: templateId,
                    personIds: participantsId
                }, "download", undefined, {
                    accept: 'application/zip'
                });
        }

        /**
         * Send the summoning for supplied meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} templateId - The unique id of template.
         * @param {Meetings~SendConfiguration[]} participants - The list of send configuration for participants.
         * @return {Promise<Void|Error>} - The promise of operation outcome
         */
        function sendSummoning(condominiumId, meetingId, templateId, participants) {
            return Api.one('condominiums', condominiumId).one('meetings', meetingId).all('summoning').all('documents')
                .customPOST({
                    templateId: templateId,
                    configurations: participants
                }, "send");
        }
    }

})();

