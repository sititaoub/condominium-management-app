/*
 * meetings.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider'];

    /* @ngInject */
    function setupRoutes ($stateProvider) {
        $stateProvider.state('index.meetings', {
            url: '/condominiums/{condominiumId}/meetings',
            redirectTo: 'index.meetings.list',
            data: {
                skipFinancialPeriodCheck: true
            },
            template: '<div class="meetings-container" ui-view></div>',
            resolve: {
                condominium: ['$transition$', 'CondominiumService', function ($transition$, CondominiumService) {
                    return CondominiumService.getOne($transition$.params().condominiumId);
                }]
            }
        }).state('index.meetings.list', {
            url : '?m_page&m_count&m_sort&m_financialPeriodId&m_description&m_fromFirstCallDate&m_toFirstCallDate&m_fromSecondCallDate&m_toSecondCallDate',
            component: 'meetingsList',
            params: {
                m_page: { dynamic: true, squash: true, value: '1', inherits: true },
                m_count: { dynamic: true, squash: true, value: '10', inherits: true },
                m_sort: { dynamic: true, squash: true, value: 'financialPeriodId,asc', inherits: true },
                m_financialPeriodId: { dynamic: true, inherits: true },
                m_description: { dynamic: true, inherits: true },
                m_fromFirstCallDate: { dynamic: true, inherits: true },
                m_toFirstCallDate: { dynamic: true, inherits: true },
                m_fromSecondCallDate: { dynamic: true, inherits: true },
                m_toSecondCallDate: { dynamic: true, inherits: true }
            }
        }).state('index.meetings.list.create', {
            url: '/create',
            views: {
                '@index.meetings': {
                    component: 'meetingCreate'
                }
            }
        }).state('index.meetings.list.edit', {
            url: '/{meetingId}/edit',
            views: {
                '@index.meetings': {
                    component: 'meetingEdit'
                }
            },
            resolve: {
                meeting: ['$transition$', 'MeetingService', function ($transition$, MeetingService) {
                    return MeetingService.getOne($transition$.params().condominiumId, $transition$.params().meetingId);
                }]
            }
        }).state('index.meetings.list.agenda', {
            url: '/{meetingId}/agenda',
            views: {
                '@index.meetings': {
                    component: 'meetingAgendaEdit'
                }
            },
            resolve: {
                meeting: ['$transition$', 'MeetingService', function ($transition$, MeetingService) {
                    return MeetingService.getOne($transition$.params().condominiumId, $transition$.params().meetingId);
                }],
                agenda: ['$transition$', 'MeetingService', function ($transition$, MeetingService) {
                    return MeetingService.getAgenda($transition$.params().condominiumId, $transition$.params().meetingId);
                }]
            }
        }).state('index.meetings.list.attachments', {
            url: '/{meetingId}/attachments',
            views: {
                '@index.meetings': {
                    component: 'meetingAttachments'
                }
            },
            resolve: {
                meeting: ['$transition$', 'MeetingService', function ($transition$, MeetingService) {
                    return MeetingService.getOne($transition$.params().condominiumId, $transition$.params().meetingId);
                }]
            }
        }).state('index.meetings.list.summon', {
            url: '/{meetingId}/summon',
            views: {
                '@index.meetings': {
                    component: 'participantsSummon'
                }
            },
            resolve: {
                meeting: ['$transition$', 'MeetingService', function ($transition$, MeetingService) {
                    return MeetingService.getOne($transition$.params().condominiumId, $transition$.params().meetingId);
                }],
                participants: ['$transition$', 'MeetingService', function ($transition$, MeetingService) {
                    return MeetingService.getCandidateParticipants($transition$.params().condominiumId, $transition$.params().meetingId);
                }],
                summoning: ['$transition$', 'MeetingService', function ($transition$, MeetingService) {
                    return MeetingService.getSummoning($transition$.params().condominiumId, $transition$.params().meetingId);
                }]
            }
        }).state('index.meetings.list.summon.send', {
            url: '/send',
            views: {
                '@index.meetings': {
                    component: 'summoningSend'
                }
            },
            resolve: {
                participants: ['$transition$', 'MeetingService', function ($transition$, MeetingService) {
                    return MeetingService.getExpectedParticipants($transition$.params().condominiumId, $transition$.params().meetingId);
                }],
                communicationTypes: ['PersonsService', function (PersonsService) {
                    return PersonsService.allCommunicationTypes();
                }]
            }
        }).state('index.meetings.list.unroll', {
            url: '/{meetingId}/call/{callId}/unroll',
            views: {
                '@index.meetings': {
                    component: 'meetingUnroll'
                }
            },
            resolve: {
                meeting: ['$transition$', 'MeetingService', function ($transition$, MeetingService) {
                    return MeetingService.getOne($transition$.params().condominiumId, $transition$.params().meetingId);
                }],
                agenda: ['$transition$', 'MeetingService', function ($transition$, MeetingService) {
                    return MeetingService.getAgenda($transition$.params().condominiumId, $transition$.params().meetingId);
                }],
                unrolling: ['$transition$', 'MeetingUnrollingService', function ($transition$, MeetingUnrollingService) {
                    return MeetingUnrollingService.getOne($transition$.params().condominiumId, $transition$.params().meetingId, $transition$.params().callId);
                }]
            }
        }).state('index.meetings.list.unroll.send', {
            url: '/send',
            views: {
                '@index.meetings': {
                    component: 'meetingUnrollSend'
                }
            },
            resolve: {
                participants: ['$transition$', 'MeetingService', function ($transition$, MeetingService) {
                    return MeetingService.getExpectedParticipants($transition$.params().condominiumId, $transition$.params().meetingId);
                }],
                communicationTypes: ['PersonsService', function (PersonsService) {
                    return PersonsService.allCommunicationTypes();
                }]
            }
        });
    }

})();