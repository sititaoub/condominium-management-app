/*
 * text-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('textCreate', {
            controller: TextCreateController,
            bindings: {
                resolve: '<',
                close: '&',
                dismiss: '&'
            },
            templateUrl: 'app/meetings/text/text-create.html'
        });

    TextCreateController.$inject = ['$q','moment', 'MeetingUnrollingService', 'NotifierService'];

    /* @ngInject */
    function TextCreateController($q, moment, MeetingUnrollingService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.text = {
                type: 'TEXT',
                dateTime: moment().add(moment().utcOffset(), 'minutes').toDate(),
            };
        }

        function doSave() {
            return MeetingUnrollingService.addText(
                $ctrl.resolve.condominiumId,
                $ctrl.resolve.meetingId,
                $ctrl.resolve.call,
                $ctrl.text
            ).then(function () {
                return NotifierService.notifySuccess('Meetings.Unroll.Text.Create.Success');
            }).then(function () {
                return $ctrl.close({ $value: $ctrl.text });
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Unroll.Text.Create.Failure');
                return $q.reject(err);
            })
        }
    }

})();

