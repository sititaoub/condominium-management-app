/*
 * text-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('textInput', {
            controller: TextInputController,
            bindings: {
                ngModel: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/meetings/text/text-input.html'
        });

    TextInputController.$inject = ['$filter', 'PhrasesService'];

    /* @ngInject */
    function TextInputController($filter, PhrasesService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.setupEditor = setupEditor;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
                if (_.isString($ctrl.value.dateTime)) {
                    $ctrl.value.dateTime = new Date($ctrl.value.dateTime);
                }
            };
            PhrasesService.getAll().then(function (phrases) {
                $ctrl.phrases = phrases;
                $ctrl.loading = false;
            });
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function setupEditor(editor) {
            if ($ctrl.phrases && $ctrl.phrases.length > 0) {
                var translate = $filter('translate');
                editor.addMenuItem('phrases', {
                    text: translate('Meetings.Unroll.Text.Editor.Phrases'),
                    context: 'insert',
                    icon: false,
                    menu: _.map($ctrl.phrases, function (el) {
                        return {
                            text: el.description,
                            onclick: function () {
                                PhrasesService.getOne(el.id).then(function (phrase) {
                                    editor.insertContent(phrase.content);
                                });
                            }
                        };
                    })
                });
            }
        }
    }

})();

