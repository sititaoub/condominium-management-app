/*
 * text-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('textEdit', {
            controller: TextEditController,
            bindings: {
                resolve: '<',
                close: '&',
                dismiss: '&'
            },
            templateUrl: 'app/meetings/text/text-edit.html'
        });

    TextEditController.$inject = ['$q','moment', 'MeetingUnrollingService', 'NotifierService'];

    /* @ngInject */
    function TextEditController($q, moment, MeetingUnrollingService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.text = $ctrl.resolve.text;
        }

        function doSave() {
            return MeetingUnrollingService.updateText(
                $ctrl.resolve.condominiumId,
                $ctrl.resolve.meetingId,
                $ctrl.resolve.call,
                $ctrl.resolve.entryId,
                $ctrl.text
            ).then(function () {
                return NotifierService.notifySuccess('Meetings.Unroll.Text.Edit.Success');
            }).then(function () {
                return $ctrl.close({ $value: $ctrl.text });
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Unroll.Text.Edit.Failure');
                return $q.reject(err);
            })
        }
    }

})();

