/*
 * buildings-chooser.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('buildingsChooser', {
            controller: BuildingsChooserController,
            bindings: {
                ngModel: '<',
                condominium: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/meetings/participants/buildings-chooser.html'
        });

    BuildingsChooserController.$inject = ['StructureService', 'NotifierService'];

    /* @ngInject */
    function BuildingsChooserController(StructureService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
            loadStructure();
            setupScrollbarConfig();
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function loadStructure() {
            StructureService.getTree($ctrl.condominium.id).then(function (tree) {
                $ctrl.buildings = tree.buildings;
            }).catch(function () {
                NotifierService.notifyError();
            }).finally(function () {
                $ctrl.loading = false;
            });
        }

        function setupScrollbarConfig() {
            $ctrl.scrollbarConfig = {
                autoHideScrollbar: false,
                theme: 'dark',
                advanced: {
                    axix: 'y',
                    updateOnContentResize: true
                }
            };
        }
    }

})();

