/*
 * participants-type-select.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('participantsTypeSelect', {
            controller: ParticipantsTypeSelectController,
            bindings: {
                id: '@?',
                name: '@?',
                controlClass: '@?',
                ngModel: '<',
                ngRequired: '<',
                ngDisabled: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/meetings/participants/participants-type-select.html'
        });

    ParticipantsTypeSelectController.$inject = [];

    /* @ngInject */
    function ParticipantsTypeSelectController() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue($ctrl.value);
        }
    }

})();

