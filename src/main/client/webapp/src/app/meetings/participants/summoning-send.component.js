/*
 * participants-send.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('summoningSend', {
            controller: SummoningSendController,
            bindings: {
                condominium: '<',
                meeting: '<',
                participants: '<',
                summoning: '<',
                communicationTypes: '<'
            },
            templateUrl: 'app/meetings/participants/summoning-send.html'
        });

    SummoningSendController.$inject = ['$q', '$window', '$translate', 'PersonsService', 'MeetingService', 'TemplateService', 'NgTableParams', '_', 'NotifierService', 'AlertService', 'WizardHandler'];

    /* @ngInject */
    function SummoningSendController($q, $window, $translate, PersonsService, MeetingService, TemplateService, NgTableParams, _, NotifierService, AlertService, WizardHandler) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;

        $ctrl.selectParticipant = selectParticipant;
        $ctrl.selectFirstParticipant = selectFirstParticipant;
        $ctrl.generatePrints = generatePrints;
        $ctrl.canDownload = canDownload;
        $ctrl.selectAllVisible = selectAllVisible;
        $ctrl.deselectAllVisible = deselectAllVisible;
        $ctrl.sendSelected = sendSelected;
        $ctrl.sendSelectedWith = sendSelectedWith;
        $ctrl.archiveAndDownloadSelected = archiveAndDownloadSelected;
        $ctrl.mergeAndDownloadSelected = mergeAndDownloadSelected;
        $ctrl.channelOf = channelOf;
        $ctrl.isTemplateChanged = isTemplateChanged;
        $ctrl.availableCommunicationTypes = availableCommunicationTypes;
        $ctrl.selectedCommunicationTypes = selectedCommunicationTypes;

        ////////////

        function onInit() {
            $ctrl.templateId = $ctrl.meeting.summoningTemplateId;
            $ctrl.sendingParticipants = [];
            $ctrl.sendingChannels = _.uniqBy(_.map($ctrl.participants, function (p) {
                return {
                    personId: p.personId,
                    channel: p.traceableContactType
                };
            }), 'personId');
            $ctrl.tableParams = new NgTableParams({
                page: 1,
                count: 10,
                sorting: {
                    fullName: 'asc'
                },
                group: 'personId'
            }, {
                filterOptions: {
                    filterComparator: angular.equals
                },
                groupOptions: {
                    isExpanded: false
                },
                dataset: $ctrl.participants
            });
        }

        function isTemplateChanged() {
            return $ctrl.templateId !== $ctrl.meeting.summoningTemplateId;
        }

        function availableCommunicationTypes(person) {
            return _.filter($ctrl.communicationTypes.plain(), function (commType) {
                switch (commType) {
                    case 'EMAIL':
                    case 'SMART_MAIL':
                        return person.hasEmailAddress;
                    case 'PEC':
                        return person.hasPec;
                    case 'REGISTERED_MAIL':
                    case 'PRIORITY_MAIL':
                        return person.hasPostalAddress;
                    case 'FAX':
                        return person.hasFax;
                    default:
                        return false;
                }
            });
        }
        
        function selectedCommunicationTypes() {
            var participants = _.filter(_.uniqBy($ctrl.participants, 'personId'), function (p) {
                return _.indexOf($ctrl.sendingParticipants, p.personId) >= 0;
            });
            return _.reduce(_.filter(_.map(participants, $ctrl.availableCommunicationTypes), function (cts) {
                return !_.isEmpty(cts);
            }), function (result, value) {
                return _.intersection(result, value);
            }, $ctrl.communicationTypes.plain());
        }

        function channelOf(personId) {
            return _.find($ctrl.sendingChannels, { personId: personId });
        }

        function selectParticipant(personId) {
            $ctrl.selectedPersonId = personId;
            $ctrl.pdfSrc = TemplateService.getExpansionUrl($ctrl.templateId, 'meetings/' + $ctrl.meeting.id + '/' + personId);
        }

        function generatePrints() {
            return MeetingService.generatePrints($ctrl.condominium.id, $ctrl.meeting.id, $ctrl.templateId).then(function () {
                NotifierService.notifySuccess('Meetings.Participants.Summoning.Send.GenerateSuccess');
                WizardHandler.wizard().next();
                selectFirstParticipant();
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Participants.Summoning.Send.GenerateFailure');
                return $q.reject(err);
            });
        }

        function selectFirstParticipant() {
            selectParticipant($ctrl.tableParams.data[0].data[0].personId);
        }

        function selectAllVisible() {
            $ctrl.sendingParticipants = _.uniq(_.map($ctrl.participants, 'personId'));
        }

        function deselectAllVisible() {
            $ctrl.sendingParticipants = [];
        }

        function canDownload() {
            return $ctrl.sendingParticipants.length > 0
        }

        function canSend() {
            // Check that all the sending participants have a corresponding contactType
            var filtered = _.filter($ctrl.sendingChannels, function (person) {
                return person.channel != null && _.indexOf($ctrl.sendingParticipants, person.personId) >= 0;
            });
            return filtered.length === $ctrl.sendingParticipants.length;
        }

        function sendSelected() {
            var promise = $q.reject();
            if (!canSend()) {
                promise = AlertService.askConfirm('Meetings.Participants.Summoning.Send.AreYouSure');
            } else {
                promise = $q.resolve();
            }
            $ctrl.sending = promise.then(function () {
                var configuration = _.filter($ctrl.sendingChannels, function (sc) {
                    return sc.channel != null && _.indexOf($ctrl.sendingParticipants, sc.personId) >= 0;
                });
                return MeetingService.sendSummoning($ctrl.condominium.id, $ctrl.meeting.id, $ctrl.templateId, configuration).then(function () {
                    NotifierService.notifySuccess('Meetings.Participants.Summoning.Send.Success');
                    return $q.resolve();
                }).catch(function (err) {
                    NotifierService.notifyError('Meetings.Participants.Summoning.Send.Failure');
                    return $q.reject(err);
                });
            }).catch(function (err) {
                $q.reject(err)
            });
            return $ctrl.sending;
        }

        function sendSelectedWith(channel) {
            var configuration = _.map(_.filter($ctrl.sendingChannels, function (sc) {
                return _.indexOf($ctrl.sendingParticipants, sc.personId) >= 0;
            }), function (cfg) {
                return {
                    personId: cfg.personId,
                    channel: channel
                };
            });
            return MeetingService.sendSummoning($ctrl.condominium.id, $ctrl.meeting.id, $ctrl.templateId, configuration).then(function () {
                NotifierService.notifySuccess('Meetings.Participants.Summoning.Send.Success');
                return $q.resolve();
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Participants.Summoning.Send.Failure');
                return $q.reject(err);
            });
        }

        function archiveAndDownloadSelected() {
            return MeetingService.archiveAndDownload($ctrl.condominium.id, $ctrl.meeting.id, $ctrl.templateId, $ctrl.sendingParticipants)
                .then(function (response) {
                    $translate('Meetings.Participants.Summoning.Send.ZipFileName', $ctrl.meeting).then(function (fileName) {
                        saveAs(response, fileName);
                    });
                }).catch(function (err) {
                    NotifierService.notifyError('Meetings.Participants.Summoning.Send.ArchiveFailure');
                    return $q.reject(err);
                });
        }

        function mergeAndDownloadSelected() {
            return MeetingService.mergeAndDownload($ctrl.condominium.id, $ctrl.meeting.id, $ctrl.templateId, $ctrl.sendingParticipants)
                .then(function (response) {
                    $translate('Meetings.Participants.Summoning.Send.PdfFileName', $ctrl.meeting).then(function (fileName) {
                        saveAs(response, fileName);
                    });
                }).catch(function (err) {
                    NotifierService.notifyError('Meetings.Participants.Summoning.Send.MergeFailure');
                    return $q.reject(err);
                });
        }

        function saveAs(response, fileName) {
            var a = document.createElement('a');
            a.style = 'display: none;';
            document.body.appendChild(a);
            var file = new Blob([response], {type: 'application/pdf'});
            a.href = ($window.URL || $window.webkitURL).createObjectURL(file);
            a.download = fileName;
            a.click();
            ($window.URL || window.$webkitURL).revokeObjectURL(file);
        }
    }

})();

