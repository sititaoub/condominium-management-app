/*
 * assignment-chooser.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('assignmentChooser', {
            controller: AssignmentChooserController,
            bindings: {
                ngModel: '<',
                condominium: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/meetings/participants/assignment-chooser.html'
        });

    AssignmentChooserController.$inject = ['$q', 'AssignmentTypeService', 'AssignmentService', 'NotifierService', '_'];

    /* @ngInject */
    function AssignmentChooserController($q, AssignmentTypeService, AssignmentService, NotifierService, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.toggleType = toggleType;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
            loadAssignments();
            setupScrollbarConfig();
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function toggleType(type) {
            if (_.indexOf($ctrl.selectedTypes, type.id) >= 0) {
                // Toggle on
                $ctrl.value = _.uniq(_.concat($ctrl.value, _.map(type.assignments, 'id')));
            } else {
                // Toggle off
                $ctrl.value = _.difference($ctrl.value, _.map(type.assignments, 'id'));
            }
            onChange();
        }

        function loadAssignments() {
            $q.all({
                types: AssignmentTypeService.getPage(0, 100, { name: 'asc' }),
                assignments: AssignmentService.getPage($ctrl.condominium.id, 0, 1000, undefined, { active: true })
            }).then(function (data) {
                var assignments = _.groupBy(data.assignments.content, 'assignmentType.id');
                $ctrl.types = _.map(data.types.content, function (type) {
                    type.assignments = assignments[type.id];
                    return type;
                });
                $ctrl.selectedTypes = _.map(_.filter($ctrl.types, function(type) {
                    // Check if all children are selected: extract all groups ids and check that all the group ids are
                    // in value array.
                    var groupIds = _.map(type.assignments, 'id');
                    return !_.isEmpty(type.assignments) && _.isEmpty(_.difference(groupIds, $ctrl.value));
                }), 'id');
            }).catch(function () {
                NotifierService.notifyError();
            }).finally(function () {
                $ctrl.loading = false;
            });
        }

        function setupScrollbarConfig() {
            $ctrl.scrollbarConfig = {
                autoHideScrollbar: false,
                theme: 'dark',
                advanced: {
                    axix: 'y',
                    updateOnContentResize: true
                }
            };
        }
    }

})();

