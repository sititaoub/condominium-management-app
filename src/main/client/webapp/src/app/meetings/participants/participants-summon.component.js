/*
 * participants-summon.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('participantsSummon', {
            controller: ParticipantsSummonController,
            bindings: {
                condominium: '<',
                meeting: '<',
                participants: '<',
                summoning: '<'
            },
            templateUrl: 'app/meetings/participants/participants-summon.html'
        });

    ParticipantsSummonController.$inject = ['$state', '$q', '$filter', 'NgTableParams', 'MeetingService', 'NotifierService', '_'];

    /* @ngInject */
    function ParticipantsSummonController($state, $q, $filter, NgTableParams, MeetingService, NotifierService, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.toggleFilters = toggleFilters;
        $ctrl.getBuildings = getBuildings;
        $ctrl.getGroups = getGroups;
        $ctrl.getRoleTypes = getRoleTypes;
        $ctrl.selectAllVisible = selectAllVisible;
        $ctrl.deselectAllVisible = deselectAllVisible;
        $ctrl.saveChanges = saveChanges;

        ////////////

        function onInit() {
            $ctrl.selectedParticipants = angular.copy($ctrl.summoning.summonedPeople || []);
            $ctrl.showFilters = false;
            $ctrl.tableParams = new NgTableParams({
                page: 1,
                count: 100,
                sorting: {
                    building: 'asc',
                    group: 'asc',
                    unit: 'asc'
                },
                group: 'person.id'
            }, {
                filterOptions: {
                    filterComparator: angular.equals
                },
                groupOptions: {
                    isExpanded: false
                },
                dataset: $ctrl.participants
            });
        }

        function selectAllVisible() {
            var visibileIds = _.map(_.flatMap($ctrl.tableParams.data, 'data'), 'person.id');
            $ctrl.selectedParticipants = _.uniq(_.union($ctrl.selectedParticipants, visibileIds));
        }

        function deselectAllVisible() {
            var visibileIds = _.map(_.flatMap($ctrl.tableParams.data, 'data'), 'person.id');
            $ctrl.selectedParticipants = _.difference($ctrl.selectedParticipants, visibileIds);
        }

        function toggleFilters() {
            return $ctrl.showFilters = !$ctrl.showFilters;
        }

        function getBuildings() {
            return _.uniqBy(_.map($ctrl.participants, function (participant) {
                return {
                    id: participant.buildingId,
                    title: participant.building
                };
            }), 'id');
        }

        function getGroups() {
            var participants = $ctrl.participants;
            var buildingId = $ctrl.tableParams.filter().buildingId;
            if (angular.isDefined(buildingId) && buildingId !== '') {
                participants = _.filter($ctrl.participants, { buildingId: buildingId });
            }
            return _.sortBy(_.uniqBy(_.map(participants, function (participant) {
                return {
                    id: participant.groupId,
                    title: participant.group,
                    buildingId: participant.buildingId,
                    fullTitle: participant.building + ' - ' + participant.group
                }
            }), 'id'), 'fullTitle');
        }

        function getRoleTypes() {
            return _.uniqBy(_.map($ctrl.participants, function (participant) {
                return {
                    id: participant.role,
                    title: $filter('translate')('Structures.Edit.Role.Type.' + participant.role)
                };
            }), 'id');
        }

        function saveChanges() {
            var deferred = $q.defer();
            MeetingService.summonPeople(
                $ctrl.condominium.id,
                $ctrl.meeting.id,
                $ctrl.selectedParticipants
            ).then(function () {
                return NotifierService.notifySuccess('Meetings.Participants.Summoning.Success');
            }).then(function () {
                deferred.resolve();
                return $state.go('.send', undefined, { reload: true });
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Participants.Summoning.Failure');
                deferred.reject(err)
            });
            return deferred.promise;
        }
    }

})();

