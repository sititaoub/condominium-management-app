/*
 * participants-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('participantsEdit', {
            controller: ParticipantsEditController,
            bindings: {
                close: '&',
                dismiss: '&',
                resolve: '<'
            },
            templateUrl: 'app/meetings/participants/participants-edit.html'
        });

    ParticipantsEditController.$inject = ['MeetingService', 'NotifierService'];

    /* @ngInject */
    function ParticipantsEditController(MeetingService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.condominium = $ctrl.resolve.condominium;
            $ctrl.meeting = $ctrl.resolve.meeting;
            $ctrl.participants = $ctrl.resolve.participants;
        }

        function doSave() {
            return MeetingService.updateParticipants($ctrl.condominium.id, $ctrl.meeting.id, $ctrl.participants).then(function () {
                return NotifierService.notifySuccess('Meetings.Participants.Edit.Success');
            }).then(function () {
                return $ctrl.close();
            }).catch(function () {
                NotifierService.notifyError('Meetings.Participants.Edit.Failure');
            });
        }
    }

})();

