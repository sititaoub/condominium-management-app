/*
 * groups-chooser.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('groupsChooser', {
            controller: GroupsChooserController,
            bindings: {
                ngModel: '<',
                condominium: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/meetings/participants/groups-chooser.html'
        });

    GroupsChooserController.$inject = ['StructureService', 'NotifierService', '_'];

    /* @ngInject */
    function GroupsChooserController(StructureService, NotifierService, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.toggleBuilding = toggleBuilding;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
            loadStructure();
            setupScrollbarConfig();
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function toggleBuilding(building) {
            if (_.indexOf($ctrl.selectedBuildings, building.id) >= 0) {
                // Toggle on
                $ctrl.value = _.uniq(_.concat($ctrl.value, _.map(building.housingUnitGroups, 'id')));
            } else {
                // Toggle off
                $ctrl.value = _.difference($ctrl.value, _.map(building.housingUnitGroups, 'id'));
            }
            onChange();
        }

        function loadStructure() {
            StructureService.getTree($ctrl.condominium.id).then(function (tree) {
                $ctrl.buildings = tree.buildings;
                $ctrl.selectedBuildings = _.map(_.filter($ctrl.buildings, function(building) {
                    // Check if all children are selected: extract all groups ids and check that all the group ids are
                    // in value array.
                    var groupIds = _.map(building.housingUnitGroups, 'id');
                    return _.isEmpty(_.difference(groupIds, $ctrl.value));
                }), 'id');
            }).catch(function () {
                NotifierService.notifyError();
            }).finally(function () {
                $ctrl.loading = false;
            });
        }

        function setupScrollbarConfig() {
            $ctrl.scrollbarConfig = {
                autoHideScrollbar: false,
                theme: 'dark',
                advanced: {
                    axix: 'y',
                    updateOnContentResize: true
                }
            };
        }
    }

})();

