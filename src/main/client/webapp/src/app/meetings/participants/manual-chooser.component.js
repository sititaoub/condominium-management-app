/*
 * manual-chooser.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('manualChooser', {
            controller: ManualChooserController,
            bindings: {
                ngModel: '<',
                condominium: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/meetings/participants/manual-chooser.html'
        });

    ManualChooserController.$inject = ['$filter', 'NgTableParams', 'StructureService', 'NotifierService', '_'];

    /* @ngInject */
    function ManualChooserController($filter, NgTableParams, StructureService, NotifierService, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.getBuildings = getBuildings;
        $ctrl.getGroups = getGroups;
        $ctrl.getRoleTypes = getRoleTypes;

        ////////////

        function onInit() {
            $ctrl.loading = true;

            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
            loadPersons();
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function loadPersons() {
            StructureService.getPersons($ctrl.condominium.id).then(function (people) {
                $ctrl.people = people;
                $ctrl.tableParams = new NgTableParams({
                    page: 1,
                    count: 10,
                    sorting: {
                        building: 'asc',
                        group: 'asc',
                        unit: 'asc'
                    }
                }, {
                    filterOptions: {
                        filterComparator: angular.equals
                    },
                    dataset: $ctrl.people
                });
            }).catch(function () {
                NotifierService.notifyError();
            }).finally(function () {
                $ctrl.loading = false;
            });
        }

        function getBuildings() {
            return _.uniqBy(_.map($ctrl.people, function (participant) {
                return {
                    id: participant.buildingId,
                    title: participant.building
                };
            }), 'id');
        }

        function getGroups() {
            var participants = $ctrl.people;
            var buildingId = $ctrl.tableParams ? $ctrl.tableParams.filter().buildingId : undefined;
            if (angular.isDefined(buildingId) && buildingId !== '') {
                participants = _.filter($ctrl.participants, { buildingId: buildingId });
            }
            return _.sortBy(_.uniqBy(_.map(participants, function (participant) {
                return {
                    id: participant.groupId,
                    title: participant.group,
                    buildingId: participant.buildingId,
                    fullTitle: participant.building + ' - ' + participant.group
                }
            }), 'id'), 'fullTitle');
        }

        function getRoleTypes() {
            return _.uniqBy(_.map($ctrl.people, function (participant) {
                return {
                    id: participant.roleType,
                    title: $filter('translate')('Structures.Edit.Role.Type.' + participant.roleType)
                };
            }), 'id');
        }
    }

})();

