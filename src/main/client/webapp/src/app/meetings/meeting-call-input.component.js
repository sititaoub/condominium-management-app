/*
 * meeting-call-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('meetingCallInput', {
            controller: MeetingCallInputController,
            bindings: {
                ngModel: '<',
                referenceDate: '<?',
                forceRefresh: '<?',
                onDateChanged: '&'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/meetings/meeting-call-input.html'
        });

    MeetingCallInputController.$inject = ['moment'];

    /* @ngInject */
    function MeetingCallInputController(moment) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.$onChanges = onChanges;
        $ctrl.doForceRefresh = doForceRefresh;
        $ctrl.onDateTimeChange = onDateTimeChange;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.referenceSet = false;
            if (!angular.isDefined($ctrl.referenceDate)) {
                var offset = moment().utcOffset();
                $ctrl.datepickerOptions = {
                    minDate: moment().utc().startOf('day').subtract(offset, 'minutes').toDate(),
                    minMode: 'day'
                };
            }
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
                if (angular.isDefined($ctrl.value)) {
                    $ctrl.currentDateTime = $ctrl.value.dateTime;
                }
            };
        }

        function onChanges(changes) {
            if (angular.isDefined(changes.referenceDate) && angular.isDefined(changes.referenceDate.currentValue)) {
                $ctrl.referenceSet = true;
                var offset = moment().utcOffset();
                $ctrl.datepickerOptions = {
                    minDate: moment(changes.referenceDate.currentValue).utc().startOf('day').add(1, 'day')
                        .add(offset, 'minutes').toDate(),
                    minMode: 'day',
                    maxDate: moment(changes.referenceDate.currentValue).utc().startOf('day').add(10, 'day')
                        .add(offset, 'minutes').toDate(),
                    maxMode: 'day'
                };
            }
        }

        function doForceRefresh() {
            $ctrl.value.dateTime = moment($ctrl.referenceDate).add(1, 'day').toDate();
        }

        function onDateTimeChange() {
            $ctrl.onDateChanged({ previousDateTime: $ctrl.currentDateTime, dateTime: $ctrl.value.dateTime });
            $ctrl.currentDateTime = $ctrl.value.dateTime;
            onChange();
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

