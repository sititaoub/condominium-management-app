/*
 * close-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('closeCreate', {
            controller: CloseCreateController,
            bindings: {
                resolve: '<',
                close: '&',
                dismiss: '&'
            },
            templateUrl: 'app/meetings/close/close-create.html'
        });

    CloseCreateController.$inject = ['$q','moment', 'MeetingUnrollingService', 'NotifierService'];

    /* @ngInject */
    function CloseCreateController($q, moment, MeetingUnrollingService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.entry = {
                type: 'CLOSE',
                dateTime: moment().add(moment().utcOffset(), 'minutes').toDate(),
            };
        }

        function doSave() {
            return MeetingUnrollingService.addClose(
                $ctrl.resolve.condominiumId,
                $ctrl.resolve.meetingId,
                $ctrl.resolve.call,
                $ctrl.entry
            ).then(function () {
                return NotifierService.notifySuccess('Meetings.Unroll.Close.Create.Success');
            }).then(function () {
                return $ctrl.close({ $value: $ctrl.entry });
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Unroll.Close.Create.Failure');
                return $q.reject(err);
            })
        }
    }

})();

