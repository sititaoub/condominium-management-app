/*
 * votation-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('votationInput', {
            controller: VotationInputController,
            bindings: {
                ngModel: '<',
                condominium: '<',
                meeting: '<',
                call: '<',
                edit: '<?',
                onEntriesLoaded: '&'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/meetings/votation/votation-input.html'
        });

    VotationInputController.$inject = ['VotingRuleService'];

    /* @ngInject */
    function VotationInputController(VotingRuleService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onAgendaEntryChange = onAgendaEntryChange;
        $ctrl.onVotingRuleChange = onVotingRuleChange;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            }
        }

        function onAgendaEntryChange() {
            if ($ctrl.value.meetingAgendaEntry.votingRule && $ctrl.value.meetingAgendaEntry.votingRule.id) {
                $ctrl.value.votingRuleId = $ctrl.value.meetingAgendaEntry.votingRule.id;
                $ctrl.value.votingRule = $ctrl.value.meetingAgendaEntry.votingRule;
            }
            onChange();
        }

        function onVotingRuleChange() {
            VotingRuleService.getOne($ctrl.value.votingRuleId).then(function (votingRule) {
                $ctrl.value.votingRule = votingRule;
                onChange();
            }).catch(function (err) {
                $ctrl.value.votingRuleId = undefined;
                $ctrl.value.votingRule = undefined;
                onChange();
            });
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

