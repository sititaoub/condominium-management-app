/*
 * vote-result.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('voteResult', {
            controller: VoteResultController,
            bindings: {
                votes: '<',
                voters: '<',
                votingSettings: '<',
                rule: '<',
                call: '<',
                onVotationResult: '&'
            },
            templateUrl: 'app/meetings/votation/vote-result.html'
        });

    VoteResultController.$inject = ['VotingRuleService'];

    /* @ngInject */
    function VoteResultController(VotingRuleService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.$onChanges = onChanges;

        ////////////

        function onInit() {
            evaulateRule();
        }

        function onChanges(changes) {
            if (angular.isDefined(changes.voters) && angular.isDefined(changes.voters.currentValue)) {
                var presents = _.filter(changes.voters.currentValue, { present: true });
                $ctrl.totalPresents = _.uniqBy(presents, 'personId').length;
                $ctrl.totalPresentsThousandths = _.sumBy(presents, 'thousandths');
                $ctrl.totalPeople = _.uniqBy(changes.voters.currentValue, 'personId').length;
                // TODO Check thousandths depending on settings.
                $ctrl.totalThousandths = _.sumBy(changes.voters.currentValue, 'thousandths')
            }
            if (angular.isDefined(changes.votes) && angular.isDefined(changes.votes.currentValue)) {
                var votes = changes.votes.currentValue;
                var yes = _.map(_.filter(votes, { vote: 'YES' }), 'personId');
                var no = _.map(_.filter(votes, { vote: 'NO' }), 'personId');
                var abs = _.map(_.filter(votes, { vote: 'ABS' }), 'personId');

                $ctrl.totalYes = yes.length;
                $ctrl.totalNo = no.length;
                $ctrl.totalAbs = abs.length;

                $ctrl.totalYesMil = _.sumBy(_.filter($ctrl.voters, function (v) {
                    return yes.indexOf(v.personId.toString(10)) >= 0;
                }), 'thousandths');
                $ctrl.totalNoMil = _.sumBy(_.filter($ctrl.voters, function (v) {
                    return no.indexOf(v.personId.toString(10)) >= 0;
                }), 'thousandths');
                $ctrl.totalAbsMil = _.sumBy(_.filter($ctrl.voters, function (v) {
                    return abs.indexOf(v.personId.toString(10)) >= 0;
                }), 'thousandths');
            }

            evaulateRule();

            $ctrl.onVotationResult({ result: {
                approved: $ctrl.approved,
                yesCount: $ctrl.totalYes,
                noCount: $ctrl.totalNo,
                absCount: $ctrl.totalAbs,
                yesThousandths: $ctrl.totalYesMil,
                noThousandths: $ctrl.totalNoMil,
                absThousandths: $ctrl.totalAbsMil
            }});
        }

        function evaulateRule() {
            if (angular.isDefined($ctrl.rule) && angular.isDefined($ctrl.call)) {
                var approved = false;

                var intervenedQuorum = VotingRuleService.evaluateRule($ctrl.rule, $ctrl.call, 'intervened', $ctrl.totalYes, $ctrl.totalPresents);
                var partecipantingQuorum = VotingRuleService.evaluateRule($ctrl.rule, $ctrl.call, 'participating', $ctrl.totalYes, $ctrl.totalPeople);
                var thousandthsQuorum = VotingRuleService.evaluateRule($ctrl.rule, $ctrl.call, 'thousandths', $ctrl.totalYesMil, $ctrl.totalThousandths);

                var hasQuorum = intervenedQuorum && partecipantingQuorum && thousandthsQuorum;

                if (hasQuorum) {
                    // Check if number of yes vote are greater than no votes
                    if ($ctrl.votingSettings.yesNoBehavior === 'YES_GT_NO') {
                        approved = $ctrl.totalYes > $ctrl.totalNo;
                    } else {
                        approved = $ctrl.totalYes > ($ctrl.totalNo + $ctrl.totalAbs);
                    }
                    if (approved && $ctrl.votingSettings.thousandthsVoteBehaviorPresent) {
                        if ($ctrl.votingSettings.thousandthsVoteBehavior === 'YES_GT_NO') {
                            approved = $ctrl.totalYesMil > $ctrl.totalNoMil;
                        } else {
                            approved = $ctrl.totalYesMil > ($ctrl.totalNoMil + $ctrl.totalAbsMil);
                        }
                    }
                }

                $ctrl.approved = hasQuorum && approved;
            }
        }
    }

})();

