/*
 * votation-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('votationEdit', {
            controller: VotationEditController,
            bindings: {
                resolve: '<',
                close: '&',
                dismiss: '&'
            },
            templateUrl: 'app/meetings/votation/votation-edit.html'
        });

    VotationEditController.$inject = ['$q', 'VotingRuleService', 'MeetingUnrollingService', 'NotifierService'];

    /* @ngInject */
    function VotationEditController($q, VotingRuleService, MeetingUnrollingService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;

        $ctrl.loadVoters = loadVoters;
        $ctrl.onConfigurationChange = onConfigurationChange;
        $ctrl.updateVotationResult = updateVotationResult;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            VotingRuleService.getSettings().then(function (settings) {
                $ctrl.votingSettings = settings;
            });
            $ctrl.currentStep = 'configuration';

            $ctrl.votation = $ctrl.resolve.votation;
            $ctrl.votation.dateTime = new Date($ctrl.votation.dateTime);
            $ctrl.votation.votingRuleId = $ctrl.votation.votingRule.id;
            $ctrl.votation.participantsType = $ctrl.votation.votersType || $ctrl.votation.participantsType;

            $ctrl.currentParticipantsType = $ctrl.votation.votersType || $ctrl.votation.participantsType;
            $ctrl.currentParticipants = $ctrl.votation.participants;
        }

        function doSave() {
            var votation = angular.copy($ctrl.votation);
            votation.propertyTableId = votation.propertyTable.id;
            votation.votersType = votation.participantsType;
            votation.voters = votation.participants;
            delete votation.propertyTable;
            delete votation.meetingAgendaEntry;
            delete votation.votingRule;
            delete votation.participantsType;
            delete votation.participants;
            delete votation.outcome;

            return MeetingUnrollingService.updateVotation(
                $ctrl.resolve.condominium.id,
                $ctrl.resolve.meeting.id,
                $ctrl.resolve.call,
                $ctrl.resolve.entryId,
                votation
            ).then(function () {
                return NotifierService.notifySuccess('Meetings.Unroll.Votation.Edit.Success');
            }).then(function () {
                return $ctrl.close({ $value: $ctrl.votation });
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Unroll.Votation.Edit.Failure');
                return $q.reject(err);
            });
        }

        function updateVotationResult(result) {
            $ctrl.votation.outcome = result;
        }

        function onConfigurationChange() {
            if (angular.isDefined($ctrl.votation.meetingAgendaEntry) && (
                    $ctrl.currentParticipantsType !== $ctrl.votation.participantsType ||
                    $ctrl.currentParticipants !== $ctrl.votation.participants)) {
                loadVoters();
            }
        }

        function loadVoters() {
            MeetingUnrollingService.getVoters(
                $ctrl.resolve.condominium.id,
                $ctrl.resolve.meeting.id,
                $ctrl.resolve.call,
                $ctrl.votation.meetingAgendaEntry.idx,
                $ctrl.votation.participantsType,
                $ctrl.votation.participants,
                $ctrl.votation.propertyTable.id
            ).then(function (voters) {
                $ctrl.voters = voters;
            }).catch(function () {
                NotifierService.notifyError();
            });
        }
    }

})();

