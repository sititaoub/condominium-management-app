/*
 * votation-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('votationCreate', {
            controller: VotationCreateController,
            bindings: {
                resolve: '<',
                close: '&',
                dismiss: '&'
            },
            templateUrl: 'app/meetings/votation/votation-create.html'
        });

    VotationCreateController.$inject = ['$q', 'moment', 'VotingRuleService', 'MeetingUnrollingService', 'NotifierService'];

    /* @ngInject */
    function VotationCreateController($q, moment, VotingRuleService, MeetingUnrollingService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;

        $ctrl.onConfigurationChange = onConfigurationChange;
        $ctrl.updateVotationResult = updateVotationResult;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            VotingRuleService.getSettings().then(function (settings) {
                $ctrl.votingSettings = settings;
            });
            $ctrl.currentStep = 'configuration';
            $ctrl.currentParticipantsType = undefined;
            $ctrl.currentParticipants = undefined;
            $ctrl.votation = {
                type: 'VOTATION',
                displayAs: 'COMPACT_LIST',
                dateTime: moment().add(moment().utcOffset(), 'minutes').toDate(),
                propertyTable: angular.copy($ctrl.resolve.meeting.propertyTable),
                participantsType: 'ALL',
                participants: []
            };
        }

        function doSave() {
            var votation = angular.copy($ctrl.votation);
            votation.propertyTableId = votation.propertyTable.id;
            votation.meetingAgendaEntryId = votation.meetingAgendaEntry.idx;
            votation.votersType = votation.participantsType;
            votation.voters = votation.participants;
            delete votation.propertyTable;
            delete votation.meetingAgendaEntry;
            delete votation.votingRule;
            delete votation.participantsType;
            delete votation.participants;
            delete votation.outcome;

            return MeetingUnrollingService.addVotation(
                $ctrl.resolve.condominium.id,
                $ctrl.resolve.meeting.id,
                $ctrl.resolve.call,
                votation
            ).then(function () {
                return NotifierService.notifySuccess('Meetings.Unroll.Votation.Create.Success');
            }).then(function () {
                return $ctrl.close({ $value: $ctrl.votation });
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Unroll.Votation.Create.Failure');
                return $q.reject(err);
            })
        }

        function updateVotationResult(result) {
            $ctrl.votation.outcome = result;
        }

        function onConfigurationChange() {
            if (angular.isDefined($ctrl.votation.meetingAgendaEntry) && (
                    $ctrl.currentParticipantsType !== $ctrl.votation.participantsType ||
                    $ctrl.currentParticipants !== $ctrl.votation.participants)) {
                loadVoters();
            }
        }

        function loadVoters() {
            MeetingUnrollingService.getVoters(
                $ctrl.resolve.condominium.id,
                $ctrl.resolve.meeting.id,
                $ctrl.resolve.call,
                $ctrl.votation.meetingAgendaEntry.idx,
                $ctrl.votation.participantsType,
                $ctrl.votation.participants,
                $ctrl.votation.propertyTable.id
            ).then(function (voters) {
                $ctrl.voters = voters;
            }).catch(function () {
                NotifierService.notifyError();
            });
        }
    }

})();

