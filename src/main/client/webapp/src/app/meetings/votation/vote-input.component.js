/*
 * vote-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('voteInput', {
            controller: VoteInputController,
            bindings: {
                voters: '<',
                votingSettings: '<',
                ngModel: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/meetings/votation/vote-input.html'
        });

    VoteInputController.$inject = ['NgTableParams', '$filter'];

    /* @ngInject */
    function VoteInputController(NgTableParams, $filter) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.$onChanges = onChanges;

        $ctrl.onChange = onChange;
        $ctrl.onVoteChange = onVoteChange;
        $ctrl.toggleYesVotes = toggleYesVotes;
        $ctrl.toggleNoVotes = toggleNoVotes;
        $ctrl.toggleAbsVotes = toggleAbsVotes;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
            $ctrl.tableParams = new NgTableParams({
                page: 1,
                count: 10,
                sorting: {
                    fullName: 'asc'
                },
                group: 'fullName'
            }, {
                groupOptions: {
                    isExpanded: false
                }
            });
        }

        function onChanges(changes) {
            if (angular.isDefined($ctrl.tableParams) &&
                angular.isDefined(changes.voters) &&
                angular.isDefined(changes.voters.currentValue)) {

                var presents = _.map(_.filter(changes.voters.currentValue, { present: true }), function (p) {
                    p.fullName = $filter('fullName')(p);
                    return p;
                });
                $ctrl.totalPresents = presents.length;
                $ctrl.totalPresentsThousandths = _.sumBy(presents, 'thousandths');
                $ctrl.totalPeople = changes.voters.currentValue.length;
                // FIX to check total matching thousanths total.
                $ctrl.totalThousandths = _.sumBy(changes.voters.currentValue, 'thousandths');

                $ctrl.tableParams.settings({ dataset: presents });
                if (angular.isDefined($ctrl.value) && $ctrl.value.length > 0) {
                    $ctrl.votes = _.zipObject(_.map($ctrl.value, 'personId'), _.map($ctrl.value, 'vote'));
                    onVoteChange();
                } else {
                    toggleAbsVotes();
                }
            }
        }

        function onVoteChange() {
            $ctrl.value = _.map($ctrl.votes, function (el, key) {
                return { personId: key, vote: el };
            });
            onChange();
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function toggleYesVotes() {
            var presents = _.filter($ctrl.voters, { present: true });
            $ctrl.votes = _.zipObject(_.map(presents, 'personId'), _.map(presents, _.constant('YES')));
            onVoteChange();
        }

        function toggleNoVotes() {
            var presents = _.filter($ctrl.voters, { present: true });
            $ctrl.votes = _.zipObject(_.map(presents, 'personId'), _.map(presents, _.constant('NO')));
            onVoteChange();
        }

        function toggleAbsVotes() {
            var presents = _.filter($ctrl.voters, { present: true });
            $ctrl.votes = _.zipObject(_.map(presents, 'personId'), _.map(presents, _.constant('ABS')));
            onVoteChange();
        }

    }

})();

