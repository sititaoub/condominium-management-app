/*
 * unrolling-expansion.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('unrollingExpansion', {
            controller: UnrollingExpansionController,
            bindings: {
                condominiumId: '<',
                meetingId: '<',
                meetingCall: '<',
                entries: '<'
            },
            templateUrl: 'app/meetings/unrolling/unrolling-expansion.html'
        });

    UnrollingExpansionController.$inject = ['$sce', '$timeout', 'MeetingUnrollingService'];

    /* @ngInject */
    function UnrollingExpansionController($sce, $timeout, MeetingUnrollingService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.$onChanges = onChanges;

        ////////////

        function onInit() {
            $ctrl.scrollbarConfig = {
                autoHideScrollbar: false,
                theme: 'dark',
                advanced: {
                    axis: 'y',
                    updateOnContentResize: true
                }
            };
            $timeout(function () {
                $ctrl.updateScrollbar('scrollTo', 'bottom');
            });
        }

        function onChanges(changes) {
            if (changes && changes.entries) {
                $ctrl.loading = true;
                MeetingUnrollingService.getExpansion(
                    $ctrl.condominiumId,
                    $ctrl.meetingId,
                    $ctrl.meetingCall
                ).then(function (content) {
                    $ctrl.content = $sce.trustAsHtml(content);
                    $timeout(function () {
                        $ctrl.updateScrollbar('scrollTo', 'bottom');
                    }, 500);
                }).finally(function () {
                    $ctrl.loading = false;
                });
            }
        }
    }

})();

