/*
 * meeting-unroll.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('meetingUnroll', {
            controller: MeetingUnrollController,
            bindings: {
                condominium: '<',
                meeting: '<',
                agenda: '<',
                unrolling: '<'
            },
            templateUrl: 'app/meetings/unrolling/meeting-unroll.html'
        });

    MeetingUnrollController.$inject = ['$uibModal', 'AlertService', 'NotifierService', 'MeetingService', 'MeetingUnrollingService', '_'];

    /* @ngInject */
    function MeetingUnrollController($uibModal, AlertService, NotifierService, MeetingService, MeetingUnrollingService, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.createAppeal = createAppeal;
        $ctrl.createVariation = createVariation;
        $ctrl.createAgenda = createAgenda;
        $ctrl.createVotation = createVotation;
        $ctrl.createText = createText;
        $ctrl.createClose = createClose;
        $ctrl.editEntry = editEntry;
        $ctrl.removeEntry = removeEntry;
        $ctrl.removeAllEntries = removeAllEntries;

        ////////////

        function onInit() {
        }

        function actualAppeal() {
            return _.last(_.filter($ctrl.unrolling.entries, function (e) {
                return e.type === 'APPEAL' || e.type === 'VARIATION'
            }))
        }
        
        function createAppeal() {
            $uibModal.open({
                size: 'lg',
                component: 'appealCreate',
                resolve: {
                    expectedParticipants: MeetingService.getExpectedParticipants($ctrl.condominium.id, $ctrl.meeting.id),
                    condominiumId: _.constant($ctrl.condominium.id),
                    meetingId: _.constant($ctrl.meeting.id),
                    call: _.constant($ctrl.unrolling.meetingCall)
                }
            }).result.then(function (entry) {
                $ctrl.unrolling.entries.push(entry);
                entriesChanged();
            });
        }

        function createVariation() {
            $uibModal.open({
                size: 'lg',
                component: 'variationCreate',
                resolve: {
                    actualAppeal: actualAppeal(),
                    expectedParticipants: MeetingService.getExpectedParticipants($ctrl.condominium.id, $ctrl.meeting.id),
                    condominiumId: _.constant($ctrl.condominium.id),
                    meetingId: _.constant($ctrl.meeting.id),
                    call: _.constant($ctrl.unrolling.meetingCall)
                }
            }).result.then(function (entry) {
                $ctrl.unrolling.entries.push(entry); // TODO Calculate variation.
                entriesChanged();
            });
        }

        function createAgenda() {
            $uibModal.open({
                size: 'lg',
                component: 'agendaCreate',
                resolve: {
                    condominium: _.constant($ctrl.condominium),
                    meeting: _.constant($ctrl.meeting),
                    call: _.constant($ctrl.unrolling.meetingCall)
                }
            }).result.then(function (entry) {
                entry.number = entry.meetingAgendaEntry.idx + 1;
                entry.title = entry.meetingAgendaEntry.title;
                $ctrl.unrolling.entries.push(entry);
                entriesChanged();
            });
        }

        function createVotation() {
            $uibModal.open({
                size: 'lg',
                component: 'votationCreate',
                resolve: {
                    condominium: _.constant($ctrl.condominium),
                    meeting: _.constant($ctrl.meeting),
                    call: _.constant($ctrl.unrolling.meetingCall)
                }
            }).result.then(function (entry) {
                $ctrl.unrolling.entries.push(entry);
                entriesChanged();
            })
        }

        function createText() {
            $uibModal.open({
                size: 'lg',
                component: 'textCreate',
                resolve: {
                    condominiumId: _.constant($ctrl.condominium.id),
                    meetingId: _.constant($ctrl.meeting.id),
                    call: _.constant($ctrl.unrolling.meetingCall)
                }
            }).result.then(function (entry) {
                $ctrl.unrolling.entries.push(entry);
                entriesChanged();
            })
        }

        function createClose() {
            $uibModal.open({
                size: 'lg',
                component: 'closeCreate',
                resolve: {
                    condominiumId: _.constant($ctrl.condominium.id),
                    meetingId: _.constant($ctrl.meeting.id),
                    call: _.constant($ctrl.unrolling.meetingCall)
                }
            }).result.then(function (entry) {
                $ctrl.unrolling.entries.push(entry);
                $ctrl.unrolling.closed = true;
                entriesChanged();
            });
        }

        function editAppeal(index, appeal) {
            $uibModal.open({
                size: 'lg',
                component: 'appealEdit',
                resolve: {
                    expectedParticipants: MeetingService.getExpectedParticipants($ctrl.condominium.id, $ctrl.meeting.id),
                    appeal: _.constant(appeal),
                    condominiumId: _.constant($ctrl.condominium.id),
                    meetingId: _.constant($ctrl.meeting.id),
                    call: _.constant($ctrl.unrolling.meetingCall),
                    entryId: _.constant(index)
                }
            }).result.then(function (entry) {
                $ctrl.unrolling.entries.splice(index, 1, entry);
                entriesChanged();
            });
        }

        function editVariation(index, variation) {
            $uibModal.open({
                size: 'lg',
                component: 'variationEdit',
                resolve: {
                    expectedParticipants: MeetingService.getExpectedParticipants($ctrl.condominium.id, $ctrl.meeting.id),
                    variation: _.constant(variation),
                    condominiumId: _.constant($ctrl.condominium.id),
                    meetingId: _.constant($ctrl.meeting.id),
                    call: _.constant($ctrl.unrolling.meetingCall),
                    entryId: _.constant(index)
                }
            }).result.then(function (entry) {
                $ctrl.unrolling.entries.splice(index, 1, entry);
                entriesChanged();
            });
        }

        function editAgenda(index, agenda) {
            $uibModal.open({
                size: 'lg',
                component: 'agendaEdit',
                resolve: {
                    agenda: _.constant(agenda),
                    condominium: _.constant($ctrl.condominium),
                    meeting: _.constant($ctrl.meeting),
                    call: _.constant($ctrl.unrolling.meetingCall),
                    entryId: _.constant(index)
                }
            }).result.then(function (entry) {
                entry.number = entry.meetingAgendaEntry.idx + 1;
                entry.title = entry.meetingAgendaEntry.title;
                $ctrl.unrolling.entries.splice(index, 1, entry);
                entriesChanged();
            });
        }

        function editVotation(index, votation) {
            $uibModal.open({
                size: 'lg',
                component: 'votationEdit',
                resolve: {
                    votation: _.constant(votation),
                    condominium: _.constant($ctrl.condominium),
                    meeting: _.constant($ctrl.meeting),
                    call: _.constant($ctrl.unrolling.meetingCall),
                    entryId: _.constant(index)
                }
            }).result.then(function (entry) {
                $ctrl.unrolling.entries.splice(index, 1, entry);
                entriesChanged();
            });
        }

        function editText(index, text) {
            $uibModal.open({
                size: 'lg',
                component: 'textEdit',
                resolve: {
                    text: _.constant(text),
                    condominiumId: _.constant($ctrl.condominium.id),
                    meetingId: _.constant($ctrl.meeting.id),
                    call: _.constant($ctrl.unrolling.meetingCall),
                    entryId: _.constant(index)
                }
            }).result.then(function (entry) {
                $ctrl.unrolling.entries.splice(index, 1, entry);
                entriesChanged();
            });
        }
        
        function editEntry(index, entry) {
            switch (entry.type) {
                case 'APPEAL':
                    return editAppeal(index, angular.copy(entry));
                case 'VARIATION':
                    return editVariation(index, angular.copy(entry));
                case 'AGENDA':
                    return editAgenda(index, angular.copy(entry));
                case 'VOTATION':
                    return editVotation(index, angular.copy(entry));
                case 'TEXT':
                    return editText(index, angular.copy(entry));
            }
        }

        function removeEntry(index) {
            AlertService.askConfirm('Meetings.Unroll.Remove.Message').then(function () {
                MeetingUnrollingService.remove(
                    $ctrl.condominium.id,
                    $ctrl.meeting.id,
                    $ctrl.unrolling.meetingCall,
                    index
                ).then(function () {
                    NotifierService.notifySuccess('Meetings.Unroll.Remove.Success');
                    var wasClose = $ctrl.unrolling.entries[index].type === 'CLOSE';
                    $ctrl.unrolling.entries.splice(index, 1);
                    if (wasClose) {
                        $ctrl.unrolling.closed = false;
                    }
                    entriesChanged();
                }).catch(function () {
                    NotifierService.notifyError('Meetings.Unroll.Remove.Failure');
                });
            });
        }

        function removeAllEntries() {
            AlertService.askConfirm('Meetings.Unroll.RemoveAll.Message').then(function () {
                MeetingUnrollingService.removeAll(
                    $ctrl.condominium.id,
                    $ctrl.meeting.id,
                    $ctrl.unrolling.meetingCall
                ).then(function () {
                    NotifierService.notifySuccess('Meetings.Unroll.RemoveAll.Success');
                    $ctrl.unrolling.entries = [];
                    entriesChanged();
                }).catch(function () {
                    NotifierService.notifyError('Meetings.Unroll.RemoveAll.Failure');
                });
            });
        }

        function entriesChanged() {
            $ctrl.unrolling.entries = angular.copy($ctrl.unrolling.entries);
        }
    }

})();

