/*
 * unrolling-timeline.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('unrollingTimeline', {
            controller: UnrollingTimelineController,
            bindings: {
                entries: '<',
                onEditEntry: '&',
                onRemoveEntry: '&'
            },
            templateUrl: 'app/meetings/unrolling/unrolling-timeline.html'
        });

    UnrollingTimelineController.$inject = ['$timeout'];

    /* @ngInject */
    function UnrollingTimelineController($timeout) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.$onChanges = onChanges;

        ////////////

        function onInit() {
            $ctrl.scrollbarConfig = {
                autoHideScrollbar: false,
                theme: 'dark',
                advanced: {
                    axis: 'y',
                    updateOnContentResize: true
                }
            };
            $timeout(function () {
                $ctrl.updateScrollbar('scrollTo', 'bottom');
            });
        }

        function onChanges(changes) {
            if (changes && changes.entries && $ctrl.updateScrollbar) {
                $ctrl.updateScrollbar('scrollTo', 'bottom');
            }
        }
    }

})();

