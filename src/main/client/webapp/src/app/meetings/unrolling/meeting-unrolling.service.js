/*
 * meeting-unrolling.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .factory('MeetingUnrollingService', MeetingUnrollingService);

    MeetingUnrollingService.$inject = ['$q', 'Restangular', 'contextAddressMeetings', '_', 'moment'];

    /* @ngInject */
    function MeetingUnrollingService($q, Restangular, contextAddressMeetings, _, moment) {
        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressMeetings + 'v1');
            configurer.addResponseInterceptor(function (data, operation, what, url, response) {
                if (operation === "post" && response.status === 201) {
                    var location = response.headers('Location');
                    if (location) {
                        return location.substr(location.lastIndexOf('/') + 1);
                    }
                }
                return data;
            });
        });

        var service = {
            getOne: getOne,
            getExpansion: getExpansion,
            getVoters: getVoters,
            addAppeal: addAppeal,
            updateAppeal: updateAppeal,
            addVariation: addVariation,
            updateVariation: updateVariation,
            addAgenda: addAgenda,
            updateAgenda: updateAgenda,
            addVotation: addVotation,
            updateVotation: updateVotation,
            addText: addText,
            updateText: updateText,
            addClose: addClose,
            remove: remove,
            removeAll: removeAll,
            generatePrints: generatePrints,
            mergeAndDownload: mergeAndDownload,
            archiveAndDownload: archiveAndDownload,
            sendUnrolling: sendUnrolling
        };
        return service;

        ////////////////

        /**
         * Meeting unrolling.
         *
         * @typedef {Object} Meetings~Unrolling
         * @memberOf Meetings
         * @property {number} meetingId - The unique id of meeting
         * @property {number} callId - The number of call.
         * @property {number} closed - True if the meeting unrolling is closed and cannot be modified.
         * @property {Meetings~UnrollingEntry[]} entries - The list of unrolling entries.
         */

        /**
         * Unrolling entry.
         *
         * @typedef {Object} Meetings~UnrollingEntry
         * @memberOf Meetings
         * @property {string} type - The type of entry.
         * @property {string} dateTime - The date and time of unrolling
         */

        /**
         * Appeal unrolling entry.
         *
         * @typedef {Object} Meetings~UnrollingAppealEntry
         * @memberOf Meetings
         * @extends Meetings~UnrollingEntry
         * @property {string} president - The name of president.
         * @property {string} secretary - The name of secretary.
         * @property {Meetings~AppealPresence[]} presences - The list of presences.
         */

        /**
         * The object identifying a presence.
         *
         * @typedef {Object} Meetings~AppealPresence
         * @memberOf Meetings
         * @property {number} personId - The unique id of person.
         * @property {string} personName - The name of person.
         * @property {string} delegate - The delegate of person.
         * @property {number} thousandths - The number of thousandths for this entry.
         */

        /**
         * Appeal variation entry.
         *
         * @typedef {Object} Meetings~UnrollingVariationEntry
         * @memberOf Meetings
         * @extends Meetings~UnrollingEntry
         * @property {Meetings~AppealVariation[]} variations - The list of variations.
         */

        /**
         * The object identifying a presence variation.
         *
         * @typedef {Object} Meetings~AppealVariation
         * @memberOf Meetings
         * @property {string} type - The type of variation.
         * @property {number} personId - The unique id of person.
         * @property {string} personName - The name of person.
         * @property {string} previousDelegate - The delegate of person.
         * @property {string} currentDelegate - The delegate of person.
         * @property {number} thousandths - The number of thousandths for this entry.
         */

        /**
         * Appeal text entry.
         *
         * @typedef {Object} Meetings~UnrollingTextEntry
         * @memberOf Meetings
         * @extends Meetings~UnrollingEntry
         * @property {number} [phraseId] - The unique id of phrase used.
         * @property {string} content - Full text content of this entry.
         */

        /**
         * Close entry.
         *
         * @typedef {Object} Meetings~UnrollingCloseEntry
         * @memberOf Meetings
         * @extends Meetings~UnrollingEntry
         */

        /**
         * Retrieve a single unrolling.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @returns {Promise<Meetings~Unrolling|Error>} - The promise of unrolling
         */
        function getOne(condominiumId, meetingId, callId) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId).get().catch(function (err) {
                if (err.status === 404) {
                    return create(condominiumId, meetingId, callId).then(function () {
                        return getOne(condominiumId, meetingId, callId);
                    });
                }
                return $q.reject(err);
            }).then(function (unrolling) {
                var offset = moment().utcOffset();
                _.forEach(unrolling.entries, function (entry) {
                    entry.dateTime = moment(entry.dateTime).add(offset, 'minutes').toDate()
                });
                return unrolling;
            });
        }

        /**
         * Retrieve a single unrolling HTML expansion.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @returns {Promise<String|Error>} - The promise of unrolling expansion.
         */
        function getExpansion(condominiumId, meetingId, callId) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId).all("expansion")
                .get("").then(function (expansion) {
                    return expansion.content;
                });
        }

        /**
         * Retrieve the voters for an entry of an agenda.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique manager id.
         * @param {number} callId - The unique id of meeting call.
         * @param {number} index - The index
         * @param {string} votersType - The type of participants.
         * @param {number[]} voters - The list of participants to retrieve.
         * @param {number} propertyTableId - The unique id of property table.
         * @return {Promise<Meetings~AgendaVoter[]|Error>} - The promise of voters
         */
        function getVoters(condominiumId, meetingId, callId, index, votersType, voters, propertyTableId) {
            return Api.one('condominiums', condominiumId).one('meetings', meetingId).one("call", callId)
                .one('agenda', index).all('voters').getList({
                    votersType: votersType,
                    voters: voters,
                    propertyTableId: propertyTableId
                });
        }

        /**
         * Create a new unrolling for supplied parameters.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @returns {Promise<number|Error>} - The promise of call id
         */
        function create(condominiumId, meetingId, callId) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId).customPOST({});
        }

        /**
         * Adds a new appeal entry to meeting call.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @param {Meetings~UnrollingAppealEntry} appeal - The appeal.
         * @return {Promise<Void|Error>} - The promise of operation outcome.
         */
        function addAppeal(condominiumId, meetingId, callId, appeal) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId)
                .customPOST(appeal, "entries", { type: 'appeal' });
        }

        /**
         * Adds a new appeal entry to meeting call.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @param {number} entryId - The unique id of entry.
         * @param {Meetings~UnrollingAppealEntry} appeal - The appeal.
         * @return {Promise<Void|Error>} - The promise of operation outcome.
         */
        function updateAppeal(condominiumId, meetingId, callId, entryId, appeal) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId)
                .one("entries", entryId).customPUT(appeal, undefined, { type: 'appeal' });
        }

        /**
         * Adds a new appeal variation to meeting call.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @param {Meetings~UnrollingVariationEntry} appeal - The appeal.
         * @return {Promise<Void|Error>} - The promise of operation outcome.
         */
        function addVariation(condominiumId, meetingId, callId, appeal) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId)
                .customPOST(appeal, "entries", { type: 'variation' });
        }

        /**
         * Updates an existing appeal variation to meeting call.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @param {number} entryId - The unique id of entry.
         * @param {Meetings~UnrollingVariationEntry} appeal - The appeal.
         * @return {Promise<Void|Error>} - The promise of operation outcome.
         */
        function updateVariation(condominiumId, meetingId, callId, entryId, appeal) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId)
                .one("entries", entryId).customPUT(appeal, undefined, { type: 'variation' });
        }

        /**
         * Adds a new agenda to meeting call.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @param {Meetings~UnrollingAgendaEntry} agenda - The agenda.
         * @return {Promise<Void|Error>} - The promise of operation outcome.
         */
        function addAgenda(condominiumId, meetingId, callId, agenda) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId)
                .customPOST(agenda, "entries", { type: 'agenda' });
        }

        /**
         * Updates an existing agenda to meeting call.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @param {number} entryId - The unique id of entry.
         * @param {Meetings~UnrollingAgendaEntry} agenda - The agenda.
         * @return {Promise<Void|Error>} - The promise of operation outcome.
         */
        function updateAgenda(condominiumId, meetingId, callId, entryId, agenda) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId)
                .one("entries", entryId).customPUT(agenda, undefined, { type: 'agenda' });
        }

        /**
         * Adds a new votation to meeting call.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @param {Meetings~UnrollingVotationEntry} votation - The votation.
         * @return {Promise<Void|Error>} - The promise of operation outcome.
         */
        function addVotation(condominiumId, meetingId, callId, votation) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId)
                .customPOST(votation, "entries", { type: 'votation' });
        }

        /**
         * Updates an existing votation to meeting call.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @param {number} entryId - The unique id of entry.
         * @param {Meetings~UnrollingVotationEntry} votation - The votation.
         * @return {Promise<Void|Error>} - The promise of operation outcome.
         */
        function updateVotation(condominiumId, meetingId, callId, entryId, votation) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId)
                .one("entries", entryId).customPUT(votation, undefined, { type: 'votation' });
        }

        /**
         * Adds a new text entry to meeting call.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @param {Meetings~UnrollingTextEntry} text - The text.
         * @return {Promise<Void|Error>} - The promise of operation outcome.
         */
        function addText(condominiumId, meetingId, callId, text) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId)
                .customPOST(text, "entries", { type: 'text' });
        }

        /**
         * Adds a new appeal variation to meeting call.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @param {number} entryId - The unique id of entry.
         * @param {Meetings~UnrollingTextEntry} text - The text.
         * @return {Promise<Void|Error>} - The promise of operation outcome.
         */
        function updateText(condominiumId, meetingId, callId, entryId, text) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId)
                .one("entries", entryId).customPUT(text, undefined, { type: 'text' });
        }

        /**
         * Adds a new close entry to meeting call.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @param {Meetings~UnrollingCloseEntry} close - The close entry.
         * @return {Promise<Void|Error>} - The promise of operation outcome.
         */
        function addClose(condominiumId, meetingId, callId, close) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId)
                .customPOST(close, "entries", { type: 'close' });
        }

        /**
         * Remove the entry with supplied id
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @param {number} entryId - The unique id of entry.
         * @return {Promise<Void|Error>} - The promise of operation outcome.
         */
        function remove(condominiumId, meetingId, callId, entryId) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId)
                .one("entries", entryId).remove();
        }

        /**
         * Remove all the entries in a meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @return {Promise<Void|Error>} - The promise of operation outcome.
         */
        function removeAll(condominiumId, meetingId, callId) {
            return Api.one("condominiums", condominiumId).one("meetings", meetingId).one("call", callId).all("entries")
                .remove();
        }


        /**
         * Generate prints for supplied meetings
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique id of meeting
         * @param {number} callId - The unique id of meeting call.
         * @param {number} templateId - The unique id of template to use to generate prints
         * @returns {Promise<Object|Error>} - The promise with operation outcome.
         */
        function generatePrints(condominiumId, id, callId, templateId) {
            return Api.one('condominiums', condominiumId).one('meetings', id).one('call', callId).all('documents').post({
                templateId: templateId
            });
        }

        /**
         * Generate prints for supplied meetings
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique id of meeting
         * @param {number} callId - The unique id of meeting call.
         * @param {number} templateId - The unique id of template to use to generate prints
         * @param {number[]} participantsId - The array of unique participants identifiers
         * @returns {Promise<Object|Error>} - The promise with operation outcome.
         */
        function mergeAndDownload(condominiumId, id, callId, templateId, participantsId) {
            return Api.one('condominiums', condominiumId).one('meetings', id).one('call', callId).all('documents')
                .withHttpConfig({ responseType: 'arraybuffer', cache: false })
                .customPOST({
                    templateId: templateId,
                    personIds: participantsId
                }, "download", undefined, {
                    accept: 'application/pdf'
                });
        }

        /**
         * Generate prints for supplied meetings
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} id - The unique id of meeting
         * @param {number} callId - The unique id of meeting call.
         * @param {number} templateId - The unique id of template to use to generate prints
         * @param {number[]} participantsId - The array of unique participants identifiers
         * @returns {Promise<Object|Error>} - The promise with operation outcome.
         */
        function archiveAndDownload(condominiumId, id, callId, templateId, participantsId) {
            return Api.one('condominiums', condominiumId).one('meetings', id).one('call', callId).all('documents')
                .withHttpConfig({ responseType: 'arraybuffer', cache: false })
                .customPOST({
                    templateId: templateId,
                    personIds: participantsId
                }, "download", undefined, {
                    accept: 'application/zip'
                });
        }

        /**
         * Send the summoning for supplied meeting.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} meetingId - The unique id of meeting.
         * @param {number} callId - The unique id of meeting call.
         * @param {number} templateId - The unique id of template.
         * @param {Meetings~SendConfiguration[]} participants - The list of send configuration for participants.
         * @return {Promise<Void|Error>} - The promise of operation outcome
         */
        function sendUnrolling(condominiumId, meetingId, callId, templateId, participants) {
            return Api.one('condominiums', condominiumId).one('meetings', meetingId).one('call', callId).all('documents')
                .customPOST({
                    templateId: templateId,
                    configurations: participants
                }, "send");
        }
    }

})();

