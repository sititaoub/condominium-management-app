/*
 * meeting-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('meetingCreate', {
            controller: MeetingCreateController,
            bindings: {
                condominium: '<'
            },
            templateUrl: 'app/meetings/meeting-create.html'
        });

    MeetingCreateController.$inject = ['$state', '$timeout', 'MeetingService', 'FinancialPeriodService', 'NotifierService', 'moment'];

    /* @ngInject */
    function MeetingCreateController($state, $timeout, MeetingService, FinancialPeriodService, NotifierService, moment) {
        var $ctrl = this;

        var EXACTLY_ONE_DAY = moment.duration(1, 'day').as('milliseconds');

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;
        $ctrl.firstCallDateChanged = firstCallDateChanged;

        ////////////

        function onInit() {
            $ctrl.value = {
                meeting: {
                    firstCall: {
                        address: angular.copy($ctrl.condominium.address)
                    },
                    secondCall: {
                        address: angular.copy($ctrl.condominium.address)
                    }
                },
                participants: {
                    participantsType: 'ALL'
                }
            };
            var cfp = FinancialPeriodService.getCurrent($ctrl.condominium.companyId, $ctrl.condominium.id);
            if (angular.isDefined(cfp)) {
                $ctrl.value.meeting.financialPeriod = angular.copy(cfp);
            }
        }

        function firstCallDateChanged(previousDateTime, dateTime) {
            var changeSecondCall = true;
            var firstMoment = moment(previousDateTime);
            var newFirstMoment = moment(dateTime);
            var newLastMoment = moment(dateTime).add(10, 'days');
            if (angular.isDefined($ctrl.value.meeting.secondCall.dateTime)) {
                var secondMoment = moment($ctrl.value.meeting.secondCall.dateTime);
                var diff = secondMoment.diff(firstMoment);
                changeSecondCall = diff === EXACTLY_ONE_DAY || secondMoment.isBefore(newFirstMoment)
                    || secondMoment.isAfter(newLastMoment);
            }
            if (changeSecondCall) {
                $timeout(function () {
                    $ctrl.value.meeting.secondCall.dateTime = moment(dateTime).add(1, 'day').utc().toDate();
                    $ctrl.value.meeting.secondCall = angular.copy($ctrl.value.meeting.secondCall);
                });
            }
        }

        function doSave() {
            return MeetingService.create($ctrl.condominium.id, $ctrl.value.meeting, $ctrl.value.participants, $ctrl.value.defaultAgenda).then(function () {
                return NotifierService.notifySuccess('Meetings.Create.Success');
            }).then(function () {
                return $state.go("^");
            }).catch(function () {
                return NotifierService.notifyError('Meetings.Create.Failure');
            });
        }
    }

})();

