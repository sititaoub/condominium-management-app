/*
 * agenda-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('agendaEdit', {
            controller: AgendaEditController,
            bindings: {
                resolve: '<',
                dismiss: '&',
                close: '&'
            },
            templateUrl: 'app/meetings/agenda/agenda-edit.html'
        });

    AgendaEditController.$inject = ['$q', 'moment', 'MeetingUnrollingService', 'NotifierService'];

    /* @ngInject */
    function AgendaEditController($q, moment, MeetingUnrollingService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;

        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.agenda = $ctrl.resolve.agenda;
            $ctrl.agenda.dateTime = new Date($ctrl.agenda.dateTime);
            if (angular.isUndefined($ctrl.agenda.meetingAgendaEntry)) {
                $ctrl.agenda.meetingAgendaEntry = {idx: $ctrl.agenda.id};
            }
        }

        function doSave() {
            var agenda = angular.copy($ctrl.agenda);
            agenda.meetingAgendaEntryId = $ctrl.agenda.meetingAgendaEntry.idx;
            delete agenda.meetingAgendaEntry;

            return MeetingUnrollingService.updateAgenda(
                $ctrl.resolve.condominium.id,
                $ctrl.resolve.meeting.id,
                $ctrl.resolve.call,
                $ctrl.resolve.entryId,
                agenda
            ).then(function () {
                return NotifierService.notifySuccess('Meetings.Unroll.Agenda.Edit.Success');
            }).then(function () {
                return $ctrl.close({ $value: angular.copy($ctrl.agenda) });
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Unroll.Agenda.Edit.Failure');
                return $q.reject(err);
            })
        }
    }

})();

