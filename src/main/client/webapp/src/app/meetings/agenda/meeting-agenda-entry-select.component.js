/*
 * agenda-chooser.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('meetingAgendaEntrySelect', {
            controller: MeetingAgendaEntrySelectController,
            bindings: {
                id: '@?',
                name: '@?',
                controlClass: '@?',
                condominiumId: '<',
                meetingId: '<',
                ngModel: '<',
                ngDisabled: '<',
                ngRequired: '<',
                onEntriesLoaded: '&'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/meetings/agenda/meeting-agenda-entry-select.html'
        });

    MeetingAgendaEntrySelectController.$inject = ['MeetingService', 'NotifierService'];

    /* @ngInject */
    function MeetingAgendaEntrySelectController(MeetingService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            loadAgenda();
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function loadAgenda() {
            MeetingService.getAgenda($ctrl.condominiumId, $ctrl.meetingId).then(function (agenda) {
                $ctrl.agenda = _.map(agenda, function (entry, idx) {
                    entry.idx = idx;
                    return entry;
                });
                if (angular.isDefined($ctrl.ngModel) && angular.isUndefined($ctrl.ngModel.idx)) {
                    $ctrl.ngModel.idx = _.findIndex($ctrl.agenda, { id: $ctrl.ngModel.id });
                }
                $ctrl.onEntriesLoaded();
            }).catch(function () {
                NotifierService.notifyError();
            });
        }
    }

})();

