/*
 * agenda-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('agendaInput', {
            controller: AgendaInputController,
            bindings: {
                ngModel: '<',
                condominium: '<',
                meeting: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/meetings/agenda/agenda-input.html'
        });

    AgendaInputController.$inject = [];

    /* @ngInject */
    function AgendaInputController() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;

        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

