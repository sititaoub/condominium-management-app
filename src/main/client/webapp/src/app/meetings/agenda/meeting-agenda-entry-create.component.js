/*
 * meeting-agenda-entry-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('meetingAgendaEntryCreate', {
            controller: MeetingAgendaEntryCreateController,
            bindings: {
                close: '&',
                dismiss: '&',
                resolve: '<'
            },
            templateUrl: 'app/meetings/agenda/meeting-agenda-entry-create.html'
        });

    MeetingAgendaEntryCreateController.$inject = ['MeetingService', 'NotifierService', '$q'];

    /* @ngInject */
    function MeetingAgendaEntryCreateController(MeetingService, NotifierService, $q) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.condominium = $ctrl.resolve.condominium;
            $ctrl.meeting = $ctrl.resolve.meeting;
            $ctrl.entry = {};
        }

        function doSave() {
            return MeetingService.addEntry($ctrl.condominium.id, $ctrl.meeting.id, $ctrl.entry).then(function () {
                return NotifierService.notifySuccess('Meetings.Agenda.Create.Success');
            }).then(function () {
                return $ctrl.close({ $value: $ctrl.entry });
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Agenda.Create.Failure');
                return $q.reject(err);
            });
        }
    }

})();

