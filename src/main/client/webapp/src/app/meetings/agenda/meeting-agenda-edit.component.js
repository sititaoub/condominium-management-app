/*
 * meeting-agenda-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('meetingAgendaEdit', {
            controller: MeetingAgendaEditController,
            bindings: {
                'condominium': '<',
                'meeting': '<',
                'agenda': '<'
            },
            templateUrl: 'app/meetings/agenda/meeting-agenda-edit.html'
        });

    MeetingAgendaEditController.$inject = ['$state', '$uibModal', 'NgTableParams', 'MeetingService', 'AlertService', 'NotifierService', '_'];

    /* @ngInject */
    function MeetingAgendaEditController($state, $uibModal, NgTableParams, MeetingService, AlertService, NotifierService, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.moveUp = moveUp;
        $ctrl.doCreate = doCreate;
        $ctrl.doEdit = doEdit;
        $ctrl.moveDown = moveDown;
        $ctrl.doDelete = doDelete;

        ////////////

        function onInit() {
            $ctrl.progress = false;
            $ctrl.tableParams = new NgTableParams({ count: 100 }, { counts: [], dataset: angular.copy($ctrl.agenda) });
        }

        function doCreate() {
            $uibModal.open({
                component: 'meetingAgendaEntryCreate',
                size: 'lg',
                resolve: {
                    condominium: _.constant($ctrl.condominium),
                    meeting: _.constant($ctrl.meeting)
                }
            }).result.then(function () {
                $state.go('.', undefined, { reload: true });
            });
        }

        function doEdit(index) {
            $uibModal.open({
                component: 'meetingAgendaEntryEdit',
                size: 'lg',
                resolve: {
                    condominium: _.constant($ctrl.condominium),
                    meeting: _.constant($ctrl.meeting),
                    index: _.constant(index),
                    entry: MeetingService.getEntry($ctrl.condominium.id, $ctrl.meeting.id, index)
                }
            }).result.then(function () {
                $state.go('.', undefined, { reload: true });
            });
        }

        function moveUp(index) {
            if (index > 0) {
                startProgress();
                MeetingService.moveEntryUp($ctrl.condominium.id, $ctrl.meeting.id, index).then(function () {
                    var tmp = $ctrl.agenda[index - 1];
                    $ctrl.agenda[index - 1] = $ctrl.agenda[index];
                    $ctrl.agenda[index] = tmp;
                    refreshTable();
                }).catch(function () {
                    NotifierService.notifyError('Meetings.Agenda.List.MoveError');
                }).finally(function () {
                    endProgress();
                });
            }
        }

        function moveDown(index) {
            if (index < $ctrl.agenda.length) {
                startProgress();
                MeetingService.moveEntryDown($ctrl.condominium.id, $ctrl.meeting.id, index).then(function () {
                    var tmp = $ctrl.agenda[index];
                    $ctrl.agenda[index] = $ctrl.agenda[index + 1];
                    $ctrl.agenda[index + 1] = tmp;
                    refreshTable();
                }).catch(function () {
                    NotifierService.notifyError('Meetings.Agenda.List.MoveError');
                }).finally(function () {
                    endProgress();
                });
            }
        }

        function doDelete(index) {
            AlertService.askConfirm('Meetings.Agenda.Remove.Message', $ctrl.agenda[index]).then(function () {
                MeetingService.removeEntry($ctrl.condominium.id, $ctrl.meeting.id, index).then(function () {
                    NotifierService.notifySuccess('Meetings.Agenda.Remove.Success');
                    $ctrl.agenda.splice(index, 1);
                    refreshTable();
                }).catch(function () {
                    NotifierService.notifyError('Meetings.Agenda.Remove.Failure');
                });
            });
        }

        function startProgress() {
            $ctrl.progress = true;
        }

        function endProgress() {
            $ctrl.progress = false;
        }

        function refreshTable() {
            $ctrl.tableParams.settings({ dataset: angular.copy($ctrl.agenda) });
        }
    }

})();

