/*
 * agenda-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('agendaCreate', {
            controller: AgendaCreateController,
            bindings: {
                resolve: '<',
                dismiss: '&',
                close: '&'
            },
            templateUrl: 'app/meetings/agenda/agenda-create.html'
        });

    AgendaCreateController.$inject = ['$q', 'moment', 'MeetingUnrollingService', 'NotifierService'];

    /* @ngInject */
    function AgendaCreateController($q, moment, MeetingUnrollingService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;

        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.agenda = {
                type: 'AGENDA',
                dateTime: moment().subtract(moment().utcOffset()).toDate(),
            };
        }

        function doSave() {
            var agenda = angular.copy($ctrl.agenda);
            agenda.meetingAgendaEntryId = $ctrl.agenda.meetingAgendaEntry.idx;
            delete agenda.meetingAgendaEntry;

            return MeetingUnrollingService.addAgenda(
                $ctrl.resolve.condominium.id,
                $ctrl.resolve.meeting.id,
                $ctrl.resolve.call,
                agenda
            ).then(function () {
                return NotifierService.notifySuccess('Meetings.Unroll.Agenda.Create.Success');
            }).then(function () {
                return $ctrl.close({ $value: angular.copy($ctrl.agenda) });
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Unroll.Agenda.Create.Failure');
                return $q.reject(err);
            })
        }
    }

})();

