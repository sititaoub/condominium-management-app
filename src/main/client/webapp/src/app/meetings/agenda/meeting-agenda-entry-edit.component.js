/*
 * meeting-agenda-entry-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('meetingAgendaEntryEdit', {
            controller: MeetingAgendaEntryEditController,
            bindings: {
                close: '&',
                dismiss: '&',
                resolve: '<'
            },
            templateUrl: 'app/meetings/agenda/meeting-agenda-entry-edit.html'
        });

    MeetingAgendaEntryEditController.$inject = ['MeetingService', 'NotifierService', '$q'];

    /* @ngInject */
    function MeetingAgendaEntryEditController(MeetingService, NotifierService, $q) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.condominium = $ctrl.resolve.condominium;
            $ctrl.meeting = $ctrl.resolve.meeting;
            $ctrl.index = $ctrl.resolve.index;
            $ctrl.entry = $ctrl.resolve.entry;
        }

        function doSave() {
            return MeetingService.updateEntry($ctrl.condominium.id, $ctrl.meeting.id, $ctrl.index, $ctrl.entry).then(function () {
                return NotifierService.notifySuccess('Meetings.Agenda.Edit.Success');
            }).then(function () {
                return $ctrl.close({ $value: $ctrl.entry });
            }).catch(function (err) {
                NotifierService.notifyError('Meetings.Agenda.Edit.Failure');
                return $q.reject(err);
            });
        }
    }

})();

