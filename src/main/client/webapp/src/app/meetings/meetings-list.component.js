/*
 * meetings-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('meetingsList', {
            controller: MeetingsListController,
            bindings: {
                'condominium': '<'
            },
            templateUrl: 'app/meetings/meetings-list.html'
        });

    MeetingsListController.$inject = ['$state', '$uibModal', 'NgTableParams', 'MeetingService', 'NotifierService', 'AlertService', '_'];

    /* @ngInject */
    function MeetingsListController($state, $uibModal, NgTableParams, MeetingService, NotifierService, AlertService, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.toggleFilters = toggleFilters;
        $ctrl.summonParticipants = summonParticipants;
        $ctrl.editParticipants = editParticipants;
        $ctrl.unrollMeeting = unrollMeeting;
        $ctrl.sendMeetingUnroll = sendMeetingUnroll;
        $ctrl.deleteMeeting = deleteMeeting;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10,
                sort: 'financialPeriodId,asc'
            }, {
                getData: loadMeetings
            }, 'm_');
            $ctrl.showFilters = $ctrl.tableParams.hasFilter();
        }

        function toggleFilters() {
            $ctrl.showFilters = !$ctrl.showFilters;
        }

        function deleteMeeting(meeting) {
            AlertService.askConfirm('Meetings.Remove.Message', meeting).then(function () {
                MeetingService.remove($ctrl.condominium.id, meeting.id).then(function () {
                    return NotifierService.notifySuccess('Meetings.Remove.Success');
                }).catch(function () {
                    return NotifierService.notifyError('Meetings.Remove.Failure');
                }).finally(function () {
                    $ctrl.tableParams.reload();
                })
            });
        }

        function unrollMeeting(meeting) {
            $uibModal.open({
                component: 'callChooser',
                resolve: {
                    meeting: _.identity(meeting)
                }
            }).result.then(function (call) {
                $state.go('.unroll', { meetingId: meeting.id, callId: call });
            });
        }

        function sendMeetingUnroll(meeting) {
            $uibModal.open({
                component: 'callChooser',
                resolve: {
                    meeting: _.identity(meeting)
                }
            }).result.then(function (call) {
                $state.go('.unroll.send', { meetingId: meeting.id, callId: call });
            })
        }

        function summonParticipants(meeting) {
            $uibModal.open({
                component: 'participantsSummon',
                size: 'lg',
                resolve: {
                    condominium: _.identity($ctrl.condominium),
                    meeting: _.identity(meeting),
                    participants: MeetingService.getParticipants($ctrl.condominium.id, meeting.id)
                }
            }).result.then(function () {
                $ctrl.tableParams.reload();
            });
        }

        function editParticipants(meeting) {
            $uibModal.open({
                component: 'participantsEdit',
                size: 'lg',
                resolve: {
                    condominium: _.constant($ctrl.condominium),
                    meeting: _.constant(meeting),
                    participants: MeetingService.getParticipants($ctrl.condominium.id, meeting.id)
                }
            });
        }

        function loadMeetings(params) {
            $state.go('.', params.uiRouterUrl('m_'), { inherit: false });
            return MeetingService.getPage($ctrl.condominium.id, params.page() - 1, params.count(), params.sorting(), params.filter())
                .then(function (data) {
                    params.total(data.totalElements);
                    return data.content;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    $ctrl.loading = false;
                });
        }
    }

})();

