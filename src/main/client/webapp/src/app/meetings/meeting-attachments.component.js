/*
 * meeting-attachments.component.js
 *
 * (C) 2018-2018 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp')
        .component('meetingAttachments', {
            controller: MeetingAttachmentsController,
            bindings: {
                'condominium': '<',
                'meeting': '<'
            },
            templateUrl: 'app/meetings/meeting-attachments.html'
        });

    MeetingAttachmentsController.$inject = ['NgTableParams', 'MeetingService', 'Upload', 'AlertService', 'NotifierService'];

    /* @ngInject */
    function MeetingAttachmentsController(NgTableParams, MeetingService, Upload, AlertService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.upload = upload;
        $ctrl.remove = remove;

        ////////////

        function onInit() {
            $ctrl.tableParams = new NgTableParams({
                page: 1,
                count: 10,
                sorting: {
                    fileName: 'asc'
                }
            }, {
                dataset: []
            });
            loadAttachments();
        }

        function upload(files) {
            if (files.length > 0) {
                Upload.upload({
                    url: MeetingService.uploadAttachment($ctrl.condominium.id, $ctrl.meeting.id),
                    data: {file: files[0]}
                }).then(function () {
                    NotifierService.notifySuccess('Meetings.Attachments.UploadSuccess');
                    loadAttachments();
                }).catch(function () {
                    NotifierService.notifyError('Meetings.Attachments.UploadFailure');
                });
            }
        }

        function remove(attachment) {
            AlertService.askConfirm('Meetings.Attachments.Remove.Message', attachment).then(function () {
                MeetingService.removeAttachment($ctrl.condominium.id, $ctrl.meeting.id, attachment.id).then(function () {
                    return NotifierService.notifySuccess('Meetings.Attachments.Remove.Success');
                }).catch(function () {
                    return NotifierService.notifyError('Meetings.Attachments.Remove.Failure');
                }).finally(function () {
                    loadAttachments();
                })
            });
        }

        function loadAttachments() {
            MeetingService.getAttachments($ctrl.condominium.id, $ctrl.meeting.id).then(function (attachments) {
                $ctrl.tableParams.settings({ dataset: attachments.plain() });
            }).catch(function () {
                NotifierService.notifyError();
            });
        }
    }

})();

