/*
 * budget.routes.spec.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
describe('budget routes', function () {
    'use strict';

    var $httpBackend, $q, $state, $templateCache, $location;

    var financialPeriodServiceMock = jasmine.createSpyObj('FinancialPeriodService', ['getCurrent']);

    function mockServices($provide) {
        $provide.factory('FinancialPeriodService', function () {
            return financialPeriodServiceMock;
        });
    }

    function services($injector) {
        $q = $injector.get('$q');
        $state = $injector.get('$state');
        $templateCache = $injector.get('$templateCache');
        $location = $injector.get('$location');
        $httpBackend = $injector.get('$httpBackend');
    }

    function setUp() {
        $httpBackend.whenGET(/.+i18n.+/).respond(200, {});
        $httpBackend.whenGET(/assets\/(.+)/).respond(200, {});
        $httpBackend.whenGET(/users\/me$/).respond(200, {});
    }

    beforeEach(function () {
        module('condominiumManagementApp', 'condominiumManagementApp.templates', mockServices);
        inject(services);
        setUp();
    });

    describe('when navigating to /index/condominiums/8/financial-period/current/budget`', function () {
        function goTo(url) {
            $location.url(url);
            $httpBackend.flush();
        }

        describe('and current financial period is not set', function () {
            beforeEach(function () {
                financialPeriodServiceMock.getCurrent.and.returnValue(undefined);
            });

            it('transition to financial period select', function () {
                goTo('/index/condominiums/8/financial-period/current/budget');

                expect($state.current.name).toBe('index.financial-period.select');
                expect(financialPeriodServiceMock.getCurrent).toHaveBeenCalledWith(undefined, '8');
            });
        });

        describe('and current financial period is set', function () {
            beforeEach(function () {
                financialPeriodServiceMock.getCurrent.and.returnValue({ id: 1 });
            });

            it('show the state', function () {
                goTo('/index/condominiums/8/financial-period/current/budget');

                expect($state.current.name).toBe('index.budget.edit');
                expect($state.params.condominiumId).toBe('8');
                expect($state.params.periodId).toBe('current');
                expect(financialPeriodServiceMock.getCurrent).toHaveBeenCalledWith(undefined, '8');
            });
        });
    });

    describe('when navigating to /index/condominiums/8/financial-period/1/budget`', function () {
        function goTo(url) {
            $location.url(url);
            $httpBackend.flush();
        }

        describe('and current financial period is not set', function () {
            beforeEach(function () {
                financialPeriodServiceMock.getCurrent.and.returnValue(undefined);
            });

            it('transition to financial period select', function () {
                goTo('/index/condominiums/8/financial-period/1/budget');

                expect($state.current.name).toBe('index.financial-period.select');
                expect(financialPeriodServiceMock.getCurrent).toHaveBeenCalledWith(undefined, '8');
            });
        });

        describe('and current financial period is set', function () {
            beforeEach(function () {
                financialPeriodServiceMock.getCurrent.and.returnValue({ id: 1 });
            });

            it('show the state', function () {
                goTo('/index/condominiums/8/financial-period/1/budget');

                expect($state.current.name).toBe('index.budget.edit');
                expect($state.params.condominiumId).toBe('8');
                expect($state.params.periodId).toBe('1');
                expect(financialPeriodServiceMock.getCurrent).toHaveBeenCalledWith(undefined, '8');
            });
        });
    });
});