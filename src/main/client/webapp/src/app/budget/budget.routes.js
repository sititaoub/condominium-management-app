/*
 * budget.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.budget')
        .config(setupRoutes);

        setupRoutes.$inject = ['$stateProvider'];

        /* @ngInject */
        function setupRoutes($stateProvider) {
            $stateProvider.state('index.budget', {
                url: '/condominiums/{condominiumId}/financial-period/{periodId}/budget',
                abstract: true,
                template: '<ui-view></ui-view>',
                resolve:{
                    financialPeriod: ['$transition$', 'FinancialPeriodService', function ($transition$, FinancialPeriodService) {
                        if ($transition$.params().periodId !== 'current') {
                            return FinancialPeriodService.getOne($transition$.params().companyId, $transition$.params().periodId);
                        } else {
                            var periodId=FinancialPeriodService.getCurrent($transition$.params().companyId, $transition$.params().condominiumId).id;
                            return FinancialPeriodService.getOne($transition$.params().companyId, periodId);
                        }
                    }]
                }
            }).state('index.budget.edit', {
                url: '',
                controller: 'BudgetEditController',
                controllerAs: 'vm',
                templateUrl: 'app/budget/budget-edit.html',
                onEnter: ['$state', '$transition$','financialPeriod','NotifierService', function ($state, $transition$,financialPeriod,NotifierService) {
                    if(financialPeriod!=null && financialPeriod.type ==='HISTORICAL'){
                        NotifierService.notifyError('Budget.HISTORICAL_FINANCIAL_PERIOD_OPEN_ERROR');
                        return false;
                    }
                    return true;
                }]
            });
        }

})();