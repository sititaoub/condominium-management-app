/*
 * budget.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.budget')
        .factory('BudgetService', BudgetService);

    BudgetService.$inject = ['$q', 'Restangular', 'contextAddressManagement', '$uibModal','$window'], 'BudgetEditController';

    /* @ngInject */
    function BudgetService($q, Restangular, contextAddressManagement, $uibModal,$window, BudgetEditController) {
        var Budgets = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressManagement + 'v1');
        }).all('quotes');

        var service = {
            getOne: getOne,
            getOneByPersons: getOneByPersons,
            update: update,
            confirm: confirm,
            unconfirm: unconfirm,
            open: open,
            getReport: getReport
        };
        return service;

        ////////////////

        /**
         * A budget object.
         *
         * @typedef {Object} Budget~Budget
         * @property {Number} id - The unique id of budget.
         * @property {Number} condominiumId - The unique id of condominium
         * @property {Number} financialPeriodId - The unique id of financial period
         * @property {Boolean} confirmed - True if budget was confirmed.
         * @property {Budget~Row[]} rows - The list of rows for this budget.
         */

        /**
         * The budget row.
         *
         * @typedef {Object} Budget~Row
         * @property {Number} id - The unique row id.
         * @property {String} materializedPath - The full fledged account Number.
         * @property {String} accountName - The account name.
         * @property {String} subAccountName - The sub account name.
         * @property {Number} previousBudget - The previous year budget.
         * @property {Number} previousFinal - The previous year final.
         * @property {Boolean} hasBreakdownCriteria - True if the account has breakdown criteria and false if not.
         * @property {Boolean} useBudget - True to use the budget and false to use the final value.
         * @property {Number} correctionPercentage - The correction percentage over the previous value.
         * @property {Number} correctionValue - The correction value over the previous value.
         * @property {Number} budgetValue - The budget value.
         */

        /**
         * Retrieve a single budget.
         *
         * @param {Number} condominiumId - The unique id of condominium
         * @param {Number} financialPeriodId - The unique id of financial period
         * @returns {Promise<Budget~Budget|Error>} - The promise over the budget object.
         */
        function getOne(condominiumId, financialPeriodId) {
            return Budgets.get("", { condominiumId: condominiumId, financialPeriodId: financialPeriodId }).then(function (page) {
                return page.content[0];
            });
        }

        /**
         * A budget by person object.
         *
         * @typedef {Object} Budget~BudgetByPerson
         * @property {Number} id - The unique id of budget.
         * @property {Number} condominiumId - The unique id of condominium
         * @property {Number} financialPeriodId - The unique id of financial period
         * @property {Boolean} confirmed - True if budget was confirmed.
         * @property {Budget~PersonRow[]} rows - The list of rows for this budget.
         */

        /**
         * The budget row.
         *
         * @typedef {Object} Budget~PersonRow
         * @property {Number} personId - The unique id of person.
         * @property {String} firstName - The first name.
         * @property {String} lastName - The last name
         * @property {String} companyName - The company name.
         * @property {String} role - The person role.
         * @property {Number} housingUnitId - The housing unit id.
         * @property {String} stair - The stair of housing unit.
         * @property {String} floor - The floor of housing unit.
         * @property {String} apartmentNumber - The Number of apartment.
         * @property {Number} previousYearAmount - The amount of previous year
         * @property {Number} budgetAmount - The budget value.
         */
        function getOneByPersons(condominiumId, financialPeriodId) {
            return Budgets.get("reparted", {
                condominiumId: condominiumId,
                financialPeriodId: financialPeriodId
            }).then(function (data) {
                data.rows = data.instalmentRole;
                delete data.instalmentRole;
                return data;
            });
        }

        /**
         * Retrieve a single budget.
         *
         * @param {Number} condominiumId - The unique id of condominium
         * @param {Number} financialPeriodId - The unique id of financial period
         * @param {Budget~Budget} budget - The budget to save.
         * @returns {Promise<Object|Error>} - The promise over the budget object.
         */
        function update(condominiumId, financialPeriodId, budget) {
            return Budgets.one("", condominiumId).one("", financialPeriodId).customPUT(budget);
        }

        /**
         * Confirm the supplied budget.
         *
         * @param {Number} condominiumId - The unique id of condominium
         * @param {Number} financialPeriodId - The unique id of financial period
         * @returns {Promise<Object|Error>} - The promise over the budget object.
         */
        function confirm(condominiumId, financialPeriodId) {
            return Budgets.one("confirm", condominiumId).one("", financialPeriodId).customPUT({}, "", { confirm: true });
        }

        /**
         * Unconfirm the supplied budget.
         *
         * @param {Number} condominiumId - The unique id of condominium
         * @param {Number} financialPeriodId - The unique id of financial period
         * @returns {Promise<Object|Error>} - The promise over the budget object.
         */
        function unconfirm(condominiumId, financialPeriodId) {
            return Budgets.one("confirm", condominiumId).one("", financialPeriodId).customPUT({}, "", { confirm: false });
        }

        /**
         * Open ui modal
         * @param {Object} options 
         * @param {String} templateUrl 
         */
        function open(options, templateUrl) {
            return $uibModal.open({
                templateUrl: templateUrl,
                windowClass: 'inmodal centre-cost-modal-container',
                bindToController: true,
                controller: 'BudgetEditController',
                controllerAs: 'vm',
                resolve: {
                    condominiumId: function () { return options.condominiumId; },
                    periodId: function () { return options.periodId; }
                }
            });
        }

        /**
         *  call end-point for download report
         * possible value of options parameters are: 
         * - condominiumId(Mandatory)
         * - financialPeriodId(Mandatory)
         * - showSubRecord
         * - showPreviousYearBalance
         * - showInstalment
         * - printDetail
         * @param {Object} options - parameter for print Report
         */
        function getReport(printType, options) {
            var url = Budgets.all("reports").all(printType).getRequestedUrl();
            url = url + "?condominiumId=" + options.condominiumId + "&financialPeriodId=" + options.financialPeriodId + "&format=" + options.outputFormat;
            if (options.showSubRecord)
                url = url + "&showSubRecord=" + options.showSubRecord;
            if (options.showPreviousYearBalance)
                url = url + "&showPreviousYearBalance=" + options.showPreviousYearBalance;
            if (options.showInstalment)
                url = url + "&showInstalment=" + options.showInstalment;
            if(options.printDetail)
            url = url + "&printDetail=" + options.printDetail;
            $window.open(url, "_blank");
        }
    }

})();

