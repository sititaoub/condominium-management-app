/*
 * budget.service.spec.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
describe('BudgetService', function () {
    'use strict';

    var $httpBackend, $q, $state, $templateCache, $location, context, fixture;

    function mockServices($provide) {
    }

    function services($injector) {
        $q = $injector.get('$q');
        $state = $injector.get('$state');
        $templateCache = $injector.get('$templateCache');
        $location = $injector.get('$location');
        $httpBackend = $injector.get('$httpBackend');
        context = $injector.get('contextAddressManagement');
        fixture = $injector.get('BudgetService');
    }

    function setUp() {
        $httpBackend.whenGET(/assets\/(.+)/).respond(200, {});
        $httpBackend.whenGET(/users\/me$/).respond(200, {});
    }

    beforeEach(function () {
        module('condominiumManagementApp', 'condominiumManagementApp.templates', mockServices);
        inject(services);
        setUp();
    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getOne', function () {
        it("should reject if invoked without parameters", function (done) {
            $httpBackend.expectGET(context + 'v1/quotes').respond(400);

            fixture.getOne().catch(done);

            $httpBackend.flush();
        });

        it("should reject if 404 is returned", function (done) {
            $httpBackend.expectGET(context + 'v1/quotes?condominiumId=8&financialPeriodId=1').respond(404);

            fixture.getOne(8, 1).catch(done);

            $httpBackend.flush();
        });

        it("should resolve when budget is found", function (done) {
            $httpBackend.expectGET(context + 'v1/quotes?condominiumId=8&financialPeriodId=1').respond(200, {content: [{id: 1}]});

            fixture.getOne(8, 1).then(function (data) {
                expect(data).toEqual({id: 1});
                done();
            });

            $httpBackend.flush();
        });
    });

    describe("update", function () {
        it("should reject when condominiumId is not found", function (done) {
            $httpBackend.expectPUT(context + 'v1/quotes/8/1', {
                id: 2,
                name: 'MyBudget'
            }).respond(404);

            fixture.update(8, 1, {id: 2, name: 'MyBudget'}).catch(done);

            $httpBackend.flush();
        });

        it("should reject when bad data is supplied", function (done) {
            $httpBackend.expectPUT(context + 'v1/quotes/8/1', {
                id: 2,
                name: 'MyBudget'
            }).respond(400);

            fixture.update(8, 1, {id: 2, name: 'MyBudget'}).catch(done);

            $httpBackend.flush();
        });

        it("should resolve when operation completes", function (done) {
            $httpBackend.expectPUT(context + 'v1/quotes/8/1', {
                id: 2,
                name: 'MyBudget'
            }).respond(204);

            fixture.update(8, 1, {id: 2, name: 'MyBudget'}).then(done);

            $httpBackend.flush();
        });
    });

    describe("confirm", function () {
        it("should reject if budget was not found", function (done) {
            $httpBackend.expectPUT(context + 'v1/quotes/confirm/8/1?confirm=true', {}).respond(404);

            fixture.confirm(8, 1).catch(done);

            $httpBackend.flush();
        });

        it("should reject if budget was already confirmed", function (done) {
            $httpBackend.expectPUT(context + 'v1/quotes/confirm/8/1?confirm=true', {}).respond(409);

            fixture.confirm(8, 1).catch(done);

            $httpBackend.flush();
        });

        it("should resolve if budget was confirmed", function (done) {
            $httpBackend.expectPUT(context + 'v1/quotes/confirm/8/1?confirm=true', {}).respond(204);

            fixture.confirm(8, 1).then(done);

            $httpBackend.flush();
        });
    });

    describe("unconfirm", function () {
        it("should reject if budget was not found", function (done) {
            $httpBackend.expectPUT(context + 'v1/quotes/confirm/8/1?confirm=false', {}).respond(404);

            fixture.unconfirm(8, 1).catch(done);

            $httpBackend.flush();
        });

        it("should reject if budget was not confirmed", function (done) {
            $httpBackend.expectPUT(context + 'v1/quotes/confirm/8/1?confirm=false', {}).respond(409);

            fixture.unconfirm(8, 1).catch(done);

            $httpBackend.flush();
        });

        it("should resolve if budget was successfully unconfirm", function (done) {
            $httpBackend.expectPUT(context + 'v1/quotes/confirm/8/1?confirm=false', {}).respond(204);

            fixture.unconfirm(8, 1).then(done);

            $httpBackend.flush();
        });
    });
});