/*
 * budget-percentage-edit.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.budget')
        .factory('BudgetPercentageEditService', BudgetPercentageEditService);

    BudgetPercentageEditService.$inject = ['$uibModal'];

    /* @ngInject */
    function BudgetPercentageEditService($uibModal) {
        var service = {
            open: open
        };
        return service;

        ////////////////

        /**
         * The options for account edit service.
         *
         * @typedef {Object} Budget~PercentageEditServiceOptions
         * @property {number} [currentPercentage=100] - The actual percentage.
         */

        /**
         * The result of the open function.
         *
         * @typedef {Object} Budget~PercentageEditServiceResult
         * @property {function} close - Can be used to close a modal, passing a result.
         * @property {function} dismiss - Can be used to dismiss a modal, passing a reason.
         * @property {Promise.<number|Error>} result - Is resolved when a modal is closed and rejected when a modal is
         *                                             dismissed. The promise result is the new percentage value.
         */

        /**
         * Open the modal with supplied options.
         *
         * @param {Budget~PercentageEditServiceOptions} options - The options.
         * @returns {Budget~PercentageEditServiceResult} - The result.
         */
        function open(options) {
            return $uibModal.open({
                templateUrl: 'app/budget/budget-percentage-edit.html',
                windowClass: 'inmodal budget-container',
                bindToController: true,
                controller: BudgetPercentageEditController,
                controllerAs: 'vm',
                resolve: {
                    percentage: function() { return options.currentPercentage || 0; }
                }
            });
        }
    }

    BudgetPercentageEditController.$inject = ['$uibModalInstance', 'percentage'];

    /* @ngInject */
    function BudgetPercentageEditController($uibModalInstance, percentage) {
        var vm = this;

        vm.doConfirm = doConfirm;

        activate();

        ////////////////

        function activate() {
            vm.percentage = percentage;
        }

        /**
         * Confirm the set percentage operation.
         */
        function doConfirm() {
            $uibModalInstance.close(vm.percentage);
        }
    }

})();

