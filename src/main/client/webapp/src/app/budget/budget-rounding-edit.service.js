/*
 * budget-rounding-edit.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.budget')
        .factory('BudgetRoundingEditService', BudgetRoundingEditService);

    BudgetRoundingEditService.$inject = ['$uibModal'];

    /* @ngInject */
    function BudgetRoundingEditService($uibModal) {
        var service = {
            open: open
        };
        return service;

        ////////////////

        /**
         * The options for budget rounding edit service.
         *
         * @typedef {Object} Budget~RoundingEditServiceOptions
         * @property {number} [actualRounding] - The actual rounding value.
         */

        /**
         * The result of the open function.
         *
         * @typedef {Object} Budget~RoundingEditServiceResult
         * @property {function} close - Can be used to close a modal, passing a result.
         * @property {function} dismiss - Can be used to dismiss a modal, passing a reason.
         * @property {Promise.<number|Error>} result - Is resolved when a modal is closed and rejected when a modal is
         *                                             dismissed. The promise result is the new rounding value.
         */

        /**
         * Open the modal with supplied options.
         *
         * @param {Budget~RoundingEditServiceOptions} options - The options.
         * @returns {Budget~RoundingEditServiceResult} - The result.
         */
        function open(options) {
            return $uibModal.open({
                templateUrl: 'app/budget/budget-rounding-edit.html',
                windowClass: 'inmodal budget-container',
                bindToController: true,
                controller: BudgetRoundingEditController,
                controllerAs: 'vm',
                resolve: {
                    actualRounding: function() { return options.actualRounding; }
                }
            });
        }
    }


    BudgetRoundingEditController.$inject = ['$uibModalInstance', 'actualRounding', 'budgetRoundingValues'];

    /* @ngInject */
    function BudgetRoundingEditController($uibModalInstance, actualRounding, budgetRoundingValues) {
        var vm = this;

        vm.doConfirm = doConfirm;

        activate();

        ////////////////

        function activate() {
            vm.budgetRoundingValues = budgetRoundingValues;
            vm.rounding = actualRounding || budgetRoundingValues[0];
        }

        /**
         * Confirm the set percentage operation.
         */
        function doConfirm() {
            $uibModalInstance.close(vm.rounding);
        }
    }

})();

