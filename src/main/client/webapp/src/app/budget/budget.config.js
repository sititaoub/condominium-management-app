/*
 * budget.config.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.budget')
        .config(setupI18n);

        setupI18n.$inject = ['$translatePartialLoaderProvider'];

        /* @ngInject */
        function setupI18n($translatePartialLoaderProvider) {
            $translatePartialLoaderProvider.addPart('budget');
        }

})();