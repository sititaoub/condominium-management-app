/*
 * budget.constants.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.budget')
        .constant('budgetRoundingValues', [0, 5, 10]);

})();