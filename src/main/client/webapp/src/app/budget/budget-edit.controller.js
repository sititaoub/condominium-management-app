/*
 * budget-edit.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.budget')
        .controller('BudgetEditController', BudgetEditController);

    BudgetEditController.$inject = ['$state', 'NgTableParams', 'BudgetService', 'FinancialPeriodService', 'NotifierService', 'AlertService', 'BudgetPercentageEditService', 'BudgetRoundingEditService', '_'];

    /* @ngInject */
    function BudgetEditController($state, NgTableParams, BudgetService, FinancialPeriodService, NotifierService, AlertService, BudgetPercentageEditService, BudgetRoundingEditService, _) {
        var vm = this;
        vm.condominiumId = $state.params.condominiumId;
        vm.periodId = $state.params.periodId;
        if ($state.params.periodId === 'current') {
            vm.periodId = FinancialPeriodService.getCurrent(undefined, vm.condominiumId).id;
            $state.go('.', { periodId: vm.periodId });
        }
        vm.toggleFilters = toggleFilters;
        vm.askPercentage = askPercentage;
        vm.askRounding = askRounding;
        vm.setBudgetFlag = setBudgetFlag;
        vm.isLastPage = isLastPage;
        vm.sumBy = _.sumBy;
        vm.sum = _.sum;
        vm.calculateRow = calculateRow;
        vm.reverseRow = reverseRow;
        vm.doSave = doSave;
        vm.doConfirm = doConfirm;
        vm.doUnconfirm = doUnconfirm;
        vm.doPrintConfirm = doPrintConfirm;
        vm.openPrintRipartitionsOption = openPrintRipartitionsOption;
        vm.openPrintQuoteOption = openPrintQuoteOption;

        activate();

        ////////////////

        function activate() {
            vm.loading = true;
            vm.showFilters = false;
            vm.breakdownCriteriaMissing = false;
            vm.tableParams = new NgTableParams({
                group: 'accountMaterializedPath',                
                page: 1,
                count: 999999
            }, {
                    groupOptions: {
                        isExpanded: true
                    },
                    counts: [],
                    total: 1
                });
            loadBudget()
        }

        function loadBudget() {
            return BudgetService.getOne(vm.condominiumId, vm.periodId).then(function (budget) {
                updateBudget(budget);
                if (!budget.confirmed && breakdownCriteriaMissing(vm.budget.rows)) {
                    vm.breakdownCriteriaMissing = true;
                    AlertService.showAlert('Budget.Edit.MissingBreakdownCriteria');
                }
            }).catch(function () {
                return NotifierService.notifyError('Budget.Edit.LoadFailure')
            }).finally(function () {
                vm.loading = false;
            });
        }

        /**
         * Toggle the show/hide status of filters.
         */
        function toggleFilters() {
            vm.showFilters = !vm.showFilters;
        }

        /**
         * Update the budget set on controller.
         *
         * @param {Budget~Budget} budget - The budget to calculate rows for.
         */
        function updateBudget(budget) {
            vm.budget = budget;
            vm.tableParams.settings({ dataset: budget.rows });
            vm.actualPercentage = Math.ceil(_.sumBy(budget.rows, 'correctionPercentage') / budget.rows.length);
            calculateRows(budget);
        }

        /**
         * Perform the calculation on each row of budget object.
         *
         * @param {Budget~Budget} budget - The budget to calculate rows for.
         */
        function calculateRows(budget) {
            _.forEach(budget.rows, calculateRow)
        }

        /**
         * Perform the save operation.
         */
        function doSave() {
            BudgetService.update(vm.condominiumId, vm.periodId, vm.budget).then(function () {
                return NotifierService.notifySuccess('Budget.Edit.SaveSuccess');
            }).then(function () {
                vm.form.$setPristine();
            }).catch(function () {
                return NotifierService.notifyError('Budget.Edit.SaveFailure');
            });
        }

        /**
         * Set the budget flags.
         *
         * @param {boolean} value - The value to set.
         */
        function setBudgetFlag(value) {
            // Apply only to visible elements.
            _.forEach(_.flatMap(_.filter(vm.tableParams.data, _.isObject), 'data'), function (row) {
                var model = _.find(vm.budget.rows, { materializedPath: row.materializedPath });
                model.useBudget = value;
                calculateRow(model);
            });
        }

        /**
         * Ask the user for a percentage to set on all objects.
         */
        function askPercentage() {
            BudgetPercentageEditService.open({ currentPercentage: vm.actualPercentage }).result.then(function (percentage) {
                setPercentage(percentage);
                vm.actualPercentage = percentage;
            });
        }

        /**
         * Ask the user for rounding value.
         */
        function askRounding() {
            BudgetRoundingEditService.open({ actualRounding: vm.actualRounding }).result.then(function (rounding) {
                roundRowsTo(rounding);
                vm.actualRounding = rounding;
            })
        }

        /**
         * Set the percentage value to all rows.
         *
         * @param {number} value - The value to set as percentage on all rows.
         */
        function setPercentage(value) {
            // Apply only to visible elements.
            _.forEach(_.flatMap(_.filter(vm.tableParams.data, _.isObject), 'data'), function (row) {
                var model = _.find(vm.budget.rows, { materializedPath: row.materializedPath });
                model.correctionPercentage = value;
                calculateRow(model);
            });
        }

        /**
         * The amount (in euro) to round all rows to.
         *
         * @param {number} value - The number to round all the rows to.
         */
        function roundRowsTo(value) {
            // Apply only to visible elements.
            _.forEach(_.flatMap(_.filter(vm.tableParams.data, _.isObject), 'data'), function (row) {
                var model = _.find(vm.budget.rows, { materializedPath: row.materializedPath });
                // The formula to round to nearest divisor is:
                // round_number_up_to_nearest_divisor = number + ((divisor - (number % divisor)) % divisor)
                // @see https://stackoverflow.com/questions/3254047/round-number-up-to-the-nearest-multiple-of-3
                var rounded = model.budgetValue + ((value - (model.budgetValue % value)) % value);
                model.correctionValue += rounded - model.budgetValue;
                model.budgetValue = rounded;
            });
        }

        /**
         * Calculate the row.
         * This method is invoked when user changes useBudget flag, correction % or value.
         *
         * @param {Budget~Row} row - The row to calculate.
         */
        function calculateRow(row) {
            var baseValue = extractBaseValue(row);
            row.budgetValue = baseValue * (row.correctionPercentage || 0) / 100 + (row.correctionValue || 0);
        }

        /**
         * Reverse the calculation of row.
         * This method is invoked when user has entered the budgetValue and we calculate the correction % and value.
         *
         * @param {Budget~Row} row - The row to reverse calculation.
         */
        function reverseRow(row) {
            // Choose the base value between the budget and final.
            var baseValue = extractBaseValue(row);
            if (baseValue === 0) {
                // The base value is 0, so no percentage can be calculated.
                row.correctionValue = row.budgetValue;
            } else {
                // Calculate the correction percentage, based on budget value and vase value.
                row.correctionPercentage = Math.floor(row.budgetValue / baseValue * 100);
                // Update the correction value as difference from budget and calculated percentage.
                row.correctionValue = row.budgetValue - (baseValue * row.correctionPercentage / 100)
            }
        }

        /**
         * Extract the base value from a row.
         *
         * @param {Budget~Row} row - The row to extract value from.
         * @returns {number} - The base value, never undefined.
         */
        function extractBaseValue(row) {
            if (row.useBudget) {
                return row.previousBudget || 0;
            } else {
                return row.previousFinal || 0;
            }
        }

        /**
         * Check if the current table params page is the last page.
         *
         * @returns {boolean} - True if we are on last page and false if not
         */
        function isLastPage() {
            return vm.tableParams.page() === totalPages();
        }

        /**
         * Returns false if at least one breakdown criteria is missing.
         *
         * @param {Budget~Row[]} rows - the budget rows.
         * @returns {boolean} - True if at least one breakdown criteria is missing and false if it's not.
         */
        function breakdownCriteriaMissing(rows) {
            return angular.isDefined(_.find(rows, function (el) {
                return !el.hasBreakdownCriteria;
            }));
        }

        /**
         * Calculate the number of total pages.
         *
         * @returns {number} - The number of pages.
         */
        function totalPages() {
            return Math.ceil(vm.tableParams.total() / vm.tableParams.count())
        }

        /**
         * Confirm the actual budget.
         */
        function doConfirm() {
            BudgetService.confirm(vm.condominiumId, vm.periodId, vm.budget).then(function () {
                return NotifierService.notifySuccess('Budget.Edit.ConfirmSuccess');
            }).then(function () {
                loadBudget();
            }).catch(function () {
                return NotifierService.notifyError('Budget.Edit.ConfirmFailure');
            });
        }

        function openPrintRipartitionsOption() {
            BudgetService.open({
                condominiumId: vm.condominiumId,
                periodId: vm.periodId
            }, 'app/budget/print-ripartition-options.html');
        }

        function openPrintQuoteOption() {
            BudgetService.open({
                condominiumId: vm.condominiumId,
                periodId: vm.periodId
            }, 'app/budget/print-quote-options.html');
        }

        /**
         * Unconfirm the actual budget.
         */
        function doUnconfirm() {
            AlertService.askConfirm('Budget.Edit.UnconfirmMessage').then(function () {
                return BudgetService.unconfirm(vm.condominiumId, vm.periodId, vm.budget).then(function () {
                    return NotifierService.notifySuccess('Budget.Edit.UnconfirmSuccess');
                }).then(function () {
                    loadBudget();
                });
            }).catch(function () {
                return NotifierService.notifyError('Budget.Edit.UnconfirmFailure');
            })
        }

        function doPrintConfirm(printType, outputFormat) {
            var param={};
            param.condominiumId=$state.params.condominiumId;
            param.financialPeriodId=$state.params.periodId;
            param.showSubRecord=vm.printOption.onlyAccount;
            param.showPreviousYearBalance=vm.printOption.showLastYear;
            param.showInstalment=vm.printOption.showInstalmentPlan;
            param.printDetail=vm.printOption.printDetail;
            param.outputFormat=outputFormat;

            BudgetService.getReport(printType,param);
        }


    }

})();

