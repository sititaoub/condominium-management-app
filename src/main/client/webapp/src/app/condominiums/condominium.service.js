/*
 * structure.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.condominiums')
        .factory('CondominiumService', CondominiumService);

    CondominiumService.$inject = ['Restangular', 'contextAddressStructure', '_'];

    /* @ngInject */
    function CondominiumService(Restangular, contextAddressStructure, _) {
        var Condominiums = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressStructure + 'v1')
        }).all('condominiums');
        var service = {
            getPage: getPage,
            getOne: getOne,
            create: create,
            update: update
        };
        return service;

        ////////////////

        /**
         * Condominiums page.
         *
         * @typedef {Object} Condominiums~CondominiumsPage
         * @property {number} number - The requested page number.
         * @property {number} size - The requested page size.
         * @property {number} totalElements - The total number of elements.
         * @property {number} totalPages - The total number of pages.
         * @property {Condominiums~Condominium[]} content - The page content.
         */

        /**
         * Condominum object.
         *
         * @typedef {Object} Condominiums~Condominium
         * @property {number} id - The unique id of condominium.
         * @property {string} taxCode - The condominium tax code.
         * @property {string} name - The condominium name.
         * @property {string} managerFirstName - The first name of condominium manager.
         * @property {string} managerLastName - The last name of condominium manager.
         * @property {string} status - The condominum status.
         * @property {string} streetName - The address street name.
         * @property {string} buildingNumber - The address building number.
         * @property {string} town - The address town.
         * @property {string} district - The address district.
         * @property {string} province - The address province.
         */

        /**
         * Retrieve a single page of financial periods for given company id.
         *
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=10] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<Condominiums~CondominiumsPage|Error>} - The promise of page.
         */
        function getPage(page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return Condominiums.get("", params);
        }

        /**
         * Retrieve a single condominium.
         *
         * @param {number} id - The unique id of condominium.
         * @returns {Promise<Condominiums~Condominium|Error>} - The promise of condominium.
         */
        function getOne(id) {
            return Condominiums.one("", id).get().then(function (condominium) {
                if (angular.isDefined(condominium.manager) && condominium.manager !== null) {
                    condominium.managerId = condominium.manager.id;
                }
                return condominium;
            });
        }

        /**
         * Creates a single condominium.
         *
         * @param {Condominiums~Condominium} condominium - The condominium to update.
         * @returns {Promise<Condominiums~Condominium|Error>} - The promise of condominium.
         */
        function create(condominium) {
            return Condominiums.post(condominium);
        }

        /**
         * Updates a single condominium.
         *
         * @param {number} id - The unique id of condominium.
         * @param {Condominiums~Condominium} condominium - The condominium to update.
         * @returns {Promise<Condominiums~Condominium|Error>} - The promise of condominium.
         */
        function update(id, condominium) {
            return Condominiums.one("", id).customPUT(condominium);
        }
    }

})();

