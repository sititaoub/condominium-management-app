(function () {
    'use strict';

    angular
        .module("condominiumManagementApp.condominiums", [
            'condominiumManagementApp.core',
            'condominiumManagementApp.commons'
        ]);
})();