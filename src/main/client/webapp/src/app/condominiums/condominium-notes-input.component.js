/*
 * condominium-notes-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.condominiums')
        .component('condominiumNotesInput', {
            controller: CondominiumNotesInputController,
            bindings: {
                ngModel: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/condominiums/condominium-notes-input.html'
        });

    CondominiumNotesInputController.$inject = [];

    /* @ngInject */
    function CondominiumNotesInputController() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.notes = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue($ctrl.notes);
        }
    }

})();

