/*
 * condominium-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.condominiums')
        .component('condominiumCreate', {
            controller: CondominiumCreateController,
            bindings: {
                '$transtion$': '<'
            },
            templateUrl: 'app/condominiums/condominium-create.html'
        });

    CondominiumCreateController.$inject = ['$state', 'CondominiumService', 'UserService', 'NotifierService'];

    /* @ngInject */
    function CondominiumCreateController($state, CondominiumService, UserService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        $ctrl.disabled = false;

        ////////////

        function onInit() {
            var user = UserService.getUser();
            $ctrl.condominium = {
                ownerPrincipal: user.username,
                meteringProfile: 'GOLD'
            };
        }

        function doSave() {
            $ctrl.disabled = true;
            CondominiumService.create($ctrl.condominium).then(function () {
                return NotifierService.notifySuccess('Condominiums.Create.Success');
            }).then(function () {
                $ctrl.disabled = false;
                $state.go("^");
            }).catch(function () {
                $ctrl.disabled = false;
                NotifierService.notifyError('Condominiums.Create.Failure');
            });
        }
    }

})();

