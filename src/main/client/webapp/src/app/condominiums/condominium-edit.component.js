/*
 * condominium-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.condominiums')
        .component('condominiumEdit', {
            controller: CondominiumEditController,
            bindings: {
                'condominium': '<'
            },
            templateUrl: 'app/condominiums/condominium-edit.html'
        });

    CondominiumEditController.$inject = ['$state', 'CondominiumService', 'NotifierService'];

    /* @ngInject */
    function CondominiumEditController($state, CondominiumService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        $ctrl.disabled = false;

        ////////////////

        function onInit() {
        }

        function doSave() {
            $ctrl.disabled = true;
            CondominiumService.update($state.params.condominiumId, $ctrl.condominium).then(function () {
                return NotifierService.notifySuccess('Condominiums.Edit.Success');
            }).then(function () {
                $ctrl.disabled = false;
                $state.go('^');
            }).catch(function () {
                $ctrl.disabled = false;
                return NotifierService.notifyError('Condominiums.Edit.Failure');
            });
        }
    }

})();

