(function () {
    'use strict';

    angular
		.module("condominiumManagementApp.condominiums")
        .config(setupI18n);

    setupI18n.$inject = ['$translatePartialLoaderProvider'];

    /* @ngInject */
    function setupI18n($translatePartialLoaderProvider) {
        $translatePartialLoaderProvider.addPart("condominiums");
    }

})();