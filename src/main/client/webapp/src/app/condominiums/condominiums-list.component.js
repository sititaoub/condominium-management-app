/*
 * condominiums-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.condominiums')
        .component('condominiumsList', {
            controller: CondominiumsListComponent,
            bindings: {},
            templateUrl: 'app/condominiums/condominiums-list.html'
        });

    CondominiumsListComponent.$inject = ['$state', 'NgTableParams', 'CondominiumService', 'NotifierService'];

    /* @ngInject */
    function CondominiumsListComponent($state, NgTableParams, CondominiumService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10,
                sort: 'taxCode,asc'
            }, {
                getData: loadCondominiums
            });
            $ctrl.showFilters = $ctrl.tableParams.hasFilter();
        }


        function loadCondominiums(params) {
            $state.go('.', params.uiRouterUrl());
            return CondominiumService.getPage(params.page() - 1, params.count(), params.sorting(), params.filter())
                .then(function (data) {
                    params.total(data.totalElements);
                    return data.content;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    $ctrl.loading = false;
                });
        }
    }

})();

