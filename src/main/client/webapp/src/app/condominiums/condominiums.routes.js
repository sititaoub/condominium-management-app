'use strict';
(function () {
    'use strict';

    angular
        .module("condominiumManagementApp.condominiums")
        .config(function ($stateProvider) {
            $stateProvider.state('index.condominiums', {
                url: '/condominiums',
                redirectTo: 'index.condominiums.list',
                template: '<div class="condominiums-wrapper" ui-view></div>',
                data: {
                    skipFinancialPeriodCheck: true
                }
            }).state('index.condominiums.list', {
                url: '?page&count&sort',
                component: 'condominiumsList',
                params: {
                    page: { dynamic: true, squash: true, value: '1' },
                    count: { dynamic: true, squash: true, value: '10' },
                    sort: { dynamic: true, squash: true, value: 'taxCode,asc' }
                }
            }).state('index.condominiums.list.create', {
                url: '/create',
                views: {
                    '@index.condominiums': {
                        component: 'condominiumCreate'
                    }
                }
            }).state('index.condominiums.list.edit', {
                url: '/{condominiumId}/edit',
                views: {
                    '@index.condominiums': {
                        component: 'condominiumEdit'
                    }
                },
                resolve: {
                    condominium: ['$transition$', 'CondominiumService', function ($transition$, CondominiumService) {
                        return CondominiumService.getOne($transition$.params().condominiumId);
                    }]
                }
            });
        });
})();