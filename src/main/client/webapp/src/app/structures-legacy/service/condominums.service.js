/*
 * condominums.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.legacyStructure')
        .factory('CondominiumsService', CondominiumsService);

    CondominiumsService.$inject = ['Restangular', 'contextAddress'];

    /* @ngInject */
    function CondominiumsService(Restangular, contextAddress) {
        var Condominiums = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddress + 'v1');
        }).all('condominiums');

        var service = {
            getOne: getOne
        };
        return service;

        ////////////////

        /**
         * Retrieve a single condominium by his unique identifier.
         *
         * @param {number} id - The unique condominium id
         * @returns {promise} - The promise over the condominium value
         */
        function getOne(id) {
            return Condominiums.one("", id).get()
        }
    }

})();

