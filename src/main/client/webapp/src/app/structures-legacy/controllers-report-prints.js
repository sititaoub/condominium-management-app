/**
 * Created by Antonio Saracino on 03/08/17.
 */


angular.module("condominiumManagementApp.legacyStructure").controller("condominiums.reportPrintsCtrl", [
    "contextAddressReportServer",
    "$scope",
    "$window",
    "$stateParams",
    "NgTableParams",
    "CondominiumReportPrints",
    "UtilsService",
    "toaster",
    "$timeout",
    function (contextAddressReportServer, $scope, $window, $stateParams, NgTableParams, CondominiumReportPrints, UtilsService, toaster, $timeout) {

        $scope.condominiumId = $stateParams.condominiumId;

        $scope.loading = false;

        init();

        $scope.downloadReport = function (item) {
            var link = contextAddressReportServer + "v1/reports/" + item.id;
            $window.open(link, '_blank');
        };

        $scope.refresh = function () {
            $scope.tableParams.reload();
        };

        /*****************************************/

        function init() {

            $scope.tableParams = new NgTableParams(
                {
                    count: 10
                },
                {
                    getData: function (params) {

                        $scope.loading = true;
                        var sorting = "";
                        // creo la stringa per l'ordinamento se presente nella lista dei parametri
                        if (params.parameters().sorting && Object.keys(params.parameters().sorting).length > 0) {
                            var key = Object.keys(params.parameters().sorting)[0];
                            var order =  params.parameters().sorting[key];
                            sorting = key + "," + order
                        }

                        return CondominiumReportPrints.resource.get(
                            {
                                condominiumId: $scope.condominiumId,
                                page: params.parameters().page - 1,
                                size: params.parameters().count,
                                sort: sorting

                            }).$promise
                            .then(function (data) {

                                $scope.loading = false;
                                params.total(data.totalElements);
                                return data.content;

                            }).catch(function (data) {

                                $scope.loading = false;
                                UtilsService.showHttpError(data.data, toaster);
                            });
                    },
                    counts: [10, 20, 50]
                }
            );
        }


    }]);
