"use strict";

angular.module("condominiumManagementApp.legacyStructure").controller("condominiums.reportCtrl", [
    "$rootScope",
    "$scope",
    "$q",
    '$stateParams',
    '$window',
    '$cacheFactory',
    '$templateRequest',
    '$interpolate',
    '$filter',
    "CondominiumResource",
    "UtilsService",
    "CondominiumHeatCost",
    "DistributionCriterionService",
    "NgTableParams",
    "$uibModal",
    "toaster",
    'CondominiumReportPrints',
    'SweetAlert',
    'NotifierService',
    function ($rootScope, $scope, $q, $stateParams, $window, $cacheFactory, $templateRequest, $interpolate, $filter, CondominiumResource, UtilsService, CondominiumHeatCost, DistributionCriterionService, NgTableParams, $uibModal, toaster, CondominiumReportPrints, SweetAlert, NotifierService) {

        var HEAT_COST_CACHE = 'heatCostCache';
        $scope.condominiumId = $stateParams.condominiumId;
        $scope.tables = [];
        $scope.sorting = {
            owner: "asc"
        };
        $scope.filter = {
            owner: ""
        };

        init();

        $scope.backToPrevious = function () {
            UtilsService.backNav();
        };

        $scope.openReportFilterModal = openModal;

        $scope.generateReportPdf = function () {
            SweetAlert.swal({
                    title: $filter('translate')('condominium.report.energy.pdf.alert.title'),
                    text: $filter('translate')('condominium.report.energy.pdf.alert.message'),
                    type: "info",
                    showCancelButton: true,
                    confirmButtonText: $filter('translate')('condominium.report.energy.pdf.alert.continue.btn'),
                    cancelButtonText: $filter('translate')('cancel')
                },
                function (isConfirm) {

                    if (isConfirm) {
                        sendRequestToGenerateReportPdf();
                    }
                });
        };

        // soluzione veloce che sostituisce momentaneamente il servizio di stampa
        // @todo da cancellare
        $scope.openDetailWindow = function (id) {

            $templateRequest('app/structures-legacy/partials/report-detail.html').then(function (content) {

                var interpolation = $interpolate(content);

                // carico gli style dalla pagina corrente
                var elements = angular.element("link[rel='stylesheet']");
                var styles = "";
                angular.forEach(elements, function (style) {
                    styles += "\n" + style.outerHTML;
                });

                // filtro i dati delle tabelle in base all'id
                var comparator = function (actual, expected) {
                    return actual === expected;
                };
                var tables = {};
                tables.summary = $filter('filter')($scope.report.listCostSummaryReporting.content, {id: id}, comparator)[0];
                tables.heating = $filter('filter')($scope.report.listCliHeatCostReporting.content, {id: id}, comparator)[0];
                tables.acs = $filter('filter')($scope.report.listAcsHeatCostReporting.content, {id: id}, comparator)[0];
                tables.afs = $filter('filter')($scope.report.listAfsCostReporting.content, {id: id}, comparator)[0];
                tables.otherCosts = $scope.report.otherCosts.content.map(function (cost) {
                    var singleCost = $filter('filter')(cost.content, {id: id}, comparator)[0];
                    singleCost.label = cost.label;
                    return singleCost;
                });

                createOtherCostsHtml(tables.otherCosts, function (otherCostsHtml) {
                    var html = interpolation({
                        condominium: $scope.condominium,
                        styles: styles,
                        report: $scope.report,
                        tables: tables,
                        otherCostsHtml: otherCostsHtml,
                        currentDate: new Date()
                    });
                    var win = $window.open("", "", "width=960,height=800,titlebar=no,toolbar=no,location=no");
                    win.document.write(html);
                });
            });
        };

        $scope.invertSort = function () {

            $scope.sorting.owner = $scope.sorting.owner === "asc" ? "desc" : "asc";

            $scope.tables.summaryParams.sorting($scope.sorting);
            $scope.tables.energyParams.sorting($scope.sorting);
            $scope.tables.acsParams.sorting($scope.sorting);
            $scope.tables.afsParams.sorting($scope.sorting);
            $scope.tables.otherCostsParams.sorting($scope.sorting);
        };

        $scope.updateFilter = function () {
            $scope.tables.summaryParams.filter($scope.filter);
            $scope.tables.energyParams.filter($scope.filter);
            $scope.tables.acsParams.filter($scope.filter);
            $scope.tables.afsParams.filter($scope.filter);
            $scope.tables.otherCostsParams.filter($scope.filter);
        };

        $scope.getTotalAmount = function () {
            return parseFloat($scope.report.totalHeatAmount) +
                parseFloat($scope.report.totalWaterAmount) +
                parseFloat($scope.report.totalOtherCosts);
        };

        $scope.getEnergyUM = function () {
            return parseFloat($scope.report.totalHeatMeter) > 10000 ? 'MWh' : 'kWh';
        };

        $scope.formatEnergyMeter = function (value) {

            if ($scope.getEnergyUM() === 'MWh') {
                return parseFloat(value) / 1000;
            }

            return parseFloat(value);
        };

        // Calculate Energy Average Unit Cost
        $scope.getEnergyAUC = function() {
            return $scope.report.totalHeatAmount / $scope.report.totalHeatMeter ;
        };

        // Calculate Water Average Unit Cost
        $scope.getWaterAUC = function() {
            return $scope.report.totalWaterAmount / $scope.report.totalWaterMeter ;
        };
        
        /*********************************************/

        function initSummaryTable() {
            $scope.tables.summaryParams = new NgTableParams({
                sorting: $scope.sorting
            }, {
                dataset: $scope.report.listCostSummaryReporting.content
            });
        }

        function initEnergyTable() {
            $scope.tables.energyParams = new NgTableParams({
                sorting: $scope.sorting
            }, {
                dataset: $scope.report.listCliHeatCostReporting.content
            });
        }

        function initACSTable() {
            $scope.tables.acsParams = new NgTableParams({
                sorting: $scope.sorting
            }, {
                dataset: $scope.report.listAcsHeatCostReporting.content
            });
        }

        function initAFSTable() {
            $scope.tables.afsParams = new NgTableParams({
                sorting: $scope.sorting
            }, {
                dataset: $scope.report.listAfsCostReporting.content
            });
        }

        function initOtherCostsTable() {

            $scope.tables.otherCostsParams = new NgTableParams({
                sorting: $scope.sorting
            }, {
                dataset: transformOtherCost($scope.report.otherCosts.content, $scope.report.otherCostsSummary.content)
            });
        }

        function transformOtherCost(list, listTotals) {
            var result = [];

            if (!list) {
                return result;
            }

            // cerca l'owner con quell'id all'interno della lista
            function findOwner(id) {

                for (var index in result) {
                    var item = result[index];
                    if (item.id === id) {
                        return item;
                    }
                }

                return null;
            }

            // cerca il totale della riga degli altri costi
            function findTotal(id) {

                for (var index in listTotals) {
                    var item = listTotals[index];
                    if (item.id === id) {
                        return item;
                    }
                }

                return null;
            }

            angular.forEach(list, function (cost) {
                angular.forEach(cost.content, function (item) {
                    var owner = findOwner(item.id);
                    var ownerTotal = findTotal(item.id);

                    if (!owner) {
                        owner = angular.copy(item);
                        owner.totalCost = ownerTotal.totalRowOtherCosts
                        result.push(owner);
                    }
                    owner[cost.oid] = {
                        thousandthCost: item.thousandthCost,
                        thousandthShare: item.thousandthShare,                        
                    };
                });
            });

            //console.log(result);
            return result;
        }

        function loadReport(startDate, endDate, totalHeatAmount, totalWaterAmount, otherCosts) {

            $scope.loading = true;

            CondominiumHeatCost.resource.generate({
                id: $scope.condominium.id,
                totalHeatAmount: totalHeatAmount,
                totalWaterAmount: totalWaterAmount,
                reportingDateFrom: startDate,
                reportingDateTo: endDate,
                otherCosts: otherCosts
            }, function (result) {
                $scope.loading = false;
                $scope.report = result;
                
                $scope.report.startFilterDate = startDate;
                $scope.report.endFilterDate = endDate;

                $cacheFactory.get(HEAT_COST_CACHE).put('report', $scope.report);

                initSummaryTable();
                initEnergyTable();
                initACSTable();
                initAFSTable();
                initOtherCostsTable();
            }, function (data) {
                $scope.loading = false;
                if(data.data.errors) {                                
                    NotifierService.notifyError('condominium.report.energy.table.' + data.data.errors[0].code);
                } else {
                    NotifierService.notifyError('condominium.report.energy.table.error.generic');
                }

            });
        }

        function sendRequestToGenerateReportPdf() {
            $scope.loadingPrint = true;

            var otherCosts = [];
            if ($cacheFactory.get(HEAT_COST_CACHE).get('filter')) {
                otherCosts = $cacheFactory.get(HEAT_COST_CACHE).get('filter').otherCosts;
            }

            var format = "yyyy-MM-dd'T00:00:00.000Z'";
                        
            var summaryRequest = {
                condominiumExternalId: $scope.condominiumId,
                totalHeatAmount: $cacheFactory.get(HEAT_COST_CACHE).get('filter').totalHeatAmount,
                totalWaterAmount: $cacheFactory.get(HEAT_COST_CACHE).get('filter').totalWaterAmount,
                reportingDateFrom: $filter('date')($cacheFactory.get(HEAT_COST_CACHE).get('filter').startDate, format),                 
                reportingDateTo: $filter('date')($cacheFactory.get(HEAT_COST_CACHE).get('filter').endDate, format),
                otherCosts: otherCosts,
                reportType: 'SUMMARY'
            };

            var promises = [];
            // effettuo due richieste di stampa
            // prima richiesta di summary
            promises.push(CondominiumReportPrints.resource.print(summaryRequest));

            // seconda richiesta di single
            var singleRequest = angular.copy(summaryRequest);
            singleRequest.reportType = "SINGLE";
            promises.push(CondominiumReportPrints.resource.print(singleRequest));

            $q.all(promises).then(function () {
                $scope.loadingPrint = false;
                toaster.pop({
                    type: 'success',
                    title: $filter('translate')('condominium.report.energy.printSuccess.msg'),
                    showCloseButton: true,
                    timeout: 2000
                });
            }).catch(function () {
                $scope.loadingPrint = false;
                UtilsService.showHttpError(data.data, toaster);
            });
        }

        function openModal() {

            if ($scope.loading) {
                console.log("Unable to make a search while loading another one");
                return;
            }

            var defaultDate = getDefaultReportDate();

            var filterData = {
                startDate: defaultDate.defaultStartDate,
                endDate: defaultDate.defaultSelectedEndDate,
                otherCosts: getDefaultOtherCosts()
            };
            // se presente carico gli ultimi dati inseriti
            if ($cacheFactory.get(HEAT_COST_CACHE).get('filter')) {
                filterData = $cacheFactory.get(HEAT_COST_CACHE).get('filter');
            }

            $uibModal.open({
                templateUrl: 'app/structures-legacy/partials/report-filter-modal.html',
                controller: 'condominiums.ReportFilterModalController',
                windowClass: 'inmodal',
                size: 'lg',
                resolve: {
                    filter: function () {
                        return filterData;
                    },
                    rangeDate: function () {
                        return defaultDate;
                    },
                    distributionTables: function () {
                        return $filter('filter')($scope.distributionTables, {thousandthType: "!INDIVIDUAL"});
                    }
                }
            }).result.then(function (result) {

                $cacheFactory.get(HEAT_COST_CACHE).put('filter', result);
                //@TRICK: Estrapola dal Date giorno, mese e anno e lo concatena ad ad un orario con timezone UTC
                var format = "yyyy-MM-dd'T00:00:00.000Z'";
                loadReport(
                    $filter('date')(result.startDate, format),
                    $filter('date')(result.endDate, format),
                    result.totalHeatAmount,
                    result.totalWaterAmount,
                    result.otherCosts
                );
            });
        }

        /**
         * @TODO da cancellare - soluzione veloce per creare una struttura HTML
         * ricorsiva basata sulla tabella dei costi
         */
        function createOtherCostsHtml(list, cb) {
            if (!list || list.length <= 0) {
                cb("");
                return;
            }

            var html = "<div class='row'><div class='col-lg-12'>" +
                "<div class='ibox float-e-margins'><div class='ibox-title'>"
                + $filter('translate')('condominium.report.energy.othercosts')
                + "</div></div></div></div>";

            $templateRequest('app/structures-legacy/partials/report-detail-other-cost.html').then(function (content) {
                angular.forEach(list, function (cost) {
                    var interpolation = $interpolate(content);
                    html += interpolation({
                        cost: cost
                    });
                });

                cb(html);
            });

        }

        /**
         * Calcola il range delle date di dafault per il report
         * @returns {{}}
         */
        function getDefaultReportDate() {
            var result = {};

            var now = new Date();
            var startYear = now.getFullYear();
            if (now.getMonth() < 8) {
                // se siamo prima di settembre il periodo che considero
                // è 1-settmbre dell'anno precedente e 31-agosto di quest'anno
                startYear -= 1;
            }

            result.defaultStartDate = new Date(startYear - 1, 8, 1);
            result.defaultEndDate = new Date(startYear + 1, 7, 31);
            result.defaultSelectedEndDate = new Date(startYear, 7, 31);

            return result;
        }

        function getDefaultOtherCosts() {

            var otherCosts = [];

            for (var index in $scope.distributionTables) {
                var table = $scope.distributionTables[index];
                if (table.thousandthType === DistributionCriterionService.tableTypeEnum.ENERGY &&
                    table.energyDistributionColumn === 'INVOLUNTARY') {
                    // trovata, aggiungo i tre costi di default

                    otherCosts.push({
                        label: 'Manutenzione centrale termica',
                        amount: 0,
                        thousandthTableId: table.id
                    });

                    otherCosts.push({
                        label: 'Costi per corrente elettrica',
                        amount: 0,
                        thousandthTableId: table.id
                    });

                    otherCosts.push({
                        label: 'Costi per servizio lettura',
                        amount: 0,
                        thousandthTableId: table.id
                    });

                    break;
                }
            }

            return otherCosts;
        }

        function init() {
            if (!$cacheFactory.get(HEAT_COST_CACHE)) {
                // creo la cache
                $cacheFactory(HEAT_COST_CACHE);
            }

            $scope.loadingCondominium = true;

            DistributionCriterionService.resource.get({condominiumId: $scope.condominiumId}).$promise.then(function (data) {
                $scope.distributionTables = data.content;
                return CondominiumResource.get({id: $scope.condominiumId}).$promise;

            }).then(function (condominium) {
                $scope.condominium = condominium;

                $scope.loadingCondominium = false;
                var cache = $cacheFactory.get(HEAT_COST_CACHE);
                if (cache.get('report')) {
                    $scope.report = cache.get('report');
                    initSummaryTable();
                    initEnergyTable();
                    initACSTable();
                    initAFSTable();
                    initOtherCostsTable();
                } else {
                    openModal();
                }
            }).catch(function (data) {
                $scope.loadingCondominium = false;
                UtilsService.showHttpError(data.data, toaster);
                NotifierService.notifyError();
            });
        }

    }


]);


