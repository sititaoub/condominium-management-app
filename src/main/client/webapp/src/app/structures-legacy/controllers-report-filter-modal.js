"use strict";

angular.module("condominiumManagementApp.legacyStructure").controller("condominiums.ReportFilterModalController", [
    '$scope',
    '$uibModalInstance',
    '$filter',
    'toaster',
    'UtilsService',
    'filter',
    'rangeDate',
    'distributionTables',
    function ($scope, $uibModalInstance, $filter, toaster, UtilsService, filter, rangeDate, distributionTables) {

        $scope.filter = filter || {
            otherCosts: []
        };

        $scope.rangeDate = rangeDate || {};
        $scope.subtitle = $filter('translate')('condominium.report.energy.selectReferencePeriodRange', {
            start: $filter('date')(rangeDate.defaultStartDate, 'dd/MM/yyyy'),
            end: $filter('date')(rangeDate.defaultEndDate, 'dd/MM/yyyy')
        });
        $scope.distributionTables = distributionTables;

        init();

        $scope.close = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.load = function (form) {

            if ($scope.filter.startDate.getTime() > $scope.filter.endDate.getTime()) {
                toaster.pop({
                    type: 'error',
                    title: $filter('translate')('condominium.report.energy.selectStartBeforeEnd'),
                    showCloseButton: true,
                    timeout: 5000
                });
                return;
            }

            // NOTA: SOLUZIONE MOMENTANEA non è la soluzione ideale (come potrebbe essere 
            // ad esempio il campo 'min' nell'input della data), ma ho utilizzato questa soluzione per evitare
            // di modificare il css del template per rappresentare correttamente l'errore.
            if ($scope.filter.startDate.getTime() < rangeDate.defaultStartDate.getTime()) {
                showRangeError();
                return;
            }

            if ($scope.filter.endDate.getTime() > rangeDate.defaultEndDate.getTime()) {
                showRangeError();
                return;
            }

            if (form.$valid) {
                $uibModalInstance.close($scope.filter);
            }
        };

        $scope.addOtherCost = function () {
            $scope.filter.otherCosts.push({
                label: null,
                amount: null,
                thousandthTableId: null
            });
        };

        $scope.removeOtherCost = function (cost) {
            $scope.filter.otherCosts.splice($scope.filter.otherCosts.indexOf(cost), 1);
        };

        /*****************************************************************/

        /**
         * Genero una lista di date ammissibili come data di inizio considerando il range
         * di partenza
         * @param start oggetto Date di partenza
         * @param end oggetto Date di fine
         */
        function generateStartDateList(start, end) {

            var list = [];
            var endDate = moment(end).startOf('month');
            var currentDate = moment(start).startOf('month');

            while (currentDate.toDate() <= endDate.toDate()) {

                // la data corrente è ancora una data valida,
                // quindi l'aggiungo alla lista dei valori accettabili
                list.push({
                    value: new Date(currentDate.toDate()),
                    label: $filter('translate')(currentDate.format('MMMM')) + " - " + currentDate.year()
                });

                // la prossima data è il primo del mese successivo
                currentDate.add(1, 'month').startOf('month');
            }

            return list;
        }

        /**
         * Genero una lista di date ammissibili come data di fine considerando il range
         * di partenza
         * @param start Date di partenza
         * @param end Date di fine
         */
        function generateEndDateList(start, end) {

            var list = [];
            var zeroTime = {'h': 0, 'm': 0, 's': 0, 'ms': 0};
            // prendo l'ultimo giorno del mese di partenza
            var endDate = moment(end).endOf('month').set(zeroTime);
            var currentDate = moment(start).endOf('month').set(zeroTime);

            while (currentDate.toDate() <= endDate.toDate()) {

                // la data corrente è ancora una data valida,
                // quindi l'aggiungo alla lista dei valori accettabili
                list.push({
                    value: new Date(currentDate.toDate()),
                    label: $filter('translate')(currentDate.format('MMMM')) + " - " + currentDate.year()
                });

                // la prossima data è l'ultimo giorno del mese successivo
                // (incremento di due mesi e impostostando il giorno a 0 ottengo l'ultimo giorno
                // del mese precedente)
                currentDate.add(1, 'months').endOf('month').set(zeroTime);
            }

            return list;
        }

        function showRangeError() {
            toaster.pop({
                type: 'error',
                title: $filter('translate')('condominium.report.energy.selectReferencePeriodRange', {
                    start: $filter('date')(rangeDate.defaultStartDate, 'dd/MM/yyyy'),
                    end: $filter('date')(rangeDate.defaultEndDate, 'dd/MM/yyyy')
                }),
                showCloseButton: true,
                timeout: 5000
            });
        }

        function init() {

            // genero le sorgenti per le combo-box delle date
            $scope.startMonths = generateStartDateList(rangeDate.defaultStartDate, rangeDate.defaultEndDate);
            $scope.endMonths = generateEndDateList(rangeDate.defaultStartDate, rangeDate.defaultEndDate);

            // aggiorno il riferimento alle combo-box in modo che possa preselezionarsi
            // seleziono la combo di inizio
            for (var index in $scope.startMonths) {
                var item = $scope.startMonths[index];
                if ($scope.filter.startDate.getTime() === item.value.getTime()) {
                    $scope.filter.startDate = item.value;
                    break;
                }
            }

            // seleziono la combo di fine
            for (var index in $scope.endMonths) {
                var item = $scope.endMonths[index];
                if ($scope.filter.endDate.getTime() === item.value.getTime()) {
                    $scope.filter.endDate = item.value;
                    break;
                }
            }
        }


    }]);