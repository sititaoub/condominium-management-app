"use strict";

angular.module("condominiumManagementApp.legacyStructure")
    .controller("structure.StructureCtrl", [
        "$scope",
        "$rootScope",
        "$state",
        "$timeout",
        '$stateParams',
        "TableService",
        "HousingUnitCrudResource",
        "HousingUnitService",
        "CondominiumTreeService",
        "CondominiumResource",
        "SweetAlert",
        "$document",
        function ($scope, $rootScope, $state, $timeout, $stateParams,
                  TableService, HousingUnitCrudResource, HousingUnitService, CondominiumTreeService,
                  CondominiumResource, SweetAlert, $document) {


            $scope.fromUrl = $rootScope.previousState;

            $scope.condominiumId = $stateParams.condominiumId;
            CondominiumResource.get({id: $scope.condominiumId}, function (data) {
                $scope.condominium_data = data;
                $scope.condominium_map = {
                    control: {},
                    zoom: 13,
                    options: {
                        draggable: false,
                        clickableIcons: false,
                        disableDefaultUI: true,
                        disableDoubleClickZoom: true,
                        draggableCursor: false,
                        gestureHandling: "none",
                        keyboardShortcuts: false,
                        scrollwheel: false,
                        backgroundColor: "red"
                    },
                    center: {
                        latitude: $scope.condominium_data.address.lat,
                        longitude: $scope.condominium_data.address.lng
                    }
                };


            });

            $document.ready(function () {
                var rootScope = getRootNodesScope();
            });

            function getRootNodesScope() {
                var parentScope = angular.element(document.getElementById("tree-root")).scope().$nodesScope;
                if (parentScope) {
                    return parentScope.childNodes()[0];
                }
                return null;
            }

            $scope.backToCondominium = function () {
                $state.transitionTo("index.structure-list");
            };


            var getCondominiumTree = function () {
                $scope.treeLoading = true;
                CondominiumTreeService.getCondominiumTree($scope.condominiumId).then(function (data) {
                    $scope.treeLoading = false;
                    $scope.tree_data = {
                        condominiums: [{
                            description: "Condominio",
                            housingUnitGroups: data.data.housingUnitGroups
                        }]
                    };

                    if ($scope.selectedNodeInfo) {
                        selectNode($scope.selectedNodeType, $scope.selectedNodeInfo);
                    }

                    if ($scope.preaddNodeInfo) {
                        preaddNode($scope.preaddNodeType, $scope.preaddNodeInfo);
                    }
                }, function (data) {
                    $scope.treeLoading = false;
                });

            };

            getCondominiumTree();


            var getHousingUnitGroup = function (hUnitGroupId) {
                var hUnitGroupNode = _.find($scope.tree_data.condominiums[0].housingUnitGroups, function (hUnitGroup) {
                    return hUnitGroup.id == hUnitGroupId;
                });
                return hUnitGroupNode;
            }
            var getHousingUnit = function (hUnitId, hUnitGroupId) {
                if (typeof hUnitGroupId !== "undefined") {
                    var hUnitNodeGroup = getHousingUnitGroup(hUnitGroupId)
                    var hUnitNode = _.find(hUnitNodeGroup.housingUnits, function (hUnit) {
                        return hUnit.id == hUnitId;
                    });
                    return hUnitNode;

                } else {
                    var hUnitNodes = _.flatten(_.map($scope.tree_data.condominiums[0].housingUnitGroups, function (hUnitGroup) {
                        return hUnitGroup.housingUnits;
                    }));
                    var hUnitNode = _.find(hUnitNodes, function (hUnit) {
                        return hUnit.id == hUnitId;
                    });
                    return hUnitNode;
                }
            }


            var getRole = function (roleId, hUnitId) {
                var hUnitNode = getHousingUnit(hUnitId);
                var roleNode = _.find(hUnitNode.roles, function (role) {
                    return role.id == roleId;
                });
                return roleNode;
            }

            $scope.visible = function (item) {
                return !($scope.query && $scope.query.length > 0
                && item.description.toLowerCase().indexOf($scope.query) == -1);

            };
            $scope.findNodes = function () {

            };

            $scope.$on("selectCondominium", function (event, condominium) {
                /*$scope.tree_data.condominiums[0].selected = true;*/
                /* switch to queued action as event is raised before tree_data is filled */
                if ($scope.tree_data) {
                    $scope.tree_data.condominiums[0].selected = true;
                }
                $scope.selectedNodeType = "selectCondominium";
                $scope.selectedNodeInfo = {};
            });

            $scope.$on("selectHousingUnitGroup", function (event, hUnitGroup) {

                if ($scope.tree_data) {
                    $scope.selectedScope.$modelValue.selected = true; //getHousingUnit(hUnit.id).selected = true;
                }
                /* switch to queued action as event is raised before tree_data is filled */
                $scope.selectedNodeType = "selectHousingUnitGroup";
                $scope.selectedNodeInfo = hUnitGroup;
            });

            $scope.$on("preaddHousingUnitGroup", function (event, hUnitGroup) {
                console.log("Showing tree node for new housing unit group");

                var newNode = {id: 0, description: "Nuova scala", housingUnits: [], selected: true, temporary: true};
                if ($scope.tree_data) {
                    $scope.tree_data.condominiums[0].housingUnitGroups.push(newNode);
                }
                $scope.preaddNodeType = "preaddHousingUnitGroup";
                $scope.preaddNodeInfo = newNode;
            });

            $scope.$on("addHousingUnitGroup", function (event, hUnitGroup) {
                console.log("Created housing unit group:" + hUnitGroup.id);

                if ($scope.preaddNodeInfo) {

                    var huGroupNode = $scope.selectedScope.$modelValue;
                    huGroupNode.id = hUnitGroup.id;
                    huGroupNode.description = hUnitGroup.description;
                    huGroupNode.temporary = false;

                    if ($scope.selectedScope) {
                        $scope.selectedScope.temporary = false;
                    }

                    $scope.preaddNodeInfo = null;
                } else {
                    $scope.tree_data.condominiums[0].housingUnitGroups.push({
                        id: hUnitGroup.id,
                        description: hUnitGroup.description,
                        housingUnits: [],
                        selected: true
                    });
                }

                $state.go('index.structure.housing-unit-group', {
                    huGroupId: hUnitGroup.id,
                    description: hUnitGroup.description
                });
            });

            $scope.$on("editHousingUnitGroup", function (event, data) {
                getCondominiumTree();
            });

            $scope.$on("removeHousingUnitGroup", function (event, hUnitGroup) {
                _.remove($scope.tree_data.condominiums[0].housingUnitGroups, function (node) {
                    return node.id == hUnitGroup.id;
                });
                if ($scope.selectedScope) {
                    $scope.selectedScope = $scope.selectedScope.$parentNodeScope;
                    $scope.selectedScope.selected = true;
                }
                $state.go('index.structure.root', {});
            });

            $scope.$on("backHousingUnitGroup", function (event, hUnitGruop) {
                if (hUnitGruop.id == 'new') {
                    _.remove($scope.tree_data.condominiums[0].housingUnitGroups, function (node) {
                        return node.id == 0;
                    });
                }
                if ($scope.selectedScope) {
                    $scope.selectedScope.selected = false;
                    $scope.selectedScope = $scope.selectedScope.$parentNodeScope;
                    $scope.selectedScope.selected = true;
                }

                $state.go('index.structure.root', {});
            });


            $scope.$on("selectHousingUnit", function (event, hUnit) {
                /*getHousingUnit(hUnit.id).selected = true;*/
                if ($scope.tree_data) {
                    $scope.selectedScope.$modelValue.selected = true; //getHousingUnit(hUnit.id).selected = true;
                }
                /* switch to queued action as event is raised before tree_data is filled */
                $scope.selectedNodeType = "selectHousingUnit";
                $scope.selectedNodeInfo = hUnit;
            });

            $scope.$on("preaddHousingUnit", function (event, hUnit) {
                console.log("Showing tree node for new housing unit");

                var newNode = {
                    id: 0,
                    description: "Nuova unità abitativa",
                    rooms: [],
                    heatMeters: [],
                    waterMeters: [],
                    selected: true,
                    temporary: true
                };
                if ($scope.tree_data) {
                    $scope.selectedScope.$modelValue.housingUnits.push(newNode);
                }
                $scope.preaddNodeType = "preaddHousingUnit";
                $scope.preaddNodeInfo = {huGroupId: hUnit.huGroupId, description: "Nuova unità abitativa"};
            });

            $scope.$on("addHousingUnit", function (event, hUnit) {
                console.log("Created housing unit " + hUnit.id);

                if ($scope.preaddNodeInfo) {
                    //var huNode = getHousingUnit(0);
                    var huNode = $scope.selectedScope.$modelValue;
                    huNode.id = hUnit.id;
                    huNode.description = hUnit.owner;
                    huNode.temporary = false;

                    if ($scope.selectedScope) {
                        $scope.selectedScope.temporary = false;
                    }

                    $scope.preaddNodeInfo = null;
                } else {
                    $scope.selectedScope.$parentNodeScope.$modelValue.housingUnits.push({
                        id: hUnit.id,
                        description: hUnit.owner,
                        rooms: [],
                        heatMeters: [],
                        waterMeters: [],
                        selected: true
                    });
                }

                $state.go('index.structure.housing-unit', {huId: hUnit.id, description: hUnit.owner});
            });

            $scope.$on("editHousingUnit", function (event, data) {
                getCondominiumTree();
            });

            $scope.$on("removeHousingUnit", function (event, hUnit) {
                var hUnitGroup = $scope.selectedScope.$parentNodeScope.$modelValue;

                _.remove(hUnitGroup.housingUnits, function (node) {
                    return node.id == hUnit.id;
                });
                if ($scope.selectedScope) {
                    $scope.selectedScope = $scope.selectedScope.$parentNodeScope;
                    $scope.selectedScope.selected = true;
                }
                $state.go('index.structure.housing-unit-group', {
                    huGroupId: hUnitGroup.id,
                    description: hUnitGroup.description
                });
            });

            $scope.$on("backHousingUnit", function (event, hUnit) {
                var hUnitGroup = $scope.selectedScope.$parentNodeScope.$modelValue;
                if (hUnit.id == 'new') {
                    _.remove(hUnitGroup.housingUnits, function (node) {
                        return node.id == 0;
                    });
                }
                if ($scope.selectedScope) {
                    $scope.selectedScope.selected = false;
                    $scope.selectedScope = $scope.selectedScope.$parentNodeScope;
                    $scope.selectedScope.selected = true;
                }

                $state.go('index.structure.housing-unit-group', {
                    huGroupId: hUnitGroup.id,
                    description: hUnitGroup.description
                });
            });

            $scope.$on("selectRoom", function (event, room) {
                /*getRoom(room.id, room.huId).selected = true;*/
                if ($scope.tree_data) {
                    $scope.selectedScope.$modelValue.selected = true; //getRoom(room.id, room.huId).selected = true;
                }
                /* switch to queued action as event is raised before tree_data is filled */
                $scope.selectedNodeType = "selectRoom";
                $scope.selectedNodeInfo = room;
            });

            $scope.$on("preaddRoom", function (event, room) {
                console.log("Showing tree node for new room");

                if ($scope.tree_data) {
                    //$scope.preaddParentNode = getHousingUnit(room.huId).rooms;
                    $scope.preaddParentNode = $scope.selectedScope.$modelValue.rooms;

                    $scope.preaddParentNode.push({
                        id: 0,
                        description: "Nuova stanza",
                        heatCostAllocators: [],
                        selected: true,
                        temporary: true
                    });
                }
                $scope.preaddNodeType = "preaddRoom";
                $scope.preaddNodeInfo = {huId: room.huId, description: "Nuova stanza"};
            });

            $scope.$on("addRoom", function (event, room) {
                console.log("Created room " + room.id);

                if ($scope.preaddNodeInfo) {
                    //var roomNode = getRoom(0, room.housingUnitId);
                    var roomNode = $scope.selectedScope.$modelValue;
                    roomNode.id = room.id;
                    roomNode.description = room.roomDescription;
                    roomNode.temporary = false;

                    if ($scope.selectedScope) {
                        $scope.selectedScope.temporary = false;
                    }

                    $scope.preaddNodeInfo = null;
                } else {
                    //getHousingUnit(room.housingUnitId).rooms.push({id:room.id, description:room.roomDescription, selected:true});
                    $scope.selectedScope.$parentNodeScope.$modelValue.rooms.push({
                        id: room.id,
                        description: room.roomDescription,
                        selected: true
                    });
                }

                $state.go('index.structure.room', {
                    huId: room.housingUnitId,
                    roomId: room.id,
                    description: room.roomDescription
                });
            });

            $scope.$on("editRoom", function (event, data) {
                getCondominiumTree();
            });
            $scope.$on("removeRoom", function (event, room) {
                //var hUnit = getHousingUnit(room.huId);
                var hUnit = $scope.selectedScope.$parentNodeScope.$modelValue;

                _.remove(hUnit.rooms, function (node) {
                    return node.id == room.id;
                });
                if ($scope.selectedScope) {
                    $scope.selectedScope = $scope.selectedScope.$parentNodeScope;
                    $scope.selectedScope.selected = true;
                }
                $state.go('index.structure.housing-unit', {huId: hUnit.id, description: hUnit.description});
            });

            $scope.$on("backRoom", function (event, room) {
                var hUnit = $scope.selectedScope.$parentNodeScope.$modelValue;
                if (room.id == 'new') {
                    _.remove(hUnit.rooms, function (node) {
                        return node.id == 0;
                    });
                }
                if ($scope.selectedScope) {
                    $scope.selectedScope.selected = false;
                    $scope.selectedScope = $scope.selectedScope.$parentNodeScope;
                    $scope.selectedScope.selected = true;
                }

                $state.go('index.structure.housing-unit', {huId: hUnit.id, description: hUnit.description});
            });


            $scope.$on("selectMeter", function (event, meter) {
                /*getMeter(meter.id, meter.huId).selected = true;*/
                if ($scope.tree_data) {
                    $scope.selectedScope.$modelValue.selected = true; //getMeter(meter.id, meter.huId).selected = true;
                }
                /* switch to queued action as event is raised before tree_data is filled */
                $scope.selectedNodeType = "selectMeter";
                $scope.selectedNodeInfo = meter;
            });

            $scope.$on("preaddMeter", function (event, meter) {
                console.log("Showing tree node for new heat meter");

                if ($scope.tree_data) {
                    //$scope.preaddParentNode = getHousingUnit(meter.huId).heatMeters;
                    $scope.preaddParentNode = $scope.selectedScope.$modelValue.heatMeters;
                    $scope.preaddParentNode.push({
                        id: 0,
                        description: "Nuovo contacalorie",
                        selected: true,
                        temporary: true
                    });
                }
                $scope.preaddNodeType = "preaddMeter";
                $scope.preaddNodeInfo = {huId: meter.huId, description: "Nuovo contacalorie"};
            });

            $scope.$on("addMeter", function (event, meter) {
                console.log("Created meter " + meter.id);

                if ($scope.preaddNodeInfo) {
                    //var meterNode = getMeter(0, meter.housingUnitId);
                    var meterNode = $scope.selectedScope.$modelValue;
                    meterNode.id = meter.id;
                    meterNode.description = meter.description;
                    meterNode.temporary = false;

                    if ($scope.selectedScope) {
                        $scope.selectedScope.temporary = false;
                    }

                    $scope.preaddNodeInfo = null;
                } else {
                    //getHousingUnit(meter.housingUnitId).heatMeters.push({id:meter.id, description:meter.description, selected:true});
                    $scope.selectedScope.$parentNodeScope.$modelValue.heatMeters.push({
                        id: room.id,
                        description: room.roomDescription,
                        selected: true
                    });
                }

                $state.go('index.structure.meter', {
                    huId: meter.housingUnitId,
                    meterId: meter.id,
                    description: meter.description
                });
            });

            $scope.$on("editMeter", function (event, data) {
                getCondominiumTree();
            });

            $scope.$on("removeMeter", function (event, meter) {
                //var hUnit = getHousingUnit(meter.huId);
                var hUnit = $scope.selectedScope.$parentNodeScope.$modelValue;
                _.remove(hUnit.heatMeters, function (node) {
                    return node.id == meter.id;
                });
                if ($scope.selectedScope) {
                    $scope.selectedScope = $scope.selectedScope.$parentNodeScope;
                    $scope.selectedScope.selected = true;
                }
                $state.go('index.structure.housing-unit', {huId: hUnit.id, description: hUnit.description});
            });

            $scope.$on("backMeter", function (event, meter) {
                var hUnit = $scope.selectedScope.$parentNodeScope.$modelValue;
                if (meter.id == 'new') {
                    _.remove(hUnit.heatMeters, function (node) {
                        return node.id == 0;
                    });
                }
                if ($scope.selectedScope) {
                    $scope.selectedScope.selected = false;
                    $scope.selectedScope = $scope.selectedScope.$parentNodeScope;
                    $scope.selectedScope.selected = true;
                }

                $state.go('index.structure.housing-unit', {huId: hUnit.id, description: hUnit.description});
            });


            //WATER METER
            $scope.$on("selectWaterMeter", function (event, meter) {
                /*getMeter(meter.id, meter.huId).selected = true;*/
                if ($scope.tree_data) {
                    $scope.selectedScope.$modelValue.selected = true; //getMeter(meter.id, meter.huId).selected = true;
                }
                /* switch to queued action as event is raised before tree_data is filled */
                $scope.selectedNodeType = "selectWaterMeter";
                $scope.selectedNodeInfo = meter;
            });

            $scope.$on("preaddWaterMeter", function (event, meter) {
                console.log("Showing tree node for new water meter");

                if ($scope.tree_data) {
                    //$scope.preaddParentNode = getHousingUnit(meter.huId).heatMeters;
                    $scope.preaddParentNode = $scope.selectedScope.$modelValue.waterMeters;
                    $scope.preaddParentNode.push({
                        id: 0,
                        description: "Nuovo contatore acqua",
                        selected: true,
                        temporary: true
                    });
                }
                $scope.preaddNodeType = "preaddWaterMeter";
                $scope.preaddNodeInfo = {huId: meter.huId, description: "Nuovo contatore acqua"};
            });

            $scope.$on("addWaterMeter", function (event, meter) {
                console.log("Created water meter " + meter.id);

                if ($scope.preaddNodeInfo) {
                    //var meterNode = getMeter(0, meter.housingUnitId);
                    var meterNode = $scope.selectedScope.$modelValue;
                    meterNode.id = meter.id;
                    meterNode.description = meter.description;
                    meterNode.temporary = false;

                    if ($scope.selectedScope) {
                        $scope.selectedScope.temporary = false;
                    }

                    $scope.preaddNodeInfo = null;
                } else {
                    //getHousingUnit(meter.housingUnitId).heatMeters.push({id:meter.id, description:meter.description, selected:true});
                    $scope.selectedScope.$parentNodeScope.$modelValue.waterMeters.push({
                        id: room.id,
                        description: room.roomDescription,
                        selected: true
                    });
                }

                $state.go('index.structure.water-meter', {
                    huId: meter.housingUnitId,
                    watermeterId: meter.id,
                    description: meter.description
                });
            });

            $scope.$on("editWaterMeter", function (event, data) {
                getCondominiumTree();
            });

            $scope.$on("removeWaterMeter", function (event, meter) {
                //var hUnit = getHousingUnit(meter.huId);
                var hUnit = $scope.selectedScope.$parentNodeScope.$modelValue;
                _.remove(hUnit.waterMeters, function (node) {
                    return node.id == meter.id;
                });
                if ($scope.selectedScope) {
                    $scope.selectedScope = $scope.selectedScope.$parentNodeScope;
                    $scope.selectedScope.selected = true;
                }
                $state.go('index.structure.housing-unit', {huId: hUnit.id, description: hUnit.description});
            });

            $scope.$on("selectRole", function (event, role) {
                if ($scope.tree_data) {
                    $scope.selectedScope.$modelValue.selected = true;
                }
                /* switch to queued action as event is raised before tree_data is filled */
                $scope.selectedNodeType = "selectRole";
                $scope.selectedNodeInfo = role;
            });

            $scope.$on("preaddRole", function (event, role) {
                console.log("Showing tree node for new person to role");

                if ($scope.tree_data) {
                    if (angular.isUndefined($scope.selectedScope.$modelValue.roles)) {
                        $scope.selectedScope.$modelValue.roles = [];
                    }
                    $scope.preaddParentNode = $scope.selectedScope.$modelValue.roles;
                    $scope.preaddParentNode.push({id: 0, description: "Nuovo Ruolo", selected: true, temporary: true});
                }
                $scope.preaddNodeType = "preaddRole";
                $scope.preaddNodeInfo = {huId: role.huId, description: "Nuovo Ruolo"};
            });

            $scope.$on("addRole", function (event, role) {
                console.log("Created role " + role.id);

                if ($scope.preaddNodeInfo) {
                    var roleNode = $scope.selectedScope.$modelValue;
                    roleNode.id = role.id;
                    roleNode.description = null;
                    roleNode.firstName = role.firstName;
                    roleNode.lastName = role.lastName;
                    roleNode.companyName = role.companyName;
                    roleNode.roleType = role.roleType;
                    roleNode.rolePercentage = role.rolePercentage;
                    roleNode.temporary = false;

                    if ($scope.selectedScope) {
                        $scope.selectedScope.temporary = false;
                    }

                    $scope.preaddNodeInfo = null;
                } else {
                    $scope.selectedScope.$parentNodeScope.$modelValue.roles.push(
                        {id: role.id, roleType: role.roleType, rolePercentage: role.rolePercentage, selected: true});
                }

                $state.go('index.structure.role', {
                    huId: role.housingUnitId,
                    roleId: role.id,
                    roleType: role.roleType,
                    rolePercentage: role.rolePercentage
                });
            });

            $scope.$on("editRole", function (event, data) {
                getCondominiumTree();
            });

            $scope.$on("removeRole", function (event, role) {

                var hUnit = $scope.selectedScope.$parentNodeScope.$modelValue;
                _.remove(hUnit.roles, function (node) {
                    return node.id == role.id;
                });
                if ($scope.selectedScope) {
                    $scope.selectedScope = $scope.selectedScope.$parentNodeScope;
                    $scope.selectedScope.selected = true;
                }
                $state.go('index.structure.housing-unit', {huId: hUnit.id, description: hUnit.description});
            });

            $scope.$on("backRole", function (event, role) {
                var hUnit = $scope.selectedScope.$parentNodeScope.$modelValue;
                if (role.id == 'new') {
                    _.remove(hUnit.roles, function (node) {
                        return node.id == 0;
                    });
                }
                if ($scope.selectedScope) {
                    $scope.selectedScope.selected = false;
                    $scope.selectedScope = $scope.selectedScope.$parentNodeScope;
                    $scope.selectedScope.selected = true;
                }

                $state.go('index.structure.housing-unit', {huId: hUnit.id, description: hUnit.description});
            });

            $scope.$on("selectHeatCostAllocator", function (event, costAllocator) {
                /*var roomNode = getRoom(costAllocator.roomId);
                 var costAllocatorId = costAllocator.id;
                 var costAllocatorNode = _.find(roomNode.heatCostAllocators, function(cAlc) {
                 return costAllocator.id == costAllocatorId;
                 });
                 costAllocatorNode.selected = true;*/
                if ($scope.tree_data) {
                    $scope.selectedScope.$modelValue.selected = true;
                }
                /* switch to queued action as event is raised before tree_data is filled */
                $scope.selectedNodeType = "selectHeatCostAllocator";
                $scope.selectedNodeInfo = costAllocator;
            });

            $scope.$on("preaddHeatCostAllocator", function (event, costAllocator) {
                console.log("Showing tree node for new heat cost allocator");

                if ($scope.tree_data) {
                    //$scope.preaddParentNode = getRoom(costAllocator.roomId, costAllocator.huId).heatCostAllocators;
                    $scope.preaddParentNode = $scope.selectedScope.$modelValue.heatCostAllocators;
                    $scope.preaddParentNode.push({
                        id: 0,
                        description: "Nuovo ripartitore",
                        temporary: true,
                        selected: true
                    });
                }
                $scope.preaddNodeType = "preaddHeatCostAllocator";
                $scope.preaddNodeInfo = {roomId: costAllocator.roomId, description: "Nuovo ripartitore"};
            });

            $scope.$on("addHeatCostAllocator", function (event, costAllocator) {
                console.log("Created costAllocator " + costAllocator.id);

                if ($scope.preaddNodeInfo) {
                    /*var roomNode = getRoom(costAllocator.roomId);
                     var costAllocatorNode = _.find(roomNode.heatCostAllocators, function (costAllocator) {
                     return costAllocator.id == 0;
                     });*/
                    var costAllocatorNode = $scope.selectedScope.$modelValue;
                    costAllocatorNode.id = costAllocator.id;
                    costAllocatorNode.description = costAllocator.heatingElementDescription;
                    costAllocatorNode.temporary = false;

                    if ($scope.selectedScope) {
                        $scope.selectedScope.temporary = false;
                    }

                    $scope.preaddNodeInfo = null;
                } else {
                    //getRoom(costAllocator.roomId).heatCostAllocators.push({id:costAllocator.id, description:costAllocator.heatingElementDescription, selected:true});
                    $scope.selectedScope.$parentNodeScope.$modelValue.heatCostAllocators.push({
                        id: costAllocator.id,
                        description: costAllocator.heatingElementDescription,
                        selected: true
                    });
                }

                $state.go('index.structure.cost-allocator', {
                    roomId: costAllocator.roomId,
                    costAllocatorId: costAllocator.id,
                    description: costAllocator.heatingElementDescription
                });
            });

            $scope.$on("editHeatCostAllocator", function (event, data) {
                getCondominiumTree();
            });

            $scope.$on("removeHeatCostAllocator", function (event, costAllocator) {
                //var room = getRoom(costAllocator.roomId);
                var room = $scope.selectedScope.$parentNodeScope.$modelValue;
                _.remove(room.heatCostAllocators, function (node) {
                    return node.id == costAllocator.id;
                });
                if ($scope.selectedScope) {
                    $scope.selectedScope = $scope.selectedScope.$parentNodeScope;
                    $scope.selectedScope.selected = true;
                }
                $state.go('index.structure.room', {
                    huId: costAllocator.huId,
                    roomId: room.id,
                    description: room.description
                });
            });

            $scope.$on("backHeatCostAllocator", function (event, costAllocator) {
                var room = $scope.selectedScope.$parentNodeScope.$modelValue;
                if (costAllocator.id == 'new') {
                    _.remove(room.heatCostAllocators, function (node) {
                        return node.id == 0;
                    });
                }
                if ($scope.selectedScope) {
                    $scope.selectedScope.selected = false;
                    $scope.selectedScope = $scope.selectedScope.$parentNodeScope;
                    $scope.selectedScope.selected = true;
                }

                $state.go('index.structure.room', {huId: null, roomId: room.id, description: room.description});
            });


            /* Invoked (max) once after model is loaded (tree setup) */
            var selectNode = function (nodeType, payload) {
                var selectedNode;
                if (nodeType == 'selectCondominium') {
                    selectedNode = $scope.tree_data.condominiums[0];
                } else if (nodeType == 'selectHousingUnitGroup') {
                    selectedNode = getHousingUnitGroup(payload.id);
                } else if (nodeType == 'selectHousingUnit') {
                    selectedNode = getHousingUnit(payload.id, payload.huGroupId);
                } else if (nodeType == 'selectRoom') {
                    selectedNode = getRoom(payload.id, payload.huId);
                } else if (nodeType == 'selectMeter') {
                    selectedNode = getMeter(payload.id, payload.huId);
                } else if (nodeType == 'selectWaterMeter') {
                    selectedNode = getWaterMeter(payload.id, payload.huId);
                } else if (nodeType == 'selectRole') {
                    selectedNode = getRole(payload.id, payload.huId);
                } else if (nodeType == 'selectHeatCostAllocator') {
                    var roomNode = getRoom(payload.roomId, payload.huId);
                    var costAllocatorId = payload.id;
                    selectedNode = _.find(roomNode.heatCostAllocators, function (costAllocator) {
                        return costAllocator.id == costAllocatorId;
                    });
                }

                selectedNode.selected = true;
            }

            /* Invoked (max) once after model is loaded (tree setup) */
            var preaddNode = function (nodeType, payload) {
                var nodeCollection;
                if (nodeType == 'preaddHousingUnitGroup') {
                    nodeCollection = $scope.tree_data.condominiums[0].housingUnitGroups;
                } else if (nodeType == 'preaddHousingUnit') {
                    nodeCollection = getHousingUnitGroup(payload.huGroupId).housingUnits;
                } else if (nodeType == 'preaddRoom') {
                    nodeCollection = getHousingUnit(payload.huId, payload.huGroupId).rooms;
                } else if (nodeType == 'preaddMeter') {
                    nodeCollection = getHousingUnit(payload.huId, payload.huGroupId).heatMeters;
                } else if (nodeType == 'preaddWaterMeter') {
                    nodeCollection = getHousingUnit(payload.huId, payload.huGroupId).waterMeters;
                } else if (nodeType == 'preaddRole') {
                    nodeCollection = getHousingUnit(payload.huId, payload.huGroupId).roles;
                } else if (nodeType == 'preaddHeatCostAllocator') {
                    nodeCollection = getRoom(payload.roomId, payload.huId).heatCostAllocators;
                }

                if (nodeCollection) {
                    nodeCollection.push({id: 0, description: payload.description, selected: true, temporary: true});
                }
            }

            /*
             var removeNode = function(nodeType, payload) {
             var nodeCollection;
             if (nodeType == 'removeHousingUnit') {
             nodeCollection = $scope.tree_data.condominiums[0].housingUnits;
             } else if (nodeType == 'removeRoom') {
             nodeCollection = getHousingUnit(payload.huId).rooms;
             } else if (nodeType == 'removeMeter') {
             nodeCollection = getHousingUnit(payload.huId).heatMeters;
             } else if (nodeType == 'removeHeatCostAllocator') {
             nodeCollection = getRoom(payload.roomId, payload.huId).heatCostAllocators;
             }

             if (nodeCollection) {
             _.remove(nodeCollection, function (node) {
             return node.id == payload.id;
             });
             }
             }*/

            /* Invoked for each tree node */
            $scope.preselectNode = function (node) {
                if (node.selected) {
                    if ($scope.selectedScope) {
                        $scope.selectedScope.selected = false;
                    }
                    this.selected = true;
                    $scope.selectedScope = this;

                    node.selected = false;
                } else {
                    this.selected = false;
                }

                if (node.temporary) {
                    this.temporary = true;
                }
            }

            $scope.condominiumClicked = function () {
                console.log("Selected condominium");

                if ($scope.selectedScope) {
                    $scope.selectedScope.selected = false;
                }
                this.selected = true;
                $scope.selectedScope = this;

                $scope.cleanupTemporaryNodes();

                $state.go('index.structure.root');
            }

            $scope.hUnitGroupClicked = function (hUnitGroup) {

                console.log("Selected housing unit group " + hUnitGroup.id + "   condominiumId=" + $scope.condominiumId);

                //Se viene cliccato sull'albero il nodo temporaneo non deve fare nula
                if (hUnitGroup.temporary) return;

                if ($scope.selectedScope) {
                    $scope.selectedScope.selected = false;
                }
                this.selected = true;
                $scope.selectedScope = this;

                $scope.cleanupTemporaryNodes();

                $state.go('index.structure.housing-unit-group', {
                    huGroupId: hUnitGroup.id,
                    description: hUnitGroup.description
                });
            }

            $scope.hUnitClicked = function (hUnit) {

                console.log("Selected housing unit " + hUnit.id + "   condominiumId=" + $scope.condominiumId);

                //Se viene cliccato sull'albero il nodo temporaneo non deve fare nula
                if (hUnit.temporary) return;

                if ($scope.selectedScope) {
                    $scope.selectedScope.selected = false;
                }
                this.selected = true;
                $scope.selectedScope = this;

                $scope.cleanupTemporaryNodes();

                $state.go('index.structure.housing-unit', {
                    huGroupId: this.hUnitGroup.id,
                    huId: hUnit.id,
                    description: hUnit.description
                });
            }

            $scope.roomClicked = function (room) {
                console.log("Selected room " + room.id);

                //Se viene cliccato sull'albero il nodo temporaneo non deve fare nula
                if (room.temporary) return;

                if ($scope.selectedScope) {
                    $scope.selectedScope.selected = false;
                }
                this.selected = true;
                $scope.selectedScope = this;

                $scope.cleanupTemporaryNodes();

                $state.go('index.structure.room', {
                    huId: this.hunit.id,
                    roomId: room.id,
                    description: room.description
                });
            }

            $scope.meterClicked = function (meter) {
                console.log("Selected meter " + meter.id);

                //Se viene cliccato sull'albero il nodo temporaneo non deve fare nula
                if (meter.temporary) return;

                if ($scope.selectedScope) {
                    $scope.selectedScope.selected = false;
                }
                this.selected = true;
                $scope.selectedScope = this;

                $scope.cleanupTemporaryNodes();

                $state.go('index.structure.meter', {
                    huId: this.hunit.id,
                    meterId: meter.id,
                    description: meter.description
                });
            }

            $scope.waterMeterClicked = function (meter) {
                console.log("Selected water meter " + meter.id);

                //Se viene cliccato sull'albero il nodo temporaneo non deve fare nula
                if (meter.temporary) return;

                if ($scope.selectedScope) {
                    $scope.selectedScope.selected = false;
                }
                this.selected = true;
                $scope.selectedScope = this;

                $scope.cleanupTemporaryNodes();

                $state.go('index.structure.water-meter', {
                    huId: this.hunit.id,
                    watermeterId: meter.id,
                    description: meter.description
                });
            }

            $scope.roleClicked = function (role) {
                console.log("Selected role " + role.id);

                //Se viene cliccato sull'albero il nodo temporaneo non deve fare nula
                if (role.temporary) return;

                if ($scope.selectedScope) {
                    $scope.selectedScope.selected = false;
                }
                this.selected = true;
                $scope.selectedScope = this;

                $scope.cleanupTemporaryNodes();

                $state.go('index.structure.role', {
                    huId: this.hunit.id,
                    roleId: role.id,
                    description: role.description
                });
            }

            $scope.allocatorClicked = function (costAllocator) {
                console.log("Selected cost allocator " + costAllocator.id);

                //Se viene cliccato sull'albero il nodo temporaneo non deve fare nula
                if (costAllocator.temporary) return;

                if ($scope.selectedScope) {
                    $scope.selectedScope.selected = false;
                }
                this.selected = true;
                $scope.selectedScope = this;

                $scope.cleanupTemporaryNodes();

                $state.go('index.structure.cost-allocator', {
                    huId: this.hunit.id,
                    roomId: this.room.id,
                    costAllocatorId: costAllocator.id,
                    description: costAllocator.description
                });
            }

            $scope.cleanupTemporaryNodes = function () {
                var nodeType = $scope.preaddNodeType;
                var payload = $scope.preaddNodeInfo;
                if ($scope.preaddNodeInfo) {
                    if (nodeType == 'preaddHousingUnitGroup') {
                        _.remove($scope.tree_data.condominiums[0].housingUnitGroups, function (node) {
                            return node.id == 0;
                        });
                    } else if (nodeType == 'preaddHousingUnit') {
                        removeTempNode(getHousingUnitGroup(payload.huGroupId).housingUnits);
                    } else if (nodeType == 'preaddRoom') {
                        removeTempNode(getHousingUnit(payload.huId).rooms);
                    } else if (nodeType == 'preaddMeter') {
                        removeTempNode(getHousingUnit(payload.huId).heatMeters);
                    } else if (nodeType == 'preaddWaterMeter') {
                        removeTempNode(getHousingUnit(payload.huId).waterMeters);
                    } else if (nodeType == 'preaddHeatCostAllocator') {
                        removeTempNode(getRoom(payload.roomId).heatCostAllocators);
                    } else if (nodeType == 'preaddRole') {
                        removeTempNode(getHousingUnit(payload.huId).roles);
                    }

                    $scope.preaddNodeType = null;
                    $scope.preaddNodeInfo = null;
                }
            }

            var removeTempNode = function (nodes) {
                var tempNode = _.remove(nodes, function (node) {
                    return node.id == 0;
                });
            }

            $scope.hasHousingUnitGroups = function (condominium) {
                return (condominium.housingUnitGroups && condominium.housingUnitGroups.length > 0);
            }

            $scope.hasHousingUnits = function (housingUnitGroup) {
                return (housingUnitGroup.housingUnits && housingUnitGroup.housingUnits.length > 0);
            }

            $scope.hasRoles = function (hUnit) {
                return (hUnit.roles && hUnit.roles.length > 0);
            }

            $scope.hasAllocators = function (room) {
                return (room.heatCostAllocators && room.heatCostAllocators.length > 0);
            }

            $scope.isSelected = function () {
                return $scope.selected;
            }

            $scope.treeOptions = {
                accept: function (sourceNodeScope, destNodesScope, destIndex) {
                    return true;
                },
                itemClicked: function (sourceItem, clickedElmDragged) {
                    $scope.$apply(function () {
                        $scope.selectedItem = sourceItem;
                    });
                }
            };

        }
    ]);

