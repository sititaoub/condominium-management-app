(function () {
    'use strict';

    angular
        .module("condominiumManagementApp.legacyStructure", [
            'condominiumManagementApp.core',
            'condominiumManagementApp.commons'
        ]);
})();