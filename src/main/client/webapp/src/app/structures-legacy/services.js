"use strict";
angular.module("condominiumManagementApp.legacyStructure")
    .factory("CondominiumResource", ["$resource", "contextAddress", function ($resource, contextAddress) {
        return $resource(contextAddress + "v1/condominiums/:id?statusId=:statusId",
            {id: '@id', statusId: '@statusId'},

            {
                update: {method: 'PUT'},
                query: {method: 'GET', isArray: false}
            }
        );
    }])
    .factory("CondominiumCrudResource", ["$resource", "contextAddress", function ($resource, contextAddress) {
        return $resource(contextAddress + "v1/condominiums/:id",
            {id: '@id'},
            {
                update: {method: 'PUT'},
                query: {method: 'GET', isArray: false}
            }
        );

    }])
    .factory("HousingUnitCrudResource", ["$resource", "contextAddress", function ($resource, contextAddress) {
        return $resource(contextAddress + "v1/housing-units/:id",
            {id: '@id'},

            {
                update: {method: 'PUT'}
            }
        );

    }])
    .service('CondominiumTreeService', ['$http', 'contextAddress', function ($http, contextAddress) {
        return {
            getCondominiumTree: function (id) {
                return $http({
                    method: 'GET',
                    url: contextAddress + 'v1/condominium-structure/' + id + '/with-persons'
                });
            }
        }
    }])
    .service('CondominiumSelection', ['$rootScope', 'CondominiumResource', function ($rootScope, CondominiumResource) {
        var self = {
            loadCondominium: function (id) {
                $rootScope.condominiumId = id;
                $rootScope.condominium_data = CondominiumResource.get({id: id}, function (data) {

                });
            }
        };

        return self;
    }])
    .service('CondominiumHeatCost', ["$resource", "contextAddressManagement", "$q", function ($resource, contextAddressManagement, $q) {

        this.resource = $resource(contextAddressManagement + "v1/:id/heatcosts", {
            id: '@id'
        }, {
            generate: {method: 'POST'}
        });
        this.mockup = {
            generate: function (params, cb) {
                var data = {
                    "totalHeatAmount": "2945.560",
                    "totalWaterAmount": "0.000",
                    "totalCliHeatCostAllocator": "7090.200",
                    "totalCliHeatMeter": "19260.000",
                    "totalCliCost": "407.671",
                    "totalAcsHeatMeter": "119900.000",
                    "totalAcsHeatCost": "2537.889",
                    "totalAcsWaterMeter": "0.000",
                    "totalAcsWaterCost": "0.000",
                    "totalAfsWaterMeter": "0.000",
                    "totalAfsWaterCost": "0.000",
                    "totalCliSystemHeatLoss": "4815.000",
                    "totalCliSystemHeatLossCost": "101.918",
                    "totalAcsSystemHeatLoss": "119900.000",
                    "totalAcsSystemHeatLossCost": "2537.889",
                    "totalHeatMeter": "139160.000",
                    "totalWaterMeter": "0.000",
                    "totalCliVoluntaryMeter": "14445.000",
                    "totalCliVoluntaryCost": "305.753",
                    "totalAcsVoluntaryMeter": "0.000",
                    "totalAcsVoluntaryCost": "0.000",
                    "totalHeatLossMeter": "124715.000",
                    "totalHeatLossCost": "2639.807",
                    "totalHeatVoluntaryMeter": "14445.000",
                    "totalHeatVoluntaryCost": "305.753",
                    "totalOtherCosts": "1656.160",
                    "systemHeatLoss": "25.000",
                    "cliStartDate": "2016-11-30T23:00:00Z",
                    "cliEndDate": "2017-04-14T22:00:00Z",
                    "reportingStartDate": "2016-09-01T22:00:00Z",
                    "reportingEndDate": "2017-08-03T08:50:09Z",
                    "thermicYearStartDate": "2016-11-30T23:00:00Z",
                    "thermicYearEndDate": "2017-04-14T22:00:00Z",
                    "listCliHeatCostReporting": {
                        "number": 0,
                        "size": 8,
                        "totalPages": 0,
                        "totalElements": 8,
                        "content": [{
                            "id": 1,
                            "apartmentNumber": "1",
                            "building": "default",
                            "stair": "1",
                            "floor": "1",
                            "owner": "Faggiano Antonio",
                            "heatingThousandthShare": "140.650",
                            "costAllocator": "1015.500",
                            "meter": "1901.670",
                            "systemHeatLoss": "791.156",
                            "costAmount": "30.715",
                            "systemLossCostAmount": "12.779",
                            "totalHeatCost": "43.494"
                        }, {
                            "id": 2,
                            "apartmentNumber": "3",
                            "building": "default",
                            "stair": "1",
                            "floor": "2",
                            "owner": "Rossi Mario",
                            "heatingThousandthShare": "102.340",
                            "costAllocator": "1235.700",
                            "meter": "2314.030",
                            "systemHeatLoss": "575.663",
                            "costAmount": "37.375",
                            "systemLossCostAmount": "9.298",
                            "totalHeatCost": "46.673"
                        }, {
                            "id": 3,
                            "apartmentNumber": "4",
                            "building": "default",
                            "stair": "1",
                            "floor": "2",
                            "owner": "Esposito Maria",
                            "heatingThousandthShare": "87.010",
                            "costAllocator": "1484.300",
                            "meter": "2779.570",
                            "systemHeatLoss": "489.431",
                            "costAmount": "44.895",
                            "systemLossCostAmount": "7.905",
                            "totalHeatCost": "52.800"
                        }, {
                            "id": 35,
                            "apartmentNumber": "8",
                            "building": "default",
                            "stair": "1",
                            "floor": "3",
                            "owner": "Monti Marcella",
                            "heatingThousandthShare": "137.520",
                            "costAllocator": "0.000",
                            "meter": "0.000",
                            "systemHeatLoss": "773.550",
                            "costAmount": "0.000",
                            "systemLossCostAmount": "12.494",
                            "totalHeatCost": "12.494"
                        }, {
                            "id": 4,
                            "apartmentNumber": "5",
                            "building": "default",
                            "stair": "1",
                            "floor": "2",
                            "owner": "Selva Giordano",
                            "heatingThousandthShare": "102.340",
                            "costAllocator": "1310.000",
                            "meter": "2453.170",
                            "systemHeatLoss": "575.663",
                            "costAmount": "39.623",
                            "systemLossCostAmount": "9.298",
                            "totalHeatCost": "48.921"
                        }, {
                            "id": 5,
                            "apartmentNumber": "6",
                            "building": "default",
                            "stair": "1",
                            "floor": "3",
                            "owner": "Alfani Lazzari",
                            "heatingThousandthShare": "141.600",
                            "costAllocator": "1195.500",
                            "meter": "2238.750",
                            "systemHeatLoss": "796.500",
                            "costAmount": "36.160",
                            "systemLossCostAmount": "12.865",
                            "totalHeatCost": "49.024"
                        }, {
                            "id": 6,
                            "apartmentNumber": "7",
                            "building": "default",
                            "stair": "1",
                            "floor": "3",
                            "owner": "Scarano Elisa",
                            "heatingThousandthShare": "165.330",
                            "costAllocator": "1015.900",
                            "meter": "1902.420",
                            "systemHeatLoss": "929.981",
                            "costAmount": "30.727",
                            "systemLossCostAmount": "15.021",
                            "totalHeatCost": "45.748"
                        }, {
                            "id": 7,
                            "apartmentNumber": "2",
                            "building": "default",
                            "stair": "1",
                            "floor": "1",
                            "owner": "Rocca Gianluca",
                            "heatingThousandthShare": "123.210",
                            "costAllocator": "1754.400",
                            "meter": "3285.380",
                            "systemHeatLoss": "693.056",
                            "costAmount": "53.064",
                            "systemLossCostAmount": "11.194",
                            "totalHeatCost": "64.258"
                        }]
                    },
                    "listAcsHeatCostReporting": {
                        "number": 0,
                        "size": 8,
                        "totalPages": 0,
                        "totalElements": 8,
                        "content": [{
                            "id": 1,
                            "apartmentNumber": "1",
                            "building": "default",
                            "stair": "1",
                            "floor": "1",
                            "owner": "Faggiano Antonio",
                            "acsThousandthShare": "136.750",
                            "waterMeter": "0.000",
                            "heatMeter": "16396.330",
                            "heatCostAmount": "264.828",
                            "waterCostAmount": "0.000",
                            "systemHeatLoss": "0.000",
                            "systemLossCostAmount": "0.000",
                            "totalAcsCost": "264.828"
                        }, {
                            "id": 2,
                            "apartmentNumber": "3",
                            "building": "default",
                            "stair": "1",
                            "floor": "2",
                            "owner": "Rossi Mario",
                            "acsThousandthShare": "136.750",
                            "waterMeter": "0.000",
                            "heatMeter": "16396.330",
                            "heatCostAmount": "264.828",
                            "waterCostAmount": "0.000",
                            "systemHeatLoss": "0.000",
                            "systemLossCostAmount": "0.000",
                            "totalAcsCost": "264.828"
                        }, {
                            "id": 3,
                            "apartmentNumber": "4",
                            "building": "default",
                            "stair": "1",
                            "floor": "2",
                            "owner": "Esposito Maria",
                            "acsThousandthShare": "135.210",
                            "waterMeter": "0.000",
                            "heatMeter": "16211.680",
                            "heatCostAmount": "261.846",
                            "waterCostAmount": "0.000",
                            "systemHeatLoss": "0.000",
                            "systemLossCostAmount": "0.000",
                            "totalAcsCost": "261.846"
                        }, {
                            "id": 35,
                            "apartmentNumber": "8",
                            "building": "default",
                            "stair": "1",
                            "floor": "3",
                            "owner": "Monti Marcella",
                            "acsThousandthShare": "54.700",
                            "waterMeter": "0.000",
                            "heatMeter": "6558.530",
                            "heatCostAmount": "105.931",
                            "waterCostAmount": "0.000",
                            "systemHeatLoss": "0.000",
                            "systemLossCostAmount": "0.000",
                            "totalAcsCost": "105.931"
                        }, {
                            "id": 4,
                            "apartmentNumber": "5",
                            "building": "default",
                            "stair": "1",
                            "floor": "2",
                            "owner": "Selva Giordano",
                            "acsThousandthShare": "136.680",
                            "waterMeter": "0.000",
                            "heatMeter": "16387.930",
                            "heatCostAmount": "264.693",
                            "waterCostAmount": "0.000",
                            "systemHeatLoss": "0.000",
                            "systemLossCostAmount": "0.000",
                            "totalAcsCost": "264.693"
                        }, {
                            "id": 5,
                            "apartmentNumber": "6",
                            "building": "default",
                            "stair": "1",
                            "floor": "3",
                            "owner": "Alfani Lazzari",
                            "acsThousandthShare": "135.210",
                            "waterMeter": "0.000",
                            "heatMeter": "16211.680",
                            "heatCostAmount": "261.846",
                            "waterCostAmount": "0.000",
                            "systemHeatLoss": "0.000",
                            "systemLossCostAmount": "0.000",
                            "totalAcsCost": "261.846"
                        }, {
                            "id": 6,
                            "apartmentNumber": "7",
                            "building": "default",
                            "stair": "1",
                            "floor": "3",
                            "owner": "Scarano Elisa",
                            "acsThousandthShare": "129.490",
                            "waterMeter": "0.000",
                            "heatMeter": "15525.850",
                            "heatCostAmount": "250.769",
                            "waterCostAmount": "0.000",
                            "systemHeatLoss": "0.000",
                            "systemLossCostAmount": "0.000",
                            "totalAcsCost": "250.769"
                        }, {
                            "id": 7,
                            "apartmentNumber": "2",
                            "building": "default",
                            "stair": "1",
                            "floor": "1",
                            "owner": "Rocca Gianluca",
                            "acsThousandthShare": "135.210",
                            "waterMeter": "0.000",
                            "heatMeter": "16211.680",
                            "heatCostAmount": "261.846",
                            "waterCostAmount": "0.000",
                            "systemHeatLoss": "0.000",
                            "systemLossCostAmount": "0.000",
                            "totalAcsCost": "261.846"
                        }]
                    },
                    "listAfsCostReporting": {
                        "number": 0,
                        "size": 0,
                        "totalPages": 0,
                        "totalElements": 0,
                        "content": []
                    },
                    "listCostSummaryReporting": {
                        "number": 0,
                        "size": 8,
                        "totalPages": 0,
                        "totalElements": 8,
                        "content": [{
                            "id": 1,
                            "apartmentNumber": "1",
                            "building": "default",
                            "stair": "1",
                            "floor": "1",
                            "owner": "Faggiano Antonio",
                            "heatingThousandthShare": "140.650",
                            "heatingThousandthCost": "323.495",
                            "totalHeatCost": "43.494",
                            "totalAcsCost": "264.828",
                            "totalAfsCost": "0.000",
                            "totalCost": "308.322",
                            "diffCost": "-15.173",
                            "diffCostPerc": "-4.690"
                        }, {
                            "id": 2,
                            "apartmentNumber": "3",
                            "building": "default",
                            "stair": "1",
                            "floor": "2",
                            "owner": "Rossi Mario",
                            "heatingThousandthShare": "102.340",
                            "heatingThousandthCost": "235.382",
                            "totalHeatCost": "46.673",
                            "totalAcsCost": "264.828",
                            "totalAfsCost": "0.000",
                            "totalCost": "311.502",
                            "diffCost": "76.120",
                            "diffCostPerc": "32.339"
                        }, {
                            "id": 3,
                            "apartmentNumber": "4",
                            "building": "default",
                            "stair": "1",
                            "floor": "2",
                            "owner": "Esposito Maria",
                            "heatingThousandthShare": "87.010",
                            "heatingThousandthCost": "200.123",
                            "totalHeatCost": "52.800",
                            "totalAcsCost": "261.846",
                            "totalAfsCost": "0.000",
                            "totalCost": "314.646",
                            "diffCost": "114.523",
                            "diffCostPerc": "57.226"
                        }, {
                            "id": 35,
                            "apartmentNumber": "8",
                            "building": "default",
                            "stair": "1",
                            "floor": "3",
                            "owner": "Monti Marcella",
                            "heatingThousandthShare": "137.520",
                            "heatingThousandthCost": "316.296",
                            "totalHeatCost": "12.494",
                            "totalAcsCost": "105.931",
                            "totalAfsCost": "0.000",
                            "totalCost": "118.425",
                            "diffCost": "-197.871",
                            "diffCostPerc": "-62.559"
                        }, {
                            "id": 4,
                            "apartmentNumber": "5",
                            "building": "default",
                            "stair": "1",
                            "floor": "2",
                            "owner": "Selva Giordano",
                            "heatingThousandthShare": "102.340",
                            "heatingThousandthCost": "235.382",
                            "totalHeatCost": "48.921",
                            "totalAcsCost": "264.693",
                            "totalAfsCost": "0.000",
                            "totalCost": "313.613",
                            "diffCost": "78.231",
                            "diffCostPerc": "33.236"
                        }, {
                            "id": 5,
                            "apartmentNumber": "6",
                            "building": "default",
                            "stair": "1",
                            "floor": "3",
                            "owner": "Alfani Lazzari",
                            "heatingThousandthShare": "141.600",
                            "heatingThousandthCost": "325.680",
                            "totalHeatCost": "49.024",
                            "totalAcsCost": "261.846",
                            "totalAfsCost": "0.000",
                            "totalCost": "310.870",
                            "diffCost": "-14.810",
                            "diffCostPerc": "-4.547"
                        }, {
                            "id": 6,
                            "apartmentNumber": "7",
                            "building": "default",
                            "stair": "1",
                            "floor": "3",
                            "owner": "Scarano Elisa",
                            "heatingThousandthShare": "165.330",
                            "heatingThousandthCost": "380.259",
                            "totalHeatCost": "45.748",
                            "totalAcsCost": "250.769",
                            "totalAfsCost": "0.000",
                            "totalCost": "296.517",
                            "diffCost": "-83.742",
                            "diffCostPerc": "-22.022"
                        }, {
                            "id": 7,
                            "apartmentNumber": "2",
                            "building": "default",
                            "stair": "1",
                            "floor": "1",
                            "owner": "Rocca Gianluca",
                            "heatingThousandthShare": "123.210",
                            "heatingThousandthCost": "283.383",
                            "totalHeatCost": "64.258",
                            "totalAcsCost": "261.846",
                            "totalAfsCost": "0.000",
                            "totalCost": "326.104",
                            "diffCost": "42.721",
                            "diffCostPerc": "15.075"
                        }]
                    },
                    "otherCosts": {
                        "number": 0,
                        "size": 4,
                        "totalPages": 0,
                        "totalElements": 4,
                        "content": [
                            {
                                "oid": "f67051d5-4b22-4508-a863-c94f04f8e98e",
                                "label": "Guardiola",
                                "thousandthCostTotal": 784.54,
                                "thosandthTotalShare": 139160.02,
                                "content": [
                                    {
                                        "id": 1,
                                        "apartmentNumber": "1",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "1",
                                        "owner": "Faggiano Antonio",
                                        "thousandthShare": "18643.310",
                                        "thousandthCost": "105.105"
                                    },
                                    {
                                        "id": 2,
                                        "apartmentNumber": "3",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "2",
                                        "owner": "Rossi Mario",
                                        "thousandthShare": "18774.437",
                                        "thousandthCost": "105.844"
                                    },
                                    {
                                        "id": 3,
                                        "apartmentNumber": "4",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "2",
                                        "owner": "Esposito Maria",
                                        "thousandthShare": "18933.823",
                                        "thousandthCost": "106.743"
                                    },
                                    {
                                        "id": 35,
                                        "apartmentNumber": "8",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "3",
                                        "owner": "Monti Marcella",
                                        "thousandthShare": "7220.689",
                                        "thousandthCost": "40.708"
                                    },
                                    {
                                        "id": 4,
                                        "apartmentNumber": "5",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "2",
                                        "owner": "Selva Giordano",
                                        "thousandthShare": "18856.697",
                                        "thousandthCost": "106.308"
                                    },
                                    {
                                        "id": 5,
                                        "apartmentNumber": "6",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "3",
                                        "owner": "Alfani Lazzari",
                                        "thousandthShare": "19040.614",
                                        "thousandthCost": "107.345"
                                    },
                                    {
                                        "id": 6,
                                        "apartmentNumber": "7",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "3",
                                        "owner": "Scarano Elisa",
                                        "thousandthShare": "18016.964",
                                        "thousandthCost": "101.574"
                                    },
                                    {
                                        "id": 7,
                                        "apartmentNumber": "2",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "1",
                                        "owner": "Rocca Gianluca",
                                        "thousandthShare": "19673.486",
                                        "thousandthCost": "110.913"
                                    }
                                ]
                            },
                            {
                                "oid": "4ccb4e63-0afc-4c9b-b556-450f22c0c620",
                                "label": "Manutenzione Gruppo Termico",
                                "thousandthCostTotal": 782.54,
                                "thosandthTotalShare": 139160.02,
                                "content": [
                                    {
                                        "id": 1,
                                        "apartmentNumber": "1",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "1",
                                        "owner": "Faggiano Antonio",
                                        "thousandthShare": "18643.310",
                                        "thousandthCost": "104.837"
                                    },
                                    {
                                        "id": 2,
                                        "apartmentNumber": "3",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "2",
                                        "owner": "Rossi Mario",
                                        "thousandthShare": "18774.437",
                                        "thousandthCost": "105.574"
                                    },
                                    {
                                        "id": 3,
                                        "apartmentNumber": "4",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "2",
                                        "owner": "Esposito Maria",
                                        "thousandthShare": "18933.823",
                                        "thousandthCost": "106.471"
                                    },
                                    {
                                        "id": 35,
                                        "apartmentNumber": "8",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "3",
                                        "owner": "Monti Marcella",
                                        "thousandthShare": "7220.689",
                                        "thousandthCost": "40.604"
                                    },
                                    {
                                        "id": 4,
                                        "apartmentNumber": "5",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "2",
                                        "owner": "Selva Giordano",
                                        "thousandthShare": "18856.697",
                                        "thousandthCost": "106.037"
                                    },
                                    {
                                        "id": 5,
                                        "apartmentNumber": "6",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "3",
                                        "owner": "Alfani Lazzari",
                                        "thousandthShare": "19040.614",
                                        "thousandthCost": "107.071"
                                    },
                                    {
                                        "id": 6,
                                        "apartmentNumber": "7",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "3",
                                        "owner": "Scarano Elisa",
                                        "thousandthShare": "18016.964",
                                        "thousandthCost": "101.315"
                                    },
                                    {
                                        "id": 7,
                                        "apartmentNumber": "2",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "1",
                                        "owner": "Rocca Gianluca",
                                        "thousandthShare": "19673.486",
                                        "thousandthCost": "110.630"
                                    }
                                ]
                            },
                            {
                                "oid": "62c556db-4c27-4ceb-865e-59ec79d23939",
                                "label": "Corrente Stabile Caldaia",
                                "thousandthCostTotal": 84.54,
                                "thosandthTotalShare": 139160.02,
                                "content": [
                                    {
                                        "id": 1,
                                        "apartmentNumber": "1",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "1",
                                        "owner": "Faggiano Antonio",
                                        "thousandthShare": "18643.310",
                                        "thousandthCost": "11.326"
                                    },
                                    {
                                        "id": 2,
                                        "apartmentNumber": "3",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "2",
                                        "owner": "Rossi Mario",
                                        "thousandthShare": "18774.437",
                                        "thousandthCost": "11.406"
                                    },
                                    {
                                        "id": 3,
                                        "apartmentNumber": "4",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "2",
                                        "owner": "Esposito Maria",
                                        "thousandthShare": "18933.823",
                                        "thousandthCost": "11.502"
                                    },
                                    {
                                        "id": 35,
                                        "apartmentNumber": "8",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "3",
                                        "owner": "Monti Marcella",
                                        "thousandthShare": "7220.689",
                                        "thousandthCost": "4.387"
                                    },
                                    {
                                        "id": 4,
                                        "apartmentNumber": "5",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "2",
                                        "owner": "Selva Giordano",
                                        "thousandthShare": "18856.697",
                                        "thousandthCost": "11.455"
                                    },
                                    {
                                        "id": 5,
                                        "apartmentNumber": "6",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "3",
                                        "owner": "Alfani Lazzari",
                                        "thousandthShare": "19040.614",
                                        "thousandthCost": "11.567"
                                    },
                                    {
                                        "id": 6,
                                        "apartmentNumber": "7",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "3",
                                        "owner": "Scarano Elisa",
                                        "thousandthShare": "18016.964",
                                        "thousandthCost": "10.945"
                                    },
                                    {
                                        "id": 7,
                                        "apartmentNumber": "2",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "1",
                                        "owner": "Rocca Gianluca",
                                        "thousandthShare": "19673.486",
                                        "thousandthCost": "11.952"
                                    }
                                ]
                            },
                            {
                                "oid": "e02d0461-30d4-4a2e-9a01-b5d107f0205b",
                                "label": "Giardino",
                                "thousandthCostTotal": 4.54,
                                "thosandthTotalShare": 139160.02,
                                "content": [
                                    {
                                        "id": 1,
                                        "apartmentNumber": "1",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "1",
                                        "owner": "Faggiano Antonio",
                                        "thousandthShare": "18643.310",
                                        "thousandthCost": "0.608"
                                    },
                                    {
                                        "id": 2,
                                        "apartmentNumber": "3",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "2",
                                        "owner": "Rossi Mario",
                                        "thousandthShare": "18774.437",
                                        "thousandthCost": "0.613"
                                    },
                                    {
                                        "id": 3,
                                        "apartmentNumber": "4",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "2",
                                        "owner": "Esposito Maria",
                                        "thousandthShare": "18933.823",
                                        "thousandthCost": "0.618"
                                    },
                                    {
                                        "id": 35,
                                        "apartmentNumber": "8",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "3",
                                        "owner": "Monti Marcella",
                                        "thousandthShare": "7220.689",
                                        "thousandthCost": "0.236"
                                    },
                                    {
                                        "id": 4,
                                        "apartmentNumber": "5",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "2",
                                        "owner": "Selva Giordano",
                                        "thousandthShare": "18856.697",
                                        "thousandthCost": "0.615"
                                    },
                                    {
                                        "id": 5,
                                        "apartmentNumber": "6",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "3",
                                        "owner": "Alfani Lazzari",
                                        "thousandthShare": "19040.614",
                                        "thousandthCost": "0.621"
                                    },
                                    {
                                        "id": 6,
                                        "apartmentNumber": "7",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "3",
                                        "owner": "Scarano Elisa",
                                        "thousandthShare": "18016.964",
                                        "thousandthCost": "0.588"
                                    },
                                    {
                                        "id": 7,
                                        "apartmentNumber": "2",
                                        "building": "default",
                                        "stair": "1",
                                        "floor": "1",
                                        "owner": "Rocca Gianluca",
                                        "thousandthShare": "19673.486",
                                        "thousandthCost": "0.642"
                                    }
                                ]
                            }
                        ]
                    }

                };

                if (cb && typeof cb === "function") cb(data);
                return {
                    $promise: $q(function (resolve, reject) {
                        resolve(data);
                    })
                }
            }
        };
    }])
    .service('CondominiumReportPrints', ['$resource', 'contextAddressReportServer', '$q', function ($resource, contextAddressReportServer, $q) {
        this.resource = $resource(
            contextAddressReportServer + "v1/reports/:id",
            {
                id: "@id"
            },
            {
                print: {method: 'POST'}
            }
        );
        this.mockup = {

            get: function (params, cb) {
                var data = {
                    "number": 0,
                    "size": 10,
                    "totalPages": 1,
                    "totalElements": 2,
                    "content": [
                        {
                            "id": 2,
                            "totalHeatAmount": 2945.56,
                            "totalWaterAmount": 4563.34,
                            "totalOtherCosts": 1656.16,
                            "reportingDateFrom": "2016-09-02",
                            "reportingDateTo": "2017-09-01",
                            "otherCostsLabels": "Manutenzione, Guardia",
                            "reportStatus": "READY"
                        },
                        {
                            "id": 3,
                            "totalHeatAmount": 2945.56,
                            "totalWaterAmount": 4563.34,
                            "totalOtherCosts": 1656.16,
                            "reportingDateFrom": "2016-09-02",
                            "reportingDateTo": "2017-09-01",
                            "otherCostsLabels": "Guardia",
                            "reportStatus": "PENDING"
                        }
                    ]
                };

                if (cb && typeof cb === "function") cb(data);
                return {
                    $promise: $q(function (resolve, reject) {
                        resolve(data);
                    })
                }
            }
        }
    }]);


