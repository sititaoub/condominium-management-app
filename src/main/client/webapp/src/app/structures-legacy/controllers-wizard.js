
"use strict";

angular.module("condominiumManagementApp.legacyStructure").controller("structure.WizardController", [
    "$scope",
    "$state",
    "$timeout",
    "$rootScope",
    "CondominiumCrudResource",
    "CondominiumResource",
    "LookupService",
    "TableService",
    "DTOptionsBuilder",
    "DTColumnBuilder",
    "toaster",
    "$stateParams",
    "$filter",
    'uiGmapGoogleMapApi',
    'uiGmapIsReady',
    'UtilsService',
    "$uibModal",
    function ($scope, $state, $timeout, $rootScope,
              CondominiumCrudResource, CondominiumResource, LookupService, TableService,
              DTOptionsBuilder, DTColumnBuilder, toaster, $stateParams, $filter, uiGmapGoogleMapApi, uiGmapIsReady, UtilsService, $uibModal) {

        $scope.condominiumId = $stateParams.id;
        if ($scope.condominiumId != 'new') {
            $scope.labelCondominium = $filter('translate')('condominium.edit.label');
        } else {
            $scope.labelCondominium = $filter('translate')('condominium.insert.label');
        }

        $scope.fromUrl = $rootScope.previousState;

        $scope.wizardActiveStep = 1;
        $scope.district = {selected: ""};
        $scope.district_tech = {selected: ""};
        $scope.district_manager = {selected: ""};
        $scope.oldQuery = "";

        $scope.setPositionModal = function () {
            $uibModal.open({
                templateUrl: 'app/structures-legacy/partials/set-position.html',
                controller: 'structure.setPositionController',
                size: 'lg',
                scope: $scope
            });

        }

        $scope.formData = {
            technicians: []
        };

        $scope.moveToStep = function (stepId) {

            $state.transitionTo('index.wizard.step' + stepId, {id: $scope.condominiumId});
            $scope.wizardActiveStep = stepId;
        };


        $scope.onSelectDistrictCallback = function (item, model) {

            $scope.formData.address.province = item.province;
            $scope.formData.address.district = item.name;


        }

        $scope.onSelectDistrictCallback_Tech = function (item, model) {

            $scope.new_technician.address.province = item.province;
            $scope.new_technician.address.district = item.name;


        }

        $scope.onSelectDistrictCallback_Manager = function (item, model) {
            $scope.formData.manager.address.province = item.province;
            $scope.formData.manager.address.district = item.name;
        }


        $scope.funcAsync = function (query) {

            /*La ricerca su 8000 comuni è molto lenta di conseguenza queste casistiche servono a ridurre
             al minimo il numero di chiamate
             */
            var strQuery = query.substr(0, 3);
            var strOldQuery = $scope.oldQuery.substr(0, 3);

            if (((strQuery != strOldQuery) && (query.length > 2))) {
                LookupService.getDistricts(query).then(function (data) {

                    $scope.districtsLookup = data.data;
                });
            }

            if (query.length <= 3) {
                $scope.districtsLookup = [];
            }

            $scope.oldQuery = query;
        };

        $scope.backToCondominium = function () {
            $state.transitionTo('index.structure-list');
        };

        $scope.map = {
            control: {},
            showMap: true,
            zoom: 13,
            options: {
                draggable: false,
                clickableIcons: false,
                disableDefaultUI: true,
                disableDoubleClickZoom: true,
                draggableCursor: false,
                gestureHandling: "none",
                keyboardShortcuts: false,
                scrollwheel: false,
                backgroundColor: "red"

            },
            refresh: function () {
                $scope.map.showMap = false;
                $timeout(function () {
                    $scope.map.showMap = true;
                }, 500);
            },
            markersEvents: {
                click: function (marker, eventName, model) {
                    $scope.map.window.model = model;
                    $scope.map.window.show = true;
                }
            },
            window: {
                marker: {},
                show: false,
                closeClick: function () {
                    this.show = false;
                },
                options: {} // define when map is ready
            },
            center: {
                latitude: null,
                longitude: null
            }
        };

        $scope.setMapPosition = function (map, lat, lng, draggable) {
            map.center.latitude = lat;
            map.center.longitude = lng;
            if (map.markers != null && map.markers[0].control.getGMarkers != null && map.markers[0].control.getGMarkers() != null) {
                map.markers[0].control.getGMarkers()[0].setPosition({"lat": lat, "lng": lng})
            }
            if (map.markers == null) {
                map.markers = [{
                    id: "first",
                    location: {
                        latitude: lat,
                        longitude: lng
                    },
                    options: {
                        draggable: (draggable == null || !draggable) ? false : true,
                        clickable: false
                    },
                    control: {}
                }];
            } else {
                map.markers[0].location.latitude = lat;
                map.markers[0].location.longitude = lng;
            }
            $scope.map.refresh();
        };

        uiGmapIsReady.promise()
            .then(function (map_instances) {
                angular.forEach(map_instances, function (mapInstance) {
                    $scope.map.refresh();
                });
            });


        //Se è un Edit
        if ($stateParams.id != 'new') {

            $scope.formData = CondominiumResource.get({id: $scope.condominiumId}, function (data) {

                $scope.district.selected = {
                    'name': data.address.district,
                    'province': data.address.province
                };

                $scope.district_manager.selected = {
                    'name': data.manager.address.district,
                    'province': data.manager.address.province
                };

                uiGmapGoogleMapApi.then(function (maps) {
                    maps.visualRefresh = true;
                    if (data.address.lat != null && data.address.lng != null) {
                        $scope.setMapPosition($scope.map, data.address.lat, data.address.lng);

                    } else {
                        UtilsService.geocoder(data.address, function (latLng) {
                            $scope.setMapPosition($scope.map, latLng.lat, latLng.lng);
                        }, function (error) {
                            var tmp = {
                                district: ($scope.formData.address.district != null ? $scope.formData.address.district : null),
                                streetName: "",
                                town: ($scope.formData.address.town != null ? $scope.formData.address.town : null),
                                buildingNumber: "",
                                postalCode: ""
                            };
                            UtilsService.geocoder(tmp, function (latLng) {// (caso A.1)
                                $scope.setMapPosition($scope.map, latLng.lat, latLng.lng, true);
                            }, function (error) {// (caso A.3)
                                $scope.setMapPosition($scope.map, 44.494193, 11.346717, true);
                            });
                        });
                    }
                });

            });

        } else { //Se è un nuovo inserimento
            $scope.formData = {
                manager: {
                    contacts: {}
                },
                technicians: [],
                ownerPrincipal: $rootScope.user.username,
                meteringProfile: 'GOLD',
                address: {
                    "country": "IT"
                }
            };

        }


        $scope.new_technician = {
            contacts: {},
            address: {
                "country": "IT"
            }
        };

        $scope.new_tech = {
            companyName: ''
        };


        TableService.getTableOptions($scope);


        $scope.remove_technician = function (idx) {

            $scope.formData.technicians.splice(idx, 1);

        };

        $scope.submitWizardForm1 = function (wizard_form1) {
            if (wizard_form1.$valid) {
                if ($scope.formData.address.lat == null) {
                    UtilsService.geocoder($scope.formData.address, function (latLng) {
                        $scope.setMapPosition($scope.map, latLng.lat, latLng.lng);
                    }, function (error) {
                        var tmp = {
                            district: ($scope.formData.address.district != null ? $scope.formData.address.district : null),
                            streetName: "",
                            town: ($scope.formData.address.town != null ? $scope.formData.address.town : null),
                            buildingNumber: "",
                            postalCode: ""
                        };
                        UtilsService.geocoder(tmp, function (latLng) {// (caso A.1)
                            $scope.setMapPosition($scope.map, latLng.lat, latLng.lng, true);
                        }, function (error) {// (caso A.3)
                            $scope.setMapPosition($scope.map, 44.494193, 11.346717, true);
                        });
                    });
                }
                $scope.wizardActiveStep = 2;
            } else {
                wizard_form1.submitted = true;
            }
        }

        $scope.$watch('wizardActiveStep', function () {
            $state.transitionTo('index.wizard.step' + $scope.wizardActiveStep, $stateParams);
        });

        $scope.submitWizardForm2 = function (wizard_form2) {
            if (wizard_form2.$valid) {
                $scope.wizardActiveStep = 3;
            } else {
                wizard_form2.submitted = true;
            }
        }

        $scope.submitWizardForm3 = function () {
            $scope.wizardActiveStep = 4;
        }

        $scope.addTechnicianSubmit = function (add_tech_form) {
            if (add_tech_form.$valid) {

                $scope.formData.technicians.push(angular.copy($scope.new_technician));

                $scope.new_technician = {
                    contacts: {},
                    address: {
                        "country": "IT"
                    }
                };
                $scope.district_tech = {selected: ""};

                add_tech_form.$setUntouched();
                add_tech_form.$setPristine();

            } else {
                add_tech_form.submitted = true;
            }
        }


        //Salvataggio dei dati del condominio
        $scope.saveCondominium = function () {

            if ($scope.condominiumId == 'new') {

                $scope.newCondominium = new CondominiumCrudResource($scope.formData);

                CondominiumCrudResource.save($scope.newCondominium,
                    function () {
                        toaster.pop({
                            type: 'success',
                            title: 'Salvataggio effettuato',
                            showCloseButton: true,
                            timeout: 2000
                        });

                        $scope.backToCondominium();

                    },
                    function (response) {
                        var textError = 'Si è vericato un problema durante il salvataggio';
                        if (response.status == "409") {
                            textError = $filter('translate')(response.data.userMessage);
                        }
                        toaster.pop({
                            type: 'error',
                            title: textError,
                            showCloseButton: true,
                            timeout: 10000
                        });
                    });
            } else {
                $scope.formData.$update({id: $scope.condominiumId},
                    function () {
                        toaster.pop({
                            type: 'success',
                            title: 'Salvataggio effettuato',
                            showCloseButton: true,
                            timeout: 2000
                        });

                        $scope.backToCondominium();
                    },
                    function (response) {
                        var textError = 'Si è vericato un problema durante il salvataggio';
                        if (response.status == "409") {
                            textError = $filter('translate')(response.data.userMessage);
                        }
                        toaster.pop({
                            type: 'error',
                            title: textError,
                            showCloseButton: true,
                            timeout: 10000
                        });
                    });
            }


        };

    }

]).controller('structure.setPositionController', ["$scope", "$timeout", "$rootScope", '$uibModalInstance', 'uiGmapGoogleMapApi', 'uiGmapIsReady', 'UtilsService',
    function ($scope, $timeout, $rootScope, $uibModalInstance, uiGmapGoogleMapApi, uiGmapIsReady, UtilsService) {
        $scope.close = function () {
            $uibModalInstance.close();
            // passo le coordinate della mappa del popup alla pagina principale
            $scope.setMapPosition($scope.map, $scope.mapPopup.markers[0].location.latitude, $scope.mapPopup.markers[0].location.longitude);
            $scope.formData.address.lat = $scope.mapPopup.markers[0].location.latitude;
            $scope.formData.address.lng = $scope.mapPopup.markers[0].location.longitude;
        }

        $scope.centerToUserPosition = function () {
            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    $scope.setMapPosition($scope.mapPopup, position.coords.latitude, position.coords.longitude, true);

                }, function () {
                    $scope.setMapPosition($scope.mapPopup, 44.494193, 11.346717, true);
                });
            } else {
                $scope.setMapPosition($scope.mapPopup, 44.494193, 11.346717, true);
            }

        }

        $scope.gmapAddress = null;

        $scope.centerToUserAddress = function () {
            UtilsService.gmapGeocoder($scope.gmapAddress, function (position) {
                $scope.setMapPosition($scope.mapPopup, position.lat, position.lng, true);
            });
        }

        $scope.mapPopup = {
            control: {},
            showMap: true,
            zoom: 16,
            options: {},
            refresh: function () {
                $scope.mapPopup.showMap = false;
                $timeout(function () {
                    $scope.mapPopup.showMap = true;
                }, 500);
            },
            markersEvents: {
                click: function (marker, eventName, model) {
                    $scope.mapPopup.window.model = model;
                    $scope.mapPopup.window.show = true;
                }
            },
            window: {
                marker: {},
                show: false,
                closeClick: function () {
                    this.show = false;
                },
                options: {} // define when map is ready
            },
            center: {
                latitude: null,
                longitude: null
            }
        };

        uiGmapGoogleMapApi.then(function (maps) {
            // passo le coordinte della mappa della pagina principale alla mappa del popup
            if ($scope.map.markers == null) {
                // inserimento nuovo condominio
                // se l'utente ha digitato l'indirizzo, calcolo le coordinate dell'indirizzo (caso A)
                if ($scope.formData.address.district != null || $scope.formData.address.town != null) {
                    // calcolo coordinate indirizzo
                    UtilsService.geocoder($scope.formData.address, function (latLng) { // (caso A.2)
                        $scope.setMapPosition($scope.mapPopup, latLng.lat, latLng.lng, true);
                    }, function (error) {
                        var tmp = {
                            district: ($scope.formData.address.district != null ? $scope.formData.address.district : null),
                            streetName: "",
                            town: ($scope.formData.address.town != null ? $scope.formData.address.town : null),
                            buildingNumber: "",
                            postalCode: ""
                        };
                        UtilsService.geocoder(tmp, function (latLng) {// (caso A.1)
                            $scope.setMapPosition($scope.mapPopup, latLng.lat, latLng.lng, true);
                        }, function (error) {// (caso A.3)
                            $scope.setMapPosition($scope.mapPopup, 44.494193, 11.346717, true);
                        });
                    });
                } else { // altrimenti coordinate utente (CASO B)
                    // Try HTML5 geolocation.
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (position) {
                            $scope.setMapPosition($scope.mapPopup, position.coords.latitude, position.coords.longitude, true);

                        }, function () {
                            $scope.setMapPosition($scope.mapPopup, 44.494193, 11.346717, true);
                        });
                    } else {
                        $scope.setMapPosition($scope.mapPopup, 44.494193, 11.346717, true);
                    }
                }
            } else {
                $scope.setMapPosition($scope.mapPopup, $scope.map.markers[0].location.latitude, $scope.map.markers[0].location.longitude, true);
            }
        });

        uiGmapIsReady.promise()
            .then(function (map_instances) {
                angular.forEach(map_instances, function (mapInstance) {
                    $scope.mapPopup.refresh();
                });
            });


    }

]).filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

