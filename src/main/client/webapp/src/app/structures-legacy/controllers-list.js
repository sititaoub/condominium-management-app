"use strict";

angular.module("condominiumManagementApp.legacyStructure").controller("structure.ListCtrl", [
    "$scope",
    "$rootScope",
    "$state",
    "$timeout",
    "DTOptionsBuilder",
    "DTColumnBuilder",
    "CondominiumCrudResource",
    "TableService",
    "CondominiumSelection",
    "toaster",
    "UtilsService",
    function ($scope, $rootScope, $state, $timeout,
        DTOptionsBuilder, DTColumnBuilder, CondominiumCrudResource,
        TableService, CondominiumSelection, toaster, UtilsService) {

        $scope.loading = true;
        init();

        $scope.condominiumSelection = function (id) {
            CondominiumSelection.loadCondominium(id);
            $state.transitionTo("index.dashboard", ({ condominiumId: id }));
        };

        //////////////////////////////////

        function init() {

            TableService.getTableOptions($scope);

            /**
             * persons - Data used in Tables view for Data Tables plugin
             */
            /* attenzione dovrebbero vedersi solo quelli configured
             lascio in questo modo per semplificare gestione e demo*/
            $scope.condominium = CondominiumCrudResource.query({ page: "0", size: "500" }, function (data) {
                //Callback
                $scope.loading = false;
                console.log($scope.condominium);
            }, function (data) {
                $scope.loading = false;
                UtilsService.showHttpError(data.data, toaster);
            });
        }


    }


]).controller("condominiums-crud.WizardController", [
    "$scope",
    "$state",
    "$timeout",
    "CondominiumCrudResource",
    "DTOptionsBuilder",
    "DTColumnBuilder",
    "toaster",
    function ($scope, $state, $timeout, CondominiumCrudResource, DTOptionsBuilder, DTColumnBuilder, toaster) {


        $scope.formData = {
            technicians: {}
        };

        $scope.new_technician = {};

        $scope.new_tech = {
            company_name: ''
        };

        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDOM('<"html5buttons"B>lTfgitp')
            .withButtons([]).withPaginationType('simple')
            .withDisplayLength(5);

        /**
         * persons - Data used in Tables view for Data Tables plugin
         */
        $scope.condominium = CondominiumCrudResource.get(function () {
            //Callback
            console.log($scope.condominium);
        });

        $scope.addTechnician = function () {

            var arr = {};
            var name = $scope.new_tech.company_name;
            var val = $scope.new_technician;
            arr[name] = val;

            angular.extend($scope.formData.technicians, arr);

            $scope.new_tech.company_name = '';
            $scope.new_technician = {};

        };

        $scope.remove_technician = function (key) {

            delete $scope.formData.technicians[key];

        };

        $scope.submitWizardForm = function () {
            if ($scope.wizard_form.$valid) {

                console.log($scope.formData);

                $scope.newCondominium = new CondominiumCrudResource($scope.formData);


                CondominiumCrudResource.save($scope.newCondominium, function () {
                    toaster.pop({
                        type: 'success',
                        title: 'Salvataggio effettuato',
                        showCloseButton: true,
                        timeout: 2000
                    });
                });


            } else {
                $scope.wizard_form.submitted = true;
            }
        };

    }


]);
