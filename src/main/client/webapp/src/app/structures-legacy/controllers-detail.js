
"use strict";

angular.module("condominiumManagementApp.legacyStructure")
	.controller("structure.DetailCtrl", [
    "$scope",
    "$state",
    "$timeout",
    '$stateParams',
    "DTOptionsBuilder",
    "DTColumnBuilder",
    "CondominiumResource",
    "CondominiumStatusService",
    "SweetAlert",
    function ($scope, $state, $timeout,$stateParams,DTOptionsBuilder,DTColumnBuilder,CondominiumResource,CondominiumStatusService,SweetAlert) {





        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDOM('<"html5buttons"B>lTfgitp')
            .withButtons([
                {
                    extend: "excelHtml5",
                    filename:  "Data_Analysis",
                    title:"Data Analysis Report",
                    exportOptions: {
                        columns: ':visible'
                    },
                    //CharSet: "utf8",
                    exportData: { decodeEntities: true }
                },
                {
                    extend: "csvHtml5",
                    fileName:  "Data_Analysis",

                    exportOptions: {
                        columns: ':visible'
                    },
                    exportData: {decodeEntities:true}
                },
                {
                    extend: "pdfHtml5",
                    fileName:  "Data_Analysis",
                    title:"Data Analysis Report",
                    exportOptions: {
                        columns: ':visible'
                    },
                    exportData: {decodeEntities:true}
                },{
                    extend: 'print',
                    //text: 'Print current page',
                    autoPrint: false,
                    exportOptions: {
                        columns: ':visible'
                    }
                }
            ]);


        $scope.condominiumId = $stateParams.condominiumId;
        $scope.condominium_data = CondominiumResource.get({id: $scope.condominiumId}, function (data) {

        });

    }


]);

