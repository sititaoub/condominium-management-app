'use strict';
(function () {
    'use strict';

    angular
        .module("condominiumManagementApp.legacyStructure")
        .config(function ($stateProvider) {
            var modal_instance = null;
            $stateProvider.state('index.structure-list', {
                url: "/structure-list",
                templateUrl: "app/structures-legacy/partials/list.html",
                controller: "structure.ListCtrl",
                data: {
                    skipFinancialPeriodCheck: true,
                    requiresAuthentication: true,
                    permissions: {
                        only: 'ROLE_INSTALLER_INSTALLATION'
                    },
                    navigationHide: true
                },
                ncyBreadcrumb: {
                    label: 'Struttura - Lista Condomini'
                }
            }).state('index.wizard', {
                abstract: true,
                url: "/wizard/{id}",
                templateUrl: "app/structures-legacy/partials/wizard.html",
                controller: "structure.WizardController",
                data: {
                    skipFinancialPeriodCheck: true,
                    navigationHide: true
                }
            }).state('index.wizard.step1', {
                url: "/condominiums-wizard-step1",
                templateUrl: "app/structures-legacy/partials/wizard-step1.html",
                data: {
                    requiresAuthentication: true,
                    permissions: {
                        only: ['ROLE_INSTALLER_CONFIG', 'ROLE_INSTALLER_INSTALLATION', 'ROLE_INSTALLER_VALIDATION', 'ROLE_INSTALLER_ACTIVE']
                    }

                }
            }).state('index.wizard.step2', {
                url: "/condominiums-wizard-step2",
                templateUrl: "app/structures-legacy/partials/wizard-step2.html",
                data: {
                    requiresAuthentication: true,
                    permissions: {
                        only: ['ROLE_INSTALLER_CONFIG', 'ROLE_INSTALLER_INSTALLATION', 'ROLE_INSTALLER_VALIDATION', 'ROLE_INSTALLER_ACTIVE']
                    }
                }
            }).state('index.wizard.step3', {
                url: "/condominiums-wizard-step3",
                templateUrl: "app/structures-legacy/partials/wizard-step3.html",
                data: {
                    requiresAuthentication: true,
                    permissions: {
                        only: ['ROLE_INSTALLER_CONFIG', 'ROLE_INSTALLER_INSTALLATION', 'ROLE_INSTALLER_VALIDATION', 'ROLE_INSTALLER_ACTIVE']
                    }
                }
            }).state('index.wizard.step4', {
                url: "/condominiums-wizard-step4",
                templateUrl: "app/structures-legacy/partials/wizard-step4.html",
                data: {
                    requiresAuthentication: true,
                    permissions: {
                        only: ['ROLE_INSTALLER_CONFIG', 'ROLE_INSTALLER_INSTALLATION', 'ROLE_INSTALLER_VALIDATION', 'ROLE_INSTALLER_ACTIVE']
                    }
                }
            }).state('index.structure', {
                //abstract: true,
                //params: {id: null}, //{description: ""},
                url: "/structure/{condominiumId}",
                templateUrl: "app/structures-legacy/partials/structure-housing-units.html",
                controller: "structure.StructureCtrl",
                data: {
                    requiresAuthentication: true,
                    skipFinancialPeriodCheck: true,
                    permissions: {
                        only: 'ROLE_INSTALLER_INSTALLATION'
                    }
                }
            }).state('index.structure.root', {
                url: "/condominium",
                //params: {description: ""},
                templateUrl: "app/structures-legacy/partials/condominium-hu-actions.html",
                controller: ['$scope', function ($scope) {
                    $scope.$emit("selectCondominium", {});
                }],
                data: {
                    requiresAuthentication: true,
                    permissions: {
                        only: 'ROLE_INSTALLER_INSTALLATION'
                    }
                }
            }).state('index.structure.root.detail', {
                url: "/condominiums/{condominiumId}",
                templateUrl: "app/structures-legacy/partials/detail.html",
                controller: "structure.DetailCtrl",
                data: {
                    requiresAuthentication: true,
                    permissions: {
                        only: 'ROLE_INSTALLER_INSTALLATION'
                    }
                }
            }).state('index.structure.housing-unit-group', {
                url: "/housing-unit-groups/{huGroupId}",
                params: {description: ""},
                templateUrl: "app/housing_units/partials/housing-unit-group.html",
                controller: "housing_units.HousingUnitGroupCtrl",
                data: {
                    requiresAuthentication: true,
                    permissions: {
                        only: 'ROLE_INSTALLER_INSTALLATION'
                    }
                }
            }).state('index.structure.housing-unit', {
                url: "/housing-unit-groups/{huGroupId}/housing-units/{huId}",
                params: {description: ""},
                templateUrl: "app/housing_units/partials/housing-unit.html",
                controller: "housing_units.HousingUnitController",
                data: {
                    requiresAuthentication: true,
                    permissions: {
                        only: 'ROLE_INSTALLER_INSTALLATION'
                    }
                }
            }).state('index.structure.room', {
                url: "/housing-units/{huId}/rooms/{roomId}",
                params: {description: ""},
                templateUrl: "app/rooms/partials/room.html",
                controller: "rooms.RoomController",
                data: {
                    requiresAuthentication: true,
                    permissions: {
                        only: 'ROLE_INSTALLER_INSTALLATION'
                    }
                }
            }).state('index.structure.cost-allocator', {
                url: "/housing-units/{huId}/rooms/{roomId}/cost-allocators/{costAllocatorId}",
                params: {description: ""},
                templateUrl: "app/devices/partials/cost-allocator.html",
                controller: "devices.CostAllocatorController",
                data: {
                    requiresAuthentication: true,
                    permissions: {
                        only: 'ROLE_INSTALLER_INSTALLATION'
                    }
                }
            }).state('index.structure.meter', {
                url: "/housing-units/{huId}/meter/{meterId}",
                params: {description: ""},
                templateUrl: "app/devices/partials/meter.html",
                controller: "devices.MeterController",
                data: {
                    requiresAuthentication: true,
                    permissions: {
                        only: 'ROLE_INSTALLER_INSTALLATION'
                    }
                }
            }).state('index.structure.water-meter', {
                url: "/housing-units/{huId}/water-meter/{watermeterId}",
                params: {description: ""},
                templateUrl: "app/devices/partials/water-meter.html",
                controller: "devices.WaterMeterController",
                data: {
                    requiresAuthentication: true,
                    permissions: {
                        only: 'ROLE_INSTALLER_INSTALLATION'
                    }
                }
            }).state('index.structure.role', {
                url: "/housing-units/{huId}/role/{roleId}",
                params: {description: ""},
                templateUrl: "app/persons/partials/role.html",
                controller: "person.RoleCtrl",
                data: {
                    requiresAuthentication: true,
                    permissions: {
                        only: 'ROLE_INSTALLER_INSTALLATION'
                    }
                }
            })
            /*
             https://github.com/angular-ui/ui-router/issues/944
             */
                .state('index.structure.persons', {
                    template: '',
                    controller: [
                        '$uibModal', '$state', '$scope', '$rootScope', function ($uibModal, $state, $scope, $rootScope) {
                            // init scope variables that you want the modal and child states to inherit
                            var previousState = $rootScope.previousState
                            var previousStateParams = $rootScope.previousStateParams
                            // launch the dialog
                            modal_instance = $uibModal.open({
                                templateUrl: "app/persons/partials/modal-person.html",
                                controller: 'person.ModalCtrl',
                                scope: $scope,
                                size: 'lg'
                            });

                            // make sure we leave the state when the dialog closes
                            return modal_instance.result['finally'](function () {
                                modal_instance = null;
                                if ($state.includes('index.structure.persons')) {
                                    return $state.go(previousState, previousStateParams);
                                }
                            });
                        }
                    ],
                    onExit: function () {
                        // make sure the dialog is closed when we leave the state
                        if (modal_instance != null) {
                            modal_instance.close();
                        }
                    }
                }).state('index.structure.persons.list', {
                views: {
                    // note: doing `modal@launch_modal` won't work!
                    'modal@': {
                        templateUrl: "app/persons/partials/list-content.html",
                        controller: "person.ListCtrl",
                    }
                }
            }).state('index.structure.persons.form', {
                params: {
                    personId: null
                },
                views: {
                    // note: doing `modal@launch_modal` won't work!
                    'modal@': {
                        templateUrl: "app/persons/partials/person-content.html",
                        controller: "person.FormCtrl",
                    }
                }
            }).state('index.condominium-report', {
                url: "/condominium-report/{condominiumId}",
                controller: 'condominiums.reportCtrl',
                templateUrl: "app/structures-legacy/partials/report.html",
                data: {
                    skipFinancialPeriodCheck: true
                }

            }).state('index.condominium-report-prints', {
                url: "/condominium-report/{condominiumId}/prints",
                controller: 'condominiums.reportPrintsCtrl',
                templateUrl: "app/structures-legacy/partials/report-prints.html",
                data: {
                    skipFinancialPeriodCheck: true
                }

            });
        });
})();