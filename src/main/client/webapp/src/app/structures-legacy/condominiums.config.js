(function () {
    'use strict';

    angular
		.module("condominiumManagementApp.legacyStructure")
        .config( function ($translatePartialLoaderProvider) {
            $translatePartialLoaderProvider.addPart("structures-legacy");
        });

})();