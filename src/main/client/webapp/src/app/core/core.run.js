/*
 * core.run.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.core')
        .run(routeChangeErrors);

    routeChangeErrors.$inject = ['$state', 'NotifierService'];

    /* @ngInject */
    function routeChangeErrors($state, NotifierService) {
        var previousErrorHandler = $state.defaultErrorHandler();
        $state.defaultErrorHandler(function (error) {
            if (angular.isDefined(error) &&
                angular.isDefined(error.detail) &&
                angular.isDefined(error.detail.status) &&
                (error.detail.status === 404 || error.detail.status === 500)) {

                NotifierService.notifyError();
            }
            previousErrorHandler(error);
        });
    }

})();