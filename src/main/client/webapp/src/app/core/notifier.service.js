/*
 * notifier.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.core')
        .factory('NotifierService', NotifierService);

    NotifierService.$inject = ['$translate', '$q', 'toaster'];

    /* @ngInject */
    function NotifierService($translate, $q, toaster) {
        var service = {
            notifyError: notifyError,
            notifySuccess: notifySuccess,
			notifyWarning: notifyWarning
        };
        return service;

        ////////////////

		        /**
         * Notify an warning with supplied message.
         *
         * @param {string} [defaultMessage='Core.Warning.Loading'] - The message to be use (will be internationalized)
         * @returns {Promise<toaster>} - The promise that will be resolved when toaster id shown
         */
        function notifyWarning(defaultMessage) {
            var deferred = $q.defer();
            var messageCode = defaultMessage || 'Core.Warning.Completed';
            $translate(messageCode).then(function (msg) {
                toaster.pop({
                    type: 'warning',
                    title: msg,
                    onShowCallback: deferred.resolve
                });
            });
            return deferred.promise;
        }
		
        /**
         * Notify an success with supplied message.
         *
         * @param {string} [defaultMessage='Core.Success.Completed'] - The message to be use (will be internationalized)
         * @returns {Promise<toaster>} - The promise that will be resolved when toaster id shown
         */
        function notifySuccess(defaultMessage) {
            var deferred = $q.defer();
            var messageCode = defaultMessage || 'Core.Success.Loading';
            $translate(messageCode).then(function (msg) {
                toaster.pop({
                    type: 'success',
                    title: msg,
                    onShowCallback: deferred.resolve
                });
            });
            return deferred.promise;
        }


        /**
         * Notify an error with supplied message.
         *
         * @param {string} [defaultMessage='Core.Failure.Loading'] - The message to be use (will be internationalized)
         * @returns {Promise<toaster>} - The promise that will be resolved when toaster id shown
         */
        function notifyServiceError(defaultMessage) {
            var deferred = $q.defer();
            var messageCode = defaultMessage || 'Core.Failure.Loading';
            $translate(messageCode).then(function (msg) {
                toaster.pop({
                    type: 'error',
                    title: msg,
                    onShowCallback: deferred.resolve
                });
            });
            return deferred.promise;
        }


        /**
         * Notification of an error,
         * it uses the error code returned from the server 'errorObject.data.errorCode',
         * otherwise the default code 'defaultMessageCode'
         * @param {String} defaultMessageCode - default message code
         * @param {Object} errorObject - error object returned from the server
         * @returns {Promise<toaster>} - The promise that will be resolved when toaster id shown
         */
        function notifyError(defaultMessageCode, errorObject) {
            //if errorObject is null or undefined I use defaultMessageCode
            if (angular.isUndefined(errorObject) || errorObject == null) {
                return notifyServiceError(defaultMessageCode);
            }

            //otherwise I try to translate server's message code, if failed use defaultMessageCode
            var messageCode = errorObject.data.errorCode;
            $translate(messageCode).then(function (msg) {
                return notifyServiceError(messageCode);
            }, function () {
                return notifyServiceError(defaultMessageCode);
            });
        }
    }

})();

