/*
 * core.route.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.core')
        .config(setupRoutes);

        setupRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

        /* @ngInject */
        function setupRoutes($stateProvider, $urlRouterProvider) {
            $stateProvider.state('index', {
                abstract: true,
                url: "/index?companyId?lfOrg",
                template: '<div id="wrapper"><div class="gray-bg"><ui-view></ui-view></div></div>'
            }).state('logout', {
            });

            // See Know Issues on https://github.com/Narzerus/angular-permission/wiki/Installation-guide-for-ui-router
            $urlRouterProvider.otherwise(function ($injector) {
                var $state = $injector.get("$state");
                $state.go('index.condominiums.list');
            });
        }
})();
