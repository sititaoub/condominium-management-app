/*
 * core.constants.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
/* global _ ajaxURL isStandalone authenticatedUser portletId portletAppContextPath companyId */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.core')
        .constant('_', _)
        .constant('moment', moment)
        .constant('googleMapsApiKey', 'AIzaSyAFxw7w_Wu2PgjejZJ4ER68-n1ruGR1CB8')
        .constant('contextAddress', '/condominium-management-app/installer-server/')
        .constant('contextAddressTemplates', '/condominium-management-app/templates-server/')
        .constant('contextAddressStructure', '/condominium-management-app/condominium-structure-server/')
        .constant('contextAddressPeople', '/condominium-management-app/condominium-people-server/')
        .constant('contextAddressPeopleUpload', '/condominium-management-app/zuul/condominium-people-server/')
        .constant('contextAddressMeetings', '/condominium-management-app/condominium-meetings-server/')
        .constant('contextAddressManagement', '/condominium-management-app/condominium-management-server/')
        .constant('contextAddressAggregatori', '/condominium-management-app/aggregatori-chartofaccounts-server/')
        .constant('contextAddressInvoices', '/condominium-management-app/aggregatori-invoices-server/')
        .constant('contextAddressProfile', '/condominium-management-app/profile/')
        .constant('contextAddressFile', '/condominium-management-app/file-manager-server/')
        .constant('contextAddressReportServer', '/condominium-management-app/management-report-server/')
        .provider('resourceUrl', resourceUrlProvider)
        .provider('uiTinymceConfig', tinymceConfigProvider);

    resourceUrlProvider.$inject = [];

    /* @ngInject */
    function resourceUrlProvider() {
        return {
            $get: function () {
                var baseURL = "";
                //if (!isStandalone) {
                //    baseURL = portletAppContextPath + 'p/' + portletId + '/';
                //}
                return baseURL;
            }
        };
    }

    tinymceConfigProvider.$inject = ['resourceUrlProvider'];

    /* @ngInject */
    function tinymceConfigProvider(resourceUrlProvider) {
        return {
            $get: function () {
                return {
                    baseUrl: resourceUrlProvider.$get() + 'scripts/tinymce'
                };
            }
        }
    }
})();
