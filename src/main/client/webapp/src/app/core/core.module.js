/*
 * core.module.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.core', [
            /* Core Angular modules. */
            'ngAnimate',
            'ngCookies',
            'ngTouch',
            'ngSanitize',
            'ngResource',
            'ngLocale',

            /* Third party modules. */
            'ngDropzone',
            'ngStorage',
            'ngTable',
            'ngScrollbars',
            'ngHolder',
            'ngFileUpload',

            'ui.router',
            'ui.bootstrap',
            'ui.bootstrap.datetimepicker',
            'ui.tree',
            'ui.validate',
            'ui.select',
            'ui.grid',
            'ui.grid.selection',
            'ui.grid.resizeColumns',
            'ui.tinymce',

            'angular.filter',
            'angular-loading-bar',
            'angular-cache',
            'datatables',
            'datatables.buttons',
            'oitozero.ngSweetAlert',
            'toaster',
            'pascalprecht.translate',
            'angularMoment',
            'frapontillo.bootstrap-switch',
            'localytics.directives',
            'xeditable',
            'permission',
            'permission.ui',
            'jcs-autoValidate',
            'chart.js',
            'restangular',
            'ng-currency',
            'mgo-angular-wizard',
            'checklist-model',
            'angular-progress-button-styles',
            'cgBusy'
        ]);

})();
