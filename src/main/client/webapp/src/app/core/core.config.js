/*
 * core.config.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.core')
        .config(configCore);

        configCore.$inject = ['$translateProvider', '$translatePartialLoaderProvider', '$logProvider', 'moment'];

        /* @ngInject */
        function configCore($translateProvider, $translatePartialLoaderProvider, $logProvider, moment) {
            $translateProvider.useLoader("$translatePartialLoader", {
                urlTemplate: "app/{part}/i18n/{lang}.json"
            });
            $translatePartialLoaderProvider.addPart('core');

            var lang = navigator.language || navigator.userLanguage;
            lang = lang.substring(0, 2).toLowerCase();

            $translateProvider.use(lang);
            $translateProvider.forceAsyncReload(false);
            $translateProvider.useSanitizeValueStrategy('escape');
            $translateProvider.useLoaderCache('$templateCache');
            $translateProvider.addInterpolation('$translateMessageFormatInterpolation');

            $logProvider.debugEnabled(true);

            moment.locale(lang);
        }

})();
