/*
 * auto-validator.config.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.core')
        .run(configureValidator);

    configureValidator.$inject = ['validator', 'bootstrap3ElementModifier', 'defaultErrorMessageResolver', '$q', '$http', '$templateCache'];

        /* @ngInject */
        function configureValidator(validator, bootstrap3ElementModifier, defaultErrorMessageResolver, $q, $http, $templateCache) {
            validator.setValidElementStyling(false);
            bootstrap3ElementModifier.enableValidationStateIcons(false);
            /*defaultErrorMessageResolver.setCulture('it', function (culture) {
                var path = 'app/core/i18n/auto-validate_{0}.json'.format(culture.toLowerCase());
                var json = $templateCache.get(path);
                if (angular.isUndefined(json)) {
                    return $http.get(path);
                }
                return $q.resolve({ data: json });
            });*/
            defaultErrorMessageResolver.setCulture('it', function (culture) {
                return $http.get('assets/json/auto-validate_{0}.json'.format(culture.toLowerCase()))
            });
        }

})();