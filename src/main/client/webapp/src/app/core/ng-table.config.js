/*
 * ng-table.run.js
 *
 * (C) 2016-2016 Cedac Software S.r.l.
 */
(function () {
  'use strict';

  angular
    .module('condominiumManagementApp.core')
    .run(extendsNgTableParams);

  extendsNgTableParams.$inject = ['NgTableParams', '_'];

  /* @ngInject */
  function extendsNgTableParams(NgTableParams, _) {
    NgTableParams.fromUiStateParams = function (state, defaults, settings, prefix) {
      var stateToKeep = {};

      if (angular.isDefined(prefix)) {
        // Transform state params in order to extract only those with prefix and hide others
        stateToKeep = _.omitBy(state, function (value, key) {
          return _.startsWith(key, prefix);
        });
        state = _.mapKeys(_.pickBy(state, function (value, key) {
          return _.startsWith(key, prefix);
        }), function (value, key) {
          return key.substring(prefix.length);
        });
      }

      var params = _.merge({}, defaults, state);

      Object.keys(params).forEach(function (key) {
        if (key !== 'page' && key !== 'count' && key !== 'sort' && key !== '#' && !_.startsWith(key, '$') && angular.isDefined(params[key])) {
          params['filter[' + key + ']'] = params[key];
          delete params[key];
        }
      });

      if (params.sort) {
        if (!angular.isArray(params.sort)) {
          params.sort = [params.sort];
        }
        params.sort.forEach(function (sort) {
          var parts = sort.split(',');
          params['sorting[' + parts[0] + ']'] = parts[1];
        });
        delete params.sort
      }
      params = new NgTableParams(params, settings);
      params.__stateToKeep = stateToKeep;
      return params;
    };

    NgTableParams.prototype.uiRouterUrl = function (prefix) {
      var params = this.url() || {};

      Object.keys(params).forEach(function (key) {
        var match = key.match(/^sorting\[([^\]]+)]$/);
        if (match) {
          params.sort = params.sort || [];
          params.sort.push(match[1] + "," + params[key]);
          delete params[key];
        } else {
          match = key.match(/^filter\[([^\]]+)]$/);
          if (match && params[key] !== 'null') {
            params[match[1]] = params[key];
            delete params[key];
          }
        }
      });

      if (angular.isDefined(prefix)) {
        // Prefix all the parameters with provided prefix.
        params = _.mapKeys(params, function (value, key) {
          return prefix + key;
        });
      }

      params = _.merge(params, this.__stateToKeep);

      return params;
    };
  }

})();
