/*
 * alert.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.core')
        .factory('AlertService', AlertService);

    AlertService.$inject = ['$q', '$translate', 'SweetAlert'];

    /* @ngInject */
    function AlertService($q, $translate, SweetAlert) {
        var service = {
            askConfirm: askConfirm,
            showAlert: showAlert,
            askDiscardUnsaved: askDiscardUnsaved
        };
        return service;

        ////////////////

        /**
         * Ask confirm for an operation, returning a promise that it's resolved if operation is confirmed and rejected
         * if it's cancelled.
         *
         * @param {string} message - The message to show (to be internationalized)
         * @param {Object} [params = {}] - The object used to perform the internationalization
         * @param {string} [title = 'Core.Alert.AreYouSure'] - The alert title (to be internationalized)
         * @param {string} [confirm = 'Core.Alert.Confirm'] - The confirm button text (to be internationalized)
         * @param {string} [cancel = 'Core.Alert.Cancel'] - The cancel button text (to be internationalized)
         * @returns {Promise<Object>} - The promise result.
         */
        function askConfirm(message, params, title, confirm, cancel) {
            var deferred = $q.defer();

            params = params || {};
            title = title || 'Core.Alert.AreYouSure';
            confirm = confirm || 'Core.Alert.Confirm';
            cancel = cancel || 'Core.Alert.Cancel';

            $translate([title, message, confirm, cancel], params).then(function (txs) {
                SweetAlert.swal({
                    title: txs[title],
                    text: txs[message],
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: txs[confirm],
                    cancelButtonText: txs[cancel]
                }, function (confirmed) {
                    if (confirmed) {
                        deferred.resolve();
                    } else {
                        deferred.reject();
                    }
                });
            });
            return deferred.promise;
        }

        /**
         * Show an alert for an operation, returning a promise that it's resolved if operation is confirmed.
         *
         * @param {string} message - The message to show (to be internationalized)
         * @param {Object} [params = {}] - The object used to perform the internationalization
         * @param {string} [title = 'Core.Alert.Warning'] - The alert title (to be internationalized)
         * @param {string} [confirm = 'Core.Alert.Close'] - The confirm button text (to be internationalized)
         * @returns {Promise<Object>} - The promise result.
         */
        function showAlert(message, params, title, confirm) {
            var deferred = $q.defer();

            params = params || {};
            title = title || 'Core.Alert.Warning';
            confirm = confirm || 'Core.Alert.Close';

            $translate([title, message, confirm], params).then(function (txs) {
                SweetAlert.swal({
                    title: txs[title],
                    text: txs[message],
                    type: 'warning',
                    confirmButtonText: txs[confirm],
                }, function () {
                    deferred.resolve();
                });
            });
            return deferred.promise;
        }

        /**
         * Ask confirm for unsaved data discard.
         *
         * @returns {Promise<Object>} - The promise result.
         */
        function askDiscardUnsaved() {
            return askConfirm('Core.Alert.UnsavedConfirm', undefined);
        }
    }

})();
