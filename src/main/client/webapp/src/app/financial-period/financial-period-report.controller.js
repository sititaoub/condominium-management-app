/*
 * financial-period-print.controller.js
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';
    angular.module('condominiumManagementApp.financialPeriod').controller('FinancialPeriodReportCtrl', FinancialPeriodReportCtrl);
    FinancialPeriodReportCtrl.$inject = ['$state', 'FinancialPeriodService', 'NgTableParams', '$transition$', 'moment', 'AlertService', '$filter', 'NotifierService'];
    function FinancialPeriodReportCtrl($state, FinancialPeriodService, NgTableParams, $transition$, moment, AlertService, $filter, NotifierService) {
        var vm = this;

        vm.loading = true;

        vm.downloadReport = downloadReport;
        vm.deleteReport = deleteReport;
        vm.reload = reload;
        vm.toggleFilters = toggleFilters;

        activate();

        ////////////////

        function activate() {
            vm.showFilters = false;
            FinancialPeriodService.getReportList($transition$.params().condominiumId, $transition$.params().periodId).then(function (data) {
                /*angular.forEach(data.content, function (value) {
                    //value.balanceRequestDate = $filter('date')(value.balanceRequestDate, 'dd/MM/yyyy', 'UTC');
                    //value.financialPeriodDateStart = $filter('date')(value.financialPeriodDateStart, 'dd/MM/yyyy', 'UTC');
                    //value.financialPeriodDateEnd = $filter('date')(value.financialPeriodDateEnd, 'dd/MM/yyyy', 'UTC');
                    //value.reportRequestDate = $filter('date')(value.reportRequestDate, 'dd/MM/yyyy HH:mm', 'UTC');
                    //value.reportFinishDate = $filter('date')(value.reportFinishDate, 'dd/MM/yyyy HH:mm', 'UTC');
                });*/
                if( angular.isDefined(data.content["0"]) ) {
                    vm.initialFilter = data.content["0"].financialPeriodName;
                    vm.tableParams = new NgTableParams(
                        {
                            filter: { financialPeriodName: vm.initialFilter }
                        },
                        {
                            dataset: data.content
                        }
                    );
                }
            }).catch(function () {
                return NotifierService.notifyError();
            }).finally(function () {
                vm.loading = false;
            });
        }

        /**
         * Perform a download of report PDF
         * @param {Number} id - The Id of report to download
         */
        function downloadReport(id) {
            FinancialPeriodService.downloadReport(id);
        }

        /**
         * Delete a report PDF
         * @param {Number} id - The id ofthe report to delete 
         */
        function deleteReport(id) {
            AlertService.askConfirm('Eliminare il report?').then(function () {
                FinancialPeriodService.deleteReport(id, $transition$.params().condominiumId, $transition$.params().periodId).then(function () {
                    $state.reload();
                }).catch(function () {
                    return NotifierService.notifyError();
                });
            });
        }

        /**
         * Reload page
         */
        function reload() {
            $state.reload();
        }

        /**
         * Toggle the show/hide status of filters.
         */
        function toggleFilters() {
            vm.showFilters = !vm.showFilters;
        }
    }
})();