/*
 * financial-period.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.financialPeriod')
        .controller('FinancialPeriodController', FinancialPeriodController);

    FinancialPeriodController.$inject = ['$state', 'FinancialPeriodService', 'NgTableParams', 'AlertService', 'NotifierService'];

    /* @ngInject */
    function FinancialPeriodController($state, FinancialPeriodService, NgTableParams, AlertService, NotifierService) {
        var vm = this;

        vm.closePeriod = closePeriod;
        vm.consolidatePeriod = consolidatePeriod;
        vm.reopenPeriod = reopenPeriod;
        vm.removePeriod = removePeriod;

        activate();

        ////////////////

        function activate() {
            vm.loading = true;
            vm.firstLoad = true;
            vm.selectedPeriod = FinancialPeriodService.getCurrent($state.params.companyId, $state.params.condominiumId);

            vm.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10
            }, {
                getData: loadFinancialPeriods
            });
        }

        /**
         * Close the supplied period: logical service operation.
         *
         * @param {FinancialPeriod~FinancialPeriod} period - The period to select.
         */
        function closePeriod(period) {
            AlertService.askConfirm('FinancialPeriod.Close.Message', period).then(function () {
                FinancialPeriodService.close($state.params.condominiumId, period.id).then(function () {
                    return NotifierService.notifySuccess('FinancialPeriod.Close.Success');
                }).catch(function (err) {
                    return NotifierService.notifyError('FinancialPeriod.Close.Failure',err);
                }).finally(function () {
                    vm.tableParams.reload();
                });
            });
        }

        /**
         * Consolidate the supplied period: logical service operation.
         *
         * @param {FinancialPeriod~FinancialPeriod} period - The period to select.
         */
        function consolidatePeriod(period) {
            AlertService.askConfirm('FinancialPeriod.Consolidate.Message', period).then(function () {
                FinancialPeriodService.consolidate($state.params.condominiumId, period.id).then(function () {
                    return NotifierService.notifySuccess('FinancialPeriod.Consolidate.Success');
                }).catch(function (err) {
                    return NotifierService.notifyError('FinancialPeriod.Consolidate.Failure',err);
                }).finally(function () {
                    vm.tableParams.reload();
                });
            });
        }

        /**
         * Re-open the supplied period: logical service operation.
         *
         * @param {FinancialPeriod~FinancialPeriod} period - The period to select.
         */
        function reopenPeriod(period) {
            AlertService.askConfirm('FinancialPeriod.Reopen.Message', period).then(function () {
                FinancialPeriodService.reopen($state.params.condominiumId, period.id).then(function () {
                    return NotifierService.notifySuccess('FinancialPeriod.Reopen.Success');
                }).catch(function (err) {
                    return NotifierService.notifyError('FinancialPeriod.Reopen.Failure',err);
                }).finally(function () {
                    vm.tableParams.reload();
                });
            });
        }

        /**
         * Remove the supplied period.
         *
         * @param {FinancialPeriod~FinancialPeriod} period - The period to select.
         */
        function removePeriod(period) {
            AlertService.askConfirm('FinancialPeriod.Remove.Message', period).then(function () {
                FinancialPeriodService.remove($state.params.condominiumId, period.id).then(function () {
                    return NotifierService.notifySuccess('FinancialPeriod.Remove.Success');
                }).catch(function (err) {
                    return NotifierService.notifyError('FinancialPeriod.Remove.Failure',err);
                }).finally(function () {
                    vm.tableParams.reload();
                });
            });
        }

        function loadFinancialPeriods(params) {
            if (!vm.firstLoad) {
                $state.go('.', params.uiRouterUrl(), { notify: false, inherit: false });
            }
            return FinancialPeriodService.getPage($state.params.companyId, params.page() - 1, params.count(), params.sorting(), params.filter()).then(function (page) {
                params.total(page.totalElements);
                vm.firstLoad = false;
                return page.content;
            }).catch(function () {
                return NotifierService.notifyError();
            }).finally(function () {
                vm.loading = false;
            });
        }
    }

})();

