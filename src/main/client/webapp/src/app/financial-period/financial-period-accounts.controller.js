/*
 * financial-period-accounts.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.financialPeriod')
        .controller('FinancialPeriodAccountsController', FinancialPeriodAccountsController);

    FinancialPeriodAccountsController.$inject = ['$uibModalInstance', '$transition$', 'rootAccounts', 'FinancialPeriodService', 'NotifierService', '_'];

    /* @ngInject */
    function FinancialPeriodAccountsController($uibModalInstance, $transition$, rootAccounts, FinancialPeriodService, NotifierService, _) {
        var vm = this;

        vm.doSave = doSave;

        activate();

        ////////////////

        function activate() {
            vm.loading = true;
            vm.rootAccounts = rootAccounts;
            loadFinancialPeriod();
        }

        function loadFinancialPeriod() {
            FinancialPeriodService.getOne($transition$.params().companyId, $transition$.params().periodId).then(function (period) {
                vm.period = period;
                _.forEach(rootAccounts, function (el, key) {
                    var f = _.filter(period.bindMaterializedPaths, function (path) {
                        return _.startsWith(path.path, el);
                    });
                    vm[key] = _.map(f, 'path');
                });
                vm.loading = false;
            }).catch(function (err) {
                NotifierService.notifyError('FinancialPeriod.Accounts.LoadingFailure',err).then(function () {
                    $uibModalInstance.dismiss();
                });
            })
        }

        function doSave() {
            vm.period.bindMaterializedPaths = [];
            _.forEach(rootAccounts, function (el, key) {
                vm.period.bindMaterializedPaths = _.concat(vm.period.bindMaterializedPaths, vm[key]);
            });

            FinancialPeriodService.update($transition$.params().companyId, $transition$.params().periodId, vm.period).then(function () {
                return NotifierService.notifySuccess('FinancialPeriod.Accounts.UpdateSuccess');
            }).then(function () {
                $uibModalInstance.close();
            }).catch(function (err) {
                return NotifierService.notifyError('FinancialPeriod.Accounts.UpdateFailure',err);
            });
        }
    }

})();

