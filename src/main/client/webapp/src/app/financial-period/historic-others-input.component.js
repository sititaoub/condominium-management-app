/*
 * historic-others-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp')
        .component('historicOthersInput', {
            controller: HistoricOthersInputController,
            bindings: {
                condominiumId: '<',
                ngModel: '<',
                others: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/financial-period/historic-others-input.html'
        });

    HistoricOthersInputController.$inject = ['NgTableParams'];

    /* @ngInject */
    function HistoricOthersInputController(NgTableParams) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.$onChanges = onChanges;
        $ctrl.sumBy = _.sumBy;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue || [];
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function loadOthers(others) {
            angular.forEach(others, function (value) {
                value.competenceDate = new Date(value.competenceDate);
                value.datePickerOpened = false;
            });
            $ctrl.tableParams = new NgTableParams(
                {},
                {
                    dataset: others
                }
            );
        }

        function onChanges(obj) {
            if (obj.others.currentValue != undefined) {
                loadOthers(obj.others.currentValue);
            }
        }
    }

})();

