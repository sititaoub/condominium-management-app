/*
 * financial-period.route.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.financialPeriod')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider'];

    /* @ngInject */
    function setupRoutes ($stateProvider) {
        $stateProvider.state('index.financial-period', {
            url: '/condominiums/{condominiumId}/financial-period',
            abstract: true,
            template: '<ui-view></ui-view>',
            data: {
                skipFinancialPeriodCheck: true
            }
        }).state('index.financial-period.historic', {
            url: '/historic',
            component: 'financialPeriodHistoric',
            resolve: {
                condominiumId: ['$transition$', function ($transition$) {
                    return $transition$.params().condominiumId;
                }],
                companyId: ['$transition$', function ($transition$) {
                    return $transition$.params().companyId;
                }]
            },
            onEnter: ['$state', '$transition$', '$uibModal','FinancialPeriodService','NotifierService', function ($state, $transition$, $uibModal,FinancialPeriodService,NotifierService) {
                FinancialPeriodService.findFirst($transition$.params().companyId,{"types":["ORDINARY"]}).then(function(data){
                    return true;
                }).catch(function (err) {
                    NotifierService.notifyError('FinancialPeriod.Historic.Error.OrdinaryNotFound',err);
                    return $state.go('index.financial-period.list', {}, { reload: true });
                });
            }]
        }).state('index.financial-period.select', {
            url: '/select?page&count',
            templateUrl: 'app/financial-period/financial-period-selector.html',
            controller: 'FinancialPeriodSelectorController',
            controllerAs: 'vm',
            params: {
                toState: 'index.financial-period.list',
                toParams: {}
            }
        }).state('index.financial-period.list', {
            url: "?page&count",
            templateUrl: "app/financial-period/financial-period.html",
            controller: "FinancialPeriodController",
            controllerAs: 'vm'
        }).state('index.financial-period.list.add', {
            url: "/add-new?makeCurrent",
            onEnter: ['$state', '$transition$', '$uibModal', function ($state, $transition$, $uibModal) {
                return $uibModal.open({
                    templateUrl: 'app/financial-period/financial-period-create.html',
                    controller: 'FinancialPeriodCreateController',
                    controllerAs: 'vm',
                    resolve: {
                        '$transition$': function () {
                            return $transition$;
                        }
                    }
                }).result.finally(function () {
                    return $state.target('index.financial-period.list', {}, { reload: true });
                });
            }]
        }).state('index.financial-period.list.print', {
            url: '/{periodId}/print',
            onEnter: ['$state', '$transition$', '$uibModal', function ($state, $transition$, $uibModal) {
                return $uibModal.open({
                    templateUrl: 'app/financial-period/financial-period-print.html',
                    controller: 'FinancialPeriodPrintCtrl',
                    controllerAs: 'vm',
                    resolve: {
                        '$transition$': function () {
                            return $transition$;
                        }
                    }
                }).result.finally(function () {
                    return $state.target('index.financial-period.list', {}, { reload: true });
                });
            }]
        }).state('index.financial-period.report', {
            url: '/{periodId}/report',
            templateUrl: 'app/financial-period/financial-period-report.html',
            controller: 'FinancialPeriodReportCtrl',
            controllerAs: 'vm',
            data: {
                skipFinancialPeriodCheck: false
            },
            onEnter: ['$state', '$transition$', 'FinancialPeriodService', function ($state, $transition$, FinancialPeriodService) {
                // Ensure that if 'current' is provided as financial period, the current financial period is provided.
                if ($transition$.params().periodId === 'current') {
                    var params = angular.copy($transition$.params());
                    params.periodId = FinancialPeriodService.getCurrent($transition$.params().companyId, $transition$.params().condominiumId).id;
                    return $state.target($transition$.targetState().name(), params);
                }
                return true;
            }]
        }).state('index.financial-period.list.edit', {
            url: '/{periodId}/edit',
            onEnter: ['$state', '$transition$', '$uibModal', function ($state, $transition$, $uibModal) {
                return $uibModal.open({
                    templateUrl: 'app/financial-period/financial-period-edit.html',
                    controller: 'FinancialPeriodEditController',
                    controllerAs: 'vm',
                    resolve: {
                        '$transition$': function () {
                            return $transition$;
                        }
                    }
                }).result.finally(function () {
                    return $state.target('index.financial-period.list', {}, { reload: true });
                });
            }]
        }).state('index.financial-period.list.accounts', {
            url: '/{periodId}/accounts',
            onEnter: ['$state', '$transition$', '$uibModal', function ($state, $transition$, $uibModal) {
                return $uibModal.open({
                    templateUrl: 'app/financial-period/financial-period-accounts.html',
                    controller: 'FinancialPeriodAccountsController',
                    controllerAs: 'vm',
                    size: 'lg',
                    windowClass: 'financial-period-accounts-modal',
                    resolve: {
                        '$transition$': function () {
                            return $transition$;
                        }
                    }
                }).result.finally(function () {
                    return $state.target('index.financial-period-list', {}, { reload: true });
                });
            }]
        });
    }

})();
