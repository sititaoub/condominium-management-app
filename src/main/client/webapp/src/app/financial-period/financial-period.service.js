/*
 * financial-period.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.financialPeriod')
        .factory('FinancialPeriodService', FinancialPeriodService);

    FinancialPeriodService.$inject = ['$rootScope', '$sessionStorage', '$timeout', 'Restangular', 'contextAddressAggregatori', '_', 'contextAddressReportServer', '$window', 'contextAddressManagement'];

    /* @ngInject */
    function FinancialPeriodService($rootScope, $sessionStorage, $timeout, Restangular, contextAddressAggregatori, _, contextAddressReportServer, $window, contextAddressManagement) {
        var FinancialPeriods = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressAggregatori + 'v2');
            configurer.addResponseInterceptor(function (data, operation, what, url, response, deferred) {
                if (operation === "post") {
                    var location = response.headers('Location');
                    console.log("POST location", location, response);
                    return parseInt(location.substr(location.lastIndexOf('/') + 1));
                }
                return data;
            });
        });

        var Report = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressReportServer + 'v1');
        }).all('/');

        var FinancialPeriodApi = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressManagement + 'v1');
        }).all('/');

        var service = {
            CURRENT_CHANGED_EVENT: 'financialPeriod:currentChanged',

            selectCurrent: selectCurrent,
            getCurrent: getCurrent,
            setCurrent: setCurrent,
            getPage: getPage,
            getOne: getOne,
            create: create,
            update: update,
            close: close,
            consolidate: consolidate,
            reopen: reopen,
            remove: remove,
            getReportList: getReportList,
            sendReport: sendReport,
            downloadReport: downloadReport,
            deleteReport: deleteReport,
            getHistoricFinancialPeriod: getHistoricFinancialPeriod,
            createHistoric: createHistoric,
            editHistoric: editHistoric,
            findFirst: findFirst,
            findLatest: findLatest
        };
        return service;

        ////////////////

        /**
         * Financial period page.
         *
         * @typedef {Object} FinancialPeriod~FinancialPeriodPage
         * @property {number} number - The requested page number.
         * @property {number} size - The requested page size.
         * @property {number} totalElements - The total number of elements.
         * @property {number} totalPages - The total number of pages.
         * @property {FinancialPeriod~FinancialPeriod[]} content - The page content.
         */

        /**
         * Financial period.
         *
         * @typedef {Object} FinancialPeriod~FinancialPeriod
         * @property {number} id - The unique id of financial period.
         * @property {string} name - The name of financial period.
         * @property {string} description - The financial period description.
         * @property {Date} startDate - The financial period start date.
         * @property {Date} endDate - The financial period end date.
         * @property {string} status - The financial period status.
         * @property {string} type - The financial period type.
         * @property {number} ownerCompanyId - The unique id of owner company.
         * @property {number} chartOfAccountsId - The unique id of chart of accounts bound to this financial period.
         * @property {string[]} bindMaterializedPaths - The list of materialized paths bound to this financial period.
         */

        function initializeSessionStorage() {
            if (angular.isUndefined($sessionStorage.financialPeriodsByCompany)) {
                $sessionStorage.financialPeriodsByCompany = {};
                $sessionStorage.financialPeriodsByCondominium = {};
            }
        }

        /**
         * Select the current financial period for supplied company id and condominium id.
         *
         * @param {number} companyId - The unique company id.
         * @param {number} condominiumId - The unique id of condominium.
         * @returns {FinancialPeriod~FinancialPeriod|undefined} - The current financial period.
         */
        function selectCurrent(companyId, condominiumId) {
            var now = new Date().getTime();
            return getPage(companyId, 0, 100, { 'period.startDate': 'desc' }).then(function (page) {
                return _.find(_.filter(page.content, { type: 'ORDINARY', status: 'OPEN' }), function (fp) {
                    var startDate = new Date(fp.period.startDate).getTime();
                    var endDate = new Date(fp.period.endDate).getTime();
                    return startDate <= now && now <= endDate;
                });
            }).then(function (period) {
                if (angular.isDefined(period)) {
                    setCurrent(companyId, condominiumId, period);
                }
                return period;
            });
        }

        /**
         * Retrieve the current financial period by supplied company identifier (or condominium identifier).
         *
         * @param {number} companyId - The unique company id.
         * @param {number} condominiumId - The unique id of condominium.
         * @returns {FinancialPeriod~FinancialPeriod|undefined} - The current financial period.
         */
        function getCurrent(companyId, condominiumId) {
            if (angular.isUndefined($sessionStorage.financialPeriodsByCompany)) {
                return undefined;
            }
            if (angular.isDefined(companyId)) {
                return $sessionStorage.financialPeriodsByCompany[companyId];
            }
            return $sessionStorage.financialPeriodsByCondominium[condominiumId];
        }

        /**
         * Store the current financial period for supplied company identifier.
         *
         * @param {number} companyId - The unique company id.
         * @param {number} condominiumId - The unique id of condominium.
         * @param {FinancialPeriod~FinancialPeriod} period - The current financial period.
         */
        function setCurrent(companyId, condominiumId, period) {
            initializeSessionStorage();
            $sessionStorage.financialPeriodsByCompany[companyId] = period;
            $sessionStorage.financialPeriodsByCondominium[condominiumId] = period;
            $timeout(function () {
                var data = {
                    companyId: companyId,
                    condominiumId: condominiumId,
                    period: period
                };
                $rootScope.$broadcast(service.CURRENT_CHANGED_EVENT, data);
            }, 300);
        }

        /**
         * Retrieve a single page of financial periods for given company id.
         *
         * @param {number} companyId - The unique company id.
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=10] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<FinancialPeriod~FinancialPeriodPage|Error>} - The promise of page.
         */
        function getPage(companyId, page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                companyId: companyId,
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return FinancialPeriods.one("companies", companyId).all("financial-periods").get("", params);
        }

        /**
         * Retrieve a single financial period.
         *
         * @param {number} companyId - The unique company id.
         * @param {number} id - The unique id of financial period.
         * @returns {Promise<FinancialPeriod~FinancialPeriod|Error>} - The promise over the financial period.
         */
        function getOne(companyId, id) {
            return FinancialPeriods.one("companies", companyId).one("financial-periods", id).get().then(function (financialPeriod) {
                financialPeriod.period.startDate = financialPeriod.period.startDate ? new Date(financialPeriod.period.startDate) : undefined;
                financialPeriod.period.endDate = financialPeriod.period.endDate ? new Date(financialPeriod.period.endDate) : undefined;
                return financialPeriod;
            });
        }

        /**
         * Create a new financial period.
         *
         * @param {number} companyId - The unique company id
         * @param {FinancialPeriod~FinancialPeriod} period - The financial period.
         * @returns {Promise<Number|Error>} - The promise over the created financial period unique identifier.
         */
        function create(condominiumId, period) {
            //return FinancialPeriods.one("companies", companyId).all("financial-periods").post(period);
            return FinancialPeriodApi.one("financial-period-bridge").one("condominiums", condominiumId).customPOST(period);
        }

        /**
         * Updates an existing financial period.
         *
         * @param {number} companyId - The unique company id
         * @param {number} id - The unique id
         * @param {FinancialPeriod~FinancialPeriod} period - The financial period to update.
         * @returns {Promise<Object|Error>} - The promise over the create operation result.
         */
        function update(companyId, id, period) {
            return FinancialPeriods.one("companies", companyId).one("financial-periods", id).doPUT(period, undefined);
        }

        /**
         * Close an existing financial period.
         *
         * @param {number} condominiumId - The unique condominium id.
         * @param {number} id - The unique id of financial period.
         * @returns {Promise<Object|Error>} - The promise over the remove operation result.
         */
        function close(condominiumId, id) {
            return FinancialPeriodApi.one("financial-period-bridge").one("financial-periods", id).one("condominiums", condominiumId).one("close").doPUT();
        }

        /**
         * Consolidate an existing financial period.
         *
         * @param {number} companyId - The unique company id.
         * @param {number} id - The unique id of financial period.
         * @returns {Promise<Object|Error>} - The promise over the remove operation result.
         */
        function consolidate(condominiumId, id) {
            return FinancialPeriodApi.one("financial-period-bridge").one("financial-periods", id).one("condominiums", condominiumId).one("consolidate").doPUT();
        }

        /**
         * Re-open an existing financial period.
         *
         * @param {number} condominiumId - The unique condominium id.
         * @param {number} id - The unique id of financial period.
         * @returns {Promise<Object|Error>} - The promise over the remove operation result.
         */
        function reopen(condominiumId, id) {
            return FinancialPeriodApi.one("financial-period-bridge").one("financial-periods", id).one("condominiums", condominiumId).one("reopen").doPUT();
        }

        /**
         * Remove an existing financial period.
         *
         * @param {number} condominiumId - The unique condominium id.
         * @param {number} id - The unique id of financial period.
         * @returns {Promise<Object|Error>} - The promise over the remove operation result.
         */
        function remove(condominiumId, id) {
            return FinancialPeriodApi.one("financial-period-bridge").one("financial-periods", id).one("condominiums", condominiumId).remove();
        }

        /**
         * Get full list of reports
         */
        function getReportList(condominiumId, financialPeriodId) {
            var url = 'reports/balance?condominiumId=' + condominiumId + '&financialPeriodId=' + financialPeriodId;
            var params = angular.merge({}, {}, {
                size: 100,
                condominiumId: condominiumId,
                financialPeriodId: financialPeriodId
            });
            return Report.get(url, params);
        }

        /**
         * Send Report to Print
         * @param {Report~Report} report - The data to send to print
         */
        function sendReport(report) {
            var url = 'reports/balance';
            return Report.customPOST(report, url);
        }

        /**
         * Download a report
         * @param {Number} id - The id of the report to download
         */
        function downloadReport(id) {
            Report.one("reports").one("balance", id).withHttpConfig({ responseType: 'blob' }).get({}, { "X-Auth-Token": 'token' }).then(function (response) {
                var url = (window.URL || window.webkitURL).createObjectURL(response);
                window.open(url);
            });
        }

        /**
         * Delete a report
         * @param {Number} id - The id of the report to delete 
         */
        function deleteReport(id, condominiumId, financialPeriodId) {
            var url = 'reports/balance/' + id + '?condominiumId=' + condominiumId + '&financialPeriodId=' + financialPeriodId;
            return Report.customDELETE(url);
        }

        /**
         * Get historic financial period
         */
        function getHistoricFinancialPeriod(condominiumId) {
            return FinancialPeriodApi.one("condominiums", condominiumId).one("financial-period").one("historic").get();
        }

        /**
         * Create historic financial period
         */
        function createHistoric(historicData, condominiumId) {
            return FinancialPeriodApi.one("condominiums", condominiumId).one("financial-period").one("historic").customPOST(historicData);
        }

        /**
         * Edit historic financial period
         */
        function editHistoric(historicData, condominiumId) {
            return FinancialPeriodApi.one("condominiums", condominiumId).one("financial-period").one("historic").customPUT(historicData);
        }

        /**
         * Find first financial period for company ID
         * @param {number} companyId - The unique company id.
         * @param {object} [filters] - The filters.
         */
        function findFirst(companyId, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters);
            return FinancialPeriods.one("companies", companyId).all("financial-periods").all("search").all("first").get("", params);
        }

        /**
         *  Find latest financial period for company ID 
         * @param {number} companyId - The unique company id.
         * @param {object} [filters] - The filters.
         */
        function findLatest(companyId, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters);
            return FinancialPeriods.one("companies", companyId).all("financial-periods").all("search").all("latest").get("", params);
        }

    }

})();

