/*
 * financial-period-list.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.financialPeriod')
        .controller('FinancialPeriodSelectorController', FinancialPeriodSelectorController);

    FinancialPeriodSelectorController.$inject = ['$state', 'FinancialPeriodService', 'NgTableParams'];

    /* @ngInject */
    function FinancialPeriodSelectorController($state, FinancialPeriodService, NgTableParams) {
        var vm = this;

        vm.selectPeriod = selectPeriod;

        activate();

        ////////////////

        function activate() {
            vm.loading = true;
            vm.tableParams = new NgTableParams({
                page: 1,
                count: 10,
                sort: 'startDate,asc'
            }, {
                    getData: loadFinancialPeriods
                });
        }

        /**
         * Select the supplied period.
         *
         * @param {FinancialPeriod~FinancialPeriod} period - The period to select.
         */
        function selectPeriod(period) {
            FinancialPeriodService.setCurrent($state.params.companyId, $state.params.condominiumId, period);
            $state.go($state.params.toState, $state.params.toParams);
        }

        function loadFinancialPeriods(params) {
            return FinancialPeriodService.getPage($state.params.companyId, params.page() - 1, params.count(), params.sorting(), params.filter()).then(function (page) {
                params.total(page.totalElements);
                return page.content;
            }).finally(function () {
                vm.loading = false;
            });
        }
    }

})();

