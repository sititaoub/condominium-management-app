/*
 * historic-roles-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.financialPeriod')
        .component('historicRolesInput', {
            controller: HistoricRolesController,
            bindings: {
                condominiumId: '<',
                ngModel: '<',
                roles: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/financial-period/historic-roles-input.html'
        });

    HistoricRolesController.$inject = ['NgTableParams', '$filter'];

    /* @ngInject */
    function HistoricRolesController(NgTableParams, $filter) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.$onChanges = onChanges;
        $ctrl.sumBy = _.sumBy;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue || [];
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function loadRoles(roles) {
            $ctrl.tableParams = new NgTableParams(
                {
                    page: 1,
                    count: 9999,
                    group: 'personId'
                },
                {
                    counts: [],
                    groupOptions: {
                        isExpanded: false
                    }
                }
            );
            $ctrl.columns = createColumns();
            angular.forEach(roles, function (value) {
                value.competenceDate = new Date(value.competenceDate);
                value.datePickerOpened = false;
            });
            $ctrl.tableParams.settings({
                dataset: roles
            });
        }

        function onChanges(obj) {
            if (obj.roles.currentValue != undefined) {
                loadRoles(obj.roles.currentValue);
            }
        }

        function createColumns() {
            var columns = [
                {
                    field: 'person',
                    title: $filter('translate')('FinancialPeriod.Historic.Roles.Person'),
                    show: true,
                    groupable: 'person',
                    kind: 'switch',
                    headerClass: 'col-md-1'
                },
                {
                    field: 'building',
                    title: $filter('translate')('FinancialPeriod.Historic.Roles.Building'),
                    show: true,
                    kind: 'text',
                    headerClass: 'col-md-3'
                },
                {
                    field: 'stair',
                    title: $filter('translate')('FinancialPeriod.Historic.Roles.Stair'),
                    show: true,
                    kind: 'text',
                    headerClass: 'col-md-3'
                },
                {
                    field: 'housingUnit',
                    title: $filter('translate')('FinancialPeriod.Historic.Roles.HousingUnit'),
                    show: true,
                    kind: 'text',
                    headerClass: 'col-md-3'
                },
                {
                    field: 'competenceDate',
                    title: $filter('translate')('FinancialPeriod.Historic.Roles.CompetenceDate'),
                    show: true,
                    kind: 'date',
                    headerClass: 'col-md-3'
                },
                {
                    field: 'toGive',
                    title: $filter('translate')('FinancialPeriod.Historic.Roles.ToGive'),
                    show: true,
                    kind: 'amount',
                    headerClass: 'col-md-3'
                },
                {
                    field: 'toHave',
                    title: $filter('translate')('FinancialPeriod.Historic.Roles.ToHave'),
                    show: true,
                    kind: 'amount',
                    headerClass: 'col-md-3'
                }
            ];
            return columns;
        }

    }

})();

