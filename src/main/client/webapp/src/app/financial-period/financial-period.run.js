/*
 * financial-period.run.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.financialPeriod')
        .run(runStateInterceptor);

        runStateInterceptor.$inject = ['$transitions', '$state', 'FinancialPeriodService'];

        /* @ngInject */
        function runStateInterceptor($transitions, $state, FinancialPeriodService) {
            $transitions.onStart({}, function (transition) {
                var toState = transition.$to();
                var toParams = transition.params('to');
                if (angular.isUndefined(toState.data) || !toState.data.skipFinancialPeriodCheck) {
                    var currentPeriod = FinancialPeriodService.getCurrent(toParams.companyId, toParams.condominiumId);
                    if (angular.isUndefined(currentPeriod)) {
                        transition.abort();

                        /* No current financial period selected. Try to check if we can auto-select it. */
                        FinancialPeriodService.selectCurrent(toParams.companyId, toParams.condominiumId).then(function (period) {
                            if (angular.isDefined(period)) {
                                $state.go(toState, toParams);
                            } else {
                                $state.go('index.financial-period.select', {
                                    companyId: toParams.companyId,
                                    condominiumId: toParams.condominiumId,
                                    toState: toState,
                                    toParams: toParams
                                })
                            }
                        });
                    }
                }
            });
        }

})();
