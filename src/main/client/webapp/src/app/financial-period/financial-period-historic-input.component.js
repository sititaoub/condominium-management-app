/*
 * financial-period-historic-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.financialPeriod')
        .component('financialPeriodHistoricInput', {
            controller: FinancialPeriodHistoricInputController,
            bindings: {
                companyId: '<',
                condominiumId: '<',
                ngModel: '<',
                data: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/financial-period/financial-period-historic-input.html'
        });

    FinancialPeriodHistoricInputController.$inject = ['managementTypes'];

    /* @ngInject */
    function FinancialPeriodHistoricInputController(managementTypes) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.$onChanges = onChanges;
        $ctrl.startDateOpened = false;
        $ctrl.endDateOpened = false;

        ////////////

        function onInit() {
            $ctrl.managementTypes = managementTypes;
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function loadData(data) {
            $ctrl.roles = data.roleBalances;
            $ctrl.accounts = [];
            $ctrl.funds = [];
            angular.forEach(data.accountBalances, function (value) {
                if (value.typology.contains('Fund')) {
                    $ctrl.funds.push(value);
                } else {
                    $ctrl.accounts.push(value);
                }
            });
            data.startDate = new Date(data.startDate);
            data.endDate = new Date(data.endDate);
            $ctrl.value = data;
            $ctrl.others = data.otherBalances;
        }

        function onChanges(obj) {
            if (obj.data != undefined) {
                if (obj.data.currentValue != undefined) {
                    loadData(obj.data.currentValue);
                }
            }
        }
    }

})();

