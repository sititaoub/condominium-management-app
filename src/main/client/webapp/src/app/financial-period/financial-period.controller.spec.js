/*
 * financial-period.controller.spec.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
describe('FinancialPeriodController', function () {
    var $rootScope, $q, $controller, FinancialPeriodService;

    beforeEach(module('condominiumManagementApp', 'condominiumManagementApp.templates'));

    beforeEach(inject(function (_$rootScope_, _$q_, _$controller_, _FinancialPeriodService_) {
        $rootScope = _$rootScope_;
        $q = _$q_;
        $controller = _$controller_;
        FinancialPeriodService = _FinancialPeriodService_;
    }));

    describe("initialState", function () {
        var fixture, state;

        beforeEach(function () {
            state = { params: { condominiumId: 8, companyId: 390, page: 1, count: 25, sort: 'name,asc' } };
            spyOn(FinancialPeriodService, 'getCurrent').and.returnValue({ id: 1, name: 'My Period' });

            fixture = $controller('FinancialPeriodController', {
                $state: state
            });
        });

        it("should start with loading flag true", function () {
            expect(fixture.loading).toBe(true);
        });

        it("should start with firstLoad flag true", function () {
            expect(fixture.firstLoad).toBe(true);
        });

        it("should initialize current financial period", function () {
            expect(FinancialPeriodService.getCurrent).toHaveBeenCalledWith(390, 8);
            expect(fixture.selectedPeriod).toEqual({ id: 1, name: 'My Period' });
        });

        it("should initialize ngTableParams from current ui state", function () {
            expect(fixture.tableParams).toBeDefined();
            expect(fixture.tableParams.page()).toEqual(1);
            expect(fixture.tableParams.count()).toEqual(25);
            expect(fixture.tableParams.sorting()).toEqual({ name: 'asc' });
            expect(fixture.tableParams.filter()).toEqual({ condominiumId: 8, companyId: 390 });
        });
    });
});