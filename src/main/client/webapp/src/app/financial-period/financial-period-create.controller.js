/*
 * financial-period-create.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.financialPeriod')
        .controller('FinancialPeriodCreateController', FinancialPeriodCreateController);

    FinancialPeriodCreateController.$inject = ['$uibModalInstance', '$state', '$transition$', 'managementTypes', 'FinancialPeriodService', 'NotifierService'];

    /* @ngInject */
    function FinancialPeriodCreateController($uibModalInstance, $state, $transition$, managementTypes, FinancialPeriodService, NotifierService) {
        var vm = this;

        vm.doSave = doSave;
        vm.checkPeriodDate = checkPeriodDate;
        vm.onFinancialPeriodTypeChange = onFinancialPeriodTypeChange;

        activate();

        ////////////////

        function activate() {
            vm.loading = false;
            vm.startDateLock = false;
            vm.managementTypes = managementTypes;
            vm.startDateOpened = false;
            vm.endDateOpened = false;
            FinancialPeriodService.findFirst($transition$.params().companyId, { "types": ["ORDINARY"] }).then(function (data) {
                vm.firstFinancialPeriod = data;
            }).catch(function (err) {
                vm.firstFinancialPeriod = undefined;
            });
            FinancialPeriodService.findLatest($transition$.params().companyId, { "types": ["ORDINARY"] }).then(function (data) {
                vm.lastOrdinaryFinancialPeriod = data;
            }).catch(function (err) {
                vm.lastOrdinaryFinancialPeriod = undefined;
            });
            FinancialPeriodService.findLatest($transition$.params().companyId, { "types": ["HISTORICAL"] }).then(function (data) {
                vm.historicalFinancialPeriod = data;
            }).catch(function (err) {
                vm.historicalFinancialPeriod = undefined;
            });
            
            vm.financialPeriod = {
                status: 'OPEN'
            };
        }

        function doSave() {
            if(vm.financialPeriod.type === 'SPECIAL'){
                // The special financial period can not intersect the historical financial period
                if (!angular.isUndefined(vm.historicalFinancialPeriod) && 
                    new Date(vm.historicalFinancialPeriod.period.endDate) > new Date(vm.financialPeriod.period.startDate)) {
                        return NotifierService.notifyError('ERR_SPECIAL_FINANCIAL_PERIOD_CONCURRENT_HISTORICAL');
                }
                // The special financiale period can not start before first financial period 
                if (angular.isUndefined(vm.firstFinancialPeriod)|| new Date(vm.firstFinancialPeriod.period.startDate) > new Date(vm.financialPeriod.period.startDate)) {
                        return NotifierService.notifyError('ERR_SPECIAL_FINANCIAL_PERIOD_BEFORE_ORDINARY');   
                }
            }

            FinancialPeriodService.create($transition$.params().condominiumId, vm.financialPeriod).then(function () {
                return FinancialPeriodService.getPage($transition$.params().companyId, 0, 1).then(function (data) {
                    if (data.totalElements === 1) {
                        FinancialPeriodService.setCurrent($transition$.params().companyId, $transition$.params().condominiumId, data.content[0]);
                    }
                });
            }).then(function () {
                return NotifierService.notifySuccess('FinancialPeriod.Create.Success').then(function () {
                    $uibModalInstance.close();
                    $state.go('index.financial-period.list', {}, { reload: true });
                });
            }).catch(function (err) {
                return NotifierService.notifyError('FinancialPeriod.Create.Failure', err);
            });
        }

        function checkPeriodDate(valTocheck) {
            if (angular.isUndefined(valTocheck)) {
                return true;
            }
            if (angular.isUndefined(vm.financialPeriod.period) || angular.isUndefined(vm.financialPeriod.period.startDate)) {
                return true;
            } else {
                return valTocheck >= vm.financialPeriod.period.startDate;
            }
        }

        function onFinancialPeriodTypeChange() {
            // Check if financial period is ORDINARY or is First ORDINARY
            if (!angular.isUndefined(vm.lastOrdinaryFinancialPeriod) && vm.financialPeriod.type === 'ORDINARY') {
                if (angular.isUndefined(vm.financialPeriod.period))
                    vm.financialPeriod.period = {};
                // Set start date equal to the day following the end of the previous financial-period
                vm.financialPeriod.period.startDate = new Date(vm.lastOrdinaryFinancialPeriod.period.endDate);
                vm.financialPeriod.period.startDate.setDate(vm.financialPeriod.period.startDate.getDate() + 1);
                //Copy associated materialized path from previous financial period
                vm.financialPeriod.bindMaterializedPaths = vm.lastOrdinaryFinancialPeriod.bindMaterializedPaths;
                vm.startDateLock = true;
            } else {
                //if is present associated materialized path delete
                vm.financialPeriod.bindMaterializedPaths = [];
                vm.startDateLock = false;
            }
        }
    }

})();

