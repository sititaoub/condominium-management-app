/*
 * financial-period-create.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.financialPeriod')
        .controller('FinancialPeriodEditController', FinancialPeriodEditController);

    FinancialPeriodEditController.$inject = ['$uibModalInstance', '$transition$', 'managementTypes', 'FinancialPeriodService', 'NotifierService'];

    /* @ngInject */
    function FinancialPeriodEditController($uibModalInstance, $transition$, managementTypes, FinancialPeriodService, NotifierService) {
        var vm = this;

        vm.doSave = doSave;
        vm.checkPeriodDate = checkPeriodDate;

        activate();

        ////////////////

        function activate() {
            vm.loading = true;
            vm.managementTypes = managementTypes;
            vm.startDateOpened = false;
            vm.endDateOpened = false;

            FinancialPeriodService.getOne($transition$.params().companyId, $transition$.params().periodId).then(function (period) {
                vm.financialPeriod = period;
                vm.loading = false;
            });
        }

        function doSave() {
            FinancialPeriodService.update($transition$.params().companyId, $transition$.params().periodId, vm.financialPeriod).then(function () {
                return NotifierService.notifySuccess('FinancialPeriod.Edit.Success').then(function () {
                    $uibModalInstance.close();
                });
            }).catch(function (err) {
                return NotifierService.notifyError('FinancialPeriod.Edit.Failure',err);
            });
        }

        function checkPeriodDate(valTocheck) {
            if (angular.isUndefined(valTocheck)) {
                return true;
            }
            if (angular.isUndefined(vm.financialPeriod.period.startDate)) {
                return false;
            } else {
                return valTocheck >= vm.financialPeriod.period.startDate;
            }

        }
    }

})();

