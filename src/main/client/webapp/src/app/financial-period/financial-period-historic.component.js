/*
 * financial-period-historic.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.financialPeriod')
        .component('financialPeriodHistoric', {
            controller: FinancialPeriodHistoricController,
            bindings: {
                condominiumId: '<',
                companyId: '<'
            },
            templateUrl: 'app/financial-period/financial-period-historic.html'
        });

    FinancialPeriodHistoricController.$inject = ['FinancialPeriodService', 'NotifierService', '$state', 'AlertService'];

    /* @ngInject */
    function FinancialPeriodHistoricController(FinancialPeriodService, NotifierService, $state, AlertService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;
        $ctrl.consolidate = consolidate;
        $ctrl.loading = true;

        ////////////

        function onInit() {
            FinancialPeriodService.getHistoricFinancialPeriod($ctrl.condominiumId).then(function (data) {
                $ctrl.loading = false;
                $ctrl.data = data;
                $ctrl.financialPeriodId = data.financialPeriodId;
                if (data.id == undefined) {
                    $ctrl.hasId = false;
                    $ctrl.data.type = "HISTORICAL";
                    $ctrl.data.status = "OPEN";
                    $ctrl.data.condominiumId = $ctrl.condominiumId;
                } else {
                    $ctrl.hasId = true;
                }
            }).catch(function (err) {
                $ctrl.loading = false;
                NotifierService.notifyError('FinancialPeriod.Historic.Get.Error',err);
            });
        }

        function doSave() {
            if ($ctrl.hasId) {
                FinancialPeriodService.editHistoric($ctrl.data, $ctrl.condominiumId).then(function () {
                    NotifierService.notifySuccess('FinancialPeriod.Historic.Edit.Success');
                    reload();
                }).catch(function (err) {
                    NotifierService.notifyError('FinancialPeriod.Historic.Edit.Error',err);
                });
            } else {
                FinancialPeriodService.createHistoric($ctrl.data, $ctrl.condominiumId).then(function () {
                    NotifierService.notifySuccess('FinancialPeriod.Historic.Create.Success');
                    reload();
                }).catch(function (err) {
                    NotifierService.notifyError('FinancialPeriod.Historic.Create.Error',err);
                });
            }
        }

        function reload() {
            $state.reload();
        }

        function consolidate() {
            var msg = 'FinancialPeriod.Historic.Consolidate.Message';
            AlertService.askConfirm(msg).then(function () {
                FinancialPeriodService.consolidate($ctrl.companyId, $ctrl.financialPeriodId).then(function () {
                    NotifierService.notifySuccess('FinancialPeriod.Historic.Consolidate.Success');
                    $state.go('index.financial-period');
                }).catch(function () {
                    NotifierService.notifyError('FinancialPeriod.Historic.Consolidate.Error',err);
                });
            });
        }

    }

})();

