/*
 * historic-funds-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp')
        .component('historicFundsInput', {
            controller: HistoricFundsInputController,
            bindings: {
                condominiumId: '<',
                ngModel: '<',
                funds: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/financial-period/historic-funds-input.html'
        });

    HistoricFundsInputController.$inject = ['NgTableParams'];

    /* @ngInject */
    function HistoricFundsInputController(NgTableParams) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.$onChanges = onChanges;
        $ctrl.sumBy = _.sumBy;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue || [];
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function loadFunds(funds) {
            angular.forEach(funds, function (value) {
                value.competenceDate = new Date(value.competenceDate);
                value.datePickerOpened = false;
            });
            $ctrl.tableParams = new NgTableParams(
                {},
                {
                    dataset: funds
                }
            );
        }

        function onChanges(obj){
            if(obj.funds.currentValue != undefined){
                loadFunds(obj.funds.currentValue);
            }
        }
    }

})();

