/*
 * financial-period-print.controller.js
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';
    angular.module('condominiumManagementApp.financialPeriod').controller('FinancialPeriodPrintCtrl', FinancialPeriodPrintCtrl);
    FinancialPeriodPrintCtrl.$inject = ['$state', '$uibModalInstance', '$transition$', 'FinancialPeriodService', 'NotifierService', 'AlertService'];
    function FinancialPeriodPrintCtrl($state, $uibModalInstance, $transition$, FinancialPeriodService, NotifierService, AlertService) {
        var vm = this;

        vm.cancel = cancel;
        vm.print = print;

        vm.report = {};
        vm.report.withDetails = false;

        activate();

        ////////////////

        function activate() {
            vm.report.condominiumId = $transition$.params().condominiumId;
            vm.report.financialPeriodId = $transition$.params().periodId;
            FinancialPeriodService.getPage($state.params.companyId).then(function (page) {
                vm.data = page.content;
                loadModal();
            });
        }

        /**
         * Prepare modal window with data
         */
        function loadModal() {
            angular.forEach(vm.data, function (value) {
                if (value.id == vm.report.financialPeriodId) {
                    vm.report.balanceRequestDate = new Date(value.period.endDate);
                }
            });
        }

        /**
         * Close modal without make any action
         */
        function cancel() {
            $state.go('index.financial-period.list');
            $uibModalInstance.close();
        }

        /**
         * Send data
         */
        function print() {
            $uibModalInstance.close();
            FinancialPeriodService.sendReport(vm.report).then(function () {
                NotifierService.notifySuccess();
                AlertService.askConfirm('FinancialPeriod.Report.AlertMessage', {}, 'FinancialPeriod.Report.AlertTitle', 'FinancialPeriod.Report.AlertConfirm', 'FinancialPeriod.Report.AlertCancel').then(function () {
                    $state.go('index.financial-period.report',
                        {
                            periodId: vm.report.financialPeriodId,
                        },
                        {
                            reload: true
                        }
                    );
                }).catch(function () {
                    $state.go('index.financial-period.list');
                });
            }).catch(function () {
                return NotifierService.notifyError();
            });
        }
    }
})();