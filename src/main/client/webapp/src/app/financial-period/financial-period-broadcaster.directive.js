/*
 * iframe-broadcaster.directive.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.financialPeriod')
        .directive('financialPeriodBroadcaster', financialPeriodBroadcaster);

    financialPeriodBroadcaster.$inject = ['$rootScope', '$window', 'FinancialPeriodService'];

    /* @ngInject */
    function financialPeriodBroadcaster($rootScope, $window, FinancialPeriodService) {
        var directive = {
            link: link,
            restrict: 'A',
            scope: {}
        };
        return directive;

        function link(/* scope, element, attrs */) {
            if ($window.top !== $window.self) {
                $rootScope.$on(FinancialPeriodService.CURRENT_CHANGED_EVENT, function (/* event, data */) {
                    $window.parent.postMessage('com.cedac.portal.js.events.financialPeriodChanged', '*');
                });
            }
        }
    }

})();

