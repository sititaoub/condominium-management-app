/*
 * financial-period.service.spec.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
describe('FinancialPeriodService', function () {
    var $httpBackend, $sessionStorage, $rootScope, $timeout, context, FinancialPeriodService;

    beforeEach(module('condominiumManagementApp', 'condominiumManagementApp.templates'));

    beforeEach(inject(function (_$httpBackend_, _FinancialPeriodService_, _$sessionStorage_, _$timeout_, _$rootScope_,
                                _contextAddressAggregatori_) {
        $httpBackend = _$httpBackend_;
        $sessionStorage = _$sessionStorage_;
        $rootScope = _$rootScope_;
        $timeout = _$timeout_;
        context = _contextAddressAggregatori_;
        FinancialPeriodService = _FinancialPeriodService_;
    }));

    beforeEach(function () {
        $httpBackend.whenGET(/profile\/v1\/users\/me$/).respond(200, {authorities: []});
    });

    describe("getCurrent", function () {
        beforeEach(function () {
            $sessionStorage.$reset();
        });

        it("Should return undefined if current financial period does not exists", function () {
            var current = FinancialPeriodService.getCurrent(390, 8);

            expect(current).toBeUndefined();
        });

        it("Should return an object when the financial period exists by companyId", function () {
            $sessionStorage.financialPeriodsByCompany = {390: "My value"};
            $sessionStorage.financialPeriodsByCondominium = {8: "My value"};

            var current = FinancialPeriodService.getCurrent(390, undefined);

            expect(current).toEqual("My value");
        });

        it("Should return an object when the financial period exists by condominiumId", function () {
            $sessionStorage.financialPeriodsByCompany = {390: "My value"};
            $sessionStorage.financialPeriodsByCondominium = {8: "My value"};

            var current = FinancialPeriodService.getCurrent(undefined, 8);

            expect(current).toEqual("My value");
        });

        it("Should return the value by company id if both values are provided", function () {
            $sessionStorage.financialPeriodsByCompany = {390: "My value"};
            $sessionStorage.financialPeriodsByCondominium = {8: "My other value"};

            var current = FinancialPeriodService.getCurrent(390, 8);

            expect(current).toEqual("My value");
        });
    });

    describe("setCurrent", function () {
        beforeEach(function () {
            $sessionStorage.$reset();
        });

        it("Should store the value on both collections and raise an event", function (done) {
            var unsubscribe = $rootScope.$on(FinancialPeriodService.CURRENT_CHANGED_EVENT, function (event, data) {
                expect(data.companyId).toEqual(390);
                expect(data.condominiumId).toEqual(8);
                expect(data.period).toEqual("My period");
                unsubscribe();
                done();
            });

            FinancialPeriodService.setCurrent(390, 8, "My period");

            expect($sessionStorage.financialPeriodsByCompany[390]).toEqual("My period");
            expect($sessionStorage.financialPeriodsByCondominium[8]).toEqual("My period");
            $timeout.flush();
        });
    });

    describe("getPage", function () {
        afterEach(function () {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        it("should return an error when companyId is missing", function (done) {
            $httpBackend.expectGET(context + 'v2/financial-periods?page=0&size=10').respond(400);

            FinancialPeriodService.getPage().catch(done);

            $httpBackend.flush();
        });

        it("should invoke http backend with default page and size", function (done) {
            $httpBackend.expectGET(context + 'v2/financial-periods?companyId=390&page=0&size=10').respond(200, {});

            FinancialPeriodService.getPage(390).then(done);

            $httpBackend.flush();
        });

        it("should invoke http backend with specified page and size", function (done) {
            $httpBackend.expectGET(context + 'v2/financial-periods?companyId=390&page=1&size=25').respond(200, {});

            FinancialPeriodService.getPage(390, 1, 25).then(done);

            $httpBackend.flush();
        });

        it("should invoke http backend with specified sorting data", function (done) {
            $httpBackend.expectGET(context + 'v2/financial-periods?companyId=390&page=0&size=10&sort=name,asc').respond(200, {});

            FinancialPeriodService.getPage(390, undefined, undefined, { name: 'asc' }).then(done);

            $httpBackend.flush();
        });

        it("should invoke http backend with specified filter, skipping empty strings", function (done) {
            $httpBackend.expectGET(context + 'v2/financial-periods?companyId=390&page=0&size=10&name=MyP').respond(200, {});

            FinancialPeriodService.getPage(390, undefined, undefined, undefined, { name: 'MyP', description: '' }).then(done);

            $httpBackend.flush();
        });
    });

    describe('getOne', function () {
        afterEach(function () {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        it("should return a rejection when object is missing", function (done) {
            $httpBackend.expectGET(context + 'v2/financial-periods/1?companyId=390').respond(404);

            FinancialPeriodService.getOne(390, 1).catch(done);

            $httpBackend.flush();
        });

        it("should return a rejection when a company id is not provided", function (done) {
            $httpBackend.expectGET(context + 'v2/financial-periods/1').respond(400);

            FinancialPeriodService.getOne(undefined, 1).catch(done);

            $httpBackend.flush();
        });

        it("should return a resolution with undefined dates when object is found", function (done) {
            $httpBackend.expectGET(context + 'v2/financial-periods/1?companyId=390').respond(200, {
                period: {}
            });

            FinancialPeriodService.getOne(390, 1).then(function (data) {
                expect(data.period.startDate).toBeUndefined();
                expect(data.period.endDate).toBeUndefined();
                done();
            });

            $httpBackend.flush();
        });

        it("should return a resolution with valid dates when object is found", function (done) {
            $httpBackend.expectGET(context + 'v2/financial-periods/1?companyId=390').respond(200, {
                period: {
                    startDate: '2017-01-01T00:00:00Z',
                    endDate: '2017-12-31T00:00:00Z'
                }
            });

            FinancialPeriodService.getOne(390, 1).then(function (data) {
                expect(data.period.startDate).toEqual(new Date('2017-01-01T00:00:00Z'));
                expect(data.period.endDate).toEqual(new Date('2017-12-31T00:00:00Z'));
                done();
            });

            $httpBackend.flush();
        });
    });

    describe("create", function () {
        afterEach(function () {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        it("should invoke the backend with a POST and return the id of newly created object", function (done) {
            $httpBackend.expectPOST(context + 'v2/financial-periods?companyId=390', {
                name: 'MyPeriod'
            }).respond(201, undefined, {
                'Location': 'http://localhost/' + context + 'v2/financial-periods/2'
            });

            FinancialPeriodService.create(390, { name: 'MyPeriod' }).then(function (data) {
                expect(data).toEqual(2);
                done();
            });

            $httpBackend.flush();
        });

        it("should reject the promise if data is not valid", function (done) {
            $httpBackend.expectPOST(context + 'v2/financial-periods?companyId=390', {
                name: 'MyPeriod'
            }).respond(400);

            FinancialPeriodService.create(390, { name: 'MyPeriod' }).catch(done);

            $httpBackend.flush();
        });

        it("should reject the promise if companyId is invalid", function (done) {
            $httpBackend.expectPOST(context + 'v2/financial-periods', { name: 'MyPeriod' }).respond(400);

            FinancialPeriodService.create(undefined, { name: 'MyPeriod' }).catch(done);

            $httpBackend.flush();
        });
    });

    describe("update", function () {
        afterEach(function () {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        it("should invoke the backend with a PUT and resolve the promise", function (done) {
            $httpBackend.expectPUT(context + 'v2/financial-periods/2?companyId=390', {
                id: 2,
                name: 'MyPeriod'
            }).respond(204);

            FinancialPeriodService.update(390, 2, { id: 2, name: 'MyPeriod'}).then(done);

            $httpBackend.flush();
        });

        it("should invoke the backend with a PUT and reject promise", function (done) {
            $httpBackend.expectPUT(context + 'v2/financial-periods/2?companyId=390', {
                id: 2,
                name: 'MyPeriod'
            }).respond(400);

            FinancialPeriodService.update(390, 2, { id: 2, name: 'MyPeriod' }).catch(done);

            $httpBackend.flush();
        });

        it("should invoke the backend with a PUT and reject promise if companyId is invalid", function (done) {
            $httpBackend.expectPUT(context + 'v2/financial-periods/2', {
                id: 2,
                name: 'MyPeriod'
            }).respond(400);

            FinancialPeriodService.update(undefined, 2, { id: 2, name: 'MyPeriod' }).catch(done);

            $httpBackend.flush();
        });
    });

    describe("remove", function () {
        afterEach(function () {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        it("should invoke the backend with a DELETE and resolve the promise", function (done) {
            $httpBackend.expectDELETE(context + 'v2/financial-periods/2?companyId=390').respond(204);

            FinancialPeriodService.remove(390, 2).then(done);

            $httpBackend.flush();
        });

        it("should invoke the backend with a DELETE and reject promise if not found", function (done) {
            $httpBackend.expectDELETE(context + 'v2/financial-periods/2?companyId=390').respond(404);

            FinancialPeriodService.remove(390, 2).catch(done);

            $httpBackend.flush();
        });

        it("should invoke the backend with a PUT and reject promise if companyId is invalid", function (done) {
            $httpBackend.expectPUT(context + 'v2/financial-periods/2').respond(400);

            FinancialPeriodService.update(undefined, 2).catch(done);

            $httpBackend.flush();
        });
    });
});