/*
 * historic-accounts-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp')
        .component('historicAccountsInput', {
            controller: HistoricAccountsInputController,
            bindings: {
                condominiumId: '<',
                ngModel: '<',
                accounts: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/financial-period/historic-accounts-input.html'
        });

    HistoricAccountsInputController.$inject = ['NgTableParams'];

    /* @ngInject */
    function HistoricAccountsInputController(NgTableParams) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.$onChanges = onChanges;
        $ctrl.sumBy = _.sumBy;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue || [];
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function loadAccounts(accounts) {
            angular.forEach(accounts, function (value) {
                value.competenceDate = new Date(value.competenceDate);
                value.datePickerOpened = false;
            });
            $ctrl.tableParams = new NgTableParams(
                {},
                {
                    dataset: accounts
                }
            );
        }

        function onChanges(obj){
            if(obj.accounts.currentValue != undefined){
                loadAccounts(obj.accounts.currentValue);
            }
        }

    }

})();

