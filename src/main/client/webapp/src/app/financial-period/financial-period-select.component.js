/*
 * financial-period-select.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.financialPeriod')
        .component('financialPeriodSelect', {
            controller: FinancialPeriodSelectComponent,
            bindings: {
                id: '@?',
                name: '@?',
                class: '@?',
                ngModel: '<',
                ngRequired: '<?',
                ngDisabled: '<?',
                companyId: '<',
                onlyOpen: '<?'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/financial-period/financial-period-select.html'
        });

    FinancialPeriodSelectComponent.$inject = ['FinancialPeriodService'];

    /* @ngInject */
    function FinancialPeriodSelectComponent(FinancialPeriodService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
            $ctrl.financialPeriods = [];
            loadFinancialPeriods()
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function loadFinancialPeriods() {
            var filter = {};
            if ($ctrl.onlyOpen) {
                filter.status = 'OPEN';
            }
            return FinancialPeriodService.getPage($ctrl.companyId, 0, 1000, {id: 'desc'}, filter).then(function (page) {
                $ctrl.financialPeriods = page.content;
            });
        }
    }

})();

