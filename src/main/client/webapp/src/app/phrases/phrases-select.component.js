/*
 * phrases-select.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.phrases')
        .component('phrasesSelect', {
            controller: DefaultAgendaSelectController,
            bindings: {
                id: '@?',
                name: '@?',
                controlClass: '@?',
                ngModel: '<',
                ngRequired: '<?',
                ngDisabled: '<?',
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/phrases/phrases-select.html'
        });

    DefaultAgendaSelectController.$inject = ['PhrasesService'];

    /* @ngInject */
    function DefaultAgendaSelectController(PhrasesService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
            loadPhrases();
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function loadPhrases() {
            PhrasesService.getAll().then(function (phrases) {
                $ctrl.phrases = phrases;
            });
        }
    }

})();

