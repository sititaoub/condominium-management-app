/*
 * phrases-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.phrases')
        .component('phrasesList', {
            controller: PhrasesListController,
            bindings: {},
            templateUrl: 'app/phrases/phrases-list.html'
        });

    PhrasesListController.$inject = ['$state', 'NgTableParams', 'PhrasesService', 'NotifierService', 'AlertService'];

    /* @ngInject */
    function PhrasesListController($state, NgTableParams, PhrasesService, NotifierService, AlertService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.toggleFilters = toggleFilters;
        $ctrl.deletePhrase = deletePhrase;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10,
                sort: 'description,asc'
            }, {
                getData: loadPhrases
            });
            $ctrl.showFilters = $ctrl.tableParams.hasFilter();
        }

        function toggleFilters() {
            $ctrl.showFilters = !$ctrl.showFilters;
        }

        function deletePhrase(phrase) {
            AlertService.askConfirm('Phrases.Remove.Message', phrase).then(function () {
                PhrasesService.remove(phrase.id).then(function () {
                    return NotifierService.notifySuccess('Phrases.Remove.Success');
                }).catch(function () {
                    return NotifierService.notifyError('Phrases.Remove.Failure');
                }).finally(function () {
                    $ctrl.tableParams.reload();
                })
            });
        }

        function loadPhrases(params) {
            $state.go('.', params.uiRouterUrl(), { inherit: false });
            return PhrasesService.getPage(params.page() - 1, params.count(), params.sorting(), params.filter())
                .then(function (data) {
                    params.total(data.totalElements);
                    return data.content;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    $ctrl.loading = false;
                });
        }
    }

})();

