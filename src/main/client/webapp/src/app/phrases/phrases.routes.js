/*
 * phrases.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.phrases')
        .config(setupRoutes);

        setupRoutes.$inject = ['$stateProvider'];

        /* @ngInject */
        function setupRoutes ($stateProvider) {
            $stateProvider.state('index.phrases', {
                url: '/phrases',
                redirectTo: 'index.phrases.list',
                data: {
                    skipFinancialPeriodCheck: true
                },
                template: '<div class="phrases-container" ui-view></div>'
            }).state('index.phrases.list', {
                url : '?page&count&sort&description',
                component: 'phrasesList',
                params: {
                    page: { dynamic: true, squash: true, value: '1', inherits: true },
                    count: { dynamic: true, squash: true, value: '10', inherits: true },
                    sort: { dynamic: true, squash: true, value: 'description,asc', inherits: true },
                    description: { dynamic: true, inherits: true }
                }
            }).state('index.phrases.list.create', {
                url: '/create',
                views: {
                    '@index.phrases': {
                        component: 'phraseCreate'
                    }
                }
            }).state('index.phrases.list.edit', {
                url: '/{phraseId}/edit',
                views: {
                    '@index.phrases': {
                        component: 'phraseEdit'
                    }
                },
                resolve: [{
                    token: 'phrase',
                    deps: ['$transition$', 'PhrasesService'],
                    resolveFn: function ($transition$, PhraseService) {
                        return PhraseService.getOne($transition$.params().phraseId);
                    }
                }]
            });
        }

})();