/*
 * phrases.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.managers')
        .factory('PhrasesService', PhrasesService);

    PhrasesService.$inject = ['$q', 'Restangular', 'contextAddressMeetings', '_'];

    /* @ngInject */
    function PhrasesService($q, Restangular, contextAddressMeetings, _) {
        var PHRASES = "phrases";
        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressMeetings + 'v1');
            configurer.addResponseInterceptor(function (data, operation, what, url, response) {
                if (operation === "post") {
                    var location = response.headers('Location');
                    return location.substr(location.lastIndexOf('/') + 1);
                }
                return data;
            });
        });

        var service = {
            getPage: getPage,
            getAll: getAll,
            getOne: getOne,
            create: create,
            update: update,
            remove: remove
        };
        return service;

        ////////////////

        /**
         * @namespace Phrases
         */

        /**
         * Phrases page.
         *
         * @typedef {Object} Phrases~PhrasesPage
         * @property {number} number - The requested page number.
         * @property {number} size - The requested page size.
         * @property {number} totalElements - The total number of elements.
         * @property {number} totalPages - The total number of pages.
         * @property {Phrases~Phrase[]} content - The page content.
         */

        /**
         * Phrase object.
         *
         * @typedef {Object} Phrases~Phrase
         * @property {number} id - The unique id of phrase.
         * @property {string} description - The phrase description.
         */

        /**
         * Phrase detail object.
         *
         * @typedef {Object} Phrases~PhraseDetail
         * @property {number} id - The unique id of phrase.
         * @property {string} description - The phrase description.
         * @property {content} content - The phrase content.
         */

        /**
         * Retrieve a single page of phrases.
         *
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=10] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<Phrases~PhrasesPage|Error>} - The promise of page.
         */
        function getPage(page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return Api.all(PHRASES).get("", params);
        }

        /**
         * Retrieve all the phrases
         *
         * @returns {Promise<Phrases~Phrase[]|Error>} - The promise of phrases.
         */
        function getAll() {
            return getPage(0, 1000, { description: 'asc' }, undefined).then(function (page) {
                return page.content;
            });
        }

        /**
         * Retrieve a single phrase detail.
         *
         * @param {number} id - The unique manager id.
         * @returns {Promise<Phrases~PhraseDetail|Error>} - The promise of detail.
         */
        function getOne(id) {
            return Api.one(PHRASES, id).get();
        }

        /**
         * Create a new phrase.
         *
         * @param {Phrases~Phrase} phrase - The phrase to be created.
         * @returns {Promise<Number|Error>} - The promise of result.
         */
        function create(phrase) {
            return Api.all(PHRASES).post(phrase);
        }

        /**
         * Updates an existing phrase.
         *
         * @param {number} id - The unique id of phrase to update.
         * @param {Phrases~Phrase} phrase - The phrase to be updated
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function update(id, phrase) {
            return Api.one(PHRASES, id).customPUT(phrase);
        }

        /**
         * Remove the phrase with supplied id.
         *
         * @param {number} id - The unique id of phrase
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function remove(id) {
            return Api.one(PHRASES, id).remove();
        }
    }

})();

