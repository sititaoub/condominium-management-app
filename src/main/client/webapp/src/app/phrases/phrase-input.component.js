/*
 * phrase-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.phrases')
        .component('phraseInput', {
            controller: PhraseInputController,
            bindings: {
                ngModel: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/phrases/phrase-input.html'
        });

    PhraseInputController.$inject = [];

    /* @ngInject */
    function PhraseInputController() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

