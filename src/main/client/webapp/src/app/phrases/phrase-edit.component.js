/*
 * phrase-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.phrases')
        .component('phraseEdit', {
            controller: PhraseEditController,
            bindings: {
                phrase: '<'
            },
            templateUrl: 'app/phrases/phrase-edit.html'
        });

    PhraseEditController.$inject = ['$state', 'PhrasesService', 'NotifierService', 'AlertService'];

    /* @ngInject */
    function PhraseEditController($state, PhrasesService, NotifierService, AlertService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.uiCanExit = uiCanExit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
        }

        function uiCanExit() {
            if ($ctrl.form.$dirty) {
                return AlertService.askDiscardUnsaved();
            }
            return true;
        }

        function doSave() {
            PhrasesService.update($ctrl.phrase.id, $ctrl.phrase).then(function () {
                return NotifierService.notifySuccess('Phrases.Edit.Success');
            }).then(function () {
                $ctrl.form.$setPristine();
                $state.go("^");
            }).catch(function () {
                return NotifierService.notifyError('Phrases.Edit.Failure');
            });
        }
    }

})();

