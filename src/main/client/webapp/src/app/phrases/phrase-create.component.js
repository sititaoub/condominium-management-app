/*
 * phrase-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.phrases')
        .component('phraseCreate', {
            controller: PhraseCreateController,
            bindings: {},
            templateUrl: 'app/phrases/phrase-create.html'
        });

    PhraseCreateController.$inject = ['$state', 'PhrasesService', 'NotifierService', 'AlertService'];

    /* @ngInject */
    function PhraseCreateController($state, PhrasesService, NotifierService, AlertService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.uiCanExit = uiCanExit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.phrase = undefined;
        }

        function uiCanExit() {
            if ($ctrl.form.$dirty) {
                return AlertService.askDiscardUnsaved();
            }
            return true;
        }

        function doSave() {
            PhrasesService.create($ctrl.phrase).then(function () {
                return NotifierService.notifySuccess('Phrases.Create.Success');
            }).then(function () {
                $ctrl.form.$setPristine();
                $state.go("^");
            }).catch(function () {
                return NotifierService.notifyError('Phrases.Create.Failure');
            });
        }
    }

})();

