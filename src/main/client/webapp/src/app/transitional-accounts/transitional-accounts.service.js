/*
 * transitional-accounts.service.js
 *
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.transitionalAccounts')
        .factory('TransitionalAccountsService', TransitionalAccountsService);

    TransitionalAccountsService.$inject = ['Restangular', 'contextAddressAggregatori', 'contextAddressManagement', '_', '$window'];

    /* @ngInject */
    function TransitionalAccountsService(Restangular, contextAddressAggregatori, contextAddressManagement, _, $window) {

        var AggregatoriApi = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressAggregatori + 'v2');
        });

        var ManagementApi = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressManagement + 'v1');
        });

        var service = {
            getAccountingTransactionPage: getAccountingTransactionPage,
            getTransitionalAccount: getTransitionalAccount,
            getTransactionRecords: getTransactionRecords,
            reconciliationAccount: reconciliationAccount,
            loadManualTransaction: loadManualTransaction
        };

        return service;

        /**
         * Manage generic transaction
         */
        function getAccountingTransactionPage(condominiumId, financialPeriodId, page, size, sorting, filters,checked) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                checked: checked,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return ManagementApi.one("condominiums", condominiumId)
                .one("financial-period", financialPeriodId).all("accounting-transactions").get("", params);
        }

        function getTransactionRecords(condominiumId, financialPeriodId, transactionId) {
            return ManagementApi.one("condominiums", condominiumId)
                .one("financial-period", financialPeriodId).one("accounting-transactions", transactionId).one("records").get("");
        }

        function getTransitionalAccount(condominiumId, financialPeriodId, page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return ManagementApi.one("condominiums", condominiumId)
                .one("financial-period", financialPeriodId).all("trasitional-account").get("", params);
        }

        function reconciliationAccount(condominiumId, financialPeriodId, accountReconciliation) {
            return ManagementApi.one("condominiums", condominiumId).one("financial-period", financialPeriodId).one("trasitional-account").doPUT(accountReconciliation);
        }

        function loadManualTransaction(condominiumId, financialPeriodId, transactionId) {
            return ManagementApi.one("condominiums", condominiumId).one("financial-period", financialPeriodId).one("manual-transactions",transactionId).get();
        }
    }
})();