/*
 * transitional-accounts-detail.component.js
 *
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.transitionalAccounts')
        .component('transitionalAccountsReconciliationComponent', {
            controller: TransitionalAccountsReconciliationController,
            bindings: {
                '$transition$': '<'
            },
            templateUrl: 'app/transitional-accounts/transitional-accounts-reconciliation.html'
        });

    TransitionalAccountsReconciliationController.$inject = ['$state', '$scope', '$uibModal', 'FinancialPeriodService', 'NotifierService', 'TransitionalAccountsService', '_'];

    function TransitionalAccountsReconciliationController($state, $scope, $uibModal, FinancialPeriodService, NotifierService, TransitionalAccountsService, _) {
        var $ctrl = this;
        $ctrl.$onInit = onInit;
        $ctrl.dismiss = dismiss;
        $ctrl.doSave = doSave;

        /**
         * Initialization of component
         */
        function onInit() {
            if (angular.isUndefined($ctrl.$transition$._targetState._params.accountSelected) || angular.isUndefined($ctrl.$transition$._targetState._params.accountSelected))
                dismiss();
            $ctrl.accountSelected = $ctrl.$transition$._targetState._params.accountSelected;
            $ctrl.transitionaleAccountSelected = $ctrl.$transition$._targetState._params.transitionaleAccountSelected;
            $ctrl.transitionalMaterializedPath = $ctrl.$transition$._targetState._params.transitionalMaterializedPath;
            $ctrl.unbalancedAmounts = false;
            $ctrl.periodLoading = true;
            $ctrl.periodId = $state.params.periodId;
            $ctrl.condominiumId = $state.params.condominiumId;
            $ctrl.companyId = $state.params.companyId;
            $ctrl.disableSave = true;
            loadFinancialPeriod();
            calculateMode();

            $scope.$watch('$ctrl.accountSelected', function (oldValue, newValue) {
                $ctrl.unselectionCheck = false;
                $ctrl.invalidRecords = false;
                var amount = 0;
                _.forEach($ctrl.accountSelected, function (account) {
                    var recordSelect = false;
                    _.forEach(account.records, function (record) {
                        var recordInvalid = (angular.isUndefined(record.description) || record.description === "") || (angular.isUndefined(record.date) || record.date === "") || (angular.isUndefined(record.amount) === "");
                        $ctrl.invalidRecords = $ctrl.invalidRecords || recordInvalid;
                        if (!angular.isUndefined(record.select) && record.select) {
                            recordSelect = true;
                            amount = record.amount + amount;
                        }
                    });
                    $ctrl.unselectionCheck = $ctrl.unselectionCheck || !recordSelect;
                });
                $ctrl.unbalancedAmounts = amount !== $ctrl.transitionaleAccountSelected.details.TO_HAVE[0].amount ? true : false;

                $ctrl.disableSave = $ctrl.unbalancedAmounts || $ctrl.invalidRecords || $ctrl.unselectionCheck;
            }, true)
        }

        function calculateMode() {
            var toGiveMathPath = $ctrl.transitionaleAccountSelected.details.TO_GIVE[0].materializedPath;
            var toHaveMathPath = $ctrl.transitionaleAccountSelected.details.TO_HAVE[0].materializedPath;

            _.forEach($ctrl.transitionalMaterializedPath, function (currentMatPath) {
                if (_.startsWith(toGiveMathPath, currentMatPath)) {
                    $ctrl.transitionalMode = 'TO_GIVE';
                    $ctrl.transactionalMode = 'TO_HAVE';
                } else if (_.startsWith(toHaveMathPath, currentMatPath)) {
                    $ctrl.transitionalMode = 'TO_HAVE';
                    $ctrl.transactionalMode = 'TO_GIVE';
                }
            });
        }

        /**
         * Function for loading financial period 
         */
        function loadFinancialPeriod() {
            FinancialPeriodService.getOne($ctrl.companyId, $ctrl.periodId).then(function (period) {
                $ctrl.period = period;
                $ctrl.periodLoading = false;
            }).catch(function (error) {
                NotifierService.notifyError('TrasitionalAccounts.LoadingError', error);
                dismiss();
            });
        }


        /**
         * Function for page dismiss
         */
        function dismiss() {
            $state.go('index.transitionalAccounts.list');
        }

        /**
         * function for saving writing 
         */
        function doSave() {
            var reconciliationAccount = {};
            reconciliationAccount.transitionalAccount = $ctrl.transitionaleAccountSelected;
            reconciliationAccount.transactions = $ctrl.accountSelected;
            reconciliationAccount.records = [];
            var withHoldingsRecord = [];
            _.forEach($ctrl.accountSelected, function (transaction) {
                _.forEach(transaction.records, function (record) {
                    if (record.id === null && _.startsWith(transaction.type, 'MAN_TX')) {
                        if (record.type === 'PAYMENT') {
                            _.forEach(transaction.records, function (currRecord) {
                                if (currRecord.id === null && currRecord.type === 'WITHHOLDING' && currRecord.addedId === record.addedId) {
                                    record.withHoldingRecord = currRecord;
                                }
                            });
                            if (!angular.isUndefined(record.select) && record.select) {
                                reconciliationAccount.records = _.concat(reconciliationAccount.records, record);
                            }
                        }
                    } else {
                        if (!angular.isUndefined(record.select) && record.select) {
                            reconciliationAccount.records = _.concat(reconciliationAccount.records, record);

                        }
                    }
                });
            });
            TransitionalAccountsService.reconciliationAccount($ctrl.condominiumId, $ctrl.periodId, reconciliationAccount)
                .then(function () {
                    dismiss();
                    return NotifierService.notifySuccess('TrasitionalAccounts.Success');
                })
                .catch(function (error) {
                    NotifierService.notifyError('TrasitionalAccounts.UpdateError', error);
                });
        }

    }
})();