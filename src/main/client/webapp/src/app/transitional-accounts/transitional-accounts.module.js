/*
 * transitional-accounts.module.js
 *
 */
(function () {
    'use strict';

    angular
    .module('condominiumManagementApp.transitionalAccounts', [
        'condominiumManagementApp.core',
        'condominiumManagementApp.commons'
    ]);

})();