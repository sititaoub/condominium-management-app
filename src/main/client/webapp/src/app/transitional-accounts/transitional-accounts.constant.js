(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.transitionalAccounts')
        .constant('transactionType', [
            'MAN_TX_GENERAL', 'MAN_TX_PERSONAL', 'MAN_TX_REVENUE', 'INSTALMENT_TX', 'INVESTMENT_FUND_TX', 'DISINVESTMENT_FUND_TX', 'USER_DEFINED_TX'
        ]);
})();