/*
 * transitional-accounts-list.component.js
 *
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.transitionalAccounts')
        .component('transitionalAccountsListComponent', {
            controller: TransitionalAccountsListController,
            bindings: {
                '$transition$': '<'
            },
            templateUrl: 'app/transitional-accounts/transitional-accounts-list.html'
        });

    TransitionalAccountsListController.$inject = ['$state', '$filter', 'NgTableParams', 'TransitionalAccountsService', 'FinancialPeriodService', 'NotifierService', '_', 'transactionType'];


    function TransitionalAccountsListController($state, $filter, NgTableParams, TransitionalAccountsService, FinancialPeriodService, NotifierService, _, transactionType) {

        var $ctrl = this;
        $ctrl.$onInit = onInit;
        $ctrl.doConfirm = doConfirm;
        $ctrl.onAccountSelection = onAccountSelection;
        $ctrl.onTransitionalSelection = onTransitionalSelection;
        $ctrl.isToGive = isToGive;
        $ctrl.toggleFilters = toggleFilters;

        function onInit() {

            $ctrl.transactionType = transactionType;
            $ctrl.loadingAccount = true;
            $ctrl.loadingTransitional = true;
            $ctrl.showFilter = true;
            initAccountsTable();
            initTransitionalAccountsTable();
            

            $ctrl.transitionaleAccountSelected = null;
            $ctrl.accountSelected = [];
            $ctrl.disableConfirm = isConfirmDisabled;
            initializeFilter();
        }


        /**
         * Function for transitional Account's table initialization
         */
        function initTransitionalAccountsTable() {
            $ctrl.transitionalAccountsTableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10,
            }, {
                    getData: loadTransitionalAccounts,
                });

            $ctrl.transitionalAccountColumns = createTransitionalAccountColumns();
        }

        /**
         * check if confirm button is disable
         */
        function isConfirmDisabled() {
            $ctrl.disableConfirm = $ctrl.transitionaleAccountSelected === null || $ctrl.accountSelected.length === 0
        }

        function doConfirm() {
            $state.go('index.transitionalAccounts.detail', { transitionaleAccountSelected: $ctrl.transitionaleAccountSelected, accountSelected: $ctrl.accountSelected, transitionalMaterializedPath:$ctrl.transitionalMaterializedPath});
        }

        /*
        * Manage selection and deselection of accounts
        */
        function onAccountSelection(row) {
            if (row.selection)
                $ctrl.accountSelected = _.concat($ctrl.accountSelected, row);
            else
                $ctrl.accountSelected = _.remove($ctrl.accountSelected, function (curr) {
                    return curr.id !== row.id;
                });
            isConfirmDisabled();
        }

        /*
        * Manage selection and deselection of trasitional account
        */
        function onTransitionalSelection(row) {
            if ($ctrl.transitionaleAccountSelected === null && row.selection) {
                $ctrl.transitionaleAccountSelected = row;
            } else if ($ctrl.transitionaleAccountSelected !== null && row.selection) {
                $ctrl.transitionaleAccountSelected.selection = false;
                $ctrl.transitionaleAccountSelected = row;
            } else if ($ctrl.transitionaleAccountSelected !== null && !row.selection) {
                $ctrl.transitionaleAccountSelected = null;
            }
            isConfirmDisabled();
        }

        /**
         * loading trasitional accounts
         * @param {*} params 
         */
        function loadTransitionalAccounts(tparams) {
            return TransitionalAccountsService.getTransitionalAccount($state.params.condominiumId, $state.params.periodId, tparams.page() - 1, tparams.count(), tparams.sorting(), tparams.filter(), tparams).then(function (transitionalPage) {
                $ctrl.transitionalMaterializedPath = transitionalPage.materializedPath;
                var page = transitionalPage.page;
                tparams.total(page.totalElements);
                if (page.totalElements < 10)
                    $ctrl.transitionalAccountsTableParams.settings().counts = []
                return page.content;
            }).catch(function () {
                return NotifierService.notifyError();
            }).finally(function () {
                $ctrl.loadingTransitional = false;
            });
        }

        /**
         * loading trasitional accounts
         * @param {*} params 
         */
        function loadAccounts(aparams) {
            return TransitionalAccountsService.getAccountingTransactionPage($state.params.condominiumId, $state.params.periodId,
                aparams.page() - 1, aparams.count(), aparams.sorting(), aparams.filter(),false).then(function (page) {
                    aparams.total(page.totalElements);
                    if (page.totalElements < 10)
                        $ctrl.accountsTableParams.settings().counts = []
                    return page.content;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    $ctrl.loadingAccount = false;
                });
        }


        /**
         * Function for Account's table initialization
         */
        function initAccountsTable() {
            $ctrl.accountsTableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10,
            }, {
                    getData: loadAccounts,
                });

            $ctrl.accountColumns = createAccountColumns();
        }

        /**
        * Funtcion that create list of columns for accoun't table
        */
        function createAccountColumns() {
            var columns = [{
                field: 'description',
                title: $filter('translate')('TrasitionalAccounts.List.Description'),
                show: true,
                groupable: 'description',
                kind: 'text',
                width: '300',
                headerClass: 'col-md-2',
                filter: { description: 'text' }
            }, {
                field: 'type',
                title: $filter('translate')('TrasitionalAccounts.List.Type'),
                show: true,
                groupable: 'type',
                kind: 'i18n-text',
                width: '300',
                headerClass: 'col-md-2',
                filter: { type: 'app/transitional-accounts/transitional-accounts-type-filter.html' }
            }, {
                field: 'totalAmount',
                title: $filter('translate')('TrasitionalAccounts.List.Amount'),
                show: true,
                groupable: 'amount',
                width: '120',
                kind: 'amount',
                headerClass: 'col-md-3',
                filter: {}
            }, {
                field: 'competence',
                title: $filter('translate')('TrasitionalAccounts.List.Competence'),
                show: true,
                groupable: 'competence',
                kind: 'competence',
                width: '120',
                headerClass: 'col-md-3',
                filter: { competence: 'app/transitional-accounts/transaction-date-filter.html' }
            }, {
                field: 'selection',
                title: $filter('translate')('TrasitionalAccounts.List.Selection'),
                show: true,
                groupable: 'selection',
                kind: 'check',
                width: '75',
                headerClass: 'col-md-1',
                filter: {}
            }];

            return columns;
        }

        function isToGive(row) {
            var toGiveMathPath = row['details'].TO_GIVE[0].materializedPath;
            var isToGive=false;
            _.forEach($ctrl.transitionalMaterializedPath,function(currMathPath){
                isToGive= isToGive || _.startsWith(toGiveMathPath, currMathPath);
            });
            return isToGive;
        }

        function createTransitionalAccountColumns() {
            var columns = [{
                field: 'description',
                title: $filter('translate')('TrasitionalAccounts.List.Description'),
                show: true,
                groupable: 'description',
                kind: 'text',
                width: '300',
                headerClass: 'col-md-2',
                filter: { description: 'text' }
            }, {
                field: 'type',
                title: $filter('translate')('TrasitionalAccounts.List.Type'),
                show: true,
                groupable: 'type',
                kind: 'i18n-text',
                width: '300',
                headerClass: 'col-md-2',
                filter: {}
            }, {
                field: 'details',
                title: $filter('translate')('TrasitionalAccounts.List.AmountToGive'),
                show: true,
                groupable: 'amountToGive',
                width: '120',
                kind: 'amountToGive',
                headerClass: 'col-md-3',
                filter: {}
            }, {
                field: 'details',
                title: $filter('translate')('TrasitionalAccounts.List.AmountToHave'),
                show: true,
                groupable: 'amountToHave',
                width: '120',
                kind: 'amountToHave',
                headerClass: 'col-md-3'
            }, {
                field: 'competence',
                title: $filter('translate')('TrasitionalAccounts.List.Competence'),
                show: true,
                groupable: 'competence',
                kind: 'competence',
                width: '120',
                filter: { recordingDate: 'app/transitional-accounts/transitional-accounts-date-filter.html' }
            }, {
                field: 'selection',
                title: $filter('translate')('TrasitionalAccounts.List.Selection'),
                show: true,
                groupable: 'selection',
                kind: 'check',
                width: '75',
                headerClass: 'col-md-1',
                filter: {}
            }];

            return columns;
        }

        function initializeFilter() {
            $ctrl.accoutingTransactionTypes = _.forEach(transactionType).map(function (val) {
                return { name: val };
            });
        }

        function toggleFilters() {
            $ctrl.showFilter = !$ctrl.showFilter;
        }
    }
})();