/*
 * transitional-accounts.config.js
 *
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.transitionalAccounts')
        .config(configI18n);

        configI18n.$inject = ['$translatePartialLoaderProvider'];

        /* @ngInject */
        function configI18n($translatePartialLoaderProvider) {
            $translatePartialLoaderProvider.addPart('transitional-accounts');
        }

})();