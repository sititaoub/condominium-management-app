/**
 * transitional-accounts-detail.js
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.transitionalAccounts')
        .component('transitionalAccountDetail', {
            controller: transitionalAccountDetailController,
            bindings: {
                'transitionalAccount': '<',
                'mode': '<'
            },
            templateUrl: 'app/transitional-accounts/transitional-account-detail/transitional-account-detail.html'
        });

    transitionalAccountDetailController.$inject = ['TransitionalAccountsService'];

    function transitionalAccountDetailController(TransitionalAccountsService) {
        var $ctrl = this;
        $ctrl.$onInit = onInit;

        function onInit() {
            $ctrl.mode = angular.isUndefined($ctrl.mode) ? 'TO_GIVE' : $ctrl.mode;
        }
    }
})();