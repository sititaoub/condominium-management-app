(function () {
    'use strict';

    angular.module('condominiumManagementApp.transitionalAccounts')
        .component('selectedTransactionDetail', {
            controller: SelectedTransactionDetailController,
            bindings: {
                transaction: '<',
                condominiumId: '<',
                period: '<',
                mode: '<'
            },
            templateUrl: 'app/transitional-accounts/selected-transaction-detail/selected-transaction-detail.html'
        });
    SelectedTransactionDetailController.$inject = ['$scope', '$state', '$filter', 'TransitionalAccountsService', 'NgTableParams'];

    function SelectedTransactionDetailController($scope, $state, $filter, TransitionalAccountsService, NgTableParams) {
        var $ctrl = this;
        $ctrl.$onInit = onInit;
        $ctrl.addRow = addRow;
        $ctrl.removeRow = removeRow;

        function onInit() {
            $ctrl.addedRecord = 0;
            $ctrl.loadingRecords = true;
            if (_.startsWith($ctrl.transaction.type, 'MAN_TX')) {
                TransitionalAccountsService.loadManualTransaction($ctrl.condominiumId, $ctrl.period.id, $ctrl.transaction.id).then(function (manualTransaction) {
                    $ctrl.manualTransaction = manualTransaction;
                });
            }
            $ctrl.transaction.records = [];
            $ctrl.mode = angular.isUndefined($ctrl.mode) ? 'TO_GIVE' : 'TO_HAVE';
            loadTrasactionsRecords();
            var data = _.concat([], $ctrl.transaction);
            $ctrl.tableParams = new NgTableParams({
                count: data.length // hides pager
            }, {
                    dataset: data,
                    counts: [] // hides page sizes
                });

            $ctrl.recordsTableParams = new NgTableParams({
                page: 1,
                count: 9999,
            }, {
                    counts: [],
                });
            $ctrl.recordsColumn = createRecordColumn();
        }

        function loadTrasactionsRecords() {
            TransitionalAccountsService.getTransactionRecords($ctrl.condominiumId, $ctrl.period.id, $ctrl.transaction.id).then(function (records) {
                _.forEach(records.content, function (record) {
                    if (angular.isUndefined($ctrl.transaction.records))
                        $ctrl.transaction.records = [];
                    record.date = new Date(record.date);
                    $ctrl.transaction.records = _.concat($ctrl.transaction.records, record);
                });
                if ($ctrl.transaction.records.length === 0) {
                    addRow();
                }
                $ctrl.recordsTableParams.settings({
                    dataset: $ctrl.transaction.records
                });
            });
        }

        function addRow() {
            var addWithHolding = !angular.isUndefined($ctrl.manualTransaction) && $ctrl.manualTransaction.withHolding;
            var payment = {
                "id": null,
                "checked": false,
                "date": new Date(),
                "description": "",
                "notes": null,
                "paymentMode": "BANK_TRANSFER",
                "recordMatPath": null,
                "referenceDocument": null,
                "rejected": false,
                "slaveRecordId": null,
                "status": "TO_PAY",
                "transactionId": $ctrl.transaction.id,
                "type": "PAYMENT",
                "addedId": $ctrl.addedRecord
            }
            $ctrl.transaction.records = _.concat($ctrl.transaction.records, payment);
            if (addWithHolding) {
                payment.withHolding = true;
                var withHolding = {
                    "id": null,
                    "checked": false,
                    "date": new Date(),
                    "description": "",
                    "notes": null,
                    "paymentMode": "BANK_TRANSFER",
                    "recordMatPath": null,
                    "referenceDocument": null,
                    "rejected": false,
                    "slaveRecordId": null,
                    "status": "TO_PAY",
                    "transactionId": $ctrl.transaction.id,
                    "type": "WITHHOLDING",
                    "addedId": $ctrl.addedRecord
                }
                $ctrl.transaction.records = _.concat($ctrl.transaction.records, withHolding);
            }
            $ctrl.addedRecord = $ctrl.addedRecord + 1;
            $ctrl.recordsTableParams.settings({
                dataset: $ctrl.transaction.records
            });
        }

        function removeRow(index) {
            $ctrl.transaction.records.splice(index, 1);
            $ctrl.recordsTableParams.settings({
                dataset: $ctrl.transaction.records
            });
            $ctrl.recordsTableParams.reload();
        }

        function createRecordColumn() {
            var column = [{
                field: 'subProtocol',
                title: $filter('translate')('TrasitionalAccounts.SelectedTransaction.Protocol'),
                show: true,
                groupable: 'protocol',
                kind: 'protocol',
                width: '300',
                headerClass: 'col-md-2'
            }, {
                field: 'type',
                title: $filter('translate')('TrasitionalAccounts.SelectedTransaction.Type'),
                show: true,
                groupable: 'type',
                kind: 'i18n-text',
                width: '300',
                headerClass: 'col-md-2'
            }, {
                field: 'description',
                title: $filter('translate')('TrasitionalAccounts.SelectedTransaction.Description'),
                show: true,
                groupable: 'type',
                kind: 'edit-text',
                width: '300',
                headerClass: 'col-md-2'
            }, {
                field: 'date',
                title: $filter('translate')('TrasitionalAccounts.SelectedTransaction.Date'),
                show: true,
                groupable: 'date',
                kind: 'date',
                width: '300',
                headerClass: 'col-md-3'
            }, {
                field: 'amount',
                title: $filter('translate')('TrasitionalAccounts.SelectedTransaction.Amount'),
                show: true,
                groupable: 'amount',
                kind: 'amount',
                width: '300',
                headerClass: 'col-md-2'
            }, {
                field: 'status',
                title: $filter('translate')('TrasitionalAccounts.SelectedTransaction.Status'),
                show: true,
                groupable: 'status',
                kind: 'i18n-text',
                width: '300',
                headerClass: 'col-md-2'
            }, {
                field: 'select',
                title: $filter('translate')('TrasitionalAccounts.SelectedTransaction.Select'),
                show: true,
                groupable: 'select',
                kind: 'check',
                width: '300',
                headerClass: 'col-md-2'
            }, {
                field: 'action',
                title: $filter('translate')('TrasitionalAccounts.SelectedTransaction.Action'),
                show: true,
                groupable: 'action',
                kind: 'action',
                width: '300',
                headerClass: 'col-md-2'
            }
            ];

            return column;
        }
    }
})();