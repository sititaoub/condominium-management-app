/*
 * transitional-accounts.routes.js
 *
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.transitionalAccounts')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider'];

    /* @ngInject */
    function setupRoutes($stateProvider) {
        $stateProvider.state('index.transitionalAccounts', {
            url: '/condominiums/{condominiumId}/financial-period/{periodId}/transitional-accounts',
            template: '<div class="transitional-accounts-container" ui-view></div>',
            abstract: true,
            data: {
                skipFinancialPeriodCheck: false
            },
            onEnter: ['$state', '$transition$', 'FinancialPeriodService', function ($state, $transition$, FinancialPeriodService) {
                // Ensure that if 'current' is provided as financial period, the current financial period is provided.
                if ($transition$.params().periodId === 'current') {
                    var params = angular.copy($transition$.params());
                    params.periodId = FinancialPeriodService.getCurrent($transition$.params().companyId, $transition$.params().condominiumId).id;
                    return $state.target($transition$.targetState().name(), params);
                }
                return true;
            }]
        })
            .state('index.transitionalAccounts.list', {
                url: '/list',
                component: 'transitionalAccountsListComponent'
            })
            .state('index.transitionalAccounts.detail', {
                url: '/detail',
                component: 'transitionalAccountsReconciliationComponent',
            })
    }

})();