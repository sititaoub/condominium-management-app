(function () {
    'use strict';

    angular.module("condominiumManagementApp.user")
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

    /* @ngInject */
    function setupRoutes($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('index.user', {
                abstract: true,
                url: "/user",
                template: '<ui-view/>'
            })
            .state('index.user.profile', {
                url: "/profile",
                templateUrl: "app/user/partials/profile.html",
                controller: "profileCtrl",
                data: {
                    pageTitle: 'Profilo'
                }
            });

        // See https://github.com/Narzerus/angular-permission/wiki/Controlling-access-in-views#async-calls-for-permissions-in-initial-states
        $urlRouterProvider.deferIntercept();
    }
})();