(function () {
    "use strict";

    angular.module("condominiumManagementApp.user")
        .service("InstallerUserService", InstallerUserService);

    InstallerUserService.$inject = ['$http', 'contextAddress'];

    /* @ngInject */
    function InstallerUserService($http, contextAddress) {
        return {
            getStats: function () {
                return $http({
                    method: 'GET',
                    url: contextAddress + 'v1/users/stats'
                });

            },
            getStatesHistory: function () {
                return $http({
                    method: 'GET',
                    url: contextAddress + 'v1/users/plant-history'
                });

            }
        }
    }
})();