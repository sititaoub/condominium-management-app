(function () {
    "use strict";

    angular.module("condominiumManagementApp.user", [
        'condominiumManagementApp.core',
        'condominiumManagementApp.commons'
    ]);
})();
