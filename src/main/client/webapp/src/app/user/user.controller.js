'use strict';

angular.module('condominiumManagementApp.user')
    .controller('profileCtrl', [
        "$scope",
        "$rootScope",
        "$state",
        "$timeout",
        "$uibModal",
        "$log",
        "DTOptionsBuilder",
        "DTColumnBuilder",
        "toaster",
        "PlantResource",
        "DashboardService",
        "TableService",
        "ChartService",
        "UserService",
        "InstallerUserService",
        "UtilsService",
        function ($scope, $rootScope, $state, $timeout, $uibModal, $log,
                  DTOptionsBuilder, DTColumnBuilder, toaster,
                  PlantResource, DashboardService,
                  TableService, ChartService, UserService, InstallerUserService,
                  UtilsService) {


            $log.debug('user: ');
            $log.debug($rootScope.user);
            $scope.timer = '?' + new Date().getTime();

            $scope.selectIcon = function (status) {
                switch (status) {
                    case "INSERTED":
                        return 'edit';
                        break;
                    case "MAPPED":
                        return 'building';
                        break;
                    case "CONFIGURED":
                        return 'gears';
                        break;
                    case "INSTALLED":
                        return 'check';
                        break;
                    case "ACTIVE":
                        return 'play';
                        break;

                }
            }

            /* $scope.changesLoading = true
             InstallerUserService.getStatesHistory().then(function(data){
                 $scope.changes = data.data.content;
                 $scope.changesLoading = false
             },function(data){
                 $scope.changesLoading = false
                 UtilsService.showHttpError(data.data, toaster);

             })*/

            $scope.statsLoading = true
            InstallerUserService.getStats().then(function (data) {
                $scope.stats = data.data;
                $scope.statsLoading = false
            }, function (data) {
                $scope.statsLoading = false
                UtilsService.showHttpError(data.data, toaster);

            })

            $scope.showChangePasswordModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/user/partials/change-password.html',
                    controller: 'changePasswordCtrl',
                    size: 'md',
                    scope: $scope
                });

            }


            $scope.showChangeUserPhotoModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/user/partials/change-user-photo.html',
                    controller: 'changeUserPhotoCtrl',
                    size: 'md',
                    scope: $scope
                });

            }

        }])
    .controller('changePasswordCtrl', [
        "$scope",
        "$rootScope",
        "$state",
        "$timeout",
        "$uibModal",
        "$uibModalInstance",
        "toaster",
        "UserService",
        "UtilsService",
        function ($scope, $rootScope, $state, $timeout, $uibModal, $uibModalInstance, toaster,
                  UserService, UtilsService) {
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

            $scope.submitForm = function (change_password_form) {
                if (change_password_form.$valid) {
                    UserService.changePassword($scope.changePassword).then(
                        function (data) {
                            UtilsService.showHttpSuccess(toaster);
                        },
                        function (data) {
                            UtilsService.showHttpError(toaster, data);
                        }
                    )
                } else {
                    change_password_form.submitted = true;
                }
            }
        }
    ])
    .controller('changeUserPhotoCtrl', [
        "$scope",
        "contextAddressProfile",
        "$state",
        "$timeout",
        "$uibModal",
        "$uibModalInstance",
        "$log",
        "toaster",
        "UserService",
        "UtilsService",
        function ($scope, contextAddressProfile, $state, $timeout, $uibModal, $uibModalInstance, $log, toaster,
                  UserService, UtilsService) {
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
                $scope.$parent.timer = '?' + new Date().getTime();

            };

            $scope.dzAddedFile = function (file) {
                $log.log(file);
            };

            $scope.dzSendingFile = function (file, hxr) {
                hxr.setRequestHeader('X-XSRF-TOKEN', UtilsService.getCookie("XSRF-TOKEN"));

                $log.log(file);
            };

            $scope.dzError = function (file, errorMessage) {
                $log.log(errorMessage);
            };

            $scope.dropzoneConfig = {
                parallelUploads: 3,
                maxFileSize: 30,
                uploadMultiple: false,
                method: 'post',
                url: contextAddressProfile + "v1/users/me/photo-changes",
            };
        }
    ]);
