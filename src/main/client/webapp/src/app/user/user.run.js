/*
 * user.run.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.user')
        .run(enforceSecurity);

    enforceSecurity.$inject = ['$transitions', '$rootScope', 'UserService', 'RoleStore', '$urlRouter'];

    /* @ngInject */
    function enforceSecurity($transitions, $rootScope, UserService, RoleStore, $urlRouter) {
        UserService.registerUser().then(function (user) {
            /* SM 2018/04/09: added user to Sentry */
            if( window.Raven !== undefined) {
                Raven.setUserContext({
                    id: user.id,
                    username: user.username
                });
            }
            $rootScope.user = user;
            RoleStore.defineManyRoles(UserService.getRoles());
        }).then(function () {
            // Once permissions are set-up
            // kick-off router and start the application rendering
            $urlRouter.sync();
            // Also enable router to listen to url changes
            $urlRouter.listen();
        });

        $transitions.onSuccess({ to: 'logout' }, function () {
            UserService.logoutUser();
        });
    }

})();