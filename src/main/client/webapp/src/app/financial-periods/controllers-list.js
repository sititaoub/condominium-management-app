
"use strict";

angular.module("condominiumManagementApp.financial-periods")
    .controller("financial-periods.ListController", [
    "$scope",
    "$state",
    "$timeout",
    "$stateParams",
    "DTOptionsBuilder",
    "DTColumnBuilder",
    "TableService",
    "FinancialPeriodResource",
    function ($scope, $state, $timeout,$stateParams,DTOptionsBuilder, DTColumnBuilder,TableService,FinancialPeriodResource) {

        TableService.getTableOptions($scope);

        $scope.condominiumId=$stateParams.condominiumId;

        $scope.loadingFinancialList = true;
        $scope.financialPeriods =
            FinancialPeriodResource.query({page:"0",size:"500",condominiumId:$scope.condominiumId},function(data) {
                $scope.loadingFinancialList = false;

        },function(data){
                $scope.loadingFinancialList = false;

        });


    }


]);

