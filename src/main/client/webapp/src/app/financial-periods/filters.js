"use strict";

angular.module("condominiumManagementApp.financial-periods").filter('abs', function () {
    return function(val) {
        return Math.abs(val);
    }
});