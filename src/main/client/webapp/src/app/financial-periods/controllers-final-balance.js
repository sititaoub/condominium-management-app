
"use strict";


angular.module("condominiumManagementApp.financial-periods")
	.controller("financial-periods.FinalBalanceController", [
    "$scope",
    "$rootScope",
    "$state",
    "$stateParams",
    "toaster",
    "$filter",
    "SweetAlert",
    "$uibModalInstance",
    "ManagementService",
    function ($scope, $rootScope, $state, $stateParams,toaster,$filter,SweetAlert,$uibModalInstance,ManagementService ) {


        ManagementService.getFinalBalance($scope.managementId).then(function(data){
            $scope.finalBalance=data.data;
        },
        function(){
            $scope.errorFinalBalance=true;
        })

        $scope.close = function(){
            $uibModalInstance.close();

        }


        $scope.back = function(){
            window.history.back();
        };



    }


]);


