'use strict';

angular.module("condominiumManagementApp.financial-periods")
	.config(function ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('index.management', {
				url: "/managements/{condominiumId}/{financialPeriodId}/{managementId}",
				templateUrl: "app/financial-periods/partials/management.html",
				controller: "financial-periods.ManagementController",
				data: {
					requiresAuthentication: true
				}
			})
			.state('index.financial-period-dashboard', {
				url: "/financial-period-dashboard/{condominiumId}/{financialPeriodId}",
				templateUrl: "app/financial-periods/partials/financial-period-dashboard.html",
				controller: "financial-periods.FinancialPeriodDashboardController",
				data: {
					requiresAuthentication: true
				}
			})
			.state('index.management-dashboard', {
				url: "/management-dashboard/{condominiumId}/{financialPeriodId}/{managementId}",
				templateUrl: "app/financial-periods/partials/management-dashboard.html",
				controller: "financial-periods.ManagementDashboardController",
				data: {
					requiresAuthentication: true
				}
			})

	});
