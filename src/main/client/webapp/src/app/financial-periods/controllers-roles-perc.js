"use strict";


angular.module("condominiumManagementApp.financial-periods")
    .controller("financial-periods.RolesPercController", [
        "$scope",
        "$rootScope",
        "$state",
        "$stateParams",
        "toaster",
        "$filter",
        "SweetAlert",
        "$uibModalInstance",
        "CentreCostConfigurationService",
        "ThousandthTableResource",
        "UtilsService",
        function ($scope, $rootScope, $state, $stateParams, toaster, $filter, SweetAlert, $uibModalInstance, CentreCostConfigurationService, ThousandthTableResource, UtilsService) {

            $scope.loading = true;

            CentreCostConfigurationService.getCentreCostConfiguration($scope.managementId).then(function (data) {

                angular.forEach(data.data, function (ccc) {
                    $scope.updateTotal(ccc);
                });

                $scope.cccs = data.data;

                ThousandthTableResource.query({
                    page: "0",
                    size: "500",
                    managementId: $scope.managementId
                }, function (data) {

                    $scope.thousandthTables = data.content;

                    CentreCostConfigurationService.getRoleLookUp().then(function (data) {
                        $scope.loading = false;
                        $scope.roleLookup = data.data;
                    }, function (data) {
                        $scope.loading = false;
                        UtilsService.showHttpError(data.data, toaster);
                    })

                }, function (data) {
                    $scope.loading = false;
                    UtilsService.showHttpError(data.data, toaster);
                });

            });

            $scope.close = function () {
                $uibModalInstance.close();

            };

            $scope.back = function () {
                window.history.back();
            };

            $scope.updateTotal = function (ccc) {

                var total = 0;
                angular.forEach(ccc.roleDistributions, function (value) {
                    total += value.percentage;
                });

                ccc.total = parseFloat(total.toFixed(2));

            };

            $scope.submitForm = function (form) {
                if (form.$valid) {

                    CentreCostConfigurationService.saveCentreCostConfiguration($scope.cccs).then(function () {

                            toaster.pop({
                                type: 'success',
                                title: 'Salvataggio effettuato',
                                showCloseButton: true,
                                timeout: 2000
                            });
                            $scope.close();
                            $state.go($state.current, $stateParams, {reload: true});

                        },
                        function () {
                            toaster.pop({
                                type: 'error',
                                title: 'Si è vericato un problema durante il salvataggio',
                                showCloseButton: true,
                                timeout: 2000
                            });
                        });


                } else {
                    form.submitted = true;
                }
            }

        }


    ]);


