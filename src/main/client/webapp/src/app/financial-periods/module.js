"use strict";

angular.module("condominiumManagementApp.financial-periods", [
    'condominiumManagementApp.core',
    'condominiumManagementApp.commons'
]);
