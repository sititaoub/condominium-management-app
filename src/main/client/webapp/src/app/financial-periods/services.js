"use strict";
angular.module("condominiumManagementApp.financial-periods")
    .factory("FinancialPeriodResource", ["$resource", "$rootScope", function ($resource, $rootScope) {
        return $resource("http://localhost:3000/condominium-management-app/condominium-management-server/v1/financial-periods/:id",
            {
                id: '@id'
            },
            {
                update: { method: "PUT" },
                query: { method: "GET", isArray: false }
            }
        );
    }])
    // .factory("FinancialPeriodResource", ["$resource", "$rootScope", function ($resource, $rootScope) {
    //     return $resource($rootScope.contextAddressManagement + "v1/financial-periods/:id",
    //         { id: '@id' },
    //         {
    //             update: { method: 'PUT' },
    //             query: { method: 'GET', isArray: false }
    //         }
    //     );
    // }])
    .factory("ManagementResource", ["$resource", "$rootScope", function ($resource, $rootScope) {
        return $resource($rootScope.contextAddressManagement + "v1/managements/:id",
            { id: '@id' },
            {
                update: { method: 'PUT' },
                query: { method: 'GET', isArray: false }
            }
        );
    }])
    .service('ManagementService', ['$http', '$rootScope', function ($http, $rootScope) {
        return {
            getManagementStatus: function (managementId) {
                return $http({
                    method: 'GET',
                    url: $rootScope.contextAddressManagement + 'v1/managements/' + managementId + '/status'
                });
            },
            getFinalBalance: function (managementId) {
                return $http({
                    method: 'GET',
                    url: $rootScope.contextAddressManagement + 'v1/managements/' + managementId + '/final-balance'
                });
            }
        };
    }])
    .factory("ThousandthTableResource", ["$resource", "$rootScope", function ($resource, $rootScope) {
        return $resource($rootScope.contextAddressManagement + "v1/thousandth-tables/:id",
            { id: '@id' },
            {
                update: { method: 'PUT' },
                query: { method: 'GET', isArray: false }
            }
        );
    }])
    .service('ThousandthTableService', ['$http', '$rootScope', function ($http, $rootScope) {
        return {
            getThousandthTableForCreation: function (condominiumId) {
                return $http({
                    method: 'GET',
                    url: $rootScope.contextAddressManagement + 'v1/thousandth-tables/new-by-condominuim/' + condominiumId
                });
            }
        };
    }])
    .service('InstallmentConfigurationService', ['$http', '$rootScope', function ($http, $rootScope) {
        return {
            getInstallmentConfiguration: function (managementId) {
                return $http({
                    method: 'GET',
                    url: $rootScope.contextAddressManagement + 'v1/instalment-configurations/by-management/' + managementId
                });
            },
            saveInstallmentConfiguration: function (installmentData) {
                return $http({
                    method: 'POST',
                    url: $rootScope.contextAddressManagement + 'v1/instalment-configurations/',
                    data: installmentData
                });
            }

        };
    }])
    .service('InstallmentPlanService', ['$http', '$rootScope', function ($http, $rootScope) {
        return {
            getInstallmentPlan: function (managementId) {
                return $http({
                    method: 'GET',
                    url: $rootScope.contextAddressManagement + 'v1/instalment-plans/' + managementId,
                    params: { size: 1000 }
                });
            }
        };
    }])
    .service('InstallmentPlanService', ['$http', 'contextAddressManagement', function ($http, contextAddressManagement) {
        return {
            getInstallmentPlan: function (managementId) {
                return $http({
                    method: 'GET',
                    url: contextAddressManagement + 'v1/instalment-plans/' + managementId,
                    params: { size: 1000 }
                });
            }

        };
    }])
    .service('CentreCostConfigurationService', ['$http', '$rootScope', function ($http, $rootScope) {
        return {
            getCentreCostConfiguration: function (managementId) {
                return $http({
                    method: 'GET',
                    url: $rootScope.contextAddressManagement + 'v1/centre-cost-configurations',
                    params: { managementId: managementId }
                });
            },
            getRoleLookUp: function () {
                return $http({
                    method: 'GET',
                    url: $rootScope.contextAddressManagement + 'v1/centre-cost-configurations/lookup-role-type'
                });
            },
            getCentreCostLookUp: function (condominiumId) {
                return $http({
                    method: 'GET',
                    url: $rootScope.contextAddressManagement + 'v1/centre-cost-configurations/lookup-centre-cost',
                    params: { condominiumId: condominiumId }
                });
            },
            saveCentreCostConfiguration: function (data) {
                return $http({
                    method: 'PUT',
                    url: $rootScope.contextAddressManagement + 'v1/centre-cost-configurations/',
                    data: data
                });
            }
        };
    }])
    .service('QuoteService', ['$http', '$rootScope', function ($http, $rootScope) {
        return {
            getQuoteMock: function () {
                return [
                    {
                        "name": "Centro di Costo 1",
                        "finalBalance": "1000",
                        "revision": "5",
                        "quote": "1050",
                        "pvc": [
                            { "name": "PVC 1" },
                            { "name": "PVC 2" },
                            { "name": "PVC 3" }
                        ]
                    },
                    {
                        "name": "Centro di Costo 2",
                        "finalBalance": "1000",
                        "revision": "5",
                        "quote": "1050",
                        "pvc": [
                            { "name": "PVC 1" },
                            { "name": "PVC 2" },
                            { "name": "PVC 3" }
                        ]
                    },
                    {
                        "name": "Centro di Costo 3",
                        "finalBalance": "1000",
                        "revision": "5",
                        "quote": "1050",
                        "pvc": [
                            { "name": "PVC 1" },
                            { "name": "PVC 2" },
                            { "name": "PVC 3" }
                        ]
                    }

                ];
            },
            getQuotesExtended: function (managementId) {
                return $http({
                    method: 'GET',
                    url: $rootScope.contextAddressManagement + 'v1/quotes/by-management/' + managementId
                });
            },
            saveQuotes: function (data) {
                return $http({
                    method: 'POST',
                    url: $rootScope.contextAddressManagement + 'v1/quotes',
                    data: data
                });
            }
        };
    }]);