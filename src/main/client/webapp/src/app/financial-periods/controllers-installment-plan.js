"use strict";


angular.module("condominiumManagementApp.financial-periods")
    .controller("financial-periods.InstallmentPlanController", [
        "$scope",
        "$rootScope",
        "$state",
        "$stateParams",
        "toaster",
        "$filter",
        "SweetAlert",
        "$uibModalInstance",
        "InstallmentConfigurationService",
        function ($scope, $rootScope, $state, $stateParams, toaster, $filter, SweetAlert, $uibModalInstance, InstallmentConfigurationService) {


            $scope.loading = true;

            InstallmentConfigurationService.getInstallmentConfiguration($scope.managementId).then(function (data) {

                    $scope.loading = false;
                    $scope.installmentConfiguration = data.data;
                    $scope.installmentNumber = data.data.details.length;
                },
                function (error) {
                    $scope.loading = false;
                    //In caso di 404 vuol dire che ancora non è stato fatta la configurazione delle rate
                    $scope.installmentConfiguration = {
                        "managementId": $scope.managementId,
                        "details": [],
                        "instalmentsNumber": 0

                    };
                });

            $scope.calculate = function () {


                if ($scope.installmentNumber <= 24) {

                    $scope.installmentConfiguration.instalmentsNumber = $scope.installmentNumber;


                    $scope.startDate = $scope.management.startDate;
                    var startDateMoment = moment($scope.startDate);

                    $scope.endDate = $scope.management.endDate;
                    var endDateMoment = moment($scope.endDate);

                    //Pulisce array
                    $scope.installmentConfiguration.details = [];

                    var daysGap = endDateMoment.diff(startDateMoment, 'days') / $scope.installmentNumber;
                    var percTot = 100 / $scope.installmentNumber;
                    var percSaldo = 100;

                    for (var i = 0; i < $scope.installmentNumber; i++) {

                        startDateMoment = startDateMoment.add('days', daysGap);

                        var tmpStartDate = moment(startDateMoment);
                        //Carica tutto sulla prima rata
                        if (i > 0) percSaldo = 0;

                        var item = {
                            "id": i + 1,
                            "instalmentDescription": "Rata " + (i + 1),
                            "instalmentDate": tmpStartDate.toDate(),
                            "balancePercentage": percSaldo,
                            "quotePercentage": percTot
                        }

                        $scope.installmentConfiguration.details.push(item);


                    }


                } else {
                    SweetAlert.swal({
                            title: "Attenzione",
                            text: "Numero massimo di rate superate ",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonText: "OK",
                            closeOnConfirm: true,
                            animation: "slide-from-top"
                        },
                        function (isConfirm) {
                            //terminare con chiamata a BE


                        });

                }
            }


            $scope.submitForm = function (form) {

                if (form.$valid) {

                    $scope.installmentConfiguration = InstallmentConfigurationService.saveInstallmentConfiguration($scope.installmentConfiguration).then(function (data) {

                            toaster.pop({
                                type: 'success',
                                title: 'Salvataggio effettuato',
                                showCloseButton: true,
                                timeout: 2000
                            });
                            $scope.close();
                            // $state.go($state.current, $stateParams, {reload: true});
                            $state.transitionTo("index.financial-period-dashboard", $stateParams, {reload: true});
                        },
                        function (error) {

                            toaster.pop({
                                type: 'error',
                                title: 'Si è vericato un problema durante il salvataggio',
                                showCloseButton: true,
                                timeout: 2000
                            });

                        });
                }
            }

            $scope.close = function () {
                $uibModalInstance.close();

            }

            $scope.back = function () {
                window.history.back();
            };


        }


    ]);


