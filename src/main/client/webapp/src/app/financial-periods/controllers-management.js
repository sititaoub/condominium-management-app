
"use strict";


angular.module("condominiumManagementApp.financial-periods")
	.controller("financial-periods.ManagementController", [
    "$scope",
    "$rootScope",
    "$state",
    "$stateParams",
    "toaster",
    "$filter",
    "SweetAlert",
    "ManagementResource",
    "UtilsService",
    "CentreCostConfigurationService",
    function ($scope, $rootScope, $state, $stateParams,toaster,$filter,SweetAlert,ManagementResource,UtilsService,CentreCostConfigurationService ) {

        $scope.condominiumId = $stateParams.condominiumId;
        $scope.financialPeriodId = $stateParams.financialPeriodId;
        $scope.managementId = $stateParams.managementId;


        $scope.fromDatePicker = UtilsService.datePicker();
        $scope.toDatePicker = UtilsService.datePicker();

        CentreCostConfigurationService.getCentreCostLookUp($scope.condominiumId).then(function(data){

            $scope.cdcLookUp=data.data.content;
        })



        //Se è un Edit
        if ($scope.managementId != 'new') {

            $scope.formData = ManagementResource.get({id: $scope.managementId}, function (data) {

                $scope.formData.startDate = moment(data.startDate).valueOf();
                $scope.formData.endDate = moment(data.endDate).valueOf();

                $scope.formData.centreCostIds=[];


                angular.forEach($scope.formData.centreCostConfigurations, function (item, index) {
                    $scope.centreCostIds=[];
                    $scope.centreCostIds.push(item.centreCostId);

                });


            });

        } else { //Se è un nuovo inserimento
            $scope.formData = {

                    "managementType":"SPECIAL",
                    "centreCostIds":[]
            };

            $scope.centreCostIds=[];
        }

        $scope.submitForm = function(form) {
            if (form.$valid) {

                $scope.formData.financialPeriodId=$scope.financialPeriodId;

                angular.forEach($scope.centreCostIds, function (item, index) {
                    $scope.formData.centreCostIds.push(item);
                });

                if($scope.managementId=='new'){

                    $scope.management = new ManagementResource($scope.formData);

                    ManagementResource.save($scope.management,
                        function(management, headers){
                            toaster.pop({
                                type: 'success',
                                title: 'Salvataggio effettuato',
                                showCloseButton: true,
                                timeout: 2000
                            });

                            $state.transitionTo("index.financial-period-dashboard",({condominiumId : $scope.condominiumId,financialPeriodId:$scope.financialPeriodId }));

                        },
                        function(){
                            toaster.pop({
                                type: 'error',
                                title: 'Si è vericato un problema durante il salvataggio',
                                showCloseButton: true,
                                timeout: 2000
                            });
                        });
                }else {
                    $scope.formData.$update({id: $scope.managementId},
                        function(management){
                            toaster.pop({
                                type: 'success',
                                title: 'Salvataggio effettuato',
                                showCloseButton: true,
                                timeout: 2000
                            });

                            $state.transitionTo("index.management-dashboard",({condominiumId : $scope.condominiumId,financialPeriodId:$scope.financialPeriodId,managementId:$scope.managementId }));

                        },
                        function(){
                            toaster.pop({
                                type: 'error',
                                title: 'Si è vericato un problema durante il salvataggio',
                                showCloseButton: true,
                                timeout: 2000
                            });
                        });
                }

            } else {
                form.submitted = true;
            }
        }

        $scope.back = function(){
            window.history.back();
        };



    }


]);


