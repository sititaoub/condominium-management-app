"use strict";


angular.module("condominiumManagementApp.financial-periods")
    .controller("financial-periods.ThousandthsTablesController", [
        "$scope",
        "$rootScope",
        "$state",
        "$stateParams",
        "toaster",
        "$filter",
        "SweetAlert",
        "$uibModalInstance",
        "ThousandthTableResource",
        "ThousandthTableService",
        "$window",
        "UtilsService",
        function ($scope, $rootScope, $state, $stateParams, toaster, $filter, SweetAlert, $uibModalInstance, ThousandthTableResource, ThousandthTableService, $window, UtilsService) {

            $scope.init = function () {

                $scope.loading = true;

                $scope.thousandthTables = ThousandthTableResource.query({
                    page: "0",
                    size: "500",
                    managementId: $scope.managementId
                }, function (data) {

                    if (data.content.length > 0) {
                        $scope.formData = data.content[0]; //Seleziona in partenza il primo

                        $scope.tableSelection(data.content[0].id);

                    } else {
                        $scope.createTable();
                    }


                }, function (data) {
                    $scope.loading = false;
                    UtilsService.showHttpError(data.data, toaster);
                });
            }

            $scope.init();


            $scope.tableSelection = function (id) {

                $scope.loading = true;

                $scope.thousandthTableId = id;

                $scope.formData = ThousandthTableResource.get({id: $scope.thousandthTableId}, function (data) {
                    $scope.loading = false;
                }, function (data) {
                    $scope.loading = false;
                    UtilsService.showHttpError(data.data, toaster);
                });
            }

            $scope.createTable = function () {

                $scope.loading = true;
                $scope.thousandthTableId = 'new';

                ThousandthTableService.getThousandthTableForCreation($scope.condominiumId).then(function (data) {

                    $scope.loading = false;
                    $scope.formData = data.data;
                    $scope.formData.managementId = $scope.managementId;
                }, function (data) {
                    $scope.loading = false;
                    UtilsService.showHttpError(data.data, toaster);
                });

            }


            $scope.deleteTable = function () {

                SweetAlert.swal({
                        title: "Vuoi davvero eliminare la tabella?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: true,
                        animation: "slide-from-top"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $scope.thousandthTables = ThousandthTableResource.delete({id: $scope.thousandthTableId}, function (data) {

                                toaster.pop({
                                    type: 'success',
                                    title: 'Eliminazione effettuata',
                                    showCloseButton: true,
                                    timeout: 2000
                                });

                                //Qui va ricaricata la pagina
                                $scope.init();

                            });
                        }

                    });


            }

            $scope.submitForm = function (form) {
                if (form.$valid) {

                    if ($scope.thousandthTableId == 'new') {

                        $scope.thousandthTable = new ThousandthTableResource($scope.formData);

                        ThousandthTableResource.save($scope.thousandthTable,
                            function (thousandthTable, headers) {
                                toaster.pop({
                                    type: 'success',
                                    title: 'Salvataggio effettuato',
                                    showCloseButton: true,
                                    timeout: 2000
                                });
                                $scope.close();
                                // $state.go($state.current, $stateParams, {reload: true});
                                $state.transitionTo("index.financial-period-dashboard", $stateParams, {reload: true});
                            },
                            function () {
                                toaster.pop({
                                    type: 'error',
                                    title: 'Si è vericato un problema durante il salvataggio',
                                    showCloseButton: true,
                                    timeout: 2000
                                });
                            });
                    } else {
                        $scope.formData.$update({id: $scope.thousandthTableId},
                            function (thousandthTable) {
                                toaster.pop({
                                    type: 'success',
                                    title: 'Salvataggio effettuato',
                                    showCloseButton: true,
                                    timeout: 2000
                                });

                                $scope.close();

                                $state.go($state.current, $stateParams, {reload: true});
                            },
                            function () {
                                toaster.pop({
                                    type: 'error',
                                    title: 'Si è vericato un problema durante il salvataggio',
                                    showCloseButton: true,
                                    timeout: 2000
                                });
                            });
                    }

                } else {
                    form.submitted = true;
                }
            }

            $scope.close = function () {
                $uibModalInstance.close();

            }

            $scope.back = function () {
                window.history.back();
            };


        }


    ]);


