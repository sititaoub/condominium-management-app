
"use strict";


angular.module("condominiumManagementApp.financial-periods")
	.controller("financial-periods.FinancialPeriodDashboardController", [
    "$scope",
    "$rootScope",
    "$state",
    "$stateParams",
    "toaster",
    "$filter",
    "SweetAlert",
    "$uibModal",
    "ManagementResource",
    "ManagementService",
    "FinancialPeriodResource",
    "InstallmentPlanService",
    "InstallmentConfigurationService",
    function ($scope, $rootScope, $state, $stateParams,toaster,$filter,SweetAlert ,$uibModal,
              ManagementResource, ManagementService,
              FinancialPeriodResource,InstallmentPlanService,InstallmentConfigurationService) {


        $scope.condominiumId=$stateParams.condominiumId;

        $scope.financialPeriodId=$stateParams.financialPeriodId;

        $scope.loadingFinancialPeriod = true;
        $scope.financialPeriod =  FinancialPeriodResource.get({id:$scope.financialPeriodId},function(data) {
            $scope.loadingFinancialPeriod = false;
            $scope.managementId=data.management.id;
            $scope.management=data.management;

            $scope.loadingInstallmentPlan = true;
            InstallmentPlanService.getInstallmentPlan($scope.managementId).then(function(data){
                $scope.loadingInstallmentPlan = false;
                $scope.installmentPlan=data.data.content;

                $scope.totalGrid=0;

                angular.forEach($scope.installmentPlan, function (item, index) {

                    var totalTmp=0;

                    angular.forEach(item.instalmentPlanDetails, function (itemDetail, index) {
                        totalTmp=totalTmp+itemDetail.amount;
                    });

                    item.total=totalTmp;
                    $scope.totalGrid=$scope.totalGrid+item.total;
                });

                InstallmentConfigurationService.getInstallmentConfiguration($scope.managementId).then(function(data){

                        $scope.installmentConfiguration =data.data;

                    });
            },function(data){
                $scope.loadingInstallmentPlan = false;
                $scope.loadInstallmentPlanError = 'Errore caricamento dati';
                if (data.status == "409") {
                    $scope.loadInstallmentPlanError = $filter('translate')(data.data.userMessage);
                }

            })

            $scope.loadingStatus = true;
            ManagementService.getManagementStatus($scope.managementId).then(function(data){
                $scope.loadingStatus = false;
                $scope.status = data.data;
            },function(data){
                $scope.loadingStatus = false;
            });

        },function(data){
            $scope.loadingFinancialPeriod = false;

        });

        $scope.managements =  ManagementResource.query({page:"0",size:"500",financialPeriodId:$scope.financialPeriodId,type:["SPECIAL","AUXILIARY"]},function(data) {

        });


        $scope.closeFinancialPlan=function(){
            //TODO
        };

        $scope.openQuoteDialog=function() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/financial-periods/partials/quote.html',
                controller: 'financial-periods.QuoteController',
                size: 'lg',
                scope: $scope
            });
        };

        $scope.openThousandthsTablesDialog=function() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/financial-periods/partials/thousandths-tables.html',
                controller: 'financial-periods.ThousandthsTablesController',
                size: 'lg',
                scope: $scope
            });
        };


        $scope.openInstallmentPlanDialog=function() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/financial-periods/partials/installment-plan.html',
                controller: 'financial-periods.InstallmentPlanController',
                size: 'lg',
                scope: $scope
            });
        };

        $scope.openFinalBalanceDialog=function() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/financial-periods/partials/final-balance.html',
                controller: 'financial-periods.FinalBalanceController',
                size: 'lg',
                scope: $scope
            });
        };

        $scope.openRolesPercDialog=function() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/financial-periods/partials/roles-perc.html',
                controller: 'financial-periods.RolesPercController',
                size: 'lg',
                scope: $scope
            });
        };


        $scope.closeFinancialPlan=function() {
            SweetAlert.swal({
                    title: "Attenzione?",
                    text: "Si sta effettuando la chiusura di un esercizio ",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Chiudi esercizio",
                    cancelButtonText: "Annulla",
                    closeOnConfirm: true,
                    animation: "slide-from-top"
                },
                function (isConfirm) {
                    //terminare con chiamata a BE



                });
        }


        /**
         * Flot chart data and options
         */
        var d1 = [[1262304000000, 6], [1264982400000, 3057], [1267401600000, 20434], [1270080000000, 31982], [1272672000000, 26602], [1275350400000, 27826], [1277942400000, 24302], [1280620800000, 24237], [1283299200000, 21004], [1285891200000, 12144], [1288569600000, 10577], [1291161600000, 10295]];
        var d2 = [[1262304000000, 5], [1264982400000, 200], [1267401600000, 1605], [1270080000000, 6129], [1272672000000, 11643], [1275350400000, 19055], [1277942400000, 30062], [1280620800000, 39197], [1283299200000, 37000], [1285891200000, 27000], [1288569600000, 21000], [1291161600000, 17000]];

        var flotChartData1 = [
            { label: "Data 1", data: d1, color: '#17a084'},
            { label: "Data 2", data: d2, color: '#127e68' }
        ];

        var flotChartOptions1 = {
            xaxis: {
                tickDecimals: 0
            },
            series: {
                lines: {
                    show: true,
                    fill: true,
                    fillColor: {
                        colors: [{
                            opacity: 1
                        }, {
                            opacity: 1
                        }]
                    }
                },
                points: {
                    width: 0.1,
                    show: false
                }
            },
            grid: {
                show: false,
                borderWidth: 0
            },
            legend: {
                show: false
            }
        };

        var flotChartData2 = [
            { label: "Data 1", data: d1, color: '#19a0a1'}
        ];

        var flotChartOptions2 = {
            xaxis: {
                tickDecimals: 0
            },
            series: {
                lines: {
                    show: true,
                    fill: true,
                    fillColor: {
                        colors: [{
                            opacity: 1
                        }, {
                            opacity: 1
                        }]
                    }
                },
                points: {
                    width: 0.1,
                    show: false
                }
            },
            grid: {
                show: false,
                borderWidth: 0
            },
            legend: {
                show: false
            }
        };

        var flotChartData3 = [
            { label: "Data 1", data: d1, color: '#fbbe7b'},
            { label: "Data 2", data: d2, color: '#f8ac59' }
        ];

        var flotChartOptions3 = {
            xaxis: {
                tickDecimals: 0
            },
            series: {
                lines: {
                    show: true,
                    fill: true,
                    fillColor: {
                        colors: [{
                            opacity: 1
                        }, {
                            opacity: 1
                        }]
                    }
                },
                points: {
                    width: 0.1,
                    show: false
                }
            },
            grid: {
                show: false,
                borderWidth: 0
            },
            legend: {
                show: false
            }
        };

        /**
         * Definition of variables
         * Flot chart
         */

        $scope.flotChartData1 = flotChartData1;
        $scope.flotChartOptions1 = flotChartOptions1;
        $scope.flotChartData2 = flotChartData2;
        $scope.flotChartOptions2 = flotChartOptions2;
        $scope.flotChartData3 = flotChartData3;
        $scope.flotChartOptions3 = flotChartOptions3;



        //------------------------------------------


        $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
        $scope.series = ['Series A', 'Series B'];
        $scope.data = [
            [65, 59, 80, 81, 56, 55, 40],
            [28, 48, 40, 19, 86, 27, 90]
        ];

        $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        $scope.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }


]);


