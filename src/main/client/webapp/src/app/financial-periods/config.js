"use strict";

angular.module("condominiumManagementApp.financial-periods")
	.config( function ($translatePartialLoaderProvider) {
		$translatePartialLoaderProvider.addPart("financial-periods");
	});
