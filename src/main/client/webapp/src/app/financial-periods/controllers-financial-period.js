"use strict";

angular.module("condominiumManagementApp.financial-periods")
    .controller("financial-periods.FinancialPeriodController", [
        "$scope",
        "$rootScope",
        "$state",
        "$stateParams",
        "toaster",
        "$filter",
        "SweetAlert",
        "FinancialPeriodResource",
        "UtilsService",
        "CentreCostConfigurationService",
        function ($scope, $rootScope, $state, $stateParams, toaster, $filter, SweetAlert, FinancialPeriodResource, UtilsService, CentreCostConfigurationService) {

            $scope.condominiumId = $stateParams.condominiumId;
            $scope.financialPeriodId = $stateParams.financialPeriodId;

            $scope.fromDatePicker = UtilsService.datePicker();
            $scope.toDatePicker = UtilsService.datePicker();

            $scope.managementFromDatePicker = UtilsService.datePicker();
            $scope.managementToDatePicker = UtilsService.datePicker();

            CentreCostConfigurationService.getCentreCostLookUp($scope.condominiumId).then(function (data) {
                $scope.cdcLookUp = data.data.content;
            });


            //Se è un Edit
            if ($scope.financialPeriodId != 'new') {

                $scope.formData = FinancialPeriodResource.get({ id: $scope.financialPeriodId }, function (data) {

                    $scope.formData.startDate = moment(data.startDate).valueOf();
                    $scope.formData.endDate = moment(data.endDate).valueOf();

                    $scope.formData.management.startDate = moment(data.management.startDate).valueOf();
                    $scope.formData.management.endDate = moment(data.management.endDate).valueOf();

                });

            } else { //Se è un nuovo inserimento
                $scope.formData = {
                    "status": "OPEN",
                    "management": {
                        "managementType": "ORDINARY",
                        "centreCostIds": []
                    }
                };
            }

            $scope.submitForm = function (form) {
                if (form.$valid) {

                    $scope.formData.condominiumId = $scope.condominiumId;

                    //Duplica date su DTO interno di management
                    $scope.formData.management.startDate = $scope.formData.startDate;
                    $scope.formData.management.endDate = $scope.formData.endDate;


                    if ($scope.financialPeriodId == 'new') {

                        $scope.financialPeriod = new FinancialPeriodResource($scope.formData);

                        FinancialPeriodResource.save($scope.financialPeriod,
                            function (financialPeriod, headers) {
                                toaster.pop({
                                    type: 'success',
                                    title: 'Salvataggio effettuato',
                                    showCloseButton: true,
                                    timeout: 2000
                                });

                                $state.transitionTo("index.financial-periods-list", ({ condominiumId: $scope.condominiumId }));

                            },
                            function () {
                                toaster.pop({
                                    type: 'error',
                                    title: 'Si è vericato un problema durante il salvataggio',
                                    showCloseButton: true,
                                    timeout: 2000
                                });
                            });
                    } else {
                        $scope.formData.$update({ id: $scope.financialPeriodId },
                            function (financialPeriod) {
                                toaster.pop({
                                    type: 'success',
                                    title: 'Salvataggio effettuato',
                                    showCloseButton: true,
                                    timeout: 2000
                                });

                                $state.transitionTo("index.financial-period-dashboard", ({
                                    condominiumId: $scope.condominiumId,
                                    financialPeriodId: $scope.financialPeriodId
                                }));

                            },
                            function () {
                                toaster.pop({
                                    type: 'error',
                                    title: 'Si è vericato un problema durante il salvataggio',
                                    showCloseButton: true,
                                    timeout: 2000
                                });
                            });
                    }

                } else {
                    form.submitted = true;
                }
            }

            $scope.back = function () {
                window.history.back();
            };


        }


    ]);


