"use strict";

angular.module("condominiumManagementApp.financial-periods")
    .controller("financial-periods.QuoteController", [
        "$scope",
        "$rootScope",
        "$state",
        "$stateParams",
        "$locale",
        "toaster",
        "$filter",
        "SweetAlert",
        "$uibModalInstance",
        "QuoteService",
        "UtilsService",
        function ($scope, $rootScope, $state, $stateParams, $locale, toaster, $filter, SweetAlert, $uibModalInstance, QuoteService, UtilsService) {

            var MARGIN_TYPE_PERCENTAGE = "PERCENTAGE";
            var MARGIN_TYPE_FIXED = "FIXED";

            var translate = $filter('translate');

            $scope.loading = true;
            $scope.quotes = [];

            init();

            $scope.calculateEstimatedCost = function (quote) {

                var transfer = convert2Float(quote.transfer);
                var revision = convert2Float(quote.revision);

                quote.margin = revision;

                if (typeof quote.revision === 'string' && quote.revision.indexOf("%") >= 0) {
                    // è stato utilizzata la forma percentuale
                    // calcolo la percentuale
                    quote.estimatedCost = convert2Float(transfer + (transfer * (revision / 100)));
                    quote.marginType = MARGIN_TYPE_PERCENTAGE;
                } else {
                    // è stato inserito un valore esplicito
                    quote.estimatedCost = transfer + revision;
                    quote.marginType = MARGIN_TYPE_FIXED;
                }

            };

            $scope.calculateRevision = function (quote) {
                var estimatedCost = convert2Float(quote.estimatedCost);
                var transfer = convert2Float(quote.transfer);

                quote.revision = estimatedCost - transfer;
                quote.margin = quote.revision;
                quote.marginType = MARGIN_TYPE_FIXED;

            };

            $scope.onRevisionBlur = function (quote) {

                if (isNaN(parseInt(quote.revision))) {
                    quote.revision = 0;
                    quote.margin = 0;
                }

            };

            $scope.submitForm = function (form) {

                if (form.$valid) {

                    QuoteService.saveQuotes($scope.quotes).then(function (data) {

                            toaster.pop({
                                type: 'success',
                                title: translate('quote.save.done.message'),
                                showCloseButton: true,
                                timeout: 2000
                            });

                            console.log("saved: ", $scope.quotes);
                            $scope.close();
                            // $state.go($state.current, $stateParams, {reload: true});
                            $state.transitionTo("index.financial-period-dashboard", $stateParams, {reload: true});
                        },
                        function (error) {


                            console.log("error: ", error, "on quotes: ", $scope.quotes);
                            toaster.pop({
                                type: 'error',
                                title: translate('quote.save.error.message'),
                                showCloseButton: true,
                                timeout: 2000
                            });

                        });
                } else {
                    console.log("not valid: ", $scope.quotes);

                    toaster.pop({
                        type: 'error',
                        title: translate('quote.form.error.message'),
                        showCloseButton: true,
                        timeout: 5000
                    });
                }
            };

            $scope.close = function () {
                $uibModalInstance.close();

            };

            $scope.back = function () {
                window.history.back();
            };

            /////////////////////////////////////////////////////


            /**
             * Converte un valore in un float, se non è valido restituisce 0
             * @param value
             * @returns {*}
             */
            function convert2Float(value) {
                var result = parseFloat(("" + value).replace(/[\.,]/g, "."));

                if (isNaN(result)) {
                    return 0;
                } else {
                    return parseFloat(result.toFixed(2));
                }


            }

            /**
             * Genera un campo (revision) da visualizzare in base al tipo di margine
             * che è stato scelto (numero esplicito o percentuale)
             * @param quote
             */
            function calculateRevision(quote) {
                if (!quote) {
                    return null;
                }

                if (quote.marginType == MARGIN_TYPE_PERCENTAGE) {
                    quote.revision = quote.margin + " %";
                } else {
                    quote.revision = quote.margin;
                }

                return quote;
            }

            function init() {
                QuoteService.getQuotesExtended($scope.managementId).then(function (data) {

                    $scope.loading = false;
                    if (data.data.quotes) {
                        data.data.quotes.forEach(function (quote) {
                            calculateRevision(quote);
                        });
                    }

                    $scope.quotes = data.data;

                }, function(data) {
                    $scope.loading = false;
                    UtilsService.showHttpError(data.data, toaster);
                });
            }


        }


    ]);


