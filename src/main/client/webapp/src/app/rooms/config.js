"use strict";

angular.module("condominiumManagementApp.rooms")
	.config( function ($translatePartialLoaderProvider) {
		$translatePartialLoaderProvider.addPart("rooms");
	});
