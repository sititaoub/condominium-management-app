"use strict";

angular.module("condominiumManagementApp.rooms", [
    'condominiumManagementApp.core',
    'condominiumManagementApp.commons'
]);
