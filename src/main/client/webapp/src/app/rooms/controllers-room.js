
"use strict";


angular.module("condominiumManagementApp.rooms")
    .controller("rooms.RoomController", [
        "$scope",
        "$rootScope",
        "$state",
        "$stateParams",
        "RoomResource",
        "toaster",
        "$filter",
        "SweetAlert",
        function ($scope, $rootScope, $state, $stateParams, RoomResource, toaster, $filter, SweetAlert) {

            $scope.huId = $stateParams.huId;
            $scope.roomId = $stateParams.roomId;
            $scope.description = $stateParams.description;

            $scope.back = function () {
                $scope.$emit("backRoom", { id: $scope.roomId, huId: $scope.huId });
            };

            //Se è un Edit
            if ($scope.roomId != 'new') {
                $scope.$emit("selectRoom", { id: $scope.roomId, huId: $scope.huId });

                $scope.formData = RoomResource.get({ id: $scope.roomId }, function (data) {

                });



            } else { //Se è un nuovo inserimento
                $scope.$emit("preaddRoom", { huId: $scope.huId });

                $scope.formData = {
                    identification: {},
                    cadastralReference: {},
                    address: {
                        "country": "IT"
                    }
                };
            }



            $scope.submitForm = function (form) {
                if (form.$valid) {

                    $scope.formData.housingUnitId = $scope.huId;


                    if ($scope.roomId == 'new') {

                        $scope.newRoom = new RoomResource($scope.formData);

                        RoomResource.save($scope.newRoom,
                            function (room, headers) {
                                toaster.pop({
                                    type: 'success',
                                    title: 'Salvataggio effettuato',
                                    showCloseButton: true,
                                    timeout: 2000
                                });
                                $scope.description = room.roomDescription;
                                room.id = headers('location').split("/").pop();
                                $scope.$emit("addRoom", room);
                            },
                            function () {
                                toaster.pop({
                                    type: 'error',
                                    title: 'Si è vericato un problema durante il salvataggio',
                                    showCloseButton: true,
                                    timeout: 2000
                                });
                            });
                    } else {
                        $scope.formData.$update({ id: $scope.roomId },
                            function (room) {
                                toaster.pop({
                                    type: 'success',
                                    title: 'Salvataggio effettuato',
                                    showCloseButton: true,
                                    timeout: 2000
                                });
                                $scope.description = room.roomDescription;
                                $scope.$emit("editRoom", room);
                            },
                            function () {
                                toaster.pop({
                                    type: 'error',
                                    title: 'Si è vericato un problema durante il salvataggio',
                                    showCloseButton: true,
                                    timeout: 2000
                                });
                            });
                    }

                } else {
                    form.submitted = true;
                }
            }



            $scope.delete = function () {

                SweetAlert.swal({
                    title: "Vuoi davvero eliminare la stanza?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    animation: "slide-from-top"
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            $scope.formData.$remove({ id: $scope.roomId },
                                function (response) {
                                    toaster.pop({
                                        type: 'success',
                                        title: 'Eliminazione effettuata',
                                        showCloseButton: true,
                                        timeout: 2000
                                    });

                                    $scope.$emit("removeRoom", { id: $scope.roomId, huId: $scope.huId });
                                    // redirect putted into event handler

                                },
                                function (response) {
                                    toaster.pop({
                                        type: 'error',
                                        title: $filter('translate')(response.data.userMessage),
                                        showCloseButton: true,
                                        timeout: 10000
                                    });
                                });
                        }


                    });





            };




        }


    ]);


