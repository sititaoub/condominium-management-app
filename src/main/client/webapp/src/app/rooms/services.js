"use strict";
angular.module("condominiumManagementApp.rooms")
    .factory("RoomResource", ["$resource", "contextAddress", function ($resource, contextAddress) {
        return $resource(contextAddress + "v1/rooms/:id",
            {id: '@id'},
            {
                update: {method: 'PUT'}
            });

    }]);
