/*
 * manager-identification-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('managerIdentificationInput', {
            controller: ManagerIdentificationInputController,
            bindings: {
                'ngModel': '<'
            },
            require: {
                'ngModelCtrl': 'ngModel'
            },
            templateUrl: 'app/managers/manager-identification-input.html'
        });

    ManagerIdentificationInputController.$inject = ['$q', 'ManagersService','ValidatorService'];

    /* @ngInject */
    function ManagerIdentificationInputController($q, ManagersService,ValidatorService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.validateTaxCode = validateTaxCode;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
                if ($ctrl.value) {
                    $ctrl.taxCode = $ctrl.value.taxCode;
                }
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function validateTaxCode(taxCode) {
            var deferred = $q.defer();
            if (taxCode && (taxCode.length === 11 || taxCode.length === 16) && taxCode !== $ctrl.taxCode) {
                    ManagersService.exists(taxCode).then(function () {
                        deferred.reject();
                    }).catch(function () {
                        deferred.resolve();
                    });
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }
    }

})();

