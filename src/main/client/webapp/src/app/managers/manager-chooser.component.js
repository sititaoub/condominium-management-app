/*
 * manager-chooser.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.managers')
        .component('managerChooser', {
            controller: ManagerChooserController,
            bindings: {
                id: '@',
                name: '@',
                ngModel: '<',
                ngRequired: '<?',
                ngDisabled: '<?'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/managers/manager-chooser.html'
        });

    ManagerChooserController.$inject = ['$uibModal', 'ManagersService', 'NotifierService'];

    /* @ngInject */
    function ManagerChooserController($uibModal, ManagersService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.$onChanges = onChanges;
        $ctrl.openLookup = openLookup;
        $ctrl.remove = remove;

        ////////////

        function onInit() {
            $ctrl.manager = undefined;

            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChanges(changes) {
            if (angular.isDefined(changes.ngModel) &&
                angular.isDefined(changes.ngModel.currentValue) &&
                changes.ngModel.currentValue !== null) {

                ManagersService.getOne(changes.ngModel.currentValue).then(function (manager) {
                    $ctrl.manager = manager;
                    $ctrl.value = manager.firstName + ' ' + manager.lastName;
                }).catch(function (error) {
                    if (error.status === 404) {
                        $ctrl.manager = undefined;
                        $ctrl.value = null;
                    } else {
                        NotifierService.notifyError();
                    }
                });
            }
        }

        function openLookup() {
            $uibModal.open({
                component: 'managerPicker',
                size: 'lg'
            }).result.then(function (manager) {
                $ctrl.manager = manager;
                $ctrl.value = manager.firstName + ' ' + manager.lastName;
                onChange();
            });
        }

        function remove() {
            $ctrl.manager = undefined;
            $ctrl.value = undefined;
            onChange();
        }

        function onChange() {
            var value = angular.isDefined($ctrl.manager) ? $ctrl.manager.id : undefined;
            $ctrl.ngModelCtrl.$setViewValue(value);
        }
    }

})();

