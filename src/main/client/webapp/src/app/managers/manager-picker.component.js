/*
 * manager-picker.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.managers')
        .component('managerPicker', {
            controller: ManagerPickerController,
            bindings: {
                'close': '&',
                'dismiss': '&'
            },
            templateUrl: 'app/managers/manager-picker.html'
        });

    ManagerPickerController.$inject = ['NgTableParams', 'ManagersService', 'NotifierService'];

    /* @ngInject */
    function ManagerPickerController(NgTableParams, ManagersService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doPick = doPick;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.tableParams = new NgTableParams({
                page: 1,
                count: 10,
                sort: 'taxCode,asc'
            }, {
                getData: loadManagers
            });
        }

        function doPick(manager) {
            $ctrl.close({ $value: manager });
        }

        function loadManagers(params) {
            return ManagersService.getPage(params.page() - 1, params.count(), params.sorting(), params.filter())
                .then(function (data) {
                    params.total(data.totalElements);
                    return data.content;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    $ctrl.loading = false;
                });
        }
    }

})();

