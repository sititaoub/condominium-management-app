/*
 * managers-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.managers')
        .component('managersList', {
            controller: ManagersListController,
            bindings: {},
            templateUrl: 'app/managers/managers-list.html'
        });

    ManagersListController.$inject = ['$state', 'NgTableParams', 'ManagersService', 'NotifierService', 'AlertService', '$translate'];

    /* @ngInject */
    function ManagersListController($state, NgTableParams, ManagersService, NotifierService, AlertService, $translate) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.toggleFilters = toggleFilters;
        $ctrl.deleteManager = deleteManager;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10,
                sort: 'taxCode,asc'
            }, {
                filterOptions: { filterLayout: "horizontal" },
                getData: loadManagers
            });

            $translate(['Managers.List.FirstName','Managers.List.LastName']).then(function(data) {
                $ctrl.fullNameFilterDef = {
                    firstName: { id: "text", placeholder: data['Managers.List.FirstName'] },
                    lastName: { id: "text", placeholder: data['Managers.List.LastName'] }
                };
            });

            
            $ctrl.showFilters = $ctrl.tableParams.hasFilter();
        }

        function toggleFilters() {
            $ctrl.showFilters = !$ctrl.showFilters;
        }

        function deleteManager(manager) {
            AlertService.askConfirm('Managers.Remove.Message', manager).then(function () {
                ManagersService.remove(manager.id).then(function () {
                    return NotifierService.notifySuccess('Managers.Remove.Success');
                }).catch(function () {
                    return NotifierService.notifyError('Managers.Remove.Failure');
                }).finally(function () {
                    $ctrl.tableParams.reload();
                })
            });
        }

        function loadManagers(params) {
            $state.go('.', params.uiRouterUrl(), { inherit: false });
            return ManagersService.getPage(params.page() - 1, params.count(), params.sorting(), params.filter())
                .then(function (data) {
                    params.total(data.totalElements);
                    return data.content;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    $ctrl.loading = false;
                });
        }
    }

})();

