/*
 * managers.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.managers')
        .config(setupRoutes);

        setupRoutes.$inject = ['$stateProvider'];

        /* @ngInject */
        function setupRoutes ($stateProvider) {
            $stateProvider.state('index.managers', {
                url: '/managers',
                redirectTo: 'index.managers.list',
                data: {
                    skipFinancialPeriodCheck: true
                },
                template: '<div class="managers-container" ui-view></div>'
            }).state('index.managers.list', {
                url : '?page&count&sort&companyName&firstName&lastName&taxCode&vatNumber',
                component: 'managersList',
                params: {
                    page: { dynamic: true, squash: true, value: '1', inherits: true },
                    count: { dynamic: true, squash: true, value: '10', inherits: true },
                    sort: { dynamic: true, squash: true, value: 'taxCode,asc', inherits: true },
                    companyName: { dynamic: true, inherits: true },
                    firstName: { dynamic: true, inherits: true },
                    lastName: { dynamic: true, inherits: true },
                    taxCode: { dynamic: true, inherits: true },
                    vatNumber: { dynamic: true, inherits: true }
                }
            }).state('index.managers.list.create', {
                url: '/create',
                views: {
                    '@index.managers': {
                        component: 'managerCreate'
                    }
                }
            }).state('index.managers.list.detail', {
                url: '/{managerId}',
                abstract: true,
                resolve: {
                    manager: ['$transition$', 'ManagersService', function ($transition$, ManagersService) {
                        return ManagersService.getOne($transition$.params().managerId);
                    }]
                }
            }).state('index.managers.list.detail.edit', {
                url: '/edit',
                views: {
                    '@index.managers': {
                        component: 'managerEdit'
                    }
                }
            }).state('index.managers.list.detail.pictures', {
                url: '/pictures',
                views: {
                    '@index.managers': {
                        component: 'managerPictures'
                    }
                }
            });
        }

})();