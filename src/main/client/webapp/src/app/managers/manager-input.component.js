/*
 * manager-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.managers')
        .component('managerInput', {
            controller: ManagerInputController,
            bindings: {
                ngModel: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/managers/manager-input.html'
        });

    ManagerInputController.$inject = [];

    /* @ngInject */
    function ManagerInputController() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            if($ctrl.value.firstName == null) {
                $ctrl.value.firstName  = "";
            }
            if($ctrl.value.lastName == null) {
                $ctrl.value.lastName  = "";
            }
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

