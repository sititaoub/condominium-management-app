/*
 * manager-contacts-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('managerContactsInput', {
            controller: ManagerContactsInputController,
            bindings: {
                'ngModel': '<',
                'ngRequired': '<'
            },
            require: {
                'ngModelCtrl': 'ngModel'
            },
            templateUrl: 'app/managers/manager-contacts-input.html'
        });

    ManagerContactsInputController.$inject = [];

    /* @ngInject */
    function ManagerContactsInputController() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

