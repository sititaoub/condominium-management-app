/*
 * managers.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.managers')
        .factory('ManagersService', ManagersService);

    ManagersService.$inject = ['$q', 'Restangular', 'contextAddressPeople', 'contextAddressPeopleUpload', '_'];

    /* @ngInject */
    function ManagersService($q, Restangular, contextAddressPeople, contextAddressPeopleUpload, _) {
        var Managers = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressPeople + 'v1');
            configurer.addResponseInterceptor(function (data, operation, what, url, response) {
                if(data.birthData && data.birthData.birthDate) {
                    data.birthData.birthDate =  new Date(data.birthData.birthDate);
                }
                if (operation === "post") {
                    var location = response.headers('Location');
                    return location.substr(location.lastIndexOf('/') + 1);
                }
                return data;
            });
        }).all('managers');
        var Upload = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressPeopleUpload + 'v1');
        });

        var service = {
            getPage: getPage,
            search: search,
            exists: exists,
            getOne: getOne,
            getLogoPath: getLogoPath,
            getLogoUploadPath: getLogoUploadPath,
            getSignaturePath: getSignaturePath,
            getSignatureUploadPath: getSignatureUploadPath,
            create: create,
            update: update,
            remove: remove,
            removeLogo: removeLogo,
            removeSignature: removeSignature
        };
        return service;

        ////////////////

        /**
         * @namespace Managers
         */

        /**
         * Managers page.
         *
         * @typedef {Object} Managers~ManagersPage
         * @property {number} number - The requested page number.
         * @property {number} size - The requested page size.
         * @property {number} totalElements - The total number of elements.
         * @property {number} totalPages - The total number of pages.
         * @property {Managers~Manager[]} content - The page content.
         */

        /**
         * Manager object.
         *
         * @typedef {Object} Managers~Manager
         * @property {number} id - The unique id of manager.
         * @property {number} version - The manager version
         * @property {string} taxCode - The tax code.
         * @property {string} vatNumber - The vat number.
         * @property {string} companyName - The company name.
         * @property {string} firstName - The first name.
         * @property {string} lastName - The last name.
         * @property {Managers~PostalAddress} postalAddress - The postal address
         * @property {Object} contacts - An hash with key the contact type and value the contact
         * @property {number} numberOfCondominiums - Number of managed condominiums
         * @property {boolean} logoPresent - True if the logo picture is present
         * @property {boolean} signaturePresent - True if the signature picture is present
         */

        /**
         * Manager postal address.
         *
         * @typedef {Object} Managers~PostalAddress
         * @property {string} streetName - The address street name.
         * @property {string} buildingNumber - The address building number.
         * @property {string} town - The address town.
         * @property {string} district - The address district.
         * @property {string} province - The address province.
         * @property {string} country - The address country
         * @property {number} [lat] - The latitude
         * @property {number} [lng] - The longitude.
         */

        /**
         * Manager birth data.
         *
         * @typedef {Object} Managers~BirthData
         * @property {string} birthDate - The birth date.
         * @property {string} birthTown - The birth town.
         * @property {string} birthStateProvince - The birth state province.
         */

        /**
         * Retrieve a single page of managers..
         *
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=10] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<Managers~ManagersPage|Error>} - The promise of page.
         */
        function getPage(page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return Managers.get("", params);
        }

        /**
         * Search for managers with name.
         *
         * @param {string} name - The name to look for.
         * @returns {Promise<Managers~Manager[]|Error>} - The promise of managers.
         */
        function search(name) {
            Managers.one("search", "by-name").getList({ name: name })
        }

        /**
         * Search for managers with tax code.
         *
         * @param {string} taxCode - The tax code to look for.
         * @returns {Promise<Object|Error>} - The promise that it's resolved if object exists and reject if does not.
         */
        function exists(taxCode) {
            return Managers.one("search", "by-tax-code").head({ taxCode: taxCode }).catch(function (err) {
                if (err.status === 302) {
                    return $q.resolve();
                } else {
                    return $q.reject(err);
                }
            });
        }

        /**
         * Retrieve a single manager.
         *
         * @param {number} id - The unique manager id.
         * @returns {Promise<Managers~Manager|Error>} - The promise of page.
         */
        function getOne(id) {
            return Managers.one("", id).get();
        }

        /**
         * Retrieve the path for logo picture.
         *
         * @param {number} id - The unique manager id.
         * @returns {string} - The path.
         */
        function getLogoPath(id) {
            return Managers.one("", id).all("logo").getRestangularUrl();
        }

        /**
         * Retrieve the path for logo picture upload.
         *
         * @param {number} id - The unique manager id.
         * @returns {string} - The path.
         */
        function getLogoUploadPath(id) {
            return Upload.one("managers", id).all("logo").getRestangularUrl();
        }

        /**
         * Retrieve the path for signature picture.
         *
         * @param {number} id - The unique manager id.
         * @returns {string} - The path.
         */
        function getSignaturePath(id) {
            return Managers.one("", id).all("signature").getRestangularUrl();
        }

        /**
         * Retrieve the path for signature picture upload.
         *
         * @param {number} id - The unique manager id.
         * @returns {string} - The path.
         */
        function getSignatureUploadPath(id) {
            return Upload.one("managers", id).all("signature").getRestangularUrl();
        }

        /**
         * Create a new manager.
         *
         * @param {Managers~Manager} manager - The manager to be created.
         * @returns {Promise<Number|Error>} - The promise of result.
         */
        function create(manager) {
            return Managers.post(manager);
        }

        /**
         * Updates an existing manager.
         *
         * @param {number} id - The unique id of manager to update.
         * @param {Managers~Manager} manager - The manager to be updated
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function update(id, manager) {
            return Managers.one("", id).customPUT(manager);
        }

        /**
         * Remove the manager with supplied id.
         *
         * @param {number} id - The unique id of manager
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function remove(id) {
            return Managers.one("", id).remove();
        }

        /**
         * Remove the for logo picture.
         *
         * @param {number} id - The unique manager id.
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function removeLogo(id) {
            return Managers.one("", id).all("logo").remove();
        }

        /**
         * Retrieve the path for signature picture.
         *
         * @param {number} id - The unique manager id.
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function removeSignature(id) {
            return Managers.one("", id).all("signature").remove();
        }
    }

})();