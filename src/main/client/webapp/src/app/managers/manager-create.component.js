/*
 * manager-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.managers')
        .component('managerCreate', {
            controller: ManagerCreateController,
            bindings: {},
            templateUrl: 'app/managers/manager-create.html'
        });

    ManagerCreateController.$inject = ['$state', 'ManagersService', 'NotifierService'];

    /* @ngInject */
    function ManagerCreateController($state, ManagersService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.manager = undefined;
        }



        function doSave() {
            _.forEach($ctrl.manager, function(value,key) {
                if(key == "") {
                    delete $ctrl.manager[key];
                }
            });
            ManagersService.create($ctrl.manager).then(function () {
                return NotifierService.notifySuccess('Managers.Create.Success');
            }).then(function () {
                $state.go("^.list");
            }).catch(function () {
                return NotifierService.notifyError('Managers.Create.Failure');
            });
        }
    }

})();

