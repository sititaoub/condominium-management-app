/*
 * manager-birthdata-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.person')
        .component('managerBirthdataInput', {
            controller: ManagerBirthdataInputController,
            bindings: {
                'ngModel': '<'
            },
            require: {
                'ngModelCtrl': 'ngModel'
            },
            templateUrl: 'app/managers/manager-birthdata-input.html'
        });

    ManagerBirthdataInputController.$inject = ['DistrictsService'];

    /* @ngInject */
    function ManagerBirthdataInputController(DistrictsService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.getDistricts = getDistricts;
        $ctrl.onSelect = onSelect;
        $ctrl.cleanProvince = cleanProvince;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function getDistricts(name) {
            return DistrictsService.getAll(name).catch(function () {
                NotifierService.notifyError();
            });
        }

        function onSelect(item) {
            $ctrl.value.birthData.birthStateProvince = item.province;
            onChange();
        }

        function cleanProvince() {
            if ($ctrl.value.birthData.birthTown == null || $ctrl.value.birthData.birthTown == "" ){
                $ctrl.value.birthData.birthStateProvince = "";
            }
        }
    }

})();