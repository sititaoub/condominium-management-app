/*
 * manager-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.managers')
        .component('managerEdit', {
            controller: ManagerEditController,
            bindings: {
                manager: '<'
            },
            templateUrl: 'app/managers/manager-edit.html'
        });

    ManagerEditController.$inject = ['$state', 'ManagersService', 'NotifierService'];

    /* @ngInject */
    function ManagerEditController($state, ManagersService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.managerId = $ctrl.manager.id;
        }

        function doSave() {
            _.forEach($ctrl.manager, function(value,key) {
                if(value == "") {
                    delete $ctrl.manager[key];
                }
            });
            ManagersService.update($ctrl.managerId, $ctrl.manager).then(function () {
                return NotifierService.notifySuccess('Managers.Edit.Success');
            }).then(function () {
                $state.go("^");
            }).catch(function () {
                return NotifierService.notifyError('Managers.Edit.Failure');
            });
        }
    }
})();

