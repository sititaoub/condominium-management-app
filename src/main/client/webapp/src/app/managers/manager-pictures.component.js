/*
 * manager-pictures.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.managers')
        .component('managerPictures', {
            controller: ManagerPicturesController,
            bindings: {
                manager: '<'
            },
            templateUrl: 'app/managers/manager-pictures.html'
        });

    ManagerPicturesController.$inject = ['$q', '$state', 'Upload', 'ManagersService', 'NotifierService'];

    /* @ngInject */
    function ManagerPicturesController($q, $state, Upload, ManagersService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.uploadLogo = uploadLogo;
        $ctrl.removeLogo = removeLogo;
        $ctrl.uploadSignature = uploadSignature;
        $ctrl.removeSignature = removeSignature;

        ////////////

        function onInit() {
            $ctrl.logoUrl = ManagersService.getLogoPath($ctrl.manager.id) + '?ts=' + new Date();
            $ctrl.signatureUrl = ManagersService.getSignaturePath($ctrl.manager.id) + '?ts=' + new Date();
        }

        function uploadLogo(file) {
            Upload.upload({
                url: ManagersService.getLogoUploadPath($ctrl.manager.id),
                data: { _method: 'POST', file: file },
                method: 'POST'
            }).then(function () {
                NotifierService.notifySuccess('Managers.Pictures.UploadSuccess');
                $state.go('.', undefined, { reload: true });
            }).catch(function (err) {
                NotifierService.notifyError('Managers.Pictures.UploadFailure');
                return $q.reject(err);
            });
        }

        function removeLogo() {
            return ManagersService.removeLogo($ctrl.manager.id).then(function () {
                return NotifierService.notifySuccess('Managers.Pictures.LogoRemoveSuccess');
            }).then(function () {
                return $state.go('.', undefined, { reload: true });
            }).catch(function (err) {
                NotifierService.notifyError('Managers.Pictures.LogoRemoveFailure');
                return $q.reject(err);
            })
        }

        function uploadSignature(file) {
            Upload.upload({
                url: ManagersService.getSignatureUploadPath($ctrl.manager.id),
                data: { _method: 'POST', file: file },
                method: 'POST'
            }).then(function () {
                NotifierService.notifySuccess('Managers.Pictures.UploadSuccess');
                $state.go('.', undefined, { reload: true });
            }).catch(function (err) {
                NotifierService.notifyError('Managers.Pictures.UploadFailure');
                return $q.reject(err);
            });
        }

        function removeSignature() {
            return ManagersService.removeSignature($ctrl.manager.id).then(function () {
                return NotifierService.notifySuccess('Managers.Pictures.SignatureRemoveSuccess');
            }).then(function () {
                return $state.go('.', undefined, { reload: true });
            }).catch(function (err) {
                NotifierService.notifyError('Managers.Pictures.SignatureRemoveFailure');
                return $q.reject(err);
            });
        }
    }

})();

