'use strict';

angular.module('condominiumManagementApp', [
    'ngRaven',
    
    'condominiumManagementApp.core',
    'condominiumManagementApp.commons',

    'blocks.maps',
    'blocks.validators',
    'blocks.controls',

    'condominiumManagementApp.assignments',
    'condominiumManagementApp.breakdownCriteria',
    'condominiumManagementApp.budget',
    'condominiumManagementApp.dashboard',
    'condominiumManagementApp.condominiums',
    'condominiumManagementApp.defaultAgendas',
    'condominiumManagementApp.structures',
    'condominiumManagementApp.configuration',
    'condominiumManagementApp.person',
    'condominiumManagementApp.housing_units',
    'condominiumManagementApp.managers',
    'condominiumManagementApp.meetings',
    'condominiumManagementApp.moneyAccount',
    'condominiumManagementApp.phrases',
    'condominiumManagementApp.rooms',
    'condominiumManagementApp.financialPeriod',
    'condominiumManagementApp.chartOfAccounts',
    'condominiumManagementApp.financial-periods',
    'condominiumManagementApp.chartOfAccounts',
    'condominiumManagementApp.installmentPlan',
    'condominiumManagementApp.accountingTransaction',
    'condominiumManagementApp.templates',
    'condominiumManagementApp.thousandthsTables',
    'condominiumManagementApp.legacyStructure',
    'condominiumManagementApp.user',
    'condominiumManagementApp.votingRules',
    'condominiumManagementApp.withholding',
    'condominiumManagementApp.accountingWriting',
    'condominiumManagementApp.transitionalAccounts',
    'condominiumManagementApp.purchaseInvoices',
    'condominiumManagementApp.automatedWritings'
]);
