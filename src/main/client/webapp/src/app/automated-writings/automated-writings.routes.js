/*
 * installment-plan.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.automatedWritings')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider'];

    /* @ngInject */
    function setupRoutes($stateProvider) {
        $stateProvider.state('index.automated-writings', {
            url: '/condominiums/{condominiumId}/automated-writings',
            abstract: true,
            template: '<div class="automated-writing-container" ui-view></div>',
            redirectTo: 'index.automated-writings.list',
            data: {
                skipFinancialPeriodCheck: true
            }   
         }).state('index.automated-writings.list', {
            url: "?page&count&sort",
            templateUrl: "app/automated-writings/automated-writings.html",
            controller: "AutomatedWritingsController",
            params: {
                page: { dynamic: true, squash: true, value: '1', inherits: true },
                count: { dynamic: true, squash: true, value: '10', inherits: true },
                description: { dynamic: true, inherits: true }
            },
            controllerAs: 'vm'
        })
        .state('index.automated-writings.list.add', {
            url: '/new',
            views: {
                '@index.automated-writings': {
                    templateUrl: "app/automated-writings/automated-writings-create.html",
                    controller: "AutomatedWritingsCreateController",
                    controllerAs: 'vm'
                }
            },
            resolve: {
                companyDetail: ['$transition$', 'CompanyService', function ($transition$, CompanyService) {
                    return CompanyService.getOne($transition$.params().companyId);
                }]
            }
        }).state('index.automated-writings.list.update', {
            url: '/edit?templateId',
            views: {
                '@index.automated-writings': {
                    templateUrl: "app/automated-writings/automated-writings-update.html",
                    controller: "AutomatedWritingsUpdateController",
                    controllerAs: 'vm'
                }
            },
            resolve: {
                companyDetail: ['$transition$', 'CompanyService', function ($transition$, CompanyService) {
                    return CompanyService.getOne($transition$.params().companyId);
                }],
                template: ['$transition$', 'CompanyService','AutomatedWritingsServices', function ($transition$, CompanyService,AutomatedWritingsServices) {
                    return AutomatedWritingsServices.getOne($transition$.params().companyId, $transition$.params().templateId);
                }]
            }
        });
    }

})();
