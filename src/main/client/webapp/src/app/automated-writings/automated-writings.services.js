/*
 * materialized-path-lookup.services.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .factory('AutomatedWritingsServices', AutomatedWritingsServices);

        AutomatedWritingsServices.$inject = ['$q', 'Restangular', 'CacheFactory', 'contextAddressAggregatori'];

    /* @ngInject */
    function AutomatedWritingsServices($q, Restangular, CacheFactory, contextAddressAggregatori) {
        var CACHE_NAME = 'AutomatedWritingsServices';
        var CACHE_EXPIRATION_MS = 60 * 1000; //1 minuto
        var pendingQueries = {};

        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressAggregatori + 'v2');
        });

        if (!CacheFactory.get(CACHE_NAME)) {
            CacheFactory.createCache(CACHE_NAME, {
                deleteOnExpire: 'passive',
                maxAge: CACHE_EXPIRATION_MS
            });
        }
        var cache = CacheFactory.get(CACHE_NAME);

        var service = {
            getPage:   getPage,
            getByCode: getByCode,
            create: create,
            update: update,
            remove: remove,
            getOne: getOne
        };
        return service;

        ////////////////


        function getPage(companyId, ownerCompanyId, page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
			});
            var params = angular.merge({}, filters, {
                ownerCompanyId: ownerCompanyId,
                page: page || 0,
                size: size || 10,
                showHidden: false,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return Api.one("companies", companyId).all("automated-writings").get("", params);
        }


        /**
         * Retrieve a single template  for supplied unique identifier.
         *
         * @param {number} companyId   - The unique identifier of company to retrieve lookup matPath for.
         * @param {number} chartOfAccountsId - The unique identifier to retrieve  lookup matPath for.
         * @param {String} code  - the key of key
         * @returns 
         */
        function getByCode(companyId, chartOfAccountsId, code) {
            var deferred = $q.defer();

        

            var key = '/companies/' + companyId + '/automated-writings/' + chartOfAccountsId;
                key += '/code/' + code;
            var result = cache.get(key);
            if (result) {
                deferred.resolve(result);
            } else if (pendingQueries[key]) {
                return pendingQueries[key];
            } else {
                pendingQueries[key] = deferred.promise;

                Api.one('companies', companyId). one("automated-writings/by-code/", code).get({
                    }).then(function (data) {
                    cache.put(key, data);
                    pendingQueries[key] = undefined;
                    deferred.resolve(data);
                }).catch(deferred.reject);
            }

            return deferred.promise;
        }


         /**
         * Create a new financial period.
         *
         * @param {number} companyId - The unique company id
         * @param {AutomatedWritingDTO~AutomatedWritingDTO} automatedWriting - The financial period.
         * @returns {Promise<Number|Error>} - The promise over the created financial period unique identifier.
         */
        function create(companyId, automatedWriting) {
            return Api.one("companies", companyId).one("automated-writings").post("",automatedWriting);
        }


        function update(companyId, id, automatedWriting) {
            return Api.one("companies", companyId).one("automated-writings",id).doPUT(automatedWriting);
        }


        function getOne(companyId, id) {
            return Api.one("companies", companyId).one("automated-writings",id).get();
        }

        function remove(companyId, automatedWriting) {
            return Api.one("companies", companyId).one("automated-writings",automatedWriting.id).remove();
        }

    }

})();

