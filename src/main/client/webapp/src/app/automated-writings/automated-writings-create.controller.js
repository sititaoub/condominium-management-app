/*
 * cost-management-create.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.automatedWritings')
        .controller('AutomatedWritingsCreateController', AutomatedWritingsCreateController);

    AutomatedWritingsCreateController.$inject = ['$state', 'UserDefinedTxService', 'FinancialPeriodService', 'MaterializedPathLookupService', 'NotifierService',
        'AccountingTransactionService', 'AutomatedWritingsServices', '$q', 'companyDetail'];

    /* @ngInject */
    function AutomatedWritingsCreateController($state, UserDefinedTxService, FinancialPeriodService, MaterializedPathLookupService, NotifierService, AccountingTransactionService,
        AutomatedWritingsServices, $q, companyDetail) {
        var vm = this;
        vm.companyId = $state.params.companyId;
        vm.doSave = doSave;
        vm.checkOwnerAccountIsDefined = checkOwnerAccountIsDefined;
        vm.loading = true;
        activate();

        ////////////////

        function activate() {
            vm.companyDetail = companyDetail;
            vm.isUpdate = false;
            vm.template = new Object();
            vm.template.details = new Array(); 
            vm.template.ownerCompanyId = vm.companyDetail.id;
            vm.template.type = "USER_DEFINED_TRANSACTION";
            vm.template.visible = true;
            vm.loading = false;
            vm.template.ownerCompanyId = undefined;
        }


        /**
         * Search on toGiveList and TohaveList for a item with ownerCompanyId valorized
         * return true if ownerId is defined , else otherwise
         * @param {*} toGive 
         * @param {*} toHave 
         */
        function checkOwnerAccountIsDefined(toGiveList, toHaveLIst) {
             var ownerId =  _.head(_.flatMap(_.filter(_.union(toGiveList, toHaveLIst), function(o) { 
                return angular.isDefined(o.selectedAccount) && o.selectedAccount.ownerCompanyId !=null                
             }),'selectedAccount.ownerCompanyId')); 

             vm.template.ownerCompanyId = ownerId;
             return angular.isDefined( vm.template.ownerCompanyId);
        }    

        function doSave() {
            vm.template.details = new Array(); 
           
            angular.forEach(vm.toGiveList, function (value) {
                vm.template.details.push(mapDetail('TO_GIVE', value));
                if (value.ownerCompanyId != null)
                    vm.template.ownerCompanyId = value.ownerCompanyId;
            });

            angular.forEach(vm.toHaveList, function (value) {
                vm.template.details.push(mapDetail('TO_HAVE', value));
    
                if (value.ownerCompanyId != null)
                    vm.template.ownerCompanyId = value.ownerCompanyId;
            });

            return AutomatedWritingsServices.create(vm.companyDetail.id, vm.template).then(function () {
                return NotifierService.notifySuccess('AutomatedWritings.Create.Success').then(function () {
                    return $state.go('^', { inherits: true });
                });
            })
                .catch(function (err) {
                    NotifierService.notifyError('AutomatedWritings.Create.Failure', err);
                    return $q.reject(err);
                });
        }
 

        function mapDetail(type, value){
            var detail = {};
            detail.type = type;
    
            if (value.selectedAccount.level === 'GRUPPO' || value.selectedAccount.level === 'MASTRO' ||
                value.selectedAccount.level === 'CONTO' ){
                detail.materializedPath = value.selectedAccount.materializedPath + '.*';                         
            }else
                detail.materializedPath = value.selectedAccount.materializedPath; 

            detail.amountType =  value.percentage ? "PERCENTAGE" : "FIXED";
            detail.amountValue = value.percentage ? value.coefficient : value.amount;
            return detail;
        }


    }

})();

