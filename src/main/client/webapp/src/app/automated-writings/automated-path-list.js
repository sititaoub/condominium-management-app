/*
 * technician-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.condominiums')
        .component('automatedPathList', {
            controller: AutomatedPathListController  ,
            bindings: {
                ngModel: '<',
                companyDetail: '<',
                externalRootPath: '<?',
                ngDisabled: '<?',
                title: '@',
                onAccountSelected: '&'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/automated-writings/automated-path-list.html'
        });

        AutomatedPathListController .$inject = ['InstalmentTransactionService', 'CompanyService','AccountingTransactionService','NotifierService', 'AlertService', '$attrs'];

    /* @ngInject */
    function AutomatedPathListController (AutomatedPathListController , CompanyService, AccountingTransactionService, NotifierService, AlertService, $attrs) {
        var $ctrl = this;
        $ctrl.$onInit = onInit;
        $ctrl.initialized = false;
        $ctrl.onChange = onChange;
        $ctrl.changeRowType = changeRowType;
        $ctrl.$onChanges = onChanges;
        $ctrl.value = undefined;
        $ctrl.doSum = doSum;
        $ctrl.fnCheckPercentage   =  fnCheckPercentage;
        $ctrl.fnCheckAmountPositive =  fnCheckAmountPositive;
        $ctrl.addRow       = addRow;
        $ctrl.removeRow    = removeRow;
        $ctrl.totalPercentage = 0;
        $ctrl.sumBy=_.sumBy;
        $ctrl.checkSelectedAccount = checkSelectedAccount;
        ////////////

 
        function onInit() {
 
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.sum = 0;
                $ctrl.rootPathAccountIsDefined   =  angular.isDefined($attrs.externalRootPath);
                $ctrl.value                      =  $ctrl.ngModelCtrl.$viewValue;
              
				//if model contains value
                if (angular.isArray($ctrl.value) && $ctrl.value.length > 0){
                    $ctrl.usingPercentage  = true;
                    var rootPath    =  $ctrl.rootPathAccountIsDefined ? $ctrl.externalRootPath : undefined;
                    $ctrl.value     = _.map($ctrl.value, function(detail){
                        
                        return _.defaults({
                            "rootPath": rootPath , 
                            "rootPathIsDefined" : $ctrl.rootPathAccountIsDefined, 
                            "percentage" : detail.percentage
                        }, detail)
                    });
                }else{

                    initEmptyTemplate();
                }

                $ctrl.initialized = true;
                doSum();
                $ctrl.loading = false;
            };

  
        }

        function initEmptyTemplate(){
            $ctrl.value= new Array();
            $ctrl.usingPercentage = true;
            var rootPath          =  $ctrl.rootPathAccountIsDefined ? $ctrl.externalRootPath : undefined; 
            var amountType        =  "PERCENTAGE";
            addInternalRow(rootPath, undefined, 100, amountType, 0 );
        }


        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function onChanges(changes) { 
        }

        function checkSelectedAccount(account){
            if (account.ownerCompanyId && account.ownerCompanyId != null)
                NotifierService.notifyWarning('AutomatedWritings.Create.SpecificTemplate');

            $ctrl.onAccountSelected({ account: account});    
        }

        function doSum(){

            var total = 0;
            
            if ($ctrl.usingPercentage){
                $ctrl.sum = 0;
                var totalFixedAmount =   _.sumBy(_.flatMap(_.filter($ctrl.value, function (row) {return !row.percentage;})),'amount');
                var foundPercentage = false;   
                
                angular.forEach($ctrl.value, function (value) {
                 
                    var computed = value.amount;
                    if (value.percentage){
                        foundPercentage = true;
                    }

                    total += value.coefficient;
                    
                    $ctrl.sum += parseFloat( computed.toFixed(2));
                });

                $ctrl.totalPercentage = parseFloat(total.toFixed(4));
                $ctrl.checkPercentage = !foundPercentage || ( $ctrl.totalPercentage == 100);
                
            }

            
            $ctrl.existsAmountNegative =   _.size(_.flatMap(_.filter($ctrl.value, function (row) {return row.amount<0;})))>0;
            onChange();
        }



        function addRow() {
            
            var newValue = 100 - parseFloat($ctrl.totalPercentage.toFixed(4));
            newValue =parseFloat(newValue.toFixed(4));
            newValue = newValue > 0 ? newValue : 0 ;
            var rootPath    =  $ctrl.rootPathAccountIsDefined ? $ctrl.externalRootPath : undefined; 
            addInternalRow(rootPath, undefined, newValue, "PERCENTAGE", 0, undefined);
            doSum();
        }

 
        function addInternalRow(rootPath, materializedPath, coefficient, amountType, amount, position) {
            
            var newRow = {"rootPath": rootPath, "rootPathIsDefined" : angular.isDefined(rootPath),  "materializedPath": materializedPath, 
            "coefficient": coefficient, "type" : $ctrl.writingType ,  "amount" :  angular.isDefined(amount) ?  amount : 0 , "percentage" : amountType ===  'PERCENTAGE'};
            var newPosition = position? position : $ctrl.value.length;
            $ctrl.value.splice(newPosition, 0, newRow);
        }

        function changeRowType(row){

            if (row.percentage && $ctrl.value.length==1){
                row.coefficient = 100;
            }
            else 
                row.coefficient = 0;
            
            row.amount = 0;
            doSum();
        }
        

        function removeRow(index) {
            $ctrl.value.splice(index,1);
            if ($ctrl.value.length==1){
                $ctrl.value[0].coefficient=$ctrl.value[0].percentage ? 100 : 0;
                $ctrl.usingPercentage = true;    
            }    
            doSum();
            onChange();
        }
        
        function fnCheckPercentage()
        {
            return  true;//$ctrl.checkPercentage;
        }

        function fnCheckAmountPositive(value){
            return !$ctrl.existsAmountNegative;
        }


    }

 







})();

