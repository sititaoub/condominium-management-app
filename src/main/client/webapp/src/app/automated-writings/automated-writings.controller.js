/*
 * accounting-record.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.automatedWritings')
        .controller('AutomatedWritingsController', AutomatedWritingsController);

        AutomatedWritingsController.$inject = ['$state', '$uibModal', 'AutomatedWritingsServices', 'AccountingTransactionService', 'NgTableParams','NotifierService','AlertService'];

    /* @ngInject */
    function AutomatedWritingsController($state, $uibModal,AutomatedWritingsServices, AccountingTransactionService, NgTableParams, NotifierService, AlertService) {
        var vm = this;
        vm.toggleFilters = toggleFilters;
        vm.companyId = $state.params.companyId;
        vm.deleteAutomatedWriting = deleteAutomatedWriting;
        activate();
        ////////////////

        function activate() {
            vm.loading = true;
            vm.firstLoad = true;
            vm.filterDateOpened = false;
            vm.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10
            }, {
                    getData: loadAccountingWritings,
                });

            vm.showFilters = vm.tableParams.hasFilter();
        }

        function toggleFilters() {
            vm.showFilters = !vm.showFilters;
        }

        function loadAccountingWritings(params) {

            $state.go('.', params.uiRouterUrl(), { inherit: false });
            return AutomatedWritingsServices.getPage($state.params.companyId, $state.params.periodId,
                params.page() - 1, params.count(), params.sorting(), params.filter()).then(function (page) {
                    params.total(page.totalElements);
                    vm.firstLoad = false;
                    return  page.content;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    vm.loading = false;
                });
        }

        function deleteAutomatedWriting(record) {

            AlertService.askConfirm('AutomatedWritings.Remove.Message', record).then(function () {
                AutomatedWritingsServices.remove($state.params.companyId, record).then(function () {
                        return NotifierService.notifySuccess('AccountingTransaction.Remove.Success');
                    }).catch(function (err) {
                        return NotifierService.notifyError('AccountingTransaction.Remove.Failure', err);
                    }).finally(function () {
                        vm.tableParams.reload();
                    })
            }
            );
             
        }

    }


 

})();

