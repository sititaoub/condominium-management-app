/*
 * cost-management-create.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.automatedWritings')
        .controller('AutomatedWritingsUpdateController', AutomatedWritingsUpdateController);

        AutomatedWritingsUpdateController.$inject = ['$state', 'UserDefinedTxService', 'FinancialPeriodService', 'MaterializedPathLookupService', 'NotifierService',
        'AccountingTransactionService', 'AutomatedWritingsServices', '$q', 'companyDetail', 'template'];

    /* @ngInject */
    function AutomatedWritingsUpdateController($state, UserDefinedTxService, FinancialPeriodService, MaterializedPathLookupService, NotifierService, AccountingTransactionService,
        AutomatedWritingsServices, $q, companyDetail, template) {
        var vm = this;
        vm.companyId = $state.params.companyId;
        vm.doSave = doSave;
        vm.loading = true;
        vm.checkOwnerAccountIsDefined = checkOwnerAccountIsDefined;
        activate();

        ////////////////

        function activate() {
            vm.companyDetail = companyDetail;
            vm.isUpdate = true;
            vm.template = new Object();
            vm.template = template;
            vm.toGiveList =  new Array();
            vm.toHaveList =  new Array();

            angular.forEach(template.details, function (detail) 
            {   
          
                var value = new Object();
                var isSottoconto = !detail.materializedPath.endsWith(".*");
                value.rootPath = undefined;
                value.rootPathIsDefined = false;
                var materializedPath = isSottoconto ?
                         detail.materializedPath : detail.materializedPath.substring(0,detail.materializedPath.indexOf('.*'));

                value.selectedAccount = new Object();
                value.selectedAccount.materializedPath = materializedPath;

                value.selectedAccount.level = isSottoconto ? "SOTTOCONTO": "GRUPPO"; //workaround
                value.percentage  =  detail.amountType === 'PERCENTAGE';
                value.coefficient =  value.percentage ? detail.amountValue : 0;
                value.amount      = !value.percentage ? detail.amountValue : 0;
           
                if (detail.type === 'TO_GIVE')
                    vm.toGiveList.push(value);
                else    
                    vm.toHaveList.push(value);
            });

            vm.loading = false;
        }



        /**
         * Search on toGiveList and TohaveList for a item with ownerCompanyId valorized
         * return true if ownerId is defined , else otherwise
         * @param {*} toGive 
         * @param {*} toHave 
         */
        function checkOwnerAccountIsDefined(toGiveList, toHaveLIst) {
            var ownerId =  _.head(_.flatMap(_.filter(_.union(toGiveList, toHaveLIst), function(o) { 
               return angular.isDefined(o.selectedAccount) && o.selectedAccount.ownerCompanyId !=null                
            }),'selectedAccount.ownerCompanyId')); 

            vm.template.ownerCompanyId = ownerId;
            return angular.isDefined( vm.template.ownerCompanyId);
       }    

        function doSave() {
            vm.template.details = new Array(); 
            
            angular.forEach(vm.toGiveList, function (value) {
                vm.template.details.push(mapDetail('TO_GIVE', value));
                if (value.ownerCompanyId != null)
                    vm.template.ownerCompanyId = value.ownerCompanyId;
            });

            angular.forEach(vm.toHaveList, function (value) {
                vm.template.details.push(mapDetail('TO_HAVE', value));

                if (value.ownerCompanyId != null)
                vm.template.ownerCompanyId = value.ownerCompanyId;
            });

            return AutomatedWritingsServices.update(vm.companyDetail.id, vm.template.id, vm.template).then(function () {
                return NotifierService.notifySuccess('AutomatedWritings.Update.Success').then(function () {
                    return $state.go('^', { inherits: true });
                });
            })
                .catch(function (err) {
                    NotifierService.notifyError('AutomatedWritings.Update.Failure', err);
                    return $q.reject(err);
                });
        }
 

        function mapDetail(type, value){
            var detail = {};
            detail.type = type;
            
            if (value.selectedAccount.level === 'GRUPPO' || value.selectedAccount.level === 'MASTRO' ||
                value.selectedAccount.level === 'CONTO' ){
                detail.materializedPath = value.selectedAccount.materializedPath + '.*';                         
            }else
                    detail.materializedPath = value.selectedAccount.materializedPath;                    

            detail.amountType =  value.percentage ? "PERCENTAGE" : "FIXED";
            detail.amountValue = value.percentage ? value.coefficient : value.amount;
            return detail;
        }


    }

})();

