"use strict";

angular.module("condominiumManagementApp.dashboard")
	.config( function ($translatePartialLoaderProvider) {
		$translatePartialLoaderProvider.addPart("dashboard");
	});
