"use strict";

angular.module("condominiumManagementApp.dashboard", [
    'condominiumManagementApp.core',
    'condominiumManagementApp.commons'
]);
