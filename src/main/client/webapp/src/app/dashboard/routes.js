'use strict';
angular.module("condominiumManagementApp.dashboard").config(function ($stateProvider) {
	$stateProvider.state('index.dashboard', {
		url: "/{condominiumId}/dashboard",
		templateUrl: "app/dashboard/partials/dashboard.html",
		controller: "DashboardCtrl",
		data: { pageTitle: 'Dashboard' }
	});
});