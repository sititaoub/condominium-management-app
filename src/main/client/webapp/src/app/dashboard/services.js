"use strict";
angular.module("condominiumManagementApp.dashboard")
    .service("DashboardService", ['$http', 'contextAddress', function ($http, contextAddress) {
        return {

            getList: function (status) {
                return $http({
                    method: 'GET',
                    url: contextAddress + 'v1/iot-plants',
                    params: {
                        statusId: status,
                        size: 500
                    }
                });

            }
        }
    }]);



