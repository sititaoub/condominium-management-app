'use strict';

angular.module('condominiumManagementApp.dashboard')
      .controller('DashboardCtrl', [
            "$scope",
            "$state",
            "$timeout",
            "DTOptionsBuilder",
            "DTColumnBuilder",
            "toaster",
            "DashboardService",
            "TableService",
            function ($scope, $state, $timeout, DTOptionsBuilder, DTColumnBuilder, toaster,
                  DashboardService,
                  TableService) {

                  TableService.getTableOptions($scope);

                  /**
                   * Flot chart data and options
                   */
                  var d1 = [[1262304000000, 6], [1264982400000, 3057], [1267401600000, 20434], [1270080000000, 31982], [1272672000000, 26602], [1275350400000, 27826], [1277942400000, 24302], [1280620800000, 24237], [1283299200000, 21004], [1285891200000, 12144], [1288569600000, 10577], [1291161600000, 10295]];
                  var d2 = [[1262304000000, 5], [1264982400000, 200], [1267401600000, 1605], [1270080000000, 6129], [1272672000000, 11643], [1275350400000, 19055], [1277942400000, 30062], [1280620800000, 39197], [1283299200000, 37000], [1285891200000, 27000], [1288569600000, 21000], [1291161600000, 17000]];

                  var flotChartData1 = [
                        { label: "Data 1", data: d1, color: '#17a084' },
                        { label: "Data 2", data: d2, color: '#127e68' }
                  ];

                  var flotChartOptions1 = {
                        xaxis: {
                              tickDecimals: 0
                        },
                        series: {
                              lines: {
                                    show: true,
                                    fill: true,
                                    fillColor: {
                                          colors: [{
                                                opacity: 1
                                          }, {
                                                opacity: 1
                                          }]
                                    }
                              },
                              points: {
                                    width: 0.1,
                                    show: false
                              }
                        },
                        grid: {
                              show: false,
                              borderWidth: 0
                        },
                        legend: {
                              show: false
                        }
                  };

                  var flotChartData2 = [
                        { label: "Data 1", data: d1, color: '#19a0a1' }
                  ];

                  var flotChartOptions2 = {
                        xaxis: {
                              tickDecimals: 0
                        },
                        series: {
                              lines: {
                                    show: true,
                                    fill: true,
                                    fillColor: {
                                          colors: [{
                                                opacity: 1
                                          }, {
                                                opacity: 1
                                          }]
                                    }
                              },
                              points: {
                                    width: 0.1,
                                    show: false
                              }
                        },
                        grid: {
                              show: false,
                              borderWidth: 0
                        },
                        legend: {
                              show: false
                        }
                  };

                  var flotChartData3 = [
                        { label: "Data 1", data: d1, color: '#fbbe7b' },
                        { label: "Data 2", data: d2, color: '#f8ac59' }
                  ];

                  var flotChartOptions3 = {
                        xaxis: {
                              tickDecimals: 0
                        },
                        series: {
                              lines: {
                                    show: true,
                                    fill: true,
                                    fillColor: {
                                          colors: [{
                                                opacity: 1
                                          }, {
                                                opacity: 1
                                          }]
                                    }
                              },
                              points: {
                                    width: 0.1,
                                    show: false
                              }
                        },
                        grid: {
                              show: false,
                              borderWidth: 0
                        },
                        legend: {
                              show: false
                        }
                  };

                  /**
                   * Definition of variables
                   * Flot chart
                   */

                  $scope.flotChartData1 = flotChartData1;
                  $scope.flotChartOptions1 = flotChartOptions1;
                  $scope.flotChartData2 = flotChartData2;
                  $scope.flotChartOptions2 = flotChartOptions2;
                  $scope.flotChartData3 = flotChartData3;
                  $scope.flotChartOptions3 = flotChartOptions3;



                  //------------------------------------------


                  $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
                  $scope.series = ['Series A', 'Series B'];
                  $scope.data = [
                        [65, 59, 80, 81, 56, 55, 40],
                        [28, 48, 40, 19, 86, 27, 90]
                  ];

                  $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
                  $scope.options = {
                        scales: {
                              yAxes: [
                                    {
                                          id: 'y-axis-1',
                                          type: 'linear',
                                          display: true,
                                          position: 'left'
                                    },
                                    {
                                          id: 'y-axis-2',
                                          type: 'linear',
                                          display: true,
                                          position: 'right'
                                    }
                              ]
                        }
                  };



            }]);
