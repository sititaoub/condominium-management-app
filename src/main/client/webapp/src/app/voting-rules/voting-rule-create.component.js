/*
 * voting-rule-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.votingRules')
        .component('votingRuleCreate', {
            controller: VotingRuleCreateController,
            bindings: {
                'quorums': '<'
            },
            templateUrl: 'app/voting-rules/voting-rule-create.html'
        });

    VotingRuleCreateController.$inject = ['$state', 'VotingRuleService', 'NotifierService', '$q'];

    /* @ngInject */
    function VotingRuleCreateController($state, VotingRuleService, NotifierService, $q) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.votingRule = {};
        }

        function doSave() {
            return VotingRuleService.create($ctrl.votingRule).then(function () {
                return NotifierService.notifySuccess('VotingRules.Create.Success');
            }).then(function () {
                return $state.go('^', { inherits: true });
            }).catch(function (err) {
                NotifierService.notifyError('VotingRules.Create.Failure');
                return $q.reject(err);
            });
        }
    }

})();

