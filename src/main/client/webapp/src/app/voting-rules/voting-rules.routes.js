/*
 * voting-rules.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.votingRules')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider'];

    /* @ngInject */
    function setupRoutes ($stateProvider) {
        $stateProvider.state('index.voting-rules', {
            url: '/voting-rules',
            abstract: true,
            redirectTo: 'index.voting-rules.list',
            template: '<div class="voting-rules-container" ui-view></div>',
            data: {
                skipFinancialPeriodCheck: true
            }
        }).state('index.voting-rules.list', {
            url: '?page&count&sort&system&description',
            component: 'votingRulesList',
            params: {
                page: { dynamic: true, squash: true, value: '1', inherits: true },
                count: { dynamic: true, squash: true, value: '10', inherits: true },
                sort: { dynamic: true, squash: true, value: 'description,asc', inherits: true },
                system: { dynamic: true, inherits: true },
                description: { dynamic: true, inherits: true }
            }
        }).state('index.voting-rules.list.create', {
            url: '/create',
            views: {
                '@index.voting-rules': {
                    component: 'votingRuleCreate'
                }
            },
            resolve: {
                quorums: ['VotingRuleService', function (VotingRuleService) {
                    return VotingRuleService.availableQuorums();
                }]
            }
        }).state('index.voting-rules.list.edit', {
            url: '/{votingRuleId}/edit',
            views: {
                '@index.voting-rules': {
                    component: 'votingRuleEdit'
                }
            },
            resolve: {
                quorums: ['VotingRuleService', function (VotingRuleService) {
                    return VotingRuleService.availableQuorums();
                }],
                votingRule: ['$transition$', 'VotingRuleService', function ($transition$, VotingRuleService) {
                    return VotingRuleService.getOne($transition$.params().votingRuleId);
                }]
            }
        }).state('index.voting-rules.list.settings', {
            url: '/settings',
            onEnter: ['$state', '$transition$', '$uibModal', 'VotingRuleService', function ($state, $transition$, $uibModal, VotingRuleService) {
                $uibModal.open({
                    component: 'editSettings',
                    resolve: {
                        'settings': VotingRuleService.getSettings(),
                        '$transition$': function () { return $transition$; }
                    }
                }).result.finally(function () {
                    $state.go('^');
                });
            }]
        });
    }

})();