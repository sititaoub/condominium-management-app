/*
 * edit-settings.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.votingRules')
        .component('editSettings', {
            controller: EditSettingsController,
            bindings: {
                'close': '&',
                'dismiss': '&',
                'resolve': '<'
            },
            templateUrl: 'app/voting-rules/edit-settings.html'
        });

    EditSettingsController.$inject = ['VotingRuleService', 'NotifierService'];

    /* @ngInject */
    function EditSettingsController(VotingRuleService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.settings = $ctrl.resolve.settings;
        }

        function doSave() {
            VotingRuleService.updateSettings($ctrl.settings).then(function () {
                return NotifierService.notifySuccess('VotingRules.Settings.Edit.Success');
            }).then(function () {
                $ctrl.close();
            }).catch(function () {
                return NotifierService.notifyError('VotingRules.Settings.Edit.Failure');
            });
        }
    }

})();

