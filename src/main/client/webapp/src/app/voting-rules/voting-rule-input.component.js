/*
 * voting-rule-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.votingRules')
        .component('votingRuleInput', {
            controller: VotingRuleInputController,
            bindings: {
                ngModel: '<',
                quorums: '<',
                ngReadonly: '<?'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/voting-rules/voting-rule-input.html'
        });

    VotingRuleInputController.$inject = [];

    /* @ngInject */
    function VotingRuleInputController() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue || {
                    firstCallIntervened: 'NONE',
                    firstCallParticipating: 'NONE',
                    firstCallThousandths: 'NONE',
                    secondCallIntervened: 'NONE',
                    secondCallParticipating: 'NONE',
                    secondCallThousandths: 'NONE'
                };
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

