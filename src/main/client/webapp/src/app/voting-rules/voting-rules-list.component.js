/*
 * voting-rules-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.votingRules')
        .component('votingRulesList', {
            controller: VotingRulesListController,
            bindings: {
            },
            templateUrl: 'app/voting-rules/voting-rules-list.html'
        });

    VotingRulesListController.$inject = ['$state', 'NgTableParams', 'VotingRuleService', 'NotifierService', 'AlertService'];

    /* @ngInject */
    function VotingRulesListController($state, NgTableParams, VotingRuleService, NotifierService, AlertService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.toggleFilters = toggleFilters;
        $ctrl.deleteVotingRule = deleteVotingRule;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10,
                sort: 'description,asc'
            }, {
                getData: loadVotingRules
            });
            $ctrl.showFilters = $ctrl.tableParams.hasFilter();
        }

        function toggleFilters() {
            $ctrl.showFilters = !$ctrl.showFilters;
        }

        function deleteVotingRule(votingRule) {
            AlertService.askConfirm('VotingRules.Remove.Message', votingRule).then(function () {
                VotingRuleService.remove(votingRule.id).then(function () {
                    return NotifierService.notifySuccess('VotingRules.Remove.Success');
                }).catch(function () {
                    return NotifierService.notifyError('VotingRules.Remove.Failure');
                }).finally(function () {
                    $ctrl.tableParams.reload();
                })
            });
        }

        function loadVotingRules(params) {

            $state.go('.', params.uiRouterUrl(), { inherit: false });
            return VotingRuleService.getPage(params.page() - 1, params.count(), params.sorting(), params.filter())
                .then(function (data) {
                    params.total(data.totalElements);
                    return data.content;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    $ctrl.loading = false;
                });
        }
    }

})();

