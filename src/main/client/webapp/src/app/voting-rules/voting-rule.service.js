/*
 * voting-rule.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.votingRules')
        .factory('VotingRuleService', VotingRuleService);

    VotingRuleService.$inject = ['Restangular', 'contextAddressMeetings', '_'];

    /* @ngInject */
    function VotingRuleService(Restangular, contextAddressMeetings, _) {
        var VotingRules = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressMeetings + 'v1');
            configurer.addResponseInterceptor(function (data, operation, what, url, response) {
                if (operation === "post") {
                    var location = response.headers('Location');
                    return location.substr(location.lastIndexOf('/') + 1);
                }
                return data;
            });
        }).all('voting-rules');

        var service = {
            getPage: getPage,
            getAll: getAll,
            getOne: getOne,
            availableQuorums: availableQuorums,
            create: create,
            update: update,
            remove: remove,
            getSettings: getSettings,
            updateSettings: updateSettings,
            getConstitutionRule: getConstitutionRule,
            availableYesNoBehaviors: availableYesNoBehaviors,
            availableThousandthsRatios: availableThousandthsRatios,
            evaluateRule: evaluateRule
        };
        return service;

        ////////////////

        /**
         * @namespace VotingRules
         */

        /**
         * Managers page.
         *
         * @typedef {Object} VotingRules~VotingRulesPage
         * @property {number} number - The requested page number.
         * @property {number} size - The requested page size.
         * @property {number} totalElements - The total number of elements.
         * @property {number} totalPages - The total number of pages.
         * @property {VotingRules~VotingRule[]} content - The page content.
         */

        /**
         * VotingRule object.
         *
         * @typedef {Object} VotingRules~VotingRule
         * @property {number} id - The unique id of voting rule.
         * @property {boolean} system - True if the voting rule is a system one.
         * @property {string} description - Textual description of voting rule.
         * @property {string} firstCallIntervened - The first call quorum of intervened people.
         * @property {string} firstCallParticipating - The first call quorum of condominium participating people.
         * @property {string} firstCallThousandths - The first call quorum for thousandths.
         * @property {string} secondCallIntervened - The second call quorum of intervened people.
         * @property {string} secondCallParticipating - The second call quorum of condominium participating people.
         * @property {string} secondCallThousandths - The second call quorum for thousandths.
         */

        /**
         * Settings object
         *
         * @typedef {Object} VotingRules~Settings
         * @property {string} yesVoteBehavior - The yes/no vote behavior.
         * @property {boolean} thousandthsVoteBehaviorPresent - True if the following field value is significant.
         * @property {string} thousandthsVoteBehavior - The yes/no vote behavior on thousandths.
         * @property {string} thousandthsRatio - The ratio to consider
         */

        /**
         * Retrieve a single page of voting rules.
         *
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=10] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<VotingRules~VotingRule|Error>} - The promise of page.
         */
        function getPage(page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return VotingRules.get("", params);
        }

        /**
         * Get the first 500 voting rules.
         *
         * @returns {Promise<VotingRules~VotingRule[]|Error>} - The promise of list
         */
        function getAll() {
            return getPage(0, 1000).then(function (page) {
                return page.content;
            });
        }

        /**
         * Retrieve a single voting rule.
         *
         * @param {number} id - The unique voting rule id.
         * @returns {Promise<VotingRules~VotingRule|Error>} - The promise of page.
         */
        function getOne(id) {
            return VotingRules.one("", id).get();
        }

        /**
         * Retrieve a list of available quorums types.
         *
         * @return {Promise<String[]|Error>} - The promise of list of quorums.
         */
        function availableQuorums() {
            return VotingRules.all("quorums").getList();
        }

        /**
         * Create a new voting rule.
         *
         * @param {VotingRules~VotingRule} votingRule - The voting rule to be created.
         * @returns {Promise<Number|Error>} - The promise of result.
         */
        function create(votingRule) {
            return VotingRules.post(votingRule);
        }

        /**
         * Updates an existing voting rule.
         *
         * @param {number} id - The unique id of voting rule to update.
         * @param {VotingRules~VotingRule} votingRule - The voting rule to be updated
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function update(id, votingRule) {
            return VotingRules.one("", id).customPUT(votingRule);
        }

        /**
         * Remove the voting rule with supplied id.
         *
         * @param {number} id - The unique id of voting rule
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function remove(id) {
            return VotingRules.one("", id).remove();
        }

        /**
         * Retrieve the current settings.
         *
         * @returns {Promise<VotingRules~Settings|Error>} - The promise of settings.
         */
        function getSettings() {
            return VotingRules.one("", "settings").get();
        }

        /**
         * Updates the settings.
         *
         * @param {VotingRules~Settings} settings - The settings.
         * @return {Promise<Object|Error>} - The promise of result
         */
        function updateSettings(settings) {
            return VotingRules.one("", "settings").customPUT(settings);
        }

        /**
         * Retrieve the constitution rule.
         *
         * @return {Promise<VotingRules~VotingRule|Error>} - The voting rule.
         */
        function getConstitutionRule() {
            return getSettings().then(function (settings) {
                return getOne(settings.constitutionVotingRuleId);
            });
        }

        /**
         * The list of all available yes/no behaviors.
         *
         * @return {Promise<string[]|Error>} - The promise of list
         */
        function availableYesNoBehaviors() {
            return VotingRules.all("settings").all('yes-no-behaviors').getList();
        }

        /**
         * The list of all available thousandths ratios behaviors.
         *
         * @return {Promise<string[]|Error>} - The promise of list
         */
        function availableThousandthsRatios() {
            return VotingRules.all("settings").all('thousandths-ratios').getList();
        }

        /**
         * Evaluate the supplied rule.
         *
         * @param {VotingRules~VotingRule} rule - The rule to evaluate.
         * @param {number} call - The number of the call (0 or 1).
         * @param {string} type - The type of quorum to evaluate ('intervened', 'participating', 'thousandths')
         * @param {number} actual - The actual value to compare.
         * @param {number} total - The total value to compare.
         * @return {boolean} - True if the rule is successfully evaluated and false if it's not
         */
        function evaluateRule(rule, call, type, actual, total) {
            var propertyName = (call === 0 ? 'first' : 'second') + 'Call' + _.upperFirst(type);
            var ruleType = rule[propertyName];
            switch (ruleType) {
                case 'NONE':
                    return true;
                case 'GE_ONE_THIRD':
                    return actual >= (total / 3);
                case 'GT_ONE_THIRD':
                    return actual > (total / 3);
                case 'GE_HALF':
                    return actual >= (total / 2);
                case 'GT_HALF':
                    return actual > (total / 2);
                case 'GE_TWO_THIRD':
                    return actual >= (total * 2 / 3);
                case 'GT_TWO_THIRD':
                    return actual > (total * 2 / 3);
                case 'ALL':
                    return actual === total;
                case 'ZERO':
                    return actual >= 0;
                case 'GE_FOUR_FIFTH':
                    return actual >= (total * 4 / 5);
                default:
                    return false;
            }
        }
    }

})();

