/*
 * settings-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.votingRules')
        .component('settingsInput', {
            controller: SettingsInputComponent,
            bindings: {
                ngModel: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/voting-rules/settings-input.html'
        });

    SettingsInputComponent.$inject = ['VotingRuleService'];

    /* @ngInject */
    function SettingsInputComponent(VotingRuleService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            VotingRuleService.availableYesNoBehaviors().then(function (behaviors) {
                $ctrl.yesNoBehaviors = behaviors;
            });
            VotingRuleService.availableThousandthsRatios().then(function (ratios) {
                $ctrl.thousandthsRatios = ratios;
            });
            VotingRuleService.getAll().then(function (votingRules) {
                $ctrl.votingRules = votingRules;
            });
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

