/*
 * voting-rule-select.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.votingRules')
        .component('votingRuleSelect', {
            controller: VotingRuleSelectController,
            bindings: {
                id: '@?',
                name: '@?',
                ngModel: '<',
                ngRequired: '<?',
                ngDisabled: '<?'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/voting-rules/voting-rule-select.html'
        });

    VotingRuleSelectController.$inject = ['VotingRuleService'];

    /* @ngInject */
    function VotingRuleSelectController(VotingRuleService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            loadVotingRules();
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue || {};
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue($ctrl.value);
        }

        function loadVotingRules() {
            VotingRuleService.getAll().then(function (votingRules) {
                $ctrl.votingRules = votingRules;
            });
        }
    }

})();

