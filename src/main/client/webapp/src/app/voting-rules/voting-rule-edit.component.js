/*
 * voting-rule-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.votingRules')
        .component('votingRuleEdit', {
            controller: VotingRuleEditController,
            bindings: {
                'votingRule': '<',
                'quorums': '<'
            },
            templateUrl: 'app/voting-rules/voting-rule-edit.html'
        });

    VotingRuleEditController.$inject = ['$state', 'VotingRuleService', 'NotifierService', '$q'];

    /* @ngInject */
    function VotingRuleEditController($state, VotingRuleService, NotifierService, $q) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.votingRuleId = $ctrl.votingRule.id;
        }

        function doSave() {
            return VotingRuleService.update($ctrl.votingRuleId, $ctrl.votingRule).then(function () {
                return NotifierService.notifySuccess('VotingRules.Create.Success');
            }).then(function () {
                return $state.go("^");
            }).catch(function (err) {
                NotifierService.notifyError('VotingRules.Create.Failure');
                return $q.reject(err);
            });
        }
    }

})();

