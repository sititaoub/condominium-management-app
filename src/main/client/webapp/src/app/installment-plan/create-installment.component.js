/*
 * create-installment.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.installmentPlan')
        .component('createInstallment', {
            controller: CreateInstallmentController,
            bindings: {
                close: '&',
                dismiss: '&',
                resolve: '<'
            },
            templateUrl: 'app/installment-plan/create-installment.html'
        });

    CreateInstallmentController.$inject = ['$q', '$state', 'InstallmentPlanService', 'NotifierService'];

    /* @ngInject */
    function CreateInstallmentController($q, $state, InstallmentPlanService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////////

        function onInit() {
            $ctrl.installment = undefined;
            $ctrl.financialPeriod = $ctrl.resolve.financialPeriod;
        }

        function doSave() {
            return InstallmentPlanService.addInstallment(
                $state.params.condominiumId,
                $state.params.periodId,
                $ctrl.installment
            ).then(function () {
                return NotifierService.notifySuccess('InstallmentPlan.CreateInstallment.SaveSuccess');
            }).then(function () {
                return $ctrl.close();
            }).catch(function () {
                NotifierService.notifyError('InstallmentPlan.CreateInstallment.SaveFailure');
                return $q.reject();
            });
        }
    }

})();

