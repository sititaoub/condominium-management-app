/*
 * installment-plan.module.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.installmentPlan', [
            'condominiumManagementApp.core',
            'condominiumManagementApp.commons',

            'condominiumManagementApp.financialPeriod',
            'condominiumManagementApp.budget'
        ]);

})();