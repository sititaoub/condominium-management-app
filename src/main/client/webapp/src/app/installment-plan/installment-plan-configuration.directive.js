/*
 * installment-plan-configuration.directive.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.installmentPlan')
        .directive('installmentPlanConfiguration', installmentPlanConfiguration);

    installmentPlanConfiguration.$inject = [];

    /* @ngInject */
    function installmentPlanConfiguration() {
        var directive = {
            bindToController: true,
            controller: InstallmentPlanConfigurationController,
            controllerAs: 'vm',
            restrict: 'E',
            scope: {
                condominiumId: '=',
                periodId: '=',
                ngModel: '=',
                loading: '=',
                totalAmount: '='
            },
            require: '^ngModel',
            templateUrl: 'app/installment-plan/installment-plan-configuration.html'
        };
        return directive;
    }

    InstallmentPlanConfigurationController.$inject = ['$filter', '$q', '$state', 'previousYearModes', 'distributionModes', 'amountRoundings', 'NgTableParams', 'InstallmentPlanService', 'FinancialPeriodService', 'NotifierService'];

    /* @ngInject */
    function InstallmentPlanConfigurationController($filter, $q, $state, previousYearModes, distributionModes, amountRoundings, NgTableParams, InstallmentPlanService, FinancialPeriodService, NotifierService) {
        var vm = this;

        var NUMBER_OF_INSTALLMENTS = 4;

        vm.recalculateAmounts = recalculateAmounts;
        vm.recalculateCoefficientSum = recalculateCoefficientSum;
        vm.recalculatePreviousYearSum = recalculatePreviousYearSum;
        vm.recalculateInstallments = recalculateInstallments;

        activate();

        ////////////////

        function activate() {
            vm.loading = true;
            vm.tableParams = new NgTableParams({
                page: 1,
                count: 10
            }, {});
            vm.previousYearModes = previousYearModes;
            vm.distributionModes = distributionModes;
            vm.amountRoundings = amountRoundings;

            loadConfiguration();
        }

        function loadConfiguration() {
            InstallmentPlanService.getConfiguration(vm.condominiumId, vm.periodId).then(function (configuration) {
                angular.copy(configuration, vm.ngModel);
                return $q.resolve();
            }).catch(function (response) {
                if (response.status !== 404) {
                    NotifierService.notifyError('InstallmentPlan.Configuration.LoadingFailure');
                }
                vm.ngModel = {
                    condominiumId: vm.condominiumId,
                    periodId: vm.periodId
                };
                return $q.resolve();
            }).then(function () {
                return FinancialPeriodService.getOne($state.params.companyId, vm.periodId).then(function (period) {
                    vm.currentPeriod = period;
                });
            }).then(function () {
                if (angular.isUndefined(vm.ngModel.numberOfInstallments)) {
                    createDefaultConfiguration();
                } else {
                    recalculatePreviousYearSum();
                    recalculateCoefficientSum();
                    recalculateAmounts();
                }
                vm.tableParams.settings({ dataset: vm.ngModel.installments });
            }).finally(function () {
                vm.loading = false;
            });
        }

        function createDefaultConfiguration() {
            vm.ngModel.condominiumId = vm.condominiumId;
            vm.ngModel.financialPeriodId = vm.periodId;
            vm.ngModel.numberOfInstallments = NUMBER_OF_INSTALLMENTS;
            vm.ngModel.distributionMode = 'Coefficient';
            vm.ngModel.roundAmounts = false;
            vm.ngModel.roundAmountsTo = 100;
            vm.ngModel.mergeAmountsLower = 0;
            vm.ngModel.previousYearMode = 'First';
            vm.ngModel.installments = generateInstallments();
            vm.ngModel.roundingInstallment = "" + (vm.ngModel.installments.length - 1);
        }

        function generateInstallments() {
            var startDate = moment(vm.currentPeriod.period.startDate).utc(),
                endDate = moment(vm.currentPeriod.period.endDate).utc(),
                periodDays = endDate.diff(startDate, 'days') + 1,
                numberOfInstallments = vm.ngModel.numberOfInstallments + (vm.ngModel.previousYearMode === 'BeforeFirst' ? 1 : 0),
                installmentPeriod = Math.floor(periodDays / numberOfInstallments);

            var installments = _.map(_.range(0, numberOfInstallments), function (i, index) {
                var expiration = moment(startDate).add(installmentPeriod * i, 'days');
                return {
                    expiration: moment.min(expiration, endDate).toDate(),
                    description: $filter('translate')('InstallmentPlan.Configuration.InstallmentNumber', { number: i + 1 }),
                    previousYear: (function (mode) {
                        switch(mode) {
                            case 'First':
                            case 'BeforeFirst':
                                return i === 0 ? 100 : 0;
                            case 'Distributed':
                                return Math.round(100 / numberOfInstallments);
                            default:
                                return 0;
                        }
                    })(vm.ngModel.previousYearMode),
                    coefficient: (function (mode, previousYearMode) {
                        if (previousYearMode === 'BeforeFirst' && index === 0) {
                            return 0;
                        }
                        switch (mode) {
                            case 'Percent':
                                return Math.round(100 / vm.ngModel.numberOfInstallments);
                            case 'Proportional':
                            case 'Coefficient':
                                return 1;
                        }
                    })(vm.ngModel.distributionMode, vm.ngModel.previousYearMode)
                };
            });

            vm.previousYearSum = _.sumBy(installments, 'previousYear');
            vm.coefficientSum = _.sumBy(installments, 'coefficient');
            // Adjust last row.
            if (vm.ngModel.previousYearMode === 'Distributed') {
                // Adjust percentage
                installments[installments.length - 1].previousYear += 100 - vm.previousYearSum;
            }
            if (vm.ngModel.distributionMode === 'Percent') {
                // Adjust percentage
                installments[installments.length - 1].coefficient += 100 - vm.coefficientSum;
                vm.coefficientSum = 100;
            }
            _.forEach(installments, function (el) {
                el.amount = vm.totalAmount * el.coefficient / vm.coefficientSum;
            });
            var totalAmount = _.sumBy(installments, 'amount');
            installments[installments.length - 1].amount += vm.totalAmount - totalAmount;

            return installments;
        }

        function recalculateInstallments() {
            vm.ngModel.installments = generateInstallments();
            vm.ngModel.numberOfInstallments = vm.ngModel.installments.length - (vm.ngModel.previousYearMode === 'BeforeFirst' ? 1 : 0);
            vm.ngModel.roundingInstallment = "" + (vm.ngModel.installments.length - 1);
            vm.tableParams.settings({ dataset: vm.ngModel.installments });
        }

        function recalculatePreviousYearSum() {
            vm.previousYearSum = _.sumBy(vm.ngModel.installments, 'previousYear');
            vm.form.previousYearSum.$setDirty();
        }

        function recalculateCoefficientSum() {
            vm.coefficientSum = _.sumBy(vm.ngModel.installments, 'coefficient');
            vm.form.coefficientSum.$setDirty();
        }

        function recalculateAmounts() {
            _.forEach(vm.ngModel.installments, function (el) {
                el.amount = vm.totalAmount * el.coefficient / vm.coefficientSum;
            });
            var totalAmount = _.sumBy(vm.ngModel.installments, 'amount');
            vm.ngModel.installments[vm.ngModel.installments.length - 1].amount += vm.totalAmount - totalAmount;
        }
    }

})();

