/*
 * installment-plan-edit.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.installmentPlan')
        .controller('InstallmentPlanEditController', InstallmentPlanEditController);

    InstallmentPlanEditController.$inject = ['$state', 'FinancialPeriodService', 'InstallmentPlanService', 'NotifierService'];

    /* @ngInject */
    function InstallmentPlanEditController($state, FinancialPeriodService, InstallmentPlanService, NotifierService) {
        var vm = this;

        vm.doSave = doSave;
        vm.doGenerate = doGenerate;

        activate();

        ////////////////

        function activate() {
            vm.budgetLoading = true;
            vm.configurationLoading = true;
            vm.condominiumId = $state.params.condominiumId;
            vm.periodId = $state.params.periodId;

            vm.configuration = {};

            vm.doPrintConfirm=doPrintConfirm;

            ensurePeriodId();
        }

        function ensurePeriodId() {
            if ($state.params.periodId === 'current') {
                var periodId = FinancialPeriodService.getCurrent(undefined, $state.params.condominiumId).id;
                $state.go('.', { periodId: periodId });
            }
        }

        /**
         * Perform the save of configuration, doing nothing but staying on the same page.
         */
        function doSave() {
            InstallmentPlanService.updateConfiguration(vm.condominiumId, vm.periodId, vm.configuration).then(function () {
                return NotifierService.notifySuccess('InstallmentPlan.Edit.SaveSuccess');
            }).catch(function () {
                NotifierService.notifyError('InstallmentPlan.Edit.SaveFailure');
            });
        }

        /**
         * Perform the save of configuration and perform the generation.
         */
        function doGenerate() {
            InstallmentPlanService.updateConfiguration(vm.condominiumId, vm.periodId, vm.configuration).then(function () {
                vm.generating = true;
                return InstallmentPlanService.create(vm.condominiumId, vm.periodId, vm.configuration);
            }).then(function () {
                return NotifierService.notifySuccess('InstallmentPlan.Edit.GenerationSuccess');
            }).then(function () {
                $state.go('^.view');
            }).catch(function () {
                NotifierService.notifyError('InstallmentPlan.Edit.GenerationFailure');
            }).finally(function () {
                vm.generating = false;
            });

        }

        function doPrintConfirm(printType, outputFormat) {
            var param={};
            param.condominiumId=$state.params.condominiumId;
            param.financialPeriodId=$state.params.periodId;
            param.divideByUnit=vm.printOption.divideByUnit;
            param.outputFormat=outputFormat;
            InstallmentPlanService.getReport(printType,param);
        }
    }

})();

