/*
 * installment-plan.constants.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.installmentPlan')
        .constant('previousYearModes', {
            First: 'First',
            BeforeFirst: 'BeforeFirst',
            Distributed: 'Distributed'
        })
        .constant('distributionModes', {
            Percent: 'Percent',
            Proportional: 'Proportional',
            Coefficient: 'Coefficient'
        })
        .constant('amountRoundings', [ 10, 100, 500, 1000 ])
        .constant('installmentTypes', {
            Split: 'Split',
            Assigned: 'Assigned',
            Balance: 'Balance'
        });

})();
