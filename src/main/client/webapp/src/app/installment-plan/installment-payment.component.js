/*
 * installment-plan-pay.controller.js
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';
    
    angular
        .module('condominiumManagementApp.installmentPlan')
        .component('installmentPayment', {
            controller: InstallmentPaymentController,
            bindings: {
                condominium: '<',
                financialPeriod: '<',
                installmentPlan: '<'
            },
            templateUrl: 'app/installment-plan/installment-payment.html'
        });

    InstallmentPaymentController.$inject = ['$state', '$filter', '$uibModal', 'NgTableParams', '_', 'moment'];

    function InstallmentPaymentController($state, $filter, $uibModal, NgTableParams, _, moment) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onPersonChange = onPersonChange;
        $ctrl.isPaidFully = isPaidFully;
        $ctrl.isPaidPartially = isPaidPartially;
        $ctrl.isExpired = isExpired;
        $ctrl.recalculatePaymentAmount = recalculatePaymentAmount;
        $ctrl.performPayment = performPayment;

        ////////////////

        function onInit() {
            $ctrl.payment = { amount: 0 };
            initPeople();
            initInstallments();
        }

        function onPersonChange() {
            $ctrl.installments = _.filter($ctrl.installmentPlan.people, { personId: $ctrl.payment.personId });
            $ctrl.tableParams.settings({
                dataset: $ctrl.installments
            });
            $ctrl.tableParams.reload();
        }

        function isPaidFully(row, column) {
            if (column.index >= 0) {
                var installment = row.installments[column.index];
                return installment.paid === installment.amount;
            }
            return false;
        }

        function isPaidPartially(row, column) {
            if (column.index >= 0) {
                var installment = row.installments[column.index];
                return installment.paid > 0 && installment.paid < installment.amount;
            }
            return false;
        }

        function isExpired(row, column) {
            if (column.index >= 0 && angular.isDefined(row.installments)) {
                var installment = row.installments[column.index];
                return moment(installment.expiration).isBefore() && installment.paid < installment.amount;
            }
            return false;
        }

        function recalculatePaymentAmount() {
            $ctrl.payment.amount = _.sumBy(_.filter(_.flatMap($ctrl.installments, 'installments'), function (i) {
                return i.selected;
            }), function (i) {
                return i.amount - i.paid;
            });
        }

        function performPayment() {
            console.log($ctrl.materializedPath);
            $uibModal.open({
                component: 'performPayment',
                resolve: {
                    condominium: _.constant($ctrl.condominium),
                    financialPeriod: _.constant($ctrl.financialPeriod),
                    installments: getSelectedInstallments,
                    personId: _.constant($ctrl.payment.personId),
                    totalAmount: _.constant($ctrl.payment.amount)
                }
            }).result.then(function () {
                $state.go('^', undefined, { reload: true });
            });
        }

        function getSelectedInstallments() {
            return _.map(_.filter(_.flatMap($ctrl.installments, 'installments'), { selected: true }), 'id');
        }

        function initPeople() {
            $ctrl.people = _.sortBy(_.map(_.groupBy($ctrl.installmentPlan.people, 'personId'), function (el) {
                return el[0];
            }), $filter('fullName'));
        }

        function initInstallments() {
            $ctrl.installments = [];
            $ctrl.grandTotal = 0;
            $ctrl.columns = createColumns($ctrl.installmentPlan);
            $ctrl.tableParams = new NgTableParams({
                page: 1,
                count: 10
            }, {
                dataset: $ctrl.installments,
                counts: []
            });
        }

        function createColumns(plan) {
            var columns = [{
                field: 'apartmentNumber',
                title: $filter('translate')('InstallmentPlan.Payment.HousingUnit'),
                show: true,
                kind: 'switch',
                headerClass: 'col-md-1'
            }];
            _.forEach(_.sortBy(plan.people[0].installments, 'expiration'), function (installment, index) {
                columns.push({
                    field: 'installments',
                    index: index,
                    title: installment.description + ' ' + $filter('date')(installment.expiration, 'shortDate'),
                    show: true,
                    kind: 'amount',
                    headerClass: 'col-md-1'
                });
            });
            return columns;
        }
    }
})();