/*
 * installment-plan.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.installmentPlan')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider'];

    /* @ngInject */
    function setupRoutes($stateProvider) {
        $stateProvider.state('index.installmentPlan', {
            url: '/condominiums/{condominiumId}/financial-period/{periodId}/installment-plan',
            redirectTo: 'index.installmentPlan.view',
            template: '<div class="installment-plan-wrapper" ui-view></div>',
            resolve: {
                condominium: ['$transition$', 'CondominiumService', function ($transition$, CondominiumService) {
                    return CondominiumService.getOne($transition$.params().condominiumId);
                }],
                financialPeriod: ['$transition$', 'FinancialPeriodService', function ($transition$, FinancialPeriodService) {
                    if ($transition$.params().periodId !== 'current') {
                        return FinancialPeriodService.getOne($transition$.params().companyId, $transition$.params().periodId);
                    } else {
                        return null;
                    }
                }]
            },
            onEnter: ['$state', '$transition$', 'FinancialPeriodService', function ($state, $transition$, FinancialPeriodService) {
                // Ensure that if 'current' is provided as financial period, the current financial period is provided.
                if ($transition$.params().periodId === 'current') {
                    var params = angular.copy($transition$.params());
                    params.periodId = FinancialPeriodService.getCurrent($transition$.params().companyId, $transition$.params().condominiumId).id;
                    return $state.target($transition$.targetState().name(), params);
                }
                return true;
            }]
        }).state('index.installmentPlan.view', {
            url: '/view',
            component: 'installmentPlanView',
            resolve: {
                installmentPlan: ['$transition$', 'InstallmentPlanService','financialPeriod',  function ($transition$, InstallmentPlanService, financialPeriod) {                    
                    if(financialPeriod.type ==='HISTORICAL' && financialPeriod.status ==='OPEN'){
                        return false;
                    }
                    return InstallmentPlanService.getOne(
                        $transition$.params().condominiumId,
                        $transition$.params().periodId
                    ).catch(function () {
                        // No installment plan exists.
                        return null;
                    });
                }]
            },
            onEnter: ['$state', '$transition$', 'installmentPlan','financialPeriod','NotifierService', function ($state, $transition$, installmentPlan,financialPeriod,NotifierService) {
               
                if(financialPeriod.type ==='HISTORICAL' && financialPeriod.status ==='OPEN'){
                    NotifierService.notifyError('InstallmentPlan.HISTORICAL_FINANCIAL_PERIOD_OPEN_ERROR');
                    return false;
                } 

                if (installmentPlan === null) {
                    var params = angular.copy($transition$.params());
                    return $state.target('index.installmentPlan.edit', params);
                }
                return true;
            }]
        }).state('index.installmentPlan.view.new', {
            url: '/create',
            onEnter: ['_', '$state', '$uibModal', 'financialPeriod', function (_, $state, $uibModal, financialPeriod) {
                $uibModal.open({
                    component: 'createInstallment',
                    resolve: {
                        financialPeriod: _.constant(financialPeriod)
                    }
                }).result.then(function () {
                    $state.go('^', undefined, { reload: true });
                }).catch(function () {
                    $state.go('^');
                });
                return true;
            }]
        }).state('index.installmentPlan.view.pay', {
            url: '/payment',
            views: {
                '@index.installmentPlan': {
                    component: 'installmentPayment',
                }
            }
        }).state('index.installmentPlan.edit', {
            url: '/edit',
            controller: 'InstallmentPlanEditController',
            controllerAs: 'vm',
            templateUrl: 'app/installment-plan/installment-plan-edit.html',
            onEnter: ['$state', '$transition$','financialPeriod','NotifierService', function ($state, $transition$,financialPeriod,NotifierService) {
               
                if(financialPeriod.type ==='HISTORICAL' && financialPeriod.status ==='OPEN'){
                    NotifierService.notifyError('InstallmentPlan.HISTORICAL_FINANCIAL_PERIOD_OPEN_ERROR');
                    return false;
                } else if(financialPeriod.status !='OPEN'){
                    NotifierService.notifyError('InstallmentPlan.FINANCIAL_PERIOD_CLOSE_OR_CONSOLIDATED');
                    return false;
                }
                
                return true;
            }]
        });
    }

})();
