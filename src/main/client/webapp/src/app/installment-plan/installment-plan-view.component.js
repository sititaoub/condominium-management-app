/*
 * installment-plan.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.installmentPlan')
        .component('installmentPlanView', {
            controller: InstallmentPlanController,
            bindings: {
                installmentPlan: '<'
            },
            templateUrl: 'app/installment-plan/installment-plan-view.html'
        });

    InstallmentPlanController.$inject = ['$state', '$filter', 'NgTableParams', 'InstallmentPlanService', 'NotifierService', 'AlertService','FinancialPeriodService', '_'];

    /* @ngInject */
    function InstallmentPlanController($state, $filter, NgTableParams, InstallmentPlanService, NotifierService, AlertService,FinancialPeriodService, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.uiCanExit = uiCanExit;
        $ctrl.approve = approve;
        $ctrl.unapprove = unapprove;
        $ctrl.sumTotals = _.sumBy;
        $ctrl.sumInstallments = sumInstallments;
        $ctrl.sumGrandTotals = sumGrandTotals;
        $ctrl.isPaidFully = isPaidFully;
        $ctrl.isPaidPartially = isPaidPartially;
        $ctrl.isExpired = isExpired;
        $ctrl.openPrintOption=openPrintOption;

        ////////////////

        function onInit() {
            $ctrl.tableParams = new NgTableParams({
                page: 1,
                count: 9999,
                group: 'personId'
            }, {
                counts: [],
                groupOptions: {
                    isExpanded: false
                }
            });

            initInstallmentPlan();
        }

        /**
         * Angular-UI router callback used to check if it's possible to exit current state.
         *
         * @param {Transition} transition - The transition object
         * @returns {boolean|Promise<any|Error>} - A promise that it's rejected if transition has to be stop and resolved if must continue
         */
        function uiCanExit(transition) {
            if (transition.targetState().name() === 'index.installmentPlan.edit') {
                return AlertService.askConfirm('InstallmentPlan.View.EditConfirm');
            }
            return true;
        }

        /**
         * Approve the current installment plan.
         */
        function approve() {
            AlertService.askConfirm('InstallmentPlan.View.ApproveWarning').then(function () {
                InstallmentPlanService.approve($state.params.condominiumId, $state.params.periodId).then(function () {
                    return NotifierService.notifySuccess('InstallmentPlan.View.ApproveSuccess');
                }).then(function () {
                    $state.go(".", {}, { reload: true });
                }).catch(function () {
                    NotifierService.notifyError('InstallmentPlan.View.ApproveFailure');
                });
            });
        }

        /**
         * Unapprove the current installment plan.
         */
        function unapprove() {
            AlertService.askConfirm('InstallmentPlan.View.UnapproveWarning').then(function () {
                InstallmentPlanService.unapprove($state.params.condominiumId, $state.params.periodId).then(function () {
                    return NotifierService.notifySuccess('InstallmentPlan.View.UnapproveSuccess');
                }).then(function () {
                    $state.go(".", {}, { reload: true });
                }).catch(function () {
                    NotifierService.notifyError('InstallmentPlan.View.UnapproveFailure');
                });
            });
        }

        function sumInstallments(data, field, index, property) {
            return _.sumBy(_.map(data, function (el) {
                return el[field][index];
            }), property);
        }

        function sumGrandTotals(groups, property) {
            return _.sumBy(_.flatMap(groups, 'data'), property);
        }

        function isPaidFully(row, column) {
            if (column.index >= 0) {
                if (angular.isDefined(row.data)) {
                    if (row.data.length === 1) {
                        return isPaidFully(row.data[0], column);
                    } else {
                        var paid = sumInstallments(row.data, column.field, column.index, 'paid'),
                            amount = sumInstallments(row.data, column.field, column.index, 'amount');
                        return paid === amount;
                    }
                }
                if (angular.isDefined(row.installments)) {
                    var installment = row.installments[column.index];
                    return installment.paid === installment.amount;
                }
            }
            return false;
        }

        function isPaidPartially(row, column) {
            if (angular.isDefined(row.data)) {
                if (row.data.length === 1) {
                    return isPaidPartially(row.data[0], column);
                } else {
                    var paid = sumInstallments(row.data, column.field, column.index, 'paid'),
                        amount = sumInstallments(row.data, column.field, column.index, 'amount');
                    return paid > 0 && paid < amount;
                }
            }
            if (column.index >= 0 && angular.isDefined(row.installments)) {
                var installment = row.installments[column.index];
                return installment.paid > 0 && installment.paid < installment.amount;
            }
            return false;
        }

        function isExpired(row, column) {
            if (angular.isDefined(row.data)) {
                if (row.data.length === 1) {
                    return isExpired(row.data[0], column);
                } else {
                    var paid = sumInstallments(row.data, column.field, column.index, 'paid'),
                        amount = sumInstallments(row.data, column.field, column.index, 'amount');
                    return row.data[0].expired && paid < amount;
                }
            }
            if (column.index >= 0 && angular.isDefined(row.installments)) {
                var installment = row.installments[column.index];
                return installment.expired && installment.paid < installment.amount;
            }
            return false;
        }

        function initInstallmentPlan() {
            FinancialPeriodService.getOne($state.params.companyId, $state.params.periodId).then(function(period){
                $ctrl.financialPeriod=period;
            }).catch(function(){
                NotifierService.notifyError();
            });
            $ctrl.plan = $ctrl.installmentPlan;
            $ctrl.columns = createColumns($ctrl.plan);
            $ctrl.tableParams.settings({
                dataset: _.map(_.flatMap($ctrl.plan.people, function (person) {
                    return _.defaults({
                        fullName: person.companyName || (person.firstName + ' ' + person.lastName),
                        flatName: person.apartmentNumber, // + '/' + person.stair + person.floor,
                        totalAmount: _.sumBy(person.installments, 'amount'),
                        totalPaid: calulateTotalPaid(person),
                        totalExpired: _.sumBy(_.filter(person.installments, function (el) {
                            return el.expired;
                        }), function (el) {
                            return el.amount - el.paid;
                        })
                    }, person);
                }), function (el) {
                    el.balance = el.totalAmount - el.totalPaid ;
                    return el;
                })
            });
        }
        
        function calulateTotalPaid(person){
        	var totalPaid=_.sumBy(person.installments, 'paid');
        	return totalPaid+ person.extraPaid;
        }
        
        function createColumns(plan) {
            var columns = [{
                field: 'fullName',
                title: $filter('translate')('InstallmentPlan.View.PersonName'),
                show: true,
                groupable: 'fullName',
                kind: 'text',
                headerClass: 'col-md-3'
            }, {
                field: 'flatName',
                title: $filter('translate')('InstallmentPlan.View.HousingUnit'),
                show: true,
                groupable: 'flatName',
                kind: 'switch',
                headerClass: 'col-md-1'
            }];
            _.forEach(_.sortBy(plan.people[0].installments, 'expiration'), function (installment, index) {
                columns.push({
                    field: 'installments',
                    index: index,
                    title: installment.description + ' ' + $filter('date')(installment.expiration, 'shortDate'),
                    show: true,
                    kind: 'amount',
                    headerClass: 'col-md-1'
                });
            });
            columns.push({
                field: 'totalAmount',
                title: $filter('translate')('InstallmentPlan.View.InstallmentsTotalAmount'),
                show: true,
                kind: 'amount',
                headerClass: 'col-md-1'
            }, {
                field: 'totalPaid',
                title: $filter('translate')('InstallmentPlan.View.InstallmentsTotalPaid'),
                show: true,
                kind: 'amount',
                headerClass: 'col-md-1'
            }, {
                field: 'balance',
                title: $filter('translate')('InstallmentPlan.View.InstallmentsBalance'),
                show: true,
                kind: 'amount',
                headerClass: 'col-md-1'
            }, {
                field: 'totalExpired',
                title: $filter('translate')('InstallmentPlan.View.InstallmentsExpired'),
                show: true,
                kind: 'amount',
                headerClass: 'col-md-1'
            });
            return columns;
        }

        function openPrintOption(){
            InstallmentPlanService.open({
                condominiumId: $state.params.condominiumId,
                periodId: $state.params.periodId
            }, 
            'app/installment-plan/print-instalment-plan-options.html');
        }

    }

})();

