/*
 * installment-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.installmentPlan')
        .component('installmentInput', {
            controller: InstallmentInputController,
            bindings: {
                ngModel: '<',
                financialPeriod: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/installment-plan/installment-input.html'
        });

    InstallmentInputController.$inject = ['installmentTypes', 'moment'];

    /* @ngInject */
    function InstallmentInputController(installmentTypes, moment) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.$onChanges = onChanges;
        $ctrl.onChange = onChange;
        $ctrl.isSplit = isSplit;
        $ctrl.isBalance = isBalance;

        ////////////

        function onInit() {
            $ctrl.rootAccount = '03.001';
            $ctrl.installmentTypes = installmentTypes;
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue || { kind: installmentTypes[0] };
            };
        }

        function onChanges(changes) {
            if (angular.isDefined(changes.financialPeriod) && angular.isDefined(changes.financialPeriod.currentValue)) {
                $ctrl.expirationOptions = {
                    // Minimum date cannot be earlier of today or start of financial period.
                    minDate: moment.max(moment(changes.financialPeriod.currentValue.period.startDate), moment()).toDate(),
                    minMode: 'day',
                    // Maximum date cannot be later than financial period end date
                    maxDate: changes.financialPeriod.currentValue.period.endDate,
                    maxMode: 'day'
                };
            }
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function isSplit() {
            return $ctrl.value.kind === 'Split';
        }

        function isBalance() {
            return $ctrl.value.kind === 'Balance';
        }
    }

})();

