/*
 * perform-payment.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.installmentPlan')
        .component('performPayment', {
            controller: PerformPaymentController,
            bindings: {
                close: '&',
                dismiss: '&',
                resolve: '<'
            },
            templateUrl: 'app/installment-plan/perform-payment.html'
        });

    PerformPaymentController.$inject = ['$q', 'InstalmentTransactionService', 'NotifierService'];

    /* @ngInject */
    function PerformPaymentController($q, InstalmentTransactionService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.performPayment = performPayment;

        ////////////

        function onInit() {
            $ctrl.payment = {
                instalmentsIdList: $ctrl.resolve.installments
            };
            $ctrl.rootAccount = "01.001.0001";
            $ctrl.condominium = $ctrl.resolve.condominium;
            $ctrl.financialPeriod = $ctrl.resolve.financialPeriod;
            $ctrl.totalAmount = $ctrl.resolve.totalAmount;
            $ctrl.personId = $ctrl.resolve.personId;
        }
        
        function performPayment() {
            return InstalmentTransactionService.addInstalmentPayment(
                $ctrl.condominium.id,
                $ctrl.financialPeriod.id,
                $ctrl.payment
            ).then(function () {
                NotifierService.notifySuccess('InstallmentPlan.Payment.PaymentSuccess');
                return $ctrl.close();
            }).catch(function (err) {
                NotifierService.notifyError('InstallmentPlan.Payment.PaymentFailure');
                return $q.reject(err);
            });
        }
    }

})();

