/*
 * installment-plan.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.installmentPlan')
        .factory('InstallmentPlanService', InstallmentPlanService);

    InstallmentPlanService.$inject = ['Restangular', 'contextAddressManagement','$uibModal'];

    /* @ngInject */
    function InstallmentPlanService(Restangular, contextAddressManagement,$uibModal) {
        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressManagement + 'v1');
        });

        var service = {
            getOne: getOne,
            getConfiguration: getConfiguration,
            create: create,
            approve: approve,
            unapprove: unapprove,
            addInstallment: addInstallment,
            updateConfiguration: updateConfiguration,
            open:open,
            getReport:getReport
        };
        return service;

        ////////////////

        /**
         * An installment plan.
         *
         * @typedef {Object} InstallmentPlan~InstallmentPlan
         * @property {Number} condominiumId - The unique id of condominium to create installment plan for.
         * @property {Number} financialPeriodId - The unique id of financial period to create installment plan for.
         * @property {Boolean} approved - True if the installment plan is approved.
         * @property {InstallmentPlan~Person[]} people - The installments
         */

        /**
         * A person inside the installment plan.
         *
         * @typedef {Object} InstallmentPlan~Person
         * @property {Number} personId - The unique id of person.
         * @property {String} firstName - The first name.
         * @property {String} lastName - The last name
         * @property {String} companyName - The company name.
         * @property {String} role - The person role.
         * @property {Number} housingUnitId - The housing unit id.
         * @property {String} stair - The stair of housing unit.
         * @property {String} floor - The floor of housing unit.
         * @property {String} apartmentNumber - The Number of apartment.
         * @property {InstallmentPlan~PersonInstallment[]} installments - The generated installment.
         */

        /**
         * A single installment for a person.
         *
         * @typedef {Object} InstallmentPlan~PersonInstallment
         * @property {Number} id - The unique id of installment.
         * @property {Date} expiration - The expiration date of installment.
         * @property {String} description - The installment description.
         * @property {Number} amount - The installment amount.
         * @property {Number} paid - The installment paid amount.
         * @property {Boolean} expired - True if installment is expired.
         */

        function financialPeriod(condominiumId, financialPeriodId) {
            return Api.one('condominiums', condominiumId).one('financial-periods', financialPeriodId);
        }

        function installmentPlan(condominiumId, financialPeriodId) {
            return financialPeriod(condominiumId, financialPeriodId).one('installment-plan', 'plan');
        }

        /**
         * Retrieve a single installment plan for supplied parameters.
         *
         * @param {number} condominiumId - The unique id of condominium to create installment plan for.
         * @param {number} financialPeriodId - The unique id of financial period to create installment plan for.
         * @returns {Promise<InstallmentPlan~InstallmentPlan|Error>} - The promise of installment plan.
         */
        function getOne(condominiumId, financialPeriodId) {
            return installmentPlan(condominiumId, financialPeriodId).get();
        }

        /**
         * A distribution mode.
         *
         * @typedef {String} InstallmentPlan~DistributionMode
         * @enum
         * @value {Percent}
         * @value {Proportional}
         * @value {Coefficient}
         */

        /**
         * @typedef {String} InstallmentPlan~PreviousYearMode
         * @enum
         * @value {First}
         * @value {BeforeFirst}
         * @value {Distributed}
         */

        /**
         * Object used to supply a single installment.
         *
         * @typedef {Object} InstallmentPlan~Installment
         * @property {Date} expiration - The expiration date of installment.
         * @property {String} description - The description of installment.
         * @property {Number} previousYear - The percentage of previous year in this installment.
         * @property {Number} coefficient - The coefficient of distribution of amount (the meaning of value depends on
         *                                  distributionMode of containing object)
         */

        /**
         * Object used to create a new installment plan.
         *
         * @typedef {Object} InstallmentPlan~Configuration
         * @property {Number} condominiumId - The unique id of condominium to create installment plan for.
         * @property {Number} financialPeriodId - The unique id of financial period to create installment plan for.
         * @property {Number} numberOfInstallments - The number of installments generated.
         * @property {InstallmentPlan~DistributionMode} distributionMode - The kind of distribution used to generate plan.
         * @property {Boolean} roundAmounts - True if amounts should be rounded.
         * @property {Number} roundAmountsTo - The value to round amounts to; needed only if roundAmounts is true.
         * @property {Number} roundingInstallment - The date of installment to assign rounding to.
         * @property {Number} mergeAmountsLower - The value to merge amounts lower than.
         * @property {Boolean} roundingInstallmentEnabled - True to set assign rounding to specific installment.
         * @property {Number} roundingInstallment - The index in installments array for the installment to which assign rounding to.
         * @property {InstallmentPlan~PreviousYearMode} previousYearMode - The mode to manage previous year remaining amount.
         * @property {InstallmentPlan~Installment[]} installments - The installments of this configuration.
         */

        /**
         * Creates a new installment plan with supplied parameters.
         *
         * @param {number} condominiumId - The unique id of condominium to create installment plan for.
         * @param {number} financialPeriodId - The unique id of financial period to create installment plan for.
         * @param {InstallmentPlan~Configuration} configuration - The configuration to be used to create installment plan.
         * @returns {Promise<Object|Error>} - The result of creation.
         */
        function create(condominiumId, financialPeriodId, configuration) {
            return installmentPlan(condominiumId, financialPeriodId).post("", configuration);
        }

        function consolidatePlan(condominiumId, financialPeriodId) {
            return financialPeriod(condominiumId, financialPeriodId).one("installment-plan", "consolidates-plan");
        }

        /**
         * Mark an installment plan as approved.
         *
         * @param {number} condominiumId - The unique id of condominium to approve installment plan for.
         * @param {number} financialPeriodId - The unique id of financial period to approve installment plan for.
         * @returns {Promise<Object|Error>} - The result of approval.
         */
        function approve(condominiumId, financialPeriodId) {
            return consolidatePlan(condominiumId, financialPeriodId).customPUT(undefined, undefined, { consolidated: 1 });
        }

        /**
         * Mark an installment plan as unapproved.
         *
         * @param {number} condominiumId - The unique id of condominium to approve installment plan for.
         * @param {number} financialPeriodId - The unique id of financial period to approve installment plan for.
         * @returns {Promise<Object|Error>} - The result of unapproval.
         */
        function unapprove(condominiumId, financialPeriodId) {
            return consolidatePlan(condominiumId, financialPeriodId).customPUT(undefined, undefined, { consolidated: 0 });
        }

        /**
         * The kind of installment to be generated.
         *
         * @typedef {String} InstallmentPlan~InstallmentKind
         * @enum
         * @value {Split} - The installment amount will be split using the linked distribution criterion.
         * @value {Assigned} - The installment amount will be assigned to every person.
         * @value {Balance} - The installment amount will be calculated with the balance for every person.
         */

        /**
         * An installment.
         *
         * @typedef {Object} InstallmentPlan~NewInstallment
         * @property {InstallmentPlan~InstallmentKind} kind - The kind of installment to generate.
         * @property {Date} expiration - The expiration date of installment.
         * @property {String} description - A textual description of installment.
         * @property {String} [account] - The account to use for splitting the amount.
         * @property {Number} [amount] - The amount to be used in installment generation
         */

        /**
         * Add an installment to installment plan identified by supplied condominiumId and financialPeriodId.
         *
         * @param {number} condominiumId - The unique id of condominium to approve installment plan for.
         * @param {number} financialPeriodId - The unique id of financial period to approve installment plan for.
         * @param {InstallmentPlan~NewInstallment} installment - The data for installment to create.
         */
        function addInstallment(condominiumId, financialPeriodId, installment) {
            return financialPeriod(condominiumId, financialPeriodId).one('installment-plan', 'instalment').post("", installment);
        }

        function installmentPlanConfiguration(condominiumId, financialPeriodId) {
            return financialPeriod(condominiumId, financialPeriodId).one('installment-plan', 'configuration');
        }

        /**
         * Retrieve the configuration for the supplied installment plan.
         * @returns {Promise<InstallmentPlan~Configuration|Error>} - The promise of configuration.
         */
        function getConfiguration(condominiumId, financialPeriodId) {
            return installmentPlanConfiguration(condominiumId, financialPeriodId).get().then(function (cfg) {
                angular.forEach(cfg.installments, function (installment) {
                    if (angular.isDefined(installment.expiration)) {
                        installment.expiration = new Date(installment.expiration);
                    }
                });
                return cfg;
            });
        }

        /**
         * Save the configuration
         * @param {number} condominiumId - The unique id of condominium to create installment plan for.
         * @param {number} financialPeriodId - The unique id of financial period to create installment plan for.
         * @param {InstallmentPlan~Configuration} configuration - The configuration used to create the new installment plan.
         * @returns {Promise<Object|Error>} - The result of update.
         */
        function updateConfiguration(condominiumId, financialPeriodId, configuration) {
            return installmentPlanConfiguration(condominiumId, financialPeriodId).customPUT(configuration);
        }

         /**
         * Open ui modal
         * @param {Object} options 
         * @param {String} templateUrl 
         */
        function open(options, templateUrl) {
            return $uibModal.open({
                templateUrl: templateUrl,
                windowClass: 'inmodal centre-cost-modal-container',
                bindToController: true,
                controller: 'InstallmentPlanEditController',
                controllerAs: 'vm',
                resolve: {
                    condominiumId: function () { return options.condominiumId; },
                    periodId: function () { return options.periodId; }
                }
            });
        }

          /**
         *  call end-point for download report
         * @param {Object} options - parameter for print Report
         */
        function getReport(printType, options) {
            var url =installmentPlan(options.condominiumId, options.financialPeriodId).all("reports").all(printType).getRequestedUrl();
            url = url + '?format=' + options.outputFormat;
            if (options.divideByUnit)
                url = url + "&divideByUnit=" + options.divideByUnit;
            window.open(url, "_blank");
        }
    }
})();