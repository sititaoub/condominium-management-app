/*
 * installment-plan-budget.directive.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.installmentPlan')
        .directive('installmentPlanBudget', installmentPlanBudget);

    installmentPlanBudget.$inject = [];

    /* @ngInject */
    function installmentPlanBudget() {
        var directive = {
            bindToController: true,
            controller: InstallmentPlanBudgetController,
            controllerAs: 'vm',
            restrict: 'E',
            scope: {
                'condominiumId': '=',
                'periodId': '=',
                'loading': '=',
                'totalAmount': '='
            },
            templateUrl: 'app/installment-plan/installment-plan-budget.html'
        };
        return directive;
    }

    InstallmentPlanBudgetController.$inject = ['NgTableParams', 'BudgetService', 'NotifierService', '_'];

    /* @ngInject */
    function InstallmentPlanBudgetController(NgTableParams, BudgetService, NotifierService, _) {
        var vm = this;

        vm.groupSum = groupSum;
        vm.isLastPage = isLastPage;

        activate();

        ////////////////

        function activate() {
            vm.loading = true;
            vm.showFilters = false;
            vm.tableParams = new NgTableParams({
                page: 1,
                count: 10,
                group: 'personId'
            }, {
                groupOptions: {
                    isExpanded: false
                }
            });
            loadBudget();
        }

        function loadBudget() {
            return BudgetService.getOneByPersons(vm.condominiumId, vm.periodId).then(function (budget) {
                vm.budget = budget;
                vm.totalPreviousYearAmount = _.sumBy(vm.budget.rows, 'previousYearAmount');
                vm.totalAmount = _.sumBy(vm.budget.rows, 'amount');
                vm.tableParams.settings({
                    dataset: vm.budget.rows
                });
            }).catch(function () {
                return NotifierService.notifyError('InstallmentPlan.Budget.LoadingError');
            }).finally(function () {
                vm.loading = false;
            })
        }

        /**
         * Sum the supplied data of supplied group.
         */
        function groupSum(group, property) {
            return _.sumBy(group.data, property);
        }

        /**
         * Check if the current table params page is the last page.
         *
         * @returns {boolean} - True if we are on last page and false if not
         */
        function isLastPage() {
            return vm.tableParams.page() === totalPages();
        }

        /**
         * Calculate the number of total pages.
         *
         * @returns {number} - The number of pages.
         */
        function totalPages() {
            return Math.ceil(vm.tableParams.total() / vm.tableParams.count())
        }
    }

})();

