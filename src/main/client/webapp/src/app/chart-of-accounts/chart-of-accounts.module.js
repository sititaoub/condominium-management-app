/*
 * chartOfAccounts.module.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts', [
            'condominiumManagementApp.core',
            'condominiumManagementApp.commons',
            'condominiumManagementApp.condominiums'
        ]);

})();
