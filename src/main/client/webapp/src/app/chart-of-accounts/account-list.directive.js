/*
 * account-list.directive.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .directive('accountList', accountList);

    accountList.$inject = [];

    /* @ngInject */
    function accountList() {
        var directive = {
            bindToController: true,
            controller: AccountListController,
            controllerAs: 'vm',
            restrict: 'E',
            scope: {
                id: '@',
                name: '@',
                ngModel: '=',
                ngRequired: '=',
                chartOfAccountsId: '=',
                companyId: '=',
                showCode: '=',
                rootAccount: '=',
                allowAccounts: '=?'
            },
            templateUrl: 'app/chart-of-accounts/account-list.html'
        };
        return directive;
    }

    AccountListController.$inject = ['AccountChooserService', 'ChartOfAccountsService', 'NotifierService'];

    /* @ngInject */
    function AccountListController(AccountChooserService, ChartOfAccountsService, NotifierService) {
        var vm = this;
        var chartOfAccounts;

        vm.getAccountName = getAccountName;
        vm.openLookup = openLookup;
        vm.remove = remove;

        activate();

        ////////////////

        function activate() {
            vm.loading = true;
            vm.ngModel = _.sortBy(_.uniq(vm.ngModel));
            ChartOfAccountsService.getOne(vm.chartOfAccountsId, vm.companyId).then(function (coa) {
                chartOfAccounts = coa;
            }).catch(function () {
                NotifierService.notifyError();
            }).finally(function () {
                vm.loading = false;
            });
            vm.scrollbars = {
                autoHideScrollbar: false,
                theme: 'dark',
                advanced: {
                    updateOnContentResize: true
                }
            };
        }

        function getAccountName(materializedPath) {
            if (!chartOfAccounts) {
                return materializedPath;
            }
            return ChartOfAccountsService.getAccount(chartOfAccounts, materializedPath).descrizione;
        }

        function remove(index) {
            vm.ngModel.splice(index, 1);
        }

        function openLookup() {
            AccountChooserService.open({
                multiple: true,
                returnObjects: true,
                chartOfAccountsId: vm.chartOfAccountsId,
                companyId: vm.companyId,
                showCode: vm.showCode,
                rootAccount: vm.rootAccount,
                allowAccounts: vm.allowAccounts
            }).result.then(function (selected) {
                selected = _.map(selected, 'materializedPath');
                vm.ngModel = _.sortBy(_.uniq(vm.ngModel.concat(selected)));
            });
        }
    }

})();

