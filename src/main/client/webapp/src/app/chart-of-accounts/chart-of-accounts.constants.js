/*
 * chart-of-accounts.constants.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        /**
         * The root accounts for simplified view.
         */
        .constant('rootAccounts', {
            costs: '03.001',
            revenue: '04.001',
            personal: '03.004',
            provisions: '03.002'
        })
        /**
         * The size of account codes from level.
         */
        .constant('accountsCodeSizes', {
            GRUPPO: 2,
            MASTRO: 3,
            CONTO: 4,
            SOTTOCONTO: 5
        });
})();
