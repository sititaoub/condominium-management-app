/*
 * edit.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .controller('ChartOfAccountsEditController', ChartOfAccountsEditController);

    ChartOfAccountsEditController.$inject = ['$state', 'rootAccounts', 'condominium', 'chartOfAccounts', 'ChartOfAccountsService', 'NotifierService'];

    /* @ngInject */
    function ChartOfAccountsEditController($state, rootAccounts, condominium, chartOfAccounts, ChartOfAccountsService, NotifierService) {
        var vm = this;

        vm.activeTab = 0;
        vm.doSave = doSave;

        activate();

        ////////////////

        function activate() {
            vm.companyId = $state.params.companyId;
            vm.condominium = condominium;
            vm.chartOfAccounts = angular.copy( chartOfAccounts );   // Beware of the cache!
            vm.costs = ChartOfAccountsService.extractSubtree(vm.chartOfAccounts, rootAccounts.costs);
            vm.revenues = ChartOfAccountsService.extractSubtree(vm.chartOfAccounts, rootAccounts.revenue);
            vm.personalExpenses = ChartOfAccountsService.extractSubtree(vm.chartOfAccounts, rootAccounts.personal);
            vm.provisions = ChartOfAccountsService.extractSubtree(vm.chartOfAccounts, rootAccounts.provisions);
        }

        function doSave() {
            ChartOfAccountsService.update($state.params.chartOfAccountsId, $state.params.companyId, vm.chartOfAccounts).then(function () {
                return NotifierService.notifySuccess('ChartOfAccounts.Edit.SaveSuccess');
            }).catch(function () {
                return NotifierService.notifyError('ChartOfAccounts.Edit.SaveFailure');
            }).then(function() {
                $state.go(".", undefined, { reload: true });
            });
        }
    }

})();

