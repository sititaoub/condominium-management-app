/*
 * account-edit.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .factory('AccountEditService', AccountEditService);

    AccountEditService.$inject = ['$uibModal'];

    /* @ngInject */
    function AccountEditService($uibModal) {
        var service = {
            open: open
        };
        return service;

        ////////////////

        /**
         * The options for account edit service.
         *
         * @typedef {Object} AccountEditService~Options
         * @property {ChartOfAccounts~Account} rootNode - The root node.
         * @property {ChartOfAccounts~Account} account - The account to be modified.
         * @property {number} companyId - The unique id of company.
         * @property {boolean} showCode - True to show the code.
         * @property {boolean} allowAddressBook - True to allow the address book.
         * @property {string} fixedAddressBook - The code of fixed address book.
         */

        /**
         * The result of the open function.
         *
         * @typedef {Object} AccountEditService~Result
         * @property {function} close - Can be used to close a modal, passing a result.
         * @property {function} dismiss - Can be used to dismiss a modal, passing a reason.
         * @property {Promise.<ChartOfAccounts~Account|Error>} result -Is resolved when a modal is closed and rejected when a modal is dismissed.
         */

        /**
         * Open the modal with supplied options.
         *
         * @param {AccountEditService~Options} options - The options.
         * @returns {AccountEditService~Result} - The result.
         */
        function open(options) {
            return $uibModal.open({
                templateUrl: 'app/chart-of-accounts/account-edit.html',
                windowClass: 'inmodal centre-cost-modal-container',
                bindToController: true,
                controller: AccountEditServiceController,
                controllerAs: 'vm',
                resolve: {
                    companyId: function() { return options.companyId; },
                    rootNode: function() { return options.rootNode; },
                    account: function() { return angular.copy(options.account); },
                    showCode: function() { return options.showCode; },
                    allowAddressBook: function () { return options.allowAddressBook; },
                    fixedAddressBook: function () { return options.fixedAddressBook; }
                }
            });
        }
    }

    AccountEditServiceController.$inject = ['$uibModalInstance', 'AddressBookTypeService', 'accountsCodeSizes', 'rootNode', 'account', 'showCode', 'companyId', 'allowAddressBook', 'fixedAddressBook'];

    /* @ngInject */
    function AccountEditServiceController($uibModalInstance, AddressBookTypeService, accountsCodeSizes, rootNode, account, showCode, companyId, allowAddressBook, fixedAddressBook) {
        var vm = this;

        vm.accountCodeDoesNotExists = accountCodeDoesNotExists;
        vm.doConfirm = doConfirm;

        activate();

        ////////////////

        function activate() {
            vm.rootNode = rootNode;
            vm.allowAddressBook = allowAddressBook;
            vm.fixedAddressBook = fixedAddressBook;
            vm.account = account;
            vm.showCode = showCode;
            vm.codeSize = accountsCodeSizes[vm.account.level] || 5;

            AddressBookTypeService.getAll(companyId).then(function (types) {
                vm.addressBookTypes = types;
            });
        }

        /**
         * Check that the account code does not exists.
         *
         * @param {string} value - The value to check
         * @returns {boolean} - True if the account code does not exists and false otherwise.
         */
        function accountCodeDoesNotExists(value) {
            return angular.isUndefined(_.find(vm.rootNode.children, { codice: value }));
        }

        function doConfirm() {
            $uibModalInstance.close(vm.account);
        }
    }

})();

