/*
 * address-book-type.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .factory('AddressBookTypeService', AddressBookTypeService);

    AddressBookTypeService.$inject = ['Restangular', 'contextAddressAggregatori'];

    /* @ngInject */
    function AddressBookTypeService(Restangular, contextAddressAggregatori) {
        var AddressBookTypes = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressAggregatori + 'v2');
        }).all('chartofaccounts').all('addressBookTypes');
        var service = {
            getAll: getAll
        };
        return service;

        ////////////////

        /**
         * Type of address book.
         *
         * @typedef {Object} AddressBookTypeService~Type
         */

        /**
         * Retrieve all the address book type for supplied company id.
         *
         * @param {number} companyId - The address book types
         * @returns {Promise.<AddressBookTypeService~Type[]|Error>} - The list of address book types.
         */
        function getAll(companyId) {
            return AddressBookTypes.getList({ companyId: companyId });
        }
    }

})();

