/*
 * accunt-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .component('accountInput', {
            controller: AccountInputController,
            bindings: {
                id: '@',
                name: '@',
                ngModel: '<',
                ngRequired: '<',
                ngDisabled: '<?',
                placeholder: '@',
                chartOfAccountsId: '<',
                companyId: '<?',
                rootAccount: '@',
                size: '@?',
                includeDescription: '<',
                hideCode: '@?',
                includeOnly: '<?',
                allowAll: '<?',
                returnObject: '<?',
                onAccountSelected: '&'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/chart-of-accounts/account-input.html'
        });

    AccountInputController.$inject = ['AccountChooserService', 'ChartOfAccountsService', 'NotifierService', '$translate'];

    /* @ngInject */
    function AccountInputController(AccountChooserService, ChartOfAccountsService, NotifierService,  $translate) {
        var $ctrl = this;
        var chartOfAccounts;

        $ctrl.$onInit = onInit;
        $ctrl.$onChanges = onChanges;
        $ctrl.openLookup = openLookup;
        $ctrl.remove = remove;

        ////////////////

        function onInit() {
            $ctrl.description = undefined;
            $ctrl.breadcrumb  = undefined;
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
               
                if (angular.isDefined($ctrl.rootAccount)){
                    ChartOfAccountsService.getOne($ctrl.chartOfAccountsId, $ctrl.companyId, true).then(function (coa) {
                        var rootAccount  = ChartOfAccountsService.getAccount(coa, $ctrl.rootAccount); 
                        $translate('SELECT_ACCOUNT',{ 
                            accountDescription: rootAccount.descrizione
                        } ).then(function (text) {
                            $ctrl.placeholder = text;
                        })
                    });
                }else{
                    $translate('SELECT_GENERIC_ACCOUNT').then(function (text) {
                        $ctrl.placeholder = text;
                    })
                }

               
            };
        }

        function onChanges(changes) {
            if (changes.chartOfAccountsId || changes.companyId || changes.ngModel) {
                loadChartOfAccounts();
            }
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue($ctrl.value);
        }

        function loadChartOfAccounts() {
            if (angular.isDefined($ctrl.ngModel) && angular.isDefined($ctrl.chartOfAccountsId) && angular.isDefined($ctrl.companyId)) {
                ChartOfAccountsService.getOne($ctrl.chartOfAccountsId, $ctrl.companyId, true).then(function (coa) {
                    chartOfAccounts = coa;
                    var matPath = angular.isString($ctrl.ngModel) ? $ctrl.ngModel : $ctrl.ngModel.materializedPath;
                    var selectedAccountTreePath  = ChartOfAccountsService.getAllAccountTraversingPath(chartOfAccounts, matPath);
                    $ctrl.description  = selectedAccountTreePath[selectedAccountTreePath.length-1].descrizione;
                    $ctrl.breadcrumb = _.map(selectedAccountTreePath, function(obj){return obj.codice + ' - ' + obj.descrizione;}).join(' > ');
   
                }).catch(function () {
                    NotifierService.notifyError();
                });
            }
        }

        function remove() {
            $ctrl.value = undefined;
            $ctrl.description = undefined;
            onChange();
        }

        function openLookup() {
            AccountChooserService.open({
                multiple: false,
                chartOfAccountsId: $ctrl.chartOfAccountsId,
                companyId: $ctrl.companyId,
                hideCode: $ctrl.hideCode,
                rootAccount: $ctrl.rootAccount,
                returnObjects: true,
                includeAddressBookItems: true,
                includeOnly: $ctrl.includeOnly,
                allowAll: $ctrl.allowAll
            }).result.then(function (account) {
                $ctrl.onAccountSelected({ account: account});
                $ctrl.value = $ctrl.returnObject ? account: account.materializedPath ;
                $ctrl.description = account.description;
                onChange();
            });

        }
    }

})();

