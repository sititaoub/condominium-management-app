/*
 * chartOfAccounts.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider'];

    /* @ngInject */
    function setupRoutes($stateProvider) {
        $stateProvider.state('index.chartOfAccounts', {
            url: '/condominiums/{condominiumId}/chart-of-accounts',
            template: '<ui-view></ui-view>',
            abstract: true,
            data: {
                skipFinancialPeriodCheck: true
            }
        }).state('index.chartOfAccounts.edit', {
            url: '/{chartOfAccountsId}/edit',
            controller: 'ChartOfAccountsEditController',
            controllerAs: 'vm',
            templateUrl: 'app/chart-of-accounts/chart-of-accounts-edit.html',
            resolve: {
                condominium: loadCondominium,
                chartOfAccounts: loadChartOfAccounts
            }
        });
    }

    loadCondominium.$inject = ['$stateParams', 'CondominiumService'];

    /* @ngInject */
    function loadCondominium($stateParams, CondominiumService) {
        return CondominiumService.getOne($stateParams.condominiumId);
    }

    loadChartOfAccounts.$inject = ['$stateParams', 'ChartOfAccountsService'];

    /* @ngInject */
    function loadChartOfAccounts($stateParams, ChartOfAccountsService) {
        return ChartOfAccountsService.getOne($stateParams.chartOfAccountsId, $stateParams.companyId);
    }

})();
