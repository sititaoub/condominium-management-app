/*
 * chart-of-accounts.routes.spec.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
describe('chart-of-accounts routes', function () {
    'use strict';

    var $httpBackend, $q, $state, $templateCache, $location;

    var financialPeriodServiceMock = jasmine.createSpyObj('FinancialPeriodService', ['getCurrent']);
    var condominiumServiceMock = jasmine.createSpyObj('CondominiumService', ['getOne']);
    var chartOfAccountsServiceMock = jasmine.createSpyObj('ChartOfAccountsService', ['getOne']);

    function mockServices($provide) {
        $provide.factory('FinancialPeriodService', function () {
            return financialPeriodServiceMock;
        });
        $provide.factory('CondominiumService', function () {
            return condominiumServiceMock;
        });
        $provide.factory('ChartOfAccountsService', function () {
            return chartOfAccountsServiceMock;
        });
    }

    function services($injector) {
        $q = $injector.get('$q');
        $state = $injector.get('$state');
        $templateCache = $injector.get('$templateCache');
        $location = $injector.get('$location');
        $httpBackend = $injector.get('$httpBackend');
    }

    function setUp() {
        $httpBackend.whenGET(/assets\/(.+)/).respond(200, {});
        $httpBackend.whenGET(/users\/me$/).respond(200, {});
    }

    beforeEach(function () {
        module('condominiumManagementApp', 'condominiumManagementApp.templates', mockServices);
        inject(services);
        setUp();
    });

    describe('when navigating to /index/condominiums/8/chart-of-accounts/76/edit`', function () {
        function goTo(url) {
            $location.url(url);
            $httpBackend.flush();
        }

        describe('and both condominium and chart of accounts can be loaded', function () {
            it('transition to show chart of accounts', function () {
                condominiumServiceMock.getOne.and.returnValue($q.resolve({ id: 8 }));
                chartOfAccountsServiceMock.getOne.and.returnValue($q.resolve({ id: 76 }));

                goTo('/index/condominiums/8/chart-of-accounts/76/edit?companyId=390');

                expect($state.current.name).toBe('index.chartOfAccounts.edit');
                expect($state.params.condominiumId).toBe('8');
                expect($state.params.companyId).toBe('390');
                expect($state.params.chartOfAccountsId).toBe('76');
                expect(financialPeriodServiceMock.getCurrent).not.toHaveBeenCalled();
                expect(condominiumServiceMock.getOne).toHaveBeenCalledWith('8');
                expect(chartOfAccountsServiceMock.getOne).toHaveBeenCalledWith('76', '390');
            });
        });

        describe('and condominium cannot be loaded', function () {
            it('prevent the transition to occurs', function () {
                condominiumServiceMock.getOne.and.returnValue($q.reject({ status: 404 }));
                chartOfAccountsServiceMock.getOne.and.returnValue($q.resolve({ id: 76 }));

                goTo('/index/condominiums/8/chart-of-accounts/76/edit?companyId=390');

                expect($state.current.name).toBe('index.condominiums.list');
                expect(financialPeriodServiceMock.getCurrent).not.toHaveBeenCalled();
                expect(condominiumServiceMock.getOne).toHaveBeenCalledWith('8');
                expect(chartOfAccountsServiceMock.getOne).toHaveBeenCalledWith('76', '390');
            });
        });

        describe('and chart of accounts cannot be loaded', function () {
            it('prevent the transition to occurs', function () {
                condominiumServiceMock.getOne.and.returnValue($q.resolve({ id: 8 }));
                chartOfAccountsServiceMock.getOne.and.returnValue($q.reject({ status: 404 }));

                goTo('/index/condominiums/8/chart-of-accounts/76/edit?companyId=390');

                expect($state.current.name).toBe('index.condominiums.list');
                expect(financialPeriodServiceMock.getCurrent).not.toHaveBeenCalled();
                expect(condominiumServiceMock.getOne).toHaveBeenCalledWith('8');
                expect(chartOfAccountsServiceMock.getOne).toHaveBeenCalledWith('76', '390');
            });
        });
    });
});