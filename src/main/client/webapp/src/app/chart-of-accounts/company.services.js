/*
 * materialized-path-lookup.services.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .factory('CompanyService', CompanyService);

        CompanyService.$inject = ['$q', 'Restangular', 'CacheFactory', 'contextAddressAggregatori'];

    /* @ngInject */
    function CompanyService($q, Restangular, CacheFactory, contextAddressAggregatori) {
        var CACHE_NAME = 'CompanyCache';
        var CACHE_EXPIRATION_MS = 1 * 60 * 60 * 1000; //12 hours 
        var pendingQueries = {};

        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressAggregatori + 'v2');
        });

        if (!CacheFactory.get(CACHE_NAME)) {
            CacheFactory.createCache(CACHE_NAME, {
                deleteOnExpire: 'passive',
                maxAge: CACHE_EXPIRATION_MS
            });
        }
        var cache = CacheFactory.get(CACHE_NAME);

        var service = {
            getOne: getOne
        };
        return service;

        ////////////////




        /**
         * Retrieve a single copany for supplied unique identifier.
         *
         * @param {number} companyId   - The unique identifier of company to retrieve company detail for.
         * @returns {Promise.<CompanyDTO~Chart|Error>} - The promise over  company detail.
         */
        function getOne(companyId) {
            var deferred = $q.defer();

            var key = '/companies/' + companyId;
            var result = cache.get(key);
            if (result) {
                deferred.resolve(result);
            } else if (pendingQueries[key]) {
                return pendingQueries[key];
            } else {
                pendingQueries[key] = deferred.promise;

                Api.one('companies', companyId).get().then(function (data) {
                    cache.put(key, data);
                    pendingQueries[key] = undefined;
                    deferred.resolve(data);
                }).catch(deferred.reject);
            }

            return deferred.promise;
        }

   
    }

})();

