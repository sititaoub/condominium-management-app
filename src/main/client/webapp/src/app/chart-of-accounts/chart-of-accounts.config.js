/*
 * chartOfAccounts.config.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .config(config);

        config.$inject = ['$translatePartialLoaderProvider'];

        /* @ngInject */
        function config ($translatePartialLoaderProvider) {
            $translatePartialLoaderProvider.addPart('chart-of-accounts');
        }

})();
