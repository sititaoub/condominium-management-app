/*
 * chart-of-accounts-tabs.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .component('chartOfAccountsTabs', {
            controller: ChartOfAccountsTabsComponent,
            bindings: {
                costsDisabled: '<',
                revenuesDisabled: '<',
                personalExpensesDisabled: '<',
                provisionsDisabled: '<'
            },
            transclude: {
                costs: 'costsPane',
                revenues: 'revenuesPane',
                personalExpenses: 'personalExpensesPane',
                provisions: 'provisionsPane'
            },
            templateUrl: 'app/chart-of-accounts/chart-of-accounts-tabs.html'
        });

    ChartOfAccountsTabsComponent.$inject = ['rootAccounts'];

    /* @ngInject */
    function ChartOfAccountsTabsComponent(rootAccounts) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;

        ////////////

        function onInit() {
            $ctrl.rootAccounts = rootAccounts;
        }
    }

})();

