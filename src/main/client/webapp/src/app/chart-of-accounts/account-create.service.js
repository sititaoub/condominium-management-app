/*
 * account-create.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .factory('AccountCreateService', AccountCreateService);

    AccountCreateService.$inject = ['$uibModal'];

    /* @ngInject */
    function AccountCreateService($uibModal) {
        var service = {
            open: open
        };
        return service;

        ////////////////

        /**
         * The options for account edit service.
         *
         * @typedef {Object} AccountCreateService~Options
         * @property {ChartOfAccounts~Account} rootNode - The root node.
         * @property {number} companyId - The unique id of company.
         * @property {boolean} showCode - True to show the code.
         * @property {boolean} allowAddressBook - True to allow the address book.
         * @property {string} fixedAddressBook - The code of fixed address book.
         */

        /**
         * The result of the open function.
         *
         * @typedef {Object} AccountCreateService~Result
         * @property {function} close - Can be used to close a modal, passing a result.
         * @property {function} dismiss - Can be used to dismiss a modal, passing a reason.
         * @property {Promise.<ChartOfAccounts~Account|Error>} result -Is resolved when a modal is closed and rejected when a modal is dismissed.
         */

        /**
         * Open the modal with supplied options.
         *
         * @param {AccountCreateService~Options} options - The options.
         * @returns {AccountCreateService~Result} - The result.
         */
        function open(options) {
            return $uibModal.open({
                templateUrl: 'app/chart-of-accounts/account-create.html',
                windowClass: 'inmodal centre-cost-modal-container',
                bindToController: true,
                controller: AccountCreateServiceController,
                controllerAs: 'vm',
                resolve: {
                    companyId: function() { return options.companyId; },
                    rootNode: function() { return options.rootNode; },
                    showCode: function() { return options.showCode; },
                    allowAddressBook: function () { return options.allowAddressBook; },
                    fixedAddressBook: function () { return options.fixedAddressBook; }
                }
            });
        }
    }

    AccountCreateServiceController.$inject = ['$uibModalInstance', 'AddressBookTypeService', 'accountsCodeSizes', 'rootNode', 'showCode', 'companyId', 'allowAddressBook', 'fixedAddressBook', '_'];

    /* @ngInject */
    function AccountCreateServiceController($uibModalInstance, AddressBookTypeService, accountsCodeSizes, rootNode, showCode, companyId, allowAddressBook, fixedAddressBook, _) {
        var vm = this;

        vm.accountCodeDoesNotExists = accountCodeDoesNotExists;
        vm.doConfirm = doConfirm;

        activate();

        ////////////////

        function activate() {
            var level = deeperLevel(rootNode);
            vm.codeSize = accountsCodeSizes[level] || 5;

            vm.rootNode = rootNode;
            vm.allowAddressBook = allowAddressBook;
            vm.fixedAddressBook = fixedAddressBook;
            vm.account = {
                ownerCompanyId: companyId,
                codice: nextAccountCode(rootNode.children),
                level: level,
                children: [],
            };
            if (vm.fixedAddressBook) {
                vm.account.addressBookRef = vm.fixedAddressBook;
            }
            vm.showCode = showCode;

            AddressBookTypeService.getAll(companyId).then(function (types) {
                vm.addressBookTypes = types;
            });
        }

        /**
         * Extract the next account code from rootNode.
         *
         * @param {ChartOfAccounts~Account[]} accounts - The accounts to extract next code.
         * @returns {string} - The next account code.
         */
        function nextAccountCode(accounts) {
            if (!accounts || accounts.length === 0) {
                return _.padStart('1', vm.codeSize, '0');
            }
            return _.padStart(parseInt(_.maxBy(accounts, 'codice').codice) + 1, vm.codeSize, '0');
        }

        /**
         * Retrieve the deeper level for parent.
         *
         * @param {ChartOfAccounts~Account} parent - The parent account.
         * @returns {string|null} - The deeper level
         */
        function deeperLevel(parent) {
            switch (parent.level) {
                case "GRUPPO":
                    return "MASTRO";
                case "MASTRO":
                    return "CONTO";
                case "CONTO":
                    return "SOTTOCONTO";
                default:
                    return null;
            }
        }

        /**
         * Check that the account code does not exists.
         *
         * @param {string} value - The value to check
         * @returns {boolean} - True if the account code does not exists and false otherwise.
         */
        function accountCodeDoesNotExists(value) {
            return angular.isUndefined(_.find(vm.rootNode.children, { codice: value }));
        }

        function doConfirm() {
            vm.account.materializedPath = vm.rootNode.materializedPath + '.' + vm.account.codice;
            $uibModalInstance.close(vm.account);
        }
    }

})();

