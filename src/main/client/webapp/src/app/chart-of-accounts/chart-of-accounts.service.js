/*
 * chart-of-accounts.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .factory('ChartOfAccountsService', ChartOfAccountsService);

    ChartOfAccountsService.$inject = ['$q', 'Restangular', 'CacheFactory', 'contextAddressAggregatori'];

    /* @ngInject */
    function ChartOfAccountsService($q, Restangular, CacheFactory, contextAddressAggregatori) {
        var CACHE_NAME = 'ChartOfAccountsCache';
        var CACHE_EXPIRATION_MS = 5 * 60 * 1000;
        var pendingQueries = {};

        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressAggregatori + 'v2');
        });

        if (!CacheFactory.get(CACHE_NAME)) {
            CacheFactory.createCache(CACHE_NAME, {
                deleteOnExpire: 'passive',
                maxAge: CACHE_EXPIRATION_MS
            });
        }
        var cache = CacheFactory.get(CACHE_NAME);

        var service = {
            getOne: getOne,
            update: update,
            getAccount: getAccount,
            extractSubtree: extractSubtree,
            getRootPath: getRootPath,
            getAllAccountTraversingPath: getAllAccountTraversingPath
        };
        return service;

        ////////////////

        /**
         * A chart of accounts.
         *
         * @typedef {Object} ChartOfAccounts~Chart
         * @property {number} id - The unique identifier.
         * @property {string} nome - The chart of accounts name.
         * @property {ChartOfAccounts~Account[]} children - The children.
         */

        /**
         * An account.
         *
         * @typedef {Object} ChartOfAccounts~Account
         * @property {number} id - The unique account id.
         * @property {string} codice - The account code.
         * @property {string} descrizione - The account description.
         * @property {string} codiceContabilita - The accounting code.
         * @property {string} tipo - The account kind.
         * @property {string} voiceAccount - The voice account.
         * @property {string} grouping - The grouping.
         * @property {string} stato - The account status.
         * @property {number} ownerCompanyId - The unique id of owner company.
         * @property {number} idVoceGenerico - Unique id of generic account.
         * @property {string} gruppoRiclassificazione - Reclassification group.
         * @property {string} level - The level.
         * @property {string} materializedPath - The materialized path of account.
         * @property {string} addressBookRef - The address book references.
         * @property {ChartOfAccounts~Account[]} children - The children.
         */

        /**
         * Retrieve a single chart of accounts for supplied unique identifier.
         *
         * @param {number} id - The unique identifier to retrieve chart of accounts for.
         * @param {number} companyId - The unique identifier of company to retrieve chart of accounts for.
         * @param {boolean} includeAddressBookItems - True to include address book items.
         * @returns {Promise.<ChartOfAccounts~Chart|Error>} - The promise over the chart of accounts detail.
         */
        function getOne(id, companyId, includeAddressBookItems) {
            var deferred = $q.defer();

            var key = '/companies/' + companyId + '/chart-of-accounts/' + id;
            if (angular.isDefined(includeAddressBookItems)) {
                key += '/with-address-book-items';
            }
            var result = cache.get(key);
            if (result) {
                deferred.resolve(result);
            } else if (pendingQueries[key]) {
                return pendingQueries[key];
            } else {
                pendingQueries[key] = deferred.promise;

                Api.one('chartofaccounts', id).get({
                    companyId: companyId,
                    includeAddressBookItems: includeAddressBookItems
                }).then(function (data) {
                    cache.put(key, data);
                    pendingQueries[key] = undefined;
                    deferred.resolve(data);
                }).catch(deferred.reject);
            }

            return deferred.promise;
        }

        /**
         * Updates the chart of accounts for supplied data.
         *
         * @param {number} id - The unique identifier to retrieve chart of accounts for.
         * @param {number} companyId - The unique identifier of company to retrieve chart of accounts for.
         * @param {ChartOfAccounts~Chart} chartOfAccounts - The chart of account to update.
         * @returns {Promise.<Object|Error>} - The result.
         */
        function update(id, companyId, chartOfAccounts) {
            return Api.one("chartofaccounts", id).customPUT(chartOfAccounts, undefined, { companyId: companyId }).then(function (data) {
                var key = '/companies/' + companyId + '/chart-of-accounts/' + id;
                cache.remove(key);
                return data;
            });
        }

        /**
         * Extract the account name from the supplied chart of account and path.
         * @param {ChartOfAccounts~Chart|ChartOfAccounts~Account} chartOfAccount - The chart of accounts to retrieve the account name for
         * @param {String} path - The path to extract name
         * @returns {ChartOfAccounts~Account|null} - The account.
         */
        function getAccount(chartOfAccount, path) {

            var splitMathPathCode = path.split('.');
            var account = chartOfAccount;
            var pathTofind;
            //find recursively into chart of account by code 
            for (var i = 0; i < splitMathPathCode.length; i++) {
                //Only for the first dont add separator point
                pathTofind = (i == 0)  ? splitMathPathCode[i] :  pathTofind + "." + splitMathPathCode[i];
                account = _.find(account.children, function(obj) {
                    return obj.materializedPath === pathTofind;
                });
            }
            return account;

        }

        getPathDescription

        /**
         * Extract all the nodes encountered during the path's navigation. 
         * @param {ChartOfAccounts~Chart|ChartOfAccounts~Account} chartOfAccount - The chart of accounts to retrieve the account name for
         * @param {String} path - The path to cross to find all node 
         * @returns {ChartOfAccounts~Account[]} - The accounts found during the path's navigation.
         */
        function getAllAccountTraversingPath(chartOfAccount, path) {

            var accounts = new Array();

            if (angular.isDefined(path)){
                var splitMathPathCode = path.split('.');
                var account = chartOfAccount;
                var pathTofind;
                
                //find recursively into chart of account by code 
                for (var i = 0; i < splitMathPathCode.length; i++) {
                    //Only for the first dont add separator point
                    pathTofind = (i == 0)  ? splitMathPathCode[i] :  pathTofind + "." + splitMathPathCode[i];
                    account = _.find(account.children, function(obj) {
                        return obj.materializedPath === pathTofind;
                    });

                    accounts.push(account);
                    
                }
            }
            return accounts;
        }

        /**
         * Extract a subtree reference from supplied chart of account.
         *
         * @param {ChartOfAccounts~Chart|ChartOfAccounts~Account} chartOfAccount - The chart of accounts to retrieve the subtree for
         * @param {string} rootPath - The root path (will be compared with materializedPath property of accounts).
         * @param {string[]} [includeOnly] - The array of materialized path to include.
         * @returns {ChartOfAccounts~Account|null} - The account for supplied materialized path
         */
        function extractSubtree(chartOfAccount, rootPath, includeOnly) {
            if (angular.isUndefined(rootPath)){
                var newNode = new Object();
                newNode.Description = chartOfAccount.description;
                newNode.children = chartOfAccount.children;
                return newNode;
            } 

            for (var i = 0; i < chartOfAccount.children.length; i++) {
                var account = chartOfAccount.children[i];
                // If child materializedPath match the supplied root path, return the child...
                if (account.materializedPath === rootPath) {
                    if (angular.isDefined(includeOnly)) {
                        return filterChildren(account, includeOnly);
                    } else {
                        return account;
                    }
                }
                // ... if not, check if the rootPath is a child of this account...
                if (_.startsWith(rootPath, account.materializedPath)) {
                    return extractSubtree(account, rootPath, includeOnly);
                }
                // ... else skip the account and go on.
            }
            // Here we haven't found any account for supplied root path
            return null;
        }

        function filterChildren(account, materializedPaths) {
            var account = angular.copy(account);
            if (angular.isUndefined(account.children) || account.children.length === 0) {
                // We are on leaf account. Return the account only if has a matching materialized path
                if (_.findIndex(materializedPaths, { path: account.materializedPath }) >= 0) {
                    account.isLeaf = true;
                    return account;
                } else {
                    return null;
                }
            }
            // We are not on a child node, iterate on all children,
            account.children = _.filter(_.map(account.children, function (child) {
                return filterChildren(child, materializedPaths);
            }), function (child) {
                return child !== null && (child.isLeaf || child.children.length > 0);
            });
            return account;
        }

        /**
         * Get the root materialized path.
         */
        function getRootPath(companyId, chartOfAccountId, name) {
            return Api.one('companies', companyId).one('chart-of-accounts', chartOfAccountId)
                .one('materializedpath-lookups', name).get({ applicationId: 'management' });
        }
    }

})();

