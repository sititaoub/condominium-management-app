/*
 * subtree-edit.directive.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .directive('chartOfAccountsSubtreeEdit', chartOfAccountsSubtreeEdit);

    chartOfAccountsSubtreeEdit.$inject = [];

    /* @ngInject */
    function chartOfAccountsSubtreeEdit() {
        var directive = {
            bindToController: true,
            controller: ChartOfAccountsSubtreeEditController,
            controllerAs: 'vm',
            restrict: 'E',
            scope: {
                ngModel: '=',
                companyId: '=',
                showCode: '@',
                allowAddressBook: '=',
                fixedAddressBook: '='
            },
            require: 'ngModel',
            templateUrl: 'app/chart-of-accounts/subtree-edit.html'
        };
        return directive;
    }

    ChartOfAccountsSubtreeEditController.$inject = ['SweetAlert', '$translate', 'AccountEditService', 'AccountCreateService', 'accountsCodeSizes', '_'];

    /* @ngInject */
    function ChartOfAccountsSubtreeEditController(SweetAlert, $translate, AccountEditService, AccountCreateService, accountsCodeSizes, _) {
        var vm = this;

        vm.addAccount = addAccount;
        vm.editAccount = editAccount;
        vm.removeAccount = removeAccount;
        vm.cloneAccount = cloneAccount;

        activate();

        ////////////////

        function activate() {
            // Nothing to do.
        }

        /**
         * Adds a new account to supplied parent.
         *
         * @param {ChartOfAccounts~Account|ChartOfAccounts~Chart} parent - The parent object used to add the child to.
         */
        function addAccount(parent) {
            AccountCreateService.open({
                companyId: vm.companyId,
                rootNode: parent,
                showCode: vm.showCode,
                allowAddressBook: vm.allowAddressBook,
                fixedAddressBook: vm.fixedAddressBook
            }).result.then(function (newAccount) {
                parent.children.push(newAccount);
            })
        }

        /**
         * Edit an existing account.
         *
         * @param {ChartOfAccounts~Account} account - The account to be edited.
         */
        function editAccount(account) {
            AccountEditService.open({
                companyId: vm.companyId,
                rootNode: vm.ngModel,
                account: account,
                showCode: vm.showCode,
                allowAddressBook: vm.allowAddressBook,
                fixedAddressBook: vm.fixedAddressBook
            }).result.then(function (newAccount) {
                account.codice = newAccount.codice;
                account.codiceContabilita = newAccount.codiceContabilita;
                account.addressBookRef = newAccount.addressBookRef;
                account.descrizione = newAccount.descrizione;
            });
        }

        /**
         * Remove the account as provided.
         *
         * @param {Object} node - The node to be removed.
         * @param {ChartOfAccounts~Account} account - The account to be removed
         */
        function removeAccount(node, account) {
            var TITLE = 'ChartOfAccounts.Edit.Remove.Title',
                MESSAGE = 'ChartOfAccounts.Edit.Remove.Message',
                CONFIRM = 'ChartOfAccounts.Edit.Remove.Confirm',
                CANCEL = 'ChartOfAccounts.Edit.Remove.Cancel';

            $translate([ TITLE, MESSAGE, CONFIRM, CANCEL ], account).then(function (txs) {
                SweetAlert.swal({
                    title: txs[TITLE],
                    text: txs[MESSAGE],
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonText: txs[CONFIRM],
                    cancelButtonText: txs[CANCEL]
                }, function (confirmed) {
                    if (confirmed) {
                        node.remove();
                    }
                });
            });
        }

        /**
         * Clone the supplied account.
         *
         * @param {ChartOfAccounts~Account} account - The account to be cloned.
         */
        function cloneAccount(account) {
            var newAccount = _.cloneDeep(account),
                parent = findParent(account);
            newAccount.id = undefined;
            newAccount.ownerCompanyId = vm.companyId;
            newAccount.codice = nextAccountCode(parent.children, account.level);
            newAccount.materializedPath = _.replace(newAccount.materializedPath, account.codice, newAccount.codice);
            refactorAccounts(account.materializedPath, newAccount);
            parent.children.push(newAccount);

            function findParent(account) {
                var parentCode = account.materializedPath.substr(0, account.materializedPath.lastIndexOf('.'));
                return findNested(vm.ngModel, parentCode);

                function findNested(root, parentCode) {
                    if (root.materializedPath === parentCode) {
                        return root;
                    }
                    for (var i = 0; i < root.children.length; i++) {
                        var parent = findNested(root.children[i], parentCode);
                        if (angular.isDefined(parent)) {
                            return parent;
                        }
                    }
                }
            }

            function refactorAccounts(path, parent) {
                _.forEach(parent.children, function (childAccount) {
                    var currentPath = childAccount.materializedPath;
                    childAccount.id = undefined;
                    childAccount.ownerCompanyId = vm.companyId;
                    childAccount.materializedPath = _.replace(childAccount.materializedPath, path, parent.materializedPath)
                    refactorAccounts(currentPath, childAccount.children);
                });
            }

            /**
             * Extract the next account code from rootNode.
             *
             * @param {ChartOfAccounts~Account[]} accounts - The accounts to extract next code.
             * @returns {string} - The next account code.
             */
            function nextAccountCode(accounts, level) {
                if (!accounts || accounts.length === 0) {
                    return _.padStart('1', accountsCodeSizes[level] || 5, '0');
                }
                return _.padStart(parseInt(_.maxBy(accounts, 'codice').codice) + 1, accountsCodeSizes[level] || 5, '0');
            }
        }
    }

})();

