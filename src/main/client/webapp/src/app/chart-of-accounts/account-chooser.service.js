/*
 * centre-cost-choose.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .factory('AccountChooserService', AccountChooserService);

    AccountChooserService.$inject = ['$uibModal', 'ChartOfAccountsService', '_'];

    /* @ngInject */
    function AccountChooserService($uibModal, ChartOfAccountsService, _) {
        var service = {
            open: open
        };
        return service;

        ////////////////

        /**
         * The options used to open the centre cost chooser.
         *
         * @typedef {Object} ChartOfAccounts~AccountChooserOptions
         * @property {number} chartOfAccountsId - The chart of accounts to use
         * @property {number} companyId - The unique id of the company
         * @property {boolean} multiple - True to set the dialog to choose multiple accounts
         * @property {boolean} returnObjects - True to return account objects instead of account materialized path.
         * @property {boolean} vm.hideCode - True to hide the account codes.
         * @property {string} rootAccount - The root account to show on choose.
         * @property {boolean} keyboard - Indicates whether the dialog should be closable by hitting the ESC key.
         * @property {string} openedClass - Class added to the body element when the modal is opened.
         * @property {boolean} includeAddressBookItems - True to include address book items.
         * @property {boolean} allowAccounts - True to allow the selection of accounts.
         * @property {boolean} allowAll  - True to allow the selection of all levels
         * @property {boolean} includeOnly - True to include only supplied materialized paths.
         */

        /**
         * The result of the open function.
         *
         * @typedef {Object} ChartOfAccounts~AccountChooserResult
         * @property {function} close - Can be used to close a modal, passing a result.
         * @property {function} dismiss - Can be used to dismiss a modal, passing a reason.
         * @property {promise} result -Is resolved when a modal is closed and rejected when a modal is dismissed.
         */

        /**
         * Open will open the centre cost chooser.
         *
         * @param {ChartOfAccounts~AccountChooserOptions} options - The options to open the chooser.
         * @returns {ChartOfAccounts~AccountChooserResult} - The object used to manage the chooser.
         */
        function open(options) {

            
            return $uibModal.open({
                bindToController: true,
                controllerAs: 'vm',
                controller: AccountChooserController,
                keyboard: options.keyboard,
                openedClass: options.openedClass,
                templateUrl: 'app/chart-of-accounts/account-chooser.html',
                resolve: {
                    hideCode: _.constant(options.hideCode),
                    returnObjects: _.constant(options.returnObjects),
                    multiple: _.constant(options.multiple),
                    allowAccounts: _.constant(options.allowAccounts),
                    allowAll: _.constant(options.allowAll),
                    includeAddressBookItems: _.constant(options.includeAddressBookItems),
                    data: ChartOfAccountsService.getOne(options.chartOfAccountsId, options.companyId, options.includeAddressBookItems).then(function (chartOfAccounts) {
                        return ChartOfAccountsService.extractSubtree(chartOfAccounts, options.rootAccount, options.includeOnly);
                    }),
                    rootAccountTree: ChartOfAccountsService.getOne(options.chartOfAccountsId, options.companyId, options.includeAddressBookItems).then(function (chartOfAccounts) {
                        return ChartOfAccountsService.getAllAccountTraversingPath(chartOfAccounts,options.rootAccount)
                    })
                },
                windowClass: 'account-chooser-modal'
            });
        }
    }

    AccountChooserController.$inject = ['$uibModalInstance', 'hideCode', 'returnObjects', 'multiple', 'allowAccounts', 'allowAll', 'includeAddressBookItems', 'data', 'rootAccountTree','_'];

    /* @ngInject */
    function AccountChooserController($uibModalInstance, hideCode, returnObjects, multiple, allowAccounts, allowAll, includeAddressBookItems, data, rootAccountTree, _) {
        var vm = this;

        vm.selectedAccounts = [];
        vm.selected = {};
        vm.multiple = multiple;
        vm.hideCode = hideCode;
        vm.allowAccounts = allowAccounts;
        vm.allowAll = allowAll;
        vm.includeAddressBookItems = includeAddressBookItems;
        vm.chartOfAccounts = data;
        vm.doChoose = doChoose;
        vm.doConfirm = doConfirm;
        vm.isSelectable = isSelectable;
        vm.isSelected = isSelected;
        vm.toggleAccount = toggleAccount;
        vm.rootAccountTree = rootAccountTree;

        activate();

        ////////////////

        function activate() {
            // Nothing to do.
        }

        function toggleAccount(account) {
            toggleAllFrom(account, account.materializedPath);
        }

        function toggleAllFrom(root, path) {
            var selected = vm.selected[path];
            if (isSelectable(root)) {
                vm.selectedAccounts.push(root);
            }
            for (var i = 0; i < root.children.length; i++) {
                var account = root.children[i];
                if (_.startsWith(account.materializedPath, path)) {
                    vm.selected[account.materializedPath] = selected;
                    if (isSelectable(account) && selected) {
                        vm.selectedAccounts.push(account);
                    } else {
                        _.remove(vm.selectedAccounts, { id: account.id });
                    }
                }
                toggleAllFrom(account, path);
            }
        }

        function isSelectable(account) {
            return ( vm.allowAll ||  (account.level !== 'GRUPPO' && account.level !== 'MASTRO' && (account.level !== 'CONTO' || vm.allowAccounts) ));
        }

        function isSelected(account) {
            var path = account.materializedPath;
            return vm.selected[path] === true;
        }

        function doChoose(account) {
            if (returnObjects) {
                $uibModalInstance.close(account);
            } else {
                $uibModalInstance.close(account.materializedPath);
            }
        }

        function doConfirm() {
            if (returnObjects) {
                $uibModalInstance.close(_.filter(vm.selectedAccounts, isSelectable));
            } else {
                $uibModalInstance.close(_.map(_.filter(vm.selectedAccounts, isSelectable), 'materializedPath'));
            }
        }
    }

})();

