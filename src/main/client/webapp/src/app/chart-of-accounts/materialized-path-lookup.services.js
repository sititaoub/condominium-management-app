/*
 * materialized-path-lookup.services.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .factory('MaterializedPathLookupService', MaterializedPathLookupService);

        MaterializedPathLookupService.$inject = ['$q', 'Restangular', 'CacheFactory', 'contextAddressAggregatori'];

    /* @ngInject */
    function MaterializedPathLookupService($q, Restangular, CacheFactory, contextAddressAggregatori) {
        var CACHE_NAME = 'MaterializedPathLookupCache';
        var CACHE_EXPIRATION_MS = 12 * 60 * 60 * 1000; //12 hours 
        var pendingQueries = {};

        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressAggregatori + 'v2');
        });

        if (!CacheFactory.get(CACHE_NAME)) {
            CacheFactory.createCache(CACHE_NAME, {
                deleteOnExpire: 'passive',
                maxAge: CACHE_EXPIRATION_MS
            });
        }
        var cache = CacheFactory.get(CACHE_NAME);

        var service = {
            getOne: getOne
        };
        return service;

        ////////////////

        /**
         * A materializedPath Lookup
         *
         * @typedef {Object} MaterializedPathLookupDTO~Chart
         * @property {String} applicationId     - the applicationId.
         * @property {number} chartOfAccountsId - The unique identifier of chart of account.
         * @property {number} companyId         - The unique identifier of company.
         * @property {String} key               - The key ofa materialize path.
         * @property {String} materializedPath  - The unique identifier of company.
         */


        /**
         * Retrieve a single chart of accounts for supplied unique identifier.
         *
         * @param {number} companyId   - The unique identifier of company to retrieve lookup matPath for.
         * @param {number} chartOfAccountsId - The unique identifier to retrieve  lookup matPath for.
         * @param {String} matPathkey  - the key of key
         * @returns {Promise.<MaterializedPathLookupDTO~Chart|Error>} - The promise over  lookup matPath detail.
         */
        function getOne(companyId, chartOfAccountsId, matPathkey) {
            var deferred = $q.defer();

            var key = '/companies/' + companyId + '/chart-of-accounts/' + chartOfAccountsId;
                key += '/key/' + matPathkey;
            var result = cache.get(key);
            if (result) {
                deferred.resolve(result);
            } else if (pendingQueries[key]) {
                return pendingQueries[key];
            } else {
                pendingQueries[key] = deferred.promise;

                Api.one('companies', companyId).one('chart-of-accounts',chartOfAccountsId).
                one("materializedpath-lookups", matPathkey).get({
                    applicationId: 'management-app'
                    }).then(function (data) {
                    cache.put(key, data);
                    pendingQueries[key] = undefined;
                    deferred.resolve(data);
                }).catch(deferred.reject);
            }

            return deferred.promise;
        }

   
    }

})();

