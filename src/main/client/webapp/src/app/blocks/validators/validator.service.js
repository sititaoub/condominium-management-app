/*
 * validator.service.js
 *
 * (C) 2016-2017 Cedac Software S.r.l.
 */
(function () {
  'use strict';

  angular
    .module('blocks.validators')
    .factory('ValidatorService', ValidatorService);

  ValidatorService.$inject = [];

  /* @ngInject */
  function ValidatorService() {
    // Constants
    var ZERO_CHAR_CODE = '0'.charCodeAt(0);
    var A_CHAR_CODE = 'A'.charCodeAt(0);
    var SET_1 = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var SET_2 = 'ABCDEFGHIJABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var EVEN = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var ODD = 'BAKPLCQDREVOSFTGUHMINJWZYX';


    var service = {
      validateTaxCode: validateTaxCode,
      validateVatCode: validateVatCode
    };
    return service;

    ////////////////

    function validateTaxCode(taxCode) {
      if (!taxCode) {
        return false;
      }
      if (taxCode.length === 11 || taxCode.length === 13) {
        return service.validateVatCode(taxCode);
      }
      if (taxCode.length !== 16) {
        return false;
      }
      taxCode = taxCode.toUpperCase();
      var sum = 0, i;
      for (i = 1; i <= 13; i += 2) {
        sum += EVEN.indexOf(SET_2.charAt(SET_1.indexOf(taxCode.charAt(i))));
      }
      for (i = 0; i <= 14; i += 2) {
        sum += ODD.indexOf(SET_2.charAt(SET_1.indexOf(taxCode.charAt(i))));
      }
      return (sum % 26) === taxCode.charCodeAt(15) - A_CHAR_CODE;
    }

    function validateVatCode(vatCode) {
      if (!vatCode) {
        return false;
      }
      if (vatCode.length === 13) {
        vatCode = vatCode.substring(2);
      }
      if (vatCode.length !== 11) {
        return false;
      }

      var sum = 0, i, c;
      for (i = 0; i <= 9; i += 2) {
        sum += vatCode.charCodeAt(i) - ZERO_CHAR_CODE;
      }
      for (i = 1; i <= 9; i += 2) {
        c = 2 * (vatCode.charCodeAt(i) - ZERO_CHAR_CODE);
        if( c > 9 ) {
          c -= 9;
        }
        sum += c;
      }
      return ((10 - sum % 10) % 10) === (vatCode.charCodeAt(10) - ZERO_CHAR_CODE);
    }
  }

})();

