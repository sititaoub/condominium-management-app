/*
 * vat-code-input.component.js
 *
 * (C) 2016-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('blocks.controls')
        .component('vatCodeInput', {
            controller: VatCodeInputController,
            bindings: {
                id: '@?',
                name: '@?',
                ngModel: '<',
                required: '<',
                disabled: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            template: '<input type="text" class="form-control" name="{{$ctrl.name}}" id="{{$ctrl.id}}"' +
                            ' ng-model="$ctrl.value" maxlength="11" ng-required="$ctrl.required"' +
                            ' ng-disabled="$ctrl.disabled" ng-minlength="11" ng-pattern="/[0-9]{11}/"' +
                            ' ng-change="$ctrl.onChange()" ui-validate="{ invalidCheck: \'$ctrl.checkDigit($value)\'}"' +
                            ' invalidCheck-err-type="vatCodeInvalid">'
        });

    VatCodeInputController.$inject = ['ValidatorService'];

    /* @ngInject */
    function VatCodeInputController(ValidatorService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.checkDigit = checkDigit;

        //////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue($ctrl.value);
        }

        function checkDigit(value) {
            if (angular.isDefined(value) && value !== null && value !== '') {
                return ValidatorService.validateVatCode(value);
            }
            return true;
        }
    }

})();

