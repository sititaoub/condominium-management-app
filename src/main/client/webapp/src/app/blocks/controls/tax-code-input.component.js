/*
 * tax-code-input.component.js
 *
 * (C) 2016-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('blocks.controls')
        .component('taxCodeInput', {
            controller: TaxCodeInputController,
            bindings: {
                id: '@?',
                name: '@',
                ngModel: '<',
                ngRequired: '<',
                ngDisabled: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            template: '<input type="text" class="form-control" name="{{$ctrl.name}}" id="{{$ctrl.id}}"' +
                            ' ng-model="$ctrl.value" maxlength="16" ng-required="$ctrl.ngRequired" ng-disabled="$ctrl.ngDisabled"' +
                            ' ng-pattern="/([0-9]{11})|([a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z])/" ' +
                            ' ng-change="$ctrl.onChange()" ui-validate="{ invalidCheck: \'$ctrl.checkDigit($value)\' }"' +
                            ' invalidCheck-err-type="taxCodeInvalid">'
        });

    TaxCodeInputController.$inject = ['ValidatorService'];

    /* @ngInject */
    function TaxCodeInputController(ValidatorService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.checkDigit = checkDigit;

        //////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue($ctrl.value);
        }

        function checkDigit(value) {
            if (angular.isDefined(value) && value !== '') {
                return ValidatorService.validateTaxCode(value);
            }
            return true;
        }
    }

})();

