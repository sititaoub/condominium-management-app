/*
 * geocoding.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('blocks.maps')
        .factory('GeocodingService', GeocodingService);

    GeocodingService.$inject = ['$q', '$log'];

    /* @ngInject */
    function GeocodingService($q, $log) {
        var service = {
            getAddressForGeocoding: getAddressForGeocoding,
            geocode: geocode
        };
        return service;

        ////////////////

        function getAddressForGeocoding(address) {
            return (address.streetName || '') + ' ' + (address.buildingNumber || '') + ' ' + (address.district || '') + ' ' + (address.postalCode || '');
        }

        function geocode(address) {
            if (angular.isObject(address)) {
                if (!address.district) {
                    return $q.reject();
                }
                return gmapGeocode(getAddressForGeocoding(address), [address.town]);
            } else {
                return gmapGeocode(address, []);
            }
        }

        function gmapGeocode(address, params) {
            var deferred = $q.defer();

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({address: address}, function (result, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    var latLng = {
                        lat: result[0].geometry.location.lat(),
                        lng: result[0].geometry.location.lng()
                    };

                    $log.debug('lat:', latLng.lat, 'lng:', latLng.lng);

                    deferred.resolve({latLng: latLng, params: params, status: status});
                } else if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
                    $log.info('gmap Zero results for geocoding address', address);
                    deferred.reject({status: status});
                } else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                    $log.info('gmap Query limit hit');
                    deferred.reject({status: status});
                } else if (status === google.maps.GeocoderStatus.REQUEST_DENIED) {
                    $log.info('gmap Request denied for geocoding address', address);
                    deferred.reject({status: status});
                } else if (status === google.maps.GeocoderStatus.INVALID_REQUEST) {
                    $log.info('gmap Invalid request for geocoding address ' + address);
                    deferred.reject({status: status});
                } else {
                    deferred.reject();
                }
            });


            return deferred.promise;
        }

    }

})();

