/*
 * core.config.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('blocks.maps')
        .config(configCore);

        configCore.$inject = ['$translatePartialLoaderProvider', 'uiGmapGoogleMapApiProvider', 'googleMapsApiKey'];

        /* @ngInject */
        function configCore($translatePartialLoaderProvider, uiGmapGoogleMapApiProvider, googleMapsApiKey) {
            $translatePartialLoaderProvider.addPart('blocks/maps')

            uiGmapGoogleMapApiProvider.configure({
                key: googleMapsApiKey,
                v: '3',
                libraries: 'weather,geometry,visualization'
            });
        }

})();
