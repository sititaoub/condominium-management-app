/*
 * maps.module.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('blocks.maps', [
            'ui.bootstrap',
            'uiGmapgoogle-maps',
            'pascalprecht.translate'
        ]);

})();