/*
 * google-map-address.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('blocks.maps')
        .component('googleMapAddress', {
            controller: GoogleMapAddressController,
            bindings: {
                id: '@',
                ngModel: '<',
                readonly: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/blocks/maps/google-map-address.html'
        });

    GoogleMapAddressController.$inject = ['GeocodingService', 'ChoosePositionService',  'uiGmapGoogleMapApi', 'uiGmapIsReady'];

    /* @ngInject */
    function GoogleMapAddressController(GeocodingService, ChoosePositionService, uiGmapGoogleMapApi, uiGmapIsReady) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.$onChanges = onChanges;
        $ctrl.openPositionModal = openPositionModal;

        ////////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
            $ctrl.map = {
                control: {},
                showMap: true,
                zoom: 13,
                options: {
                    draggable: false,
                    clickableIcons: false,
                    disableDefaultUI: true,
                    disableDoubleClickZoom: true,
                    draggableCursor: false,
                    gestureHandling: "none",
                    keyboardShortcuts: false,
                    scrollwheel: false,
                    backgroundColor: "red"
                },
                markersEvents: {
                    click: function(marker, eventName, model) {
                        $ctrl.map.window.model = model;
                        $ctrl.map.window.show = true;
                    }
                },
                window: {
                    marker: {},
                    show: false,
                    closeClick: function () {
                        this.show = false;
                    },
                    options: {} // define when map is ready
                },
                center: {
                    latitude: null,
                    longitude: null
                }
            };
        }

        function onChanges(changes) {
            if (angular.isDefined(changes.ngModel) && angular.isDefined(changes.ngModel.currentValue)) {
                $ctrl.value = angular.copy(changes.ngModel.currentValue);
                geocodeCurrentAddress();
            }
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue($ctrl.value);
            $ctrl.ngModelCtrl.$setDirty();
        }

        function geocodeCurrentAddress() {
            uiGmapGoogleMapApi.then(function (maps) {
                maps.visualRefresh = true;
                if (angular.isDefined($ctrl.value)) {
                    if ($ctrl.value.lat && $ctrl.value.lng) {
                        setMapPosition($ctrl.value.lat, $ctrl.value.lng);
                    } else {
                        GeocodingService.geocode($ctrl.value).catch(function () {
                            return GeocodingService.geocode({
                                district: $ctrl.value ? $ctrl.value.district : '',
                                streetName: '',
                                buildingNumber: '',
                                postalCode: ''
                            });
                        }).then(function (data) {
                            setMapPosition(data.latLng.lat, data.latLng.lng, true);
                        }).catch(function () {
                            setMapPosition(44.494193, 11.346717, true)
                        });
                    }
                }
            });
        }

        function setMapPosition(lat, lng, draggable) {
            $ctrl.map.center.latitude = lat;
            $ctrl.map.center.longitude = lng;
            if ($ctrl.map.markers && $ctrl.map.markers[0].control.getGMarkers && $ctrl.map.markers[0].control.getGMarkers()) {
                $ctrl.map.markers[0].control.getGMarkers()[0].setPosition({ "lat": lat, "lng": lng });
            }
            if (!$ctrl.map.markers) {
                $ctrl.map.markers = [{
                    id: "first",
                    location: {
                        latitude: lat,
                        longitude: lng
                    },
                    options: {
                        draggable: !draggable,
                        clickable: false
                    },
                    control: {}
                }];
            } else {
                $ctrl.map.markers[0].location.latitude = lat;
                $ctrl.map.markers[0].location.longitude = lng;
            }
            uiGmapIsReady.promise().then(function () {
                $ctrl.map.control.refresh();
            });
        }

        function openPositionModal() {
            ChoosePositionService.open({
                address: GeocodingService.getAddressForGeocoding($ctrl.value)
            }).result.then(function (latLng) {
                $ctrl.value.lat = latLng.lat;
                $ctrl.value.lng = latLng.lng;
                setMapPosition(latLng.lat, latLng.lng);
                onChange();
            });
        }
    }

})();

