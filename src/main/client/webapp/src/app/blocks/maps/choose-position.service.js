/*
 * choose-position.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('blocks.maps')
        .factory('ChoosePositionService', ChoosePositionService);

    ChoosePositionService.$inject = ['$uibModal'];

    /* @ngInject */
    function ChoosePositionService($uibModal) {
        var service = {
            open: open
        };
        return service;

        ////////////////

        /**
         * The options used to open the centre cost chooser.
         *
         * @typedef {Object} Maps~ChoosePositionOptions
         * @property {Maps~Address} address - The address to use
         */

        /**
         * The result of the open function.
         *
         * @typedef {Object} Maps~ChoosePositionResult
         * @property {function} close - Can be used to close a modal, passing a result.
         * @property {function} dismiss - Can be used to dismiss a modal, passing a reason.
         * @property {promise} result -Is resolved when a modal is closed and rejected when a modal is dismissed.
         */

        /**
         * Open will open the centre cost chooser.
         *
         * @param {ChartOfAccounts~AccountChooserOptions} options - The options to open the chooser.
         * @returns {ChartOfAccounts~AccountChooserResult} - The object used to manage the chooser.
         */

        function open(options) {
            return $uibModal.open({
                bindToController: true,
                controllerAs: 'vm',
                controller: ChoosePositionController,
                templateUrl: 'app/blocks/maps/choose-position.html',
                resolve: {
                    address: function () { return options.address }
                }
            });
        }
    }

    ChoosePositionController.$inject = ['$uibModalInstance', 'address', 'GeocodingService', 'uiGmapGoogleMapApi', 'uiGmapIsReady'];

    function ChoosePositionController($uibModalInstance, address, GeocodingService, uiGmapGoogleMapApi, uiGmapIsReady) {
        var vm = this;

        vm.doSave = doSave;
        vm.centerToUserPosition = centerToUserPosition;
        vm.centerToUserAddress = centerToUserAddress;

        activate();

        ////////////////

        function activate() {
            vm.address = address;
            vm.map = {
                control: {},
                showMap: true,
                zoom: 16,
                options: {
                },
                markersEvents: {
                    click: function (marker, eventName, model) {
                        vm.map.window.model = model;
                        vm.map.window.show = true;
                    }
                },
                window: {
                    marker: {},
                    show: false,
                    closeClick: function () {
                        this.show = false;
                    },
                    options: {} // define when map is ready
                },
                center: {
                    latitude: null,
                    longitude: null
                }
            };
            geocodeCurrentAddress();
        }

        function doSave() {
            $uibModalInstance.close({
                lat: vm.map.markers[0].location.latitude,
                lng: vm.map.markers[0].location.longitude
            });
        }

        function centerToUserPosition() {
            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    setMapPosition(position.coords.latitude, position.coords.longitude, true);
                }, function () {
                    setMapPosition(44.494193, 11.346717, true);
                });
            } else {
                setMapPosition(44.494193, 11.346717, true);
            }
        }

        function centerToUserAddress() {
            geocodeCurrentAddress();
        }

        function geocodeCurrentAddress() {
            uiGmapGoogleMapApi.then(function (maps) {
                maps.visualRefresh = true;
                GeocodingService.geocode(vm.address).then(function (data) {
                    setMapPosition(data.latLng.lat, data.latLng.lng);
                }).catch(function () {
                    setMapPosition(44.494193, 11.346717)
                });
            });
        }

        function setMapPosition(lat, lng) {
            vm.map.center.latitude = lat;
            vm.map.center.longitude = lng;
            if (vm.map.markers && vm.map.markers[0].control.getGMarkers && vm.map.markers[0].control.getGMarkers()) {
                vm.map.markers[0].control.getGMarkers()[0].setPosition({ "lat": lat, "lng": lng });
            }
            if (!vm.map.markers) {
                vm.map.markers = [{
                    id: "first",
                    location: {
                        latitude: lat,
                        longitude: lng
                    },
                    options: {
                        draggable: true,
                        clickable: false
                    },
                    control: {}
                }];
            } else {
                vm.map.markers[0].location.latitude = lat;
                vm.map.markers[0].location.longitude = lng;
            }
            uiGmapIsReady.promise().then(function () {
                vm.map.control.refresh();
            });
        }
    }

})();

