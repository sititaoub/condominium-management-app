/*
 * accunt-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .component('amountRangeFilter', {
            controller: AmountRangeFilter,
            bindings: {
                id: '@',
                name: '@',
                ngModel: '<',
                ngRequired: '<',
                ngDisabled: '<?',
                placeholder: '@',
                filterFromName: '@',
                filterToName: '@',
                size: '@?'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/accounting-writing/amount-range-filter.html'
        });

    AmountRangeFilter.$inject = ['$uibModal', 'NotifierService', '$filter'];

    /* @ngInject */
    function AmountRangeFilter($uibModal, NotifierService, $filter) {
        var $ctrl = this;
        var chartOfAccounts;

        $ctrl.$onInit = onInit;
        $ctrl.$onChanges = onChanges;
        $ctrl.openLookup = openLookup;
        $ctrl.remove = remove;

        ////////////////

        function onInit() {
            $ctrl.description = undefined;
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
                $ctrl.filterIsEmpty = true;

                $ctrl.filterIsEmpty = angular.isUndefined($ctrl.value.filter()[$ctrl.filterFromName]) && angular.isUndefined($ctrl.value.filter()[$ctrl.filterToName]);
                   
                if (!$ctrl.filterIsEmpty){
                    var startDescription = angular.isDefined($ctrl.value.filter()[$ctrl.filterFromName]) ? $filter('currency')($ctrl.value.filter()[$ctrl.filterFromName]) : "";
                    var endDescription   = angular.isDefined($ctrl.value.filter()[$ctrl.filterToName]) ? $filter('currency')($ctrl.value.filter()[$ctrl.filterToName]) : "";
                    $ctrl.description = startDescription + " - " + endDescription;
                }
            
            };
        }

        function onChanges(changes) {

        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue($ctrl.value);
        }



        function remove() {
            $ctrl.value.filter()[$ctrl.filterFromName] = undefined;
            $ctrl.value.filter()[$ctrl.filterToName]  = undefined;
            $ctrl.filterIsEmpty = true;
            $ctrl.description = undefined;
            onChange();
        }

        function openLookup(options) {
            
            
            $ctrl.startAmount = $ctrl.value.filter()[$ctrl.filterFromName];
            $ctrl.endAmount   = $ctrl.value.filter()[$ctrl.filterToName];
            return $uibModal.open({
                bindToController: true,
                controllerAs: '$ctrl',
                controller: AmountRangeFilterController,
                keyboard: $ctrl.keyboard,
                openedClass: $ctrl.openedClass,
                templateUrl: 'app/accounting-writing/amount-range-filter-popup.html',
                resolve: {
                    startAmount: _.constant($ctrl.startAmount),
                    endAmount:   _.constant($ctrl.endAmount)
                },
                windowClass: 'range-chooser-modal'
            }).result.then(function (result) {

                var startDescription = angular.isDefined(result.startAmount) ? $filter('currency')(result.startAmount)  : "";
                var endDescription = angular.isDefined(result.endAmount) ? $filter('currency')(result.endAmount)  : "";
                $ctrl.description = startDescription + " - " + endDescription;
                $ctrl.value.filter()[$ctrl.filterFromName] = angular.isDefined(result.startAmount) ?  result.startAmount : undefined;
                $ctrl.value.filter()[$ctrl.filterToName]   = angular.isDefined(result.endAmount)   ?  result.endAmount : undefined;
                $ctrl.filterIsEmpty = angular.isUndefined(result.startAmount) && angular.isUndefined(result.endAmount);
                
                onChange();
            });;
        }


    }

    AmountRangeFilterController.$inject = ['$uibModalInstance', '_','NotifierService','startAmount','endAmount'];

    /* @ngInject */
    function AmountRangeFilterController($uibModalInstance, _, NotifierService, startAmount, endAmount) {
        var $ctrl = this;

        $ctrl.doConfirm = doConfirm;

        activate();

        ////////////////

        function activate() {
            
            if(angular.isDefined(startAmount))
                $ctrl.startAmount = startAmount;  
            if(angular.isDefined(endAmount))
                $ctrl.endAmount = endAmount;       
        }


        function doConfirm() {
            if ($ctrl.endAmount === '')
                $ctrl.endAmount = undefined;

            if (angular.isDefined($ctrl.startAmount) && angular.isDefined($ctrl.endAmount) && !($ctrl.endAmount === '') 
                && ($ctrl.startAmount > $ctrl.endAmount))
                return NotifierService.notifyError('AmountRangeFilter.RangeAmountError');
            else {
                $ctrl.result = {};
                $ctrl.result.startAmount = $ctrl.startAmount;
                $ctrl.result.endAmount = $ctrl.endAmount;
                $uibModalInstance.close($ctrl.result);
            }
        }
    }

})();

