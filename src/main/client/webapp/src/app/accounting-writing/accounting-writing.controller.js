/*
 * accounting-record.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingWriting')
        .controller('AccountingWritingController', AccountingWritingController);

        AccountingWritingController.$inject = ['$state', '$uibModal', 'AccountingWritingService', 'AccountingTransactionService', 'NgTableParams','NotifierService','FinancialPeriodService'];

    /* @ngInject */
    function AccountingWritingController($state, $uibModal,AccountingWritingService, AccountingTransactionService, NgTableParams,NotifierService,FinancialPeriodService) {
        var vm = this;
        vm.toggleFilters = toggleFilters;
        activate();
        loadFinancialPeriod();
        ////////////////

        function activate() {
            vm.loading = true;
            vm.firstLoad = true;
            vm.filterDateOpened = false;
            vm.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10
            }, {
                    getData: loadAccountingWritings,
                });

            vm.showFilters = vm.tableParams.hasFilter();
        }

        function toggleFilters() {
            vm.showFilters = !vm.showFilters;
        }

        function loadAccountingWritings(params) {
            return AccountingWritingService.getPage($state.params.companyId, $state.params.periodId,
                params.page() - 1, params.count(), params.sorting(), params.filter()).then(function (page) {
                    params.total(page.totalElements);
                    vm.firstLoad = false;
               
                    var result = _.map(page.content, function(writing){
                        return _.defaults({
                            rowsNumber: writing.details['TO_GIVE'].length + writing.details['TO_GIVE'].length + 1
                            
                        }, writing)
                    });

                    return result;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    vm.loading = false;
                });
        }


        function loadFinancialPeriod() {
            FinancialPeriodService.getOne($state.params.companyId, $state.params.periodId).then(function (period) {
                vm.period = period;
                vm.periodLoaded = true;
            }).catch(function () {
                NotifierService.notifyError();
            })
        }

    }


 

})();

