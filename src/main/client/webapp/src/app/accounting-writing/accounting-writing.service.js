/*
 * accounting-transaction.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingWriting')
        .factory('AccountingWritingService', AccountingWritingService);

        AccountingWritingService.$inject = ['$q',  'Restangular', 'contextAddressAggregatori', 'CacheFactory', '_'];

    /* @ngInject */
    function AccountingWritingService($q, Restangular, contextAddressAggregatori,  CacheFactory,  _) {
        var AccountingTransactions = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressAggregatori + 'v2');

        });

        var CACHE_NAME = 'AccountingWritingTypeListCache';
        var CACHE_EXPIRATION_MS = 60 * 60 * 1000;
        var pendingQueries = {};

        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressAggregatori + 'v2');
        });

        if (!CacheFactory.get(CACHE_NAME)) {
            CacheFactory.createCache(CACHE_NAME, {
                deleteOnExpire: 'passive',
                maxAge: CACHE_EXPIRATION_MS
            });
        }
        var cache = CacheFactory.get(CACHE_NAME);

        var service = {
            getPage: getPage,
            getTypeList: getTypeList
        };
        return service;



        /**
         * Manage generic transaction
         */
        function getPage(companyId, financialPeriodId, page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                financialPeriodId: financialPeriodId,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return AccountingTransactions.one("companies", companyId)
                .all("accounting-writings").get("", params);
        }

        
        function getTypeList(companyId, visibleOnly) {
            var deferred = $q.defer();

            var key = '/typelist/' + visibleOnly;
 
            var result = cache.get(key);
            if (result) {
                deferred.resolve(result);
            } else if (pendingQueries[key]) {
                return pendingQueries[key];
            } else {
                pendingQueries[key] = deferred.promise;

                AccountingTransactions.one("companies", companyId).all("accounting-writings").all('/types').get("",{
                    visibleOnly: visibleOnly
                }).then(function (data) {
                    cache.put(key, data);
                    pendingQueries[key] = undefined;
                    deferred.resolve(data);
                }).catch(deferred.reject);
            }

            return deferred.promise;
        }



    }

})();

