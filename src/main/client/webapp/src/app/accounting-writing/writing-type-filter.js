/*
 * accunt-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .component('writingTypeFilter', {
            controller: WritingTypeFilter,
            bindings: {
                id: '@',
                name: '@',
                ngModel: '<',
                ngRequired: '<',
                ngDisabled: '<?',
                placeholder: '@',
                size: '@?',
                companyId: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/accounting-writing/writing-type-filter.html'
        });

    WritingTypeFilter.$inject = ['$uibModal', 'NotifierService', '$filter'];

    /* @ngInject */
    function WritingTypeFilter($uibModal, NotifierService, $filter) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.$onChanges = onChanges;
        $ctrl.openLookup = openLookup;
        $ctrl.remove = remove;

        ////////////////

        function onInit() {
            $ctrl.description = undefined;
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
                $ctrl.filterIsEmpty = true;
            };
        }

        function onChanges(changes) {

        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue($ctrl.value);
        }



        function remove() {
            $ctrl.value = undefined;
            $ctrl.filterIsEmpty = true;
            $ctrl.description = undefined;
            onChange();
        }

        function openLookup(options) {

            $ctrl.writingType = $ctrl.value;
            return $uibModal.open({
                bindToController: true,
                controllerAs: '$ctrl',
                controller: WritingTypeFilterController,
                keyboard: $ctrl.keyboard,
                openedClass: $ctrl.openedClass,
                templateUrl: 'app/accounting-writing/writing-type-filter-popup.html',
                resolve: {
                    writingType: _.constant($ctrl.writingType),
                    companyId: $ctrl.companyId
                },
                windowClass: 'range-chooser-modal'
            }).result.then(function (result) {

                $ctrl.description =   $filter('translate')('UserDefinedTransaction.Types.' + result.writingType);
                $ctrl.value = angular.isDefined(result.writingType) ? result.writingType : undefined;
                $ctrl.filterIsEmpty = angular.isUndefined(result.writingType);
                $ctrl.dataFromModel = $ctrl.writingType;

                onChange();
            });;
        }


    }

    WritingTypeFilterController.$inject = ['$uibModalInstance', '$filter', '_', 'NotifierService', 'AccountingWritingService', 'writingType','companyId' ];

    /* @ngInject */
    function WritingTypeFilterController($uibModalInstance, $filter, _, NotifierService, AccountingWritingService, writingType, companyId ) {
        var $ctrl = this;
        $ctrl.loading = true;
        $ctrl.doConfirm = doConfirm;
        $ctrl.writingTypeList = null;
        activate();

        ////////////////

        function activate() {

            $ctrl.writingType = writingType;
            loadWritingType();
        }

        function loadWritingType() {
            AccountingWritingService.getTypeList(companyId, false).then(function (list) {
               
                var result = _.map(list, function(type){
                    return _.defaults({
                        name: type,
                        translated: $filter('translate')('UserDefinedTransaction.Types.' + type)
                    }, type)
                });
                $ctrl.writingTypeList = result;
                $ctrl.loading  = false;
            }).catch(function () {
                NotifierService.notifyError();
            });
        }


        function doConfirm() {
            $ctrl.result = {};
            $ctrl.result.writingType = $ctrl.writingType;
            $uibModalInstance.close($ctrl.result);

        }
    }

})();

