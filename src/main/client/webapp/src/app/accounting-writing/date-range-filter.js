/*
 * accunt-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.chartOfAccounts')
        .component('dateRangeFilter', {
            controller: DateRangeFilter,
            bindings: {
                id: '@',
                name: '@',
                ngModel: '<',
                ngRequired: '<',
                ngDisabled: '<?',
                placeholder: '@',
                filterFromName: '@',
                filterToName: '@',
                size: '@?'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/accounting-writing/date-range-filter.html'
        });

    DateRangeFilter.$inject = ['$uibModal', 'NotifierService', '$filter'];

    /* @ngInject */
    function DateRangeFilter($uibModal, NotifierService, $filter) {
        var $ctrl = this;
        var chartOfAccounts;

        $ctrl.$onInit = onInit;
        $ctrl.$onChanges = onChanges;
        $ctrl.openLookup = openLookup;
        $ctrl.remove = remove;

        ////////////////

        function onInit() {
            $ctrl.description = undefined;
            $ctrl.ngModelCtrl.$render = function () {
   
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
                $ctrl.filterIsEmpty = angular.isUndefined($ctrl.value.filter()[$ctrl.filterFromName]) && angular.isUndefined($ctrl.value.filter()[$ctrl.filterToName]);
                   
                if (!$ctrl.filterIsEmpty){
                    var startDateDescription = angular.isDefined($ctrl.value.filter()[$ctrl.filterFromName]) ? $filter('date')($ctrl.value.filter()[$ctrl.filterFromName], 'dd/MM/yyyy') : "";
                    var endDateDescription   = angular.isDefined($ctrl.value.filter()[$ctrl.filterToName]) ? $filter('date')($ctrl.value.filter()[$ctrl.filterToName], 'dd/MM/yyyy') : "";
                    $ctrl.description = startDateDescription + " - " + endDateDescription;
                }
            

            };
        }

        function onChanges(changes) {

        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue($ctrl.value);
        }



        function remove() {
            $ctrl.value.filter()[$ctrl.filterFromName] = undefined;
            $ctrl.value.filter()[$ctrl.filterToName]  = undefined;
            $ctrl.filterIsEmpty = true;
            $ctrl.description = undefined;
            onChange();
        }

        function openLookup(options) {
            
            $ctrl.startDate = $ctrl.value.filter()[$ctrl.filterFromName];
            $ctrl.endDate   = $ctrl.value.filter()[$ctrl.filterToName];
            return $uibModal.open({
                bindToController: true,
                controllerAs: '$ctrl',
                controller: DateRangeFilterController,
                keyboard: $ctrl.keyboard,
                openedClass: $ctrl.openedClass,
                templateUrl: 'app/accounting-writing/date-range-filter-popup.html',
                resolve: {
                    startDate: _.constant($ctrl.startDate),
                    endDate:   _.constant($ctrl.endDate)
                },
                windowClass: 'range-chooser-modal'
            }).result.then(function (result) {

                var startDateDescription = angular.isDefined(result.startDate) ? $filter('date')(result.startDate, 'dd/MM/yyyy') : "";
                var endDateDescription = angular.isDefined(result.endDate) ? $filter('date')(result.endDate, 'dd/MM/yyyy') : "";

                $ctrl.description = startDateDescription + " - " + endDateDescription;
                $ctrl.value.filter()[$ctrl.filterFromName] = angular.isDefined(result.startDate) ?  $filter('date')(result.startDate, 'yyyy-MM-dd') : undefined;
                $ctrl.value.filter()[$ctrl.filterToName]   = angular.isDefined(result.endDate)   ?  $filter('date')(result.endDate, 'yyyy-MM-dd') : undefined;
                $ctrl.filterIsEmpty = angular.isUndefined(result.startDate) && angular.isUndefined(result.endDate);
                $ctrl.dataFromModel = $ctrl.startDate; 
                
                onChange();
            });;
        }


    }

    DateRangeFilterController.$inject = ['$uibModalInstance', '_','NotifierService','startDate','endDate'];

    /* @ngInject */
    function DateRangeFilterController($uibModalInstance, _, NotifierService, startDate, endDate) {
        var $ctrl = this;

        $ctrl.doConfirm = doConfirm;
        $ctrl.dateFromOpened = false;
        $ctrl.dateToOpened = false;

        activate();

        ////////////////

        function activate() {
            
            if(angular.isDefined(startDate))
                $ctrl.startDate = new Date(startDate);  
            if(angular.isDefined(endDate))
                $ctrl.endDate = new Date(endDate);       
        }


        function doConfirm() {
 
            if ($ctrl.endDate === '' || $ctrl.endDate == null)
                $ctrl.endDate = undefined;

            if (angular.isDefined($ctrl.startDate) && angular.isDefined($ctrl.endDate)
                && ($ctrl.startDate > $ctrl.endDate))
                return NotifierService.notifyError('DataRangeFilter.RangeDateError');
            else {
                $ctrl.result = {};
                $ctrl.result.startDate = $ctrl.startDate;
                $ctrl.result.endDate = $ctrl.endDate;
                $uibModalInstance.close($ctrl.result);
            }
        }
    }

})();

