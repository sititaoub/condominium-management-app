/*
 * installment-plan.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.accountingWriting')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider'];

    /* @ngInject */
    function setupRoutes($stateProvider) {
        $stateProvider.state('index.accounting-writing', {
            url: '/condominiums/{condominiumId}/financial-period/{periodId}/accounting-writing',
            abstract: true,
            template: '<ui-view></ui-view>',
            data: {
                skipFinancialPeriodCheck: false
            },
            onEnter: ['$state', '$transition$', 'FinancialPeriodService', function ($state, $transition$, FinancialPeriodService) {
                // Ensure that if 'current' is provided as financial period, the current financial period is provided.
                if ($transition$.params().periodId === 'current') {
                    var params = angular.copy($transition$.params());
                    params.periodId = FinancialPeriodService.getCurrent($transition$.params().companyId, $transition$.params().condominiumId).id;
                    return $state.target($transition$.targetState().name(), params);
                }
                return true;
            }]    
         }).state('index.accounting-writing.list', {
            url: "?page&count",
            templateUrl: "app/accounting-writing/accounting-writing.html",
            controller: "AccountingWritingController",
            controllerAs: 'vm'
        });
    }

})();
