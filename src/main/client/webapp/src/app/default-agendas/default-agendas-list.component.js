/*
 * default-agendas-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.defaultAgendas')
        .component('defaultAgendasList', {
            controller: DefaultAgendasListController,
            bindings: {},
            templateUrl: 'app/default-agendas/default-agendas-list.html'
        });

    DefaultAgendasListController.$inject = ['$state', 'NgTableParams', 'DefaultAgendaService', 'NotifierService', 'AlertService'];

    /* @ngInject */
    function DefaultAgendasListController($state, NgTableParams, DefaultAgendaService, NotifierService, AlertService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.toggleFilters = toggleFilters;
        $ctrl.cloneDefaultAgenda = cloneDefaultAgenda;
        $ctrl.deleteDefaultAgenda = deleteDefaultAgenda;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10,
                sort: 'description,asc'
            }, {
                getData: loadDefaultAgendas
            });
            $ctrl.showFilters = $ctrl.tableParams.hasFilter();
        }

        function toggleFilters() {
            $ctrl.showFilters = !$ctrl.showFilters;
        }

        function cloneDefaultAgenda(agenda) {
            DefaultAgendaService.clone(agenda.id).then(function () {
                return NotifierService.notifySuccess('DefaultAgendas.Clone.Success');
            }).catch(function () {
                return NotifierService.notifyError('DefaultAgendas.Clone.Failure');
            }).finally(function () {
                $ctrl.tableParams.reload();
            });
        }

        function deleteDefaultAgenda(agenda) {
            AlertService.askConfirm('DefaultAgendas.Remove.Message', agenda).then(function () {
                DefaultAgendaService.remove(agenda.id).then(function () {
                    return NotifierService.notifySuccess('DefaultAgendas.Remove.Success');
                }).catch(function () {
                    return NotifierService.notifyError('DefaultAgendas.Remove.Failure');
                }).finally(function () {
                    $ctrl.tableParams.reload();
                })
            });
        }

        function loadDefaultAgendas(params) {
            $state.go('.', params.uiRouterUrl(), { inherit: false });
            return DefaultAgendaService.getPage(params.page() - 1, params.count(), params.sorting(), params.filter())
                .then(function (data) {
                    params.total(data.totalElements);
                    return data.content;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    $ctrl.loading = false;
                });
        }
    }

})();

