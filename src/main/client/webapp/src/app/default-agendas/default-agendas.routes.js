/*
 * default-agendas.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.defaultAgendas')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider'];

    /* @ngInject */
    function setupRoutes ($stateProvider) {
        $stateProvider.state('index.default-agendas', {
            url: '/default-agendas',
            abstract: true,
            redirectTo: 'index.default-agendas.list',
            template: '<div class="default-agendas-container" ui-view></div>',
            data: {
                skipFinancialPeriodCheck: true
            }
        }).state('index.default-agendas.list', {
            url: '?page&count&sort&system&description',
            component: 'defaultAgendasList',
            params: {
                page: { dynamic: true, squash: true, value: '1', inherits: true },
                count: { dynamic: true, squash: true, value: '10', inherits: true },
                sort: { dynamic: true, squash: true, value: 'description,asc', inherits: true },
                system: { dynamic: true, inherits: true },
                description: { dynamic: true, inherits: true }
            }
        }).state('index.default-agendas.list.create', {
            url: '/create',
            views: {
                '@index.default-agendas': {
                    component: 'defaultAgendaCreate'
                }
            }
        }).state('index.default-agendas.list.edit', {
            url: '/{agendaId}/edit',
            views: {
                '@index.default-agendas': {
                    component: 'defaultAgendaEdit'
                }
            },
            resolve: {
                defaultAgenda: ['$transition$', 'DefaultAgendaService', function ($transition$, DefaultAgendaService) {
                    return DefaultAgendaService.getOne($transition$.params().agendaId);
                }]
            }
        });
    }

})();