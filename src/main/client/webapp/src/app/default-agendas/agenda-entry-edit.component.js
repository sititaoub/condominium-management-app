/*
 * agenda-entry-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.defaultAgendas')
        .component('agendaEntryEdit', {
            controller: AgendaEntryEditComponent,
            bindings: {
                close: '&',
                dismiss: '&',
                resolve: '<'
            },
            templateUrl: 'app/default-agendas/agenda-entry-edit.html'
        });

    AgendaEntryEditComponent.$inject = [];

    /* @ngInject */
    function AgendaEntryEditComponent() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;

        ////////////

        function onInit() {
            $ctrl.entry = $ctrl.resolve.entry;
        }
    }

})();

