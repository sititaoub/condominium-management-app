/*
 * default-agenda-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.defaultAgendas')
        .component('defaultAgendaCreate', {
            controller: DefaultAgendaCreateController,
            bindings: {
            },
            templateUrl: 'app/default-agendas/default-agenda-create.html'
        });

    DefaultAgendaCreateController.$inject = ['$state', 'DefaultAgendaService', 'NotifierService'];

    /* @ngInject */
    function DefaultAgendaCreateController($state, DefaultAgendaService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.defaultAgenda = undefined;
        }

        function doSave() {
            DefaultAgendaService.create($ctrl.defaultAgenda).then(function () {
                return NotifierService.notifySuccess('DefaultAgendas.Create.Success');
            }).then(function () {
                $state.go("^");
            }).catch(function () {
                return NotifierService.notifyError('DefaultAgendas.Create.Failure');
            });
        }
    }

})();

