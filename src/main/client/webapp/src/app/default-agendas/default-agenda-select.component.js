/*
 * default-agenda-select.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.defaultAgendas')
        .component('defaultAgendaSelect', {
            controller: DefaultAgendaSelectController,
            bindings: {
                id: '@?',
                name: '@?',
                controlClass: '@?',
                ngModel: '<',
                ngRequired: '<?',
                ngDisabled: '<?',
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/default-agendas/default-agenda-select.html'
        });

    DefaultAgendaSelectController.$inject = ['DefaultAgendaService'];

    /* @ngInject */
    function DefaultAgendaSelectController(DefaultAgendaService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
            loadAgendas();
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function loadAgendas() {
            DefaultAgendaService.getPage(0, 1000, { description: 'asc' }).then(function (page) {
                $ctrl.agenda = page.content;
            });
        }
    }

})();

