/*
 * default-agenda-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.defaultAgendas')
        .component('defaultAgendaEdit', {
            controller: DefaultAgendaEditController,
            bindings: {
                'defaultAgenda': '<'
            },
            templateUrl: 'app/default-agendas/default-agenda-edit.html'
        });

    DefaultAgendaEditController.$inject = ['$state', 'DefaultAgendaService', 'NotifierService'];

    /* @ngInject */
    function DefaultAgendaEditController($state, DefaultAgendaService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.defaultAgendaId = $ctrl.defaultAgenda.id;
        }

        function doSave() {
            DefaultAgendaService.update($ctrl.defaultAgendaId, $ctrl.defaultAgenda).then(function () {
                return NotifierService.notifySuccess('DefaultAgendas.Edit.Success');
            }).then(function () {
                $state.go("^");
            }).catch(function () {
                return NotifierService.notifyError('DefaultAgendas.Edit.Failure');
            });
        }
    }

})();

