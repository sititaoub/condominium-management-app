/*
 * agenda-entry-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.defaultAgendas')
        .component('agendaEntryCreate', {
            controller: AgendaEntryCreateComponent,
            bindings: {
                close: '&',
                dismiss: '&'
            },
            templateUrl: 'app/default-agendas/agenda-entry-create.html'
        });

    AgendaEntryCreateComponent.$inject = [];

    /* @ngInject */
    function AgendaEntryCreateComponent() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;

        ////////////

        function onInit() {
            $ctrl.entry = {};
        }
    }

})();

