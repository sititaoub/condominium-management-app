/*
 * default-agenda.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.defaultAgendas')
        .factory('DefaultAgendaService', DefaultAgendaService);

    DefaultAgendaService.$inject = ['Restangular', 'contextAddressMeetings', '_'];

    /* @ngInject */
    function DefaultAgendaService(Restangular, contextAddressMeetings, _) {
        var DefaultAgendas = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressMeetings + 'v1');
            configurer.addResponseInterceptor(function (data, operation, what, url, response) {
                if (operation === "post") {
                    var location = response.headers('Location');
                    return location.substr(location.lastIndexOf('/') + 1);
                }
                return data;
            });
        }).all('default-agendas');

        var service = {
            getPage: getPage,
            getOne: getOne,
            create: create,
            update: update,
            remove: remove,
            clone: clone
        };
        return service;

        ////////////////

        /**
         * @namespace DefaultAgendas
         */

        /**
         * Default agendas page.
         *
         * @typedef {Object} DefaultAgendas~DefaultAgendasPage
         * @property {number} number - The requested page number.
         * @property {number} size - The requested page size.
         * @property {number} totalElements - The total number of elements.
         * @property {number} totalPages - The total number of pages.
         * @property {DefaultAgendas~DefaultAgenda[]} content - The page content.
         */

        /**
         * DefaultAgenda object.
         *
         * @typedef {Object} DefaultAgendas~DefaultAgenda
         * @property {number} id - The unique id of default agenda.
         * @property {boolean} system - True if the default agenda is a system one.
         * @property {string} description - Textual description of default agenda.
         * @property {DefaultAgendas~AgendaEntry[]} - The entries of default agenda.
         */

        /**
         * Agenda entry object.
         *
         * @typedef {Object} DefaultAgendas~AgendaEntry
         * @property {string} title - The title of agenda entry.
         * @property {string} description - Full text description (RTF) of entry
         * @property {string} votersRole - The role used for vote
         * @property {number} votingRuleId - The standard default agenda
         * @property {boolean} readonly - True to generate this entry as readonly.
         */

        /**
         * Retrieve a single page of default agendas.
         *
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=10] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<DefaultAgendas~DefaultAgenda|Error>} - The promise of page.
         */
        function getPage(page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return DefaultAgendas.get("", params);
        }

        /**
         * Retrieve a single default agenda.
         *
         * @param {number} id - The unique default agenda id.
         * @returns {Promise<DefaultAgendas~DefaultAgenda|Error>} - The promise of page.
         */
        function getOne(id) {
            return DefaultAgendas.one("", id).get();
        }

        /**
         * Create a new default agenda.
         *
         * @param {DefaultAgendas~DefaultAgenda} defaultAgenda - The default agenda to be created.
         * @returns {Promise<Number|Error>} - The promise of result.
         */
        function create(defaultAgenda) {
            return DefaultAgendas.post(defaultAgenda);
        }

        /**
         * Updates an existing default agenda.
         *
         * @param {number} id - The unique id of default agenda to update.
         * @param {DefaultAgendas~DefaultAgenda} defaultAgenda - The default agenda to be updated
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function update(id, defaultAgenda) {
            return DefaultAgendas.one("", id).customPUT(defaultAgenda);
        }

        /**
         * Remove the default agenda with supplied id.
         *
         * @param {number} id - The unique id of default agenda
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function remove(id) {
            return DefaultAgendas.one("", id).remove();
        }

        /**
         * Clone the default agenda with supplied id.
         *
         * @param {number} id - The unique id of default agenda
         * @returns {Promise<Number|Error>} - The promise of new identifier.
         */
        function clone(id) {
            return DefaultAgendas.one("", id).all("").post({});
        }
    }

})();

