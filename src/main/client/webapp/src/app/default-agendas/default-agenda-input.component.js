/*
 * default-agenda-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.defaultAgendas')
        .component('defaultAgendaInput', {
            controller: DefaultAgendaInputController,
            bindings: {
                ngModel: '<',
                ngReadonly: '<?'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/default-agendas/default-agenda-input.html'
        });

    DefaultAgendaInputController.$inject = ['NgTableParams', '$uibModal'];

    /* @ngInject */
    function DefaultAgendaInputController(NgTableParams, $uibModal) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.addNew = addNew;
        $ctrl.doEdit = doEdit;
        $ctrl.doDelete = doDelete;
        $ctrl.moveUp = moveUp;
        $ctrl.moveDown = moveDown;

        ////////////

        function onInit() {
            $ctrl.tableParams = new NgTableParams({
                count: 100
            }, { counts: [], dataset: [] });
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue || { entries: [] };
                refreshTable();
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function addNew() {
            $uibModal.open({
                animation: true,
                component: 'agendaEntryCreate',
                size: 'lg'
            }).result.then(function (entry) {
                $ctrl.value.entries.push(entry);
                onChange();
                refreshTable();
            });
        }

        function doEdit(index) {
            $uibModal.open({
                animation: true,
                component: 'agendaEntryEdit',
                size: 'lg',
                resolve: {
                    entry: function () { return angular.copy($ctrl.value.entries[index]); }
                }
            }).result.then(function (entry) {
                $ctrl.value.entries.splice(index, 1, entry);
                onChange();
                refreshTable();
            });
        }

        function doDelete(index) {
            $ctrl.value.entries.splice(index, 1);
            onChange();
            refreshTable();
        }

        function moveUp(index) {
            if (index > 0) {
                var tmp = $ctrl.value.entries[index - 1];
                $ctrl.value.entries[index - 1] = $ctrl.value.entries[index];
                $ctrl.value.entries[index] = tmp;
                onChange();
                refreshTable();
            }
        }

        function moveDown(index) {
            if (index < $ctrl.value.entries.length) {
                var tmp = $ctrl.value.entries[index];
                $ctrl.value.entries[index] = $ctrl.value.entries[index + 1];
                $ctrl.value.entries[index + 1] = tmp;
                onChange();
                refreshTable();
            }
        }

        function refreshTable() {
            $ctrl.tableParams.settings({ dataset: angular.copy($ctrl.value.entries) });
        }
    }

})();

