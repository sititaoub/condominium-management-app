/*
 * agenda-entry-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.defaultAgendas')
        .component('agendaEntryInput', {
            controller: AgendaEntryInputController,
            bindings: {
                ngModel: '<',
                meetingMode: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/default-agendas/agenda-entry-input.html'
        });

    AgendaEntryInputController.$inject = ['RoleService', 'VotingRuleService'];

    /* @ngInject */
    function AgendaEntryInputController(RoleService, VotingRuleService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue || {};
                $ctrl.hasRoles = $ctrl.value.votersRoles && $ctrl.value.votersRoles.length > 0;
            };
            loadRoles();
            loadVotingRules();
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
            $ctrl.hasRoles = $ctrl.value.votersRoles && $ctrl.value.votersRoles.length > 0;
        }

        function loadRoles() {
            RoleService.getRoleType().then(function (roleTypes) {
                $ctrl.roleTypes = roleTypes;
            });
        }

        function loadVotingRules() {
            VotingRuleService.getAll().then(function (votingRules) {
                $ctrl.votingRules = votingRules;
            });
        }
    }

})();

