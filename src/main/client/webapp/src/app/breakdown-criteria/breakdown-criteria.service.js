/*
 * breakdown-criteria.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.breakdownCriteria')
        .factory('BreakdownCriteriaService', BreakdownCriteriaService);

    BreakdownCriteriaService.$inject = ['Restangular', 'contextAddressManagement','$window'];

    /* @ngInject */
    function BreakdownCriteriaService(Restangular, contextAddressManagement, $window) {
        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressManagement + 'v1');
        });
        var service = {
            getOne: getOne,
            update: update,
            getReport:getReport
        };
        return service;

        ////////////////

        /**
         * Retrieve a single breakdown criteria by financial period id and materialized path.
         *
         * @param {number} financialPeriodId - The unique id of financial period.
         * @param {string} materializedPath - The materialized path of account.
         * @return {Promise<any|error>} - The promise of distribution criteria result.
         */
        function getOne(financialPeriodId, materializedPath) {
            return Api.all('distribution-criterion').one('by-materialized-path', financialPeriodId)
                .get({ materializedPath: materializedPath }).then(function (result) {
                    return result.content[0];
                });
        }

        /**
         * Updates an existing breakdown criteria.
         *
         * @param {number} id - The unique id of object to update.
         * @param {any} breakdownCriteria - The object.
         * @returns {Promise<any|error>} - The promise of distribution criteria update.
         */
        function update(id, breakdownCriteria, condominiumId) {
            return Api.one('distribution-criterion', id).one('condominium', condominiumId).customPUT(breakdownCriteria);
        }

        /**
         * 
         */
        function getReport(options){
            var url = Api.all('distribution-criterion').all('reports').getRequestedUrl();
            url = url + '?condominiumId=' + options.condominiumId+'&financialPeriodId='+options.financialPeriodId + "&format=" + options.outputFormat;
            $window.open(url, "_blank");
        }
    }

})();

