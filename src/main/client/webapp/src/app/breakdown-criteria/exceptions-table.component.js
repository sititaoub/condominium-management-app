/*
 * exception-table.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.breakdownCriteria')
        .component('exceptionsTable', {
            controller: ExceptionsTableController,
            bindings: {
                'condominium': '<',
                'exceptions': '<',
                'onExceptionAdded': '&',
                'onExceptionUpdated': '&',
                'onExceptionRemoved': '&'
            },
            templateUrl: 'app/breakdown-criteria/exceptions-table.html'
        });

    ExceptionsTableController.$inject = ['NgTableParams', 'AlertService', '$uibModal'];

    /* @ngInject */
    function ExceptionsTableController(NgTableParams, AlertService, $uibModal) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.$onChanges = onChanges;
        $ctrl.addException = addException;
        $ctrl.editException = editException;
        $ctrl.removeException = removeException;

        ////////////

        function onInit() {
            $ctrl.tableParams = new NgTableParams({ page: 1, count: 10 }, { dataset: $ctrl.rows });
        }

        function onChanges(changes) {
            if ($ctrl.tableParams && changes.exceptions.currentValue) {
                $ctrl.tableParams.settings({ dataset: changes.exceptions.currentValue });
            }
        }

        function addException() {
            $uibModal.open({
                animation: true,
                component: 'exceptionEdit',
                resolve: {
                    condominiumId: function () { return $ctrl.condominium.id; }
                }
            }).result.then(function (exception) {
                $ctrl.onExceptionAdded({ exception: exception });
            })
        }

        function editException(index, row) {
            $uibModal.open({
                animation: true,
                component: 'exceptionEdit',
                resolve: {
                    condominiumId: function () { return $ctrl.condominium.id; },
                    exception: function () { return angular.copy(row); }
                }
            }).result.then(function (exception) {
                $ctrl.onExceptionUpdated({ index: index, exception: exception });
            })
        }

        function removeException(index, row) {
            AlertService.askConfirm('BreakdownCriteria.Exceptions.DeleteConfirm', row).then(function () {
                $ctrl.onExceptionRemoved({ index: index, exception: row });
            });
        }
    }

})();

