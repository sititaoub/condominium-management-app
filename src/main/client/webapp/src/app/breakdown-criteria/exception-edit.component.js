/*
 * exception-form.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.breakdownCriteria')
        .component('exceptionEdit', {
            controller: ExceptionEditComponent,
            bindings: {
                resolve: '<',
                close: '&',
                dismiss: '&'
            },
            templateUrl: 'app/breakdown-criteria/exception-edit.html'
        });

    ExceptionEditComponent.$inject = ['$q', 'ThousandthsTablesService', 'HousingUnitService', 'NotifierService', '_', '$state'];

    /* @ngInject */
    function ExceptionEditComponent($q, ThousandthsTablesService, HousingUnitService, NotifierService, _, $state) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.updateHousingUnitDescription = updateHousingUnitDescription;
        $ctrl.updateThousandthsTablesKind = updateThousandthsTablesKind;
        $ctrl.validatePercentSum = validatePercentSum;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.editMode = !(!$ctrl.resolve.exception);
            $ctrl.exception = $ctrl.resolve.exception || {
                ownerPercent: 0,
                tenantPercent: 0,
                usufructPercent: 0,
                othersPercent: 0
            };
            validatePercentSum();
            if ($ctrl.exception.housingUnitId) {
                // Fix for select that work only with strings
                $ctrl.exception.housingUnitId = $ctrl.exception.housingUnitId.toString();
            }
            $q.all({
                housingUnits: HousingUnitService.getHousingUnits($ctrl.resolve.condominiumId, 0, 1000),
                thousandthsTables: ThousandthsTablesService.getPage($ctrl.resolve.condominiumId, $state.params.periodId, 0, 1000)
            }).then(function (data) {
                $ctrl.housingUnits = data.housingUnits.content;
                $ctrl.thousandthsTables = data.thousandthsTables.content;
            }).catch(function () {
                NotifierService.notifyError();
                $ctrl.dismiss();
            }).finally(function () {
                $ctrl.loading = false;
            });
        }

        function updateHousingUnitDescription(housingUnitId) {
            var el = _.find($ctrl.housingUnits, { id: housingUnitId });
            $ctrl.exception.housingUnitDescription = el.owner;
        }

        function updateThousandthsTablesKind(tableId) {
            var el = _.find($ctrl.thousandthsTables, { id: tableId });
            $ctrl.exception.thousandthTableDescription = el.description;
        }

        function validatePercentSum() {
            $ctrl.sum100 = $ctrl.exception.ownerPercent + $ctrl.exception.tenantPercent + $ctrl.exception.usufructPercent + $ctrl.exception.othersPercent === 100;
        }
    }

})();

