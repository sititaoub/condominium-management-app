/*
 * accounts-tree.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.breakdownCriteria')
        .component('accountsTree', {
            controller: AccountsTreeController,
            bindings: {
                chartOfAccounts: '<',
                financialPeriod: '<',
                type: '<',
                onRowSelected: '&'
            },
            templateUrl: 'app/breakdown-criteria/accounts-tree.html'
        });

    AccountsTreeController.$inject = ['ChartOfAccountsService', 'rootAccounts', '_'];

    /* @ngInject */
    function AccountsTreeController(ChartOfAccountsService, rootAccounts, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;

        ////////////

        function onInit() {
            $ctrl.selectAccount = $ctrl.type === 'personal';
            $ctrl.subtree = ChartOfAccountsService.extractSubtree($ctrl.chartOfAccounts, rootAccounts[$ctrl.type],$ctrl.financialPeriod.bindMaterializedPaths);
            if ($ctrl.selectAccount) {
                $ctrl.subtree.children = filterChildren($ctrl.subtree.children);
            } else {
                _.each($ctrl.subtree.children, function (child) {
                    child.children = filterChildren(child.children);
                });
            }
        }

        function filterChildren(children) {
            return _.filter(children, function (el) {
                var f = _.filter($ctrl.bindMaterializedPaths, function (path) {
                    return _.startsWith(path.path, el);
                });
                return _.map(f, 'path');
//                return _.indexOf($ctrl.financialPeriod.bindMaterializedPaths, el.materializedPath) >= 0;
            });
        }
    }

})();

