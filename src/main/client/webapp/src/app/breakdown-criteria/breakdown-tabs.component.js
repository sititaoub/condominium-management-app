/*
 * breakdowns-tabs.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.breakdownCriteria')
        .component('breakdownTabs', {
            controller: BreakdownTabsController,
            bindings: {
                condominium: '<',
                budget: '<',
                final: '<',
                separatedDistribution: '<',
                onSeparatedDistributionChanged: '&',
                onBudgetAdded: '&',
                onBudgetUpdated: '&',
                onBudgetRemoved: '&',
                onFinalAdded: '&',
                onFinalUpdated: '&',
                onFinalRemoved: '&'
            },
            templateUrl: 'app/breakdown-criteria/breakdown-tabs.html'
        });

    BreakdownTabsController.$inject = [];

    /* @ngInject */
    function BreakdownTabsController() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;

        ////////////

        function onInit() {
        }
    }

})();

