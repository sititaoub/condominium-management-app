/*
 * breakdown-table.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.breakdownCriteria')
        .component('breakdownTable', {
            controller: BreakdownTableComponent,
            bindings: {
                condominium: '<',
                rows: '<',
                type: '@?',
                onRowAdded: '&',
                onRowUpdated: '&',
                onRowRemoved: '&'
            },
            templateUrl: 'app/breakdown-criteria/breakdown-table.html'
        });

    BreakdownTableComponent.$inject = ['NgTableParams', 'AlertService', '$uibModal', '_'];

    /* @ngInject */
    function BreakdownTableComponent(NgTableParams, AlertService, $uibModal, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.$onChanges = onChanges;
        $ctrl.addRow = addRow;
        $ctrl.editRow = editRow;
        $ctrl.removeRow = removeRow;

        ////////////

        function onInit() {
            $ctrl.tableParams = new NgTableParams({ page: 1, count: 10 }, { dataset: $ctrl.rows });
            $ctrl.coefficentSum = _.sumBy($ctrl.rows, 'coefficent');

        }

        function onChanges(changes) {
            if ($ctrl.tableParams && changes.rows.currentValue) {
                $ctrl.tableParams.settings({ dataset: changes.rows.currentValue });
                $ctrl.coefficentSum = _.sumBy(changes.rows.currentValue, 'coefficent');
            }
        }

        function addRow() {
            $uibModal.open({
                animation: true,
                component: 'breakdownEdit',
                resolve: {
                    condominiumId: function () { return $ctrl.condominium.id; }
                }
            }).result.then(function (data) {
                $ctrl.onRowAdded({ row: data });
            })
        }

        function editRow(index, row) {
            $uibModal.open({
                animation: true,
                component: 'breakdownEdit',
                resolve: {
                    condominiumId: function () { return $ctrl.condominium.id; },
                    data: function () { return angular.copy(row); }
                }
            }).result.then(function (data) {
                $ctrl.onRowUpdated({ index: index, row: data });
            })
        }

        function removeRow(index, row) {
            AlertService.askConfirm('BreakdownCriteria.Breakdowns.DeleteConfirm', row).then(function () {
                $ctrl.onRowRemoved({ index:index, row: row });
            });
        }
    }

})();

