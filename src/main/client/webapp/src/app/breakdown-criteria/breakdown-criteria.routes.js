/*
 * breakdown-criteria.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.breakdownCriteria')
        .config(setupRoutes);

        setupRoutes.$inject = ['$stateProvider'];

        /* @ngInject */
        function setupRoutes ($stateProvider) {
            $stateProvider.state("index.breakdownCriteria", {
                url: '/condominiums/{condominiumId}/financial-period/{periodId}/breakdown-criteria',
                abstract: true,
                template: '<div class="breakdown-criteria-container" ui-view></div>'
            }).state('index.breakdownCriteria.main', {
                url: '?type&account',
                controller: 'BreakdownCriteriaMainController',
                controllerAs: 'vm',
                templateUrl: 'app/breakdown-criteria/breakdown-criteria-main.html',
                params: {
                    type: 'costs'
                }
            });
        }
})();
