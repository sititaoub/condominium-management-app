/*
 * breakdown-criteria-main.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.breakdownCriteria')
        .controller('BreakdownCriteriaMainController', BreakdownCriteriaMainController);

    BreakdownCriteriaMainController.$inject = ['$state', 'rootAccounts', 'CondominiumService', 'FinancialPeriodService', 'ChartOfAccountsService', 'NotifierService', 'BreakdownCriteriaService'];

    /* @ngInject */
    function BreakdownCriteriaMainController($state, rootAccounts, CondominiumService, FinancialPeriodService, ChartOfAccountsService, NotifierService, BreakdownCriteriaService) {
        var vm = this;
        if ($state.params.periodId === 'current') {
            vm.periodId = FinancialPeriodService.getCurrent($state.params.companyId, $state.params.condominiumId).id;
            $state.go('.', { periodId: vm.periodId });
        }

        vm.type = $state.params.type;
        vm.scrollbarConfig = {
            autoHideScrollbars: false,
            theme: 'dark',
            advanced: {
                updateOnContentResize: true
            }
        };

        vm.accountSelected = accountSelected;
        vm.doPrintConfirm = doPrintConfirm;

        activate();

        ////////////////

        function activate() {
            vm.loading = true;
            CondominiumService.getOne($state.params.condominiumId).then(function (condominium) {
                vm.condominium = condominium;
                return FinancialPeriodService.getOne($state.params.companyId, $state.params.periodId);
            }).then(function (financialPeriod) {
                vm.financialPeriod = financialPeriod;
                return ChartOfAccountsService.getOne(vm.financialPeriod.chartOfAccountsId, $state.params.companyId, false);
            }).then(function (chartOfAccounts) {
                vm.chartOfAccounts = chartOfAccounts;
                vm.subtree = ChartOfAccountsService.extractSubtree(vm.chartOfAccounts, rootAccounts[$state.params.type]);
                if ($state.params.account) {
                    _.each(vm.subtree.children, function (father) {
                        _.each(father.children, function (child) {
                            child.fatherDescription = father.descrizione;
                        });
                    });
                    if (vm.type === 'personal') {
                        vm.account = _.find(vm.subtree.children, { materializedPath: $state.params.account });
                    } else {
                        vm.account = _.find(_.flatMap(vm.subtree.children, 'children'), { materializedPath: $state.params.account });
                    }
                }
            }).catch(function () {
                return NotifierService.notifyError();
            }).finally(function () {
                vm.loading = false;
            });
        }

        /**
         * Callback invoked when an account is selected on tree component.
         *
         * @param {ChartOfAccounts~Account} row - The account selected
         */
        function accountSelected(row) {
            vm.account = row;
            $state.go('.', { account: row.materializedPath, fatherDescription : row.fatherDescription }, { notify: false });
        }

        function doPrintConfirm(outputFormat) {
            var options = {};
            options.financialPeriodId =$state.params.periodId;
            options.condominiumId = $state.params.condominiumId;
            options.outputFormat = outputFormat;
            BreakdownCriteriaService.getReport(options);
        }
    }

})();

