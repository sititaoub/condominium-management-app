/*
 * breakdown-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.breakdownCriteria')
        .component('breakdownEdit', {
            controller: BreakdownEditController,
            bindings: {
                resolve: '<',
                close: '&',
                dismiss: '&'
            },
            templateUrl: 'app/breakdown-criteria/breakdown-edit.html'
        });

    BreakdownEditController.$inject = ['ThousandthsTablesService', 'NotifierService', '_', '$state'];

    /* @ngInject */
    function BreakdownEditController(ThousandthsTablesService, NotifierService, _, $state) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.updateThousandthsTablesKind = updateThousandthsTablesKind;
        $ctrl.validatePercentSum = validatePercentSum;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.editMode = !(!$ctrl.resolve.data);
            $ctrl.data = $ctrl.resolve.data || {
                coefficent: 100,
                ownerPercent: 0,
                tenantPercent: 0,
                usufructPercent: 0,
                othersPercent: 0
            };
            validatePercentSum();
            ThousandthsTablesService.getPage($ctrl.resolve.condominiumId, $state.params.periodId, 0, 1000).then(function (data) {
                $ctrl.thousandthsTables = data.content;
            }).catch(function () {
                NotifierService.notifyError();
                $ctrl.dismiss();
            }).finally(function () {
                $ctrl.loading = false;
            });
        }

        function updateThousandthsTablesKind(tableId) {
            var el = _.find($ctrl.thousandthsTables, { id: tableId });
            $ctrl.data.thousandthType = el.thousandthType;
            $ctrl.data.thousandthTableDescription = el.description;
        }

        function validatePercentSum() {
            $ctrl.sum100 = $ctrl.data.ownerPercent + $ctrl.data.tenantPercent + $ctrl.data.usufructPercent + $ctrl.data.othersPercent === 100;
        }
    }

})();
