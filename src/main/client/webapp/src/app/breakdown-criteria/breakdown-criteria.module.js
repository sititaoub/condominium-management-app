/*
 * breakdown-criteria.module.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.breakdownCriteria', [
            'condominiumManagementApp.core',
            'condominiumManagementApp.commons'
        ]);
})();
