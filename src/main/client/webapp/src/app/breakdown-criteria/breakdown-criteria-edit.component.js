/*
 * breakdown-criteria-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.breakdownCriteria')
        .component('breakdownCriteriaEdit', {
            controller: BreakdownCriteriaEditController,
            bindings: {
                condominium: '<',
                financialPeriod: '<',
                account: '<'

            },
            templateUrl: 'app/breakdown-criteria/breakdown-criteria-edit.html'
        });

    BreakdownCriteriaEditController.$inject = ['BreakdownCriteriaService', 'NotifierService','AlertService', '_', 'toaster', '$translate'];

    /* @ngInject */
    function BreakdownCriteriaEditController(BreakdownCriteriaService, NotifierService, AlertService, _, toaster, $translate) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.$onChanges = onChanges;
        $ctrl.doSave = doSave;
        $ctrl.updateCriteria = updateCriteria;
        $ctrl.changeSeparatedDistribution = changeSeparatedDistribution;
        $ctrl.addRow = addRow;
        $ctrl.updateRow = updateRow;
        $ctrl.removeRow = removeRow;
        $ctrl.hasTable = hasTable;

        ////////////

        function onInit() {
        }

        function onChanges() {
            if ($ctrl.account) {
                $ctrl.loading = true;
                BreakdownCriteriaService.getOne($ctrl.financialPeriod.id, $ctrl.account.materializedPath).then(function (criteria) {
                    $ctrl.criteria = criteria;
                }).catch(function () {
                    NotifierService.notifyError();
                }).finally(function () {
                    $ctrl.loading = false;
                });
            }
        }

        function doSave() {
            if($ctrl.criteria.propagateToAllSubAccounts) {
                AlertService.askConfirm('Confermi di voler applicare il criterio a tutti i sottoconti del conto '+ $ctrl.account.fatherDescription).then(function () {
                updateCriteria();
            })
            } else {
                updateCriteria();
            }
        }

        function updateCriteria() {
            return BreakdownCriteriaService.update($ctrl.criteria.id, $ctrl.criteria, $ctrl.condominium.id ).then(function () {
                NotifierService.notifySuccess('BreakdownCriteria.Edit.Success');
            }).catch(function () {
                NotifierService.notifyError('BreakdownCriteria.Edit.Failure');
            });
        }

        function changeSeparatedDistribution(value) {
            if (value == true) {
                var obj = {};
                angular.forEach($ctrl.criteria.distributionCriterionDetailFinal, function (value) {
                    obj.coefficent = value.coefficent;
                    obj.othersPercent = value.othersPercent;
                    obj.ownerPercent = value.ownerPercent;
                    obj.tenantPercent = value.tenantPercent;
                    obj.thousandthTableDescription = value.thousandthTableDescription;
                    obj.thousandthTableId = value.thousandthTableId;
                    obj.thousandthType = value.thousandthType;
                    obj.usufructPercent = value.usufructPercent;
                    $ctrl.criteria.distributionCriterionDetailQuote.push(obj);
                });
            }
            if (value == false) {
                $ctrl.criteria.distributionCriterionDetailQuote = [];
            }
            $ctrl.criteria.separatedDistribution = value;
        }

        function addRow(collection, item) {
            $ctrl.criteria[collection].push(item);
            $ctrl.criteria[collection] = angular.copy($ctrl.criteria[collection]);
            $ctrl.criteriaForm.$setDirty();
        }

        function updateRow(collection, index, item) {
            $ctrl.criteria[collection].splice(index, 1, item);
            $ctrl.criteria[collection] = angular.copy($ctrl.criteria[collection]);
            $ctrl.criteriaForm.$setDirty();
        }

        function removeRow(collection, index) {
            if (collection != "distributionCriterionExceptions") {
                if ($ctrl.criteria[collection].length == 1) {
                    if ($ctrl.criteria.separatedDistribution) {
                        alert(collection);
                    } else {
                        alert('distributionCriterionDetail');
                    }
                    return;
                }
            }
            $ctrl.criteria[collection].splice(index, 1);
            $ctrl.criteria[collection] = angular.copy($ctrl.criteria[collection]);
            $ctrl.criteriaForm.$setDirty();
        }

        function hasTable() {
            return $ctrl.criteria && (($ctrl.criteria.distributionCriterionDetailQuote || []).length +
                ($ctrl.criteria.distributionCriterionDetailFinal || []).length > 0);
        }

        function alert(message) {
            var text = 'BreakdownCriteria.Breakdowns.EmptyAlert.' + message;
            $translate(text).then(function (msg) {
                toaster.pop({
                    type: 'error',
                    title: msg,
                    timeout: 0
                });
            });
        }
    }

})();