/*
 * money-account-close.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.moneyAccount')
        .controller('MoneyAccountCloseController', MoneyAccountCloseController);

    MoneyAccountCloseController.$inject = ['$uibModalInstance', '$rootScope', '$state', 'moneyAccountTypes', 'MoneyAccountService', 'NotifierService', '$transition$'];

    /* @ngInject */
    function MoneyAccountCloseController($uibModalInstance, $rootScope, $state, moneyAccountTypes, MoneyAccountService, NotifierService, $transition$) {
        var vm = this;

        vm.doClose = doClose;

        activate();

        ////////////////

        function activate() {
            vm.moneyAccountTypes = moneyAccountTypes;
            vm.loading = true;
            loadAccount();
        }

        function loadAccount() {
            MoneyAccountService.getOne($transition$.params().companyId, $transition$.params().accountId).then(function (account) {
                vm.account = account;
                vm.loading = false;
            }).catch(function () {
                NotifierService.notifyError().then($uibModalInstance.dismiss());
            });
        }

        function doClose() {
            MoneyAccountService.close($state.params.companyId, $state.params.accountId, vm.account.closingDate).then(function () {
                return NotifierService.notifySuccess('MoneyAccount.Close.Success');
            }).then(function () {
                $rootScope.$broadcast('moneyAccount:reload');
                $uibModalInstance.close();
            }).catch(function () {
                return NotifierService.notifyError('MoneyAccount.Close.Failure');
            });
        }
    }

})();

