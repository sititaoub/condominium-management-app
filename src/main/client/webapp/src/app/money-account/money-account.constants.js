/*
 * money-account.constants.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.moneyAccount')
        .constant('moneyAccountTypes', {
            Generic: "Generic",
            Cash: "Cash",
            Bank: "Bank",
            Postal: "Postal",
            OrdinaryFund: "OrdinaryFund",
            StraordinaryFund: "StraordinaryFund",
            Cheques: "Cheques",
            Voucher: "Voucher",
            Recharges: "Recharges",
            Stamps: "Stamps"
        });

})();