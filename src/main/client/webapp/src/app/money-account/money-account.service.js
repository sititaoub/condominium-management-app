/*
 * money-account.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.moneyAccount')
        .factory('MoneyAccountService', MoneyAccountService);

    MoneyAccountService.$inject = ['Restangular', 'contextAddressAggregatori', '_', 'moment'];

    /* @ngInject */
    function MoneyAccountService(Restangular, contextAddressAggregatori, _, moment) {
        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressAggregatori + 'v2');
        });

        var vm = [];

        var service = {
            getPage: getPage,
            getOne: getOne,
            create: create,
            update: update,
            close: close,
            shareData: shareData
        };
        return service;

        ////////////////

        /**
         * Money Account page.
         *
         * @typedef {Object} MoneyAccount~MoneyAccountPage
         * @property {Number} number - The requested page number.
         * @property {Number} size - The requested page size.
         * @property {Number} totalElements - The total number of elements.
         * @property {Number} totalPages - The total number of pages.
         * @property {MoneyAccount~MoneyAccount[]} content - The page content.
         */

        /**
         * money account.
         *
         * @typedef {Object} MoneyAccount~MoneyAccount
         * @property {Number} id - The unique id of the money account.
         * @property {String} name - The name of money account.
         * @property {String} description - The money account description.
         * @property {String} code - The money account code.
         * @property {String} tipology - [Cash, Bank, Postal, Fund, Generic]
         * @property {Date} openingDate - The money account start date.
         * @property {Date} closingDate - The money account end date.
         * @property {Number} openingBalance - The opening balance of an account.
         */

        /**
         * Retrieve a single page of money accounts for given company id.
         *
         * @param {Number} companyId - The unique company id.
         * @param {Number} [page=0] - The page (0 based).
         * @param {Number} [size=20] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<MoneyAccount~MoneyAccountPage|Error>} - The promise of page.
         */
        function getPage(companyId, page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 20,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return Api.one('companies', companyId).all('money-accounts').get("", params);
        }

        /**
         * Retrieve a single money account.
         *
         * @param {Number} companyId - The unique company id.
         * @param {Number} id - The unique id of money account.
         * @returns {Promise<MoneyAccount~MoneyAccount|Error>} - The promise over the money account.
         */
        function getOne(companyId, id) {
            return Api.one('companies', companyId).one('money-accounts', id).get().then(function (account) {
                account.openingDate = account.openingDate ? new Date(account.openingDate) : undefined;
                account.closingDate = account.closingDate ? new Date(account.closingDate) : undefined;
                return account;
            });
        }

        /**
         * Create a new money account.
         *
         * @param {Number} companyId - The unique company id
         * @param {MoneyAccount~MoneyAccount} moneyAccount - The money account.
         * @returns {Promise<Object|Error>} - The promise over the create operation result.
         */
        function create(companyId, moneyAccount) {
            return Api.one('companies', companyId).all('money-accounts').post(moneyAccount);
        }

        /**
         * Updates an existing money account.
         *
         * @param {Number} companyId - The unique company id
         * @param {Number} id - The unique id
         * @param {MoneyAccount~MoneyAccount} moneyAccount - The money account to update.
         * @returns {Promise<Object|Error>} - The promise over the create operation result.
         */
        function update(companyId, id, moneyAccount) {
            return Api.one('companies', companyId).one('money-accounts', id).customPUT(moneyAccount);
        }

        /**
         * Close an existing money account.
         *
         * @param {Number} companyId - The unique company id.
         * @param {Number} id - The unique id of money account.
         * @param {Date} date - The date at which close the account.
         * @returns {Promise<Object|Error>} - The promise over the remove operation result.
         */
        function close(companyId, id, date) {
            return Api.one('companies', companyId).one('money-accounts', id).all('close').post({
                closingDate: date
            });
        }

        /**
         * Share data from controllers
         */
        function shareData() {
            var addData = function (data) {
                vm = [];
                vm.push(data);
            };
            var getData = function () { return vm; };
            return {
                addData: addData,
                getData: getData
            };
        }
    }

})();

