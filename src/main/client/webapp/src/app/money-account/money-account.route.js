/*
 * money-account.route.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.moneyAccount')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider'];

    /* @ngInject */
    function setupRoutes($stateProvider) {
        $stateProvider.state('index.money-accounts', {
            url: '/condominiums/{condominiumId}/accounts',
            abstract: true,
            template: '<div class="money-account"><ui-view></ui-view></div>',
            data: {
                skipFinancialPeriodCheck: true
            }
        }).state('index.money-accounts.list', {
            url: "?page&count&sort&funds",
            templateUrl: "app/money-account/money-accounts.html",
            controller: "MoneyAccountsController",
            controllerAs: 'vm'
        }).state('index.money-accounts.list.add', {
            url: "/create",
            onEnter: ['$state', '$uibModal', function ($state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/money-account/money-account-create.html',
                    controller: 'MoneyAccountCreateController',
                    controllerAs: 'vm'
                }).result.finally(function () {
                    $state.go('^');
                });
            }]
        }).state('index.money-accounts.list.edit', {
            url: '/{accountId}',
            onEnter: ['$state', '$transition$', '$uibModal', function ($state, $transition$, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/money-account/money-account-edit.html',
                    controller: 'MoneyAccountEditController',
                    controllerAs: 'vm',
                    resolve: {
                        '$transition$': function () {
                            return $transition$;
                        }
                    }
                }).result.finally(function () {
                    $state.go('^');
                });
            }]
        }).state('index.money-accounts.list.close', {
            url: '/{accountId}/close',
            onEnter: ['$state', '$transition$', '$uibModal', function ($state, $transition$, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/money-account/money-account-close.html',
                    controller: 'MoneyAccountCloseController',
                    controllerAs: 'vm',
                    resolve: {
                        '$transition$': function () {
                            return $transition$;
                        }
                    }
                }).result.finally(function () {
                    $state.go('^');
                });
            }]
        });
    }

})();