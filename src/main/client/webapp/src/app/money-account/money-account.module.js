/*
 * money-account.module.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.moneyAccount', [
            'condominiumManagementApp.core',
            'condominiumManagementApp.commons'
        ]);

})();