/*
 * money-accounts.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.moneyAccount')
        .controller('MoneyAccountsController', MoneyAccountsController);

    MoneyAccountsController.$inject = ['$state', '$rootScope', '$scope', 'NgTableParams', 'MoneyAccountService', 'AlertService', 'NotifierService'];

    /* @ngInject */
    function MoneyAccountsController($state, $rootScope, $scope, NgTableParams, MoneyAccountService, AlertService, NotifierService) {
        var vm = this;

        activate();

        ////////////////

        function activate() {
            vm.loading = true;
            vm.firstLoad = true;
            vm.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10
            }, {
                getData: loadMoneyAccounts
            });

            registerEventHandlers();
        }

        function registerEventHandlers() {
            var unsubscribe = $rootScope.$on('moneyAccount:reload', function () {
                vm.tableParams.reload();
            });
            $scope.$on('$destroy', function () {
                unsubscribe();
            });
        }

        function loadMoneyAccounts(params) {
            if (!vm.firstLoad) {
                $state.go('.', params.uiRouterUrl(), { notify: false, inherit: false });
            }
            if ($state.params.funds == "true") {
                vm.activeTab = 1;
            } else {
                vm.activeTab = 0;
            }
            return MoneyAccountService.getPage($state.params.companyId, params.page() - 1, params.count(), params.sorting(), params.filter()).then(function (page) {
                params.total(page.totalElements);
                vm.page = page;
                var shareData = MoneyAccountService.shareData();
                shareData.addData(vm);
                vm.firstLoad = false;
                return page.content;
            }).catch(function () {
                return NotifierService.notifyError();
            }).finally(function () {
                vm.loading = false;
            });
        }
    }

})();

