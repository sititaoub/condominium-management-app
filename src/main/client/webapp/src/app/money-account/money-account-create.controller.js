/*
 * money-account-create.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.moneyAccount')
        .controller('MoneyAccountCreateController', MoneyAccountCreateController);

    MoneyAccountCreateController.$inject = ['$uibModalInstance', '$rootScope', '$state', 'moneyAccountTypes', 'MoneyAccountService', 'NotifierService'];

    /* @ngInject */
    function MoneyAccountCreateController($uibModalInstance, $rootScope, $state, moneyAccountTypes, MoneyAccountService, NotifierService) {
        var vm = this;

        vm.doSave = doSave;
        vm.selectedAccountTipology = selectedAccountTipology;
        vm.selectAccount = false;
        vm.selectedAccount = selectedAccount;
        vm.readonly = false;
        vm.onBlur = onBlur;
        vm.invalid = false;
        vm.onChange = onChange;

        activate();

        ////////////////

        function activate() {
            vm.moneyAccountTypes = moneyAccountTypes;
            MoneyAccountService.getPage($state.params.companyId, $state.params.page - 1, $state.params.count, $state.params.sorting, $state.params.filter).then(function (page) {
                vm.accounts = page.content;
            });
        }

        /**
         * Create new account
         */
        function doSave() {
            vm.account.openingBalance = 0;
            MoneyAccountService.create($state.params.companyId, vm.account).then(function () {
                return NotifierService.notifySuccess('MoneyAccount.Create.Success');
            }).then(function () {
                $rootScope.$broadcast('moneyAccount:reload');
                $uibModalInstance.close();
            }).catch(function () {
                return NotifierService.notifyError('MoneyAccount.Create.Failure');
            });
        }

        /**
         * Check selected account type
         */
        function selectedAccountTipology() {
            if (vm.account.tipology == "OrdinaryFund" || vm.account.tipology == "StraordinaryFund") {
                vm.selectAccount = true;
            } else {
                vm.selectAccount = false;
            }
        }

        /**
         * Check selected account
         */
        function selectedAccount() {
            vm.account.selectAccount = vm.account.selectedAccount.id;
        }

        function onBlur(form) {
            vm.pattern = false;
            vm.length = false;
            vm.invalid = false;

            if (vm.account.iban == undefined) {
                vm.pattern = false;
                vm.length = false;
                vm.invalid = false;
                return;
            }

            if (vm.account.iban.length != 27) {
                vm.pattern = false;
                vm.length = true;
                vm.invalid = true;
                form.$invalid = true;
                return;
            }

            if (vm.account.iban.length == 27) {
                vm.account.iban = vm.account.iban.toUpperCase();
                vm.pattern = false;
                vm.length = false;
                vm.invalid = false;
                var pattern = /[A-Z]{2}[0-9]{2}[A-Z]{1}[0-9]{10}[A-Z0-9]{12}/;

                if (pattern.test(vm.account.iban)) {
                    vm.pattern = false;
                    vm.length = false;
                    vm.invalid = false;
                } else {
                    vm.pattern = true;
                    vm.length = false;
                    vm.invalid = true;
                    form.$invalid = true;
                }
            }
        }

        function onChange(form) {
            if (vm.account.iban.length == 27) {
                vm.account.iban = vm.account.iban.toUpperCase();
                vm.pattern = false;
                vm.length = false;
                vm.invalid = false;
                var pattern = /[A-Z]{2}[0-9]{2}[A-Z]{1}[0-9]{10}[A-Z0-9]{12}/;

                if (pattern.test(vm.account.iban)) {
                    vm.pattern = false;
                    vm.length = false;
                    vm.invalid = false;
                } else {
                    vm.pattern = true;
                    vm.length = false;
                    vm.invalid = true;
                    form.$invalid = true;
                }
            } else {
                form.$invalid = true;
            }
        }
    }

})();

