/*
 * fullName.filter.spec.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
describe('full-name filter', function () {
    var $filter;

    beforeEach(module('condominiumManagementApp'));

    beforeEach(inject(function (_$filter_) {
        $filter = _$filter_;
    }));

    it("should return an undefined when supplied object is undefined", function () {
        var result = $filter('fullName')(undefined);
        expect(result).toBeUndefined();
    });

    it("should return companyName if it's defined", function () {
        var model = {
            companyName: 'Nightswatch',
            firstName: "John",
            lastName: "Snow"
        };
        var result = $filter('fullName')(model);
        expect(result).toEqual('Nightswatch');
    });

    it("should return full name if companyName is undefined", function () {
        var model = {
            firstName: 'John',
            lastName: 'Snow'
        };
        var result = $filter('fullName')(model);
        expect(result).toEqual('John Snow');
    });

    it("should return undefined if companyName, firstName and lastName are undefined", function () {
        var model = {
        };
        var result = $filter('fullName')(model);
        expect(result).toBeUndefined();
    })
});