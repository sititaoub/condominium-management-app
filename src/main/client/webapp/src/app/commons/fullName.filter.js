/*
 * fullName.filter.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .filter('fullName', fullName);

    function fullName() {
        return fullNameFilter;

        ////////////////

        function fullNameFilter(model) {
            var output;

            if (model && model.companyName) {
                output = model.companyName;
            } else if (model && model.firstName && model.lastName) {
                output = model.firstName + ' ' + model.lastName;
            }

            return output;
        }
    }

})();

