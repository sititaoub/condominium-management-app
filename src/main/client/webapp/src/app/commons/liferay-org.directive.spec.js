/*
 * liferay-org.directive.spec.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
describe('liferay-org', function () {
    var $httpBackend, $rootScope, $compile, $location;

    beforeEach(module('condominiumManagementApp', 'condominiumManagementApp.templates'));

    beforeEach(inject(function (_$httpBackend_, _$rootScope_, _$compile_, _$location_) {
        $httpBackend = _$httpBackend_;
        $rootScope = _$rootScope_;
        $compile = _$compile_;
        $location = _$location_;
    }));

    beforeEach(function () {
        $httpBackend.whenGET(/^assets.+/).respond(200, {});
        $httpBackend.whenGET(/profile\/v1\/users\/me$/).respond(200, { authorities: [] });
    });

    it("Should apply 'neutral' class to element", function () {
        var element = $compile('<div liferay-org></div>')($rootScope);
        $rootScope.$digest();

        expect(element.attr('class')).toContain('neutral');
    });

    it("Should apply organization class to element", function () {
        spyOn($location, 'search').and.returnValue({ lfOrg: 'gabetti' });

        var element = $compile('<div liferay-org></div>')($rootScope);
        $rootScope.$digest();

        expect(element.attr('class')).toContain('gabetti');
    });
});