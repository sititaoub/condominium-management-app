(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.commons', [
            'condominiumManagementApp.core'
        ]);

})();