/*
 * address.filter.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.commons')
        .filter('address', address);

    function address() {
        return addressFilter;

        ////////////////

        function addressFilter(model) {
            var output;

            if (angular.isDefined(model) && model !== null &&
                angular.isDefined(model.streetName) && model.streetName !== null) {

                output = model.streetName;
                if (angular.isDefined(model.buildingNumber) && model.buildingNumber !== null) {
                    output += ' ' + model.buildingNumber;
                }
                if (angular.isDefined(model.town) && model.town !== null) {
                    output += ', ' + model.town + ',';
                }
                if (angular.isDefined(model.district) && model.district !== null) {
                    output += ' ' + model.district;
                }
                if (angular.isDefined(model.province) && model.province !== null) {
                    output += ' (' + model.province + ')';
                }
            }

            return output;
        }
    }

})();

