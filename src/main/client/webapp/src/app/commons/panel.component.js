/*
 * panel.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.commons')
        .component('panel', {
            controller: angular.noop,
            bindings: {
                'title': '@',
                'type': '@'
            },
            transclude: true,
            templateUrl: 'app/commons/panel.html'
        });

})();

