/*
 * auto-numeric.directive.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.commons')
        .directive('autoNumeric', autoNumeric);

    autoNumeric.$inject = [];

    /* @ngInject */
    function autoNumeric() {
        var directive = {
            bindToController: true,
            link: link,
            restrict: 'A',
            scope: {},
            require: '?ngModel'
        };
        return directive;

        function link(scope, elm, attrs) {
            var options = {};
            // Get instance-specific options.
            var isTextInput = elm.is('input:text');
            var opts = angular.extend({}, options, scope.$eval(attrs.numeric));

            if(opts.euro) {
                opts.aDec = ",";
                opts.aSep = ".";
            }

            // Initialize element as autoNumeric with options.
            elm.autoNumeric(opts);

            // if element has ctrl, wire it (only for <input type="text" />)
            if (ctrl && isTextInput) {
                // watch for external changes to model and re-render element
                scope.$watch(attrs.ngModel, function (current, old) {
                    ctrl.$setViewValue(elm.autoNumeric('get'));
                    ctrl.$render();
                });

                // render element as autoNumeric
                ctrl.$render = function () {
                    updateElement(elm, ctrl.$viewValue);
                };

                // Detect keypress and setviewvalue
                elm.keypress(function(){
                    ctrl.$setViewValue(elm.autoNumeric('get'));
                    ctrl.$render();
                    updateElement(elm, ctrl.$viewValue);
                });
                // Detect backspace and other chars
                elm.keydown(function(){
                    ctrl.$setViewValue(elm.autoNumeric('get'));
                    ctrl.$render();
                    updateElement(elm, ctrl.$viewValue);
                });

                // Detect changes on element and update model.
                elm.on('change', function (e) {
                    //console.log("Change");
                    scope.$apply(function () {
                        ctrl.$setViewValue(elm.autoNumeric('get'));
                    });
                });
            } else {
                // Listen for changes to value changes and re-render element.
                // Useful when binding to a readonly input field.
                if (isTextInput) {
                    attrs.$observe('value', function (val) {
                        updateElement(elm, val);
                    });
                }
            }

            // Helper method to update autoNumeric with new value.
            function updateElement(element, newVal) {
                // Check if has value
                if (!newVal) return;

                // Only set value if value is numeric (+ support for . seperator)
                if(opts.euro) {
                    if ($.isNumeric(newVal) && newVal.indexOf(".") === -1) element.autoNumeric('set', newVal);
                } else {
                    if ($.isNumeric(newVal)) element.autoNumeric('set', newVal);
                }
            }
        }
    }
})();

