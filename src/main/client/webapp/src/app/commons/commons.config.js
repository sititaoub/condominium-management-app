(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.commons')
		.config(configI18n);

	configI18n.$inject = ['$translatePartialLoaderProvider'];

	/* @ngInject */
	function configI18n ($translatePartialLoaderProvider) {
        $translatePartialLoaderProvider.addPart("commons");
	}

})();