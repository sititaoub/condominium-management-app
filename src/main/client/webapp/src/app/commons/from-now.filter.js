/*
 * from-now.filter.js
 *
 * (C) 2016-2017 Cedac Software S.r.l.
 */
(function () {
  'use strict';

  angular
    .module('condominiumManagementApp.commons')
    .filter('fromNow', fromNow);

  fromNow.$inject = ['moment'];

  function fromNow(moment) {
    return fromNowFilter;

    ////////////////

    function fromNowFilter(input) {
      return moment.utc(input).fromNow();
    }
  }

})();

