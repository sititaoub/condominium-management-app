/*
 * address-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.commons')
        .component('addressInput', {
            controller: AddressInputController,
            bindings: {
                id: '@?',
                ngModel: '<',
                ngRequired: '<',
                templateUrl: '@?',
                includeTown: '<?',
                ngRequiredTown: '<?',
                defaultCountry: '@?'
            },
            require: {
                'ngModelCtrl': 'ngModel'
            },
            template: '<div ng-include="$ctrl.templateUrl"></div>'
        });

    AddressInputController.$inject = ['DistrictsService', 'NotifierService'];

    /* @ngInject */
    function AddressInputController(DistrictsService, NotifierService) {
        var $ctrl = this;

        $ctrl.defaultCountry = angular.isUndefined($ctrl.defaultCountry) ? 'IT' : $ctrl.defaultCountry;
        $ctrl.includeTown = angular.isUndefined($ctrl.includeTown) ? true : $ctrl.includeTown;
        $ctrl.templateUrl = angular.isUndefined($ctrl.templateUrl) ? 'app/commons/address-input.html' : $ctrl.templateUrl;

        $ctrl.$onInit = onInit;
        $ctrl.getDistricts = getDistricts;
        $ctrl.onSelect = onSelect;
        $ctrl.onChange = onChange;
        $ctrl.cleanProvince = cleanProvince;

        ////////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue || { country: $ctrl.defaultCountry };
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function getDistricts(name) {
            return DistrictsService.getAll(name).catch(function () {
                NotifierService.notifyError();
            });
        }

        function onSelect(item) {
            $ctrl.value.province = item.province;
            onChange();
        }

        function cleanProvince() {
            if ($ctrl.value.district == null || $ctrl.value.birthTown == "" ){
                $ctrl.value.province = "";
            }
        }
    }

})();

