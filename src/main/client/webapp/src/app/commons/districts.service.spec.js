/*
 * districts.service.spec.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
describe('DistrictsService', function () {
    var DistrictsService, $httpBackend, contextAddress;

    beforeEach(module('condominiumManagementApp', 'condominiumManagementApp.templates'));

    beforeEach(inject(function (_$httpBackend_, _DistrictsService_, _contextAddress_) {
        $httpBackend = _$httpBackend_;
        DistrictsService = _DistrictsService_;
        contextAddress = _contextAddress_;
    }));

    beforeEach(function () {
        $httpBackend.whenGET(/profile\/v1\/users\/me$/).respond(200, { authorities: [] });
    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getAll', function () {
        it("should invoke right URL", function (done) {
            // Given
            $httpBackend.expectGET(contextAddress + 'v1/lookups/districts?name=Foo').respond([]);

            // When
            var promise = DistrictsService.getAll('Foo');

            // Then
            promise.then(done).catch(done);
            $httpBackend.flush();
        });
    });


});