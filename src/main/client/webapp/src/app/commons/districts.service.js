/*
 * DistrictsService.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.commons')
        .factory('DistrictsService', DistrictsService);

    DistrictsService.$inject = ['Restangular', 'contextAddress'];

    /* @ngInject */
    function DistrictsService(Restangular, contextAddress) {
        var Districts = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddress + 'v1/lookups');
        }).all('districts');

        var service = {
            getAll: getAll
        };
        return service;

        ////////////////

        /**
         * Retrieve all the districts optionally filtered by name.
         *
         * @param {string} name - The name to filter
         * @returns {Promise<Commons~District[]|Error>} - The promise of result.
         */
        function getAll(name) {
            return Districts.getList({ name: name });
        }
    }

})();

