/*
 * template-editor.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.commons')
        .component('templateEditor', {
            controller: TemplateEditorController,
            bindings: {
                id: '@?',
                name: '@?',
                ngModel: '<',
                customFields: '<?',
                ngRequired: '<?',
                rows: '<?',
                resize: '<?',
                onEditorSetup: '&'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/commons/template-editor.html'
        });

    TemplateEditorController.$inject = ['$filter', 'uiTinymceConfig', '_'];

    /* @ngInject */
    function TemplateEditorController($filter, uiTinymceConfig, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.customFields = $ctrl.customFields || [];
            $ctrl.rows = $ctrl.rows || 20;
            $ctrl.options = {
                theme: 'modern',
                language_url: uiTinymceConfig.baseUrl + '/langs/it.js',
                elementpath: false,
                menubar: 'edit insert view format table tools',
                resize: $ctrl.resize,
                plugins: [
                    'advlist lists charmap print preview',
                    'searchreplace visualblocks fullscreen hr',
                    'table paste noneditable textcolor wordcount'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
                noneditable_regexp: /\[\[[^\]]+]]/g,
                setup: setupEditor
            };

            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue($ctrl.value);
        }

        function setupEditor(editor) {
            var skipStandardSetup = $ctrl.onEditorSetup({ editor: editor });
            if (!skipStandardSetup && $ctrl.customFields.length > 0) {
                var translate = $filter('translate');
                editor.addMenuItem('placeholder', {
                    text: translate('TEMPLATE_EDITOR.RECIPIENT_DATA'),
                    context: 'insert',
                    icon: false,
                    menu: _.map($ctrl.customFields, function (el) {
                        return {
                            text: translate('TEMPLATE_EDITOR.DATA.' + el.name),
                            onclick: function () {
                                editor.insertContent('[[' + translate('TEMPLATE_EDITOR.DATA.' + el.name) + ']]');
                            }
                        }
                    })
                });
            }
        }
    }

})();

