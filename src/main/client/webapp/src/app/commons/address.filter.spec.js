/*
 * address.filter.spec.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
describe("address filter", function () {
    var $filter;

    beforeEach(module('condominiumManagementApp'));

    beforeEach(inject(function (_$filter_) {
        $filter = _$filter_;
    }));

    it("should return an undefined when address is undefined", function () {
        var result = $filter('address')(undefined);
        expect(result).toBeUndefined();
    });

    it("should return undefined if streetName is undefined", function () {
        var address = {
            streetName: undefined,
            buildingNumber: '514',
            postalCode: '32806',
            town: undefined,
            district: 'Orlando',
            province: 'FL',
            country: 'US'
        };
        var result = $filter('address')(address);
        expect(result).toBeUndefined();
    });

    it("should return full formatted address", function () {
        var address = {
            streetName: 'S. Magnolia St.',
            buildingNumber: '514',
            postalCode: '32806',
            town: undefined,
            district: 'Orlando',
            province: 'FL',
            country: 'US'
        };
        var result = $filter('address')(address);
        expect(result).toEqual('S. Magnolia St. 514 Orlando (FL)');
    });

    it("should omit building number if it's undefined", function () {
        var address = {
            streetName: 'S. Magnolia St.',
            buildingNumber: undefined,
            postalCode: '32806',
            town: undefined,
            district: 'Orlando',
            province: 'FL',
            country: 'US'
        };
        var result = $filter('address')(address);
        expect(result).toEqual('S. Magnolia St. Orlando (FL)');
    });

    it("should add town if it's included", function () {
        var address = {
            streetName: 'S. Magnolia St.',
            buildingNumber: '514',
            postalCode: '32806',
            town: 'A Town',
            district: 'Orlando',
            province: 'FL',
            country: 'US'
        };
        var result = $filter('address')(address);
        expect(result).toEqual('S. Magnolia St. 514, A Town, Orlando (FL)');
    })
});