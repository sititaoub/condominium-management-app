(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.commons')
        .component('spinnerLoader', {
            bindings: {},
            templateUrl: 'app/commons/spinner-loader.html'
        });
})();

