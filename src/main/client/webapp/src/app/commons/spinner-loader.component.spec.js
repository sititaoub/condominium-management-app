/*
 * spinner-loader.directive.spec.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
describe('spinner-loader', function () {
    var $httpBackend, $rootScope, $compile, $location;

    beforeEach(module('condominiumManagementApp', 'condominiumManagementApp.templates'));

    beforeEach(inject(function (_$httpBackend_, _$rootScope_, _$compile_, _$location_) {
        $httpBackend = _$httpBackend_;
        $rootScope = _$rootScope_;
        $compile = _$compile_;
        $location = _$location_;
    }));

    beforeEach(function () {
        $httpBackend.whenGET(/profile\/v1\/users\/me$/).respond(200, { authorities: [] });
    });

    it("Should output a spinner template", function () {
        var element = $compile('<spinner-loader></spinner-loader>')($rootScope);
        $rootScope.$digest();

        expect(element.eq(0).html()).toContain('<div class="sk-cube"></div>')
    });
});