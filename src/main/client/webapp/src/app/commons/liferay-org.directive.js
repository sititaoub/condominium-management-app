/*
 * liferay-org.directive.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.commons')
        .directive('liferayOrg', liferayOrg);

    liferayOrg.$inject = ['$location', '$log'];

    /* @ngInject */
    function liferayOrg($location, $log) {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
            var liferayOrg = $location.search().lfOrg || 'neutral';
            $log.info("Detected liferay organization is", liferayOrg);
            element.addClass(liferayOrg);
        }
    }
})();

