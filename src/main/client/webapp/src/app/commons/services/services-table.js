"use strict";
angular.module("condominiumManagementApp.commons")
    .service('TableService',
        [
            'DTOptionsBuilder',
            '$templateCache',
            function(DTOptionsBuilder,$templateCache){
        return {
                    getTableOptions: function ($scope,varName) {
                            if(!varName){
                                varName ='dtOptions';
                            }

                            $scope.rowCallback= function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                    // Unbind first in order to avoid any duplicate handler (see https://github.com/l-lin/angular-datatables/issues/87)
                                    $('td', nRow).unbind('click');
                                    $('td', nRow).bind('click', function() {
                                            $scope.$apply(function() {


                                            });
                                    });
                                    return nRow;
                            }
                            var jsonTableLang = null;
                            var JsonCache = $templateCache.get('app/commons/i18n/it.json');
                            if(Array.isArray(JsonCache)){
                                if(JsonCache[0] == 200) {
                                    jsonTableLang = angular.fromJson(JsonCache[1])
                                }
                            } else {
                                jsonTableLang = angular.fromJson(JsonCache);
                            }

                            $scope[varName] = DTOptionsBuilder.newOptions()
                                .withLanguage(jsonTableLang)
                                .withDOM('<"html5buttons"B>lTfgitp')
                                .withButtons([
                                ])
                                .withOption('rowCallback', $scope.rowCallback)
                                .withOption('lengthMenu', [ 10, 25, 50, 100, 200, 500 ]);
                    }
        }
    }]);


