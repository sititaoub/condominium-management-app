"use strict";
angular.module("condominiumManagementApp.commons")
    .service('PlantStatusService', ['$http', 'contextAddress', function ($http, contextAddress) {
        return {
            setNextPlantStatus: function (id) {
                return $http({
                    method: 'PUT',
                    url: contextAddress + 'v1/plants-status/' + id + '/next'
                });
            },
            setPlantStatusMapped: function (id) {
                return $http({
                    method: 'PUT',
                    url: contextAddress + 'v1/plants-status/' + id + '/set-mapped'
                });
            },
            setPlantStatusConfigured: function (id) {
                return $http({
                    method: 'PUT',
                    url: contextAddress + 'v1/plants-status/' + id + '/set-configured'
                });
            },
            setPlantStatusInstalled: function (id) {
                return $http({
                    method: 'PUT',
                    url: contextAddress + 'v1/plants-status/' + id + '/set-installed'
                });
            },
            setPlantStatusActive: function (id) {
                return $http({
                    method: 'PUT',
                    url: contextAddress + 'v1/plants-status/' + id + '/set-active'
                });
            },
            checkStatusHistoreyValue: function (id, status) {
                return $http({
                    method: 'GET',
                    url: contextAddress + 'v1/plants-status/' + id + '/check-status-history',
                    params: {status: status}
                });
            },
            getStatusHistoryByUser: function () {
                return $http({
                    method: 'GET',
                    url: contextAddress + 'v1/plants-status/current-user'
                });
            }
        }
    }]);
