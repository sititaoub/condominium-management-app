"use strict";
angular.module("condominiumManagementApp.commons")
    .service('LookupService', ['$http', 'contextAddress', function ($http, contextAddress) {
        return {
            getDistricts: function (name) {
                return $http({
                    method: 'GET',
                    url: contextAddress + 'v1/lookups/districts?name=' + name
                });
            }

        }
    }]);
