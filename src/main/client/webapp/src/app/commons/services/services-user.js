"use strict";
angular.module("condominiumManagementApp.commons")
    .service('UserService', ['$http', 'contextAddressProfile', '$window', '$log', '$q',
        function ($http, contextAddressProfile, $window, $log, $q) {

            var self = {
                userData: null,
                roles: null,
                getRoles: function () {
                    if (self.roles == null) {
                        self.roles = {}
                        _.forEach(self.userData.authorities, function (value) {
                            self.roles[value] = function () {
                                return true
                            }
                        })
                    }
                    $log.debug('roles: ');
                    $log.debug(self.roles);
                    return self.roles;
                },
                getUser: function () {
                    return self.userData;
                },
                registerUser: function () {
                    var d = $q.defer();
                    if (self.userData == null) {

                        $http({
                            method: 'GET',
                            url: contextAddressProfile + 'v1/users/me'
                        }).then(function (data) {
                            self.userData = data.data;
                            d.resolve(self.userData);
                        }, function (error) {
                            $log.error('Error retrieve user me info: ');
                            $log.error(error);
                            self.userData = {
                                username: 'admin@cedac.com',
                                firstName: 'Gianmarco',
                                lastName: 'Parini',
                                authorities: ['USER', 'INSTALLER', 'MANAGER', 'ADMIN']
                            };
                            d.resolve(self.userData);
                        });

                    } else {
                        d.resolve(self.userData);

                    }
                    return d.promise;
                },
                logoutUser: function () {
                    var cookies = _.zipObject(
                        _.map(document.cookie.split(":"), function (el) {
                            return el.split("=")
                        })
                    );
                    return $http({
                        method: 'POST',
                        headers: {'X-XSRF-TOKEN': cookies["XSRF-TOKEN"]},
                        url: $rootScope.logoutUrl,
                        withCredentials: true
                    }).then(function (response) {
                        $rootScope.user = null;
                        $window.location.reload();
                        return response;
                    });
                },
                changePassword: function (passwd) {
                    return $http({
                        method: 'POST',
                        url: contextAddressProfile + 'v1/users/me/password-changes',
                        data: passwd
                    });
                }
            }

            return self;
        }]);


