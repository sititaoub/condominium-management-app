"use strict";
angular.module("condominiumManagementApp.commons")
    .service('ChartService',['$http','$rootScope', '$log', function($http,$rootScope, $log){
         /*
            var DEBUG = true;
            var logConsole = function(value){
                if(DEBUG){
                    console.log(value);
                }
            }
        */
        /* was pairs from lodash */
        var arrayMap = function(array, iteratee) {
            var index = -1,
                length = array ? array.length : 0,
                result = Array(length);

            while (++index < length) {
                result[index] = iteratee(array[index], index, array);
            }
            return result;
        }
        var mapToArray = function(map) {
            var index = -1,
                result = Array(map.size);

            map.forEach(function(value, key) {
                result[++index] = [key, value];
            });
            return result;
        }
        var baseToPairs = function(object, props) {
            return arrayMap(props, function(key) {
                return [key, object[key]];
            });
        }

        var setToPairs = function(set) {
            var index = -1,
                result = Array(set.size);

            set.forEach(function(value) {
                result[++index] = [value, value];
            });
            return result;
        }
        var mapTag = '[object Map]',
            setTag = '[object Set]';
        var objectToString = Object.prototype.toString;
        function getTag(value) {
            return objectToString.call(value);
        }

        var createToPairs = function(keysFunc) {
            return function(object) {
                var tag = getTag(object);
                if (tag == mapTag) {
                    return mapToArray(object);
                }
                if (tag == setTag) {
                    return setToPairs(object);
                }
                return baseToPairs(object, keysFunc(object));
            };
        }

        var newToPairs = createToPairs(_.keys)
        var baseSum = function(array, iteratee) {
            var result,
                index = -1,
                length = array.length;

            while (++index < length) {
                var current = iteratee(array[index]);
                if (current !== undefined) {
                    result = result === undefined ? current : (result + current);
                }
            }
            return result;
        }

        var sumBy = function(array, iteratee) {
            return (array && array.length)
                ? baseSum(array, _.iteratee(iteratee))
                : 0;
        }

        var objToArrayChart = function(obj){
            var arrayChart = []
            _.forEach(obj,function(val,key){
                var arrayChartInt = [];
                arrayChartInt.push(key);
                arrayChartInt.push(val);
                arrayChart.push(arrayChartInt);
            })
            return arrayChart;
        }

            var remapValueToObjectArray = function(dataOrigin,doSum){
                    var start = null;
                    var stop = null;

                    start = new Date();
                    // from array to object
                    $log.debug('mapObjectArray start ' + start);

                    var mapObjectArray = [];

                    _.forEach(dataOrigin, function (obj, indexOrigin, arrays) {
                            var mapObject = {};
                            var label = doSum ?  'sum':obj.uuid;

                            _.forEach(obj.points, function (a, i) {
                                    var obj = {};
                                    obj['date'] = a.date;
                                    obj['label'] = label;
                                    obj[label] = a[label];
                                    mapObject[obj['date']] = obj
                            });
                            mapObjectArray.push(mapObject);

                    });

                    stop = new Date();
                    $log.debug('mapObjectArray end ' + stop);
                    $log.debug('mapObjectArray take ' + (stop - start));
                    $log.debug(mapObjectArray);
                    return mapObjectArray;
            }

        function mergeObject(mapObjectArray ,doSum) {
                var start = null;
                var stop = null;

                start = new Date();
                $log.debug('objectmerge start '+start);
                var mapObject = {};
                _.forEach(mapObjectArray,function(obj,index){

                        _.each(obj, function(findObj2) {
                            var findObj1 = mapObject[findObj2['date']];
                            if(findObj1){
                                 // elemento presente in dst devo aggiungere i punti o sommare
                                if(doSum) {
                                    findObj1['sum'] = findObj1['sum'] + findObj2['sum'];
                                } else {
                                    _.extend(findObj1, _.cloneDeep(findObj2));
                                }
                            } else {
                                // elemento non presente lo copio
                                mapObject[findObj2['date']]= _.cloneDeep(findObj2);
                            }
                        });

                });

                stop = new Date();
                $log.debug('objectmerge stop '+stop);
                $log.debug('objectmerge take '+(stop-start));
                //console.log(mapObject);

            var mapObjectToArray;
            mapObjectToArray = _.map(mapObject,function(val,index){
                    return val;
            });
            return mapObjectToArray;

                // per ogni elmento valuto presenza in dest
        }



            var mergeAndSumNoFill = function (dataOrigin,doSum) {

                    var start = null;
                    var stop = null;

                    // from array to object
                    var mapObjectArray = remapValueToObjectArray(dataOrigin,doSum);

                    // merge
                    var mapObjectToArray = mergeObject(mapObjectArray,doSum);

                    // sort
                    mapObjectToArray = _.sortBy(mapObjectToArray,'date');
                    $log.debug('object to array merge');
                    $log.debug(mapObjectToArray);

                    return mapObjectToArray;
            }

            var remapDiffAndSort  = function (dataOrigin,label,doDiff) {
                var kairosData = [];
                _.forEach(dataOrigin, function (obj, indexOrigin, arrays) {
                    $log.debug(obj);
                    if(!label){
                        label = 'sum';
                    }

                    var mapObject = {};
                    mapObject['date'] = new Date(obj.timestamp);
                    mapObject['label'] = label;
                    mapObject[label] = parseFloat(obj.point).toFixed(2);

                    kairosData.push(mapObject);
                    $log.debug('remapDiffAndSort remap value');
                    $log.debug(mapObject);
                });

                kairosData = _.sortBy(kairosData,'date');
                if(doDiff){
                    $log.debug('remapDiffAndSort doDiff');
                    for(var i=kairosData.length-1;i>0;i--){
                        kairosData[i][label] = (kairosData[i][label] - kairosData[i-1][label]).toFixed(2);
                        kairosData[i]['value'] = kairosData[i][label];
                        $log.debug('remapDiffAndSort doDiff value:');
                        $log.debug(kairosData[i][label]);
                    }
                    kairosData.splice(0,1);
                }
                return kairosData;
            }

            var generateClusterdChartData = function(dataOrigin, doDiffAndSum) {

                var allPoints = [];
                _.forEach(dataOrigin, function (d, index) {
                    d.points = remapDiffAndSort(d.points, d.uuid, doDiffAndSum);
                    allPoints = allPoints.concat(d.points);
                });

                if(doDiffAndSum){
                    /* calculating total sum of heat cost all. values for each sample */

                    var allPointsByDate = _.groupBy(allPoints, "date"); // eg { date1: [{value: x}], date2: [{value: y}] }

                    //var sumArray = _.toPairs(allPointsByDate); // [ [date1, [{value: x}], [date2, [{value: 5}] ]
                    var sumArray = newToPairs(allPointsByDate); // [ [date1, [{value: x}], [date2, [{value: 5}] ]

                    var sumPoints = _.map(sumArray, function (obj) {
                        //var sum = _.sumBy(obj[1], function(elem) {
                        var sum = sumBy(obj[1], function (elem) {
                            return parseFloat(elem.value);
                        });
                        return {date: obj[1][0].date, label: "sumamt", "sumamt": sum.toFixed(2)};
                    });

                    var sumData = {"uuid": "sumamt", "description": "sum", "points": sumPoints};
                    dataOrigin.push(sumData);
                    /* end calculating total sum of heat cost all. values for each sample */
                }
                return mergeAndSumNoFill(dataOrigin,false);

            }

            var generateStackedChartData = function(dataOrigin){
                _.forEach(dataOrigin, function (d, index) {
                    d.points = remapDiffAndSort(d.points, d.uuid,true);
                });

                return mergeAndSumNoFill(chartData,false);

            }

            return {
                    mergePointAndSumNofill: function(dataOrigin,doSum){
                            return mergeAndSumNoFill(dataOrigin,doSum)
                    },
                    remapAndSort: function(dataOrigin,label){
                        return remapDiffAndSort(dataOrigin,label,false);
                    },
                    remapDiffAndSort: function(dataOrigin,label){
                        return remapDiffAndSort(dataOrigin,label,true);
                    },
                    generateClusterdChartData: function(dataOrigin,doDiffAndSum){
                        return generateClusterdChartData(dataOrigin,doDiffAndSum);
                    },
                    generateStackedChartData: function(dataOrigin){
                        return generateStackedChartData(dataOrigin);
                    },
                    sumBy: sumBy,
                    toPairs: newToPairs

            }
}]);


