"use strict";
angular.module("condominiumManagementApp.commons")
    .service('UtilsService',[
            "$locale",
            "$log",
            "$filter",
            'uiGmapGoogleMapApi',
            '$localStorage',
            function($locale, $log, $filter, uiGmapGoogleMapApi,$localStorage){

                var self = {
                    locations: ($localStorage.locations ? JSON.parse($localStorage.locations) : {}),
                    convertDateStringsToDates:function(input) {
                        var regexIso8601 = /^(\d{4}|\+\d{6})(?:-(\d{2})(?:-(\d{2})(?:T(\d{2}):(\d{2}):(\d{2})\.(\d{1,})(Z|([\-+])(\d{2}):(\d{2}))?)?)?)?$/;
                        // Ignore things that aren't objects.
                        if (typeof input !== "object") return input;

                        for (var key in input) {
                            if (!input.hasOwnProperty(key)) continue;

                            var value = input[key];
                            var match;
                            // Check for string properties which look like dates.
                            if (typeof value === "string" && (match = value.match(regexIso8601))) {
                                var milliseconds = Date.parse(match[0])
                                if (!isNaN(milliseconds)) {
                                    input[key] = new Date(milliseconds);
                                }
                            } else if (typeof value === "object") {
                                // Recurse into object
                                convertDateStringsToDates(value);
                            }
                        }
                    },
                    gmapGeocoder: function(address,callback,params){
                        if (_.has(self.locations, address)) {
                            callback(self.locations[address], params)

                        } else {
                            var geocoder = new google.maps.Geocoder();
                            geocoder.geocode({ address : address }, function (result, status) {

                                if (status === google.maps.GeocoderStatus.OK) {
                                    var latLng = {
                                        lat: result[0].geometry.location.lat(),
                                        lng: result[0].geometry.location.lng()
                                    };

                                    self.locations[address] = latLng;
                                    $localStorage.locations = JSON.stringify(self.locations);

                                    $log.debug('lat: '+latLng.lat + ' lng: '+latLng.lng);
                                    callback(latLng,params);

                                } else if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
                                    $log.info('gmap Zero results for geocoding address ' + address);
                                } else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                                    $log.info('gmap Query limit ');
                                } else if (status === google.maps.GeocoderStatus.REQUEST_DENIED) {
                                    $log.info('gmap Request denied for geocoding address ' + address);
                                } else if (status === google.maps.GeocoderStatus.INVALID_REQUEST) {
                                    $log.info('gmap Invalid request for geocoding address ' + address);
                                }
                            });
                        }
                    },
                    geocoder: function(plantAddress,callback) {
                        var address = plantAddress.streetName + ' ' +
                            plantAddress.buildingNumber + ' ' +
                            plantAddress.town + ' ' +
                            plantAddress.postalCode;
                        $log.debug('Address: ' + address);
                        this.gmapGeocoder(address,callback,[plantAddress.town]);
                    },
                    formatterNumber: function (sNumber) {
                        var andIndex = sNumber.indexOf('&');
                        if(andIndex!=-1){
                            sNumber = sNumber.substr(0, andIndex);
                        }
                        var andIndex = sNumber.indexOf(' ');
                        if(andIndex!=-1){
                            sNumber = sNumber.substr(0, andIndex);
                        }

                        var formatter = $locale.NUMBER_FORMATS;
                        var dec = sNumber.indexOf(formatter.DECIMAL_SEP);
                        var group =sNumber.indexOf(formatter.GROUP_SEP);

                        var noDec = sNumber;
                        if(dec!=-1){
                            noDec = sNumber.substr(0, dec) + '.' + sNumber.substr(dec+1);
                        }
                        var noGroup = noDec;
                        if(group!=-1) {
                            noGroup = noDec.substr(0, group) + noDec.substr(group+1);
                        }
                        return Number(noGroup);
                    },
                    timeUnitConstant: {
                        YEAR: 1
                    },
                    backNav: function(){
                        $log.debug('pre back : ')
                        $log.debug(window.history)

                        window.history.back();

                        $log.debug('post back: ')
                        $log.debug(window.history)

                    },
                    showHttpError: function(data, toaster, httpStatus){
                        $log.debug('error loading data');
                        $log.debug('error code'+data.errorCode);
                        $log.debug('user message'+data.userMessage);
                        $log.debug('error loading data');
                        var textError = 'Errore caricamento dati';
                        if (httpStatus == "409") {
                            textError = $filter('translate')(data.userMessage);
                        }
                        toaster.pop({
                            type: 'error',
                            title: textError,
                            showCloseButton: true,
                            timeout: 4000
                        });

                    },
                    getCookie: function(name){
                        var match = document.cookie.match(new RegExp(name + '=([^;]+)'));
                        if (match) return match[1]
                        else return undefined;
                    },
                    showHttpSuccess: function(toaster){
                        $log.debug('success loading data');
                        toaster.pop({
                            type: 'success',
                            title: 'Reset effettuato',
                            showCloseButton: true,
                            timeout: 3000
                        });

                    },
                    linkQualityToIndex: function(value){
                        if (value <= 10) return '1';
                        if (value <= 30) return '2';
                        if (value <= 50) return '3';
                        if (value <= 70) return '4';
                        return '5';
                    },
                    datePicker: function(){
                        var self = {
                            format: "dd-MM-yyyy",
                            opened: false,
                            openDatePicker: function() { self.opened = true;  },
                            altInputFormats: ['d!-M!-yyyy'],
                            dateOptions: {
                                altInputFormats: ['d!-M!-yyyy'],
                            }
                        }
                        return self;
                    }

                }

        return self;

    }]);


