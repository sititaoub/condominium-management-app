"use strict";
angular.module("condominiumManagementApp.commons")
    .service('CheckDeviceService',['$http','contextAddress', function($http,contextAddress){
            return {
                    getDeviceBySerialNumber: function(id, deviceType, serialNumbers){
                            return $http({
                                    method: 'GET',
                                    url: contextAddress + 'v1/check-devices/search-by-serial-number/iot-plants/' + id + '/',
                                    params: {
                                            deviceType: deviceType,
                                            serialNumbers: serialNumbers.join(),
                                            size: serialNumbers.length
                                    }
                            });
                    },
                    getGateways: function (id) {
                            return $http({
                                    method: 'GET',
                                    url: contextAddress + 'v1/check-devices/iot-plants/' + id + '/gateways/',
                                    params: {size: 500}
                            });
                    },
                    getMeters: function (id) {
                            return $http({
                                    method: 'GET',
                                    url: contextAddress + 'v1/check-devices/iot-plants/' + id + '/meters/',
                                    params: {size: 500}
                            });
                    }

    } }]);
