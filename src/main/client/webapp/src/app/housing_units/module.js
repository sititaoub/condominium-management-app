"use strict";

angular.module("condominiumManagementApp.housing_units", [
    'condominiumManagementApp.core',
    'condominiumManagementApp.commons'
]);
