'use strict';

angular.module("condominiumManagementApp.housing_units")
	.config(function ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('index.housing_units', {
				abstract: true,
				url: "/{condominiumId}/housing_units",
				template: '<ui-view/>'
			})
			.state('index.housing_units.detail', {
				url: "/{id}",
				templateUrl: "app/housing_units/partials/detail.html",
				controller: "housing_units.DetailController",
				data: {
					requiresAuthentication: true,
					permissions: {
						only: 'ROLE_INSTALLER_ACTIVE'
					}
				}
			})
			.state('index.housing_units.add_to_condominium', {
				url: "/add-to-condominium/{huId}",
				templateUrl: "app/housing_units/partials/housing-unit.html",
				controller: "housing_units.HousingUnitController",
				data: {
					requiresAuthentication: true,
					permissions: {
						only: 'ROLE_INSTALLER_INSTALLATION'
					}
				}
			});

	});
