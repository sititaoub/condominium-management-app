"use strict";
angular.module("condominiumManagementApp.housing_units")
    .factory("HousingUnitResource", ["$resource", "contextAddress", function ($resource, contextAddress) {
        return $resource(contextAddress + "v1/housing-units/:id",
            {id: '@id'},
            {
                update: {method: 'PUT'}
            });

    }])
    .factory("HousingUnitGroupResource", ["$resource", "contextAddress", function ($resource, contextAddress) {
        return $resource(contextAddress + "v1/housing-unit-groups/:id",
            {id: '@id'},
            {
                update: {method: 'PUT'}
            });

    }]);
