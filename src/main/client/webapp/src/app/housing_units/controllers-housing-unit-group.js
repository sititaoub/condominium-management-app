
"use strict";


angular.module("condominiumManagementApp.housing_units")
	.controller("housing_units.HousingUnitGroupCtrl", [
    "$scope",
    "$rootScope",
    "$state",
    "$stateParams",
    "toaster",
    "LookupService",
    "HousingUnitGroupResource",
    "CondominiumResource",
    "$filter",
    "SweetAlert",
    function ($scope, $rootScope, $state, $stateParams,toaster,
              LookupService,HousingUnitGroupResource, CondominiumResource, $filter, SweetAlert) {
        $scope.condominiumId = $stateParams.condominiumId || $stateParams.id;
        $scope.huGroupId = $stateParams.huGroupId;
        $scope.description = $stateParams.description;

        $scope.back = function(){
            $scope.$emit("backHousingUnitGroup", {id:$scope.huGroupId});
        };

        $scope.formData = {
            address:{
                "country": "IT"
            }
        };


        //Se è un Edit
        if ($scope.huGroupId != 'new') {
            $scope.$emit("selectHousingUnitGroup", {id:$scope.huGroupId});

            $scope.housingUnitGroupLoading = true;
            $scope.formData = HousingUnitGroupResource.get({id: $scope.huGroupId}, function (data) {
                $scope.housingUnitGroupLoading = false;

                $scope.district.selected={
                    'name':data.address.district,
                    'province':data.address.province
                };
            },function(){
                $scope.housingUnitGroupLoading = false;

            });


        } else { //Se è un nuovo inserimento
            $scope.housingUnitGroupLoading = false;
            $scope.$emit("preaddHousingUnitGroup");

             $scope.condominium = CondominiumResource.get({id: $scope.condominiumId }, function (data) {
                 $scope.formData.address.streetName = $scope.condominium.address.streetName;
                 $scope.formData.address.buildingNumber = $scope.condominium.address.buildingNumber;
                 $scope.formData.address.town = $scope.condominium.address.town;
                 $scope.formData.address.postalCode = $scope.condominium.address.postalCode;
                 $scope.formData.address.town = $scope.condominium.address.town;
                 $scope.formData.address.province = $scope.condominium.address.province;

                 $scope.district.selected={

                     'name':$scope.condominium.address.district,
                     'province':$scope.condominium.address.province
                 };


            });




        }


        //Datepicker
        $scope.format="dd-MM-yyyy";
        $scope.popupFrom = { opened: false  };
        $scope.openFrom = function() { $scope.popupFrom.opened = true;  };


        //District
        $scope.district={selected:""};
        $scope.oldQuery="";

        $scope.onSelectDistrictCallback= function(item, model){
            $scope.formData.address.province=item.province;
            $scope.formData.address.district=item.name;

        }

        $scope.funcAsync = function (query) {
            /*La ricerca su 8000 comuni è molto lenta di conseguenza queste casistiche servono a ridurre
             al minimo il numero di chiamate
             */
            var strQuery= query.substr(0,3);
            var strOldQuery= $scope.oldQuery.substr(0,3);

            if (((strQuery!=strOldQuery)&&(query.length>2)))
            {
                LookupService.getDistricts(query).then(function (data) {

                    $scope.districtsLookup = data.data;
                });
            }

            if (query.length<=3)
            {
                $scope.districtsLookup=[];
            }

            $scope.oldQuery=query;
        };

        $scope.submitForm = function(form) {
            if (form.$valid) {

                if ($scope.formData.address.postalCode=="")  $scope.formData.address.postalCode=null;
                if ($scope.formData.address.province=="")  $scope.formData.address.province=null;
                $scope.formData.condominiumId = $scope.condominiumId;

                if ($scope.huGroupId=='new') {

                    $scope.newHUG = new HousingUnitGroupResource($scope.formData);
                    $scope.newHUG.address.district = $scope.district.selected.name;

                    HousingUnitGroupResource.save($scope.newHUG,
                        function(hUnitGroup, headers){
                            toaster.pop({
                                type: 'success',
                                title: 'Salvataggio effettuato',
                                showCloseButton: true,
                                timeout: 2000
                            });
                            hUnitGroup.description = $scope.formData.stair;
                            $scope.huGroupId = hUnitGroup.id = headers('location').split("/").pop();
                            $scope.$emit("addHousingUnitGroup", hUnitGroup);

                        },
                        function(){
                            toaster.pop({
                                type: 'error',
                                title: 'Si è vericato un problema durante il salvataggio',
                                showCloseButton: true,
                                timeout: 2000
                            });
                        });
                } else {
                    $scope.formData.$update({id: $scope.huGroupId},
                        function(hUnitGroup){
                            toaster.pop({
                                type: 'success',
                                title: 'Salvataggio effettuato',
                                showCloseButton: true,
                                timeout: 2000
                            });
                            hUnitGroup.description = $scope.formData.stair;
                            $scope.$emit("editHousingUnitGroup", hUnitGroup);
                        },
                        function(){
                            toaster.pop({
                                type: 'error',
                                title: 'Si è vericato un problema durante il salvataggio',
                                showCloseButton: true,
                                timeout: 2000
                            });
                        });
                }

            } else {
                form.submitted = true;
            }
        }


        $scope.delete= function() {

            SweetAlert.swal({
                    title: "Vuoi davvero eliminare la scala?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    animation: "slide-from-top"
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $scope.formData.$remove({id: $scope.huGroupId},
                            function(response){
                                toaster.pop({
                                    type: 'success',
                                    title: 'Eliminazione effettuata',
                                    showCloseButton: true,
                                    timeout: 2000
                                });

                                $scope.$emit("removeHousingUnitGroup", {id:$scope.huGroupId});
                                // redirect putted into event handler

                            },
                            function(response){
                                toaster.pop({
                                    type: 'error',
                                    title: $filter('translate')(response.data.userMessage),
                                    showCloseButton: true,
                                    timeout: 10000
                                });
                            });
                    }


                });



        };



    }


]);


