"use strict";

angular.module("condominiumManagementApp.housing_units")
    .directive('validchartdate', [
    function() {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                var validateFn = function (viewValue) {
                    if (!ctrl.$isEmpty(viewValue)){

                        if (viewValue.toLowerCase().indexOf(attrs.mustcontainword.toLowerCase()) === -1) {
                            ctrl.$setValidity('validchartdate', false);
                            return undefined;
                        }
                    }
                    ctrl.$setValidity('validchartdate', true);
                    return viewValue;

                };

                ctrl.$parsers.push(validateFn);
                ctrl.$formatters.push(validateFn);
            }
        }
    }]);