"use strict";
angular.module("condominiumManagementApp.housing_units")
    .controller('housing_units.MoreDetailCtrl',
        [
            '$scope',
            '$rootScope',
            '$uibModalInstance',
            function($scope, $rootScope, $uibModalInstance ){
                $scope.close = function(){
                    $uibModalInstance.close();

                }



            }
        ]
    );


