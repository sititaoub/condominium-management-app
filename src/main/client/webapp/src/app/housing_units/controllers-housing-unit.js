
"use strict";


angular.module("condominiumManagementApp.housing_units")
	.controller("housing_units.HousingUnitController", [
    "$scope",
    "$rootScope",
    "$state",
    "$stateParams",
    "toaster",
    "LookupService",
    "HousingUnitResource",
    "$filter",
    "SweetAlert",
    function ($scope, $rootScope, $state, $stateParams,toaster,
              LookupService,HousingUnitResource,$filter,SweetAlert) {
        $scope.condominiumId = $stateParams.condominiumId || $stateParams.id;
        $scope.huId = $stateParams.huId;
        $scope.huGroupId = $stateParams.huGroupId;
        $scope.description = $stateParams.description;

        $scope.back = function(){
            $scope.$emit("backHousingUnit", {id:$scope.huId,huGroupId:$scope.huGroupId});
        };

        //Se è un Edit
        if ($scope.huId != 'new') {
            $scope.$emit("selectHousingUnit", {id:$scope.huId,huGroupId:$scope.huGroupId});
            $scope.personLoading = true;
            $scope.formData = HousingUnitResource.get({id: $scope.huId}, function (data) {
                $scope.personLoading = false;

                $scope.phone=$scope.formData.contacts['PHONE'];
                $scope.mobile=$scope.formData.contacts['MOBILE'];;
                $scope.email=$scope.formData.contacts['EMAIL'];;


            },function(){
                $scope.personLoading = false;

            });


        } else { //Se è un nuovo inserimento
            $scope.personLoading = false;

            $scope.$emit("preaddHousingUnit", {huGroupId:$scope.huGroupId});

            $scope.formData = {
                identification:{},
                cadastralReference: {},
                address:{
                    "country": "IT"
                }
            };


        }


        //Datepicker
        $scope.format="dd-MM-yyyy";
        $scope.popupFrom = { opened: false  };
        $scope.openFrom = function() { $scope.popupFrom.opened = true;  };


        //District
        $scope.district={selected:""};
        $scope.oldQuery="";

        $scope.onSelectDistrictCallback= function(item, model){
            $scope.formData.address.province=item.province;
            $scope.formData.address.district=item.name;

        }

        $scope.funcAsync = function (query) {
            /*La ricerca su 8000 comuni è molto lenta di conseguenza queste casistiche servono a ridurre
             al minimo il numero di chiamate
             */
            var strQuery= query.substr(0,3);
            var strOldQuery= $scope.oldQuery.substr(0,3);

            if (((strQuery!=strOldQuery)&&(query.length>2)))
            {
                LookupService.getDistricts(query).then(function (data) {

                    $scope.districtsLookup = data.data;
                });
            }

            if (query.length<=3)
            {
                $scope.districtsLookup=[];
            }

            $scope.oldQuery=query;
        };

        $scope.submitForm = function(form) {
            if (form.$valid) {

                $scope.formData.condominiumId = $scope.condominiumId;
                var arr = {};
                var phone =  $scope.phone;
                arr['PHONE'] = phone;
                var mobile =  $scope.mobile;
                arr['MOBILE'] = mobile;
                var email =  $scope.email;
                arr['EMAIL'] = email;

                $scope.formData.contacts = arr;



                if ($scope.huId=='new') {
                    $scope.formData.housingUnitGroupId = $scope.huGroupId;

                    $scope.newHU = new HousingUnitResource($scope.formData);

                    HousingUnitResource.save($scope.newHU,
                        function(hUnit, headers){
                            toaster.pop({
                                type: 'success',
                                title: 'Salvataggio effettuato',
                                showCloseButton: true,
                                timeout: 2000
                            });
                            $scope.description = hUnit.owner;
                            $scope.huId = hUnit.id = headers('location').split("/").pop();
                            $scope.$emit("addHousingUnit", hUnit);

                        },
                        function(){
                            toaster.pop({
                                type: 'error',
                                title: 'Si è vericato un problema durante il salvataggio',
                                showCloseButton: true,
                                timeout: 2000
                            });
                        });
                } else {
                    $scope.formData.housingUnitGroupId = $scope.huGroupId;
                    $scope.formData.$update({id: $scope.huId},
                        function(hUnit){
                            toaster.pop({
                                type: 'success',
                                title: 'Salvataggio effettuato',
                                showCloseButton: true,
                                timeout: 2000
                            });
                            $scope.description = hUnit.owner;
                            $scope.$emit("editHousingUnit", hUnit);
                        },
                        function(){
                            toaster.pop({
                                type: 'error',
                                title: 'Si è vericato un problema durante il salvataggio',
                                showCloseButton: true,
                                timeout: 2000
                            });
                        });
                }

            } else {
                form.submitted = true;
            }
        }


        $scope.delete= function() {

            SweetAlert.swal({
                    title: "Vuoi davvero eliminare l'alloggio?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    animation: "slide-from-top"
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $scope.formData.$remove({id: $scope.huId},
                            function(response){
                                toaster.pop({
                                    type: 'success',
                                    title: 'Eliminazione effettuata',
                                    showCloseButton: true,
                                    timeout: 2000
                                });

                                $scope.$emit("removeHousingUnit", {id:$scope.huId});
                                // redirect putted into event handler

                            },
                            function(response){
                                toaster.pop({
                                    type: 'error',
                                    title: $filter('translate')(response.data.userMessage),
                                    showCloseButton: true,
                                    timeout: 10000
                                });
                            });
                    }


                });



        };



    }


]);


