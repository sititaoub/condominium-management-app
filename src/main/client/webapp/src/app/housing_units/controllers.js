
"use strict";


angular.module("condominiumManagementApp.housing_units")
    .controller("housing_units.ListController", [
        "$scope",
        "$state",
        "$stateParams",
        "$timeout",
        "DTOptionsBuilder",
        "DTColumnBuilder",
        "HousingUnitResource",
        "CondominiumResource",
        "TableService",
        function ($scope, $state,$stateParams, $timeout,DTOptionsBuilder, DTColumnBuilder,HousingUnitResource,
                  CondominiumResource, TableService) {

            TableService.getTableOptions($scope);

            $scope.condominium_data = CondominiumResource.get({condominiumId: $scope.id}, function () {
                //Callback
                $scope.condominium_housing_unit = $scope.condominium_data.housingUnits;
            });

        }


    ]).controller("housing_units.DetailController", [
    "$scope",
    '$uibModal',
    "$state",
    "$stateParams",
    "$timeout",
    "DTOptionsBuilder",
    "DTColumnBuilder",
    "HousingUnitResource",
    "HeatCostCalculatorService",
    "TableService",
    "UtilsService",
    function ($scope, $uibModal, $state,$stateParams, $timeout,DTOptionsBuilder, DTColumnBuilder,HousingUnitResource,
              HeatCostCalculatorService, TableService, UtilsService) {

        $scope.nowDate = new Date();

        TableService.getTableOptions($scope);
        var computeSums = function(row, data, start, end, display){
            var sums = {
                costAllocator: 0,
            };

            for(var i=start;i<end;i++){
                sums.costAllocator += UtilsService.formatterNumber(data[i][3]);
            }

            return sums;
        }

        $scope.dtOptions = $scope.dtOptions
            .withOption('footerCallback', function (row, data, start, end, display) {
                if (data.length > 0) {
                    // Need to call $apply in order to call the next digest
                    $scope.$apply(function () {
                        $scope.sums = computeSums(row, data, start, end, display);
                    });
                }
            });

        $scope.backToCondominium = function(){
            UtilsService.backNav();
            //$state.transitionTo('index.condominiums-detail',{id:$scope.condominiumId});
        };

        $scope.moreDetailModal=function(){
            var modalInstance = $uibModal.open({
                templateUrl: 'app/housing_units/partials/more-detail.html',
                controller:'housing_units.MoreDetailCtrl',
                size: 'lg',
                scope: $scope
            });

        }

        $scope.showCostAllocatorCalculatorModal = function(costAllocatorId){
            $scope.costAllocatorIdShowModal = costAllocatorId;
            var modalInstance = $uibModal.open({
                templateUrl: 'app/devices/partials/cost-allocator-calculator-modal.html',
                controller:'devices.CostAllocatorCalculatorModalCtrl',
                size: 'lg',
                scope: $scope
            });
        }

        $scope.housing_units_data = HousingUnitResource.get({id: $stateParams.id}, function (data) {

            $scope.condominiumId=data.condominiumId;
        });

        $scope.heatCostAllocatorsLoading = true;
        HeatCostCalculatorService.getHeatCostsHousingUnitsAllocator($stateParams.id).then(function (data){
            $scope.heatCostAllocatorsLoading = false;
            $scope.housing_units_costs_allocator_data = data.data;
        },function(data){
            $scope.heatCostAllocatorsLoading = false;

        });

        $scope.heatMetersLoading = true;
        HeatCostCalculatorService.getHeatCostsHousingUnitsMeter($stateParams.id).then(function (data){
            $scope.heatMetersLoading = false;
            $scope.housing_units_costs_meter_data = data.data;

        },function(data){
            $scope.heatMetersLoading = false;

        });

        $scope.waterMetersLoading = true;
        HeatCostCalculatorService.getHeatCostsHousingUnitsWaterMeter($stateParams.id).then(function (data){
            $scope.waterMetersLoading = false;
            $scope.housing_units_costs_water_meter_data = data.data;

        },function(data){
            $scope.waterMetersLoading = false;

        });

    }


]).controller("housing_units.SummaryChartController", [
    "$scope",
    "$state",
    "$stateParams",
    "$timeout",
    "$log",
    "toaster",
    "HeatCostCalculatorService",
    "ChartService",
    "UtilsService",
    function ($scope, $state, $stateParams, $timeout, $log, toaster,
        HeatCostCalculatorService, ChartService, UtilsService) {

        $scope.chartType = "clustered"; //$scope.clusteredChartSelected = 'true';

        var showChart = function() {
            if ($scope.chartType == "clustered") { //if ($scope.clusteredChartSelected == 'true') {
                showClusteredChart();
            } else {
                //if (chartId == "stacked") {
                showStackedChartOld();
                //} else {
                //    showStackedChartOld();
                //}
            }
        }

        $scope.toggleChart = function(chartType) {
            $scope.chartType = chartType; //$scope.clusteredChartSelected = !$scope.clusteredChartSelected;

            showChart();
        };

        $scope.popupFromUUID = { opened: false  };
        $scope.openFromUUID = function() { $scope.popupFromUUID.opened = true;  };

        $scope.chartUUID = {
            startDate: new Date()
        };

        $scope.$watch('chartUUID.startDate',function(newValue, oldValue) {
            if(newValue) {
                $log.debug('value:'+newValue)
                heatCostsHousingUnitsUuidsChart(newValue);
            }
        })

        var heatCostsHousingUnitsUuidsChart = function(startDate) {
            $scope.chartUUID.loading = true
            HeatCostCalculatorService.getHeatCostsHousingUnitsUuidsChart($stateParams.id, startDate).then(function (data) {
                $scope.chartUUID.loading = false
                $scope.chartData = data.data;

                showChart();
            },function(data){
                $scope.chartUUID.loading = false
                if (data.status=="404" && data.statusText=="Not Found") {
                    $scope.chartData = [];
                } else {
                    UtilsService.showHttpError(data.data, toaster);
                }
            });
        }

        var showClusteredChart = function() {
            var chartData = _.cloneDeep($scope.chartData);
            var kairosDataDetail;

            kairosDataDetail = ChartService.generateClusterdChartData(chartData)

            var lastItem = kairosDataDetail[kairosDataDetail.length - 1];
            lastItem.dashLengthColumn = 3;
            lastItem.alpha = 0.4;
            lastItem.additional = " (in corso)";

            var graphsDetail = [];
            _.forEach(chartData, function (d, index) {
                if (d.description !== 'sum') {
                    var g = {
                        "id": "g" + index,
                        "title": d.description,
                        "fillAlphas": 0.6,
                        "type": "column",
                        "valueField": d.uuid,
                        "balloonText": "<div style='margin:3px; font-size:12px;'>[[title]] <b>[[value]]</b></div>"
                    }
                    graphsDetail.push(g);
                }
            });

            var graphSum = {
                "id": "gsum",
                "showBalloon": false,
                "title": "Totale",
                "fillAlphas": 0.05,
                "fillColor": "#ff0000",
                "type": "step",
                "valueField": "sumamt",
                "labelText": "[[value]]",
                "fontSize": 13,
                "balloonText": "<div style='margin:3px; font-size:12px;'>[[title]] <b>[[value]]</b>[[additional]]</div>",
                //"alphaField": "alpha",
                "dashLengthField": "dashLengthColumn",
                "animationPlayed": true
            }
            graphsDetail.push(graphSum);

            var clustered = AmCharts.makeChart("chartDivHousingUnitClustered", {
                "type": "serial",
                "theme": "light",
                "marginRight": 80,
                "dataProvider": kairosDataDetail,
                "legend": {
                    "divId": "legenddiv",
                    "equalWidths": true,
                    "position": "top",
                    "valueAlign": "left",
                    //"valueWidth": 100
                },
                "valueAxes": [{
                    "position": "left"
                }],
                //"startAlpha": 0.3,
                "startDuration": 0.3,
                "graphs": graphsDetail,
                "chartScrollbar": {
                    "scrollbarHeight": 24,
                    "backgroundAlpha": 0,
                    "selectedBackgroundAlpha": 0.1,
                    "selectedBackgroundColor": "#888888",
                    "graphFillAlpha": 0,
                    "graphLineAlpha": 0.5,
                    "selectedGraphFillAlpha": 0,
                    "selectedGraphLineAlpha": 1,
                    "autoGridCount": true,
                    "color": "#AAAAAA"
                },
                "chartCursor": {
                    "fullWidth":true,
                    "cursorAlpha": 0.1,
                    "categoryBalloonEnabled": false,
                    "categoryBalloonDateFormat": "DD-MM-YYYY",
                    "cursorPosition": "mouse"
                },
                "categoryField": "date",
                "categoryAxis": {
                    //"gridPosition": "start",
                    //"position": "left",
                    "boldPeriodBeginning": false,
                    "minPeriod": "DD",
                    "dateFormats": [
                        {period:'mm',format:'JJ:NN'},{period:'hh',format:'JJ:NN'},
                        {period:'DD',format:'DD-MM-YYYY'},
                        {period:'WW',format:'MMM DD'},{period:'MM',format:'DD-MM-YYYY'},{period:'YYYY',format:'YYYY'}
                    ],
                    "parseDates": true
                },
                "balloon": {
                    "borderThickness": 1,
                    "shadowAlpha": 0
                },
                "export": {
                    "enabled": true
                }
            });

        };




        var showStackedChart = function(data) {
            var chartData = _.cloneDeep($scope.chartData);

            _.forEach(chartData, function (d, index) {
                d.points = ChartService.remapDiffAndSort(d.points, d.uuid);
            });

            var kairosDataHistory = ChartService.mergePointAndSumNofill(chartData,false);

            _.forEach(kairosDataHistory, function (d, index) {
                d.date = AmCharts.formatDate(d.date, "DD-MM-YY");
            });

            var lastItem = _.cloneDeep(kairosDataHistory[kairosDataHistory.length-1]);

            kairosDataHistory.pop();

            var graphsHistory = [];
            _.forEach(chartData, function (d, index) {
                if (d.description !== 'sum') {
                    var g = {
                        "id": "g" + index,
                        title: d.description,
                        "fillAlphas": 0.6,
                        "type": "line",
                        "valueField": d.uuid,
                        "balloonText": "<div style='margin:3px; font-size:12px;'>[[title]] <b>[[value]]</b></div>"
                    }
                    graphsHistory.push(g);
                }
            });

            var kairosDataToday = [lastItem];
            kairosDataToday[0].sumamt = null;
            kairosDataToday[0].dashLengthColumn = 5;

            var graphsToday = [];
            _.forEach(chartData, function (d, index) {
                var g = {
                    "id": "g" + index,
                    title: d.description,
                    "fillAlphas": 0.6,
                    "type": "column",
                    "valueField": d.uuid,
                    "balloonText": "<div style='margin:3px; font-size:12px;'>[[title]] <b>[[value]]</b></div>",
                    "dashLengthField": "dashLengthColumn"
                }
                graphsToday.push(g);
            });

            var stackedChartToday = AmCharts.makeChart("chartDivHousingUnitToday", {
                "type": "serial",
                "theme": "light",
                "marginLeft": 80,
                "dataProvider": kairosDataToday,
                "valueAxes": [{
                    "stackType": "regular",
                    "position": "right",
                    "totalText": "[[total]]",
                    //"minimum": stackedChartHistory.valueAxes[0].min,
                    //"maximum": stackedChartHistory.valueAxes[0].max
                }],
                "startDuration": 0.5,
                "graphs": graphsToday,
                "chartCursor": {
                    "categoryBalloonDateFormat": "DD-MM-YYYY",
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0
                },
                "categoryField": "date",
                "categoryAxis": {
                    "startOnAxis": false,
                    "gridPosition": "start",
                    "centerLabelOnFullPeriod": true,
                    //"centerLabels": true,
                    "boldPeriodBeginning": false,
                    "parseDates": false,
                    "equalSpacing": false
                },
                "balloon": {
                    "borderThickness": 1,
                    "shadowAlpha": 0
                },
                "export": {
                    "enabled": true
                }
            });

            var suspendHovers = false;
            // legend events (hide/show)


            function replicateAction(chart, action, graph) {
                if (suspendHovers)
                    return;
                suspendHovers = true;
                var targetGraph = _.find(stackedChartToday.graphs, {valueField: graph.valueField});
                stackedChartToday[action](targetGraph);
                suspendHovers = false;
            }

            var initChart = function(event) {
                var chart = event.chart;
                if (chart.legend !== undefined) {
                    chart.legend.addListener("hideItem", function (event) {
                        replicateAction(chart, "hideGraph", event.dataItem);
                    });
                    chart.legend.addListener("showItem", function (event) {
                        replicateAction(chart, "showGraph", event.dataItem);
                    });

                    chart.valueAxes[0].addListener("axisChanged", function (event) {
                        if (suspendHovers)
                            return;
                        suspendHovers = true;
                        stackedChartToday.valueAxes[0].minimum = event.chart.valueAxes[0].min;
                        stackedChartToday.valueAxes[0].maximum = event.chart.valueAxes[0].max;
                        suspendHovers = false;
                    });
                }
            };

            var stackedChartHistory = AmCharts.makeChart("chartDivHousingUnitHistory", {
                "type": "serial",
                "theme": "light",
                "marginRight": 30,
                "dataProvider": kairosDataHistory,
                "legend": {
                    "divId": "legenddiv",
                    "equalWidths": true,
                    "position": "top",
                    "valueAlign": "left",
                    //"valueWidth": 100
                },
                "listeners": [{"event":"init", "method":initChart}],
                "valueAxes": [{
                    "stackType": "regular",
                    "position": "left"
                    //"totalText": "[[total]]"
                }],
                "startDuration": 0.2,
                "startEffect": "easeInSine",
                "graphs": graphsHistory,
                /*"chartScrollbar": {
                 "scrollbarHeight": 24,
                 "backgroundAlpha": 0,
                 "selectedBackgroundAlpha": 0.05,
                 "selectedBackgroundColor": "#888888",
                 "graphFillAlpha": 0,
                 "graphLineAlpha": 0.5,
                 "selectedGraphFillAlpha": 0,
                 "selectedGraphLineAlpha": 1,
                 "autoGridCount": true,
                 "color": "#AAAAAA"
                 },*/
                "chartCursor": {
                    "categoryBalloonDateFormat": "DD-MM-YYYY",
                    "cursorPosition": "mouse"
                },
                "categoryField": "date",
                "categoryAxis": {
                    //"gridPosition": "start",
                    //"position": "left",
                    "startOnAxis": true,
                    "equalSpacing": true,
                    "boldPeriodBeginning": false,
                    "parseDates": false
                },
                "balloon": {
                    "borderThickness": 1,
                    "shadowAlpha": 0
                },
                "export": {
                    "enabled": true
                }
            });



        };





        var showStackedChartOld = function(data) {
            var chartData = _.cloneDeep($scope.chartData);

            var kairosDataDetail;

            /*_.forEach(chartData, function (d, index) {
                d.points = ChartService.remapDiffAndSort(d.points, d.uuid);
            });*/

            /* calculating total sum of heat cost all. values for each sample */
            var allPoints = [];
            _.forEach(chartData, function (d, index) {
                d.points = ChartService.remapDiffAndSort(d.points, d.uuid);
                //allPoints = _.concat(allPoints, d.points);
                allPoints = allPoints.concat( d.points);
            });

            var allPointsByDate = _.groupBy(allPoints, "date"); // eg { date1: [{value: x}], date2: [{value: y}] }

//            var sumArray = _.toPairs(allPointsByDate); // [ [date1, [{value: x}], [date2, [{value: 5}] ]
            var sumArray = ChartService.toPairs(allPointsByDate); // [ [date1, [{value: x}], [date2, [{value: 5}] ]

            var sumPoints = _.map(sumArray, function(obj) {
//                var sum = _.sumBy(obj[1], function(elem) {
                var sum = ChartService.sumBy(obj[1], function(elem) {
                    return parseFloat(elem.value);
                });
                return {date: obj[1][0].date, label: "sumamt", "sumamt": sum.toFixed(2)};
            });

            var sumData = {"uuid": "sumamt", "description": "sum", "points": sumPoints};
            chartData.push(sumData);
            /* end calculating total sum of heat cost all. values for each sample */


            kairosDataDetail = ChartService.mergePointAndSumNofill(chartData,false);

            //var lastItem = _.cloneDeep(kairosDataDetail[kairosDataDetail.length-1]);

            _.forEach(kairosDataDetail, function (d, index) {
                d.date = AmCharts.formatDate(d.date, "DD-MM-YY");
            });

            var prelastItem = kairosDataDetail[kairosDataDetail.length - 2];
            prelastItem.dashLengthColumn = 5;
            prelastItem.alpha = 0.4;
            prelastItem.additional = " (in corso)";


            var graphsDetail = [];
            _.forEach(chartData, function (d, index) {
                if (d.description !== 'sum') {
                    var g = {
                        "id": "g" + index,
                        title: d.description,
                        "fillAlphas": 0.6,
                        "type": "line",
                        "valueField": d.uuid,
                        "balloonText": "<div style='margin:3px; font-size:12px;'>[[title]] <b>[[value]]</b></div>",
                        "dashLengthField": "dashLengthColumn"
                    }
                    graphsDetail.push(g);
                }
            });

            var graphSum = {
                "id": "gsum",
                "showBalloon": true,
                "title": "Totale",
                //"lineAlpha": 0,
                "fillAlphas": 0,
                "type": "line",
                "valueAxis": "total",
                "valueField": "sumamt",
                "labelText": "[[value]]",
                "fontSize": 13,
                "balloonText": "<div style='margin:3px; font-size:12px;'>[[title]] <b>[[value]]</b>[[additional]]</div>",
                //"alphaField": "alpha",
                "dashLengthField": "dashLengthColumn",
                "animationPlayed": true
            }
            graphsDetail.push(graphSum);

            var stacked = AmCharts.makeChart("chartDivHousingUnitClustered", {
                "type": "serial",
                "theme": "light",
                "marginRight": 80,
                "dataProvider": kairosDataDetail,
                "legend": {
                    "divId": "legenddiv",
                    "equalWidths": true,
                    "position": "top",
                    "valueAlign": "left"
                },
                "valueAxes": [{
                    "id": "main",
                    "stackType": "regular",
                    "position": "left",
                    "synchronizeWith": "total",
                    "synchronizationMultiplier": 1
                },{
                    "id": "total",
                    "position": "left",
                    "gridAlpha": 0,
                    "axisAlpha": 1,
                    "labelsEnabled": false,
                    "minimum": 0
                }],
                "graphs": graphsDetail,
                "chartScrollbar": {
                    "scrollbarHeight": 24,
                    "backgroundAlpha": 0,
                    "selectedBackgroundAlpha": 0.05,
                    "selectedBackgroundColor": "#888888",
                    "graphFillAlpha": 0,
                    "graphLineAlpha": 0.5,
                    "selectedGraphFillAlpha": 0,
                    "selectedGraphLineAlpha": 1,
                    "autoGridCount": true,
                    "color": "#AAAAAA"
                },
                "chartCursor": {
                    "categoryBalloonDateFormat": "DD-MM-YYYY",
                    "cursorPosition": "mouse"
                },
                "categoryField": "date",
                "categoryAxis": {
                    //"gridPosition": "start",
                    //"position": "left",
                    "startOnAxis": true,
                    "equalSpacing": true,
                    "minPeriod": "DD",
                    "dateFormats": [
                        {period:'mm',format:'JJ:NN'},{period:'hh',format:'JJ:NN'},
                        {period:'DD',format:'DD-MM-YYYY'},
                        {period:'WW',format:'MMM DD'},{period:'MM',format:'DD-MM-YYYY'},{period:'YYYY',format:'YYYY'}
                    ],
                    "parseDates": false
                },
                "balloon": {
                    "borderThickness": 1,
                    "shadowAlpha": 0
                },
                "export": {
                    "enabled": true
                }
            });

            stacked.addListener("init", function(event) {
                var chart = event.chart;
                chart.valueAxes[1].minimum = chart.valueAxes[0].min;
                chart.valueAxes[1].maximum = chart.valueAxes[0].max;
                chart.graphs[graphs.length - 1].labelText = "[[value]]";
                chart.graphs[graphs.length - 1].showBalloon = true;
            });

        };

        $scope.format="dd-MM-yyyy";
        $scope.maxDate = new Date();
        $scope.minDate = new Date($scope.maxDate.getFullYear()-1,
            $scope.maxDate.getMonth() , $scope.maxDate.getDate());
        $scope.dateOptions = {
            altInputFormats: ['d!-M!-yyyy'],
        }

        $scope.chartHousingUnit = {
            startDate: new Date()
        };
        $scope.popupFrom = { opened: false  };
        $scope.openFrom = function() { $scope.popupFrom.opened = true;  };

        $scope.$watch('chartHousingUnit.startDate',function(newValue, oldValue) {
            if(newValue) {
                $log.debug('value:'+newValue)
                heatCostsHousingUnitsChart(newValue);
            }
        })

        var heatCostsHousingUnitsChart = function(startDate){
            $scope.chartHousingUnit.loading = true
            HeatCostCalculatorService.getHeatCostsHousingUnitsChart($stateParams.id,startDate).then(function (data) {
                $scope.chartHousingUnit.loading = false
                var chartData = data.data[0].points;

                var graph = {
                    "id": "gSum",
                    title: 'sum',
                    "fillAlphas": 0.8,
                    "fillColors":"#DCDCDC",
                    "lineAlpha":"0.3",
                    "lineColor":"#FF0000",
                    "type": "column",
                    "valueField": 'sum',
                    "colorField":'color'
                    //"balloonText": "<div style='margin:5px; font-size:19px;'>Value:<b>[[value]]</b></div>"
                }

                var kairosData = ChartService.remapDiffAndSort(chartData);

                var chart3 = AmCharts.makeChart("chartDivHousingUnit", {
                    "type": "serial",
                    "theme": "black",
                    "marginRight": 8,
                    "dataProvider": kairosData,
                    "graphs": [graph],
                    "categoryField": "date",
                    "categoryAxis": {
                        "minPeriod": "DD",
                        "dateFormats": [
                            {period:'mm',format:'JJ:NN'},{period:'hh',format:'JJ:NN'},
                            {period:'DD',format:'DD-MM-YYYY'},
                            {period:'WW',format:'MMM DD'},{period:'MM',format:'MMM'},{period:'YYYY',format:'YYYY'}
                        ],
                        "parseDates": true
                    }
                });

            },function(data){
                $scope.chartHousingUnit.loading = false
                if (data.status=="404" && data.statusText=="Not Found") {
                    var chartData = [];
                } else {
                    UtilsService.showHttpError(data.data, toaster);
                }
            });
}


    }

]);


