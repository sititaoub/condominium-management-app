"use strict";

angular.module("condominiumManagementApp.housing_units")
	.config( function ($translatePartialLoaderProvider) {
		$translatePartialLoaderProvider.addPart("housing_units");
	});
