// Created by Damiele De Pasquale on 22/08/17
'use strict';
angular.module('condominiumManagementApp.configuration').controller('DistributionCriterioCtrl', ["$state", "$scope", "$rootScope", "NgTableParams", "DistributionCriterioResource", "SweetAlert", "DistributionCriterioHousing", "DistributionCriterioThousandthTables", "DistributionCriterioChartOfAccounts", "DistributionCriterionByMaterializedPath", "FinancialPeriodResource", "FinancialPeriodService", function ($state, $scope, $rootScope, NgTableParams, DistributionCriterioResource, SweetAlert, DistributionCriterioHousing, DistributionCriterioThousandthTables, DistributionCriterioChartOfAccounts, DistributionCriterionByMaterializedPath, FinancialPeriodResource, FinancialPeriodService) {
    $scope.loading = true; // flag Spinner show on loading data
    $scope.separatedDistribution = false; // flag (Checkbox) ripartizione Preventivo e Consuntivo
    $scope.criterioName = ""; // Criterio Description (nome)
    $scope.showButton = true; $scope.showButtonC = true; $scope.showButtonP = true; // bottoni inserisci elimina
    $scope.formData = {}; $scope.formDataC = {}; $scope.formDataP = {}; // dati form inserimento
    $scope.editingException = false; $scope.editingConsuntivo = false; $scope.editingPreventivo = false; // bottoni modifica
    $scope.updateException = false; $scope.updateConsuntivo = false; $scope.updatePreventivo = false; // bottoni aggiorna
    $scope.edit = {}; $scope.editC = {}; $scope.editP = {}; // abilitazione modifica dati
    $scope.dataHousingUnits = {}; // combo box selezione unità abitative
    $scope.dataThousandthTables = {}; // combo box selezione tabelle
    $scope.rowId = 0; // ID riga tabella
    $scope.adding = false; $scope.addingC = false; $scope.addingP = false; // abilitazione inserimento dati
    $scope.data = {}; // data from server
    $scope.NomeCriterio = false; // show/hide criterio e ripartizione (con relativi tab di scelta ripartizione)
    $scope.tabEccezioni = false; // show/hide tabella eccezioni
    $scope.contoChildren = []; // show/hide sottoconti

    init();

    // evento click su checbox ripartizione
    $scope.ripartizione = function (value) {
        $scope.data.separatedDistribution = value;
        if (!value) {
            $scope.tabPreventivo = false;
            $scope.tabConsuntivo = true;
        }
    };

    // mostra tabella in base al tab selezionato (se flag separatedDistribution è true)
    $scope.clickedTab = function (index) {
        if (index == 1) {
            $scope.tabPreventivo = true;
            $scope.tabConsuntivo = false;
        } else {
            $scope.tabPreventivo = false;
            $scope.tabConsuntivo = true;
        }
    };

    // inserisci un eccezione (mostra form inserimento dati)
    $scope.addException = function () {
        $scope.adding = true;
        $scope.showButton = false;
    };

    // inserisci un consuntivo (mostra form inserimento dati)
    $scope.addConsuntivo = function () {
        $scope.addingC = true;
        $scope.showButtonC = false;
    };

    // inserisci un preventivo (mostra form inserimento dati)
    $scope.addPreventivo = function () {
        $scope.addingP = true;
        $scope.showButtonP = false;
    };

    // salva inserimento eccezione
    $scope.saveException = function () {
        $scope.formData.controlli = "E";
        if (controlli($scope.formData)) { return; }
        var data = { "housingUnitId": $scope.formData.housingUnitId, "thousandthTableId": $scope.formData.thousandthTableId, "ownerPercent": $scope.formData.ownerPercent, "tenantPercent": $scope.formData.tenantPercent, "usufructPercent": $scope.formData.usufructPercent, "othersPercent": $scope.formData.othersPercent, "id": getId($scope.tableParamsE.data) };
        angular.forEach($scope.dataHousingUnits, function (value) { if (value.id == data.housingUnitId) { data.housingUnitDescription = value.description; } });
        $scope.data.distributionCriterionExceptions.push(data);
        aggiorna($scope.tableParamsE, $scope.data);
        // riabilita editing
        $scope.adding = false;
        $scope.showButton = true;
        $scope.formData = {};
        // ripristino bottone modifica
        $scope.editingException = false;
    };

    // salva inserimento consuntivo
    $scope.saveConsuntivo = function () {
        $scope.formDataC.controlli = "C";
        if (controlli($scope.formDataC)) { return; }
        var data = { "coefficent": 100, "thousandthTableId": $scope.formDataC.thousandthTableId, "ownerPercent": $scope.formDataC.ownerPercent, "tenantPercent": $scope.formDataC.tenantPercent, "usufructPercent": $scope.formDataC.usufructPercent, "othersPercent": $scope.formDataC.othersPercent, "id": getId($scope.tableParamsC.data) };
        $scope.data.distributionCriterionDetailFinal.push(data);
        aggiorna($scope.tableParamsC, $scope.data);
        $scope.addingC = false;
        $scope.showButtonC = true;
        $scope.formDataC = {};
        $scope.editingConsuntivo = false;
    };

    // salva inserimento preventivo
    $scope.savePreventivo = function () {
        $scope.formDataP.controlli = "P";
        if (controlli($scope.formDataP)) { return; }
        var data = { "coefficent": 100, "thousandthTableId": $scope.formDataP.thousandthTableId, "ownerPercent": $scope.formDataP.ownerPercent, "tenantPercent": $scope.formDataP.tenantPercent, "usufructPercent": $scope.formDataP.usufructPercent, "othersPercent": $scope.formDataP.othersPercent, "id": getId($scope.tableParamsP.data) };
        $scope.data.distributionCriterionDetailQuote.push(data);
        aggiorna($scope.tableParamsP, $scope.data);
        $scope.addingP = false;
        $scope.showButtonP = true;
        $scope.formDataP = {};
        $scope.editingPreventivo = false;
    };

    // annulla inserimento eccezione
    $scope.cancelAdding = function () {
        $scope.adding = false;
        $scope.showButton = true;
        $scope.formData = {};
        $scope.editingException = false;
    };

    // annulla inserimento consuntivo
    $scope.cancelAddingC = function () {
        $scope.addingC = false;
        $scope.showButtonC = true;
        $scope.formDataC = {};
        $scope.editingConsuntivo = false;
    };

    // annulla inserimento preventivo
    $scope.cancelAddingP = function () {
        $scope.addingP = false;
        $scope.showButtonP = true;
        $scope.formDataP = {};
        $scope.editingPreventivo = false;
    };

    // elimina eccezione
    $scope.deleteException = function () {
        var noElementSelected = true;
        $scope.selectedCheckBox = document.getElementsByClassName("checkBoxException");
        var arrayOfDelete = [];
        var id = 0;
        angular.forEach($scope.selectedCheckBox, function (value) {
            if (value.checked == true) {
                noElementSelected = false;
                id = parseInt(value.id);
                arrayOfDelete.push(id);
            }
        });
        if (noElementSelected) { alertWarning("Nessun elemento selezionato!", "Nessun elemento da eliminare selezionato, per favore seleziona almeno un elemento."); }
        else { deleteExceptionItem(arrayOfDelete); }
    };

    // elimina consuntivo
    $scope.deleteConsuntivo = function () {
        var noElementSelected = true;
        $scope.selectedCheckBoxC = document.getElementsByClassName("checkBoxConsuntivo");
        var arrayOfDelete = [];
        var id = 0;
        angular.forEach($scope.selectedCheckBoxC, function (value) {
            if (value.checked == true) {
                noElementSelected = false;
                id = parseInt(value.id);
                arrayOfDelete.push(id);
            }
        });
        if (noElementSelected) { alertWarning("Nessun elemento selezionato!", "Nessun elemento da eliminare selezionato, per favore seleziona almeno un elemento"); }
        else { deleteConsuntivoItem(arrayOfDelete); }
    };

    // elimina preventivo
    $scope.deletePreventivo = function () {
        var noElementSelected = true;
        $scope.selectedCheckBoxP = document.getElementsByClassName("checkBoxPreventivo");
        var arrayOfDelete = [];
        var id = 0;
        angular.forEach($scope.selectedCheckBoxP, function (value) {
            if (value.checked == true) {
                noElementSelected = false;
                id = parseInt(value.id);
                arrayOfDelete.push(id);
            }
        });
        if (noElementSelected) { alertWarning("Nessun elemento selezionato!", "Nessun elemento da eliminare selezionato, per favore seleziona almeno un elemento"); }
        else { deletePreventivoItem(arrayOfDelete); }
    };

    // seleziona tutte le eccezioni
    $scope.selectAllExceptions = function () { selectAll("checkBoxAllExceptions", "checkBoxException"); };

    // seleziona tutti i consuntivi
    $scope.selectAllConsuntivi = function () { selectAll("checkBoxAllConsuntivi", "checkBoxConsuntivo"); };

    // seleziona tutti i preventivi
    $scope.selectAllPreventivi = function () { selectAll("checkBoxAllPreventivi", "checkBoxPreventivo"); };

    // seleziona una eccezione
    $scope.selectAnException = function () { controlliCheckBox("checkBoxAllExceptions", "checkBoxException", "E"); };

    // seleziona un consuntivo
    $scope.selectAConsuntivo = function () { controlliCheckBox("checkBoxAllConsuntivi", "checkBoxConsuntivo", "C"); };

    // seleziona un preventivo
    $scope.selectAPreventivo = function () { controlliCheckBox("checkBoxAllPreventivi", "checkBoxPreventivo", "P"); };

    // modifica eccezioni
    $scope.editException = function () {
        $scope.selectedCheckBox = document.getElementsByClassName("checkBoxException");
        var isSelectedCheckBox = false;
        var id = 0;
        angular.forEach($scope.selectedCheckBox, function (value) {
            if (value.checked == true) {
                value.disabled = true;
                isSelectedCheckBox = true;
                $scope.editingException = false;
                $scope.updateException = true;
                id = parseInt(value.id);
                editExceptionItem(id);
            }
        });
        if (isSelectedCheckBox == false) { alertWarning("Nessun elemento selezionato!", "Nessun elemento da modificare selezionato, per favore seleziona un elemento"); }
    };

    // modifica consuntivi
    $scope.editConsuntivo = function () {
        $scope.selectedCheckBoxC = document.getElementsByClassName("checkBoxConsuntivo");
        var isSelectedCheckBox = false;
        var id = 0;
        angular.forEach($scope.selectedCheckBoxC, function (value) {
            if (value.checked == true) {
                value.disabled = true;
                isSelectedCheckBox = true;
                $scope.editingConsuntivo = false;
                $scope.updateConsuntivo = true;
                id = parseInt(value.id);
                editConsuntivoItem(id);
            }
        });
        if (isSelectedCheckBox == false) { alertWarning("Nessun elemento selezionato!", "Nessun elemento da modificare selezionato, per favore seleziona un elemento"); }
    };

    // modifica preventivi
    $scope.editPreventivo = function () {
        $scope.selectedCheckBoxP = document.getElementsByClassName("checkBoxPreventivo");
        var isSelectedCheckBox = false;
        var id = 0;
        angular.forEach($scope.selectedCheckBoxP, function (value) {
            if (value.checked == true) {
                value.disabled = true;
                isSelectedCheckBox = true;
                $scope.editingPreventivo = false;
                $scope.updatePreventivo = true;
                id = parseInt(value.id);
                editPreventivoItem(id);
            }
        });
        if (isSelectedCheckBox == false) { alertWarning("Nessun elemento selezionato!", "Nessun elemento da modificare selezionato, per favore seleziona un elemento"); }
    };

    // aggiorna eccezione
    $scope.updateExceptionItem = function () {
        var i = 0;
        // array id elementi in 'EDIT MODE'
        var arrayId = Object.keys($scope.edit);
        var length = $scope.data.distributionCriterionExceptions.length;
        var errore = false;
        for (i = 0; i < length; i++) {
            angular.forEach(arrayId, function (value) {
                value = parseInt(value);
                if (!errore) {
                    if (value == $scope.data.distributionCriterionExceptions[i].id) {
                        $scope.data.distributionCriterionExceptions[i].controlli = "E";
                        errore = controlli($scope.data.distributionCriterionExceptions[i]);
                        if (!errore) {
                            angular.forEach($scope.dataHousingUnits, function (value) { if ($scope.data.distributionCriterionExceptions[i].housingUnitId == value.id) { $scope.data.distributionCriterionExceptions[i].housingUnitDescription = value.description; } });
                            angular.forEach($scope.dataThousandthTables, function (value) { if ($scope.data.distributionCriterionExceptions[i].thousandthTableDescription == value.description) { $scope.data.distributionCriterionExceptions[i].thousandthTableId = value.id; } });
                        }
                    }
                }
            });
            if (errore) { break; }
        }
        if (errore) { return; }
        // ripristino
        $scope.updateException = false;
        $scope.edit = {};
        angular.forEach($scope.selectedCheckBox, function (value) {
            if (value.checked == true) {
                value.checked = false;
                value.disabled = false;
            }
        });
        // ripristino select all (master check)
        $scope.selectAllCheckBox = document.getElementsByClassName("checkBoxAllExceptions");
        $scope.selectAllCheckBox["0"].indeterminate = false;
        aggiorna($scope.tableParamsE, $scope.data);
    };

    // aggiorna consuntivo
    $scope.updateConsuntivoItem = function () {
        var i = 0;
        var arrayId = Object.keys($scope.editC);
        var length = $scope.data.distributionCriterionDetailFinal.length;
        var errore = false;
        for (i = 0; i < length; i++) {
            angular.forEach(arrayId, function (value) {
                value = parseInt(value);
                if (!errore) {
                    if (value == $scope.data.distributionCriterionDetailFinal[i].id) {
                        $scope.data.distributionCriterionDetailFinal[i].controlli = "C";
                        errore = controlli($scope.data.distributionCriterionDetailFinal[i]);
                        if (!errore) {
                            angular.forEach($scope.dataThousandthTables, function (value) {
                                if ($scope.data.distributionCriterionDetailFinal[i].thousandthTableId == value.id) {
                                    $scope.data.distributionCriterionDetailFinal[i].thousandthTableDescription = value.description;
                                    $scope.data.distributionCriterionDetailFinal[i].thousandthType = value.thousandthType;
                                }
                            });
                        }
                    }
                }
            });
            if (errore) { break; }
        }
        if (errore) { return; }
        $scope.updateConsuntivo = false;
        $scope.editC = {};
        $scope.selectAllCheckBoxC = document.getElementsByClassName("checkBoxAllConsuntivi");
        $scope.selectAllCheckBoxC["0"].indeterminate = false;
        angular.forEach($scope.selectedCheckBox, function (value) {
            if (value.checked == true) {
                value.checked = false;
                value.disabled = false;
            }
        });
        aggiorna($scope.tableParamsC, $scope.data);
    };

    // aggiorna preventivo
    $scope.updatePreventivoItem = function () {
        var i = 0;
        var arrayId = Object.keys($scope.editP);
        var length = $scope.data.distributionCriterionDetailQuote.length;
        var errore = false;
        for (i = 0; i < length; i++) {
            angular.forEach(arrayId, function (value) {
                value = parseInt(value);
                if (!errore) {
                    if (value == $scope.data.distributionCriterionDetailQuote[i].id) {
                        $scope.data.distributionCriterionDetailQuote[i].controlli = "P";
                        errore = controlli($scope.data.distributionCriterionDetailQuote[i]);
                        if (!errore) {
                            angular.forEach($scope.dataThousandthTables, function (value) {
                                if ($scope.data.distributionCriterionDetailQuote[i].thousandthTableId == value.id) {
                                    $scope.data.distributionCriterionDetailQuote[i].thousandthType = value.thousandthType;
                                    $scope.data.distributionCriterionDetailQuote[i].thousandthTableDescription = value.description;
                                }
                            });
                        }
                    }
                }
            });
            if (errore) { break; }
        }
        if (errore) { return; }
        $scope.updatePreventivo = false;
        $scope.editP = {};
        $scope.selectAllCheckBoxP = document.getElementsByClassName("checkBoxAllPreventivi");
        $scope.selectAllCheckBoxP["0"].indeterminate = false;
        angular.forEach($scope.selectedCheckBox, function (value) {
            if (value.checked == true) {
                value.checked = false;
                value.disabled = false;
            }
        });
        aggiorna($scope.tableParamsP, $scope.data);
    };

    // click riga conto (show/hide sottoconti)
    $scope.clickConto = function (conto) { angular.forEach(conto.children, function (value) { $scope.contoChildren[value.id] = !($scope.contoChildren[value.id]); }); };

    // click riga sottoconto
    $scope.clickSottoconto = function (materializedPath, id) {
        $scope.NomeCriterio = true;
        $scope.tabEccezioni = true;
        $scope.data.materializedPath = materializedPath;
        $scope.loading = true;
        DistributionCriterionByMaterializedPath.get({ materializedPath: $scope.data.materializedPath, financialPeriodId: $scope.financialPeriodId }).$promise.then(function (data) {
            $scope.loading = false;
            load(data, id);
        }, function () { $scope.loading = false; });
    };
    /*************************************************************************************************************************/
    function init() {
        $scope.condominiumId = $state.params.condominiumId;
        $scope.condominiumId = parseInt($scope.condominiumId);
        $scope.loading = true;
        // chiamata periodo finanziario corrente
        var result = FinancialPeriodService.getCurrent(undefined, $scope.condominiumId);
        $scope.financialPeriodId = result.id;
        // chiamata dati conti sottoconti
        getChartOfAccounts($scope.condominiumId, $scope.financialPeriodId);
        // chiamata dati unità abitative
        DistributionCriterioHousing.query({ id: $scope.condominiumId }).$promise.then(function (responseData) { $scope.dataHousingUnits = responseData; });
        // chiamata dati tabelle ripartizione
        DistributionCriterioThousandthTables.get({ condominiumId: $scope.condominiumId }).$promise.then(function (responseData) {
            $scope.loading = false;
            $scope.dataThousandthTables = responseData.content;
        }, function () { $scope.loading = false; });
    }

    // cancellazione eccezione
    function deleteExceptionItem(idArray) {
        SweetAlert.swal({
            title: "Vuoi davvero eliminare gli elementi selezionati?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: true,
            animation: "slide-from-top"
        }, function (isConfirm) {
            if (isConfirm) {
                // eliminazione autorizzata
                angular.forEach(idArray, function (value, key) { idArray[key] = parseInt(value); });
                var i = 0;
                var length = idArray.length;
                for (i = 0; i < length; i++) {
                    angular.forEach($scope.tableParamsE.data, function (value, key) {
                        if (idArray[i] == value.id) {
                            $scope.tableParamsE.data.splice(key, 1);
                            $scope.data.distributionCriterionExceptions.splice(key, 1);
                        }
                    });
                }
                aggiorna($scope.tableParamsE, $scope.data);
                // ripristino select all
                if ($scope.selectAllCheckBox) {
                    $scope.selectAllCheckBox["0"].indeterminate = false;
                    $scope.selectAllCheckBox["0"].checked = false;
                }
                // ripristino bottone modifica
                $scope.editingException = false;
            }
        });
    }

    // cancellazione consuntivo
    function deleteConsuntivoItem(idArray) {
        SweetAlert.swal({
            title: "Vuoi davvero eliminare gli elementi selezionati?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: true,
            animation: "slide-from-top"
        }, function (isConfirm) {
            if (isConfirm) {
                angular.forEach(idArray, function (value, key) { idArray[key] = parseInt(value); });
                var i = 0;
                var length = idArray.length;
                for (i = 0; i < length; i++) {
                    angular.forEach($scope.tableParamsC.data, function (value, key) {
                        if (idArray[i] == value.id) {
                            $scope.tableParamsC.data.splice(key, 1);
                            $scope.data.distributionCriterionDetailFinal.splice(key, 1);
                        }
                    });
                }
                aggiorna($scope.tableParamsC, $scope.data);
                if ($scope.selectAllCheckBoxC) {
                    $scope.selectAllCheckBox["0"].indeterminate = false;
                    $scope.selectAllCheckBoxC["0"].checked = false;
                }
                $scope.editingConsuntivo = false;
            }
        });
    }

    // cancellazione preventivo
    function deletePreventivoItem(idArray) {
        SweetAlert.swal({
            title: "Vuoi davvero eliminare gli elementi selezionati?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: true,
            animation: "slide-from-top"
        }, function (isConfirm) {
            if (isConfirm) {
                angular.forEach(idArray, function (value, key) { idArray[key] = parseInt(value); });
                var i = 0;
                var length = idArray.length;
                for (i = 0; i < length; i++) {
                    angular.forEach($scope.tableParamsP.data, function (value, key) {
                        if (idArray[i] == value.id) {
                            $scope.tableParamsP.data.splice(key, 1);
                            $scope.data.distributionCriterionDetailQuote.splice(key, 1);
                        }
                    });
                }
                aggiorna($scope.tableParamsP, $scope.data);
                if ($scope.selectAllCheckBoxP) {
                    $scope.selectAllCheckBox["0"].indeterminate = false;
                    $scope.selectAllCheckBoxP["0"].checked = false;
                }
                $scope.editingPreventivo = false;
            }
        });
    }

    // aggiornamento eccezione
    function editExceptionItem(id) { enableEdit(id, $scope.tableParamsE.data, "E"); }

    // aggiornamento consuntivo
    function editConsuntivoItem(id) { enableEdit(id, $scope.tableParamsC.data, "C"); }

    // aggiornamento preventivo
    function editPreventivoItem(id) { enableEdit(id, $scope.tableParamsP.data, "P"); }

    // abilita modifica dati
    function enableEdit(id, tableParams, edit) {
        angular.forEach(tableParams, function (value) {
            if (edit == "P") { if (value.id == id) { $scope.editP[id] = true; } }
            if (edit == "C") { if (value.id == id) { $scope.editC[id] = true; } }
            if (edit == "E") { if (value.id == id) { $scope.edit[id] = true; } }
        });
    }

    // ricarica dati (click su sottoconto dopo init)
    function reload(data) {
        $scope.data = data;
        $scope.tableParamsC.data = data.distributionCriterionDetailFinal;
        $scope.data.distributionCriterionDetailFinal = data.distributionCriterionDetailFinal;
        $scope.tableParamsE.data = data.distributionCriterionExceptions;
        $scope.data.distributionCriterionExceptions = data.distributionCriterionExceptions;
        $scope.tableParamsP.data = data.distributionCriterionDetailQuote;
        $scope.data.distributionCriterionDetailQuote = data.distributionCriterionDetailQuote;
        // recupero financialPeriodId relativo a questo condominio
        $scope.loading = true;
        // chiamata dati in base al criterio selezionato (valori in base a materialized path)
        DistributionCriterionByMaterializedPath.get({ materializedPath: $scope.data.materializedPath, financialPeriodId: $scope.financialPeriodId }).$promise.then(function (data) {
            $scope.loading = false;
            load(data);
        }, function () { $scope.loading = false; });
    }

    // controlli inserimento dati in tabelle
    function controlli(formData) {
        var errore = false;
        if (formData.thousandthTableId == "" || formData.thousandthTableId == undefined) {
            alertWarning("Campo Tabella richiesto!", "Il campo Tabella è un campo obbligatorio");
            errore = true;
            return errore;
        }
        if (formData.controlli == "E") {
            if (formData.housingUnitId == "" || formData.housingUnitId == undefined) {
                alertWarning("Campo Unità richiesto!", "Il campo Unità è un campo obbligatorio");
                errore = true;
                return errore;
            }
        }
        if (formData.othersPercent < 0) {
            alertWarning("La Percentuale Altri deve essere positiva!", "La percentuale di Altri non può essere negativa, per favore inserisci una percentuale positiva");
            errore = true;
            return errore;
        }
        if (formData.ownerPercent < 0) {
            alertWarning("La Percentuale Proprietario deve essere positiva!", "La percentuale del Proprietario non può essere negativa, per favore inserisci una percentuale positiva");
            errore = true;
            return errore;
        }
        if (formData.tenantPercent < 0) {
            alertWarning("La Percentuale Coinquilino deve essere positiva!", "La percentuale del Coinquilino non può essere negativa, per favore inserisci una percentuale positiva");
            errore = true;
            return errore;
        }
        if (formData.usufructPercent < 0) {
            alertWarning("La Percentuale Usufruttuario deve essere positiva!", "La percentuale dell' Usufruttuario non può essere negativa, per favore inserisci una percentuale positiva");
            errore = true;
            return errore;
        }
        if (formData.othersPercent == undefined) { formData.othersPercent = 0; }
        else { formData.othersPercent = formData.othersPercent; }
        if (formData.ownerPercent == undefined) { formData.ownerPercent = 0; }
        else { formData.ownerPercent = formData.ownerPercent; }
        if (formData.tenantPercent == undefined) { formData.tenantPercent = 0; }
        else { formData.tenantPercent = formData.tenantPercent; }
        if (formData.usufructPercent == undefined) { formData.usufructPercent = 0; }
        else { formData.usufructPercent = formData.usufructPercent; }
        var sumAllPercent = formData.othersPercent + formData.ownerPercent + formData.tenantPercent + formData.usufructPercent;
        if (sumAllPercent != 100) {
            alertWarning("Somma percentuali errata!", "Somma percentuali differente da 100, controlla per favore i dati inseriti");
            errore = true;
            return errore;
        }
        return errore;
    }

    // trova max ID in tabella e assegna a rowId
    function getId(tableParams) {
        $scope.rowId = 0;
        angular.forEach(tableParams, function (value) { if ($scope.rowId <= value.id) { $scope.rowId = value.id; } });
        $scope.rowId++;
        return $scope.rowId;
    }

    // chiamata aggiornamento dati
    function aggiorna(tableParams, data) {
        data.managementId = 9;
        $scope.data.managementId = 9;
        $scope.loading = true;
        DistributionCriterioResource.update(data).$promise.then(function (data) {
            tableParams.reload();
            DistributionCriterionByMaterializedPath.get({ materializedPath: data.materializedPath, financialPeriodId: $scope.financialPeriodId }).$promise.then(function (data) {
                $scope.loading = false;
                reload(data.content["0"]);
            }, function () { $scope.loading = false; });
        }, function () { $scope.loading = false; });
    }

    // seleziona tutti i checkbox della tabella
    function selectAll(selectAllClass, selectedClass) {
        $scope.selectAllCheckBox = document.getElementsByClassName(selectAllClass);
        $scope.selectedCheckBox = document.getElementsByClassName(selectedClass);
        angular.forEach($scope.selectedCheckBox, function (value) { value.checked = $scope.selectAllCheckBox["0"].checked; });
    }

    // load init
    function load(data, id) {
        $scope.data = data.content["0"];
        $scope.criterioName = data.content["0"].description;
        $scope.separatedDistribution = data.content["0"].separatedDistribution;
        $scope.loading = true;
        changeCriterionState($scope.dataChartOfAccounts, id);
        // chiamata dati tabelle ripartizione
        DistributionCriterioThousandthTables.get({ condominiumId: $scope.condominiumId }).$promise.then(function (responseData) {
            $scope.loading = false;
            $scope.dataThousandthTables = responseData.content;
        }, function () { $scope.loading = false; });
        if ($scope.separatedDistribution) {
            $scope.tabConsuntivo = false;
            $scope.tabPreventivo = true;
        } else {
            $scope.tabConsuntivo = true;
            $scope.tabPreventivo = false;
        }
        // tabella Preventivo
        $scope.tableParamsP = new NgTableParams({ count: 10 }, { counts: [], dataset: data.content["0"].distributionCriterionDetailQuote });
        // tabella Consuntivo
        $scope.tableParamsC = new NgTableParams({ count: 10 }, { counts: [], dataset: data.content["0"].distributionCriterionDetailFinal });
        // tabella Eccezioni
        $scope.tableParamsE = new NgTableParams({ count: 10 }, { counts: [], dataset: data.content["0"].distributionCriterionExceptions });
    }

    // alert di warning
    function alertWarning(title, text) { SweetAlert.swal({ title: title, text: text, type: "warning", showCancelButton: false, confirmButtonColor: "#DD6B55", confirmButtonText: "Ok", closeOnConfirm: true, animation: "slide-from-top" }); }

    // gestione checkbox
    function controlliCheckBox(checkBoxAll, checkBox, editing) {
        $scope.selectAllCheckBox = document.getElementsByClassName(checkBoxAll);
        $scope.selectedCheckBox = document.getElementsByClassName(checkBox);
        $scope.checkBoxLength = $scope.selectedCheckBox.length;
        var i = 0;
        var length = $scope.checkBoxLength;
        var checkedCount = 0;
        for (i = 0; i < length; i++) {
            // se elemento corrente selezionato
            if ($scope.selectedCheckBox[i].checked == true) {
                $scope.selectAllCheckBox["0"].indeterminate = true;
                checkedCount++;
                if (editing == "P") { $scope.editingPreventivo = true; }
                if (editing == "C") { $scope.editingConsuntivo = true; }
                if (editing == "E") { $scope.editingException = true; }
            }
            // se tutti selezionati
            if (checkedCount == length) {
                $scope.selectAllCheckBox["0"].indeterminate = false;
                $scope.selectAllCheckBox["0"].checked = true;
            }
            // se nessuno selezionato
            if (checkedCount == 0) {
                $scope.selectAllCheckBox["0"].indeterminate = false;
                $scope.selectAllCheckBox["0"].checked = false;
                if (editing == "P") { $scope.editingPreventivo = false; }
                if (editing == "C") { $scope.editingConsuntivo = false; }
                if (editing == "E") { $scope.editingException = false; }
            }
        }
    }

    // chiamata associazione conti sottoconti
    function getChartOfAccounts(condominiumId, financialPeriodId) {
        $scope.loading = true;
        DistributionCriterioChartOfAccounts.get({ condominiumId: condominiumId, financialPeriodId: financialPeriodId }).$promise.then(function (responseData) {
            $scope.loading = false;
            $scope.dataChartOfAccounts = responseData;
            $scope.tableMasterDetailChartOfAccounts = new NgTableParams({ count: 10 }, { counts: [], dataset: $scope.dataChartOfAccounts.children });
        }, function () { $scope.loading = false; });
    }

    // cambia hasDistributionCriterion dei conti e sottoconti
    function changeCriterionState(data, id) {
        var n = 0; // numero sottoconti
        var hasDistributionIs2 = 0; // contatore di quanti hasDistributionCriterion sono uguali a 2
        var changeCriterionStateContoTo = 0; // cambia hasDistributionCriterion del conto a (valore variabile, che può essere 0,1 o 2)
        angular.forEach(data.children, function (value) {
            hasDistributionIs2 = 0;
            n = value.children.length;
            angular.forEach(value.children, function (value) {
                if (value.id == id) { value.hasDistributionCriterion = 2; }
                if (value.hasDistributionCriterion == 2) { hasDistributionIs2++; }
                if (hasDistributionIs2 == n) { changeCriterionStateContoTo = 2; } // tutti sono uguali a 2
                else if (hasDistributionIs2 == 0) { changeCriterionStateContoTo = 0; } // nessuno è uguale a 2
                else { changeCriterionStateContoTo = 1; } // qualcuno è uguale a 2
            });
            value.hasDistributionCriterion = changeCriterionStateContoTo;
        });
    }
}]);