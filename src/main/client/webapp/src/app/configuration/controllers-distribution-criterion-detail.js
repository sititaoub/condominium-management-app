/**
 * Created by Antonio Saracino on 26/06/17.
 */

'use strict';

angular.module('condominiumManagementApp.configuration')
    .controller('DistributionCriterionDetailCtrl', [
        '$scope',
        '$filter',
        '$state',
        'NgTableParams',
        'ngTableEventsChannel',
        'DistributionCriterionService',
        'UtilsService',
        'toaster',
        function ($scope, $filter, $state, NgTableParams, ngTableEventsChannel, DistributionCriterionService, UtilsService, toaster) {

            $scope.loading = true;
            $scope.individualNameFilter = {
                firstName: {
                    id: "text",
                    placeholder: $filter('translate')('distribution-criterion-detail.table.firstName.label')
                },
                lastName: {
                    id: "text",
                    placeholder: $filter('translate')('distribution-criterion-detail.table.lastName.label')
                }
            };

            $scope.energyTypes = [
                {
                    value: 'VOLUNTARY_INVOLUNTARY',
                    label: $filter('translate')('distribution-criterion-detail.energy-distribution.voluntary_involontary.radio'),
                    group: $filter('translate')('distribution-criterion-detail.heating.label')
                },
                {
                    value: 'VOLUNTARY',
                    label: $filter('translate')('distribution-criterion-detail.energy-distribution.voluntary.radio'),
                    group: $filter('translate')('distribution-criterion-detail.heating.label')
                },
                {
                    value: 'INVOLUNTARY',
                    label: $filter('translate')('distribution-criterion-detail.energy-distribution.involuntary.radio'),
                    group: $filter('translate')('distribution-criterion-detail.heating.label')
                },
                {
                    value: 'CONSUMPTION',
                    label: $filter('translate')('distribution-criterion-detail.energy-distribution.consumption.radio'),
                    group: $filter('translate')('distribution-criterion-detail.water.label')
                }
            ];

            init();

            $scope.updatedTableType = function () {
                $scope.tableParams.settings().dataset = transformData($scope.distribution);
                ngTableEventsChannel.publishDatasetChanged($scope.tableParams, $scope.tableParams.settings().dataset);
            };

            $scope.groupFind = function(item) {
                return item.group;
            };

            $scope.save = function () {

                $scope.saving = true;

                if (isNewObject()) {
                    create($scope.distribution);
                } else {
                    update($scope.distribution);
                }

            };

            $scope.isNew = isNewObject;

            $scope.calcolateTotal = function() {
                updateTotal($scope.tableParams.settings().dataset);
            };

            $scope.back = function () {
                window.history.back();
            };

            /********************************************/

            function create(distribution) {
                DistributionCriterionService.resource.save(distribution).$promise.then(function (data) {
                    $scope.saving = false;
                    $state.go('index.distribution-criterion-detail', {id : data.id});
                    toaster.pop({
                        type: 'success',
                        title: 'Salvataggio effettuato',
                        showCloseButton: true,
                        timeout: 2000
                    });
                }, function (data) {
                    $scope.saving = false;
                    UtilsService.showHttpError(data.data, toaster);
                });
            }

            function update(distribution) {
                DistributionCriterionService.resource.update(distribution).$promise.then(function (data) {
                    $scope.saving = false;
                    toaster.pop({
                        type: 'success',
                        title: 'Salvataggio effettuato',
                        showCloseButton: true,
                        timeout: 2000
                    });
                }, function (data) {
                    $scope.saving = false;
                    UtilsService.showHttpError(data.data, toaster);
                });
            }

            function updateTotal(data) {
                if (!data) {
                    return;
                }

                var total = 0;

                angular.forEach(data, function(item) {
                    total += item.coefficient;
                });

                $scope.total = total;
            }

            /**
             * converte la struttura annidata in ingresso in lista piatta in modo
             * da ottenere una rappresentazione raggruppabile dei dati (ng-table.com)
             *
             * La lista che viene generata dipende dal tipo di tabella che si sta gestendo. Se è di tipo
             * INDIVIDUAL genererà una lista di individui, in caso contrario una lista di unità abitative
             */
            function transformData(data) {

                var result = [];

                if (!data) {
                    return result;
                }

                angular.forEach(data.groups, function (group) {

                    angular.forEach(group.housingUnits, function (housing) {
                        housing.group = group.description;

                        if (data.thousandthType === DistributionCriterionService.tableTypeEnum.INDIVIDUAL) {
                            // lista di individui
                            angular.forEach(housing.roles, function (role) {
                                role.group = group.description;
                                role.housingUnitDescription = housing.description;
                                result.push(role);
                            });

                        } else {
                            // lista di unità abitative
                            housing.housingUnitDescription = housing.description;
                            result.push(housing);
                        }
                    });

                });

                return result;
            }

            /**
             * listener che aggiorna il filtro sulle colonne delle unità abitative
             */
            function handlerTableFilter(table, newData) {

                if ($scope.distribution.thousandthType !== DistributionCriterionService.tableTypeEnum.INDIVIDUAL &&
                    $scope.distribution.thousandthType !== DistributionCriterionService.tableTypeEnum.THOUSANDTH) {
                    return;
                }

                var uniqueObject = _.uniq(newData, 'housingUnitDescription');
                $scope.housingUnitDataSelect = uniqueObject.map(function (item) {
                    return {id: item.housingUnitDescription, title: item.housingUnitDescription};
                });
            }

            function handlerTotalCalculator(table, newData) {
                updateTotal(newData);
            }

            function createTableTypeSelect() {

                var list = [];

                angular.forEach(Object.keys(DistributionCriterionService.tableTypeEnum), function (type, index) {
                    list.push({id: index, value: type, label: $filter('translate')(type)});
                });

                return list;
            }

            function isNewObject() {
                return $state.params.id === 'new';
            }


            function init() {
                ngTableEventsChannel.onDatasetChanged(handlerTableFilter, $scope);
                ngTableEventsChannel.onDatasetChanged(handlerTotalCalculator, $scope);

                $scope.tableTypeSelect = createTableTypeSelect();
                $scope.condominiumId = $state.params.condominiumId;

                var promise;

                if (isNewObject()) {
                    // creo un nuovo oggetto
                    promise = DistributionCriterionService.resourceCreation.new({condominiumId: $scope.condominiumId}).$promise;
                } else {
                    promise = DistributionCriterionService.resource.get({id: $state.params.id}).$promise;
                }

                promise.then(function (data) {
                    $scope.loading = false;
                    $scope.distribution = data;


                    $scope.tableParams = new NgTableParams(
                        {
                            count: 10,
                            sorting: { housingUnitDescription: "asc" }
                        },
                        {
                            dataset: transformData(data),
                            filterOptions: {filterLayout: "horizontal"}
                        });


                }, function (data) {
                    $scope.loading = false;
                    UtilsService.showHttpError(data.data, toaster);
                });
            }

        }]);