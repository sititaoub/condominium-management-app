"use strict";

angular.module("condominiumManagementApp.configuration", [
    'condominiumManagementApp.core',
    'condominiumManagementApp.commons'
]);
