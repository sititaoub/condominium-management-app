"use strict";

angular.module("condominiumManagementApp.configuration")
	.config( function ($translatePartialLoaderProvider) {
		$translatePartialLoaderProvider.addPart("configuration");
	});
