/**
 * Created by Antonio Saracino on 26/06/17.
 */

'use strict';

angular.module('condominiumManagementApp.configuration')
    .controller('DistributionCriterionCtrl', [
        '$scope',
        '$filter',
        '$state',
        '$q',
        'SweetAlert',
        'NgTableParams',
        'DistributionCriterionService',
        'UtilsService',
        'toaster',
        '$rootScope',
        function ($scope, $filter, $state, $q, SweetAlert, NgTableParams, DistributionCriterionService, UtilsService, toaster, $rootScope) {

            $scope.loading = true;
            init();

            $scope.getTypeList = function () {

                var list = [];
                angular.forEach(Object.keys(DistributionCriterionService.tableTypeEnum), function (type) {
                    list.push({ id: type, title: $filter('translate')(type) });
                });

                return list;
            };

            $scope.isSelectedSomeCheck = function () {

                if (!$scope.tableParams) {
                    return false;
                }

                for (var i in $scope.tableParams.settings().dataset) {
                    var item = $scope.tableParams.settings().dataset[i];
                    if (item.selected) {
                        return true;
                    }
                }

                return false;
            };

            $scope.delete = function () {

                SweetAlert.swal({
                    title: $filter('translate')('distribution-criterion.alert.title'),
                    text: $filter('translate')('distribution-criterion.alert.delete.message'),
                    type: "info",
                    showCancelButton: true,
                    confirmButtonText: $filter('translate')('distribution-criterion.alert.ok.btn'),
                    cancelButtonText: $filter('translate')('distribution-criterion.alert.cancel.btn')
                },
                    function (isConfirm) {

                        if (isConfirm) {
                            remove();
                        }
                    });
            };



            /********************************************/

            function remove() {
                var promises = [];
                $scope.deleting = true;

                // genero una promise per ogni richiesta
                angular.forEach($scope.tableParams.settings().dataset, function (item) {
                    if (item.selected) {
                        promises.push(DistributionCriterionService.resource.delete(item).$promise.then(function (data) {
                            item.deleted = true;
                        }));
                    }
                });

                // al termine dell'esecuzione di tutte le promise visualizzo
                // il messaggio
                $q.all(promises).then(function () {
                    toaster.pop({
                        type: 'success',
                        title: 'Cancellazione effettuata',
                        showCloseButton: true,
                        timeout: 2000
                    });
                }).catch(function (data) {
                    UtilsService.showHttpError(data.data, toaster);
                }).finally(function () {
                    $scope.deleting = false;
                    // elimino i dati eliminati dalla tabella
                    $scope.tableParams.settings().dataset = _.filter($scope.tableParams.settings().dataset, function (item) {
                        return !item.deleted;
                    });
                    $scope.tableParams.reload();
                });
            }

            function init() {

                $scope.condominiumId = $state.params.condominiumId;

                DistributionCriterionService.resource.get({condominiumId: $scope.condominiumId}).$promise.then(function (data) {
                    $scope.loading = false;
                    $scope.tableParams = new NgTableParams(
                        {
                            count: 10
                        },
                        {
                            counts: [],
                            dataset: data.content
                        });
                }, function (data) {
                    $scope.loading = false;
                    UtilsService.showHttpError(data.data, toaster);
                });
            }

        }]);