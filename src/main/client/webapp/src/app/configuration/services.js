"use strict";
angular.module("condominiumManagementApp.configuration")
    .factory("DistributionCriterioResource", ["$resource", "contextAddressManagement", function ($resource, contextAddressManagement) {
        return $resource(contextAddressManagement + "v1/distribution-criterion/:id",
            {
            },
            {
                update: {
                    method: 'PUT',
                    params: {
                        id: '@id'
                    }
                }
            }
        );
    }])
    .service("DistributionCriterioHousing", ["$resource", "contextAddressManagement", function ($resource, contextAddressManagement) {
        return $resource(contextAddressManagement + "v1/distribution-criterion/housing-units/:id",
            {
                id: '@id'
            }
        );
    }])
    .service("DistributionCriterioThousandthTables", ["$resource", "contextAddressManagement", function ($resource, contextAddressManagement) {
        return $resource(contextAddressManagement + "v1/thousandth-tables/:id",
            {
                condominiumId: '@id'
            }
        );
    }])
    .service("DistributionCriterioChartOfAccounts", ["$resource", "contextAddressManagement", function ($resource, contextAddressManagement) {
        return $resource(contextAddressManagement + "v1//distribution-criterion/chart-of-accounts/condominiumId/:condominiumId/financialPeriodId/:financialPeriodId",
            {
                condominiumId: '@condominiumId',
                financialPeriodId: '@financialPeriodId'
            }
        );
    }])
    .service("DistributionCriterionByMaterializedPath", ["$resource", "contextAddressManagement", function ($resource, contextAddressManagement) {
        return $resource(contextAddressManagement + "v1/distribution-criterion/by-materialized-path/:financialPeriodId:id",
            {
                financialPeriodId: "@financialPeriodId"
            },
            {
                get: {
                    method: "GET",
                    params: {
                        materializedPath: '@id'
                    }
                },
                update: {
                    method: "PUT"
                }
            }
        );
    }])
    .factory("FinancialPeriodResource", ["$resource", "contextAddressManagement", function ($resource, contextAddressManagement) {
        return $resource(contextAddressManagement + "v1/financial-periods/:id/:idCondominio",
            {
                id: '@id',
                condominiumId: 'idCondominio'
            },
            {
                update: { method: 'PUT' },
                query: { method: 'GET', isArray: false }
            }
        );
    }])
    .service('DistributionCriterioService', ['$http', 'contextAddressManagement', function ($http, contextAddressManagement) {
        return {
            getDistributionCriterioForCreation: function (financialPeriodId) {
                return $http({
                    method: 'GET',
                    url: contextAddressManagement + 'v1/distribution-criterion/' + financialPeriodId
                });
            }
        };
    }])
    .service("CentreCostService", ['$resource', 'contextAddressManagement', function ($resource, contextAddressManagement) {

        this.resource = $resource(contextAddressManagement + "v1/managements/:id",
            {
                id: '@id'
            },
            {
                update: { method: 'PUT' }
            }
        );
    }])
    .service("DistributionCriterionService", ['$resource', 'contextAddressManagement', function ($resource, contextAddressManagement) {
        this.tableTypeEnum = {
            THOUSANDTH: 'THOUSANDTH',
            ENERGY: 'ENERGY',
            INDIVIDUAL: 'INDIVIDUAL',
            EQUALS_PART: 'EQUALS_PART'
        };
        this.resource = $resource(contextAddressManagement + 'v1/thousandth-tables/:id',
            {
                id: '@id',
                page: 0,
                size: 1000
            },
            {
                update: { method: 'PUT' }
            }
        );

        this.resourceCreation = $resource(contextAddressManagement + 'v1/thousandth-tables/new-by-condominuim/:condominiumId', null, {
            new: {
                transformResponse: function (data) {

                    var json = angular.fromJson(data);
                    json.thousandthType = 'ENERGY';
                    json.competenceType = 'FULL';
                    json.energyDistributionColumn = 'VOLUNTARY_INVOLUNTARY';

                    angular.forEach(json.groups, function (group) {
                        angular.forEach(group.housingUnits, function (unit) {
                            unit.coefficient = unit.coefficient || 0;
                            angular.forEach(unit.roles, function (role) {
                                role.coefficient = role.coefficient || 0;
                            });
                        });
                    });

                    return json;
                }
            }
        });

    }]);