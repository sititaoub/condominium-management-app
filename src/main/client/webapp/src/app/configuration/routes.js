'use strict';

angular.module("condominiumManagementApp.configuration")
    .config(function ($stateProvider) {
        $stateProvider
            .state('index.centre-cost', {
                url: "/{condominiumId}/configuration/centre-cost",
                templateUrl: "app/configuration/partials/centre-cost.html",
                controller: "CentreCostCtrl",
                data: {
                    requiresAuthentication: true
                }
            })
            .state('index.distribution-criterion', {
                url: "/{condominiumId}/configuration/distribution-criterion",
                templateUrl: "app/configuration/partials/distribution-criterion.html",
                controller: "DistributionCriterionCtrl",
                data: {
                    requiresAuthentication: true,
                    skipFinancialPeriodCheck: true
                }
            })
            .state('index.distribution-criterion-detail', {
                url: "/{condominiumId}/configuration/distribution-criterion-detail/{id}",
                templateUrl: "app/configuration/partials/distribution-criterion-detail.html",
                controller: "DistributionCriterionDetailCtrl",
                data: {
                    requiresAuthentication: true,
                    skipFinancialPeriodCheck: true
                }
            })
            .state('index.distribution-criterio', {
                url: "/{condominiumId}/configuration/distribution-criterio",
                templateUrl: "app/configuration/partials/distribution-criterio.html",
                controller: "DistributionCriterioCtrl",
                data: {
                    requiresAuthentication: true
                }
            });
    });
