'use strict';

angular.module('condominiumManagementApp.configuration')
    .controller('CentreCostCtrl', [
        "$scope",
        "$filter",
        "CentreCostService",
        "UtilsService",
        "toaster",
        function ($scope, $filter, CentreCostService, UtilsService, toaster) {

            init();

            $scope.addCentreCost = function () {
                $scope.ccList.push(new CentreCost());
            };

            $scope.ccTreeOptions = {
                accept: function (sourceNodeScope, destNodesScope, destIndex) {
                    if (sourceNodeScope.depth() === 1) {
                        // sto spostando un centro di costo radice
                        return destNodesScope.depth() === 0; // è possibile spostarlo solo a livello 0
                    } else {
                        // sposto una voce di costo
                        return destNodesScope.depth() === 1; // è possibile spostarla solo sotto ad una voce di costo radice
                    }
                }
            };

            $scope.newSubItem = function (scope) {
                var nodeData = scope.$modelValue;
                nodeData.items.push(new CentreCost(nodeData.description + '_' + (nodeData.items.length + 1)));
            };

            $scope.save = function() {
                CentreCostService.resource.update(null, $scope.ccList, function(data){
                    toaster.pop({
                        type: 'success',
                        title: translate('centre-cost.save.done.message'),
                        showCloseButton: true,
                        timeout: 2000
                    });
                }, function(data) {
                    console.error("Error occurred while saving centre cost: ", data);
                    UtilsService.showHttpError(data.data, toaster);
                });
            };

            /**********************************************/

            function CentreCost(description, code, items) {
                this.description = description || $filter("translate")("centre-cost.new.label");
                this.code = code || calculateNewCode($scope.ccList);
                this.items = items || [];
            }

            /**
             * Calcola un nuovo codice numerico successivo al maggiore trovato
             * all'interno della lista passata
             * @param items
             */
            function calculateNewCode(items) {
                var code = 0;

                angular.forEach(items, function(item) {
                    var childrenMaxCount = calculateNewCode(item.items);
                    code = Math.max(code, childrenMaxCount, parseInt(item.code) +1);
                });

                return code;
            }

            function init() {
                $scope.loading = true;
                CentreCostService.resource.query().$promise.then(function (data) {
                    $scope.loading = false;
                    $scope.ccList = data;
                }, function (data) {
                    $scope.loading = false;
                    UtilsService.showHttpError(data.data, toaster);
                });
            }

        }]);
