/*
 * role.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .factory('RoleService', RoleService);

    RoleService.$inject = ['Restangular', 'contextAddressStructure', '_'];

    /* @ngInject */
    function RoleService(Restangular, contextAddressStructure, _) {
        var Roles = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressStructure + 'v1');
            configurer.addResponseInterceptor(function (data, operation, what, url, response) {
                if (operation === "post") {
                    var location = response.headers('Location');
                    return location.substr(location.lastIndexOf('/') + 1);
                }
                return data;
            });
        }).all('housing-unit-person-role');

        var service = {
            getRoleType: getRoleType,
            getPage: getPage,
            getOne: getOne,
            create: create,
            update: update,
            remove: remove
        };
        return service;

        ////////////////

        /**
         * Retrieve a list of role types.
         *
         * @returns {Promise<Object[]|Error>} - The promise of result.
         */
        function getRoleType() {
            return Roles.all('lookup-role-type').getList();
        }

        /**
         * Retrieve a single page of roles.
         *
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=10] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<Structure~Role|Error>} - The promise of page.
         */
        function getPage(page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return Roles.get("", params);
        }

        /**
         * Retrieve a single role.
         *
         * @param {Number} id - The unique id of role to retrieve.
         * @returns {Promise<Structure~Role|Error>} - The promise of result.
         */
        function getOne(id) {
            return Roles.one("", id).get().then(function (role) {
                role.from = role.from ? new Date(role.from) : undefined;
                role.to = role.to ? new Date(role.to) : undefined;
                return role;
            });
        }

        /**
         * Creates a new role.
         *
         * @param {Structure~Role} role - The role to create.
         * @returns {Promise<Object|Error>} - The promise of creation result.
         */
        function create(role) {
            return Roles.post(role);
        }

        /**
         * Updates an existing role.
         *
         * @param {Number} id - The unique id of role to update.
         * @param {Structure~Role} role - The data to update role with.
         * @returns {Promise<Object|Error>} - The promise of update result.
         */
        function update(id, role) {
            return Roles.one("", id).customPUT(role);
        }

        /**
         * Removes an existing role.
         *
         * @param {Number} id - The unique id of role to delete
         * @returns {Promise<Object|Error>} - The promise of remove result.
         */
        function remove(id) {
            return Roles.one("", id).remove();
        }
    }

})();

