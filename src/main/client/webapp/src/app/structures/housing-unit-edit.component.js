/*
 * housing-unit-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('housingUnitEdit', {
            controller: StructureHousingUnitEditController,
            bindings: {
                condominium: '<',
                building: '<',
                group: '<',
                unit: '<',
                onUnitUpdated: '&'
            },
            templateUrl: 'app/structures/housing-unit-edit.html'
        });

    StructureHousingUnitEditController.$inject = ['HousingUnitService', 'NotifierService'];

    /* @ngInject */
    function StructureHousingUnitEditController(HousingUnitService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////////

        function onInit() {
            $ctrl.unitId = $ctrl.unit.id;
        }

        function doSave() {
            HousingUnitService.update($ctrl.unitId, $ctrl.unit).then(function () {
                return NotifierService.notifySuccess('Structures.Edit.Unit.UpdateSuccess');
            }).then(function () {
                $ctrl.onUnitUpdated({ unit: angular.copy($ctrl.unit) })
            }).catch(function () {
                return NotifierService.notifyError('Structures.Edit.Unit.UpdateFailure');
            });
        }
    }

})();

