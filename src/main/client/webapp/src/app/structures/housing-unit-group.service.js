/*
 * housing-unit-group.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .factory('HousingUnitGroupService', HousingUnitGroupService);

    HousingUnitGroupService.$inject = ['Restangular', 'contextAddressStructure'];

    /* @ngInject */
    function HousingUnitGroupService(Restangular, contextAddressStructure) {
        var HousingUnitGroups = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressStructure + 'v1');
            configurer.addResponseInterceptor(function (data, operation, what, url, response, deferred) {
                if (operation === "post") {
                    var location = response.headers('Location');
                    return location.substr(location.lastIndexOf('/') + 1);
                }
                return data;
            });
        }).all('housing-unit-groups');

        var service = {
            getOne: getOne,
            create: create,
            update: update,
            remove: remove
        };
        return service;

        ////////////////

        /**
         * @namespace Structure
         */

        /**
         * An housing unit group.
         *
         * @typedef {Object} Structure~HousingUnitGroup
         * @property {Number} id - The unique identifier.
         * @property {String} stair - The stair name.
         * @property {String} building - The building description.
         * @property {Object} - The group address.
         */

        /**
         * Retrieve a single housing unit group.
         *
         * @param {Number} id - The unique id of housing unit to retrieve.
         * @returns {Promise<Structure~HousingUnitGroup|Error>} - The promise of result.
         */
        function getOne(id) {
            return HousingUnitGroups.one("", id).get();
        }

        /**
         * Creates a new housing unit group.
         *
         * @param {Structure~HousingUnitGroup} group - The group to create.
         * @returns {Promise<Object|Error>} - The promise of creation result.
         */
        function create(group) {
            return HousingUnitGroups.post(group);
        }

        /**
         * Updates an existing housing unit group.
         *
         * @param {Number} id - The unique id of housing unit group to update.
         * @param {Structure~HousingUnitGroup} group - The data to update group with.
         * @returns {Promise<Object|Error>} - The promise of update result.
         */
        function update(id, group) {
            return HousingUnitGroups.one("", id).customPUT(group);
        }

        /**
         * Removes an existing housing unit group.
         *
         * @param {Number} id - The unique id of housing unit group to delete
         * @returns {Promise<Object|Error>} - The promise of remove result.
         */
        function remove(id) {
            return HousingUnitGroups.one("", id).remove();
        }
    }

})();

