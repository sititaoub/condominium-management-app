/*
 * structures.config.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .config(setupI18n)
        .config(setupTree);

        setupI18n.$inject = ['$translatePartialLoaderProvider'];
        setupTree.$inject = ['treeConfig'];

        /* @ngInject */
        function setupI18n ($translatePartialLoaderProvider) {
            $translatePartialLoaderProvider.addPart('structures');
        }
        function setupTree (treeConfig) {
            treeConfig.defaultCollapsed = true;
        }

})();