/*
 * structure.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('structure', {
            controller: StructureController,
            bindings: {
                condominium: '<'
            },
            templateUrl: 'app/structures/structure.html'
        });

    StructureController.$inject = ['$state','StructureService'];

    /* @ngInject */
    function StructureController($state,StructureService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doPrintConfirm=doPrintConfirm;

        ////////////

        function onInit() {
        }

        function doPrintConfirm(outputFormat) {
            var params = {};
            params.condominiumId = $state.params.condominiumId;
            params.outputFormat=outputFormat;
            StructureService.getReport(params)
        }
    }

})();

