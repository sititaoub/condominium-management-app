/*
 * building-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('buildingCreate', {
            controller: StructureBuildingCreateController,
            bindings: {
                condominium: '<',
                onBuildingAdded: '&'
            },
            templateUrl: 'app/structures/building-create.html'
        });

    StructureBuildingCreateController.$inject = ['$state', 'BuildingService', 'NotifierService'];

    /* @ngInject */
    function StructureBuildingCreateController($state, BuildingService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////////

        function onInit() {
            $ctrl.building = {
                condominiumId: $ctrl.condominium.id
            };
        }

        function doSave() {
            BuildingService.create($ctrl.building).then(function (id) {
                return NotifierService.notifySuccess('Structures.Edit.Building.CreateSuccess').then(function () {
                    $ctrl.building.id = id;
                    $ctrl.onBuildingAdded({ building: angular.copy($ctrl.building) });
                    $state.go("^.building", { buildingId: id });
                });
            }).catch(function () {
                return NotifierService.notifyError('Structures.Edit.Building.CreateFailure');
            });
        }
    }

})();