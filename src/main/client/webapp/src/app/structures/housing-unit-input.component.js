/*
 * housing-unit-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('housingUnitInput', {
            controller: HousingUnitInputController,
            bindings: {
                ngModel: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/structures/housing-unit-input.html'
        });

    HousingUnitInputController.$inject = ['HousingUnitService', 'NotifierService'];

    /* @ngInject */
    function HousingUnitInputController(HousingUnitService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
            loadTypologies();
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function loadTypologies() {
            HousingUnitService.typologyLookup().then(function (list) {
                $ctrl.housingUnitTypologyList = list;
            }).catch(function () {
                NotifierService.notifyError();
            });
        }
    }

})();

