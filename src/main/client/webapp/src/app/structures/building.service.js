/*
 * building.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .factory('BuildingService', BuildingService);

    BuildingService.$inject = ['Restangular', 'contextAddressStructure', '_'];

    /* @ngInject */
    function BuildingService(Restangular, contextAddressStructure, _) {
        var Buildings = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressStructure + 'v1');
            configurer.addResponseInterceptor(function (data, operation, what, url, response) {
                if (operation === "post") {
                    var location = response.headers('Location');
                    return location.substr(location.lastIndexOf('/') + 1);
                }
                return data;
            });
        }).all('buildings');

        var service = {
            getPage: getPage,
            getOne: getOne,
            create: create,
            update: update,
            remove: remove
        };
        return service;

        ////////////////

        /**
         * @namespace Structure
         */

        /**
         * Building page.
         *
         * @typedef {Object} Structure~BuildingPage
         * @property {number} number - The requested page number.
         * @property {number} size - The requested page size.
         * @property {number} totalElements - The total number of elements.
         * @property {number} totalPages - The total number of pages.
         * @property {Structure~Building[]} content - The page content.
         */

        /**
         * An building.
         *
         * @typedef {Object} Structure~Building
         * @property {Number} id - The unique identifier.
         * @property {Number} condominiumId - The unique identifier of condominium.
         * @property {String} name - The name.
         */

        /**
         * Retrieve a single page of housing units for given condominium id.
         *
         * @param {number} condominiumId - The unique company id.
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=200] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<Structure~HousingUnitPage|Error>} - The promise of page.
         */
        function getPage(condominiumId, page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                condominiumId: condominiumId,
                page: page || 0,
                size: size || 200,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return Buildings.get("", params);
        }

        /**
         * Retrieve a single building.
         *
         * @param {Number} id - The unique id of building to retrieve.
         * @returns {Promise<Structure~Building|Error>} - The promise of result.
         */
        function getOne(id) {
            return Buildings.one("", id).get();
        }

        /**
         * Creates a new building.
         *
         * @param {Structure~Building} building - The building to create.
         * @returns {Promise<Object|Error>} - The promise of creation result.
         */
        function create(building) {
            return Buildings.post(building);
        }

        /**
         * Updates an existing building.
         *
         * @param {Number} id - The unique id of building to update.
         * @param {Structure~Building} building - The data to update unit with.
         * @returns {Promise<Object|Error>} - The promise of update result.
         */
        function update(id, building) {
            return Buildings.one("", id).customPUT(building);
        }

        /**
         * Removes an existing building.
         *
         * @param {Number} id - The unique id of building to delete
         * @returns {Promise<Object|Error>} - The promise of remove result.
         */
        function remove(id) {
            return Buildings.one("", id).remove();
        }
    }

})();

