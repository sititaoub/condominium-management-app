/*
 * housing-unit-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('housingUnitCreate', {
            controller: StructureHousingUnitCreateController,
            bindings: {
                condominium: '<',
                building: '<',
                group: '<',
                onUnitAdded: '&'
            },
            templateUrl: 'app/structures/housing-unit-create.html'
        });

    StructureHousingUnitCreateController.$inject = ['$state', 'HousingUnitService', 'NotifierService'];

    /* @ngInject */
    function StructureHousingUnitCreateController($state, HousingUnitService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////////

        function onInit() {
            $ctrl.unit = {
                housingUnitGroupId: $ctrl.group.id,
                tipology: 1,
                cadastralReference: {
                    page: ($ctrl.condominium.cadastralReference || { page: undefined }).page,
                    parcel: ($ctrl.condominium.cadastralReference || { page: undefined }).parcel
                }
            };
        }

        function doSave() {
            HousingUnitService.create($ctrl.unit).then(function (id) {
                return NotifierService.notifySuccess('Structures.Edit.Unit.CreateSuccess').then(function () {
                    $ctrl.unit.id = id;
                    $ctrl.onUnitAdded({ unit: angular.copy($ctrl.unit) })
                    $state.go("^.housingUnit", {
                        buildingId: $ctrl.building.id,
                        groupId: $ctrl.group.id,
                        housingUnitId: id
                    });
                });
            }).catch(function () {
                return NotifierService.notifyError('Structures.Edit.Unit.CreateFailure');
            });
        }
    }

})();

