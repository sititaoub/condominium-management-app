/*
 * role-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('roleInput', {
            controller: RoleInputController,
            bindings: {
                ngModel: '<',
                condominiumId: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/structures/role-input.html'
        });

    RoleInputController.$inject = ['RoleService', 'NotifierService'];

    /* @ngInject */
    function RoleInputController(RoleService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
            loadRoleTypes();
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function loadRoleTypes() {
            RoleService.getRoleType().then(function (roleTypes) {
                $ctrl.roleTypes = roleTypes;
            }).catch(function () {
                NotifierService.notifyError();
            });
        }
    }

})();

