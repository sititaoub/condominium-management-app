/*
 * group-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('groupCreate', {
            controller: StructureGroupCreateController,
            bindings: {
                condominium: '<',
                building: '<',
                onGroupAdded: '&'
            },
            templateUrl: 'app/structures/group-create.html'
        });

    StructureGroupCreateController.$inject = ['$state', 'HousingUnitGroupService', 'NotifierService'];

    /* @ngInject */
    function StructureGroupCreateController($state, HousingUnitGroupService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////////

        function onInit() {
            $ctrl.group = {
                address: angular.copy($ctrl.condominium.address),
                buildingId: $ctrl.building.id
            };
        }

        function doSave() {
            HousingUnitGroupService.create($ctrl.group).then(function (id) {
                return NotifierService.notifySuccess('Structures.Edit.Group.CreateSuccess').then(function () {
                    $ctrl.group.id = id;
                    $ctrl.onGroupAdded({ group: angular.copy($ctrl.group) });
                    $state.go("^.group", { buildingId: $ctrl.building.id, groupId: id });
                });
            }).catch(function () {
                return NotifierService.notifyError('Structures.Edit.Group.CreateFailure');
            });
        }
    }

})();

