/*
 * structure-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('structureEdit', {
            controller: StructureEditController,
            bindings: {
                condominium: '<',
                tree: '<'
            },
            templateUrl: 'app/structures/structure-edit.html'
        });

    StructureEditController.$inject = ['$state', '$transitions', '$scope',
        'BuildingService', 'HousingUnitGroupService', 'HousingUnitService',
        'RoleService', 'NotifierService', 'AlertService', '_','$timeout'];

    /* @ngInject */
    function StructureEditController($state, $transitions, $scope,
                                     BuildingService, HousingUnitGroupService, HousingUnitService,
                                     RoleService, NotifierService, AlertService, _,$timeout) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.addBuilding = addBuilding;
        $ctrl.updateBuilding = updateBuilding;
        $ctrl.deleteBuilding = deleteBuilding;
        $ctrl.addGroup = addGroup;
        $ctrl.updateGroup = updateGroup;
        $ctrl.deleteGroup = deleteGroup;
        $ctrl.addUnit = addUnit;
        $ctrl.updateUnit = updateUnit;
        $ctrl.deleteUnit = deleteUnit;
        $ctrl.addRole = addRole;
        $ctrl.updateRole = updateRole;
        $ctrl.deleteRole = deleteRole;

        $ctrl.expandAll = expandAll;
        $ctrl.token = false;
        ////////////////

        function onInit() {
            $ctrl.treeScrollbarConfig = {
                autoHideScrollbar: false,
                theme: 'dark',
                advanced: {
                    axis: 'y',
                    updateOnContentResize: true
                }
            };
            updateControllerFromParams($state.params);
            setupRouteChangeHooks();

            if($ctrl.tree.condominiums[0].buildings.length > 1) {
                $timeout(function() {
                    var el = document.getElementById('buildingsTree');
                    angular.element(el).triggerHandler('click');
                }, 500);
            } else {
                $timeout(function() {
                    var el = document.getElementById('buildingsTree');
                    angular.element(el).triggerHandler('click');
                    el = document.getElementById('housingUnitGroupsTree');
                    angular.element(el).triggerHandler('click');
                }, 500);
            }
        }
        function expandAll(scope) {
            if($ctrl.token) {
                $scope.$broadcast('angular-ui-tree:collapse-all');
                $ctrl.token = false;
            } else {
                $scope.$broadcast('angular-ui-tree:expand-all');
                $ctrl.token = true;
            }
        }
        function setupRouteChangeHooks() {
            var handler = $transitions.onSuccess({ to: 'index.condominiumStructure.**' }, function (transition) {
                updateControllerFromParams(transition.params('to'));
            });
            $scope.$on('$destroy', function () {
                handler();
            });
        }

        function updateControllerFromParams(params) {
            $ctrl.condominiumId = params.condominiumId;
            $ctrl.buildingId = params.buildingId;
            $ctrl.groupId = params.groupId;
            $ctrl.housingUnitId = params.housingUnitId;
            $ctrl.roleId = params.roleId;
        }

        function addBuilding(building) {
            $ctrl.tree.condominiums[0].buildings.push({
                id: parseInt(building.id),
                description: building.name,
                housingUnitGroups: []
            });
        }

        function updateBuilding(building) {
            _.find($ctrl.tree.condominiums[0].buildings, { id: parseInt(building.id) }).description = building.name;
        }

        function deleteBuilding(building) {
            AlertService.askConfirm('Structures.Edit.Building.DeleteConfirm', building).then(function () {
                BuildingService.remove(building.id).then(function () {
                    return NotifierService.notifySuccess('Structures.Edit.Building.DeleteSuccess');
                }).then(function () {
                    // Remove the object from tree
                    _.remove($ctrl.tree.condominiums[0].buildings, { id: parseInt(building.id) });
                    // If the actually selected element was deleted, move up to condominium.
                    if ($state.params.buildingId == building.id) {
                        $state.go('index.condominiumStructure.edit', $state.params);
                    }
                }).catch(function () {
                    return NotifierService.notifyError('Structures.Edit.Building.DeleteFailure');
                })
            });
        }

        function addGroup(group) {
            var building = _.find($ctrl.tree.condominiums[0].buildings, { id: parseInt($state.params.buildingId) });
            building.housingUnitGroups.push({
                id: parseInt(group.id),
                description: group.stair,
                housingUnits: []
            });
        }

        function updateGroup(group) {
            var building = _.find($ctrl.tree.condominiums[0].buildings, { id: parseInt($state.params.buildingId) });
            _.find(building.housingUnitGroups, { id: parseInt(group.id) }).description = group.stair;
        }

        function deleteGroup(building, group) {
            AlertService.askConfirm('Structures.Edit.Group.DeleteConfirm', group).then(function () {
                HousingUnitGroupService.remove(group.id).then(function () {
                    return NotifierService.notifySuccess('Structures.Edit.Group.DeleteSuccess');
                }).then(function () {
                    // Remove the object from tree
                    var treeBuilding = _.find($ctrl.tree.condominiums[0].buildings, { id: parseInt(building.id) });
                    _.remove(treeBuilding.housingUnitGroups, { id: parseInt(group.id) });
                    // If the actually selected element was deleted, move up to condominium.
                    if ($state.params.groupId == group.id) {
                        $state.go('index.condominiumStructure.edit.building', $state.params);
                    }
                }).catch(function () {
                    return NotifierService.notifyError('Structures.Edit.Group.DeleteFailure');
                })
            });
        }

        function addUnit(unit) {
            var building = _.find($ctrl.tree.condominiums[0].buildings, { id: parseInt($state.params.buildingId) });
            var group = _.find(building.housingUnitGroups, { id: parseInt($state.params.groupId) });
            group.housingUnits.push({
                id: parseInt(unit.id),
                description: unit.owner,
                roles: []
            })
        }

        function updateUnit(unit) {
            var building = _.find($ctrl.tree.condominiums[0].buildings, { id: parseInt($state.params.buildingId) });
            var group = _.find(building.housingUnitGroups, { id: parseInt($state.params.groupId) });
            _.find(group.housingUnits, { id: parseInt(unit.id) }).description = unit.owner;
        }

        function deleteUnit(building, group, unit) {
            AlertService.askConfirm('Structures.Edit.Unit.DeleteConfirm', unit).then(function () {
                HousingUnitService.remove(unit.id).then(function () {
                    return NotifierService.notifySuccess('Structures.Edit.Unit.DeleteSuccess');
                }).then(function () {
                    // Remove the object from tree
                    var treeBuilding = _.find($ctrl.tree.condominiums[0].buildings, { id: parseInt(building.id) });
                    var treeGroup = _.find(treeBuilding.housingUnitGroups, { id: parseInt(group.id) });
                    _.remove(treeGroup.housingUnits, { id: parseInt(unit.id) });
                    // If the actually selected element was deleted, move up to group.
                    if ($state.params.housingUnitId == unit.id) {
                        $state.go('index.condominiumStructure.edit.group', $state.params);
                    }
                }).catch(function () {
                    return NotifierService.notifyError('Structures.Edit.Unit.DeleteFailure');
                })
            })
        }

        function addRole(role) {
            var building = _.find($ctrl.tree.condominiums[0].buildings, { id: parseInt($state.params.buildingId) });
            var group = _.find(building.housingUnitGroups, { id: parseInt($state.params.groupId) });
            var housingUnit = _.find(group.housingUnits, { id: parseInt($state.params.housingUnitId )});
            housingUnit.roles.push({
                id: parseInt(role.id),
                companyName: role.person.companyName,
                firstName: role.person.firstName,
                lastName: role.person.lastName,
                rolePercentage: role.rolePercentage,
                roleType: role.roleType
            });
        }

        function updateRole(role) {
            var building = _.find($ctrl.tree.condominiums[0].buildings, { id: parseInt($state.params.buildingId) });
            var group = _.find(building.housingUnitGroups, { id: parseInt($state.params.groupId) });
            var housingUnit = _.find(group.housingUnits, { id: parseInt($state.params.housingUnitId )});
            var item = _.find(housingUnit.roles, { id: parseInt(role.id) });
            item.companyName = role.person.companyName;
            item.firstName = role.person.firstName;
            item.lastName = role.person.lastName;
            item.rolePercentage = role.rolePercentage;
            item.roleType = role.roleType;
        }

        function deleteRole(building, group, unit, role) {
            AlertService.askConfirm('Structures.Edit.Role.DeleteConfirm',role).then(function () {
                RoleService.remove(role.id).then(function () {
                    return NotifierService.notifySuccess('Structures.Edit.Role.DeleteSuccess');
                }).then(function () {
                    // Remove the object from tree
                    var treeBuilding = _.find($ctrl.tree.condominiums[0].buildings, { id: parseInt(building.id) });
                    var treeGroup = _.find(treeBuilding.housingUnitGroups, { id: parseInt(group.id) });
                    var treeUnit = _.find(treeGroup.housingUnits, { id: parseInt(unit.id) });
                    _.remove(treeUnit.roles, { id: parseInt(role.id) });
                    // If the actually selected element was deleted, move up to housing unit.
                    if ($state.params.roleId == role.id) {
                        $state.go('index.condominiumStructure.edit.housingUnit', $state.params);
                    }
                }).catch (function () {
                    return NotifierService.notifyError('Structures.Edit.Role.DeleteFailure');
                })
            })
        }
    }

})();

