/*
 * tenant-terminate.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('tenantTerminate', {
            controller: TenantTerminateController,
            bindings: {
                condominium: '<',
                tenant: '<',
                housingUnit: '<'
            },
            templateUrl: 'app/structures/tenants/tenant-terminate.html'
        });

    TenantTerminateController.$inject = ['$state', '$q', 'TenantsService', 'NotifierService', 'moment', '_'];

    /* @ngInject */
    function TenantTerminateController($state, $q, TenantsService, NotifierService, moment, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.terminateTenant = {
                condominiumId: $ctrl.condominium.id,
                housingUnitId: $ctrl.housingUnit.id
            };
            $ctrl.minDate = moment($ctrl.tenant.from).add(1, 'day').toDate();
            $ctrl.toOptions = { minDate: $ctrl.minDate };
        }

        function doSave() {
            $ctrl.terminateTenant.tenantId = $ctrl.tenant.roleId;
            return TenantsService.terminate($ctrl.condominium.id, $ctrl.terminateTenant).then(function () {
                return NotifierService.notifySuccess('Structures.Tenants.Terminate.Success');
            }).then(function () {
                return $state.go('^', undefined, { reload: true });
            }).catch(function (err) {
                NotifierService.notifyError('Structures.Tenants.Terminate.Failure');
                return $q.reject(err);
            })
        }
    }

})();

