/*
 * tenant-assign.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('tenantEdit', {
            controller: TenantEditController,
            bindings: {
                condominium: '<',
                housingUnit: '<',
                tenant : '<'
            },
            templateUrl: 'app/structures/tenants/tenant-edit.html'
        });

    TenantEditController.$inject = ['$state', '$q', 'TenantsService', 'NotifierService'];

    /* @ngInject */
    function TenantEditController($state, $q, TenantsService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.tenant = angular.copy($ctrl.tenant);
            $ctrl.roleId = $ctrl.tenant.roleId;
            $ctrl.personId = $ctrl.tenant.personId;
        }

        function doSave() {
            $ctrl.tenant.roleId =  $ctrl.roleId;
            if(!$ctrl.tenant.person.hasOwnProperty("id")){
                $ctrl.tenant.person.id = $ctrl.personId;
            }
            return TenantsService.update($ctrl.condominium.id, $ctrl.tenant).then(function () {
                return NotifierService.notifySuccess('Structures.Tenants.Edit.Success');
            }).then(function () {
                return $state.go('^', undefined, { reload: true });
            }).catch(function (err) {
                NotifierService.notifyError('Structures.Tenants.Edit.Failure');
                return $q.reject(err);
            })
        }
    }

})();

