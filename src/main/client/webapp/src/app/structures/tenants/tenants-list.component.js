/*
 * tenants-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('tenantsList', {
            controller: TenantsListController,
            bindings: {
                condominium: '<',
                tenants: '<'
            },
            templateUrl: 'app/structures/tenants/tenants-list.html'
        });

    TenantsListController.$inject = ['NgTableParams', '$uibModal', 'TenantsService', '_'];

    /* @ngInject */
    function TenantsListController(NgTableParams, $uibModal, TenantsService, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.toggleFilters = toggleFilters;
        $ctrl.getBuildings = getBuildings;
        $ctrl.getGroups = getGroups;
        $ctrl.showHistory = showHistory;

        ////////////

        function onInit() {
            $ctrl.showFilters = false;
            $ctrl.tableParams = new NgTableParams({
                page: 1,
                count: 10,
                sorting: { building: 'asc', group: 'asc', unit: 'asc' }
            }, {
                dataset: $ctrl.tenants
            });
        }

        function toggleFilters() {
            $ctrl.showFilters = !$ctrl.showFilters;
        }

        function getBuildings() {
            return _.sortBy(_.uniqBy(_.map($ctrl.tenants, function (tenant) {
                return {
                    id: tenant.buildingId,
                    title: tenant.building
                };
            }), 'id'), 'title');
        }

        function getGroups() {
            var tenants = $ctrl.tenants;
            var buildingId = $ctrl.tableParams.filter().buildingId;
            if (angular.isDefined(buildingId) && buildingId !== '') {
                tenants = _.filter($ctrl.tenants, { buildingId: buildingId });
            }
            return _.sortBy(_.uniqBy(_.map(tenants, function (tenant) {
                return {
                    id: tenant.groupId,
                    title: tenant.group,
                    buildingId: tenant.buildingId,
                    fullTitle: tenant.building + ' - ' + tenant.group
                }
            }), 'id'), 'fullTitle');
        }

        function showHistory(unitId) {
            $uibModal.open({
                size: 'lg',
                component: 'tenantsHistory',
                resolve: {
                    condominium: _.constant($ctrl.condominium),
                    history: TenantsService.getHistory($ctrl.condominium.id, unitId)
                }
            });
        }
    }

})();

