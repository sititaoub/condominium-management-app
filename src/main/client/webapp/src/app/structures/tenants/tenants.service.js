/*
 * tenants.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .factory('TenantsService', TenantsService);

    TenantsService.$inject = ['Restangular', 'contextAddressStructure'];

    /* @ngInject */
    function TenantsService(Restangular, contextAddressStructure) {
        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressStructure + 'v1');
        });
        var service = {
            getAll: getAll,
            getOne: getOne,
            getHistory: getHistory,
            assign: assign,
            update : update,
            replace: replace,
            terminate: terminate
        };
        return service;

        ////////////////

        function getAll(condominiumId) {
            return Api.one("condominiums", condominiumId).all("housing-unit").all("tenants").getList();
        }

        function getOne(condominiumId, housingUnitId) {
            return Api.one("condominiums", condominiumId).one("housing-unit", housingUnitId).all("tenant").get("");
        }

        function getHistory(condominiumId, housingUnitId) {
            return Api.one("condominiums", condominiumId).one("housing-unit", housingUnitId).all("tenants").all("history").getList();
        }

        function assign(condominiumId, tenant) {
            return Api.one("condominiums", condominiumId).one("housing-unit", tenant.housingUnitId).all("tenant").post(tenant);
        }

        function update(condominiumId, tenant) {
            return Api.one("condominiums", condominiumId).one("housing-unit", tenant.housingUnitId).all("tenant").customPUT(tenant);
        }

        function replace(condominiumId, tenant) {
            return Api.one("condominiums", condominiumId).one("housing-unit", tenant.housingUnitId).one("tenant","takeover").customPUT(tenant);
        }

        function terminate(condominiumId, tenant) {
            return Api.one("condominiums", condominiumId).one("housing-unit", tenant.housingUnitId).one("tenant", "termination").customPUT(tenant);
        }
    }
})();

