/*
 * tenant-display.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('tenantDisplay', {
            controller: angular.noop,
            bindings: {
                'tenant': '<'
            },
            templateUrl: 'app/structures/tenants/tenant-display.html'
        });

})();

