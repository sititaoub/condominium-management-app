/*
 * tenant-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('tenantInput', {
            controller: TenantInputController,
            bindings: {
                ngModel: '<',
                condominium: '<',
                housingUnitId: '<',
                minDate: '<?'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/structures/tenants/tenant-input.html'
        });

    TenantInputController.$inject = [];

    /* @ngInject */
    function TenantInputController() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.fromOptions = {
                minDate: $ctrl.minDate
            };
            $ctrl.ngModelCtrl.$render = function () {
                    $ctrl.value = $ctrl.ngModel != undefined ?
                        {
                            housingUnitId: $ctrl.housingUnitId,
                            from: new Date($ctrl.ngModel.from),
                            person : { companyName : $ctrl.ngModel.companyName == null ?
                                $ctrl.ngModel.firstName + " " +  $ctrl.ngModel.lastName : $ctrl.ngModel.companyName
                            }
                        } :
                    $ctrl.value = $ctrl.ngModelCtrl.$viewValue ||                     {
                        housingUnitId: $ctrl.housingUnitId,
                        from: new Date()
                    };
            };
            if($ctrl.ngModel != undefined) {
                onChange()
            }
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

