/*
 * tenant-assign.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('tenantAssign', {
            controller: TenantAssignController,
            bindings: {
                condominium: '<',
                housingUnit: '<'
            },
            templateUrl: 'app/structures/tenants/tenant-assign.html'
        });

    TenantAssignController.$inject = ['$state', '$q', 'TenantsService', 'NotifierService'];

    /* @ngInject */
    function TenantAssignController($state, $q, TenantsService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.tenant = undefined;
        }

        function doSave() {
            return TenantsService.assign($ctrl.condominium.id, $ctrl.tenant).then(function () {
                return NotifierService.notifySuccess('Structures.Tenants.Assign.Success');
            }).then(function () {
                return $state.go('^', undefined, { reload: true });
            }).catch(function (err) {
                NotifierService.notifyError('Structures.Tenants.Assign.Failure');
                return $q.reject(err);
            })
        }
    }

})();

