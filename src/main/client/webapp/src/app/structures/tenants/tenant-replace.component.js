/*
 * tenant-replace.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('tenantReplace', {
            controller: TenantReplaceController,
            bindings: {
                condominium: '<',
                tenant: '<',
                housingUnit: '<'
            },
            templateUrl: 'app/structures/tenants/tenant-replace.html'
        });

    TenantReplaceController.$inject = ['$state', '$q', 'TenantsService', 'NotifierService', 'moment', '_'];

    /* @ngInject */
    function TenantReplaceController($state, $q, TenantsService, NotifierService, moment, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.replaceTenant = undefined;
            $ctrl.minDate = moment($ctrl.tenant.from).add(1, 'day').toDate();
        }

        function doSave() {
            $ctrl.replaceTenant.previousTenant = $ctrl.tenant.roleId;
            return TenantsService.replace($ctrl.condominium.id, $ctrl.replaceTenant).then(function () {
                return NotifierService.notifySuccess('Structures.Tenants.Replace.Success');
            }).then(function () {
                return $state.go('^', undefined, { reload: true });
            }).catch(function (err) {
                NotifierService.notifyError('Structures.Tenants.Replace.Failure');
                return $q.reject(err);
            })
        }
    }

})();

