/*
 * structures.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider'];

    /* @ngInject */
    function setupRoutes ($stateProvider) {
        $stateProvider.state('index.condominiumStructure.tenants', {
            url: '/tenants',
            redirectTo: 'index.condominiumStructure.tenants.list',
            template: '<div class="owners-container" ui-view></div>'
        }).state('index.condominiumStructure.tenants.list', {
            url: '?page&count&sort',
            component: 'tenantsList',
            params: {
                page: { dynamic: true, squash: true, value: '1' },
                count: { dynamic: true, squash: true, value: '10' },
                sort: { dynamic: true, squash: true, value: 'name,asc' }
            },
            resolve: {
                tenants: ['condominium', 'TenantsService', function (condominium, TenantsService) {
                    return TenantsService.getAll(condominium.id);
                }]
            }
        }).state('index.condominiumStructure.tenants.list.assign', {
            url: '/{unitId}/assign',
            views: {
                '@index.condominiumStructure.tenants': {
                    component: 'tenantAssign'
                }
            },
            resolve: {
                housingUnit: ['$transition$', 'HousingUnitService', function ($transition$, HousingUnitService) {
                    return HousingUnitService.getOne($transition$.params().unitId);
                }]
            }
        }).state('index.condominiumStructure.tenants.list.edit', {
            url: '/{unitId}/edit',
            views: {
                '@index.condominiumStructure.tenants': {
                    component: 'tenantEdit'
                }
            },
            resolve: {
                housingUnit: ['$transition$', 'HousingUnitService', function ($transition$, HousingUnitService) {
                    return HousingUnitService.getOne($transition$.params().unitId);
                }],
                tenant: ['condominium', '$transition$', 'TenantsService', function (condominium, $transition$, TenantsService) {
                    return TenantsService.getOne(condominium.id, $transition$.params().unitId)
                }]
            }
        }).state('index.condominiumStructure.tenants.list.replace', {
            url: '/{unitId}/replace',
            views: {
                '@index.condominiumStructure.tenants': {
                    component: 'tenantReplace'
                }
            },
            resolve: {
                housingUnit: ['$transition$', 'HousingUnitService', function ($transition$, HousingUnitService) {
                    return HousingUnitService.getOne($transition$.params().unitId);
                }],
                tenant: ['condominium', '$transition$', 'TenantsService', function (condominium, $transition$, TenantsService) {
                    return TenantsService.getOne(condominium.id, $transition$.params().unitId)
                }]
            }
        }).state('index.condominiumStructure.tenants.list.terminate', {
            url: '/{unitId}/terminate',
            views: {
                '@index.condominiumStructure.tenants': {
                    component: 'tenantTerminate'
                }
            },
            resolve: {
                housingUnit: ['$transition$', 'HousingUnitService', function ($transition$, HousingUnitService) {
                    return HousingUnitService.getOne($transition$.params().unitId);
                }],
                tenant: ['condominium', '$transition$', 'TenantsService', function (condominium, $transition$, TenantsService) {
                    return TenantsService.getOne(condominium.id, $transition$.params().unitId)
                }]
            }
        });
    }

})();