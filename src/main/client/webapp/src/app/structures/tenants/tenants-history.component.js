/*
 * tenants-history.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('tenantsHistory', {
            controller: TenantsHistoryController,
            bindings: {
                dismiss: '&',
                resolve: '<'
            },
            templateUrl: 'app/structures/tenants/tenants-history.html'
        });

    TenantsHistoryController.$inject = [];

    /* @ngInject */
    function TenantsHistoryController() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;

        ////////////

        function onInit() {
            $ctrl.condominium = $ctrl.resolve.condominium;
            $ctrl.history = $ctrl.resolve.history;
        }
    }

})();

