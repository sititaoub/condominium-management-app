/*
 * structure.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .factory('StructureService', StructureService);

    StructureService.$inject = ['Restangular', 'contextAddressStructure', 'contextAddressManagement','$window'];

    /* @ngInject */
    function StructureService(Restangular, contextAddressStructure, contextAddressManagement,$window) {
        var ApiManagement = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressManagement + 'v1');
        });

        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressStructure + 'v1');
        });

        var service = {
            getTree: getTree,
            getSimpleTree: getSimpleTree,
            getReport: getReport,
            getPersons: getPersons
        };
        return service;

        ////////////////

        /**
         * @namespace Structures
         */

        /**
         * Container of housing units.
         *
         * @typedef {Object} Structures~Tree
         * @property {Structures~HousingUnitGroup[]} housingUnitGroups - The groups of housing units.
         */

        /**
         * A single housing unit group.
         *
         * @typedef {Object} Structures~HousingUnitGroup
         * @property {Number} id - The unique id of group.
         * @property {String} description - The group description.
         * @property {Structures~HousingUnit[]} - The housing units of this group.
         */

        /**
         * An housing unit.
         *
         * @typedef {Object} Structures~HousingUnit
         * @property {Number} id - The unique id of housing unit.
         * @property {String} description - The description.
         * @property {Object[]} heatMeters - Ignored but mandatory.
         * @property {Object[]} waterMeters - Ignored but mandatory.
         * @property {Object[]} rooms - Ignored but mandatory.
         * @property {Structures~Role[]} roles - The people with roles in this housing unit.
         */

        /**
         * A role in housing unit.
         *
         * @typedef {Object} Structures~Role
         * @property {Number} id - The unique identifier of role
         * @property {String} companyName - The name of company.
         * @property {String} firstName - The company's first name.
         * @property {String} lastName - The company's last name.
         * @property {Number} rolePercentage - The percentage of role for this person.
         * @property {String} roleType - The enumeration for role type.
         */

        /**
         * An extended role in housing unit.
         *
         * @typedef {Object} Structures~ExtendedRole
         * @property {Number} id - The unique identifier of role
         * @property {String} companyName - The name of company.
         * @property {String} firstName - The company's first name.
         * @property {String} lastName - The company's last name.
         * @property {Number} rolePercentage - The percentage of role for this person.
         * @property {String} roleType - The enumeration for role type.
         * @property {string} buildingId - The unique id of building.
         * @property {string} building - The name of building.
         * @property {string} groupId - The unique id of group.
         * @property {string} group - The description of group.
         * @property {string} housingUnitId - The unique id of housing unit.
         * @property {string} housingUnit - The description of housing unit.
         */

        /**
         * Retrieve the tree of condominium.
         *
         * @param {Number} condominiumId - The unique id of condominium.
         * @returns {Promise<Structures~Tree|Error>} - The promise of condominium structure tree.
         */
        function getTree(condominiumId) {
            return Api.one("condominiums", condominiumId).all("structure").all("with-persons").get("");
        }

        /**
         * Retrieve the tree of condominium without people.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @returns {Promise<Structures~Tree|Error>} - The promise of condominium structure tree.
         */
        function getSimpleTree(condominiumId) {
            return Api.one("condominiums", condominiumId).all("structure").get("");
        }

          /**
         *  call end-point for download report
         * possible value of options parameters are: 
         * - condominiumId(Mandatory)
         * - financialPeriodId(Mandatory)
         * @param {Object} options - parameter for print Report
         */
        function getReport(options) {
            var url = ApiManagement.all("condominium-structure").all("reports").getRequestedUrl();
            url = url + "?condominiumId=" + options.condominiumId  + "&format=" + options.outputFormat;
            $window.open(url, "_blank");
        }

        /**
         * Retrieve the persons of a given condominium with roles, buildings and housing units.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @returns {Promise<Structure~ExtendedRole!Error>} - The promise of condominium structure persons roles.
         */
        function getPersons(condominiumId) {
            return getTree(condominiumId).then(function (tree) {
                return _.flatMap(tree.buildings, function (building) {
                    return _.map(building.housingUnitGroups, function (group) {
                        group.buildingId = building.id;
                        group.building = building.description;
                        return group;
                    });
                });
            }).then(function (groups) {
                return _.flatMap(groups, function (group) {
                    return _.map(group.housingUnits, function (hu) {
                        hu.buildingId = group.buildingId;
                        hu.building = group.building;
                        hu.groupId = group.id;
                        hu.group = group.description;
                        return hu;
                    });
                });
            }).then(function (housingUnits) {
                return _.flatMap(housingUnits, function (hu) {
                    return _.map(hu.roles, function (r) {
                        r.buildingId = hu.buildingId;
                        r.building = hu.building;
                        r.groupId = hu.groupId;
                        r.group = hu.group;
                        r.housingUnitId = hu.id;
                        r.housingUnit = hu.description;
                        return r;
                    });
                });
            }).then(function (roles) {
                return _.sortBy(roles, 'personId');
            });
        }
    }

})();

