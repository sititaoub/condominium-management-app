/*
 * building-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('buildingInput', {
            controller: BuildingInputController,
            bindings: {
                ngModel: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/structures/building-input.html'
        });

    BuildingInputController.$inject = [];

    /* @ngInject */
    function BuildingInputController() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

