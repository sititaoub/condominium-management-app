/*
 * building-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('buildingEdit', {
            controller: StructureBuildingEditController,
            bindings: {
                condominium: '<',
                building: '<',
                onBuildingUpdated: '&'
            },
            templateUrl: 'app/structures/building-edit.html'
        });

    StructureBuildingEditController.$inject = ['BuildingService', 'NotifierService'];

    /* @ngInject */
    function StructureBuildingEditController(BuildingService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////////

        function onInit() {
            $ctrl.buildingId = $ctrl.building.id;
        }

        function doSave() {
            BuildingService.update($ctrl.buildingId, $ctrl.building).then(function () {
                return NotifierService.notifySuccess('Structures.Edit.Building.UpdateSuccess');
            }).then(function () {
                $ctrl.onBuildingUpdated({ building: angular.copy($ctrl.building ) });
            }).catch(function () {
                return NotifierService.notifyError('Structures.Edit.Building.UpdateFailure');
            });
        }
    }

})();

