/*
 * person-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('roleEdit', {
            controller: StructureRoleEditController,
            bindings: {
                condominium: '<',
                building: '<',
                group: '<',
                unit: '<',
                role: '<',
                onRoleUpdated: '&'
            },
            templateUrl: 'app/structures/role-edit.html'
        });

    StructureRoleEditController.$inject = ['RoleService', 'NotifierService'];

    /* @ngInject */
    function StructureRoleEditController(RoleService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////////

        function onInit() {
            $ctrl.roleId = $ctrl.role.id;
        }

        function doSave() {
            RoleService.update($ctrl.roleId, $ctrl.role).then(function () {
                return NotifierService.notifySuccess('Structures.Edit.Role.UpdateSuccess');
            }).then(function () {
                $ctrl.onRoleUpdated({ role: angular.copy($ctrl.role) });
            }).catch(function () {
                return NotifierService.notifyError('Structures.Edit.Role.UpdateFailure');
            });
        }
    }

})();

