/*
 * housing-unit.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .factory('HousingUnitService', HousingUnitService);

    HousingUnitService.$inject = ['Restangular', 'contextAddressStructure', '_'];

    /* @ngInject */
    function HousingUnitService(Restangular, contextAddressStructure, _) {
        var HousingUnits = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressStructure + 'v1');
            configurer.addResponseInterceptor(function (data, operation, what, url, response) {
                if (operation === "post") {
                    var location = response.headers('Location');
                    return location.substr(location.lastIndexOf('/') + 1);
                }
                return data;
            });
        }).all('housing-units');

        var service = {
            getHousingUnits: getHousingUnits,
            getOne: getOne,
            create: create,
            update: update,
            remove: remove,
            typologyLookup: typologyLookup
        };
        return service;

        ////////////////

        /**
         * @namespace Structure
         */

        /**
         * Housing unit page.
         *
         * @typedef {Object} Structure~HousingUnitPage
         * @property {number} number - The requested page number.
         * @property {number} size - The requested page size.
         * @property {number} totalElements - The total number of elements.
         * @property {number} totalPages - The total number of pages.
         * @property {Structure~HousingUnit[]} content - The page content.
         */

        /**
         * An housing unit.
         *
         * @typedef {Object} Structure~HousingUnit
         * @property {Number} id - The unique identifier.
         * @property {Number} housingUnitGroupId - The unique identifier of group.
         * @property {String} taxCode - The tax code.
         * @property {String} owner - The owner.
         * @property {Object} contacts - The contacts.
         * @property {Structure~Identification} - Housing unit identification.
         * @property {Structure~CadastralReference} - The cadastral reference.
         * @property {String} tipology - Kind.
         */

        /**
         * An identification.
         *
         * @typedef {Object} Structure~Identification
         * @property {String} floor - The floor.
         * @property {Boolean} middleFloor - True if middle floor.
         * @property {String} apartmentNumber - The apartment number.
         */

        /**
         * The cadastral reference.
         *
         * @typedef {Object} Structure~CadastralReference
         */

        /**
         * Retrieve a single page of housing units for given condominium id.
         *
         * @param {number} condominiumId - The unique company id.
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=200] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<Structure~HousingUnitPage|Error>} - The promise of page.
         */
        function getHousingUnits(condominiumId, page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                condominiumId: condominiumId,
                page: page || 0,
                size: size || 200,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return HousingUnits.get("", params);
        }

        /**
         * Retrieve a single housing unit.
         *
         * @param {Number} id - The unique id of housing unit to retrieve.
         * @returns {Promise<Structure~HousingUnit|Error>} - The promise of result.
         */
        function getOne(id) {
            return HousingUnits.one("", id).get();
        }

        /**
         * Creates a new housing unit.
         *
         * @param {Structure~HousingUnit} unit - The housing unit to create.
         * @returns {Promise<Object|Error>} - The promise of creation result.
         */
        function create(unit) {
            return HousingUnits.post(unit);
        }

        /**
         * Updates an existing housing unit.
         *
         * @param {Number} id - The unique id of housing unit to update.
         * @param {Structure~HousingUnit} group - The data to update unit with.
         * @returns {Promise<Object|Error>} - The promise of update result.
         */
        function update(id, unit) {
            return HousingUnits.one("", id).customPUT(unit);
        }

        /**
         * Removes an existing housing unit.
         *
         * @param {Number} id - The unique id of housing unit to delete
         * @returns {Promise<Object|Error>} - The promise of remove result.
         */
        function remove(id) {
            return HousingUnits.one("", id).remove();
        }

        /**
         * Return typology lookup.
         *
         * @param {Number} id - The unique id of housing unit to delete
         * @returns {Promise<Object|Error>} - The promise of remove result.
         */
        function typologyLookup() {
            return HousingUnits.all('lookup-housing-unit-typology').get("");
        }

    }

})();

