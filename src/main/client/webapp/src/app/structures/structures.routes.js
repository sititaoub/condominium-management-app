/*
 * structures.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .config(setupRoutes);

        setupRoutes.$inject = ['$stateProvider'];

        /* @ngInject */
        function setupRoutes ($stateProvider) {
            $stateProvider.state('index.condominiumStructure', {
                url: '/condominiums/{condominiumId}/structure',
                redirectTo: 'index.condominiumStructure.edit',
                component: 'structure',
                data: {
                    skipFinancialPeriodCheck: true
                },
                resolve: {
                    condominium: ['$transition$', 'CondominiumService', 'ManagersService', function ($transition$, CondominiumService, ManagersService) {
                        return CondominiumService.getOne($transition$.params().condominiumId).then(function (condominium) {
                            if (angular.isDefined(condominium.managerId) && condominium.managerId !== null) {
                                return ManagersService.getOne(condominium.managerId).then(function (manager) {
                                    condominium.manager = manager;
                                    return condominium;
                                });
                            }
                            return condominium;
                        });
                    }]
                }
            }).state('index.condominiumStructure.edit', {
                url: '/edit',
                component: 'structureEdit',
                resolve: {
                    tree: ['$transition$', 'StructureService', 'condominium',  function ($transition$, StructureService, condominium) {
                        return StructureService.getSimpleTree($transition$.params().condominiumId).then(function (tree) {
                            return {
                                condominiums: [{
                                    description: condominium.name,
                                    buildings: tree.buildings
                                }]
                            };
                        });
                    }]
                }
            }).state('index.condominiumStructure.edit.newBuilding', {
                url: '/building/create',
                component: 'buildingCreate'
            }).state('index.condominiumStructure.edit.building', {
                url: '/building/{buildingId}',
                component: 'buildingEdit',
                resolve: {
                    building: ['$transition$', 'BuildingService', function ($transition$, BuildingService) {
                        return BuildingService.getOne($transition$.params().buildingId);
                    }]
                }
            }).state('index.condominiumStructure.edit.building.newGroup', {
                url: '/group/create',
                views: {
                    '@index.condominiumStructure.edit': {
                        component: 'groupCreate'
                    }
                }
            }).state('index.condominiumStructure.edit.building.group', {
                url: '/group/{groupId}',
                views: {
                    '@index.condominiumStructure.edit': {
                        component: 'groupEdit'
                    }
                },
                resolve: {
                    group: ['$transition$', 'HousingUnitGroupService', function ($transition$, HousingUnitGroupService) {
                        return HousingUnitGroupService.getOne($transition$.params().groupId);
                    }]
                }
            }).state('index.condominiumStructure.edit.building.group.newHousingUnit', {
                url: '/housing-unit/create',
                views: {
                    '@index.condominiumStructure.edit': {
                        component: 'housingUnitCreate'
                    }
                }
            }).state('index.condominiumStructure.edit.building.group.housingUnit', {
                url: '/housing-unit/{housingUnitId}',
                views: {
                    '@index.condominiumStructure.edit': {
                        component: 'housingUnitEdit'
                    }
                },
                resolve: {
                    unit: ['$transition$', 'HousingUnitService', function ($transition$, HousingUnitService) {
                        return HousingUnitService.getOne($transition$.params().housingUnitId);
                    }]
                }
            }).state('index.condominiumStructure.edit.building.group.housingUnit.newRole', {
                url: '/role/create',
                views: {
                    '@index.condominiumStructure.edit': {
                        component: 'roleCreate'
                    }
                }
            }).state('index.condominiumStructure.edit.building.group.housingUnit.role', {
                url: '/role/{roleId}',
                views: {
                    '@index.condominiumStructure.edit': {
                        component: 'roleEdit'
                    }
                },
                resolve: {
                    role: ['$transition$', 'RoleService', function ($transition$, RoleService) {
                        return RoleService.getOne($transition$.params().roleId);
                    }]
                }
            });
        }

})();