/*
 * owners.constants.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .constant('dependentOwners', [
            ["PROPRIETARIO"],
            ["NUDO_PROPRIETARIO", "USUFRUTTUARIO"],
            ["UTILIZZATORE_LEASING", "LOCATORE_FINANZIARIO"],
            ["COND_RENT_TO_BUY", "PROP_REND_TO_BUY"],
            ["IACP", "ASSEGNATARIO_IACP"],
            ["COOP_ABITAZIONE", "ASSEGNATARIO_TERMINE"],
            ["COOP_ABITAZIONE", "ASSEGNATARIO_PROPDIF"]
        ]);
})();