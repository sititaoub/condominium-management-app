/*
 * owner-display.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('ownerDisplay', {
            controller: angular.noop,
            bindings: {
                'owner': '<'
            },
            templateUrl: 'app/structures/owners/owner-display.html'
        });

})();

