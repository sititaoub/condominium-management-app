/*
 * owner-assign.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('ownerAssign', {
            controller: OwnerAssignController,
            bindings: {
                condominium: '<',
                housingUnit: '<'
            },
            templateUrl: 'app/structures/owners/owner-assign.html'
        });

    OwnerAssignController.$inject = ['$state', '$q', 'OwnersService', 'NotifierService'];

    /* @ngInject */
    function OwnerAssignController($state, $q, OwnersService, NotifierService) {
        var $ctrl = this;
        var heldDown = false;
        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.owner = undefined;
        }

        function doSave() {
            if(heldDown == false) {
                return OwnersService.assign($ctrl.condominium.id, $ctrl.owner).then(function () {
                    heldDown = true;
                    return NotifierService.notifySuccess('Structures.Owners.Assign.Success');
                }).then(function () {
                    heldDown = true;
                    $state.go('^', undefined, { reload: true });
                }).catch(function (err) {
                    NotifierService.notifyError('Structures.Owners.Assign.Failure');
                    return $q.reject(err);
                })
            }
        }
    }

})();

