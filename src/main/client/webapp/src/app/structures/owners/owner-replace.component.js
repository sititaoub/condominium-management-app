/*
 * owner-replace.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('ownerReplace', {
            controller: OwnerReplaceController,
            bindings: {
                condominium: '<',
                owner: '<',
                housingUnit: '<'
            },
            templateUrl: 'app/structures/owners/owner-replace.html'
        });

    OwnerReplaceController.$inject = ['$state', '$q', 'OwnersService', 'NotifierService', 'moment', '_'];

    /* @ngInject */
    function OwnerReplaceController($state, $q, OwnersService, NotifierService, moment, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.replaceOwner = undefined;
            $ctrl.minDate = moment($ctrl.owner.from).add(1, 'day').toDate();
        }

        function doSave() {
            $ctrl.replaceOwner.previousOwners = _.map($ctrl.owner.roles, 'roleId');
            return OwnersService.replace($ctrl.condominium.id, $ctrl.replaceOwner).then(function () {
                return NotifierService.notifySuccess('Structures.Owners.Replace.Success');
            }).then(function () {
                $state.go('^', undefined, { reload: true });
            }).catch(function (err) {
                NotifierService.notifyError('Structures.Owners.Replace.Failure');
                return $q.reject(err);
            })
        }
    }

})();

