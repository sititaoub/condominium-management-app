/*
 * owner-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('ownerInput', {
            controller: OwnerInputController,
            bindings: {
                ngModel: '<',
                condominium: '<',
                housingUnitId: '<',
                minDate: '<?'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/structures/owners/owner-input.html'
        });

    OwnerInputController.$inject = ['$filter', 'dependentOwners', '_'];

    /* @ngInject */
    function OwnerInputController($filter, dependentOwners, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;
        $ctrl.onOwnerTypeChange = onOwnerTypeChange;
        $ctrl.onOwnerTypeChangeEdit = onOwnerTypeChangeEdit;
        ////////////

        function onInit() {
            var translate = $filter('translate');
            $ctrl.ownerType = undefined;
            $ctrl.dependendOwners = _.map(dependentOwners, function (owners) {
                return {
                    title: _.join(_.map(owners, function (role) {
                        return translate('Structures.Edit.Role.Type.' + role);
                    }), ' - '),
                    roles: owners
                }
            });
            $ctrl.fromOptions = {
                minDate: $ctrl.minDate
            };
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue || {
                    housingUnitId: $ctrl.housingUnitId,
                    from: new Date()
                };
            };

            if($ctrl.ngModel.roles[0]) {
                $ctrl.value = {
                    from :  new Date($ctrl.ngModel.from)
                }
                var rolesUpdate = [];
                _.forEach($ctrl.ngModel.roles, function(role) {
                    rolesUpdate.push(role.roleType);
                });
                var token = 0;
                _.forEach($ctrl.dependendOwners, function(owner,i) {
                    if(_.isEqual(rolesUpdate, owner.roles)) {
                        token = i;
                    }
                });
                $ctrl.ownerType =   $ctrl.dependendOwners[token];
                var roles = [];
                _.forEach($ctrl.ownerType.roles, function(role,i) {
                    var element = {};
                    element.role = role;
                    element.companyName = $ctrl.ngModel.roles[i].companyName == null ?
                        $ctrl.ngModel.roles[i].firstName + " "+ $ctrl.ngModel.roles[i].lastName
                    : $ctrl.ngModel.roles[i].companyName ;
                    roles.push({element: element});
                });
                onOwnerTypeChangeEdit(roles);
            }
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function onOwnerTypeChangeEdit(roles) {
            $ctrl.value.owners = _.map(roles, function (role) {
                return {
                    roleType: role.element.role,
                    person : {
                        companyName : role.element.companyName }
                };
            });
            onChange();
        }

        function onOwnerTypeChange() {
            $ctrl.value.owners = _.map($ctrl.ownerType.roles, function (role) {
                return {
                    roleType: role
                };
            });
            onChange();
        }
    }

})();

