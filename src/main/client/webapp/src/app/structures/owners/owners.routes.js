/*
 * structures.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .config(setupRoutes);

        setupRoutes.$inject = ['$stateProvider'];

        /* @ngInject */
        function setupRoutes ($stateProvider) {
            $stateProvider.state('index.condominiumStructure.owners', {
                url: '/owners',
                redirectTo: 'index.condominiumStructure.owners.list',
                template: '<div class="owners-container" ui-view></div>'
            }).state('index.condominiumStructure.owners.list', {
                url: '?page&count&sort',
                component: 'ownersList',
                params: {
                    page: { dynamic: true, squash: true, value: '1' },
                    count: { dynamic: true, squash: true, value: '10' },
                    sort: { dynamic: true, squash: true, value: 'name,asc' }
                },
                resolve: {
                    owners: ['condominium', 'OwnersService', function (condominium, OwnersService) {
                        return OwnersService.getAll(condominium.id);
                    }]
                }
            }).state('index.condominiumStructure.owners.list.assign', {
                url: '/{unitId}/assign',
                views: {
                    '@index.condominiumStructure.owners': {
                        component: 'ownerAssign'
                    }
                },
                resolve: {
                    housingUnit: ['$transition$', 'HousingUnitService', function ($transition$, HousingUnitService) {
                        return HousingUnitService.getOne($transition$.params().unitId);
                    }]
                }
            }).state('index.condominiumStructure.owners.edit', {
                url: '/{unitId}/edit',
                views: {
                    '@index.condominiumStructure.owners': {
                        component: 'ownerEdit'
                    }
                },
                resolve: {
                    housingUnit: ['$transition$', 'HousingUnitService', function ($transition$, HousingUnitService) {
                        return HousingUnitService.getOne($transition$.params().unitId);
                    }],
                    owner: ['condominium', '$transition$', 'OwnersService', function (condominium, $transition$, OwnersService) {
                        return OwnersService.getOne(condominium.id, $transition$.params().unitId)
                    }]
                }
            }).state('index.condominiumStructure.owners.list.replace', {
                url: '/{unitId}/replace',
                views: {
                    '@index.condominiumStructure.owners': {
                        component: 'ownerReplace'
                    }
                },
                resolve: {
                    housingUnit: ['$transition$', 'HousingUnitService', function ($transition$, HousingUnitService) {
                        return HousingUnitService.getOne($transition$.params().unitId);
                    }],
                    owner: ['condominium', '$transition$', 'OwnersService', function (condominium, $transition$, OwnersService) {
                        return OwnersService.getOne(condominium.id, $transition$.params().unitId)
                    }]
                }
            });
        }

})();