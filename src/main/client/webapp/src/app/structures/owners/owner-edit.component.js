/*
 * owner-assign.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('ownerEdit', {
            controller: OwnerEditController,
            bindings: {
                ngModel: '<',
                condominium: '<',
                housingUnit: '<',
                owner: '<'
            },
            templateUrl: 'app/structures/owners/owner-edit.html'
        });

    OwnerEditController.$inject = ['$state', '$q', 'OwnersService', 'NotifierService'];

    /* @ngInject */
    function OwnerEditController($state, $q, OwnersService, NotifierService) {
        var $ctrl = this;
        var heldDown = false;
        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;
        ////////////

        function onInit() {
            $ctrl.ngModel = $ctrl.owner;
        }

        function doSave() {
            if(heldDown == false) {
                $ctrl.owner.housingUnitId =  $ctrl.housingUnit.id;
                $ctrl.owner.owners[0].roleId = $ctrl.ngModel.roles[0].roleId;
                if(!$ctrl.owner.owners[0].person.hasOwnProperty("id")){
                    $ctrl.owner.owners[0].person.id = $ctrl.ngModel.roles[0].personId;
                }
                if($ctrl.owner.owners[1] && $ctrl.ngModel.roles[1]) {
                    $ctrl.owner.owners[1].roleId = $ctrl.ngModel.roles[1].roleId;
                    if(!$ctrl.owner.owners[1].person.hasOwnProperty("id")){
                        $ctrl.owner.owners[1].person.id = $ctrl.ngModel.roles[1].personId;
                    }
                }
                return OwnersService.update($ctrl.condominium.id, $ctrl.owner).then(function () {
                    heldDown = true;
                    return NotifierService.notifySuccess('Structures.Owners.Edit.Success');
                }).then(function () {
                    heldDown = true;
                    $state.go('^', undefined, { reload: true });
                }).catch(function (err) {
                    NotifierService.notifyError('Structures.Owners.Edit.Failure');
                    return $q.reject(err);
                })
            }
        }
    }

})();

