/*
 * owners-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('ownersList', {
            controller: OwnersListController,
            bindings: {
                condominium: '<',
                owners: '<'
            },
            templateUrl: 'app/structures/owners/owners-list.html'
        });

    OwnersListController.$inject = ['NgTableParams', 'OwnersService', '$uibModal', '_'];

    /* @ngInject */
    function OwnersListController(NgTableParams, OwnersService, $uibModal, _) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.toggleFilters = toggleFilters;
        $ctrl.getBuildings = getBuildings;
        $ctrl.getGroups = getGroups;
        $ctrl.showHistory = showHistory;

        ////////////

        function onInit() {
            $ctrl.showFilters = false;
            $ctrl.tableParams = new NgTableParams({
                page: 1,
                count: 10,
                sorting: { building: 'asc', group: 'asc', unit: 'asc' }
            }, {
                dataset: $ctrl.owners
            });
            $ctrl.noOwnersCount = _.filter($ctrl.owners, function (o) { return o.roles.length === 0 }).length;
        }

        function toggleFilters() {
            $ctrl.showFilters = !$ctrl.showFilters;
        }

        function getBuildings() {
            return _.sortBy(_.uniqBy(_.map($ctrl.owners, function (owner) {
                return {
                    id: owner.buildingId,
                    title: owner.building
                };
            }), 'id'), 'title');
        }

        function getGroups() {
            var owners = $ctrl.owners;
            var buildingId = $ctrl.tableParams.filter().buildingId;
            if (angular.isDefined(buildingId) && buildingId !== '') {
                owners = _.filter($ctrl.owners, { buildingId: buildingId });
            }
            return _.sortBy(_.uniqBy(_.map(owners, function (owner) {
                return {
                    id: owner.groupId,
                    title: owner.group,
                    buildingId: owner.buildingId,
                    fullTitle: owner.building + ' - ' + owner.group
                }
            }), 'id'), 'fullTitle');
        }

        function showHistory(housingUnitId) {
            $uibModal.open({
                component: 'ownersHistory',
                size: 'lg',
                resolve: {
                    condominium: _.constant($ctrl.condominium),
                    history: OwnersService.getHistory($ctrl.condominium.id, housingUnitId)
                }
            });
        }
    }

})();

