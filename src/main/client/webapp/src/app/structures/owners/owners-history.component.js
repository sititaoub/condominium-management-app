/*
 * owners-history.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('ownersHistory', {
            controller: OwnersHistoryController,
            bindings: {
                dismiss: '&',
                resolve: '<'
            },
            templateUrl: 'app/structures/owners/owners-history.html'
        });

    OwnersHistoryController.$inject = [];

    /* @ngInject */
    function OwnersHistoryController() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;

        ////////////

        function onInit() {
            $ctrl.condominium = $ctrl.resolve.condominium;
            $ctrl.history = $ctrl.resolve.history;
        }
    }

})();

