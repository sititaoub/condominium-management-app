/*
 * owners.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .factory('OwnersService', OwnersService);

    OwnersService.$inject = ['Restangular', 'contextAddressStructure', 'RoleService'];

    /* @ngInject */
    function OwnersService(Restangular, contextAddressStructure, RoleService) {
        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressStructure + 'v1');
        });
        var service = {
            getAll: getAll,
            getOne: getOne,
            getHistory: getHistory,
            assign: assign,
            replace: replace,
            update: update

        };
        return service;

        ////////////////

        function getAll(condominiumId) {
            return Api.one("condominiums", condominiumId).all("housing-unit").all("owners").getList();
        }

        function getOne(condominiumId, housingUnitId) {
            return Api.one("condominiums", condominiumId).one("housing-unit", housingUnitId).all("owner").get("");
        }

        function getHistory(condominiumId, housingUnitId) {
            return Api.one("condominiums", condominiumId).one("housing-unit", housingUnitId).all("owners").all("history").getList();
        }

        function assign(condominiumId, owner) {
            return Api.one("condominiums", condominiumId).one("housing-unit", owner.housingUnitId).all("owner").post(owner);
        }

        function replace(condominiumId, owner) {
            return Api.one("condominiums", condominiumId).one("housing-unit", owner.housingUnitId).one("owner", "takeover").customPUT(owner);
        }

        function update(condominiumId, owner) {
            return Api.one("condominiums", condominiumId).one("housing-unit", owner.housingUnitId).all("owner").customPUT(owner);
        }
    }

})();

