/*
 * group-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('groupEdit', {
            controller: StructureGroupEditController,
            bindings: {
                controller: '<',
                building: '<',
                group: '<',
                onGroupUpdated: '&'
            },
            templateUrl: 'app/structures/group-edit.html'
        });

    StructureGroupEditController.$inject = ['HousingUnitGroupService', 'NotifierService'];

    /* @ngInject */
    function StructureGroupEditController(HousingUnitGroupService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////////

        function onInit() {
            $ctrl.groupId = $ctrl.group.id;
        }

        function doSave() {
            HousingUnitGroupService.update($ctrl.groupId, $ctrl.group).then(function () {
                return NotifierService.notifySuccess('Structures.Edit.Group.UpdateSuccess');
            }).then(function () {
                $ctrl.onGroupUpdated({ group: angular.copy($ctrl.group) });
            }).catch(function () {
                return NotifierService.notifyError('Structures.Edit.Group.UpdateFailure');
            });
        }
    }

})();

