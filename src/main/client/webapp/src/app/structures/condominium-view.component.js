/*
 * condominium-view.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('condominiumView', {
            controller: angular.noop,
            bindings: {
                condominium: '<'
            },
            templateUrl: 'app/structures/condominium-view.html'
        });
})();

