/*
 * person-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.structures')
        .component('roleCreate', {
            controller: StructureRoleCreateController,
            bindings: {
                condominium: '<',
                building: '<',
                group: '<',
                unit: '<',
                onRoleAdded: '&'
            },
            templateUrl: 'app/structures/role-create.html'
        });

    StructureRoleCreateController.$inject = ['$state', 'RoleService', 'NotifierService'];

    /* @ngInject */
    function StructureRoleCreateController($state, RoleService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////////

        function onInit() {
            $ctrl.role = {
                condominiumId: $ctrl.condominium.id,
                housingUnitId: $ctrl.unit.id,
                rolePercentage: 100
            };
        }

        function doSave() {
            $ctrl.role.personId = $ctrl.role.person.id;
            RoleService.create($ctrl.role).then(function (id) {
                return NotifierService.notifySuccess('Structures.Edit.Role.CreateSuccess').then(function () {
                    $ctrl.role.id = id;
                    $ctrl.onRoleAdded({ role: angular.copy($ctrl.role) });
                    $state.go("^.role", { buildingId: $ctrl.building.id, groupId: $ctrl.group.id, housingUnitId: $ctrl.unit.id, roleId: id });
                });
            }).catch(function () {
                return NotifierService.notifyError('Structures.Edit.Role.CreateFailure');
            });
        }
    }

})();

