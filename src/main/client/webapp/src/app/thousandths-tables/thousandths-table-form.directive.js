/*
 * thousandths-table-form.directive.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.thousandthsTables')
        .directive('thousandthsTableForm', thousandthsTableForm);

    thousandthsTableForm.$inject = [];

    /* @ngInject */
    function thousandthsTableForm() {
        var directive = {
            bindToController: true,
            controller: ThousandthsTableFormController,
            controllerAs: 'vm',
            restrict: 'E',
            scope: {
                ngModel: '=',
                readOnly: '='
            },
            require: 'ngModel',
            templateUrl: 'app/thousandths-tables/thousandths-table-form.html'
        };
        return directive;
    }

    ThousandthsTableFormController.$inject = ['$q', '$filter', 'thousandthsTablesTypes', 'energyTypes',
        'NgTableParams', '_'];

    /* @ngInject */
    function ThousandthsTableFormController($q, $filter, thousandthsTablesTypes, energyTypes, NgTableParams, _) {
        var vm = this;

        vm.thousandthsTablesTypes = thousandthsTablesTypes;
        vm.energyTypes = _.map(energyTypes, function (el) {
            return {
                value: el.value,
                group: $filter('translate')('ThousandthsTables.Form.EnergyType.Groups.' + el.group)
            };
        });

        vm.calculateSum = calculateSum;
        vm.isCompetencePeriod = isCompetencePeriod;
        vm.isEnergyTable = isEnergyTable;
        vm.change = change;

        vm.readonly = false;

        activate();

        ////////////////

        function activate() {
            vm.buildings = _.sortBy(_.map(vm.ngModel.buildings, function (el) {
                return { id: el.description, title: el.description };
            }), 'title');
            vm.groups = _.sortBy(_.uniqBy(_.map(_.flatMap(vm.ngModel.buildings, 'groups'), function (el) {
                return { id: el.description, title: el.description };
            }), 'id'), 'title');
            vm.housingUnits = _.sortBy(_.uniqBy(_.map(_.flatMap(_.flatMap(vm.ngModel.buildings, 'groups'), 'housingUnits'), function (el) {
                return { id: el.description, title: el.description };
            }), 'id'), 'title');
            var applyFilter = false;
            var filter = {};
            if (angular.isDefined(vm.ngModel.buildingFilter)) {
                if (vm.ngModel.buildingFilter != null) {
                    applyFilter = true;
                    filter.building = vm.ngModel.buildingFilter;
                }
            }
            if (angular.isDefined(vm.ngModel.stairFilter)) {
                if (vm.ngModel.stairFilter != null) {
                    applyFilter = true;
                    filter.group = vm.ngModel.stairFilter;
                }
            }
            vm.tableParams = new NgTableParams(
                {
                    page: 1,
                    count: 10
                },
                {
                    filterOptions: { filterLayout: "horizontal" },
                    getData: generateData
                }
            );
            calculateSum();
            if (applyFilter) {
                angular.extend(vm.tableParams.filter(), filter);
            }
            vm.readonly = vm.readOnly;
        }

        function isEnergyTable() {
            return angular.isDefined(vm.ngModel) && vm.ngModel.thousandthType === 'ENERGY';
        }

        function isCompetencePeriod() {
            return angular.isDefined(vm.ngModel) && vm.ngModel.competenceType === 'PERIOD';
        }

        function calculateSum() {
            if (angular.isUndefined(vm.ngModel)) {
                return 0;
            }
            if (vm.ngModel.thousandthType === 'INDIVIDUAL') {
                vm.coefficientSum = _.sumBy(_.flatMap(_.flatMap(_.flatMap(vm.ngModel.buildings, 'groups'), 'housingUnits'), 'roles'), function (object) {
                    return object.coefficient == "" ? null : object.coefficient;
                });
            } else {
                vm.coefficientSum = _.sumBy(_.flatMap(_.flatMap(vm.ngModel.buildings, 'groups'), 'housingUnits'), function (object) {
                    return object.coefficient == "" ? null : object.coefficient;
                });
            }
        }

        function generateData(params) {
            if (angular.isUndefined(vm.ngModel)) {
                params.total(0);
                return $q.resolve([]);
            }
            var result = [];
            _.each(vm.ngModel.buildings, function (building) {
                _.each(building.groups, function (group) {
                    _.each(group.housingUnits, function (hu) {
                        hu.building = building.description;
                        hu.group = group.description;
                        if (vm.ngModel.thousandthType === 'INDIVIDUAL') {
                            _.each(hu.roles, function (role) {
                                //Group & Company management
                                if (!role.groupFlag && role.firstName && role.lastName)
                                    role.fullName = role.firstName + ' ' + role.lastName;
                                else
                                    role.fullName = role.companyName;
                                role.building = building.description;
                                role.group = group.description;
                                role.housingUnitDescription = hu.description;
                                result.push(role);
                            });
                        } else {
                            hu.housingUnitDescription = hu.description;
                            result.push(hu);
                        }
                    });
                });
            });
            var filters = params.filter();
            result = _.filter(result, function (el) {
                return _.reduce(filters, function (acc, value, key) {
                    return acc && _.startsWith(el[key], value);
                }, true);
            });
            var key = _.findKey(params.sorting());
            result = _.sortBy(result, key);
            if (params.sorting()[key] === 'desc') {
                result = _.reverse(result);
            }

            params.total(result.length);
            var start = (params.page() - 1) * params.count();
            var end = params.page() * params.count();
            return $q.resolve(_.slice(result, start, end));
        }

        function change(vm) {
            if (vm.$error.date != undefined) {
                vm.$error.date["0"].$error.date = false;
                vm.$error.date["0"].$error.customDate = true;
            } else {
                vm.$setPristine();
            }
        }

    }

})();
