/*
 * thousandths-table-create.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.thousandthsTables')
        .controller('ThousandthsTableCreateController', ThousandthsTableCreateController);

    ThousandthsTableCreateController.$inject = ['$state', 'ThousandthsTablesService', 'NotifierService'];

    /* @ngInject */
    function ThousandthsTableCreateController($state, ThousandthsTablesService, NotifierService) {
        var vm = this;

        vm.doSave = doSave;

        activate();

        ////////////////

        function activate() {
            vm.loading = true;
            createEmpty();
        }

        function isEnergyTable() {
            return vm.table.thousandthType === 'ENERGY';
        }

        function doSave() {
            if (!isEnergyTable()) {
                vm.table.energyDistributionColumn = undefined;
            }
            if (angular.isDefined(vm.form["vm.tableForm"].building)) {
                vm.table.buildingFilter = vm.form["vm.tableForm"].building.$viewValue;
            }
            if (angular.isDefined(vm.form["vm.tableForm"].group)) {
                vm.table.stairFilter = vm.form["vm.tableForm"].group.$viewValue;
            }
            ThousandthsTablesService.create(vm.table).then(function () {
                return NotifierService.notifySuccess('ThousandthsTables.Create.Success');
            }).then(function () {
                $state.go('^');
            }).catch(function () {
                NotifierService.notifyError('ThousandthsTables.Create.Failure');
            });
        }

        function createEmpty() {
            ThousandthsTablesService.getEmpty($state.params.condominiumId).then(function (table) {
                vm.table = table;
                vm.loading = false;
            }).catch(function () {
                NotifierService.notifyError().then(function () {
                    $state.go('^');
                });
            });
        }
    }

})();

