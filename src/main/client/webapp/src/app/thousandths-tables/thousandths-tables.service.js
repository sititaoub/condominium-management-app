/*
 * thousandths-tables.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.thousandthsTables')
        .factory('ThousandthsTablesService', ThousandthsTablesService);

    ThousandthsTablesService.$inject = ['Restangular', 'contextAddressManagement', '_', '$window'];

    /* @ngInject */
    function ThousandthsTablesService(Restangular, contextAddressManagement, _, $window) {
        var ThousandthsTables = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressManagement + 'v1');
        }).all('thousandth-tables');

        var readOnly = false;

        var service = {
            getPage: getPage,
            getOne: getOne,
            getEmpty: getEmpty,
            create: create,
            update: update,
            remove: remove,
            getReport: getReport,
            setReadOnly: setReadOnly,
            getReadOnly: getReadOnly
        };
        return service;

        ////////////////

        /**
         * @namespace ThousandthsTables
         */

        /**
         * Persons page.
         *
         * @typedef {Object} ThousandthsTables~ThousandthsTablesPage
         * @property {number} number - The requested page number.
         * @property {number} size - The requested page size.
         * @property {number} totalElements - The total number of elements.
         * @property {number} totalPages - The total number of pages.
         * @property {ThousandthsTables~ThousandthsTable[]} content - The page content.
         */

        /**
         * A thousandths table.
         *
         * @typedef {Object} ThousandthsTables~ThousandthsTable
         * @property {Number} id - The unique id of table.
         * @property {Number} condominiumId - The unique id of condominium.
         * @property {String} description - The table description.
         * @property {String} thousandthType - The kind of table.
         * @property {String} energyDistributionColumn - The kind of energy distribution (valid only if table is energy).
         * @property {String} competenceType - The kind of competence of this table.
         * @property {Date} firstCompentencePeriodFrom - Starting date of competence period.
         * @property {Date} firstCompentencePeriodTo - Ending date of competence period.
         * @property {Date} secondCompentencePeriodFrom - Starting date of second competence period
         * @property {Date} secondCompentencePeriodTo - Ending date of second competence period.
         * @property {ThousandthsTables~Group[]} groups - The groups of this table.
         */

        /**
         * Retrieve a single page of person for given condominium id.
         *
         * @param {number} condominiumId - The unique id of condominium.
         * @param {number} [financialPeriodId] - The unique id of financial period.
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=10] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<ThousandthsTables~ThousandthsTablesPage|Error>} - The promise of page.
         */
        function getPage(condominiumId, financialPeriodId, page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                condominiumId: condominiumId,
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            var Api = ThousandthsTables;
            if (angular.isDefined(financialPeriodId)) {
                return Api.one('financial-periods', financialPeriodId).get(params);
            }
            return Api.get('', params);
        }

        /**
         * Retrieve a single financial period.
         *
         * @param {number} id - The unique id of thousandths table period.
         * @returns {Promise<ThousandthsTables~ThousandthsTable|Error>} - The promise over the thousandths table.
         */
        function getOne(id) {
            return ThousandthsTables.one("", id).get().then(function (table) {
                _.each(['firstCompentencePeriodFrom', 'firstCompentencePeriodTo', 'secondCompentencePeriodFrom', 'secondCompentencePeriodTo'], function (el) {
                    if (table[el]) {
                        table[el] = new Date(table[el]);
                    }
                });

                return table;
            });
        }

        /**
         * Retrieve an empty object for supplied condominium.
         *
         * @param {Number} condominiumId - The unique id of condominium
         * @returns {Promise<ThousandthsTables~ThousandthsTable|Error>} - The promise over the thousandths table.
         */
        function getEmpty(condominiumId) {
            return ThousandthsTables.one('new-by-condominuim', condominiumId).get();
        }

        /**
         * Create a new financial period.
         *
         * @param {ThousandthsTables~ThousandthsTable} table - The table to create.
         * @returns {Promise<Number|Error>} - The promise over the create operation result.
         */
        function create(table) {
            return ThousandthsTables.post(table);
        }

        /**
         * Updates an existing table.
         *
         * @param {number} id - The unique id
         * @param {ThousandthsTables~ThousandthsTable} table - The table to update.
         * @returns {Promise<Object|Error>} - The promise over the create operation result.
         */
        function update(financialPeriodId, condominiumId, id, table) {
            return ThousandthsTables.one("financial-periods", financialPeriodId).one("condominiumId", condominiumId).one("", id).doPUT(table);
        }

        /**
         * Remove an existing table.
         *
         * @param {number} id - The unique id of table.
         * @returns {Promise<Object|Error>} - The promise over the remove operation result.
         */
        function remove(financialPeriodId, condominiumId, id) {
            return ThousandthsTables.one("financial-periods", financialPeriodId).one("condominiumId", condominiumId).one("", id).remove();
        }

        /**
         *  call end-point for download report
         * possible value of options parameters are: 
         * - condominiumId(Mandatory)
         * @param {*} options  - options requested for print possible parameters are:
         */
        function getReport(options) {
            var url = ThousandthsTables.all("reports").getRequestedUrl();
            url = url + "/financial-periods/" + options.financialPeriodId +  "?condominiumId=" + options.condominiumId + "&format=" + options.outputFormat;
            $window.open(url, "_blank");
        }

        function setReadOnly(value) {
            readOnly = value;
        }

        function getReadOnly() {
            return readOnly;
        }

    }

})();
