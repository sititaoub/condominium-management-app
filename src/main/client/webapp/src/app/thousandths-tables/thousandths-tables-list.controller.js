/*
 * thousandths-tables-list.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.thousandthsTables')
        .controller('ThousandthsTablesListController', ThousandthsTablesListController);

    ThousandthsTablesListController.$inject = ['$state', 'NgTableParams', 'thousandthsTablesTypes',
        'ThousandthsTablesService', 'NotifierService', 'AlertService', 'FinancialPeriodService'];

    /* @ngInject */
    function ThousandthsTablesListController($state, NgTableParams, thousandthsTablesTypes, ThousandthsTablesService,
        NotifierService, AlertService, FinancialPeriodService) {
        var vm = this;

        vm.thousandthsTablesTypes = thousandthsTablesTypes;

        vm.toggleFilters = toggleFilters;
        vm.removeTable = removeTable;
        vm.doPrintConfirm = doPrintConfirm;

        vm.readOnly = false;

        activate();

        ////////////////

        function activate() {
            vm.loading = true;
            vm.firstLoad = true;
            vm.showFilters = false;
            vm.financialPeriod = FinancialPeriodService.getCurrent($state.params.companyId, $state.params.condominiumId);
            if (angular.isDefined(vm.financialPeriod)) {
                if (vm.financialPeriod.status == "CONSOLIDATED") {
                    vm.readOnly = true;
                } else {
                    vm.readOnly = false;
                }
                ThousandthsTablesService.setReadOnly(vm.readOnly);
            }
            vm.tableParams = NgTableParams.fromUiStateParams($state.params,
                {
                    page: 1,
                    count: 10
                },
                {
                    getData: loadThousandthsTables
                }
            );
        }

        function toggleFilters() {
            vm.showFilters = !vm.showFilters;
        }

        /**
         * Remove the supplied table.
         *
         * @param {ThousandthsTables~ThousandthsTable} table - The table to remove.
         */
        function removeTable(table) {
            AlertService.askConfirm('ThousandthsTables.Remove.Message', table).then(function () {
                FinancialPeriodService.selectCurrent($state.params.companyId, $state.params.condominiumId).then(function (data) {
                    ThousandthsTablesService.remove(data.id, $state.params.condominiumId, table.id).then(function () {
                        return NotifierService.notifySuccess('ThousandthsTables.Remove.Success');
                    }).catch(function () {
                        return NotifierService.notifyError('ThousandthsTables.Remove.Failure');
                    }).finally(function () {
                        vm.tableParams.reload();
                    });
                }).catch(function () {
                    NotifierService.notifyError('ThousandthsTables.Get.Fail');
                });
            });
        }

        function loadThousandthsTables(params) {
            if (!vm.firstLoad) {
                $state.go('.', params.uiRouterUrl(), { notify: false, inherit: false });
            }
            return ThousandthsTablesService.getPage($state.params.condominiumId, vm.financialPeriod.id, params.page() - 1, params.count(), params.sorting(), params.filter()).then(function (page) {
                vm.firstLoad = false;
                params.total(page.totalElements);
                return page.content;
            }).catch(function () {
                return NotifierService.notifyError();
            }).finally(function () {
                vm.loading = false;
            });
        }

        function doPrintConfirm(outputFormat) {
            var param = {};
            param.condominiumId = $state.params.condominiumId;
            param.financialPeriodId = vm.financialPeriod.id;
            param.outputFormat=outputFormat;
            ThousandthsTablesService.getReport(param);
        }
    }

})();
