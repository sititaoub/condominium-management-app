/*
 * thousandths-table-edit.controller.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.thousandthsTables')
        .controller('ThousandthsTableEditController', ThousandthsTableEditController);

    ThousandthsTableEditController.$inject = ['$state', 'ThousandthsTablesService', 'NotifierService', 'FinancialPeriodService'];

    /* @ngInject */
    function ThousandthsTableEditController($state, ThousandthsTablesService, NotifierService, FinancialPeriodService) {
        var vm = this;

        vm.doSave = doSave;
        vm.readOnly = false;

        activate();

        ////////////////

        function activate() {
            vm.loading = true;
            vm.readOnly = ThousandthsTablesService.getReadOnly();
            loadTable();
        }

        function doSave() {
            if (angular.isDefined(vm.form["vm.tableForm"].building)) {
                vm.table.buildingFilter = vm.form["vm.tableForm"].building.$viewValue;
            }
            if (angular.isDefined(vm.form["vm.tableForm"].group)) {
                vm.table.stairFilter = vm.form["vm.tableForm"].group.$viewValue;
            }
            var data = FinancialPeriodService.getCurrent($state.params.companyId, $state.params.condominiumId);
            if( data !== undefined ) {
                ThousandthsTablesService.update(data.id, $state.params.condominiumId, $state.params.tableId, vm.table).then(function () {
                    return NotifierService.notifySuccess('ThousandthsTables.Edit.Success');
                }).then(function () {
                    $state.go('^');
                }).catch(function () {
                    NotifierService.notifyError('ThousandthsTables.Edit.Failure');
                });
            } else {
                NotifierService.notifyError('ThousandthsTables.Get.Fail');
            }
        }

        function loadTable() {
            ThousandthsTablesService.getOne($state.params.tableId).then(function (table) {
                vm.table = table;
                vm.loading = false;
            }).catch(function () {
                NotifierService.notifyError().then(function () {
                    $state.go('^');
                });
            });
        }
    }

})();
