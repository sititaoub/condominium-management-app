/*
 * thousandths-tables.constants.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.thousandthsTables')
        .constant('thousandthsTablesTypes', {
            THOUSANDTH: 'THOUSANDTH',
            ENERGY: 'ENERGY',
            INDIVIDUAL: 'INDIVIDUAL',
            EQUALS_PART: 'EQUALS_PART'
        })
        .constant('energyTypes', [{
                value: 'VOLUNTARY_INVOLUNTARY',
                group: 'HEATING'
            }, {
                value: 'VOLUNTARY',
                group: 'HEATING'
            }, {
                value: 'INVOLUNTARY',
                group: 'HEATING'
            }, {
                value: 'CONSUMPTION',
                group: 'WATER'
            }
        ]);

})();