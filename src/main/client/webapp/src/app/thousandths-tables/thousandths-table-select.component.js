/*
 * thousandths-table-select.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.thousandthsTables')
        .component('thousandthsTableSelect', {
            controller: ThousandthsTableSelectController,
            bindings: {
                id: '@?',
                name: '@?',
                controlClass: '@?',
                ngModel: '<',
                ngRequired: '<?',
                ngDisabled: '<?',
                condominiumId: '<',
                financialPeriodId: '<?',
                propertyOnly: '<?'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/thousandths-tables/thousandths-table-select.html'
        });

    ThousandthsTableSelectController.$inject = ['ThousandthsTablesService'];

    /* @ngInject */
    function ThousandthsTableSelectController(ThousandthsTablesService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
            loadTables();
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function loadTables() {
            var filter = {};
            if ($ctrl.propertyOnly) {
                filter.thousandthType = ['THOUSANDTH', 'EQUALS_PART']
            }
            ThousandthsTablesService.getPage($ctrl.condominiumId, $ctrl.financialPeriodId, 0, 1000, { description: 'asc' }, filter).then(function (page) {
                $ctrl.tables = page.content;
            });
        }
    }

})();

