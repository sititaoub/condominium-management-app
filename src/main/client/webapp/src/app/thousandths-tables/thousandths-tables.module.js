/*
 * thousands-tables.module.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.thousandthsTables', [
            'condominiumManagementApp.core',
            'condominiumManagementApp.commons'
        ]);

})();