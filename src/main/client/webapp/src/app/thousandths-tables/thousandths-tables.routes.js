/*
 * thousands-tables.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.thousandthsTables')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider'];

    /* @ngInject */
    function setupRoutes($stateProvider) {
        $stateProvider.state('index.thousandths-tables', {
            url: '/condominiums/{condominiumId}/thousandths-tables',
            abstract: true,
            template: '<div class="thousands-tables-container"><ui-view></ui-view></div>',
            data: {
                skipFinancialPeriodCheck: false
            }
        }).state('index.thousandths-tables.list', {
            url: '?page&count&sort&description&thousandthType',
            controller: 'ThousandthsTablesListController',
            controllerAs: 'vm',
            templateUrl: 'app/thousandths-tables/thousandths-tables-list.html'
        }).state('index.thousandths-tables.list.create', {
            url: '/create',
            views: {
                '@index.thousandths-tables': {
                    controller: 'ThousandthsTableCreateController',
                    controllerAs: 'vm',
                    templateUrl: 'app/thousandths-tables/thousandths-table-create.html'
                }
            }
        }).state('index.thousandths-tables.list.edit', {
            url: '/{tableId}/edit',
            views: {
                '@index.thousandths-tables': {
                    controller: 'ThousandthsTableEditController',
                    controllerAs: 'vm',
                    templateUrl: 'app/thousandths-tables/thousandths-table-edit.html'
                }
            }
        });
    }
})();