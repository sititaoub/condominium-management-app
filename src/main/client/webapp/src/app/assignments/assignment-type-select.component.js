/*
 * assignment-type-select.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.assignments')
        .component('assignmentTypeSelect', {
            controller: AssignmentTypeSelectController,
            bindings: {
                id: '@',
                name: '@',
                class: '@',
                ngModel: '<',
                ngRequired: '<',
                ngDisabled: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/assignments/assignment-type-select.html'
        });

    AssignmentTypeSelectController.$inject = ['AssignmentTypeService'];

    /* @ngInject */
    function AssignmentTypeSelectController(AssignmentTypeService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
            AssignmentTypeService.getPage(0, 1000).then(function (page) {
                $ctrl.assignmentTypes = page.content;
            });
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue($ctrl.value);
        }
    }

})();

