/*
 * assignments.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.assignments')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider'];

    /* @ngInject */
    function setupRoutes ($stateProvider) {
        $stateProvider.state('index.assignmentsTypes', {
            url: '/assignments-types',
            redirectTo: 'index.assignmentsTypes.list',
            template: '<div class="assignments-wrapper" ui-view></div>',
            data: {
                skipFinancialPeriodCheck: true
            }
        }).state('index.assignmentsTypes.list', {
            url: '?page&count&sort&name',
            component: 'assignmentTypesList',
            params: {
                page: { dynamic: true, squash: true, value: '1', inherits: true },
                count: { dynamic: true, squash: true, value: '10', inherits: true },
                sort: { dynamic: true, squash: true, value: 'name,asc', inherits: true },
                name: { dynamic: true, squash: true }
            }
        }).state('index.assignmentsTypes.list.create', {
            url: '/create',
            views: {
                '@index.assignmentsTypes': {
                    component: 'assignmentTypeCreate'
                }
            }
        }).state('index.assignmentsTypes.list.edit', {
            url: '/{typeId}/edit',
            views: {
                '@index.assignmentsTypes': {
                    component: 'assignmentTypeEdit'
                }
            },
            resolve: {
                assignmentType: ['$transition$', 'AssignmentTypeService', function ($transition$, AssignmentTypeService) {
                    return AssignmentTypeService.getOne($transition$.params().typeId);
                }]
            }
        }).state('index.assignments', {
            url: '/condominiums/{condominiumId}/assignments',
            redirectTo: 'index.assignments.list',
            template: '<div class="assignments-wrapper" ui-view></div>',
            data: {
                skipFinancialPeriodCheck: true
            },
            resolve: {
                condominium: ['$transition$', 'CondominiumService', function ($transition$, CondominiumService) {
                    return CondominiumService.getOne($transition$.params().condominiumId);
                }]
            }
        }).state('index.assignments.list', {
            url: '?a_page&a_count&a_sort&a_personId&a_assignmentTypeId&a_active',
            component: 'assignmentsList',
            params: {
                a_page: { dynamic: true, squash: true, value: '1', inherits: true },
                a_count: { dynamic: true, squash: true, value: '10', inherits: true },
                a_sort: { dynamic: true, squash: true, value: 'personId,asc', inherits: true },
                a_personId: { dynamic: true, squash: true },
                a_assignmentTypeId: { dynamic: true, squash: true },
                a_active: { dynamic: true, squash: true }
            }
        }).state('index.assignments.list.create', {
            url: '/create',
            views: {
                '@index.assignments': {
                    component: 'assignmentCreate'
                }
            }
        }).state('index.assignments.list.edit', {
            url: '/{assignmentId}/edit',
            views: {
                '@index.assignments': {
                    component: 'assignmentEdit'
                }
            },
            resolve: {
                assignment: ['$transition$', 'AssignmentService', function ($transition$, AssignmentService) {
                    return AssignmentService.getOne($transition$.params().condominiumId, $transition$.params().assignmentId);
                }]
            }
        }).state('index.assignments.list.terminate', {
            url: '/{assignmentId}/terminate',
            views: {
                '@index.assignments': {
                    component: 'assignmentTerminate'
                }
            },
            resolve: {
                assignment: ['$transition$', 'AssignmentService', function ($transition$, AssignmentService) {
                    return AssignmentService.getOne($transition$.params().condominiumId, $transition$.params().assignmentId);
                }]
            }
        });
    }

})();