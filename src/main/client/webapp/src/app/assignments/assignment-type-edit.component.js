/*
 * assignment-type-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.assignments')
        .component('assignmentTypeEdit', {
            controller: AssignmentTypeEditController,
            bindings: {
                'assignmentType': '<'
            },
            templateUrl: 'app/assignments/assignment-type-edit.html'
        });

    AssignmentTypeEditController.$inject = ['$state', 'AssignmentTypeService', 'NotifierService'];

    /* @ngInject */
    function AssignmentTypeEditController($state, AssignmentTypeService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.assignmentTypeId = $ctrl.assignmentType.id;
        }

        function doSave() {
            AssignmentTypeService.update($ctrl.assignmentTypeId, $ctrl.assignmentType).then(function () {
                return NotifierService.notifySuccess('Assignments.Types.Edit.Success');
            }).then(function () {
                $state.go("^");
            }).catch(function () {
                return NotifierService.notifyError('Assignments.Types.Edit.Failure');
            });
        }
    }

})();

