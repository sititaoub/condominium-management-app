/*
 * assignment.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.assignments')
        .factory('AssignmentService', AssignmentService);

    AssignmentService.$inject = ['Restangular', 'contextAddressPeople', '_'];

    /* @ngInject */
    function AssignmentService(Restangular, contextAddressPeople, _) {
        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressPeople + 'v1');
            configurer.addResponseInterceptor(function (data, operation, what, url, response) {
                if (operation === "post") {
                    var location = response.headers('Location');
                    return location.substr(location.lastIndexOf('/') + 1);
                }
                return data;
            });
        });
        var service = {
            getPage: getPage,
            getOne: getOne,
            create: create,
            update: update,
            terminate: terminate,
            remove: remove
        };
        return service;

        ////////////////

        /**
         * @namespace Assignments
         */

        /**
         * Assignments page.
         *
         * @typedef {Object} Assignments~AssignmentsPage
         * @property {number} number - The requested page number.
         * @property {number} size - The requested page size.
         * @property {number} totalElements - The total number of elements.
         * @property {number} totalPages - The total number of pages.
         * @property {Assignments~Assignment[]} content - The page content.
         */

        /**
         * Assignment object.
         *
         * @typedef {Object} Assignments~Assignment
         * @property {number} id - The unique id of type.
         * @property {Persons~Person} person - The the persons.
         * @property {Assignments~AssignmentType} assignmentType - The assignment type.
         * @property {string} fromDate - The assignment starting date
         * @property {string} toDate - The assignment ending date
         */

        /**
         * Retrieve a single page of assignment types..
         *
         * @param {number} condominiumId - The condominium id.
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=10] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<Assignments~AssignmentPage|Error>} - The promise of page.
         */
        function getPage(condominiumId, page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return Api.one('condominiums', condominiumId).all('assignments').get("", params);
        }

        /**
         * Retrieve a single assignment.
         *
         * @param {number} condominiumId - The condominium id.
         * @param {number} id - The unique id of assignment.
         * @returns {Promise<Assignments~Assignment|Error>} - The promise of assignment.
         */
        function getOne(condominiumId, id) {
            return Api.one('condominiums', condominiumId).one('assignments', id).get().then(function (item) {
                if (angular.isDefined(item.fromDate) && item.fromDate !== null) {
                    item.fromDate = new Date(item.fromDate);
                }
                if (angular.isDefined(item.toDate) && item.toDate !== null) {
                    item.toDate = new Date(item.toDate);
                }
                return item;
            });
        }

        /**
         * Creates a single assignment type.
         *
         * @param {number} condominiumId - The condominium id.
         * @param {Assignments~Assignment} assignment - The assignment to create.
         * @returns {Promise<Number|Error>} - The promise of unique id of newly created assignment type.
         */
        function create(condominiumId, assignment) {
            return Api.one('condominiums', condominiumId).all('assignments').post({
                personId: assignment.person.id,
                assignmentTypeId: assignment.assignmentType.id,
                fromDate: assignment.fromDate,
                toDate: assignment.toDate
            });
        }

        /**
         * Updates a single assignment.
         *
         * @param {number} condominiumId - The condominium id.
         * @param {number} id - The unique id of assignment.
         * @param {Assignments~Assignment} assignment - The assignment to update.
         * @returns {Promise<Void|Error>} - The promise of operation outcome.
         */
        function update(condominiumId, id, assignment) {
            return Api.one('condominiums', condominiumId).one('assignments', id).all('starting-date').customPUT({
                fromDate: assignment.fromDate
            });
        }

        /**
         * Updates a single assignment ending date
         *
         * @param {number} condominiumId - The condominium id.
         * @param {number} id - The unique id of assignment.
         * @param {Assignments~Assignment} assignment - The assignment to update.
         * @returns {Promise<Void|Error>} - The promise of operation outcome.
         */
        function terminate(condominiumId, id, assignment) {
            return Api.one('condominiums', condominiumId).one('assignments', id).all('end-date').customPUT({
                toDate: assignment.toDate
            });
        }

        /**
         * Remove the assignment with supplied id.
         *
         * @param {number} condominiumId - The condominium id.
         * @param {number} id - The unique id of assignment type
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function remove(condominiumId, id) {
            return Api.one('condominiums', condominiumId).one('assignments', id).remove();
        }
    }

})();

