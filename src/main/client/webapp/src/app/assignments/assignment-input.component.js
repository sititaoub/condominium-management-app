/*
 * assignment-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.assignments')
        .component('assignmentInput', {
            controller: AssignmentInputController,
            bindings: {
                ngModel: '<',
                condominiumId: '<',
                terminate: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/assignments/assignment-input.html'
        });

    AssignmentInputController.$inject = [];

    /* @ngInject */
    function AssignmentInputController() {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
                $ctrl.edit = angular.isDefined($ctrl.value.id);
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }
    }

})();

