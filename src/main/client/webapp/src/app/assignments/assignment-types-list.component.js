/*
 * assignment-types-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.assignments')
        .component('assignmentTypesList', {
            controller: AssignmentTypesListController,
            bindings: {},
            templateUrl: 'app/assignments/assignment-types-list.html'
        });

    AssignmentTypesListController.$inject = ['$state', 'NgTableParams', 'AssignmentTypeService', 'NotifierService', 'AlertService'];

    /* @ngInject */
    function AssignmentTypesListController($state, NgTableParams, AssignmentTypeService, NotifierService, AlertService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.toggleFilters = toggleFilters;
        $ctrl.deleteAssignmentType = deleteAssignmentType;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10,
                sort: 'name,asc'
            }, {
                getData: loadAssignmentTypes
            });
            $ctrl.showFilters = $ctrl.tableParams.hasFilter();
        }

        function toggleFilters() {
            $ctrl.showFilters = !$ctrl.showFilters;
        }

        function deleteAssignmentType(assignmentType) {
            AlertService.askConfirm('Assignments.Types.Remove.Message', assignmentType).then(function () {
                AssignmentTypeService.remove(assignmentType.id).then(function () {
                    return NotifierService.notifySuccess('Assignments.Types.Remove.Success');
                }).catch(function () {
                    return NotifierService.notifyError('Assignments.Types.Remove.Failure');
                }).finally(function () {
                    $ctrl.tableParams.reload();
                })
            });
        }

        function loadAssignmentTypes(params) {
            $state.go('.', params.uiRouterUrl(), { inherit: false });
            return AssignmentTypeService.getPage(params.page() - 1, params.count(), params.sorting(), params.filter())
                .then(function (data) {
                    params.total(data.totalElements);
                    return data.content;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    $ctrl.loading = false;
                });
        }
    }

})();

