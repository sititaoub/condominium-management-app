/*
 * assignment-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.assignments')
        .component('assignmentEdit', {
            controller: AssignmentEditController,
            bindings: {
                'assignment': '<'
            },
            templateUrl: 'app/assignments/assignment-edit.html'
        });

    AssignmentEditController.$inject = ['$state', 'AssignmentService', 'NotifierService'];

    /* @ngInject */
    function AssignmentEditController($state, AssignmentService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.condominiumId = $state.params.condominiumId;
            $ctrl.assignmentId = $ctrl.assignment.id;
        }

        function doSave() {
            AssignmentService.update($ctrl.condominiumId, $ctrl.assignmentId, $ctrl.assignment).then(function () {
                return NotifierService.notifySuccess('Assignments.Edit.Success');
            }).then(function () {
                $state.go("^");
            }).catch(function () {
                return NotifierService.notifyError('Assignments.Edit.Failure');
            });
        }
    }

})();

