/*
 * assignment-type-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.assignments')
        .component('assignmentTypeCreate', {
            controller: AssignmentTypeCreateController,
            bindings: {},
            templateUrl: 'app/assignments/assignment-type-create.html'
        });

    AssignmentTypeCreateController.$inject = ['$state', 'AssignmentTypeService', 'NotifierService'];

    /* @ngInject */
    function AssignmentTypeCreateController($state, AssignmentTypeService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.assignmentType = {};
        }

        function doSave() {
            AssignmentTypeService.create($ctrl.assignmentType).then(function () {
                return NotifierService.notifySuccess('Assignments.Types.Create.Success');
            }).then(function () {
                $state.go("^");
            }).catch(function () {
                return NotifierService.notifyError('Assignments.Types.Create.Failure');
            });
        }
    }

})();

