/*
 * assignments-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.assignments')
        .component('assignmentsList', {
            controller: AssignmentsListController,
            bindings: {
                'condominium': '<'
            },
            templateUrl: 'app/assignments/assignments-list.html'
        });

    AssignmentsListController.$inject = ['$state', 'NgTableParams', 'AssignmentService', 'NotifierService', 'AlertService'];

    /* @ngInject */
    function AssignmentsListController($state, NgTableParams, AssignmentService, NotifierService, AlertService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.toggleFilters = toggleFilters;
        $ctrl.deleteAssignment = deleteAssignment;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10,
                sort: 'fromDate,asc'
            }, {
                getData: loadAssignments
            }, 'a_');
            $ctrl.showFilters = $ctrl.tableParams.hasFilter();
        }

        function toggleFilters() {
            $ctrl.showFilters = !$ctrl.showFilters;
        }

        function deleteAssignment(assignment) {
            AlertService.askConfirm('Assignments.Remove.Message', assignment).then(function () {
                AssignmentService.remove($ctrl.condominium.id, assignment.id).then(function () {
                    return NotifierService.notifySuccess('Assignments.Remove.Success');
                }).catch(function () {
                    return NotifierService.notifyError('Assignments.Remove.Failure');
                }).finally(function () {
                    $ctrl.tableParams.reload();
                })
            });
        }

        function loadAssignments(params) {
            $state.go('.', params.uiRouterUrl('a_'), { inherit: false });
            return AssignmentService.getPage($ctrl.condominium.id, params.page() - 1, params.count(), params.sorting(), params.filter())
                .then(function (data) {
                    params.total(data.totalElements);
                    return data.content;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    $ctrl.loading = false;
                });
        }
    }

})();

