/*
 * assignment-type.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.assignments')
        .factory('AssignmentTypeService', AssignmentTypeService);

    AssignmentTypeService.$inject = ['Restangular', 'contextAddressPeople', '_'];

    /* @ngInject */
    function AssignmentTypeService(Restangular, contextAddressPeople, _) {
        var AssignmentTypes = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressPeople + 'v1');
            configurer.addResponseInterceptor(function (data, operation, what, url, response) {
                if (operation === "post") {
                    var location = response.headers('Location');
                    return location.substr(location.lastIndexOf('/') + 1);
                }
                return data;
            });
        }).all('assignments').all('types');
        var service = {
            getPage: getPage,
            getOne: getOne,
            create: create,
            update: update,
            remove: remove
        };
        return service;

        ////////////////

        /**
         * @namespace Assignments
         */

        /**
         * AssignmentType page.
         *
         * @typedef {Object} Assignments~AssignmentTypePage
         * @property {number} number - The requested page number.
         * @property {number} size - The requested page size.
         * @property {number} totalElements - The total number of elements.
         * @property {number} totalPages - The total number of pages.
         * @property {Assignments~AssignmentType[]} content - The page content.
         */

        /**
         * AssignmentType object.
         *
         * @typedef {Object} Assignments~AssignmentType
         * @property {number} id - The unique id of type.
         * @property {string} name - The type name.
         */

        /**
         * Retrieve a single page of assignment types..
         *
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=10] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<Assignments~AssignmentTypePage|Error>} - The promise of page.
         */
        function getPage(page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return AssignmentTypes.get("", params);
        }

        /**
         * Retrieve a single assignment type.
         *
         * @param {number} id - The unique id of assignment type.
         * @returns {Promise<Assignments~AssignmentType|Error>} - The promise of assignment type..
         */
        function getOne(id) {
            return AssignmentTypes.one("", id).get();
        }

        /**
         * Creates a single assignment type.
         *
         * @param {Assignments~AssignmentType} assignmentType - The assignment type to create.
         * @returns {Promise<Number|Error>} - The promise of unique id of newly created assignment type.
         */
        function create(assignmentType) {
            return AssignmentTypes.post(assignmentType);
        }

        /**
         * Updates a single assignment type.
         *
         * @param {number} id - The unique id of  assignment type.
         * @param {Assignments~AssignmentType} assignmentType - The  assignment type to update.
         * @returns {Promise<Void|Error>} - The promise of operation outcome.
         */
        function update(id,  assignmentType) {
            return AssignmentTypes.one("", id).customPUT(assignmentType);
        }

        /**
         * Remove the assignment type with supplied id.
         *
         * @param {number} id - The unique id of assignment type
         * @returns {Promise<Object|Error>} - The promise of result.
         */
        function remove(id) {
            return AssignmentTypes.one("", id).remove();
        }
    }

})();

