/*
 * assignment-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.assignments')
        .component('assignmentCreate', {
            controller: AssignmentCreateController,
            bindings: {},
            templateUrl: 'app/assignments/assignment-create.html'
        });

    AssignmentCreateController.$inject = ['$state', 'AssignmentService', 'NotifierService'];

    /* @ngInject */
    function AssignmentCreateController($state, AssignmentService, NotifierService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.condominiumId = $state.params.condominiumId;
            $ctrl.assignment = { person: {}, assignmentType: {} };
        }

        function doSave() {
            AssignmentService.create($ctrl.condominiumId, $ctrl.assignment).then(function () {
                return NotifierService.notifySuccess('Assignments.Create.Success');
            }).then(function () {
                $state.go("^");
            }).catch(function () {
                return NotifierService.notifyError('Assignments.Create.Failure');
            });
        }
    }

})();

