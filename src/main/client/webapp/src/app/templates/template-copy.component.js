/*
 * template-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('templateCopy', {
            controller: TemplateEditController,
            bindings: {
                'template': '<',
                'typologies': '<'
            },
            templateUrl: 'app/templates/template-copy.html'
        });

    TemplateEditController.$inject = ['$filter','$state', 'TemplateService', 'NotifierService', '$q'];

    /* @ngInject */
    function TemplateEditController($filter, $state, TemplateService, NotifierService, $q) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.template = angular.copy($ctrl.template);
            $ctrl.template.title += $filter('translate')('Templates.Copy.Suffix')
        }

        function doSave() {
            return TemplateService.create($ctrl.template).then(function () {
                return NotifierService.notifySuccess('Templates.Copy.Success');
            }).then(function () {
                return $state.go("^");
            }).catch(function (err) {
                NotifierService.notifyError('Templates.Copy.Failure');
                return $q.reject(err);
            });
        }
    }

})();

