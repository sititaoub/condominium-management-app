/*
 * template-create.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.templates')
        .component('templateCreate', {
            controller: TemplateCreateController,
            bindings: {
                'typologies': '<'
            },
            templateUrl: 'app/templates/template-create.html'
        });

    TemplateCreateController.$inject = ['$state', 'TemplateService', 'NotifierService', '$q'];

    /* @ngInject */
    function TemplateCreateController($state, TemplateService, NotifierService, $q) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.template = {};
        }

        function doSave() {
            return TemplateService.create($ctrl.template).then(function () {
                return NotifierService.notifySuccess('Templates.Create.Success');
            }).then(function () {
                return $state.go('^', { inherits: true });
            }).catch(function (err) {
                NotifierService.notifyError('Templates.Create.Failure');
                return $q.reject(err);
            });
        }
    }

})();

