/*
 * template.constant.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.templates')
        .constant('templatesCustomItems', {
            'Meetings': {
                'FirstCallLocation': false,
                'FirstCallDate': false,
                'FirstCallTime': false,
                'FirstCallLongDate': false,
                'SecondCallLocation': false,
                'SecondCallDate': false,
                'SecondCallTime': false,
                'SecondCallLongDate': false,
                'Type': false,
                'Agenda': false,
                'Unrolling': false,
                'Call': false,
                'Date': false,
                'Time': false,
                'President': false,
                'Secretary': false
            },
            'Condominium': {
                'Name': false,
                'TaxCode': false,
                'Address': {
                    'Full': false,
                    'StreetName': false,
                    'BuildingNumber': false,
                    'PostalCode': false,
                    'Town': false,
                    'StateProvince': false,
                    'CountryCode': false
                },
                'Notes': false,
                'CadastralData': {
                    'State': false,
                    'DistrictCode': false,
                    'PropertyType': false,
                    'PropertySection': false,
                    'CadastralDistrict': false,
                    'Page': false,
                    'Parcel': false,
                    'Subordinate': false
                }
            },
            'FinancialPeriod': {
                'OpeningDate': false,
                'ClosingDate': false,
                'Description': false,
                'Kind': false
            },
            'HousingUnit': {
                'Typology': false,
                'Building': false,
                'Group': false,
                'Floor': false,
                'Number': false,
                'Owner': {
                    'FullName': false,
                    'Address': {
                        'Full': false,
                        'StreetName': false,
                        'BuildingNumber': false,
                        'PostalCode': false,
                        'Town': false,
                        'StateProvince': false,
                        'CountryCode': false
                    }
                },
                'Tenant': {
                    'FullName': false,
                    'Address': {
                        'Full': false,
                        'StreetName': false,
                        'BuildingNumber': false,
                        'PostalCode': false,
                        'Town': false,
                        'StateProvince': false,
                        'CountryCode': false
                    }
                }
            },
            "Recipient": {
                "Title": false,
                "FullName": false,
                'PostalAddress': {
                    'Full': false,
                    'StreetName': false,
                    'BuildingNumber': false,
                    'PostalCode': false,
                    'Town': false,
                    'StateProvince': false,
                    'CountryCode': false
                },
                'TaxAddress': {
                    'Full': false,
                    'StreetName': false,
                    'BuildingNumber': false,
                    'PostalCode': false,
                    'Town': false,
                    'StateProvince': false,
                    'CountryCode': false
                },
                "Phone": false,
                "Email": false,
                "PEC": false,
                "FAX": false,
                "TaxCode": false,
                "VatNumber": false,
                "Thousandths": false
            },
            "Manager": {
                "Name": false,
                "Address": {
                    'Full': false,
                    'StreetName': false,
                    'BuildingNumber': false,
                    'PostalCode': false,
                    'Town': false,
                    'StateProvince': false,
                    'CountryCode': false
                },
                "Phone": false,
                "Mobile": false,
                "FAX": false,
                "Email": false,
                "PEC": false,
                "Website": false,
                "TaxCode": false,
                "VatNumber": false,
                "Signature": false,
                "Logo": false
            },
            "Today": false
        });

})();