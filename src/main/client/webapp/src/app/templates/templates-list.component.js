/*
 * templates-list.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.templates')
        .component('templatesList', {
            controller: TemplatesListComponent,
            bindings: {},
            templateUrl: 'app/templates/templates-list.html'
        });

    TemplatesListComponent.$inject = ['$state', 'NgTableParams', 'TemplateService', 'NotifierService', 'AlertService'];

    /* @ngInject */
    function TemplatesListComponent($state, NgTableParams, TemplateService, NotifierService, AlertService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.toggleFilters = toggleFilters;
        $ctrl.deleteTemplate = deleteTemplate;

        ////////////

        function onInit() {
            $ctrl.loading = true;
            $ctrl.tableParams = NgTableParams.fromUiStateParams($state.params, {
                page: 1,
                count: 10,
                sort: 'title,asc'
            }, {
                getData: loadTemplates
            });
            $ctrl.showFilters = $ctrl.tableParams.hasFilter();
        }

        function toggleFilters() {
            $ctrl.showFilters = !$ctrl.showFilters;
        }

        function deleteTemplate(template) {
            AlertService.askConfirm('Templates.Remove.Message', template).then(function () {
                TemplateService.remove(template.id).then(function () {
                    return NotifierService.notifySuccess('Templates.Remove.Success');
                }).catch(function () {
                    return NotifierService.notifyError('Templates.Remove.Failure');
                }).finally(function () {
                    $ctrl.tableParams.reload();
                })
            });
        }

        function loadTemplates(params) {
            $state.go('.', params.uiRouterUrl(), { inherit: false });
            return TemplateService.getPage(params.page() - 1, params.count(), params.sorting(), params.filter())
                .then(function (data) {
                    params.total(data.totalElements);
                    return data.content;
                }).catch(function () {
                    return NotifierService.notifyError();
                }).finally(function () {
                    $ctrl.loading = false;
                });
        }
    }

})();

