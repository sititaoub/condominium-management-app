/*
 * templates.routes.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.templates')
        .config(setupRoutes);

    setupRoutes.$inject = ['$stateProvider'];

    /* @ngInject */
    function setupRoutes ($stateProvider) {
        $stateProvider.state('index.templates', {
            url: '/templates',
            abstract: true,
            redirectTo: 'index.templates.list',
            template: '<div class="templates-container" ui-view></div>',
            data: {
                skipFinancialPeriodCheck: true
            }
        }).state('index.templates.list', {
            url: '?page&count&sort&system&title',
            component: 'templatesList',
            params: {
                page: { dynamic: true, squash: true, value: '1', inherits: true },
                count: { dynamic: true, squash: true, value: '10', inherits: true },
                sort: { dynamic: true, squash: true, value: 'description,asc', inherits: true },
                system: { dynamic: true, inherits: true },
                title: { dynamic: true, inherits: true }
            }
        }).state('index.templates.list.create', {
            url: '/create',
            views: {
                '@index.templates': {
                    component: 'templateCreate'
                }
            },
            resolve: {
                typologies: ['TemplateService', function (TemplateService) {
                    return TemplateService.allTypologies();
                }]
            }
        }).state('index.templates.list.edit', {
            url: '/{templateId}/edit',
            views: {
                '@index.templates': {
                    component: 'templateEdit'
                }
            },
            resolve: {
                typologies: ['TemplateService', function (TemplateService) {
                    return TemplateService.allTypologies();
                }],
                template: ['$transition$', 'TemplateService', function ($transition$, TemplateService) {
                    return TemplateService.getOne($transition$.params().templateId);
                }]
            }
        }).state('index.templates.list.copy', {
            url: '/{templateId}/copy',
            views: {
                '@index.templates': {
                    component: 'templateCopy'
                }
            },
            resolve: {
                typologies: ['TemplateService', function (TemplateService) {
                    return TemplateService.allTypologies();
                }],
                template: ['$transition$', 'TemplateService', function ($transition$, TemplateService) {
                    return TemplateService.getOne($transition$.params().templateId);
                }]
            }
        });
    }
})();