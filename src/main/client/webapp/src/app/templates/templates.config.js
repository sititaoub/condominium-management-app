/*
 * templates.config.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.templates')
        .config(configI18n);

    configI18n.$inject = ['$translatePartialLoaderProvider'];

    /* @ngInject */
    function configI18n($translatePartialLoaderProvider) {
        $translatePartialLoaderProvider.addPart('templates');
    }

})();