/*
 * template-edit.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.meetings')
        .component('templateEdit', {
            controller: TemplateEditController,
            bindings: {
                'template': '<',
                'typologies': '<'
            },
            templateUrl: 'app/templates/template-edit.html'
        });

    TemplateEditController.$inject = ['$state', 'TemplateService', 'NotifierService', '$q'];

    /* @ngInject */
    function TemplateEditController($state, TemplateService, NotifierService, $q) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.doSave = doSave;

        ////////////

        function onInit() {
            $ctrl.templateId = $ctrl.template.id;
        }

        function doSave() {
            return TemplateService.update($ctrl.templateId, $ctrl.template).then(function () {
                return NotifierService.notifySuccess('Templates.Create.Success');
            }).then(function () {
                return $state.go("^");
            }).catch(function (err) {
                NotifierService.notifyError('Templates.Create.Failure');
                return $q.reject(err);
            });
        }
    }

})();

