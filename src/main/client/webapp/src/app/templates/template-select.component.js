/*
 * template-select.component.js
 *
 * (C) 2018-2018 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.templates')
        .component('templateSelect', {
            controller: TemplateSelectController,
            bindings: {
                id: '@?',
                name: '@?',
                ngModel: '<',
                ngRequired: '<?',
                ngDisabled: '<?'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/templates/template-select.html'
        });

    TemplateSelectController.$inject = ['TemplateService'];

    /* @ngInject */
    function TemplateSelectController(TemplateService) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;
        $ctrl.onChange = onChange;

        ////////////

        function onInit() {
            loadTemplates();
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue || {};
            };
        }

        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue($ctrl.value);
        }

        function loadTemplates() {
            TemplateService.getPage(0, 1000).then(function (page) {
                $ctrl.templates = page.content;
            });
        }
    }

})();

