/*
 * template-input.component.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.templates')
        .component('templateInput', {
            controller: TemplateInputController,
            bindings: {
                ngModel: '<',
                typologies: '<'
            },
            require: {
                ngModelCtrl: 'ngModel'
            },
            templateUrl: 'app/templates/template-input.html'
        });

    TemplateInputController.$inject = ['$filter', 'templatesCustomItems', '_'];

    /* @ngInject */
    function TemplateInputController($filter, templatesCustomItems, _) {
        var PREFIX = 'Templates.Form.CustomItems';

        var $ctrl = this;

        $ctrl.$onInit = onInit;

        $ctrl.onChange = onChange;
        $ctrl.editorSetup = editorSetup;

        ////////////

        function onInit() {
            $ctrl.ngModelCtrl.$render = function () {
                $ctrl.value = $ctrl.ngModelCtrl.$viewValue;
            };
        }
        
        function onChange() {
            $ctrl.ngModelCtrl.$setViewValue(angular.copy($ctrl.value));
        }

        function editorSetup(editor) {
            var translate = $filter('translate');
            editor.addMenuItem('placeholder', {
                text: translate(PREFIX + '.Label'),
                context: 'insert',
                icon: false,
                menu: generateItems(editor, templatesCustomItems, translate, '')
            });
            return false;
        }

        function generateItems(editor, items, translate, path) {
            return _.map(items, function (el, key) {
                var currentPath = path + '.' + key;
                var menuItem = {
                    text: translate(PREFIX + currentPath + '.Label')
                };
                if (angular.isObject(el)) {
                    menuItem.menu = generateItems(editor, el, translate, currentPath);
                } else {
                    menuItem.onclick = function () {
                        editor.insertContent('[[' + translate(PREFIX + currentPath + '.Field') + ']]');
                    };
                }
                return menuItem;
            });
        }
    }

})();

