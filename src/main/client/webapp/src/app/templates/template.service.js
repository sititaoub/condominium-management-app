/*
 * template.service.js
 *
 * (C) 2017-2017 Cedac Software S.r.l.
 */
(function () {
    'use strict';

    angular
        .module('condominiumManagementApp.templates')
        .factory('TemplateService', TemplateService);

    TemplateService.$inject = ['Restangular', 'contextAddressTemplates', '_'];

    /* @ngInject */
    function TemplateService(Restangular, contextAddressTemplates, _) {
        var TYPOLOGIES = "typologies";
        var TEMPLATES = "templates";
        var EXPANSION = "expansion";

        var Api = Restangular.withConfig(function (configurer) {
            configurer.setBaseUrl(contextAddressTemplates + 'v1');
            configurer.addResponseInterceptor(function (data, operation, what, url, response) {
                if (operation === "post") {
                    var location = response.headers('Location');
                    if (angular.isDefined(location) && location !== null) {
                        return location.substr(location.lastIndexOf('/') + 1);
                    }
                }
                return data;
            });
        });

        var service = {
            allTypologies: allTypologies,
            getPage: getPage,
            getOne: getOne,
            create: create,
            update: update,
            remove: remove,
            getExpansionUrl: getExpansionUrl
        };
        return service;

        ////////////////

        /**
         * @namespace Templates
         */

        /**
         * Templates page.
         *
         * @typedef {Object} Templates~TemplatesPage
         * @property {number} number - The requested page number.
         * @property {number} size - The requested page size.
         * @property {number} totalElements - The total number of elements.
         * @property {number} totalPages - The total number of pages.
         * @property {Templates~Template[]} content - The page content.
         */

        /**
         * Typology object.
         *
         * @typedef {Object} Templates~Typology
         * @property {number} id - The unique id of template.
         * @property {string} description - The description of typology.
         */

        /**
         * Template object.
         *
         * @typedef {Object} Templates~Template
         * @property {number} id - The unique id of template.
         * @property {string} title - The template title.
         * @property {string} description - The description of template.
         * @property {number} typologyId - The unique id of typology of template.
         * @property {string} [typologyName] - The name of typology.
         * @property {string} [content] - The content of template.
         * @property {boolean} shared - True if the template is shared.
         */

        /**
         * Retrieve the list of all typologies.
         *
         * @return {Promise<Templates~Typology[]|Error>} - The promise of list.
         */
        function allTypologies() {
            return Api.all(TYPOLOGIES).getList();
        }
        
        /**
         * Retrieve a single page of templates.
         *
         * @param {number} [page=0] - The page (0 based).
         * @param {number} [size=10] - The size.
         * @param {object} [sorting] - The sorting.
         * @param {object} [filters] - The filters.
         * @returns {Promise<Templates~TemplatesPage|Error>} - The promise of page.
         */
        function getPage(page, size, sorting, filters) {
            filters = _.omitBy(filters, function (val) {
                return val === "";
            });
            var params = angular.merge({}, filters, {
                page: page || 0,
                size: size || 10,
                sort: _.map(_.keys(sorting), function (key) {
                    return key + "," + sorting[key];
                })
            });
            return Api.all(TEMPLATES).get("", params);
        }

        /**
         * Retrieve a single template.
         *
         * @param {number} id - The unique template id.
         * @returns {Promise<Templates~Template|Error>} - The promise of template detail.
         */
        function getOne(id) {
            return Api.one(TEMPLATES, id).get();
        }

        /**
         * Create a new template.
         *
         * @param {Templates~Template} template - The template to be created.
         * @returns {Promise<Number|Error>} - The promise of unique identifier of created template
         */
        function create(template) {
            return Api.all(TEMPLATES).post(template);
        }

        /**
         * Updates an existing template.
         *
         * @param {number} id - The unique id of template to update.
         * @param {Templates~Template} template - The template to be updated
         * @returns {Promise<Void|Error>} - The promise of result.
         */
        function update(id, template) {
            return Api.one(TEMPLATES, id).customPUT(template);
        }

        /**
         * Remove the template with supplied id.
         *
         * @param {number} id - The unique id of template
         * @returns {Promise<Void|Error>} - The promise of result.
         */
        function remove(id) {
            return Api.one(TEMPLATES, id).remove();
        }

        /**
         * Retrieve the URL for the supplied template and expansion reference.
         *
         * @param {number} id the unqiue id
         * @param {string} expansionId the unique expansion id
         * @return {string} - The url
         */
        function getExpansionUrl(id, expansionId) {
            return Api.one(TEMPLATES, id).all(EXPANSION).getRestangularUrl() + '/' + expansionId + '/content';
        }
    }

})();
