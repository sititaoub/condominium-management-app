'use strict';

require('phantomjs-prebuilt').path = './node_modules/.bin/phantomjs';

module.exports = function (config) {
    config.set({
        autoWatch: false,
        //logLevel: config.LOG_DEBUG,
        frameworks: ['jasmine'],
        browsers: ['PhantomJS'],
        preprocessors: {
            'src/app/**/!(*spec|*mock).js': ['coverage'],
            'src/app/**/*.html': ['ng-html2js'],
            'src/app/**/*.json': ['ng-html2js']
        },
        reporters: ['mocha', 'junit', 'coverage', 'notify'],
        plugins: [
            'karma-phantomjs-launcher',
            'karma-jasmine',
            'karma-coverage',
            'karma-mocha-reporter',
            'karma-junit-reporter',
            'karma-ng-html2js-preprocessor',
            'karma-notify-reporter'
        ],
        ngHtml2JsPreprocessor: {
            stripPrefix: 'src/',
            moduleName: 'condominiumManagementApp.templates'
        },
        junitReporter: {
            outputDir: '../../../../target/karma-reports',
            suite: 'com.cedac.condominio102.managementapp.client'
        },
        coverageReporter: {
            reporters: [{
                type: 'cobertura',
                dir: '../../../../target/karma-coverage'
            }, {
                type: 'html',
                dir: 'coverage/'
            }, {
                type: 'text-summary'
            }],
            instrumenterOptions: {
                istanbul: { noCompact: true }
            }
        },
        notifyReporter: {
            reportEachFailure: true,
            reportSuccess: true
        }
    });
};
