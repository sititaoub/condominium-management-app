'use strict';

var gulp = require('gulp');

var paths = gulp.paths;

var util = require('util');

var browserSync = require('browser-sync');


function browserSyncInit(baseDir, files, browser) {
  var middleware = require('./proxy');

  browser = browser === undefined ? 'default' : browser;

  var routes = null;
  if(baseDir === paths.src || (util.isArray(baseDir) && baseDir.indexOf(paths.src) !== -1)) {
    routes = {
      '/bower_components': 'bower_components',
      '/scripts/tinymce/plugins': 'bower_components/tinymce/plugins',
      '/scripts/tinymce/skins': 'bower_components/tinymce/skins',
      '/scripts/tinymce/themes': 'bower_components/tinymce/themes',
      '/scripts/tinymce/langs': 'bower_components/tinymce-i18n/langs',
      '/custom': 'custom'
    };
  }

  browserSync.instance = browserSync.init(files, {
    startPath: '/index.html',
    ghostMode: false,
    server: {
      baseDir: baseDir,
      middleware: middleware,
      routes: routes
    },
    browser: browser
  });
}

gulp.task('serve', ['watch'], function () {
  browserSyncInit([
    paths.tmp + '/serve',
    paths.src
  ], [
    paths.tmp + '/serve/{app,components}/**/*.css',
    paths.src + '/{app,components}/**/*.js',
    paths.src + 'src/assets/images/**/*',
    // paths.tmp + '/serve/*.html',
    paths.tmp + '/serve/{app,components}/**/*.html',
    paths.src + '/{app,components}/**/*.html'
  ], process.env.BROWSER);
});

gulp.task('serve:dist', ['build'], function () {
  browserSyncInit(paths.dist, undefined, process.env.BROWSER);
});

gulp.task('serve:e2e', ['inject'], function () {
  browserSyncInit([paths.tmp + '/serve', paths.src], null, []);
});

gulp.task('serve:e2e-dist', ['build'], function () {
  browserSyncInit(paths.dist, null, []);
});

gulp.task('server-static', function () {
  var middleware = require('./proxy');
  var express = require('express');

  var app = express();
  app.use('/custom',express.static('custom'));
  app.use('/bower_components',express.static('bower_components'));
  app.use('/app',express.static(paths.src + '/app'));
  app.use('/assets',express.static(paths.src + '/assets'));
  app.use('/components',express.static(paths.src + '/components'));
  app.use('/condominium-management-app', middleware);
  app.use('/',express.static(paths.tmp + '/serve'));
  app.listen(3000);
});
