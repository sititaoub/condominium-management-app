'use strict';

var gulp = require('gulp');

var paths = gulp.paths;

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

gulp.task('partials', function () {
  return gulp.src([
    paths.src + '/{app,components}/**/*.html',
    paths.tmp + '/{app,components}/**/*.html'
  ])
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe($.angularTemplatecache('templateCacheHtml.js', {
      module: 'condominiumManagementApp'
    }))
    .pipe(gulp.dest(paths.tmp + '/partials/'));
});

gulp.task('json', function () {
  return gulp.src([
    paths.src + '/{app,components}/**/*.json'
  ])
      .pipe($.angularTemplatecache('templateCacheJson.js', {
        module: 'condominiumManagementApp'
      }))
      .pipe(gulp.dest(paths.tmp + '/partials/'));
});

gulp.task('html', ['inject', 'partials','json'], function () {
  var partialsInjectFile = gulp.src([
        paths.tmp + '/partials/templateCacheHtml.js',
        paths.tmp + '/partials/templateCacheJson.js'
      ],
      { read: false });
  var partialsInjectOptions = {
    starttag: '<!-- inject:partials -->',
    ignorePath: paths.tmp + '/partials',
    addRootSlash: false
  };

  var htmlFilter = $.filter('*.html');
  var jsFilter = $.filter('**/*.js');
  var cssFilter = $.filter('**/*.css');
  var assets;

  return gulp.src(paths.tmp + '/serve/*.html')
    .pipe($.inject(partialsInjectFile, partialsInjectOptions))
    .pipe(assets = $.useref.assets())
    .pipe($.rev())
    .pipe(jsFilter)
    .pipe($.ngAnnotate())
    .pipe($.uglify({preserveComments: $.uglifySaveLicense}))
    .pipe(jsFilter.restore())
    .pipe(cssFilter)
    .pipe($.replace('../../bower_components/bootstrap/fonts', '../fonts/'))
    .pipe($.replace('../../bower_components/fontawesome/fonts', '../fonts/'))
    .pipe($.replace('../../bower_components/angular-ui-grid/', '../fonts/'))
    .pipe($.csso())
    .pipe(cssFilter.restore())
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe($.revReplace())
    .pipe(htmlFilter)
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe(htmlFilter.restore())
    .pipe(gulp.dest(paths.dist + '/'))
    .pipe($.size({ title: paths.dist + '/', showFiles: true }));
});

gulp.task('json-assets', function () {
  return gulp.src(paths.src + '/assets/json/**/*')
      .pipe(gulp.dest(paths.dist + '/assets/json/'));
});

gulp.task('pdf-assets', function () {
  return gulp.src(paths.src + '/assets/pdf/**/*')
      .pipe(gulp.dest(paths.dist + '/assets/pdf/'));
});

gulp.task('images', function () {
  return gulp.src(paths.src + '/assets/images/**/*')
    .pipe(gulp.dest(paths.dist + '/assets/images/'));
});

gulp.task('fonts', function () {
  return gulp.src($.mainBowerFiles())
    .pipe($.filter('**/*.{eot,svg,ttf,woff,woff2}'))
    .pipe($.flatten())
    .pipe(gulp.dest(paths.dist + '/fonts/'));
});

gulp.task('fontawesome', function () {
    return gulp.src('bower_components/fontawesome/fonts/*.{eot,svg,ttf,woff,woff2}')
     .pipe(gulp.dest(paths.dist + '/fonts/'));
});

gulp.task('bootstrapfonts', function () {
  return gulp.src('bower_components/bootstrap/fonts/*.{eot,svg,ttf,woff,woff2}')
    .pipe(gulp.dest(paths.dist + '/fonts/'));
});

gulp.task('tinymce', function () {
    var fileFilter = $.filter(function (file) {
        return file.stat.isFile();
    });

    return gulp.src(['bower_components/tinymce/{plugins,skins,themes}/**/*', 'bower_components/tinymce-i18n/**/*'])
        .pipe(fileFilter)
        .pipe(gulp.dest(paths.dist + '/scripts/tinymce/'));
});


gulp.task('misc', function () {
  return gulp.src(paths.src + '/**/*.ico')
    .pipe(gulp.dest(paths.dist + '/'));
});

gulp.task('clean', function (done) {
  $.del([paths.dist + '/', paths.tmp + '/'], done);
});


gulp.task('build', ['html', 'json-assets', 'pdf-assets', 'images', 'fonts', 'fontawesome','bootstrapfonts', 'tinymce', 'misc']);
