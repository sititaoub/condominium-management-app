/*jshint unused:false */

/***************

 This file allow to configure a proxy system plugged into BrowserSync
 in order to redirect backend requests while still serving and watching
 files from the web project

 IMPORTANT: The proxy is disabled by default.

 If you want to enable it, watch at the configuration options and finally
 change the `module.exports` at the end of the file

 ***************/

'use strict';

var request = require('sync-request');
var url = require('url');
var chalk = require('chalk');
var proxyMiddleware = require('http-proxy-middleware');

console.log('Retrieving token ... ');

// Obtaining OAuth2 token

/* DEV */
/**/
var host = process.env.SERVER_HOST || '192.168.1.90:9000';
var clientId = process.env.CLIENT_ID || 'G5iWcAE9ph_test';
var clientSecret = process.env.CLIENT_SECRET || 'wqxaykISxVpUTZmRkVKu83JpzPFUl9';
var username = process.env.OAUTH_USERNAME || 'tech@gabetti.it';
var password = process.env.OAUTH_PASSWORD || 'gabetti123';

/* TEST */
/*
 var host = process.env.SERVER_HOST || '172.16.1.90:9000';
 var clientId = process.env.CLIENT_ID || 'G5iWcAE9ph';
 var clientSecret = process.env.CLIENT_SECRET || 'wqxaykISxVpUTZmRkVKu83JpzPFUl9';
 var username = process.env.OAUTH_USERNAME || 'tech@gabetti.it';
 var password = process.env.OAUTH_PASSWORD || 'gabetti123';
 */

/* CUSTOM */
/*
 */

// console.log('http://' + host + '/uaa/oauth/token?client_id=' + clientId + '&client_secret=' + clientSecret + '&grant_type=password&username=' + username + '&password=' + password);
// var res = request('GET', 'http://' + host + '/uaa/oauth/token?client_id=' + clientId + '&client_secret=' + clientSecret + '&grant_type=password&username=' + username + '&password=' + password);
// // var res = request('GET', 'http://192.168.1.90:9000/uaa/oauth/token?client_id=G5iWcAE9ph_test&client_secret=wqxaykISxVpUTZmRkVKu83JpzPFUl9&grant_type=password&username=admin@cedac.com&password=admin');
// var parsed = JSON.parse(res.getBody());
/*
{
  "sub": "admin@cedac.com",
  "iss": "CEDAC",
  "iat": 1491303025,
  "exp": 130340321845,
  "cbiCredentials": "00084@37000101@master"
}
 */
var token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZWNoQGdhYmV0dGkuaXQiLCJpc3MiOiJDRURBQyIsImlhdCI6MTUxMzc2MDQyNywiZXhwIjo2MTUxMzc2MDM2NywiY2JpQ3JlZGVudGlhbHMiOiIwMDA4NEAzNzAwMDEwMUBtYXN0ZXIifQ.cvudqJTcrsMG7jIU76xflymOClEXL3T5LghGYfJtyg8";

//var token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbkBjZWRhYy5jb20iLCJpc3MiOiJDRURBQyIsImlhdCI6MTQ5MTMwMzAyNSwiZXhwIjoxMzAzNDAzMjE4NDUsImNiaUNyZWRlbnRpYWxzIjoiMDAyMDBAMzQwMDAxMDFAbWFzdGVyIn0.ub53W81HtdZXbilAn6D-ULXKMRSR8EATjGYLXicRuTE";

console.log('Retrieved token: ' + token);

console.log('Configuring proxy ... ');

// configure proxy middleware context
var context = '/condominium-management-app';                     // requests with this path will be proxied

// configure proxy middleware options
var options = {
    //target: 'http://127.0.0.1:9103', // target host
    target: process.env.TARGET || 'http://linux-102-man-svil:9200',
    //target: 'http://localhost:9200',
    changeOrigin: true,               // needed for virtual hosted sites
    ws: true,                         // proxy websockets
    pathRewrite: {
        '^/condominium-management-app/zuul/condominium-people-server' : '/zuul/condominium-people-server',     // rewrite path
        '^/condominium-management-app/installer-server' : '/installer-server',     // rewrite path
        '^/condominium-management-app/templates-server' : '/templates-server',     // rewrite path
        '^/condominium-management-app/aggregatori-chartofaccounts-server' : '/aggregatori-chartofaccounts-server',     // rewrite path
        '^/condominium-management-app/aggregatori-invoices-server' : '/aggregatori-invoices-server',     // rewrite path
        '^/condominium-management-app/condominium-management-server' : '/condominium-management-server',     // rewrite path
        '^/condominium-management-app/condominium-meetings-server' : '/condominium-meetings-server',     // rewrite path
        '^/condominium-management-app/condominium-structure-server' : '/condominium-structure-server',     // rewrite path
        '^/condominium-management-app/condominium-people-server' : '/condominium-people-server',     // rewrite path
        '^/condominium-management-app/profile' : '/profile',       // remove path
        '^/condominium-management-app/management-report-server' : '/management-report-server',
    },
    proxyTable: {
        // when request.headers.host == 'dev.localhost:3000',
        // override target 'http://www.example.org' to 'http://localhost:8000'
        /* DEV */
        //'/condominium-management-app/installer-server/v1/condominiums': 'http://localhost:9200'
        /*
         '/condominium-management-app/profile': 'http://192.168.1.90:9104',
         '/condominium-management-app/condominium-registry-server/' : 'http://192.168.1.90:9200/', //API GW
         '/condominium-management-app/condominium-management-server': 'http://192.168.1.90:9200', //API GW
         */
        //'/condominium-management-app/aggregatori-invoices-server' : 'http://linux-102-man-svil:9149' // Direct call

        /* TEST */
        /*
         '/condominium-management-app/profile': 'http://172.16.1.43:9104',
         '/condominium-management-app/condominium-registry-server/' : 'http://172.16.1.90:9200/', //API GW
         '/condominium-management-app/condominium-management-server': 'http://172.16.1.90:9200', //API GW
         */

        /* CUSTOM */
        /*
         '/condominium-management-app/profile': 'http://127.0.0.1:9200',
         '/condominium-management-app/condominium-management-server': 'http://127.0.0.1:9200',
         '/condominium-management-app/condominium-registry-server/' : 'http://127.0.0.1:9200/',
         */
        //'/condominium-management-app/profile': 'http://192.168.1.90:9200',
        //'/condominium-management-app/condominium-management-server': 'http://192.168.1.90:9200',
        //'/condominium-management-app/installer-server/' : 'http://192.168.1.90:9200/',
        //'/condominium-management-app/management-report-server/' : 'http://192.168.1.90:9200/',

        //'localhost:3000' : 'http://localhost:9103',
    },
    logLevel: 'debug',
    headers: {
        Authorization: 'Bearer ' + token
    }
};
// create the proxy
var proxy = proxyMiddleware(context, options);

// module.exports = [proxyMiddleware];
module.exports = [proxy];

