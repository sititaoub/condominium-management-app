'use strict';

var gulp = require('gulp');
var Server = require('karma').Server;

var $ = require('gulp-load-plugins')();

var wiredep = require('wiredep');

var paths = gulp.paths;

function runTests(singleRun, done) {
    var bowerDeps = wiredep({
        directory: 'bower_components',
        exclude: ['bootstrap-sass-official'],
        dependencies: true,
        devDependencies: true
    });

    var testFiles = bowerDeps.js.concat([
        paths.src + '/app/**/*module.js',
        paths.src + '/app/**/*.js',
        paths.src + '/app/**/*.html',
        paths.src + '/app/**/i18n/*.json'
    ]);
    new Server({
        configFile: __dirname + '/../karma.conf.js',
        singleRun: singleRun,
        files: testFiles
    }).start();
}

gulp.task('test', function (done) {
    runTests(true /* singleRun */, done)
});
gulp.task('test:auto', function (done) {
    runTests(false /* singleRun */, done)
});
