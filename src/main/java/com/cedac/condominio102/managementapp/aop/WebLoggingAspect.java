package com.cedac.condominio102.managementapp.aop;
/*
 * WebLoggingAspect.java
 *
 * (C) 2016 - 2016 Cedac Software S.r.l.
 */




import static java.util.Arrays.asList;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class WebLoggingAspect {
    private static final Marker WEB_LAYER = MarkerFactory.getMarker("web");
    private static final Logger LOGGER = LoggerFactory.getLogger(WebLoggingAspect.class);

    @Pointcut("within(com.cedac.condominio102.managementapp.web.*)")
    public void webLayer() {
    }

    @Pointcut("within(@org.springframework.stereotype.Controller *)")
    public void controller() {
    }

    @Pointcut("webLayer() && controller() && execution(* *.*(..))")
    public void executionOfWebMethod() {
    }

    @Before("executionOfWebMethod()")
    public void logTraceBeforeEnter(JoinPoint jp) {
        if (LOGGER.isTraceEnabled(WEB_LAYER)) {
            Class<?> targetClass = jp.getTarget().getClass();
            LOGGER.trace(WEB_LAYER, "[WEB][ENTER]     {}.{}({})", targetClass.getSimpleName(),
                    jp.getSignature().getName(), asList(jp.getArgs()));
        }
    }

    @AfterReturning(value = "executionOfWebMethod()", returning = "retVal")
    public void logTraceAfterReturning(JoinPoint jp, Object retVal) {
        if (LOGGER.isTraceEnabled(WEB_LAYER)) {
            Class<?> targetClass = jp.getTarget().getClass();
            LOGGER.trace(WEB_LAYER, "[WEB][RETURNING] {}.{}({}) = {}", targetClass.getSimpleName(),
                    jp.getSignature().getName(), asList(jp.getArgs()), retVal);
        }
    }

    @AfterThrowing(value = "executionOfWebMethod()", throwing = "ex")
    public void logTraceAfterThrowing(JoinPoint jp, Throwable ex) {
        if (LOGGER.isTraceEnabled(WEB_LAYER)) {
            Class<?> targetClass = jp.getTarget().getClass();
            LOGGER.trace(WEB_LAYER, "[WEB][THROWING]  {}.{}({}) => {}: {}", targetClass.getSimpleName(),
                    jp.getSignature().getName(), asList(jp.getArgs()), ex.getClass().getName(), ex.getMessage());
        }
    }
}
