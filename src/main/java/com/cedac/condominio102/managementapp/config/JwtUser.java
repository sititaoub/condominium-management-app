/*
 * JwtUser.java
 *
 * (C) 2017 - 2017 Cedac Software S.r.l.
 */
package com.cedac.condominio102.managementapp.config;

import java.util.Date;

public class JwtUser {

    private String username;
    private String issuer;
    private Date issuedAt;
    private Date expiredAt;
    private String cbiCredentials;
    
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getIssuer() {
        return issuer;
    }
    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }
    public Date getIssuedAt() {
        return issuedAt;
    }
    public void setIssuedAt(Date issuedAt) {
        this.issuedAt = issuedAt;
    }
    public Date getExpiredAt() {
        return expiredAt;
    }
    public void setExpiredAt(Date expiredAt) {
        this.expiredAt = expiredAt;
    }
    public String getCbiCredentials() {
        return cbiCredentials;
    }
    public void setCbiCredentials(String cbiCredentials) {
        this.cbiCredentials = cbiCredentials;
    }
    @Override
    public String toString() {
        return "JwtUser [username=" + username + ", issuer=" + issuer + ", issuedAt=" + issuedAt + ", expiredAt="
            + expiredAt + ", cbiCredentials=" + cbiCredentials + "]";
    }

    
}
