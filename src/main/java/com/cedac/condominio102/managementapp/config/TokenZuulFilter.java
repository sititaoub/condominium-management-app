/*
 * UserHeaderExtractorFilter.java
 */
package com.cedac.condominio102.managementapp.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

import io.jsonwebtoken.JwtException;

/**
 * Zuul Filter that verify authentication token (oauth2 and/or jwt) and put user and roles from token 
 * to custom security headers.
 *
 */
@Component
public class TokenZuulFilter extends ZuulFilter {
    private static final Logger LOG = LoggerFactory.getLogger(TokenZuulFilter.class);

	private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
	private static final String BEARER_PREFIX = "bearer ";
    private static final String SESSION_TOKEN = "Cedac-session-jwToken";

	/*
	@Autowired
	private JwtUtil jwtUtil;
    */
	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 90;
	}

	@Override
	public boolean shouldFilter() {
		return true; 		
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		
		HttpServletRequest request = ctx.getRequest();
		HttpSession session =  request.getSession(false);
		
//        final String accessToken = (String) session.getServletContext().getAttribute(SESSION_TOKEN);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        TokenCustomUser user = (TokenCustomUser)auth.getPrincipal();
        String accessToken = user.getToken();
        LOG.debug("Zuul request url:'{}' for session:'{}' with accessToken:'{}'",
            new Object[]{ request.getRequestURI(), session.getId(), accessToken});
    		
            try {
    		
                ctx.addZuulRequestHeader(HttpHeaders.AUTHORIZATION, BEARER_PREFIX+" "+accessToken);           
    			
    			
    		} catch (JwtException ex) {
    		    LOG.warn("error token invalid:", ex);
    			ctx.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());
    			ctx.setSendZuulResponse(false);
    			
    		} catch(HttpClientErrorException ex){
                LOG.error("error client:", ex);
                ctx.setResponseStatusCode(ex.getStatusCode().value());
                ctx.setSendZuulResponse(false);
    		    
            } catch(RuntimeException ex){
                LOG.error("error runtime:", ex);
                ctx.setResponseStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
                ctx.setSendZuulResponse(false);
            }
    		
		
        return null;
	}
	
	
}
