/*
 * TokenCustomAuthenticationFilter.java
 *
 * (C) 2016 - 2016 Cedac Software S.r.l.
 */
package com.cedac.condominio102.managementapp.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

public class TokenCustomAuthenticationFilter extends OncePerRequestFilter  {
    private static final Logger LOG = LoggerFactory.getLogger(TokenCustomAuthenticationFilter.class);
    private static final String SESSION_TOKEN = "Cedac-session-jwToken";
    private static JwtUtil jwtUtil = new JwtUtil();
    static {
        jwtUtil.setIssuer("CEDAC");
        jwtUtil.setSecret("$myi0>YbD;7V5Q9");
        jwtUtil.setTokenExpirationTimeMinutes(60);
    }

    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException,
            ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        LOG.debug("start filter");
        
        try {
            String stringToken = request.getParameter("token"); 
            HttpSession session = req.getSession(false);
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            LOG.debug("filter stringToken:'{}' session:'{}'",stringToken,
                session!=null?session.getId():null);

//            if (stringToken == null && session == null) {
            if (stringToken == null && auth == null) {
                LOG.debug("header sec null  stringToken:'{}' session:'{}'",stringToken,
                    session!=null?session.getId():null);
                SecurityContextHolder.clearContext();
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Authorization header not found");
                
            } else {            
                if(auth != null){
//                if(stringToken == null){
                    TokenCustomUser user = (TokenCustomUser)auth.getPrincipal();
                    String stringTokenSession = user.getToken();
                    LOG.debug("Auth already created for session:'{}' auth stringToken:'{}' stringTokenSession:'{}'",
                        new Object[]{session.getId(), stringToken,stringTokenSession});
                    if(!StringUtils.equals(stringToken, stringTokenSession) && stringToken!=null){
                        LOG.debug("http session:'{}'reset token in session:'{}' with token from url:'{}'", 
                            new Object[]{session.getId(), stringTokenSession, stringToken});
                        auth = validateJwt(stringToken);
                        
                        SecurityContextHolder.getContext().setAuthentication(auth);
                        
                    }
                    
                    chain.doFilter(request, response);
                    
                } else {
                    try {
                        LOG.debug("MUST create session value:'{}' and set token:'{}'",
                            session!=null?session.getId():null, stringToken);
                        
                        //Authentication auth = validateJwt(stringToken);
                        auth = validateJwt(stringToken);
                                                
                        SecurityContextHolder.getContext().setAuthentication(auth);
                        LOG.debug("create auth for session:'{}'",session!=null?session.getId():null );
                        
                        session.getServletContext().setAttribute(SESSION_TOKEN, stringToken);
                        LOG.debug("created for session:'{}' set token:'{}'",session!=null?session.getId():null, stringToken);
                        
                        chain.doFilter(request, response);

                    } catch (ParseException e) {
                        SecurityContextHolder.clearContext();
                        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid token");
                        //throw new InvalidTokenException("Invalid token");
                    }
                }
            }
        } catch (AuthenticationException ex) {
            LOG.debug("Error auth:",ex);
            SecurityContextHolder.clearContext();

        }    
    }
    
    
    private Authentication validateJwt(String accessToken){
        LOG.debug("validate Jwt token:'{}'", accessToken);
        JwtUser user = jwtUtil.parseToken(accessToken);
        if(user != null){
            LOG.debug("Requesting user data for username '{}'", user.getUsername());

            Authentication auth = new CustomAuthenticationToken(new TokenCustomUser(user.getUsername(),accessToken));
            return auth;
        } else {
            return null;
        }
    }

    
}
