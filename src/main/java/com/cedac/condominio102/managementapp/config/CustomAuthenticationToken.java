/*
 * CustomAuthenticationToken.java
 *
 * (C) 2016 - 2016 Cedac Software S.r.l.
 */
package com.cedac.condominio102.managementapp.config;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class CustomAuthenticationToken extends AbstractAuthenticationToken {

    private static final long serialVersionUID = -2252297543020104101L;    
    private TokenCustomUser user;
    
    public CustomAuthenticationToken(TokenCustomUser user){
        super(user.getAuthorities());
        this.user = user;
    }
    
    public CustomAuthenticationToken(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    @Override
    public Object getCredentials() {
        return user.getPassword();
    }

    @Override
    public Object getPrincipal() {
        return user;
    }
    
}
