/*
 * JwtUtil.java
 *
 * (C) 2017 - 2017 Cedac Software S.r.l.
 */
package com.cedac.condominio102.managementapp.config;

import java.util.Date;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtUtil {
    private static final Logger LOG = LoggerFactory.getLogger(JwtUtil.class);
    
    private static final String TAG_CBI_CREDENTIALS = "cbiCredentials";
    
    @Value("${jwt.secret}")
    private String secret;
    
    @Value("${jwt.issuer}")
    private String issuer;
    
    @Value("${jwt.expiration.minutes}")
    private int tokenExpirationTimeMinutes;
    
    /**
     * Tries to parse specified String as a JWT token. If successful, returns User object with username, id and role
     * prefilled (extracted from token).
     * If unsuccessful (token is invalid or not containing all required user properties), simply returns null.
     * 
     * @param token the JWT token to parse
     * @return the User object extracted from specified token or null if a token is invalid.
     */
    public JwtUser parseToken(String token) {
        try {
            Claims body = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
            
            JwtUser u = new JwtUser();
            u.setUsername(body.getSubject());
            u.setIssuer(body.getIssuer());
            u.setIssuedAt(body.getIssuedAt());
            u.setExpiredAt(body.getExpiration());
            u.setCbiCredentials((String)body.get(TAG_CBI_CREDENTIALS));

            Date referenceTime = new Date();            
            if (u.getExpiredAt() == null || u.getExpiredAt().before(referenceTime)) {
                LOG.warn("The token: {} is expired",u);
                throw new JwtException("The token is expired");
            }
            
            return u;
            
        } catch (JwtException | ClassCastException e) {
            return null;
        }
    }
    
    /**
     * Generates a JWT token containing username as subject, and userId and role as additional claims. These properties
     * are taken from the specified
     * User object. Tokens validity is infinite.
     * 
     * @param u the user for which the token will be generated
     * @return the JWT token
     */
    public String generateToken(JwtUser u) {
        DateTime currentTime =new DateTime();
        
        Claims claims = Jwts.claims().setSubject(u.getUsername())
            .setIssuer(issuer)
            .setIssuedAt(currentTime.toDate())
            .setExpiration(currentTime.plusMinutes(tokenExpirationTimeMinutes).toDate());
        claims.put(TAG_CBI_CREDENTIALS, u.getCbiCredentials());

        
        return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS256, secret).compact();
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public int getTokenExpirationTimeMinutes() {
        return tokenExpirationTimeMinutes;
    }

    public void setTokenExpirationTimeMinutes(int tokenExpirationTimeMinutes) {
        this.tokenExpirationTimeMinutes = tokenExpirationTimeMinutes;
    }
    
    
}
