/*
 * TokenCustomUser.java
 *
 * (C) 2016 - 2016 Cedac Software S.r.l.
 */
package com.cedac.condominio102.managementapp.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class TokenCustomUser implements UserDetails {

    private static final long serialVersionUID = 1522586797005165952L;

    private Collection<? extends GrantedAuthority> authorities;
    private final String username;
    private final String token;
    
    public TokenCustomUser(String username, String token){
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("USER"));        
        
        this.authorities = authorities;
        this.username = username;
        this.token = token;
        
    }
    
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public String getToken() {
        return token;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    
}
