/*
 * ResourceServerConfig.java
 */
package com.cedac.condominio102.managementapp.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

/**
 * Configuration for the resource server.
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
    private static final String XSRF_TOKEN_COOKIE = "XSRF-TOKEN";
    private static final String XSRF_TOKEN_HEADER = "X-XSRF-TOKEN";
    private String logoutSuccessUrl = "http://localhost:9000/uaa/logout";

    public void setLogoutSuccessUrl(String logoutSuccessUrl) {
      this.logoutSuccessUrl = logoutSuccessUrl;
    }

    @Autowired
    private TokenCustomAuthenticationProvider customAuthenticationProvider;
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {        
        http.headers().cacheControl().disable();
        http
        .addFilterBefore(new TokenCustomAuthenticationFilter(), BasicAuthenticationFilter.class)                
        .authorizeRequests()
        .antMatchers("/metrics","/health","/info").permitAll();
    // @formatter:off
    http
        .antMatcher("/**")
            .authorizeRequests()
        .anyRequest()
            .authenticated()
     .and()
        .logout()
            .logoutUrl("/logout")
            .invalidateHttpSession(true)
            .deleteCookies(new String[0])
            .logoutSuccessUrl(this.logoutSuccessUrl)
        .and()
            .csrf()
            .csrfTokenRepository(csrfTokenRepository())
        .and()
        .addFilterAfter(csrfHeaderFilter(), CsrfFilter.class)
        .headers().frameOptions().disable();
    // @formatter:on
                    
    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    final CorsConfiguration config = new CorsConfiguration();
    config.addAllowedOrigin("*");
    config.addAllowedHeader("*");
    config.setAllowCredentials(true);
    config.addAllowedMethod("GET");
    config.addAllowedMethod("PUT");
    config.addAllowedMethod("POST");
    source.registerCorsConfiguration("/**", config);
    
    http.addFilterBefore(new CorsFilter(source), LogoutFilter.class);

    }
    
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {        
        auth.authenticationProvider(customAuthenticationProvider);        
    }    

    private Filter csrfHeaderFilter()
    {
      return new OncePerRequestFilter()
      {
        protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException
        {
          CsrfToken csrf = (CsrfToken)request.getAttribute(CsrfToken.class.getName());
          if (csrf != null) {
            Cookie cookie = WebUtils.getCookie(request, XSRF_TOKEN_COOKIE);
            String token = csrf.getToken();
            if ((cookie == null) || ((token != null) && (!token.equals(cookie.getValue())))) {
              cookie = new Cookie(XSRF_TOKEN_COOKIE, token);
              cookie.setPath("/");
              response.addCookie(cookie);
            }
          }
          filterChain.doFilter(request, response);
        }
      };
    }

    @Bean
    public CsrfTokenRepository csrfTokenRepository() {
      HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
      repository.setHeaderName(XSRF_TOKEN_HEADER);
      return repository;
    }

    
}
