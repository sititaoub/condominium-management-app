/*
 * WebConfig.java
 *
 * (C) 2017 - 2017 Cedac Software S.r.l.
 */
package com.cedac.condominio102.managementapp.config;

import javax.servlet.Filter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConfig {
    
    @Bean
    public Filter authCheckFilter(){
        TokenCustomAuthenticationFilter filter = new TokenCustomAuthenticationFilter();
         //supply dependencies
         return filter;
    }
 
}
