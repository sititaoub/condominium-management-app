package com.cedac.condominio102.managementapp.web;
/*
 * MainController.java
 */


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author mauro.franceschini
 * @since 1.0.0
 */
@Controller
public class MainController {
    @RequestMapping("/")
    public String home() {
        return "redirect:/dist/index.html";
    }

}
